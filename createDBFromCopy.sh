#!/bin/bash

HOST=$1
PORT=$2
PW=$3
DB=MDBG_1
CREATION_FILE=$4

psql --dbname=postgresql://postgres:${PW}@${HOST}:${PORT}/postgres -c "DROP DATABASE IF EXISTS \"${DB}\""

psql --dbname=postgresql://postgres:${PW}@${HOST}:${PORT}/${DB} -f ${CREATION_FILE}

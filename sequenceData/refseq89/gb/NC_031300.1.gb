LOCUS       NC_031300              16717 bp    DNA     circular VRT 12-OCT-2016
DEFINITION  Malaclemys terrapin terrapin mitochondrion, complete genome.
ACCESSION   NC_031300
VERSION     NC_031300.1
DBLINK      BioSample: SAMN05591378
KEYWORDS    RefSeq.
SOURCE      mitochondrion Malaclemys terrapin terrapin
  ORGANISM  Malaclemys terrapin terrapin
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Archelosauria; Testudines; Cryptodira; Durocryptodira;
            Testudinoidea; Emydidae; Malaclemys.
REFERENCE   1  (bases 1 to 16717)
  AUTHORS   Pop,M., Cummings,M., Koren,S., El Sayed,N., Hill,C., Treangen,T.,
            Roosenburg,W., Mount,S., Beier,S., Neukamm,J., Schliebs,O.,
            Biancani,L. and Goodheart,J.
  TITLE     The mitochondrial genome of the diamondback terrapin
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 16717)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (28-SEP-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 16717)
  AUTHORS   Pop,M., Cummings,M., Koren,S., El Sayed,N., Hill,C., Treangen,T.,
            Roosenburg,W., Mount,S., Beier,S., Neukamm,J., Schliebs,O.,
            Biancani,L. and Goodheart,J.
  TITLE     Direct Submission
  JOURNAL   Submitted (24-AUG-2016) Computer Science, University of Maryland,
            3120F Biomolecular Sciences Building, College Park, MD 20742, USA
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KX774423.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..16717
                     /organism="Malaclemys terrapin terrapin"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /sub_species="terrapin"
                     /db_xref="taxon:391341"
                     /dev_stage="adult"
                     /lat_lon="38.766944 N 76.381667 W"
                     /collection_date="18-Jun-2014"
     tRNA            complement(25..90)
                     /product="tRNA-Cys"
     tRNA            complement(91..161)
                     /product="tRNA-Tyr"
     gene            163..1710
                     /gene="COX1"
                     /db_xref="GeneID:29139755"
     CDS             163..1710
                     /gene="COX1"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009305083.1"
                     /db_xref="GeneID:29139755"
                     /translation="MILTRWFFSTNHKDIGTLYLIFGAWAGMVGTALSLLVRAELSQP
                     GALLGDDQIYNVIVTAHAFIMIFFMVMPIMIGGFGNWLVPLMIGAPDMAFPRMNNMSF
                     WLLPPSLLLLLASSGIEAGAGTGWTVYPPLAGNLAHAGASVDLTIFSLHLAGVSSILG
                     AINFITTAINMKSPAMSQYQTPLFVWSVLITAVLLLLSLPVLAAGITMLLTDRNLNTT
                     FFDPSGGGDPILYQHLFWFFGHPEVYILILPGFGMISHVVTYYAGKKEPFGYMGMVWA
                     MMSIGFLGFIVWAHHMFTVGMDVDTRAYFTSATMIIAIPTGVKVFSWLATLHGGMVKW
                     DAPMLWALGFIFLFTIGGLTGIVLANSSLDIVLHDTYYVVAHFHYVLSMGAVFAIMAG
                     FTHWFPLFTGYSLHQTWAKVHFGVMFAGVNMTFFPQHFLGLAGMPRRYSDYPDAYTLW
                     NSVSSIGSLISLVAVIMMMFIIWEAFSSKRKVTKVELTTTNVEWLHGCPPPYHTYEEP
                     AHVQTQE"
     tRNA            complement(1702..1772)
                     /product="tRNA-Ser"
                     /codon_recognized="UCN"
     tRNA            1775..1844
                     /product="tRNA-Asp"
     gene            1845..2531
                     /gene="COX2"
                     /db_xref="GeneID:29139743"
     CDS             1845..2531
                     /gene="COX2"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009305084.1"
                     /db_xref="GeneID:29139743"
                     /translation="MAHPLLLGFQDAMSPIMEELLHFHDHTLMIVFLISTLVLYIITL
                     MMTTKLTYTNTMNAQEVEMIWTILPAIVLITIALPSLRVLYLMDEINNPHLTIKAMGH
                     QWYWTYEYTDYENLEFDSYMVPTQDLPNGHFRLLEVDHRMVMPMESPIRMLISAEDVL
                     HSWAVPSLGVKTDAVPGRLNQTTFIVTRPGVFYGQCSEICGANHSFMPIVIESVPLQH
                     FENWSSLTLS"
     tRNA            2533..2604
                     /product="tRNA-Lys"
     gene            2608..2796
                     /gene="ATP8"
                     /db_xref="GeneID:29139744"
     CDS             2608..2796
                     /gene="ATP8"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="YP_009305085.1"
                     /db_xref="GeneID:29139744"
                     /translation="MPQLNPDPWLLILLSSWLTYIIILQPKISSYLSTNNPTNKDNKS
                     THMDPWTWPWIQHSSINS"
     gene            2766..3449
                     /gene="ATP6"
                     /db_xref="GeneID:29139745"
     CDS             2766..3449
                     /gene="ATP6"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009305086.1"
                     /db_xref="GeneID:29139745"
                     /translation="MNPTFFDQFMSPQILGVPLAILALLMPPIIFPTLNNRWLTNRLS
                     TIQLWAINLFTKQLMLPINKTGHQWSIILTSLMIMLLTTNLLGLLPYTFTPTTQLSMN
                     MGMAIPMWLAMVLSGLRNQPTKSLGHLLPEGTPTLLTPTLIIIETISLFIRPLALGVR
                     LTANLTAGHLLIQLTSTATLSLLPTMLTLSVVTTVILLSLTILELAVAMIQAYVFVLL
                     LSLYLQENV"
     gene            3449..4232
                     /gene="COX3"
                     /db_xref="GeneID:29139746"
     CDS             3449..4232
                     /gene="COX3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:4232,aa:TERM)
                     /transl_table=2
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009305087.1"
                     /db_xref="GeneID:29139746"
                     /translation="MAYQMHAYHMVDPSPWPLTGAAAALLMTSGLAMWFHYNSTLLMT
                     LGLLIMLLTMLQWWRDVVREGTFQGHHTPPVQKSLRYGMILFITSEVFFFIGFFWAFY
                     HSSLAPTPELGGCWPPTGITPLNPFEVPLLNTAVLLASGVTITWAHHSLMEANRNQTI
                     QALTLTILLGLYFTALQAMEYYETPFTISDGVYGSTFFIATGFHGLHVIIGSTFLIVC
                     LLRQLKFHFTSTHHFGFEAAAWYWHFVDVVWLFLYVSIYWWGS"
     tRNA            4233..4301
                     /product="tRNA-Gly"
     gene            4302..4651
                     /gene="ND3"
                     /db_xref="GeneID:29139747"
     CDS             join(4302..4475,4477..4651)
                     /gene="ND3"
                     /note="frameshift mechanism unknown (Mindell et al., 1998,
                     Mol. Biol. Evol., 15:1568-1571); TAA stop codon is
                     completed by the addition of 3' A residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:4651,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009305088.1"
                     /db_xref="GeneID:29139747"
                     /translation="MNVTISTMTIAITLSTILMALNYWLTLMKPDTEKLSPYECGFDP
                     LESARLPFSIRFFLVAILFLLFDLEIALLLPLPWAIQLPSPIYTFTWALIILLLLTLG
                     LIYEWVQGGLEWAE"
     tRNA            4652..4722
                     /product="tRNA-Arg"
     gene            4722..5019
                     /gene="ND4L"
                     /db_xref="GeneID:29139748"
     CDS             join(4722..4955,4957..5019)
                     /gene="ND4L"
                     /note="frameshift mechanism unknown (Mindell et al., 1998,
                     Mol. Biol. Evol., 15:1568-1571)'"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009305089.1"
                     /db_xref="GeneID:29139748"
                     /translation="MTPLHFSYYSAFVITITGLSLHQTHLISTLLCLESMMLSLYIAL
                     SMWPIQLQTSSFMLTPMLMLSFSACEAGTGLSLLVASSRTHGSDHLQNLNLLQC"
     gene            5013..6393
                     /gene="ND4"
                     /db_xref="GeneID:29139749"
     CDS             5013..6393
                     /gene="ND4"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:6393,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009305090.1"
                     /db_xref="GeneID:29139749"
                     /translation="MLKILLPTFMLFPTITLCKPKQLWPTMSINTFGIALLSMQWFKP
                     SQELTMSFSNYYMSIDYISAPLLTLSCWLTPLMILASQNHLITEPISRKRTFTFIIIL
                     LQVSLVLAFSATELIMFFIMFETTLIPTLAIITRWGNQMERLNAGTYFLFYTLIGSLP
                     LLIALLSLQNQIGTLSTHMIQLSQPTMSNTWAHTTWWFALLTAFMIKMPLYGLHLWLP
                     KAHVEAPIAGSMILAAVLLKLGGYGIIRIMPTLNPLSKTLPYPFMVLALWGVIMTSSI
                     CLRQTDLKSLIAYSSVSHMGLVIAATLTQTQWAYTGAITLMIAHGLTSSMLFCLANTN
                     YERTHSRTLLLARNMQLLYPLMSLWWLLASLANMAIPPTINLMGELTIIASLFNWSNI
                     TILMTGSGTIITATYTLYMLATTQWGGTPSYIKTMPPTHTREHLLMVLHTLPMMLLVM
                     KPELIWGVFY"
     tRNA            6394..6463
                     /product="tRNA-His"
     tRNA            6464..6530
                     /product="tRNA-Ser"
                     /note="Anticodon: GCU"
     tRNA            6530..6601
                     /product="tRNA-Leu"
                     /note="Anticodon: UAG"
     gene            6603..8432
                     /gene="ND5"
                     /db_xref="GeneID:29139750"
     CDS             6603..8432
                     /gene="ND5"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009305091.1"
                     /db_xref="GeneID:29139750"
                     /translation="MCLANTFLLLALLTLTMPLIVSLFMSKTWLILKTESTVKTTFYI
                     SLIPLLHSMLFPEQTLCDVTLLGTSLFSIKLNFIHDQYSAMFMPIALYVTWAILQYSR
                     WYMMENPHIDKFFKYLLFFLMAMMILITSNNLFQFFIGWEGVGIMSFLLIGWWRGREE
                     TLLSALMAIIYNRIGDFGLYIGMAWLIANTNSLNFQPLSYTNPPSLLTLLALILAATG
                     KSAQFGLHPWLPAAMEGPTPVSALLHSSTMVVAGIFLLIRVHPLLASNNTALSICLCL
                     GAITTFFASLSALSQNDLKKIIAYSTTSQLGLMMITIGLNQPDLAFLHICMHAFFKAM
                     LFMCAGFIIHSLNNEQDIRKMGGLHKPLPITSACLTIGNMALMGMPFLTGYYSKDAII
                     ETMSTSYLNTFALLMTLMATSFTALYTLRIMLLVQTGHPLQNSTYLLHENIPTIMNPI
                     TRLATGSIVAGWLITKNIQMLNTPTTTMPMWIKIAALTVTVIGLLVGFKVICMATKMP
                     LKPSKPCHPTSMAAYLNYLAHRSVSKNYLKMAQKIATHLADTSWFEYLGPKVTAKWQK
                     TPMRFTSLMQKGLIKIYMTSFLLTLSTMLIMIIVLSIVNFIFP"
     gene            complement(8555..9079)
                     /gene="ND6"
                     /db_xref="GeneID:29139751"
     CDS             complement(8555..9079)
                     /gene="ND6"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009305092.1"
                     /db_xref="GeneID:29139751"
                     /translation="MMYFMFLFGFCFVFWMVGVSCNPSPYYGVLSLVLGAASGCGVLI
                     GMGGSFVSLVLFLIYLGGMLVVFAYSSALVAEPYPVGWGSLGGLVYVVGFVLLVLGLA
                     VMGSFLYGVEGFGEVTADSVGLYVIRLDFGGVSWFYYYGSVMFLISGWGLLLTLFVVL
                     EMVRGLSCGMLHSI"
     tRNA            complement(9080..9147)
                     /product="tRNA-Glu"
     gene            9152..10291
                     /gene="CYTB"
                     /db_xref="GeneID:29139752"
     CDS             9152..10291
                     /gene="CYTB"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome b"
                     /protein_id="YP_009305093.1"
                     /db_xref="GeneID:29139752"
                     /translation="MIMNLRKTHPLAKIINNSFIDLPSPSNISAWWNFGSLLGTCLIL
                     QTLTGIFLAMHYSPDISLAFSSVAHITRDVQYGWLIRNMHANGASLFFMCIYLHIGRG
                     LYYGSYLYKETWNTGIILLLLTMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGNT
                     LVQWIWGGFSVDNATLTRFFTFHFLLPFTIMGLTLVHLLFLHETGSNNPTGLNSNTDK
                     IPFHPYFSYKDLLGIILMLALLLTLTLFSPNLLGDPDNFTPANPLSTPPHIKPEWYFL
                     FAYAILRSIPNKLGGVLALLLSILVLFLMPALHTSKQRTTQFRPLTQTLFWCLIANLL
                     VLTWIGGQPVENPFITIGQVASILYFSTLLILIPAAGTIENKMLT"
     tRNA            10295..10367
                     /product="tRNA-Thr"
     tRNA            complement(10369..10438)
                     /product="tRNA-Pro"
     D-loop          10439..11474
                     /note="control region"
     tRNA            11475..11544
                     /product="tRNA-Phe"
     rRNA            11545..12515
                     /product="12S ribosomal RNA"
     tRNA            12516..12586
                     /product="tRNA-Val"
     rRNA            12587..14204
                     /product="16S ribosomal RNA"
     tRNA            14205..14279
                     /product="tRNA-Leu"
                     /note="Anticodon: UAA"
     gene            14280..15250
                     /gene="ND1"
                     /db_xref="GeneID:29139753"
     CDS             14280..15250
                     /gene="ND1"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:15249..15250,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009305094.1"
                     /db_xref="GeneID:29139753"
                     /translation="MKPFLSNLMPPLMYMIPILIAVAFFTLLERKVLGYMQLRKGPNI
                     VGPYGLLQPVADGVKLFTKEPIYPLNSSTTLFMMSPILALLLALSIWLPLPLPFPLAD
                     LNLGLLFLISLSSFMVYSILWSGWASNSKYALMGALRAVAQTISYEVTLGIILLSLTL
                     FSGGFNMQTFMTTQEPMYLMFSSWPLMMMWYISTLAETNRAPFDLTEGESELVSGFNI
                     EYSAGPFALFFLAEYANILMMNTLTTILFLNPAYMSNTPELFSLSLMSKVMLLSAGFL
                     WIRASYPRFRYDQLMHLLWKNFLPITLALCLWHMSMPITSSGLPPML"
     tRNA            15251..15320
                     /product="tRNA-Ile"
     tRNA            complement(15319..15390)
                     /product="tRNA-Gln"
     tRNA            15390..15458
                     /product="tRNA-Met"
     gene            15459..16497
                     /gene="ND2"
                     /db_xref="GeneID:29139754"
     CDS             15459..16497
                     /gene="ND2"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:16497,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009305095.1"
                     /db_xref="GeneID:29139754"
                     /translation="MNPHASMIITSSLIMGPLLTVSSNHWILAWTGLEISTLAIIPLI
                     AKQHHPRAIEAATKYFLTQATASTLILFSSIMNAWTLGQWDITQMSNNTSCIILTTAL
                     AIKLGLAPFHFWLPEVLQGATTMTALILTTWQKLAPLSLLVITSQSLNTPLMLTLGLT
                     SALIGGWYGLNQTQLRKIMASSSIAHLGWMTTILTLSPKLTLLTFYTYTIMTSTTFLM
                     IKTLETNMMSVMMTSWTKLPTLNTMMMLTLMSLAGLPPLTGFMPKWLILQELTKQHMF
                     IMAIAMALISLLSLFFYLRISYYTTITLPPNSTNYLQQWRHKTNKKPYLATMTMLSIT
                     LLPITPTLLTIP"
     tRNA            16498..16572
                     /product="tRNA-Trp"
     tRNA            complement(16574..16642)
                     /product="tRNA-Ala"
     tRNA            complement(16644..16716)
                     /product="tRNA-Asn"
ORIGIN      
        1 ctttcccgct ctctaaaaag cgggaaaccc cgacaccaac taaagtgtat cttcaaattt
       61 gcaatttgac atgaatttca ctacggggtt tgataagaag aggaattaaa cctctgtcaa
      121 aaggtctaca gcccaacgct taaccactca gccatcttac ctgtgatttt aacccgctga
      181 tttttctcta ctaatcataa agacatcggc actttatact taatttttgg ggcctgagca
      241 ggaatagtag gcacagcatt aagtttattg gtccgcgcag aattaagcca accgggggcc
      301 cttttagggg acgaccaaat ctacaatgtt atcgtcacag cccatgcttt tattataatt
      361 ttcttcatgg ttataccaat tataattggt gggtttggaa attgacttgt ccccttaata
      421 attggagcac cagatatggc atttccacgt ataaacaata taagtttttg acttttaccc
      481 ccttcattat tattactact agcatcatca ggaattgaag caggggcagg cacaggctga
      541 actgtatatc ccccattagc cggaaaccta gcccacgccg gtgcctctgt agacctaact
      601 atcttttctc ttcacttagc aggagtatcc tcaattctag gggctattaa cttcattacc
      661 acagcaatta acataaaatc cccagctata tcacaatacc aaacacccct atttgtgtga
      721 tcagtactta ttacagctgt cctattacta ctatcgctac cagtactagc tgcaggcatc
      781 actatactat taacagatcg aaacctaaat acaaccttct ttgacccttc aggaggggga
      841 gacccaatct tataccaaca tttattctga ttctttggtc atcccgaagt atatatccta
      901 attctaccag gatttggcat aatctcccat gtagttacct attacgctgg taaaaaagaa
      961 ccttttggtt atataggaat agtttgagca ataatatcta ttggattttt aggcttcatc
     1021 gtgtgagccc accacatatt taccgtcggg atagacgtag atacccgagc ctattttaca
     1081 tctgcaacaa taattatcgc cattccaacg ggagtaaaag tattcagctg attagctacc
     1141 ttgcacggcg gaatagtcaa atgagatgcc ccaatactat gagcccttgg tttcatcttt
     1201 ctctttacta tcggaggcct aacaggcatt gtactagcca attcatcctt agacattgta
     1261 ctacatgaca cttattatgt agtagcacat ttccactatg tcctatcaat aggggctgta
     1321 ttcgctatta tagcaggatt tacccactga ttcccacttt ttacaggcta ctcactgcac
     1381 caaacttgag caaaagtaca ctttggagta atatttgcag gagtaaacat aacctttttc
     1441 ccccaacatt tcctaggcct tgctggaata ccacgacgtt actccgatta cccagacgca
     1501 tacactctat gaaattctgt ttcatcaatc ggatctttaa tttccctagt agcagtaatt
     1561 ataataatat ttattatctg agaagcattc tcctcaaagc ggaaagttac aaaagttgag
     1621 ctcacaacca ccaatgtaga gtgacttcat ggctgcccac ctccctacca tacctatgaa
     1681 gagccagccc atgtacaaac ccaagaaagg agggaatcga acccccttca attagtttca
     1741 agccaattac ataaccttta tgcttccttc tcccaagacg ttagtaaaat acattactta
     1801 accttgtcaa ggctaaatta taggtttaaa tcctttacga cttaatagca caccccctcc
     1861 tattaggatt ccaagacgca atatcaccta ttatagaaga actcctccac tttcatgacc
     1921 ataccctaat aattgtgttt ttaattagca ccctagtact ctacatcatc acactaataa
     1981 taacaacaaa actaacatat accaacacta taaatgccca agaagtagaa ataatttgaa
     2041 ctattttacc agccattgtc ctgattacta ttgcactccc atccctacga gtcctctacc
     2101 taatagatga aattaataac ccacatttaa ccattaaagc catgggacat caatgatatt
     2161 gaacatatga atacactgac tatgaaaacc ttgagtttga ctcctacatg gttccaactc
     2221 aagatctgcc aaacggacac ttccgattat tagaagtaga ccaccgtata gtgataccaa
     2281 tagaatcacc cattcggatg ctaatctcag ctgaggatgt cttacactca tgagcagtac
     2341 catcattagg tgtaaaaaca gatgcggtcc caggacgatt gaaccaaaca actttcattg
     2401 tcacacgacc tggggtattt tacggacagt gctcagaaat ctgcggagct aaccatagct
     2461 ttatgccaat cgtaattgaa tctgtgccac tacaacactt cgaaaattga tcttcactaa
     2521 cactttccta gccactatag aagctaaaag gatagcgcta gccttttaag ctagaaaaag
     2581 agaattttcc gtcctcctta gtggtttatg ccacaattaa atccagaccc gtgacttcta
     2641 atcctcctct cctcgtgatt aacgtacatt attattttgc agccaaaaat ttcatcctac
     2701 ttatcaacaa acaaccctac caataaagac aataaatcca cacacataga tccatgaact
     2761 tggccatgaa tccaacattc ttcgatcaat tcataagccc gcaaatccta ggggtcccac
     2821 tagccatcct agccttacta atacccccaa tcatcttccc aaccctaaat aaccgatgat
     2881 tgacaaatcg cttatcaacc atccaactat gagcaatcaa cttattcaca aaacaattaa
     2941 tactgccaat caacaaaaca ggccaccaat gatctattat cttgacatca cttataatca
     3001 tactcttaac aactaatcta cttggacttc taccatatac attcactcca accacacaac
     3061 tctctatgaa tataggaatg gctattccaa tgtggctagc tatagtactt tccggcctcc
     3121 gaaaccaacc aaccaaatca ctgggacatt tactaccaga aggtactcca accctattaa
     3181 ctccaaccct tatcattatt gaaacaatta gccttttcat ccgaccatta gccctgggcg
     3241 tgcgacttac ggccaactta acagccggcc atttattaat ccaactcacc tccaccgcaa
     3301 cactctccct attgccaaca atacttaccc tatctgtagt aaccactgta atcctcctat
     3361 cgttaacaat cctggagcta gccgtagcaa taattcaagc ctatgtgttt gtattattgc
     3421 taagcctata ccttcaagaa aacgtctaat ggcctaccaa atacatgcct accatatagt
     3481 agatccaagc ccatgaccac taacaggcgc agcagcagca ctactaataa cttcgggact
     3541 tgccatatga tttcactaca actcaacact gctaataacc ctgggcttat taattatact
     3601 actaaccata ctccaatgat gacgagatgt cgtacgagaa ggaactttcc aaggacacca
     3661 tacccctcct gtacaaaaaa gtctacgata tggtataatc cttttcatca catcagaagt
     3721 attcttcttc attgggttct tctgagcttt ttatcactca agcttagccc ccaccccaga
     3781 actgggcggg tgttgaccgc caacaggaat caccccatta aacccatttg aagtacccct
     3841 attaaataca gcagtcctgc tggcttcagg agtgacaatc acctgagctc accatagctt
     3901 aatagaagcc aaccgaaatc aaaccattca agccctcacc cttacaatcc tactaggctt
     3961 atacttcaca gcattacaag ccatagaata ctatgaaact ccattcacaa tttctgatgg
     4021 tgtatacggc tctacatttt ttatcgcaac aggcttccac ggacttcatg taatcattgg
     4081 ctcaacattc ttaattgtat gccttctacg acaactcaaa tttcacttca cctctactca
     4141 ccacttcgga tttgaagcag ctgcctgata ttgacatttc gtagatgtcg tatgactatt
     4201 tctctatgta tcgatctact gatggggctc atactcttct agtataatag tacaaatgac
     4261 ttccaatcat taagttttag ttgcaaccct aaagaagagt aataaatgta acaatctcaa
     4321 ctataacaat tgccattacc ctatctacaa tcctaatagc attaaactac tgactaacac
     4381 taataaaacc agatactgaa aaactatccc catatgaatg cgggtttgat ccattagaat
     4441 cagctcgcct accattctct atccgattct tcctgagtag caatcttatt cctcttattt
     4501 gacctagaaa tcgcactact cctacctcta ccatgagcca tccaactccc atcaccaatt
     4561 tacaccttta cttgagctct tattatctta ttacttttga cattaggact tatctatgag
     4621 tgagtccaag gaggcctaga atgagcagaa tagatagcta gtccaacata agacaactaa
     4681 tttcgactta gttaatcgtg attaaactcc acggctacca aatgacacct ttacatttca
     4741 gctactactc ggctttcgtt atcaccatca caggcctctc gttacaccaa actcacctaa
     4801 tttcaaccct actatgcctt gaaagtataa tactatcact ttatattgca ctatcaatat
     4861 ggcctattca actacaaacc tcatcattta tactaacccc catgttaata ttatccttct
     4921 cagcctgcga agccggcaca ggattatcat tacttagtag catcttcacg aacccacggc
     4981 tcagatcacc tgcagaactt aaaccttcta cagtgctaaa aattctactt ccaacattta
     5041 tattattccc aacaatcaca ctctgcaaac caaaacagct ctgacccacc atatccatta
     5101 acactttcgg aattgccctt ctaagcatac aatgatttaa gccctcccaa gaactaacca
     5161 taagtttctc caactattat ataagcatcg actacatctc agctccactc ctcaccctat
     5221 catgctgact caccccatta ataatcctag ctagtcaaaa ccatcttatt acagaaccaa
     5281 tttcacgaaa acgaaccttc accttcatta ttatcctatt acaagtctca ctagtacttg
     5341 ccttctcagc cactgaacta attatattct tcattatatt cgaaactaca ttaatcccaa
     5401 cactagcaat tattacacgt tggggtaatc aaatagaacg actaaacgct gggacttact
     5461 tcctattcta cacccttatt ggatctctcc cattactaat cgccctccta tccctacaaa
     5521 atcaaattgg tacactatcc actcacataa tccaactcag ccagcctact atatcaaaca
     5581 catgagccca tacaacatga tgatttgcat tactaactgc ctttataatc aaaataccac
     5641 tttatggact acacttatga ctaccaaaag cacacgtaga agccccaatc gcagggtcaa
     5701 taatcctagc agcagtacta ctcaaattag ggggatatgg aatcattcga attataccaa
     5761 cactaaatcc tctatcaaag acactcccct acccgtttat agtattagca ttatgaggag
     5821 taattataac tagttcaatc tgcctacgac aaacagattt aaaatcatta attgcttact
     5881 catcagtaag ccacataggt cttgttattg ctgcaacact aacacaaacc caatgagcat
     5941 acacaggtgc tattacactt ataatcgccc atggactaac atcatcaata ctcttttgct
     6001 tagctaacac aaactacgaa cgaactcata gccgaacact actattagcc cgaaacatgc
     6061 aactactata cccattaata agcctatgat gactactcgc tagcttagcc aacatagcca
     6121 ttccaccaac cattaaccta ataggagaac taactatcat cgcctcacta ttcaactgat
     6181 caaacattac aattctaata acaggttcgg gaaccattat taccgctaca tacaccctat
     6241 acatattggc cacaacacaa tgaggaggga caccctcata catcaaaaca ataccaccaa
     6301 cccacacacg agaacacctt ctcatagtcc tccataccct ccctataata ctgctagtaa
     6361 taaaaccaga actaatctga ggggtttttt actgttaata tagttttaaa acaaacatta
     6421 gactgtggct ctaaaaatag gagttcaaac ctccttataa accgagagag gtgtttcaca
     6481 ataagaactg ctaattccta taactgagaa taattccctc agctccctca cttttaaagg
     6541 atagaagtaa tccattggtt ttagaaacca tccacccttg gtgcaaatcc aagtaaaagt
     6601 ttatgtgtct agccaacaca ttccttctat tagcgctact caccctgaca ataccactta
     6661 ttgtctcatt attcatgtct aaaacatgac ttattctaaa aacagaatca actgtaaaaa
     6721 caacattcta catctccctg attccactac tccactcaat actatttcct gaacaaaccc
     6781 tttgtgacgt aaccttatta ggcacatcct tattttctat taaactaaac ttcattcacg
     6841 accaatattc cgccatattc ataccaatcg ccctatatgt cacatgagct atcctacagt
     6901 attctcgctg atatataata gaaaatccac atatcgacaa attctttaaa tacctattat
     6961 ttttcctaat agccataata attttaatta catctaacaa tctattccaa tttttcattg
     7021 gctgggaagg agtaggaatc atgtccttct tactcattgg ctgatgacgc ggccgagaag
     7081 agacactctt atcagcccta atagccatta tctacaaccg cattggtgac tttggattat
     7141 atatcggcat agcctgatta attgcaaaca caaactcatt aaattttcaa ccactctcat
     7201 atactaaccc cccttctcta ctaacacttc tagctctcat cctagctgca actggaaaat
     7261 cagcccaatt cggacttcat ccatgactac cagcagctat agaaggccca accccagtct
     7321 cagcactact acactccagc actatagttg tcgccggcat tttcctactt atccgcgtac
     7381 atcctttact agcatccaac aatacagctc tctctatttg cctatgctta ggagctatta
     7441 ccacattctt cgcatcctta tccgccctat cccaaaatga tcttaaaaaa atcattgcct
     7501 actctacaac aagccaactt ggccttatga taattaccat tggcctaaac caaccagatt
     7561 tagctttcct acacatctgc atgcacgcat tctttaaagc catattattt atatgcgcag
     7621 gctttatcat ccacagccta aacaatgaac aagacattcg aaaaatagga ggactgcata
     7681 aacccctacc aattacctct gcatgtttaa ctatcggcaa catagcactc atgggtatac
     7741 cattcttaac agggtactac tctaaagatg ccatcattga aactatatca acatcatact
     7801 taaacacctt cgccctactt ataacactaa tagcaacctc atttactgca ctctatacgc
     7861 tacgaattat attactagtg caaacaggac accccctaca aaactccaca tacctcctac
     7921 atgaaaacat tcccacaatc ataaacccaa ttactcgcct tgccacaggc agcattgtag
     7981 ccggatgact aattacaaaa aacattcaaa tacttaatac accaaccaca accataccaa
     8041 tatgaattaa aattgcagca ttaacagtaa cagtcatcgg cctcctagtg ggatttaaag
     8101 taatctgtat agccactaaa atacccctaa aaccttccaa accttgtcat ccaaccagca
     8161 tagccgccta cctcaactac ctagcccacc gtagtgtctc aaaaaattac ctaaaaatag
     8221 cccaaaaaat tgcaacccac ttagctgata catcctgatt cgaatatctt ggcccaaaag
     8281 taacagccaa atgacaaaaa actccaatac gctttacttc tttaatacaa aaaggcctca
     8341 tcaaaattta cataacctca ttcctcctaa cactatcaac aatacttatc ataattattg
     8401 tattaagcat tgttaatttc atttttccat aaaccctgcc aagtaaccta cataaccaat
     8461 tcaatcagta gcattactaa ccctaattgg gttaggacat tcatacaccc atctaataat
     8521 agctactgat taaaataacc cacaatatct aacccctaat agaatgcaac atcccacaag
     8581 acaagcctcg caccatctcc agcacaacaa ataatgttag caacaaccct caaccagaaa
     8641 tcaaaaacat cacactacca taataataaa accacgaaac tccaccaaaa tccaaacgaa
     8701 taacatacaa tccaacacta tccgccgtaa cttctccaaa cccttccaca ccatacaaaa
     8761 aagaacctat aaccgcaaga cctaaaacaa gcaacacaaa ccctaccaca taaaccaacc
     8821 ctcccaaact cccccaacca acaggataag gctcagccac taacgcagat gaataagcaa
     8881 aaaccactaa catcccccct aaataaatta aaaataacac caaagataca aaagatcctc
     8941 ctataccgat caacaccccg cacccggagg ctgcccctaa gactaaactt agaaccccgt
     9001 aataaggaga cgggttacaa gaaacaccca ccattcaaaa aacaaagcag aagccaaata
     9061 aaaacataaa gtatatcata gttattactc gggtttcaac cgagacctgt ggtttgaaaa
     9121 accaccgttg ttttcaacta taataaaacc aatgatcata aacctccgaa aaacccaccc
     9181 actagcaaaa atcatcaaca actcattcat tgatctacca agcccctcca acatctctgc
     9241 ttgatgaaac tttggatcct tattaggcac ttgcctaatt ctacaaaccc ttactggaat
     9301 tttcctggct atacactact ccccagacat ttcactagca ttctcatcag tagcccacat
     9361 tacccgagac gtacaatacg gatgacttat ccgtaatata catgctaacg gcgcctccct
     9421 cttcttcata tgcatctacc ttcatattgg acgaggactt tattacggtt catatttata
     9481 taaagaaacc tgaaacacag gaattatttt actactccta acaatagcca ctgcattcat
     9541 aggctatgtt ttaccatgag gccaaatatc cttctgaggt gcaaccgtta ttaccaatct
     9601 actctcagct attccataca ttggcaacac actagtacaa tgaatctgag gcgggttctc
     9661 agtagacaac gcaaccctga cccgattctt taccttccac ttcctcctac catttaccat
     9721 tataggtcta acactagtac acctactttt tctacatgaa actggatcaa acaacccaac
     9781 agggttaaac tcaaacaccg ataaaatccc attccacccc tatttctcat acaaagacct
     9841 cctaggcatc attttaatac tagctctcct actaacccta acattattct ctccaaacct
     9901 tttaggggac ccagataatt tcacaccagc taacccacta tccacccctc cacatattaa
     9961 accagagtga tactttcttt tcgcctacgc aattctacga tctatcccaa acaagctagg
    10021 tggagtactt gccctactac tatctattct tgtactattt ctaatacccg ccctacacac
    10081 atcaaaacaa cgaacaaccc aattccgacc acttacacaa accctattct gatgtttaat
    10141 cgccaacctt ttagtactaa catgaattgg aggacaaccc gttgaaaacc cattcattac
    10201 aatcggccaa gtagcctcta ttctctattt ctcaacctta ttaatcctca tccctgccgc
    10261 aggcacaatt gaaaacaaaa tactaactta aaacattcta gtagcttaac ccattaaagc
    10321 attggtcttg taaaccaaag actgaagact acaaccttcc tagaataatc aaaagagaag
    10381 gactttaacc ttcatccccg gctcccaaag ctgagatctt ttattaaact acctcttgat
    10441 gaggtatacc ggggcataaa cactaaatac aagcccattt tcatctaccg ttgctcggta
    10501 gaagattttt taaggtaccc ccccctcccc cccaaagggt ttatcgggca taaacgctaa
    10561 atacaagccc attttcatct accgttgctc gggtagaaaa tatccacata acactctcta
    10621 tgtattatcg tgcattcatt tattttccgt tagcatatct ttaacaatgt tattgtttaa
    10681 tttgacattt acatgaaatc attagtttta cataagaatt atttcaacat gaatattata
    10741 taggtattgt taatgtaatg gtttaaggac ataaacttaa atagttattc tacaccatga
    10801 ctatcgccgc agtacctggt tatttattaa tctacctaat cacgagaaat aagcaaccct
    10861 tgttagtaag atacaacatc actagtttca cgtccatata cagatggcgt acataactga
    10921 tctattctgg cctctggttg ttttttcagg cacattgtac taataaagtt catacgtctc
    10981 tttttaaaag gcctctggtt aatgtgttct atacattaag tttataacct ggcatatagt
    11041 ggttttactt gcatatagta tctctttttt tctctgtacc ttcaggcccg catacctgat
    11101 acctgccgac tcagtgaaac tggaccctcg ttcaaattgg ttggctttac ataattgata
    11161 tggtattatt taattaatgc tagtaggaca taaaattttg caaaaaccta cgacagtaat
    11221 ttcaacctaa acagttaaaa ctacatatct tatttaacct aaacccccct accccccata
    11281 aaaactaaca ccagtccgaa tagccacttt attctcgtca aacccctaaa tccgagggca
    11341 actaaactga cataatgtta attatggtac taagttgaag taagagtacc catcatatat
    11401 attatattat attatattat attatattat attatattat atatatatat tatatatata
    11461 ttatatatat atatgttatt gtagcttatc ataaagcacg gcactgaagt tgccaagatg
    11521 ggtaaccaat ataccccaaa aacacaaaga tttggtccta accttactgt tactttttgc
    11581 taaacttaca catgcaagta tcagcacacc agtgaaaacg ccctagcaat ctaataagac
    11641 tgaaggagcc ggtatcaggc gcaccatgat agcccaaaac acctagcttt agccacaccc
    11701 ccaagggcac tcagcagtga taaaaattaa gcaataagcg aaagcttgac ttagtcaata
    11761 gcaaacatga gctggtaaat ctcgtgccag ccaccgcggt tacacaagaa gcccgaacta
    11821 acagacaacc ggcgtaaaat gtgattaaaa ctttttaacc cataaaatta aggtgaacta
    11881 cttacttcgc tgtcatacgc aaaagtatac attaacacac tatgaaaata accttaatat
    11941 aacggaacta tttgaaccca cgatcgctaa aacacaaact gggattagat accccactat
    12001 gcttagccct aaacctagat atttatatac aaaaatatcc gccagagaat tacgagcgaa
    12061 aacgcttaaa actctaagga cttggcggta cctcaaaccc acctagagga gcctgttcta
    12121 taatcgataa cccacgattc acctcaccat ctcttgccaa ccagcctata taccaccgtc
    12181 accagcttac cctatgaagg atacaaaagt aagcaaaaca atattaacca ttaacaagtc
    12241 aggtcaaggt gtagctaatt gaaatggaag aaatgggcta cattttctat actagaaata
    12301 acactcacgg aaaggaacca tgaaacagtc cagcaagtag gatttagcag taaactggga
    12361 acagagagcc caatttaaac cggtcctgag gtgcgcacac accgcccgtc accctcatca
    12421 atataaacca acaaaccata aataaactat aacaaataaa acagatgagg caagtcgtaa
    12481 caaggtaagt gcaccggaag gtgtacttgg aatatcaaaa catagcttaa caataaagca
    12541 ttcagcttac acctgaaaga tgtctattaa aacaggacta ttttgagcaa ttaatcctag
    12601 cccaacaaca aacaataatc catcaaacaa taattacact accaaaaact taaccaaaac
    12661 attttactat ctaagtatag gtgatagaaa agtaaaacag gagcaataaa gacagtaccg
    12721 caagggaaag atgaaaaaca tgaaagacca ctaagcagta aaaagcaaag attaaccctt
    12781 atacctcttg catcatgatt taaccagcac acttaagcaa agagaactaa agtctaaacc
    12841 cccgaaacca agtgagctac ttaaaggccg ccaatatcac aggctaaatc cgtctctgtg
    12901 gcaaaagagt ggaaagacct ataagtagaa gtgaaaagcc tatcgaactt ggtgatagct
    12961 ggttgctcaa taaatgaatc tgagttcagc cttaaatctt ctaaaaacaa cctaaagttt
    13021 gcccaaaaga aaagtttaag atatattcaa ttggggtaca gcccaattga aaaaggacac
    13081 aacctaaaat ggaggacaaa acaccaaagt atattactcc gtaggcctta aagcagccac
    13141 catcaaagaa agcgtcaaag cccactatta tgtaaatatt aacataaaac ttttatcccc
    13201 aaataatact gagccattct acccctatag aagaactaat gctaaaatga gtaacaagaa
    13261 gataaaactt ctcttatgcg ctagcttaaa tcataataga taaactactg attattaaca
    13321 atcaataata tatgaaacaa caacacttaa aacaccatat aatcaaactg ttaacccaac
    13381 acaggagcgc acataagaaa gattaaaatc cgtaaaagga actaggcaaa ctaaacgagc
    13441 ccgactgttt accaaaaaca tagcccctag caacacacaa gtattagggg taatgcctgc
    13501 ccagtgacac tgttttaaac ggccgcggta tcctaaccgt gcaaaggtag cgtaatcact
    13561 tgtcttccaa ataaagacta gaatgaatgg ccaaacgagg ttctacctgt ctcttacaga
    13621 taatcagtga aattggtctc cccgtgcaaa agcgaggata accctataag acgagaagac
    13681 cctgtggaac tttaaacgca aatcaactac catcaacacc aaactaaaga tttataataa
    13741 actagtacat gatccacgtt ttcggttggg gcgacctcgg agtaaagcaa aacctccgaa
    13801 aaaagagcat acttctttaa acctagactt acaactcaaa gtgctagcag taaaaagatc
    13861 caatatattt gatcaacgaa ccaagctacc ccagggataa cagcgcaatc ccatcctaga
    13921 gcccttatcg acgatggggt ttacgacctc gatgttggat caggacatcc tgatggtgaa
    13981 accgctatca agggttcgtt tgttcaacga ttaatagtcc tacgtgatct gagttcagac
    14041 cggagcaatc caggtcggtt tctatctata tgaactttaa gtcttttcta gtacgaaagg
    14101 accgaaaaga caaggcctac attaaaaaca agccttacct tacattagtg aaaacaacta
    14161 aactgataat taaagcaata aaccacaacc ccaaaataag ggttattggc gtggcagagc
    14221 tgggtaaaat gcaaaaggcc taaacccttt atccaggggt tcaactcccc tccccaatta
    14281 taaaaccttt tctatccaac ttaatacctc cactcatata tataatccca atcttaattg
    14341 ctgttgcctt ctttacccta ttagaacgaa aagtgctcgg atatatacaa cttcgaaaag
    14401 gacccaacat tgtaggccca tatggactat tacagccagt agcagacggt gtaaaactct
    14461 tcaccaaaga accaatttat ccactaaatt catccaccac actatttata atatcaccaa
    14521 ttctggccct attattagct ttatcaattt gactcccact acccctacct ttcccactgg
    14581 ctgacctaaa tctaggcctc ctattcctaa tctccttatc tagcttcata gtttactcta
    14641 tcctatgatc tggttgggct tcaaattcta aatacgccct aatgggcgcc ctacgagcag
    14701 tagcccagac tatctcatat gaagtaaccc taggaattat cctactctca ttaactttat
    14761 tctcaggtgg atttaacata caaacattta taaccacaca agaaccaata tacttaatat
    14821 tttcatcctg accactaata ataatatgat acatctccac actagcagaa acaaatcggg
    14881 caccattcga cctaaccgaa ggagaatccg aactggtatc tggatttaac attgaatact
    14941 ccgccggacc tttcgcctta ttcttcctgg cagagtatgc caacatccta ataataaata
    15001 cactcacaac tattctattc cttaacccag cctacataag caacacccca gaactattct
    15061 ctctatcact aatatcaaaa gtaatactac tctcagcagg attcctatga atccgagcct
    15121 cctatccacg atttcgatac gatcaactta tgcacctact atgaaaaaac ttccttccaa
    15181 tcactctagc attatgcctt tgacatatat caataccaat cacctcctct ggactccctc
    15241 caatactata ggatacgtgc ctgaactaaa gggttacctt gatagggtga ataatagagg
    15301 ctaaaaccct ctcgtctcct agaaaaatag gactcgaacc tacaccagag agatcaaaac
    15361 tctctatact tccaattata ctacatccta gtaaagtcag ctaactaagc tattgggccc
    15421 ataccccgaa aatgttggtt taaacccttc ctctactaat aaatccgcat gcaagcataa
    15481 ttattacttc aagtctaatt ataggccccc tacttacagt atctagcaac cactgaatct
    15541 tagcatggac tggtctagaa atcagcaccc tggctattat tccattaatc gctaaacaac
    15601 accacccacg agcaattgaa gcagctacca aatacttcct aacacaagca actgcttcaa
    15661 cactaatctt attctcaagt attataaatg cctgaacatt aggacaatga gatatcacac
    15721 aaatatctaa taacacttca tgcattattc tcactacagc tttagccatc aaattaggat
    15781 tagctccatt tcacttttga ttaccagaag tactacaagg agccactaca ataacagccc
    15841 taatcttaac cacctgacag aaattagctc cattatccct actagtaatt acctctcaat
    15901 ccctaaatac accactaata ctaacgctag gactaacatc cgccctaatc ggtggatgat
    15961 atggactaaa ccaaacccaa ctacgaaaaa tcatagcatc atcctccatc gcccacctag
    16021 gatgaataac cacaatcctt actctatccc ctaaactcac actacttaca ttctacacat
    16081 acaccatcat aacctcaaca acatttctaa taattaaaac tttagaaaca aacataatat
    16141 ccgtaataat aacatcatga acaaaattac caacactaaa caccatgata atactcaccc
    16201 ttatatcact agctggccta cccccactaa caggattcat acctaaatga ttaattctcc
    16261 aagaactaac taaacaacac atattcatta tagccattgc aatagcccta atctcactac
    16321 tcagcctatt cttttaccta cgaatctcat actacacaac catcacatta ccaccaaact
    16381 caaccaacta cttacaacaa tggcgccaca aaaccaacaa aaaaccctac ctagctacaa
    16441 taaccatact atccatcacc cttctaccaa ttacaccaac cctactaacc atcccataga
    16501 aacttaggat caagcctgtt aaaccagggg ccttcaaagc cccaaacaag agatagaacc
    16561 tcttagtttc tgctaagacc tataagactc tatcttatat cttatgaatg caactcaaac
    16621 actttaatta agctaaggcc ttactagaca aatgggcctc gatcccataa caatttagtt
    16681 aacagctaaa cacccaatcc agcgggcttt tgcctac
//

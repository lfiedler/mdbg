LOCUS       NC_025315              16722 bp    DNA     circular VRT 26-SEP-2016
DEFINITION  Piaractus brachypomus mitochondrion, complete genome.
ACCESSION   NC_025315
VERSION     NC_025315.2
DBLINK      BioProject: PRJNA264694
KEYWORDS    RefSeq.
SOURCE      mitochondrion Piaractus brachypomus (pirapitinga)
  ORGANISM  Piaractus brachypomus
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Actinopterygii; Neopterygii; Teleostei; Ostariophysi;
            Characiformes; Characoidei; Piaractus.
REFERENCE   1  (bases 1 to 16722)
  AUTHORS   Chen,H., Li,S., Xie,Z., Zhang,Y., Zhu,C., Deng,S., Li,G. and
            Huang,H.
  TITLE     The complete mitochondrial genome of the Piaractus brachypomus
            (Characiformes: Characidae)
  JOURNAL   Mitochondrial DNA A DNA Mapp Seq Anal 27 (2), 1289-1290 (2016)
   PUBMED   25090392
REFERENCE   2  (bases 1 to 16722)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (23-OCT-2014) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 16722)
  AUTHORS   Chen,H., Li,S., Xie,Z., Zhang,Y., Zhu,C., Deng,S., Li,G. and
            Huang,H.
  TITLE     Direct Submission
  JOURNAL   Submitted (11-JUN-2014) Key Laboratory of Aquaculture in South
            China Sea for Aquatic Economic, Animal of Guangdong Higher
            Education Institutes, Guangdong Ocean University, No. 44, Haibing
            Road, Zhanjiang, Guangdong 524088, P.R. China
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KJ993871.
            On Sep 26, 2016 this sequence version replaced NC_025315.1.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..16722
                     /organism="Piaractus brachypomus"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /db_xref="taxon:42529"
     tRNA            2..68
                     /product="tRNA-Phe"
     rRNA            69..1021
                     /product="12S ribosomal RNA"
     tRNA            1022..1093
                     /product="tRNA-Val"
     rRNA            1094..2780
                     /product="16S ribosomal RNA"
     tRNA            2781..2856
                     /product="tRNA-Leu"
                     /codon_recognized="UUR"
     gene            2857..3831
                     /gene="ND1"
                     /db_xref="GeneID:20834360"
     CDS             2857..3831
                     /gene="ND1"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009093548.2"
                     /db_xref="GeneID:20834360"
                     /translation="MLTLLITHVINPLAYIVPVLLAVAFLTLIERKVLGYMQLRKGPN
                     VVGPYGLLQPIADGLKLFIKEPVRPSTSSPFLFLAAPILALTLALMLWAPMPIPYPVT
                     DLNLGILFILALSSLAVYSILGSGWASNSKYALIGALRAVAQTISYEVSLGLILLSII
                     IFTGGFTLQMFNVTQEATWLLLPAWPLAAMWYVSTLAETNRAPFDLTEGESELVSGFN
                     VEYAGGPFALFFLAEYANILLMNTLSVILFLGASHIPTFPELTSVNLMTKAALLSILF
                     LWIRASYPRFRYDQLMHLVWKNFLPLTLALILWHTALPIAFAGLPPQL"
     tRNA            3838..3913
                     /product="tRNA-Ile"
     tRNA            complement(3911..3981)
                     /product="tRNA-Gln"
     tRNA            3982..4049
                     /product="tRNA-Met"
     gene            4050..5094
                     /gene="ND2"
                     /db_xref="GeneID:20834348"
     CDS             4050..5094
                     /gene="ND2"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:5094,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009093549.2"
                     /db_xref="GeneID:20834348"
                     /translation="MNPYVLTVFLFSLGLGTTLTFASSHWLLAWMGLEINTLAILPLM
                     AQHHHPRAVEATTKYFLTQATAAATILFASTTNAWLVGEWSITHLSHPIAITMAMMGL
                     ALKIGLAPLHFWLPEVLQGLDLTTGLIMSTWQKLAPFALLVQMAPTIDPALLTALGVL
                     STLIGGWGGLNQTQLRKILAYSSIAHLGWMVIILQFAPQLTLLALGTYIIMTSAAFLT
                     FKATMTTKINTLALAWTKTPTLTTTAALVLLSLGGLPPLTGFMPKWLILQELTKQGLP
                     LIATLAALSALLSLYFYLRLCYAMTLTISPNTNNSTTPWRLPTTQTTLPLTITTTATL
                     LLLPLTPAIVALTH"
     tRNA            5095..5168
                     /product="tRNA-Trp"
     tRNA            complement(5171..5239)
                     /product="tRNA-Ala"
     tRNA            complement(5241..5313)
                     /product="tRNA-Asn"
     tRNA            complement(5345..5416)
                     /product="tRNA-Cys"
     tRNA            complement(5415..5485)
                     /product="tRNA-Tyr"
     gene            5487..7043
                     /gene="COX1"
                     /db_xref="GeneID:20834349"
     CDS             5487..7043
                     /gene="COX1"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009093550.2"
                     /db_xref="GeneID:20834349"
                     /translation="MAITRWFFSTNHKDIGTLYLVFGAWAGMVGTALSLLIRAELSQP
                     GSLLGDDQIYNVIVTAHAFVMIFFMVMPIMIGGFGNWLVPLMIGAPDMAFPRMNNMSF
                     WLLPPSFLLLLASSGIEAGAGTGWTVYPPLAGNLAHAGASVDLTIFSLHLAGVSSILG
                     AINFITTIINMKPPAISQYQTPLFVWAVLITAVLLLLSLPVLAAGITMLLTDRNLNTT
                     FFDPAGGGDPILYQHLFWFFGHPEVYILILPGFGMISHIVAYYSGKKEPFGYMGMVWA
                     MMAIGLLGFIVWAHHMFTVGMDVDTRAYFTSATMIIAIPTGVKVFSWLATLHGGSIKW
                     ETPLLWALGFIFLFTVGGLTGIVLANSSLDIMLHDTYYVVAHFHYVLSMGAVFAIMGA
                     FVHWFPLFSGYTLHSTWTKIHFGVMFVGVNLTFFPQHFLGLAGMPRRYSDYPDAYTLW
                     NTISSIGSLISLIAVIMFLFILWEAFAAKREVLSVELTSTNAEWLHGCPPPYHTFEEP
                     AFVQVQSWRE"
     tRNA            complement(7035..7105)
                     /product="tRNA-Ser"
                     /codon_recognized="UCN"
     tRNA            7111..7174
                     /product="tRNA-Asp"
     gene            7198..7888
                     /gene="COX2"
                     /db_xref="GeneID:20834350"
     CDS             7198..7888
                     /gene="COX2"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:7888,aa:TERM)
                     /transl_table=2
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009093551.2"
                     /db_xref="GeneID:20834350"
                     /translation="MAHPSQLGFQDAASPVMEELLHFHDHALMIVFLISTLVLYIIVA
                     MVSTKLTNKYILDSQEIEIVWTILPAVILILIALPSLRILYLMDEINDPHLTVKAMGH
                     QWYWSYEYTDYEDLGFDSYMIPTQDLTPGQFRLLETDHRMVVPMESPIRVLVSAEDVL
                     HSWAVPALGVKMDAVPGRLNQTAFIASRPGVFYGQCSEICGANHSFMPIVVEAVPLEH
                     FENWSSLMLEDA"
     tRNA            7889..7962
                     /product="tRNA-Lys"
     gene            7964..8131
                     /gene="ATP8"
                     /db_xref="GeneID:20834351"
     CDS             7964..8131
                     /gene="ATP8"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="YP_009093552.2"
                     /db_xref="GeneID:20834351"
                     /translation="MPQLNPAPWFAILVFSWLIFLTVIPPKVLKHTFTNEPTALSTEK
                     PKTEPWNWPWH"
     gene            8122..8805
                     /gene="ATP6"
                     /db_xref="GeneID:20834352"
     CDS             8122..8805
                     /gene="ATP6"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009093553.2"
                     /db_xref="GeneID:20834352"
                     /translation="MTLSFFDQFMSPTYLGIPLIAIALVFPWILYPSPSNRWLNNRLI
                     TLQSWFINRFTQQLLLPLNPGGHKWALILTSLMVFLLSLNLLGLLPYTFTPTTQLSLN
                     MGLAVPFWLATVIIGMRNQPTAALGHLLPEGTPVPLIPVLIIIETISLFIRPLALGVR
                     LTANLTAGHLLIQLIATAAFVLLPMMTTVAILTATVLFLLTLLEIAVAMIQAYVFVLL
                     LSLYLQENV"
     gene            8805..9589
                     /gene="COX3"
                     /db_xref="GeneID:20834353"
     CDS             8805..9589
                     /gene="COX3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:9588..9589,aa:TERM)
                     /transl_table=2
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009093554.2"
                     /db_xref="GeneID:20834353"
                     /translation="MAHQAHAYHMVDPSPWPLTGAVAALLMTSGLAIWFHFHSTTLMT
                     LGLVLLLLTMYQWWRDIIREGTFQGHHTPPVQKGLRYGMILFITSEVFFFLGFFWAFY
                     HSSLAPTPELGGCWPPTGITTLDPFEVPLLNTAVLLASGVTVTWAHHSLMEGERKQAI
                     QALTLTILLGFYFTALQAMEYYEAPFTIADGVYGSTFFVATGFHGLHVIIGSTFLAVC
                     LLRQIQYHFTSEHHFGFEAAAWYWHFVDVVWLFLYVSIYWWGS"
     tRNA            9590..9662
                     /product="tRNA-Gly"
     gene            9663..10011
                     /gene="ND3"
                     /db_xref="GeneID:20834354"
     CDS             9663..10011
                     /gene="ND3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:10011,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009093555.2"
                     /db_xref="GeneID:20834354"
                     /translation="MNLVTTILLITAALSMLLALVAFWLPQMNPDAEKLSPYECGFDP
                     LGSARLPFSLRFFLVAILFLLFDLEIALLLPLPWGNQLSTPTLTFFWAAAILILLTLG
                     LVYEWIQGGLEWAE"
     tRNA            10012..10081
                     /product="tRNA-Arg"
     gene            10082..10378
                     /gene="ND4L"
                     /db_xref="GeneID:20834355"
     CDS             10082..10378
                     /gene="ND4L"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009093556.2"
                     /db_xref="GeneID:20834355"
                     /translation="MTPVHFSFTSAFVLGLVGLAFHRTHLLSALLCLEGLMLSLFIAL
                     ALWALQLEATAFSTAPMLLLAFSACEASAGLALMVATARTHGTDRLQNLNLLQC"
     gene            10372..11752
                     /gene="ND4"
                     /db_xref="GeneID:20834356"
     CDS             10372..11752
                     /gene="ND4"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:11752,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009093557.2"
                     /db_xref="GeneID:20834356"
                     /translation="MLKVLLPTIMLFPTIWLISPKWLWTATTAQSLFIALISLNWLKW
                     NSETGWATSNLYMGTDPLSTPLLVLTCWLLPLMILASQNHINPEPLSRQRMYITLLTS
                     LQTFLIMAFGATEIIMFYIMFEATLIPTLIIITRWGNQTERLNAGTYFLFYTLAGSLP
                     LLVALLLLQQSTGTLSMLVLQYSQPLTLHTWGDKIWWAGCLIAFLVKMPLYGVHLWLP
                     KAHVEAPVAGSMVLAAILLKLGGYGMMRMMVMLDPLTKDMAYPFIILALWGIIMTGSI
                     CLRQTDLKSLIAYSSVSHMGLVAGGILIQTPWGFTGAIILMIAHGLVSSALFCLANTT
                     YERTHSRTMILARGLQMIFPLTATWWFIANLANLALPPLPNLMGELFIITAMFNWSPW
                     TLVLTGAGTLITAAYSLYLFLMSQRGPTPAHIMGLQPFHTREHLLMALHLIPVILLIT
                     KPELMWGWCY"
     tRNA            11753..11821
                     /product="tRNA-His"
     tRNA            11822..11890
                     /product="tRNA-Ser"
                     /codon_recognized="AGY"
     tRNA            11892..11964
                     /product="tRNA-Leu"
                     /codon_recognized="CUN"
     gene            11965..13803
                     /gene="ND5"
                     /db_xref="GeneID:20834357"
     CDS             11965..13803
                     /gene="ND5"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009093558.2"
                     /db_xref="GeneID:20834357"
                     /translation="MYTAPLILSSSLILTLALLLYPLIMTLDPKPQKPDWAITHVKTA
                     VSSAFFMSLLPLMIFLDQGTESIVTNWHWMNAASFDINISFKFDHYSLIFTPIALFVT
                     WSILEFASWYMHSDPHMNRFFKYLLLFLVAMIILVTANNMFQLFIGWEGVGIMSFLLI
                     GWWYGRADANTAALQAVLYNRVGDIGLILSMAWIATNLNSWEIQQIFFFSKDFDMTLP
                     LLGLILAATGKSAQFGLHPWLPSAMEGPTPVSALLHSSTMVVAGIFLLIRLHPLMENN
                     QLALTTCLCLGALTTLFTATCALTQNDIKKIVAFSTSSQLGLMMVTIGLNQPQLAFLH
                     ICTHAFFKAMLFLCSGSIIHSLNDEQDIRKMGGLHKLMPFTSSCLTIGSLALTGTPFL
                     AGFFSKDAIIEALNTSYLNAWALSLTLIATAFTAVYSFRVVFFVTMGTPRFLPLSPIN
                     ENDPAVINPIKRLAWGSIIAGLIITSNFLPSKTPVMTMPPLLKLAALTVTIIGFLMAM
                     ELASMTSKQFKTIPILPLHNFSNMLGYFPATVHRLVPKLNLTLGQSIATQLMDQTWLE
                     MAGPKGLSATQVKMATTTSNAQRGMIKTYLAMFLLTSTLAILLITL"
     gene            complement(13800..14318)
                     /gene="ND6"
                     /db_xref="GeneID:20834358"
     CDS             complement(13800..14318)
                     /gene="ND6"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009093559.2"
                     /db_xref="GeneID:20834358"
                     /translation="MNYIFSLFLLGLVVGLVAVASNPAPYFAALGLVVAAGVGCGVLV
                     GHGGSFLSLMLFLIYLGGMLVVFAYSAALAAEPFPESWGDRSVAGYVLIYLLVVGAVA
                     WWFHGGWYEGSWGVVDNKEFFIVRGDTSGVALMYSFGGGMLIVSGWVLLLTLFVVLEL
                     TRGLSRGALRAV"
     tRNA            complement(14319..14387)
                     /product="tRNA-Glu"
     gene            14393..15533
                     /gene="CYTB"
                     /db_xref="GeneID:20834359"
     CDS             14393..15533
                     /gene="CYTB"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:15533,aa:TERM)
                     /transl_table=2
                     /product="cytochrome b"
                     /protein_id="YP_009093560.2"
                     /db_xref="GeneID:20834359"
                     /translation="MASLRKTHPLLKIANNALIDLPAPSNISAWWNFGSLLLLCLIMQ
                     ILTGLFLAMHYTSDISTAFSSVAHICRDVNYGWLIRNMHANGASFFFICIYLHIGRGL
                     YYGSYLYKETWNIGVVLLLLVMMTAFVGYVLPWGQMSFWGATVITNLLSAVPYVGDAL
                     VQWIWGGFSVDNATLTRFFAFHFLLPFAIIAATALHALFLHETGSNNPTGLNSDADKI
                     SFHPYFSYKDLLGFVILIMALASLALFSPNLLGDPENFTPANPLVTPPHIKPEWYFLF
                     AYAILRSIPNKLGGVLALLFSILVLMLVPLLHTSKQQGLTFRPITQFLFWTLVADVMI
                     LTWIGGMPVEHPFIIIGQIASILYFALFLIFNPLAGWLENKSLNWS"
     tRNA            15534..15605
                     /product="tRNA-Thr"
     tRNA            complement(15604..15674)
                     /product="tRNA-Pro"
     D-loop          15675..16722
ORIGIN      
        1 gctagtgtag cttaaataaa gcataacact gaagatgtta agatgaaccc tagaaagttc
       61 cacgggcaca aaggcctggt cctgacttta ctatcagctt tagcccaatt tatacatgca
      121 agtatccgca ctcctgtgag aatgccctca atcccccgtc cggggacgag gagctggcat
      181 caggcacact tattttagcc caagacgcct tgctacgcca cacccccaag ggaactcagc
      241 agtaataaac attaagccat aagtgaaaac ttgacttagt tagggctaag agggtcggta
      301 aaactcgtgc cagccaccgc ggttatacga gagaccctag ttgatgacta cggcgtaaag
      361 agtggtttgg gaccctacaa taaataaagc caaagacctc ccaagctgtc aaacgcaccc
      421 cggaggcacg aagccctaac acgaaagtag ctttaccctc tattcccgac gccacgaaag
      481 ctaagagaca aactgggatt agatacccca ctatgcttag ctataaactc agatgcaaga
      541 catacaaata gcatccgcca gggtactaca agcgctagct taaaacccaa aggacttgac
      601 ggtgtctcag acccgcctag aggagcctgt tctagaaccg ataatccccg ttaaacctca
      661 ccatcccttg tttttcccgc ctatataccg ccgtcgcaag cttaccctgt gaagggtctg
      721 tagtaagcaa aatgggcaag ccccagaacg tcaggtcgag gtgtagctta cgagatggaa
      781 agaaatgggc tacattttct acaacagaat atcacgaacg gtaccatgaa actggtgccc
      841 gaaggtggat ttagtagtaa aaaacaaaca gagcgtgttt ttgaacccgg ctctgagacg
      901 cgcacacacc gcccgtcact ctccccatca cctaaccaca caagtaaata accaaaaata
      961 tttcacagcg gggaggcaag tcgtaacatg gtaagtgtac cggaaggtgc acttggaaca
     1021 ccaggacgtg gctgagacag ttaagcacct cccttacacc gagactacat ccatgcaagt
     1081 tggatcgtcc tgagctaaat agctagctcg accaccaaga ctaaaatcac accatccata
     1141 accttattta aacctaaaca accaaactaa accattctcc tgccttagta cgggcgacgg
     1201 aaaaggcact tactagaagc aatagaaaaa gtaccgcaag ggaaagctga aagagaggtg
     1261 aaacagccca tcaaagccta aaaaagcaga gactaaccct cgtacctttt gcatcatgat
     1321 ttagctagta tccttgagca aagagaactt tagttcaaac ccccgaaacc aagtgagcta
     1381 ccccgagaca gcctacatca gggccaaccc gtctctgtgg caaaagagtg ggaagatctc
     1441 cgggtagcgg tgacagacct accgaacttg gtgatagctg gttgcctagg aaatggatag
     1501 aagttcagcc tcgtactcct caactcatat aagtaccact acacacgaaa ctgagaaata
     1561 tacgagagtt agtcaaaggg ggtacagccc ctttgaacca ggacacaacc ttacataagg
     1621 aggttaagga tcatatttaa taagactact gctccagtgg gcctaaaagc agccacctga
     1681 atagaaagcg ttaaagctca ggcagaacaa taaatctatt attctgataa ctcatcccaa
     1741 acccctccaa atactaggcc attccatgcc aacatggaag agaccctgct aaaatgagta
     1801 ataagaagga acccccttct cccgatccac gtgtaagcta gaccggacca accactaaca
     1861 attaacgaac ccaacacaag agggcaacgt gattataaac aaacaacaag aaaacaccac
     1921 atcacaccca atcgttaacc ccacactgga ggaccaaagg gaaagactaa aagaaaaaga
     1981 aggaactcgg caaacacaag cctcgcctgt ttaccaaaaa catcgcctcc tgcataataa
     2041 ctcaacgtat aggaggtcct gcctgcccag tgacaactag ttaaacggcc gcggtatttt
     2101 gaccgtgcta aggtagcgca atcacttgtc ttttaaatga ggacctgtat gaatggcgga
     2161 acgagggctt aactgtctcc ttttcctagt caatgaaatt gatctacccg tgcagaagcg
     2221 ggtatactaa tacaagacga gaagaccctt tggagcttaa gataaaaggc caactatgtc
     2281 aaaggcccaa ataaaactaa gccaaacaaa acagccaact ggccaatatc ttcggttggg
     2341 gcgaccgcgg gggaaaacaa agcccccacg tggaacgggg aatataccct aaaaccaaga
     2401 gagacatctc taagtcacag aacatctgac caaaagatcc ggccaaccaa gccgatcaac
     2461 ggaccaagtt accctaggga taacagcgca atcccctcca agagttcata tcgacaaggg
     2521 ggtttacgac ctcgatgttg gatcaggaca tcctaatggt gcagccgcta ttaagggttc
     2581 gtttgttcaa cgattaaagt cctacgtgat ctgagttcag accggagcaa tccaggtcag
     2641 tttctatctg taaagctact tttcctagta cgaaaggacc ggaaaaatga ggcccatgct
     2701 acaagtacgc ctccctccaa cttaatgaaa ccaactaaat taaataaagg aagagcaacc
     2761 cagcccccaa gataagggtc tactaaggtg gcagagcctg gtaattgcaa aaggcctaag
     2821 ccctttccat cagaggttca aatcctctcc ttagttatgc taaccctact aatcacccac
     2881 gtaattaacc cactcgccta tattgtgcct gttctactag cagtagcctt cctaacactc
     2941 attgaacgaa aagttctagg ctacatacaa ctacgaaaag ggccaaacgt agtaggaccc
     3001 tacggactac ttcaaccaat cgcagacgga ctcaaacttt ttattaaaga acccgtacgc
     3061 ccctcaacct catccccatt tctattttta gccgccccaa tcctagcact aaccctagca
     3121 ctaatactat gggcccccat acctatcccc taccccgtaa ccgacctaaa tctaggaatc
     3181 ttgtttatcc tggccctatc aagcctagct gtttactcaa tcctaggctc aggatgagca
     3241 tcaaactcaa aatatgccct catcggagct ctacgagctg ttgcccagac aatctcctac
     3301 gaagtaagct taggcctaat cctcctctca attatcattt tcacaggagg attcacccta
     3361 caaatattta acgtaaccca agaagccact tggctcctcc tacctgcttg acccctagca
     3421 gcaatatgat atgtctcaac ccttgcagaa acaaatcgag ccccatttga cctcactgaa
     3481 ggagagtcag agctagtctc cggcttcaac gtagaatatg ctggagggcc tttcgcccta
     3541 ttctttctag ccgaatacgc taacattcta ctaataaata cactatcagt catcctattt
     3601 ctaggcgcct cgcacatccc aaccttccca gaattaacat cagtaaactt aataaccaaa
     3661 gccgcactac tatcaatcct attcctatga atccgagcct cctacccacg attccgatac
     3721 gaccagttaa tacatctagt atgaaaaaac ttcctcccct taaccctagc cctcatccta
     3781 tgacatactg cccttccaat cgcattcgcg gggcttcctc cccaactata acactataat
     3841 ggagccgtgc ctgaatgccc aaggaccact ttgatagagt gactcatgga ggttaaaatc
     3901 ctcccagctc ctagaaagaa gggactcgaa cccatgctca agagatcaaa actccaggtg
     3961 cttccactac accactttct agtaaggtca gctaaataag cttttgggcc cataccccaa
     4021 aaatgttggt taaactcctt cccttactaa tgaaccccta cgtacttacc gtgttcctat
     4081 ttagcctagg cctagggaca accctcacct tcgctagctc acactgactc ctggcatgaa
     4141 taggccttga aatcaatacc ctagccatcc ttcccctcat agcacaacac caccacccac
     4201 gagcagtaga agctaccact aaatacttcc tcacccaggc aacagcagca gctacaattc
     4261 tatttgccag caccaccaat gcttgactag ttggagaatg aagcatcacc catctctctc
     4321 accccatcgc aatcacaatg gcaataatgg gattagccct taaaattggc ctcgcacccc
     4381 tacacttctg actaccagaa gttctccaag gcctagacct aaccacagga ctaatcatat
     4441 ccacctgaca aaaactagca ccattcgcat tacttgttca aatggccccc accatcgacc
     4501 cagccctact caccgcccta ggggttctat caactcttat tggaggatga ggtggactaa
     4561 accaaactca actacgaaaa atcctagcct actcatcaat cgcacacctc ggctgaatag
     4621 tcattatcct tcaattcgca ccacaactaa ccctcctagc cctaggaacc tacatcatca
     4681 taacatcagc agccttccta acattcaaag caacaataac aacaaaaatt aacacactag
     4741 ccctcgcctg aacaaaaacc ccaacactca caacaaccgc cgccctcgtc ctactatcat
     4801 taggcggcct gcccccctta accggattta taccaaaatg actaatctta caagagctaa
     4861 caaaacaagg gctacccctc attgccaccc tcgcagccct aagtgcccta ctcagcctat
     4921 acttttacct tcgactatgc tacgcaataa ccctaacaat ttctcccaac acaaacaact
     4981 ctacaacccc atgacgattg cccaccacac aaacaaccct ccccctaaca attacaacaa
     5041 ccgcaaccct actactactc cccctcacac cagcaattgt agcactaaca cactagagac
     5101 ttaggataga acctttagac caagagcctt caaagctcta agcaggagtg aaaatctcct
     5161 agtctctgaa taaggcttgc aggactttat cccacatctt ctgaatgcaa cccagacact
     5221 ttaattaagc taaagccttt ctagatgaga aggcctcgat cctacaaact cttagttaac
     5281 agctaagcgc tcaaaccagc gagcattcat ctatctttcc ccgccctacc ggggcgaggc
     5341 ggggaaagcc ccggcaggga attagcctac acttttagac ttgcaatcta atgtgcctta
     5401 caccacggag cttctggcaa gaagaggact taaacctcta tatacggagc tacaatccgc
     5461 cgcctagacc ttcggccatc ctacctgtgg caatcacacg ctgattcttc tccaccaacc
     5521 acaaagacat tggcaccctc tacctggtat ttggtgcctg agccggaata gttggaacgg
     5581 cccttagcct cttaattcga gcggagctaa gccaacccgg atccctctta ggtgatgacc
     5641 agatctataa tgttatcgtt actgcgcacg ccttcgtaat aattttcttt atagtaatac
     5701 caattatgat tggaggcttc gggaattgat tggttcccct aatgattggt gcacccgaca
     5761 tagcattccc acgaataaat aatataagct tctgactcct acccccatcc ttccttcttc
     5821 tgctagcatc ctcaggaatc gaagccggag cagggacagg ctgaactgta tatccccctc
     5881 ttgccggtaa cctcgcacac gcgggcgcct ctgttgacct aaccatcttt tcacttcatc
     5941 ttgctggggt ttcctccatc cttggggcta ttaacttcat tacaactatt attaacatga
     6001 agcctccagc catttcacaa tatcaaacac ccctatttgt atgagcagtc ctaatcactg
     6061 ccgttcttct tcttctctcg ctaccagttc tggctgccgg aattactata cttctgacag
     6121 atcgaaacct taacaccaca ttctttgacc ccgcgggggg aggagaccca attctctacc
     6181 aacatttatt ctgattcttt gggcaccctg aagtttatat tctaatttta ccaggctttg
     6241 gaataatttc tcacatcgta gcctactact caggaaaaaa agaacctttt ggctatatgg
     6301 gaatggtttg ggctataata gcaattggcc tattaggctt catcgtatgg gcccatcaca
     6361 tatttacagt tggaatggac gtagacaccc gagcatactt tacatctgca acaataatta
     6421 tcgcaatccc aacaggggta aaagtattta gctgattagc tacacttcat gggggctcca
     6481 ttaaatgaga gacaccccta ctatgggccc tagggtttat tttcctattc acagttggag
     6541 gcctaacagg aattgtacta gccaactcat cccttgacat catactccac gacacatatt
     6601 acgtagttgc ccacttccac tacgtcttat ccatgggagc tgtgtttgca atcataggag
     6661 cttttgtcca ttgattcccc ctattctcag gctacacact acacagcacc tgaacaaaaa
     6721 tccattttgg agttatgttt gtaggtgtta acctcacctt cttcccacaa cacttcctag
     6781 gcctagccgg catgccacgg cgatactcag actacccaga cgcctacacc ctttgaaata
     6841 caatctcctc tatcggctcc ctaatttctc taatcgcagt aattatgttc ctatttatcc
     6901 tgtgagaagc cttcgctgcc aaacgagaag tcttatctgt tgaactaacc tctacaaacg
     6961 cagaatgact ccacgggtgt cctcccccct accacacatt cgaagagcca gcatttgttc
     7021 aagttcaatc atgacgagaa aggaaggaat cgaacccccg tgaactagtt tcaagccagt
     7081 tgcataacca ctctgccact ttctttctat aagatactag taaaactagc tattacattg
     7141 ccttgtcaag gcaaaattgc aggttaaacc cctgcgtatc ttaagcccta cggcttaatg
     7201 gcacatccct cacaattagg attccaagac gcggcctcac ctgtaataga agaacttctt
     7261 cacttccacg accatgcact aatgattgtt ttcctaatca gcaccttggt cttatacatc
     7321 attgttgcaa tggtttccac taaattaacc aacaaataca ttttagactc ccaagaaatt
     7381 gagattgttt gaaccatttt accagcagta atccttatcc tcattgccct cccctccctt
     7441 cgaatcctct acctaataga tgaaattaac gaccctcacc taaccgtaaa agccatagga
     7501 catcaatgat actgaagcta cgagtataca gactatgaag accttggctt cgactcttac
     7561 atgatcccga cccaagacct aaccccagga cagtttcgac tccttgaaac agaccatcgc
     7621 atagtagtcc caatagaatc accgattcga gttctagtct cagctgaaga cgtcctccac
     7681 tcctgggccg ttccagccct aggtgtaaaa atagacgccg tccccggacg cctaaaccaa
     7741 acagccttta ttgcctcccg ccctggagta ttctatggac agtgctctga aatctgtggt
     7801 gccaaccaca gttttatacc aattgtagta gaagccgtcc cactagaaca tttcgaaaac
     7861 tgatcctccc tcatgcttga agacgcctca ctaagaagct aaatagggcc tagcgttagc
     7921 cttttaagct aaagactggt gactcccaac cacccttagt gacatgcccc aattaaaccc
     7981 cgccccatgg ttcgccattc tagtattttc ctgactaatc tttctaacag taatcccacc
     8041 taaagtcctt aagcacactt ttactaacga acccacagca ctcagcaccg aaaaacctaa
     8101 aactgaaccc tgaaattgac catgacacta agcttcttcg accaatttat gagccccaca
     8161 tacctaggca tccctttaat tgccatcgca cttgtattcc cctgaatcct ttacccttca
     8221 ccatcaaacc gatgactaaa caaccgtctc attaccctcc agagctgatt tattaatcgc
     8281 ttcacacaac aactacttct tcccctcaac cccggcggac acaaatgagc cctaatcctg
     8341 acatcattaa tagttttcct cttatcacta aatctactgg gtctccttcc atatactttt
     8401 accccaacca cacaactttc attaaacata ggactagcag tcccattctg actggccacc
     8461 gtcattatcg gaatacgaaa ccagcctacc gcggccctag gacacctcct gccagaagga
     8521 acccccgtac ctctgatccc tgtgcttatt atcatcgaaa caattagctt atttattcga
     8581 cccctggcct taggcgtacg actaacagcc aaccttacag ctggccactt gctcattcaa
     8641 cttattgcaa ctgcagcctt tgtattacta cctatgataa ccaccgtagc aatcctaacc
     8701 gccacagtcc tcttcctcct tacccttctt gaaatcgcag tggccataat ccaagcctac
     8761 gtattcgtac ttctactaag cctctaccta caagaaaacg tctaatggcc caccaagcac
     8821 acgcatacca tatggttgac ccaagtccct ggcccctaac aggcgcagta gctgctctcc
     8881 tcatgacatc aggcctcgca atctggtttc actttcactc aacaacctta ataacactag
     8941 gactagttct tcttttactt acaatgtatc aatgatggcg agacattatc cgagaaggaa
     9001 ctttccaagg acatcatacg cccccagtcc aaaaaggcct acgatacgga ataattctat
     9061 ttattacatc agaagttttc ttcttcctag gcttcttctg agccttctac cactcaagcc
     9121 ttgcccccac cccagaacta ggaggatgct ggcctcccac aggaattacc acccttgacc
     9181 catttgaagt ccccctacta aataccgccg tcctcctagc atcaggcgtc accgtgacct
     9241 gagctcatca cagcctaatg gagggagaac gtaaacaagc catccaagca cttaccctaa
     9301 ccattctcct aggcttctac ttcaccgccc tccaagctat agaatactac gaagcccctt
     9361 ttaccatcgc cgacggcgta tacggctcaa cattctttgt agctacaggc ttccatggcc
     9421 tacatgtcat tattggctca accttcctag ccgtctgcct tctacgacaa atccaatacc
     9481 acttcacttc cgaacaccac tttggatttg aagcggctgc ctgatactga cactttgtag
     9541 acgttgtctg attattccta tacgtctcca tctactgatg aggttcataa tctttctagt
     9601 atttaaagtt agtacaagtg acttccaatc atttagtctt ggttaaaatc caaggaaaga
     9661 taatgaattt agttacaaca atcttactta tcacagcagc cctttcgata ctactagctc
     9721 tagtggcatt ctgactccca caaataaatc cagacgcaga aaaactctca ccctatgaat
     9781 gcgggtttga tcctcttggt tccgcacgtc taccattttc cttacgattc ttcctcgtcg
     9841 ccatcctctt cctcttattt gacctagaaa ttgccctact tcttccactg ccctgaggta
     9901 atcaattatc cacgcccaca cttaccttct tttgagccgc agccatccta attttactca
     9961 ccttaggact agtctacgaa tgaattcaag gaggcctcga atgagcagaa tagggaatta
    10021 gtccaaaata agacctctga tttcggctca gaaaattgtg gtttaagtcc acgactccct
    10081 tatgacaccc gtacatttca gttttacctc agcattcgta ctaggacttg taggcctagc
    10141 cttccaccga acccacctcc tgtcggccct attatgccta gaagggctga tattatcatt
    10201 atttattgct ctggccctat gagcacttca actcgaagca acggccttct ccaccgcccc
    10261 tatactcctc ctagccttct cagcctgcga agccagcgca ggcttagcct taatagtcgc
    10321 cacagcgcga acacacggga cagaccgact ccaaaacctc aacctactac aatgctaaaa
    10381 gtcctactac caacaatcat gctattccca acaatctggc taatttcccc caaatggtta
    10441 tgaactgcta caacagccca aagtcttttt attgccctaa ttagcctaaa ttgacttaag
    10501 tgaaactcag aaacaggctg agcaacctcc aatctctata taggaacaga ccccttatca
    10561 acccccctgc tagtccttac ctgctgacta ctaccactaa taattcttgc cagccaaaac
    10621 catattaacc ccgaaccgct cagccgccaa cgaatgtaca ttactctttt aacctctcta
    10681 caaacctttc ttatcatagc atttggagca accgaaatca tcatgttcta catcatattt
    10741 gaggccaccc tcatcccaac cctaattatc atcactcggt gaggcaacca aactgaacgc
    10801 cttaatgcag gaacatactt cctattttat acactcgcag gatctcttcc cctattagtt
    10861 gccctcctcc tccttcaaca aagcacaggc acactttcaa tgttagtact ccaatattcc
    10921 caacctctaa ccctacacac atgaggcgat aaaatctgat gagcaggctg cctaatcgcc
    10981 ttcctggtaa aaatgcccct atatggcgtc cacctgtgac taccaaaagc acatgtagaa
    11041 gcccccgtag ccggatcaat agtcctagcc gcaatcctac taaaacttgg aggatatggt
    11101 ataatacgaa tgatagtaat actcgaccca ctcactaaag acatagccta cccttttatt
    11161 atcctagctc tatgaggcat tatcataacc ggctccatct gtttacgaca gacagaccta
    11221 aaatccttaa tcgcctactc ctccgtcagc cacataggac tagtagcagg aggaatccta
    11281 attcaaaccc cctgaggatt caccggagca atcatcctaa taattgccca cggtctggta
    11341 tcgtcagctc tcttctgtct agccaacacc acatacgaac gaacccacag tcgaacaata
    11401 attcttgctc gcggcctgca aataatcttc cccttaacag ccacatgatg attcatcgcc
    11461 aacctggcca acctagccct accacctcta ccaaatctaa taggagaact cttcatcatc
    11521 acagctatat ttaattgatc tccatgaaca ctggttttaa ccggagcagg caccctaatc
    11581 acagctgctt attcgctcta cctcttcctg atgtcccaac gaggcccaac ccctgcccat
    11641 attataggac ttcaaccgtt ccacacccga gaacatctac ttatagccct acatctaatc
    11701 cccgtcattc tcctaattac aaaaccagag ctcatatgag gatgatgcta ctgtagatat
    11761 agtttaagca aaacactaga ttgtggttct aaaaatagag gttaaagtcc tcttatccac
    11821 cgagagaggc cctgaggcaa taggaactgc taatttctac cgcccgtggt taaaatccac
    11881 ggctcactcg agcttctaaa ggataacagc tcatccattg gtcttaggaa ccaaaaactc
    11941 ttggtgcaaa tccaagtagc agctatgtac actgcacccc ttattctttc atcctccctc
    12001 atcctaaccc ttgccctcct actctacccc ctaatcataa ccctagaccc aaagccccaa
    12061 aaaccagatt gagccatcac ccacgtcaaa acagccgtaa gcagcgcatt ctttataagc
    12121 ctcctcccat taataatctt ccttgaccaa ggaacagaga gcatcgtaac caattgacac
    12181 tgaataaatg ctgcaagctt cgacatcaac atcagcttta agtttgacca ctactcctta
    12241 atcttcacac caatcgcctt attcgttacc tgatcaatcc tagagtttgc atcatgatac
    12301 atacactcag atccccacat aaaccgcttc ttcaaatacc ttctactctt ccttgtagcc
    12361 ataattattc tagtgaccgc caacaatata tttcaactat tcattggttg agaaggagtt
    12421 ggcatcatat ctttcctttt aatcggctga tggtatggcc gagctgatgc taacactgcc
    12481 gccctacaag ccgtcctata caaccgagtg ggagacatcg gactaatctt gagcatagca
    12541 tgaattgcaa caaacctaaa ctcatgagaa atccaacaaa tcttcttctt ctccaaagat
    12601 tttgatataa ccctacccct tctaggccta atccttgccg ccactggaaa atccgcccaa
    12661 tttggcctcc acccgtgact tccctcagcc atagaaggcc caacaccagt ctctgcctta
    12721 ctacactcaa gcacaatagt ggttgcagga atcttcctac taattcgact tcaccccctc
    12781 atagaaaaca atcagcttgc gctgaccacc tgcctatgct taggagcctt aaccacctta
    12841 ttcacagcta cctgcgcttt aacccaaaac gatatcaaaa aaatcgtagc tttctccaca
    12901 tcaagccaac taggcctcat aatagttacc attggactaa accaacccca actagcattt
    12961 ctacacatct gtacgcacgc ctttttcaaa gctatactct tcctatgctc cggatcaatt
    13021 atccacagcc tcaatgacga acaagacatt cgaaaaatag gaggcctgca caaactaatg
    13081 ccatttacct cgtcttgcct aaccatcggc agcctagcac taacaggcac accattccta
    13141 gccgggtttt tctcaaaaga cgccattatt gaagccctaa acacctccta ccttaatgcc
    13201 tgagccctgt cccttacact tatcgctacc gccttcactg cggtctacag cttccgagta
    13261 gtcttcttcg taaccatagg aaccccacga ttcctacccc tctccccaat taacgaaaac
    13321 gaccccgccg ttattaaccc aatcaaacga ctagcctgag gcagcatcat tgctggacta
    13381 attattacat ctaattttct tccatcaaaa acccccgtca taacaatacc cccgctcctc
    13441 aaactagccg ccttaacagt aacaatcatt ggcttcctca tagcgataga actagcatca
    13501 ataacatcaa aacaattcaa aactatcccc atcctaccac tacacaactt ctcaaacata
    13561 ctaggctact tccctgcaac cgtgcatcga ctagtaccta aactaaatct tacactagga
    13621 caatcaattg ccacccaact catagaccaa acatgacttg aaatagcagg ccctaaagga
    13681 ctctccgcaa cacaagtcaa aatagccaca accacaagca acgcccaacg aggaataatc
    13741 aaaacctacc tagccatatt cctcttaacc agcaccctag ccatcctcct aatcaccctt
    13801 taaaccgctc gaagcgcccc acgacttaaa ccgcgagtta actcaagtac aacaaataaa
    13861 gttaacaaca acactcaacc agacacaatc aacattcccc ctccaaaaga atatattaat
    13921 gcaaccccac tagtatctcc ccgcacaata aaaaactcct tattatccac aaccccccat
    13981 gacccctcat atcaaccccc atggaaccat cacgccaccg cccccaccac taacaaataa
    14041 atcaacacat acccagcaac cgaacgatcc ccccagctct ccggaaaagg ctctgcagcc
    14101 aaagccgccg aataagcaaa tactaccaac atccctccca aataaatcaa aaacaacatt
    14161 aaagacaaaa aagacccccc atgtcccacc aacacaccac aaccaacccc cgctgccaca
    14221 accaacccta aagcagcaaa atatggtgca ggattagaag ccacagccac taaaccaaca
    14281 actaagccca ataaaaacaa agaaaaaata taattcataa ttcccacccg gattttaacc
    14341 gagaccagcg acttgaaaaa ccaccgttgt aattcaacta tgagaacccc taatggccag
    14401 cctacgaaaa acacacccac tactaaaaat cgccaacaat gcactaattg acctgcccgc
    14461 cccatccaac atttccgcat gatgaaactt cggctctctc cttcttttat gcctaattat
    14521 acaaattcta acaggactct tcctggctat acactacacc tcagatatct ctacagcctt
    14581 ctcctcagta gcccacatct gccgtgacgt caactacggc tgactcatcc gaaacatgca
    14641 cgccaacgga gcctcatttt tcttcatctg catctaccta cacattggtc gaggcttata
    14701 ctacggctcc tacctgtata aagaaacatg aaacattgga gtagtgctgc tcctactagt
    14761 tataatgacc gcatttgtgg gatatgtact cccatgagga caaatatcat tttgaggagc
    14821 aaccgtaatt acaaacctcc tctccgcagt gccatacgta ggagatgctt tagtacaatg
    14881 aatctgaggc gggttctccg tagacaacgc aaccctgaca cgattcttcg ccttccattt
    14941 ccttctccct ttcgcaatca ttgcagccac agccttacac gccctatttc ttcacgaaac
    15001 aggctccaac aaccccacag gcctaaactc agacgcagac aaaatctcat tccacccata
    15061 cttctcctac aaagacttac taggctttgt aatccttatt atagccctag catcgctagc
    15121 cctattttcc cccaacctct tgggggaccc cgaaaacttt acccccgcca accccctagt
    15181 cacaccccca cacatcaaac cagagtgata cttcctattt gcctacgcca ttctacgatc
    15241 aatccccaac aaactaggag gtgtcctcgc cttactattc tctatcctcg tccttatact
    15301 agtcccacta ctacacacct ccaaacaaca aggactcaca ttccgtccaa tcacacaatt
    15361 cctattctgg accctggtag cagacgtaat aatcctaaca tgaattggag gaataccagt
    15421 agaacaccca tttattatca tcggccaaat cgcctccatc ctttacttcg cgctattcct
    15481 aatttttaat cctctggccg gttgattaga aaacaaatca cttaactgat cctgtactag
    15541 tagcttagtt taaaagcatc ggtcttgtaa tccgaagatc ggaggttaaa tccctcccta
    15601 gtgccagaaa agagagattt taactcccac cactaactcc caaagctagt attctaaaat
    15661 taaactattt tctgacctaa tgcccagtac ctaataatcc caggtgcata tatatatata
    15721 tattacataa tgatcctagt acattatatg tatatagtac attatggtat agtacatatt
    15781 atgtataatc ttacattaat ggtgtagtac attacattaa atattccaca tacatatata
    15841 tatatacatt aatactagta cattactatt aaggtgtaca taaaccataa atttactttc
    15901 acaagattaa ttaatcagta ctgataactt gaataatccc cataacttca tcacaaattt
    15961 ttccttgcaa ggactcaact aatattggac ctcacattat taatgtagta agagaccacc
    16021 aaccagtcta taacttaatg catattatcc ttgatgggtc agggacaaat atcgtgaggg
    16081 tcgcacaata tgaactatta ctggcatctg gttcctattt cagggccata tctagtaaac
    16141 cttccccccc cggataacta tatctggcat ttgattattg gtgttagtat cgattgtcca
    16201 tgacccacca tgccaaggcg ttcacttaaa tgcatagggt tctcttattt ttaggtctct
    16261 tccacttgac atttggtcac tttcagagta atggttaaca aggtggtaca tttccttgaa
    16321 attaaaagat aatacctgca gagcttcata aacatcctat aaagtaacca catacgtctc
    16381 tatcaggtgc ataaactatt gatactatcc ccattcctat ctaaggtgtc ccccctgccg
    16441 cctgcacttg gattttgcgc gacaaacccc cctacccccc tacgccggac agttcatata
    16501 ttaatcctgt caaaccccaa aaccaggaat gaccgaccag cgtaatcaac gagtggtttg
    16561 tgttgatata tatatagtgt tacataacta acacattgca tgcttaatac acagccatgt
    16621 ctgggtcaac tacacccaaa attacccacg caatcgtatt atttatgtat aaatacacca
    16681 caatttttat aacatgtctg tatcacatat tatatagttg ta
//

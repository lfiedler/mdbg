LOCUS       NC_030039              14598 bp    DNA     circular INV 31-MAY-2016
DEFINITION  Phyllidia ocellata voucher 394 mitochondrion, complete genome.
ACCESSION   NC_030039
VERSION     NC_030039.1
DBLINK      BioProject: PRJNA320591
KEYWORDS    RefSeq.
SOURCE      mitochondrion Phyllidia ocellata
  ORGANISM  Phyllidia ocellata
            Eukaryota; Metazoa; Lophotrochozoa; Mollusca; Gastropoda;
            Heterobranchia; Euthyneura; Nudipleura; Nudibranchia; Doridina;
            Eudoridoidea; Phyllidiidae; Phyllidia.
REFERENCE   1  (bases 1 to 14598)
  AUTHORS   Xiang,P., Lin,M., Wang,Y., Shen,K.-N. and Hsiao,C.-D.
  TITLE     The complete mitogenome of Sea Slug, Phyllidia ocellata (Mollusca:
            Phyllidiidae)
  JOURNAL   Mitochondrial DNA Part B Resour (2016) In press
REFERENCE   2  (bases 1 to 14598)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (04-MAY-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 14598)
  AUTHORS   Xiang,P. and Hsiao,C.-D.
  TITLE     Direct Submission
  JOURNAL   Submitted (21-DEC-2015) Department of Bioscience Technology, Chung
            Yuan Christian University, 200 Chung-Pei Rd., Chung-Li, Taiwan
            32023, Taiwan
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KU351090.
            
            ##Assembly-Data-START##
            Assembly Method       :: Geneious v. R9
            Assembly Name         :: Phyllidia ocellata
            Coverage              :: 46 X
            Sequencing Technology :: Illumina
            ##Assembly-Data-END##
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..14598
                     /organism="Phyllidia ocellata"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /specimen_voucher="394"
                     /db_xref="taxon:190925"
                     /tissue_type="muscle"
                     /collected_by="Chung-Der Hsiao"
     gene            1..1530
                     /gene="COX1"
                     /locus_tag="A7F35_gp01"
                     /db_xref="GeneID:27438281"
     CDS             1..1530
                     /gene="COX1"
                     /locus_tag="A7F35_gp01"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009250779.1"
                     /db_xref="GeneID:27438281"
                     /translation="MRWLFSTNHKDIGTLYIVFGMWCGLLGTGLSLLIRFELGTAGPF
                     LGDDHFYNVIVTAHAFVMIFFLVMPLMIGGFGNWMVPLLIGAPDMSFPRMNNMSFWLL
                     PPSFILLLCSTLMEGGAGTGWTVYPPLSGPMGHGGTSVDLVIFSLHLAGASSLLGAIN
                     FITTIFNMRSPAMTMERLSLFVWSVLVTAFLLLLSLPVLAGAITMLLTDRNFNTSFFD
                     PAGGGDPILYQHLFWFFGHPEVYILILPGFGMISHILSNFTSKPAFGTLGMIYALISI
                     GILGFIVWAHHMFTVGMDVDTRAYFTAATMIIAVPTGIKVFSWMMTLYGSRGPFSAAM
                     YWVLGFIFLFTLGGLTGIVLSNSSLDIVLHDTYYVVAHFHYVLSMGAVFAIFGGFVYW
                     FPLMTGLTLHERWAKAHFLVMFCAVNLTFFPQHFLGLAGMPRRYSDYPDAYYKWNQIS
                     SFGSLFSIFAVLMFVFVLWESLVSQRGVMFTKAPAISREWEEILPLDFHSNTESSVTV
                     A"
     gene            1548..1616
                     /locus_tag="A7F35_gt01"
                     /db_xref="GeneID:27438271"
     tRNA            1548..1616
                     /locus_tag="A7F35_gt01"
                     /product="tRNA-Val"
                     /db_xref="GeneID:27438271"
     gene            1621..2747
                     /locus_tag="A7F35_gr02"
                     /db_xref="GeneID:27438282"
     rRNA            1621..2747
                     /locus_tag="A7F35_gr02"
                     /product="16S ribosomal RNA"
                     /db_xref="GeneID:27438282"
     gene            2756..2822
                     /locus_tag="A7F35_gt02"
                     /db_xref="GeneID:27438283"
     tRNA            2756..2822
                     /locus_tag="A7F35_gt02"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:27438283"
     gene            2819..2889
                     /locus_tag="A7F35_gt03"
                     /db_xref="GeneID:27438284"
     tRNA            2819..2889
                     /locus_tag="A7F35_gt03"
                     /product="tRNA-Ala"
                     /db_xref="GeneID:27438284"
     gene            2897..2962
                     /locus_tag="A7F35_gt04"
                     /db_xref="GeneID:27438285"
     tRNA            2897..2962
                     /locus_tag="A7F35_gt04"
                     /product="tRNA-Pro"
                     /db_xref="GeneID:27438285"
     gene            2966..3436
                     /gene="ND6"
                     /locus_tag="A7F35_gp13"
                     /db_xref="GeneID:27438286"
     CDS             2966..3436
                     /gene="ND6"
                     /locus_tag="A7F35_gp13"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009250780.1"
                     /db_xref="GeneID:27438286"
                     /translation="MFLYWFCSSLIFCFPLFKNPISMAAMLVCISLVMVIMISMLSSF
                     WFSYVLFLVYVGGLLVLFIYICLVSSNYPFKFNLVSLLCIMLGSCLISLKSESSLPLG
                     ILGFSTWVSGENLMNDKNLSLFLFLAILLLSMLLVVVRICQPGGFAVNMNNEKN"
     gene            3474..5102
                     /gene="ND5"
                     /locus_tag="A7F35_gp12"
                     /db_xref="GeneID:27438280"
     CDS             3474..5102
                     /gene="ND5"
                     /locus_tag="A7F35_gp12"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009250781.1"
                     /db_xref="GeneID:27438280"
                     /translation="MTMLCLSAFFFSLNKTIVLEINLFSLSSMDFSLMIIFDKISVGF
                     SMIVTLISGSVFLFAQKYMEEDPFCGRFIWILLSFVISMNLLIFSGSIFFLLLGWDGL
                     GITSFALIIYYESKESQLAGFQTLLVNRIGDVIIVLSVFLFLSEGQLTLISMSNSMFY
                     SSGLILMLCIAALTKSAQFPFSSWLPAAMAAPTPVSALVHSSTLVTAGIFIIIRLSMN
                     LDLDEMICGILMMCGSITCLLGGWAATYENDVKKIIALSTLSQLGVMVFSLGLNLPAL
                     ALFHLYTHALFKALLFLAAGHILMVTFGSQDIRMIGGVGMLMPFTCVMFNISSLCLVG
                     APFMSAFYSKHMILEKMLMSPINFISVLVMLVATLMTAKYVSRTLKAICWNKTGTFLL
                     SSYSGIYTFLPVLVLALGAIFGGKVILSMDISNFEFSFLPSSSSMLINMITVLGVYMG
                     LIENNLMKKSFFLSTLFFLTPLIYGSTKPLSLALSKMKILDQGWIEPYFLIKNKIYWI
                     GAKVSSSGIWPDTRILISASFIIFCLLNGTLFNN"
     gene            5080..5994
                     /gene="ND1"
                     /locus_tag="A7F35_gp11"
                     /db_xref="GeneID:27438279"
     CDS             5080..5994
                     /gene="ND1"
                     /locus_tag="A7F35_gp11"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009250782.1"
                     /db_xref="GeneID:27438279"
                     /translation="MVLYLTISILTCLCILLAVAFFTLLERKVLGYVQLRKGPNKVSL
                     WGILQPIADAVKLFVKEKVMPYGSNQYMFLFAPCLSLVLGLSVWYLYPSQYGNKFVSW
                     GLLLFFCITSLNVYGTMLAGWASNSKYAFLGALRAAAQTISYEVSMLLLLLFSAFLLL
                     TCSWDEAFKFGFPVMFLLIPMLMVWFTSTVAETNRAPFDFAEGESELVSGFNIEFGGG
                     LFAMLFLAEYASILFMSMATSVWFFSQFNFSTVLFPLQITIISIMFLLVRGAYPRFRY
                     DLLMMLCWKSFLPFSLCALSLTFLSINL"
     gene            6002..6063
                     /locus_tag="A7F35_gt05"
                     /db_xref="GeneID:27438274"
     tRNA            6002..6063
                     /locus_tag="A7F35_gt05"
                     /product="tRNA-Tyr"
                     /db_xref="GeneID:27438274"
     gene            6066..6135
                     /locus_tag="A7F35_gt06"
                     /db_xref="GeneID:27438287"
     tRNA            6066..6135
                     /locus_tag="A7F35_gt06"
                     /product="tRNA-Trp"
                     /db_xref="GeneID:27438287"
     gene            6160..6429
                     /gene="ND4L"
                     /locus_tag="A7F35_gp10"
                     /db_xref="GeneID:27438288"
     CDS             6160..6429
                     /gene="ND4L"
                     /locus_tag="A7F35_gp10"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009250783.1"
                     /db_xref="GeneID:27438288"
                     /translation="MGMVTSFFAFTKLNFHILILLISLEALMLSLLVFLFSFCLTYKT
                     NYSMFLVLLTFAACEAALGLSLLVSILRLRGNDFVTSFNSMKFYA"
     gene            6431..7549
                     /gene="CYTB"
                     /locus_tag="A7F35_gp09"
                     /db_xref="GeneID:27438278"
     CDS             6431..7549
                     /gene="CYTB"
                     /locus_tag="A7F35_gp09"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome b"
                     /protein_id="YP_009250784.1"
                     /db_xref="GeneID:27438278"
                     /translation="MRQANPAEQFLSLPSPMSFSIWWNGGSILGLLLGLQILTGLFLS
                     MHYTADMTNTFASVIHIMRDVPGGWMFRNFHANGASLFFVFLYLHIGRGLYYQSYISQ
                     PHTWMVGVTIFLVSMGTAFLGYVLPWGQMSFWGATVITNLVSAIPYLGTYIVEWVWGG
                     FSVGQSTLNRFYSLHFILPFLIGGLSALHILFLHEKGSTNPLGETHHISKIPFHPYFT
                     WKDIVGFMVLISMLTVLGIFFPTILGDPENFNPASAMVTPVHIQPEWYFLFAYAILRS
                     IPNKLGGVIALAASVLILYFLPMSSNYGKMVPSSFTPTYQIIFWNLVVFFVILTWLGA
                     CPIEEPYATLAIPISILYFLSFILLVSMPAIWKKFLAF"
     gene            7545..7617
                     /locus_tag="A7F35_gt07"
                     /db_xref="GeneID:27438270"
     tRNA            7545..7617
                     /locus_tag="A7F35_gt07"
                     /product="tRNA-Asp"
                     /db_xref="GeneID:27438270"
     gene            7618..7684
                     /locus_tag="A7F35_gt08"
                     /db_xref="GeneID:27438289"
     tRNA            7618..7684
                     /locus_tag="A7F35_gt08"
                     /product="tRNA-Phe"
                     /db_xref="GeneID:27438289"
     gene            7686..8363
                     /gene="COX2"
                     /locus_tag="A7F35_gp08"
                     /db_xref="GeneID:27438290"
     CDS             7686..8363
                     /gene="COX2"
                     /locus_tag="A7F35_gp08"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009250785.1"
                     /db_xref="GeneID:27438290"
                     /translation="MSFWGQLNLMDASSPLQSEMFLFHDHAMILLIGIFSFCGLLGFK
                     LILNKFTSRTVLEAQRLETVWTIVPAMLLIWLALPSLRLLYLLDEQSSDNGMILKATG
                     HQWYWSYEVPLSNNGSFDSYMIPENDLSEGDYRLLEVDNRAVVPFGVETTVIVTSADV
                     LHAWTLPSMGVKMDAVPGRLNSMSMFVEKPGVYYGQCSEICGANHSFMPIVLEALNLE
                     DFCQLQF"
     gene            8367..8430
                     /locus_tag="A7F35_gt09"
                     /db_xref="GeneID:27438272"
     tRNA            8367..8430
                     /locus_tag="A7F35_gt09"
                     /product="tRNA-Gly"
                     /db_xref="GeneID:27438272"
     gene            8436..8500
                     /locus_tag="A7F35_gt10"
                     /db_xref="GeneID:27438291"
     tRNA            8436..8500
                     /locus_tag="A7F35_gt10"
                     /product="tRNA-His"
                     /db_xref="GeneID:27438291"
     gene            8517..8584
                     /locus_tag="A7F35_gt11"
                     /db_xref="GeneID:27438292"
     tRNA            8517..8584
                     /locus_tag="A7F35_gt11"
                     /product="tRNA-Cys"
                     /db_xref="GeneID:27438292"
     gene            complement(8746..8806)
                     /locus_tag="A7F35_gt12"
                     /db_xref="GeneID:27438293"
     tRNA            complement(8746..8806)
                     /locus_tag="A7F35_gt12"
                     /product="tRNA-Gln"
                     /db_xref="GeneID:27438293"
     gene            complement(8814..8879)
                     /locus_tag="A7F35_gt13"
                     /db_xref="GeneID:27438294"
     tRNA            complement(8814..8879)
                     /locus_tag="A7F35_gt13"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:27438294"
     gene            complement(8875..9036)
                     /gene="ATP8"
                     /locus_tag="A7F35_gp07"
                     /db_xref="GeneID:27438295"
     CDS             complement(8875..9036)
                     /gene="ATP8"
                     /locus_tag="A7F35_gp07"
                     /codon_start=1
                     /transl_table=5
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="YP_009250786.1"
                     /db_xref="GeneID:27438295"
                     /translation="MPQLSPMLGFFMFTVILFSYFFLLSSLSKKAPFISSTKVKKTKK
                     VSLPYFSNL"
     gene            complement(9040..9109)
                     /locus_tag="A7F35_gt14"
                     /db_xref="GeneID:27438269"
     tRNA            complement(9040..9109)
                     /locus_tag="A7F35_gt14"
                     /product="tRNA-Asn"
                     /db_xref="GeneID:27438269"
     gene            complement(9123..9599)
                     /gene="ATP6"
                     /locus_tag="A7F35_gp06"
                     /db_xref="GeneID:27438296"
     CDS             complement(9123..9599)
                     /gene="ATP6"
                     /locus_tag="A7F35_gp06"
                     /codon_start=1
                     /transl_table=5
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009250787.1"
                     /db_xref="GeneID:27438296"
                     /translation="MLFTLMTILLVLNFIGLIPFVYGPTSNIWISASLAVVFWSMLIF
                     SGWVKFPMESAAHFAPSGAPNLFIPFLVIIETVSILIRPLTLTIRIIANISAGHIIMG
                     LIATSLTACSFLALGPVITVHIGYNMFEIFVCSVQAYVFSLLIKLYGEEHPVLVSS"
     gene            complement(9798..9863)
                     /locus_tag="A7F35_gt15"
                     /db_xref="GeneID:27438268"
     tRNA            complement(9798..9863)
                     /locus_tag="A7F35_gt15"
                     /product="tRNA-Arg"
                     /db_xref="GeneID:27438268"
     gene            complement(9878..9944)
                     /locus_tag="A7F35_gt16"
                     /db_xref="GeneID:27438297"
     tRNA            complement(9878..9944)
                     /locus_tag="A7F35_gt16"
                     /product="tRNA-Glu"
                     /db_xref="GeneID:27438297"
     gene            complement(9937..10696)
                     /locus_tag="A7F35_gr01"
                     /db_xref="GeneID:27438298"
     rRNA            complement(9937..10696)
                     /locus_tag="A7F35_gr01"
                     /product="12S ribosomal RNA"
                     /db_xref="GeneID:27438298"
     gene            complement(10695..10758)
                     /locus_tag="A7F35_gt17"
                     /db_xref="GeneID:27438299"
     tRNA            complement(10695..10758)
                     /locus_tag="A7F35_gt17"
                     /product="tRNA-Met"
                     /db_xref="GeneID:27438299"
     gene            complement(10757..11095)
                     /gene="ND3"
                     /locus_tag="A7F35_gp05"
                     /db_xref="GeneID:27438300"
     CDS             complement(10757..11095)
                     /gene="ND3"
                     /locus_tag="A7F35_gp05"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009250788.1"
                     /db_xref="GeneID:27438300"
                     /translation="MNFVIILSIALVVVYLMTNYISMIDSSEKLTSFECGFDPLSKMR
                     SPFSTRFFLLVVLFLIFDVEIALLFPVLSMLATNYSFLMMTSLFMFLLILILGMFHEW
                     NEGALDWFTN"
     gene            complement(11138..11199)
                     /locus_tag="A7F35_gt18"
                     /db_xref="GeneID:27438276"
     tRNA            complement(11138..11199)
                     /locus_tag="A7F35_gt18"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:27438276"
     gene            11200..11256
                     /locus_tag="A7F35_gt19"
                     /db_xref="GeneID:27438301"
     tRNA            11200..11256
                     /locus_tag="A7F35_gt19"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:27438301"
     gene            11353..12636
                     /gene="ND4"
                     /locus_tag="A7F35_gp04"
                     /db_xref="GeneID:27438302"
     CDS             11353..12636
                     /gene="ND4"
                     /locus_tag="A7F35_gp04"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009250789.1"
                     /db_xref="GeneID:27438302"
                     /translation="MYMNMNFTLLIDTIFLGSTFSSVLIFLTCFLCLLSLLCTPEEKN
                     SWTYMLCLASLLLVLILAFSSSNLMMFYIFFEASLIPTLMLIVGWGYQPERLQAGTYM
                     MLYTVGASLPLLVILIWHCSSMASTEVYMLHLSSPLFKGVCGLVLFMAFLVKLPMYGV
                     HLWLPKAHVEAPLAGSMILAGILLKLGGYGMMQMIYSFNLVLDNYTMLMICLSMWGGF
                     LATMMCIQQVDVKSLVAYSSVGHMSIVSAGLLMDTSWGVMSAMVTMVAHGFSSSAMFC
                     LAYFSYKKSHTRNIPYMKGMLQVYPILSMFWFMFCCINMACPPTLNLMGEMMIVGPLW
                     KYSFLLVMCMGLMIFFSAAYNMYLYSCVNHGTFSSYVLPGYPLKTYSMIGLVSHMLPL
                     VLIFKSSLFLSCNFLVFWNYSLQVVLSKTCRFSHM"
     gene            complement(12636..12698)
                     /locus_tag="A7F35_gt20"
                     /db_xref="GeneID:27438277"
     tRNA            complement(12636..12698)
                     /locus_tag="A7F35_gt20"
                     /product="tRNA-Thr"
                     /db_xref="GeneID:27438277"
     gene            complement(12694..13473)
                     /gene="COX3"
                     /locus_tag="A7F35_gp03"
                     /db_xref="GeneID:27438303"
     CDS             complement(12694..13473)
                     /gene="COX3"
                     /locus_tag="A7F35_gp03"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009250790.1"
                     /db_xref="GeneID:27438303"
                     /translation="MGHPFHLVEYSPWPLLNSFAVLCMPTGLVYYLRTGDLILMSLGA
                     LMVSFISYLWWRDVVRESTFQGHHSSYVVSGLKAGFILFIVSEVCFFFSFFWAYFHSS
                     LAPTLEVGSVWPPVGISTLSPFQVPLLNTSVLLLSGVSVTWTHHSLEKGDKTSALQGL
                     FLTLALGFYFLWLQYGEYSETSFSIADSVYGSAFFIATGFHGMHVMVGASFLTVCFFR
                     MLTLHFDKGHHLGFLAAAWYWHFVDVVWLFLYVSIYWWGSF"
     gene            13529..13595
                     /locus_tag="A7F35_gt21"
                     /db_xref="GeneID:27438273"
     tRNA            13529..13595
                     /locus_tag="A7F35_gt21"
                     /product="tRNA-Ile"
                     /db_xref="GeneID:27438273"
     gene            13599..14532
                     /gene="ND2"
                     /locus_tag="A7F35_gp02"
                     /db_xref="GeneID:27438304"
     CDS             13599..14532
                     /gene="ND2"
                     /locus_tag="A7F35_gp02"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:14532,aa:TERM)
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009250791.1"
                     /db_xref="GeneID:27438304"
                     /translation="MRLSYVLFLLMMGLGPAVTISSSNWITSWIGLELMFLGTIPMLF
                     SESSFFSLSKESSMKYFCIQALSSALMMFSGVLLYLGYVSVWIFYFVFMVSLFMKLGV
                     FPMHFWVPSVVSGLQWVPIFFILVWQKIPPLAIVKNFCENFDWFSPYIVILGGLSTLV
                     GSLIGLNQTSFRAMLGGSSIAHTGWACFGSIFGGLWIYFFIYFVTFFFLMVSCFLMDY
                     LMVSFLVLSLSGVPPFSMFIGKWSIIKLALCNDLWFVYIMLPIFGAILSLFFYLKFFY
                     SFYLKFILSLTKMKYSPVASLIFLLISGVMFISLF"
     gene            14533..14597
                     /locus_tag="A7F35_gt22"
                     /db_xref="GeneID:27438275"
     tRNA            14533..14597
                     /locus_tag="A7F35_gt22"
                     /product="tRNA-Lys"
                     /db_xref="GeneID:27438275"
ORIGIN      
        1 atgcgttgac ttttttctac taatcacaaa gatattggaa cattatatat tgtttttggt
       61 atgtgatgtg gtcttctagg aactggttta agtctcttaa ttcggtttga gttagggacg
      121 gcagggcctt ttttaggaga tgatcatttt tataatgtta ttgtaacagc tcatgctttt
      181 gtaataattt tctttttggt aataccttta ataattgggg ggttcggaaa ttgaatggtt
      241 ccattactaa ttggtgctcc agacataaga tttccccgaa taaataatat aagtttttgg
      301 ttactaccac catcttttat tttattatta tgctcaactt taatagaagg tggggctgga
      361 acagggtgaa ctgtttatcc cccattatcc ggaccaatag gacatggtgg gacttctgtt
      421 gatttggtaa ttttttcttt acacttagct ggagcctctt ctcttctagg ggcaattaat
      481 tttattacta ctatcttcaa tatacgttca cctgctataa caatggaacg tttaagatta
      541 tttgtttggt cagttttagt gacagctttt cttttacttc tttctttacc tgttttagca
      601 ggggctatta ctatactttt aacggatcga aattttaaca cgagtttttt tgatccagct
      661 ggtggtggag acccaatttt atatcaacat ctattttggt tctttggtca tccagaggta
      721 tatattctaa ttttaccagg tttcggaatg atttctcaca ttttaagaaa ttttacctct
      781 aaacctgctt ttggtacgct aggaataatt tatgctctaa tttcgattgg aattcttgga
      841 tttattgtat gagcacacca tatatttact gttggaatgg atgttgatac tcgggcttat
      901 tttaccgcag caacaataat tattgctgtt cctactggaa ttaaagtctt tagttgaata
      961 ataactttgt atggaagtcg tggtcctttt agagctgcta tatattgggt attaggattt
     1021 atttttttat ttacccttgg tggacttact ggaattgtct tgtctaactc ttctttagat
     1081 attgttttac atgacacata ctatgttgtg gcacattttc attatgtttt atcaatagga
     1141 gcagtttttg ctatttttgg gggttttgtt tattggtttc ctttaataac ggggttaacc
     1201 ttacatgaac gttgagccaa agcccatttt ttagtaatgt tttgtgcagt gaatttaaca
     1261 ttttttcctc aacatttttt aggtttagct ggaatacctc gacgttattc ggactatcca
     1321 gacgcgtatt ataaatgaaa ccaaatttct tcgtttggat cattattttc tatttttgct
     1381 gtattaatat ttgtatttgt tttatgagaa tctttagttt cacaacgtgg agttatattt
     1441 actaaagctc ctgccatttc acgtgaatga gaagaaattt tacctttaga tttccataga
     1501 aatactgaat cttctgttac cgtagcataa taataataat taatttttaa ggtaaagtat
     1561 aaattaatac atttcagtta cattgaaaag actcaaatat aattttggtg ccttgatatt
     1621 tttataattt tttaaattga ttactaaaag ataaattact ttaatggaaa tagtaaaaat
     1681 ttatattttc taatatattg taccttttgt ataatggtta tattaaaatt tcattattta
     1741 aaagtccccg aactaagaag agctaacttt taaatagtac tttagtacaa atagagtaac
     1801 tatgttgaat tatggttaga aaattattag ttagtagtga aaaactctca attcttaaga
     1861 tatctggtag ttttggaaag gcatttagtg ctaaaagaat taaacataaa gtgattagat
     1921 tttatgtaaa taatatttta agtttaattt atttctttct agcgttaacc ctaatgaaaa
     1981 tttagactta cattaaattt tttattgtta taatttattt tttacaaaac acatttaatt
     2041 aattctaatt aaattctaaa atgtataaat tagtaataat ttaaaataag aattcttatt
     2101 aataattgaa aactttattg caagtaaaaa gttttataaa tagtttaaag gaactcggca
     2161 aatataaatt tcgactgttt aacaaaaaca tagccttagg ggagtgatct aaggttaaac
     2221 ctgctcaatg tataaatttt caatacaata taaatagccg cggtactctg accgtgctaa
     2281 ggtagcgcaa taaattggct tttaattgga gtcaggtatg aatggaatta cggagtttat
     2341 ctgtctctaa actaattttt tgaaattact gtataggtga aaaagcctat gttaataaaa
     2401 tagacgagaa gacccttaga gttttattta atgtaaatta ttcttacaac aatttagttg
     2461 gggcaactta ggaacagaaa ttctacatta acttcctaaa atatgattac ttaacgattt
     2521 taataatact gtataaacta ccttagggat aacagcataa tttattatat aagtttgtga
     2581 cctcgatgtt ggactaggga attaaaaggt tagccacttt ttaaaaaggt tctgttcgaa
     2641 ctattaatcc tacatgatct gagttcagac cggcgtgagc caggtcagat tcaatctttt
     2701 taatttaaga acttagtacg aaaggaccag tttttagtac ataaatgtgc atcatattga
     2761 cttggcagaa ataatatgcg actgatttag gatcattata taataaaaga tattagtcgg
     2821 ttctgtactt tatataatta tagaagatct attttgcaag tagaaagaag atttaatatc
     2881 tcagaacttt tattattcaa aaagagttta acagaatact aactttggga gttagaggat
     2941 agtaattatt actatttttg aattgatgtt tctttactga ttttgttcat ctttaatttt
     3001 ttgttttcct ttatttaaaa acccaattag aatagcggct atattagtat gtattaggtt
     3061 agtaatggta attatgattt caatactttc aaggttttga ttttcctatg ttttgttcct
     3121 tgtgtatgtt gggggattat tagtattatt tatttatatt tgtttagtta ggagaaatta
     3181 tccttttaaa tttaatttag ttagattact atgtattatg ctaggatcat gtttaatttc
     3241 tttaaagtca gaaagaaggt tacctcttgg tattttaggg tttagaactt gagtaagtgg
     3301 agaaaattta ataaatgata aaaatctatc actattttta tttctagcaa ttctattatt
     3361 aagtatactc cttgtagttg tacgaatttg ccaacctgga gggtttgctg taaatataaa
     3421 taatgagaag aattaaaagg ttgaagtttt ctctgttact ttttaggtac tcattgacca
     3481 tgttatgctt atcagctttt tttttcaggt taaataaaac aattgtttta gaaattaatt
     3541 tgttttcttt gtcaagaatg gatttttcgt taatgatcat ttttgataaa atcagggttg
     3601 ggtttagaat aattgtaaca ttaatttcgg gaagtgtatt tttgtttgct caaaaatata
     3661 tagaagaaga cccattttgt ggtcggttta tttgaattct cttatctttt gtaatttcga
     3721 taaatttact aattttttca gggtcaatct tctttttact tttaggttga gatggattag
     3781 gaatcacttc ttttgctcta attatttatt atgaaagaaa agagtcccag ttagcagggt
     3841 tccaaacact cttagtaaat cggattgggg atgtaattat tgtgttgaga gtctttttat
     3901 ttctttctga aggtcagctt actttaattt ctataagaaa ttcaatgttt tatagttctg
     3961 gtctaatttt aatattatgt attgcagctt tgacgaaaag ggcacaattc ccatttagtt
     4021 catgattacc agcagctata gcagcaccaa ctcctgtaag ggctttagtt cattcttcaa
     4081 ccttagttac ggctggaatt tttatcatta ttcgattaag aataaatttg gatttagatg
     4141 aaataatttg tggaatttta ataatatgtg ggtcaattac ttgtttactt ggtggctgag
     4201 ctgcaactta tgaaaatgat gttaagaaaa ttattgcctt atcaacccta agacaacttg
     4261 gcgttatagt ttttagatta ggattaaatc tacctgcttt ggctcttttc catttatata
     4321 cccatgcact atttaaggca ttgctatttt tagctgcagg gcatatttta atggtaactt
     4381 ttggttctca agacatccga ataattggtg gtgtaggtat attaatacca tttacgtgcg
     4441 taatatttaa tattaggaga ttatgtttag ttggggcccc atttataagt gccttttatt
     4501 ctaaacatat aattttagaa aaaatactca taagcccaat taattttatt agagttttag
     4561 ttatattagt cgctacattg atgacagcca aatatgtttc acgaacttta aaggccatct
     4621 gttggaataa aacaggcaca tttttacttt ctagttattc tggaatctac acttttttac
     4681 ctgtattagt tttagctcta ggtgctatct ttgggggtaa agttattcta agtatagaca
     4741 tttcaaactt tgaatttaga tttttgccaa ggtccagatc tatactaatt aatataatta
     4801 ctgtattagg agtatatata ggcttaattg aaaacaattt aataaaaaaa agattctttc
     4861 tatcaacttt atttttttta actcccctaa tttatgggtc tactaaacca ttaagattgg
     4921 ctctttccaa aatgaaaatt ttagatcaag gctgaattga gccttatttt ttgattaaaa
     4981 acaaaattta ttggattgga gctaaagttt ctagttcggg aatttgacca gatactcgaa
     5041 ttctcatttc ggcttctttt attatttttt gtcttctaaa tggtacttta tttaacaatt
     5101 agaatcttaa cttgtttatg tattttgttg gctgtagcat tctttactct cttggaacga
     5161 aaagttttag ggtatgttca gcttcgtaaa ggtcctaaca aggttagtct ctgaggaatt
     5221 cttcaaccaa ttgctgatgc agtaaaattg tttgtaaaag aaaaagttat accttatggt
     5281 agtaatcaat atatattttt gtttgctcct tgtctcagtt tagttttagg actgagggtt
     5341 tgatacttat accctagaca gtatggaaat aaatttgttt cttgaggttt gttattattt
     5401 ttttgtatta cttctttaaa tgtgtatggg acaatacttg ctggctgagc atcaaactca
     5461 aaatacgctt ttttaggggc tctacgagca gccgcccaaa ctatttcata cgaagttaga
     5521 atattattat tacttctgtt tagagctttt ttactattaa cctgttcatg ggatgaggct
     5581 tttaaatttg ggtttccagt aatatttcta ttaattccta tattaatagt ctgatttaca
     5641 agcactgttg cggaaactaa tcgagcacct tttgattttg cagaaggtga atcagaatta
     5701 gtttctgggt ttaatattga atttggagga ggcttatttg ctatattatt tctggctgaa
     5761 tatgccagaa ttttatttat atcaatagca actagagttt ggtttttttc acaatttaac
     5821 ttctcaacag ttctatttcc tttacaaatc actatcattt caattatgtt tttgttggtt
     5881 cgaggagctt acccacgttt tcgttacgat ttgctaataa tattatgttg aaaatctttt
     5941 ttgcctttct ctttatgtgc tctttcatta acctttctaa gtatcaatct ataaaataag
     6001 tattttggtg gctgaaacaa aggcaataaa ttgtaaattt atctatggca tttacgcccc
     6061 ttacgagaac tgtaggttaa aagtaattat taaaccattg gccttcaaag ccgaaaatga
     6121 ataattccag ttttggtgta cttaatgaaa atctcttgaa tcggtatagt tacttctttc
     6181 tttgccttta cgaaattaaa ttttcacatt ctaattcttt taattagtct agaggcttta
     6241 atattgagat tactagtttt tctgttttct ttttgtttaa cttataaaac taattattct
     6301 atgttcttag tactactaac ttttgctgct tgtgaagcag ccttagggtt atccttactc
     6361 gtaagaattt tgcgtttacg aggaaatgac tttgtaactt cttttaattc tataaagttt
     6421 tatgcataaa atacgtcaag ctaacccagc cgagcaattt ttaagtctgc ctagaccaat
     6481 aaggttttct atttggtgaa acgggggatc aattttaggt ctacttttag ggcttcaaat
     6541 tttgacaggt ttatttttat caatacatta cactgctgac ataactaata catttgcgtc
     6601 tgtaattcac attatacgag atgttccagg aggctgaata ttccgaaatt ttcacgccaa
     6661 tggagcatct ttattctttg tgtttttata tttacatatt ggtcgaggtt tgtattacca
     6721 aagctacatt tctcaacctc atacctgaat agtaggagta actatctttc ttgttagtat
     6781 agggacagca tttttaggat atgtgctacc ttgaggacaa atatcatttt gaggtgctac
     6841 tgtaattact aacttggtat cagctattcc ttatttgggg acttatattg tggagtgagt
     6901 ttgaggaggg ttttcggtcg gtcaatcaac tttaaatcgg ttctattctt tacattttat
     6961 tctaccgttt ttaattgggg gattaagggc tttacatatt ttgtttcttc atgaaaaagg
     7021 atcaactaat cctttaggag aaacacacca tattagaaaa attccatttc atccttactt
     7081 tacttgaaaa gatattgtgg gatttatggt actaattagg atgctaacag ttttagggat
     7141 tttttttcca acaattttag gggaccccga aaattttaat ccggcaagtg ctatagttac
     7201 cccagtccat attcaacctg agtgatattt tttattcgca tatgctatct tgcggtccat
     7261 tcctaacaag ttaggaggag ttattgctct tgcggcttct gttcttattc tttatttttt
     7321 acctataaga agaaattatg gaaaaatggt tcctagatct tttacaccca cataccaaat
     7381 cattttttgg aatctggtag tcttttttgt aattttaact tgattagggg catgccccat
     7441 tgaggagcct tacgccactt tagcaattcc aattagaatt ttatactttc tatcttttat
     7501 tttattagtt agtataccag caatttggaa aaaattttta gctttttagt ttagtttaaa
     7561 taaaaacaaa agcttgtcaa gctttaaata caaagcaaat tatatttgta gctaagtaaa
     7621 caaatagctt aaagtcttaa agcattacat tgaagctgta aatatagaat gtttcttttg
     7681 tttaaatgag attttgaggt caattaaact taatggatgc tagttctcca ttacaaagag
     7741 aaatattttt attccatgat cacgcaataa ttcttttaat tggtattttt tctttctgtg
     7801 gattattagg gtttaaacta attttaaata aatttacttc acgaactgtt ttagaagctc
     7861 agcgattaga gacagtttgg acaattgttc ctgcaatatt actaatttgg ctagcacttc
     7921 cctctcttcg gcttctatat ttattagatg aacagagaag agataacgga ataattttga
     7981 aagctacagg tcatcaatgg tattgaagtt atgaagtacc actatcaaat aacggaagat
     8041 ttgactcgta tataattcca gaaaatgacc taagagaggg ggattatcgc cttctagaag
     8101 ttgataatcg agctgttgta ccttttggag ttgaaaccac tgtaattgta acgtctgctg
     8161 atgttctcca tgcttgaaca cttccatcca taggagtaaa aatagatgcg gtaccggggc
     8221 ggttaaatag aataagcatg tttgtggaaa aaccaggagt ttattatggt caatgctccg
     8281 aaatttgtgg agcaaaccat tcttttatac caattgttct tgaagcatta aatcttgaag
     8341 atttttgcca attacagttt taatttcttt agaaagtatt atgtacattt gccttccaag
     8401 caaaaggact aactaagttt agcctaaaga gtccataaaa gttagtttaa tttaaaatgt
     8461 aggtttgtga ctcctaagat aattctaata ttacttttac taaatgcagc ttgttacagg
     8521 ttgtagttta ataaaacgac aagttgcaag cttgtagaaa agatgtaaga tgtcttctat
     8581 ctggaaaaga tatattccta attatatatt aaattctatt gggccccaag ttcagaaggt
     8641 ttaggtctag tacttccaaa ggtgactgac tgctggctag acctaaacct tctgaacttg
     8701 gggcccaata gaatttaata tataattagg aatatatctt tttctaatct tttgggttta
     8761 cccctcatga aggttcaaaa ccttccgtgc ctgacaccga aaagatatta aatttataaa
     8821 agacttaatt agtcatagta agctcttaaa gcttatgcat tttagtctgc cattttataa
     8881 attagaaaag tagggtaaag aaactttttt tgttttttta actttagtag atgaaataaa
     8941 aggagccttt tttctcaatc tagataataa aaagaaataa gaaaacaaaa ttactgtaaa
     9001 cataaaaaat ccaagtattg ggctaagctg aggcataaac aaggaaggta taacttgtaa
     9061 atatatacta tctttaatta acagttaaat gctatttagc ttcatcctta attatagaaa
     9121 acttaagaac taacaagaac cgggtgttcc tcaccataaa gcttaataag taaagagaaa
     9181 acatatgctt gaacagaaca aacaaaaatt tcaaacatat tgtaaccaat atggacagta
     9241 ataactggtc ctaaagctaa gaacctacaa gctgtaagag aagtggcaat taatcctata
     9301 ataatatgac cagctctaat attagcaata attcgaattg ttaaagttaa tggacgaatt
     9361 aaaattctta cggtttcaat aataactagg aaaggaataa ataagttagg ggctcctgat
     9421 ggtgcaaaat gagctgcaga ctccattggg aacttaactc aacctgaaaa aattagtatt
     9481 ctccaaaata ctactgctag agatgcagaa atccagatat ttcttgtggg cccataaaca
     9541 aatggaatta acccaataaa atttagaact agaagaattg ttattaaagt aaataatatt
     9601 agagggaacc caagcaatga tttttgccga gtttctctat tagttgaaat taaagataaa
     9661 caaattttcg tgtaggactg ccttcaagag gatgtagtaa taaataacga tgctataaaa
     9721 attggagtga ttcataataa caccgaaaat tttcctgctt gtatacaatc taatgaagaa
     9781 aataaatcgg aatacattta tttgagagaa tctttttctc aaaactaagg ccgaaactta
     9841 gtgcaaattt tcgcttccaa atactaggaa aattaattat cttatgaata tgaagttcaa
     9901 tttcaccttg aaaaagtgac gtgatttttt ttcactaaaa agacaggtac actttccagt
     9961 acacctacta tgttacgact tatttcgttt ttcaacgaaa gtgacgggcg atatgtgcac
    10021 agttaaaatt ttttattcaa tttaggtttt ttttaacttt tacttttaag tccactttat
    10081 attttatttt tcaaaaaaat tccaaattta atcaacattg taactcactt aaaactttca
    10141 cgttagctgc atcatgacct gtcttttttt ggtttgttag attttgggct catctaaaat
    10201 gagaactgaa gacggcgata tacaaacttt tgacttattt gaaagtagaa ttttttgtgg
    10261 aatgtcattt atttagcaag ttcccctaaa gtttttaatt gccgccatgt aatttaagtt
    10321 ttaactttaa agccttagtg cttaggaaac aaattttagt tctatataat agggtatcta
    10381 atcctagttt atgtctagaa ttttagcatt aaagaattaa atcaaaataa agttcttttt
    10441 tataaaaatt ttacctactt ataaaaattc ttttgtttat agctttccta ataaaattaa
    10501 cttagatgtt aggtataacc gcgactgctg gcacctaatt agtccaactt attaagctta
    10561 actaaattat tatttctaat atagtttaat gtcactttct gggttaaaaa gaataattta
    10621 atttttcctt ttctaaaaca aaaattacca tgttaagtta aacttgagct aatttttaat
    10681 cgatacaaag ttcataaata agggaaatat ccattgttgg gttatgagcc caaaagctta
    10741 gagttagctt acttatttaa tttgtgaatc agtctagagc tccctcattt cattcatgga
    10801 acattccgag aattaaaata agtagaaata taaataaact ggttatcatt aaaaaggaat
    10861 aattggttgc taatattctt agaaccggga ataagagggc aatttccaca tcaaaaatta
    10921 agaataatac tactaataaa aaaaaacggg tagaaaatgg agaccgtatc ttacttaaag
    10981 ggtcaaatcc acattcaaaa gaggttaatt tttctctaga gtcaattatt ctaatataat
    11041 tggttattag gtaaactact actaaagcga tggacaaaat aataacaaaa ttaatatgaa
    11101 ggaataacat ttttacgggt tttaagactt atttacgtaa atgttggaaa atcgttttcc
    11161 tccaaaggct ttcaaaacct ttatagtaac aaaatgtttt ggagaattat gatttaggct
    11221 gctaacttga gtctgggaaa ttttttccat ctccatatga ttgagattat tactgttatt
    11281 tctatatctt tttttatact caattgggaa gttttttttg ttgggttaac tttatccaca
    11341 tttttagggc taatatatat gaatataaat tttactttgt taattgatac tatttttttg
    11401 gggtcaacct tttctagagt tttaattttc ttgacctgtt ttctttgttt attatcatta
    11461 ttgtgcaccc cggaagagaa aaattcttgg acttacatat tatgtttggc ttctttatta
    11521 ttagtattaa ttcttgcatt ttccagaagt aacttaataa tattttatat tttttttgag
    11581 gcttctctaa ttccaactct aatattaatt gtagggtggg ggtaccaacc cgaacgatta
    11641 caggctggta cttacataat attgtataca gtaggagctt ctcttcctct tttagtaatc
    11701 ctaatttggc attgttcatc aatggctaga acggaagtat atatattaca tctgtctagc
    11761 cctttattta aaggagtctg tggcttggtt ctttttatag cctttttagt aaaattgcct
    11821 atgtatggag ttcatctttg gcttccgaaa gcacatgtag aagcaccttt agctgggtca
    11881 ataattttgg ctgggattct tttgaagttg ggtggttatg gaataataca gataatttat
    11941 tcctttaatc tggttttaga taattacaca atattaataa tttgtttaag tatatgagga
    12001 gggtttctag caactataat atgtatccaa caagtagatg ttaagtcact agtagcatat
    12061 tcttcagtcg ggcatataag aattgtatca gctgggttat tgatagacac aagatgggga
    12121 gtaataagag caatggttac tatggtagct catggttttt cctcctctgc aatattttgt
    12181 ttagcttact tttcttataa gaaaagacat actcgaaata ttccatacat aaaaggaatg
    12241 ctccaagttt accctatctt aagaatattc tggtttatat tttgttgtat taatatagct
    12301 tgccctccaa cattaaactt aataggagaa ataataattg ttggacctct ctgaaaatac
    12361 agctttttac tagtaatgtg tatagggtta ataatttttt ttagggctgc atacaatata
    12421 tatttatatt catgtgttaa ccatgggaca ttctccagtt atgttttacc tggttatccg
    12481 ctaaaaacct atagaataat tggtctggtc agacatatat tacctttagt tctgattttt
    12541 aagtcttcat tatttctttc ttgtaacttt ttagttttct gaaattatag tttacaagtt
    12601 gtgctatcta aaacttgtcg gtttagccat atgtaatcta gaaaaattta atttatctct
    12661 gagttacaaa ttcagtgctt tatttaagct attctagaaa gaacctcatc agtaaattct
    12721 aacatataaa aataatcaga caacatcaac aaaatgtcag tatcaggcgg ctgctaagaa
    12781 tcctaaatgg tgccctttgt caaaatggag agttaatatt cgaaaaaagc aaactgttaa
    12841 aaatgaagca ccaactatta catgtattcc atgaaatcct gtagcaataa aaaaggcgga
    12901 accgtatact ctatctgcaa tagaaaatga ggtttcccta tattccccat attgaagtca
    12961 aaggaaatag aagcctagag caagagttaa aaataaccct tgtagagctc tagtcttatc
    13021 tcctttctcg agtctatggt gggttcaagt tactcttacc cctgataata aaagaacaga
    13081 tgtatttaat aaaggaacct gaaatggtga aagtgttcta atcccaacag gaggtcaaac
    13141 tgaacctact tccaatgttg gggctaatct actatgaaaa taagctcaaa aaaatgagaa
    13201 aaagaaacaa acttctgata caataaacaa aataaaccca gcctttaatc cagaaacaac
    13261 gtaagaggaa tgatggcctt gaaaagtaga ttcccgcaca acgtctcgtc atcacagata
    13321 tgaaatgaat gaaactataa gtgcccctag acttattaaa attaagtcac ctgttcggag
    13381 atagtaaact aaccctgttg gtatacataa aacggcaaat gagtttagta aaggtcaggg
    13441 cctgtactca actaaatgaa aaggatgtcc cataaatagg aaaaattaag tctttatttt
    13501 atattgggtt tccatcaaga taattttaaa gtagccgccg ggtgaacgga tattattgat
    13561 gttaataaat aaaaaacttt tggttttgct gcttttttat gcgtctttca tatgtattat
    13621 ttttacttat aataggatta gggcctgctg ttactatttc ttcttcaaat tgaattacca
    13681 gctgaattgg gttagagtta atatttttgg ggactatccc catattattt agggagagaa
    13741 gcttcttttc tttaagaaaa gagtcatcaa taaagtactt ttgtattcaa gctttaagga
    13801 gggctttaat aatatttaga ggggttttat tatatttagg ttatgtttcg gtatgaattt
    13861 tttattttgt ttttatagta agtttattta taaaattagg ggttttccca atacactttt
    13921 gggtaccaag agtggtttct ggtttacagt gggttccaat tttttttatt ttagtatgac
    13981 aaaagattcc ccctttggca attgttaaaa atttttgtga gaactttgat tggttttctc
    14041 cttatattgt gattttaggg ggtctaagta ccttagtagg gtcattaatt gggttaaatc
    14101 aaacatcctt ccgagcaata ttaggaggtt cttcaattgc acataccggg tgagcgtgct
    14161 ttggttctat ttttggaggc ctctgaatct atttttttat ttattttgta actttctttt
    14221 ttctaatagt ttcatgtttt ctaatagatt atttaatagt tagttttttg gttctaagat
    14281 tgagaggagt cccccctttt agaatattta ttggaaaatg gaggattatc aaattagccc
    14341 tttgtaacga tttgtggttt gtgtacatta tacttcctat ttttggtgct attttaagtc
    14401 tttttttcta tctaaaattt ttctattctt tctatttaaa gtttattttg agtttgacaa
    14461 aaataaaata ttccccagtt gctagattga tttttctttt aattagagga gttatgttta
    14521 ttagtctttt tttcttcgat gaccgaggga aggtgaaaga tttttaatct tttaacagaa
    14581 tttttttctt cgaagaaa
//

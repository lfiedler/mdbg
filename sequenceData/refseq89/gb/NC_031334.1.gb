LOCUS       NC_031334              13919 bp    DNA     circular INV 12-OCT-2016
DEFINITION  Sarcoptes scabiei type hominis mitochondrial complete genome.
ACCESSION   NC_031334
VERSION     NC_031334.1
DBLINK      BioProject: PRJNA298180
KEYWORDS    RefSeq; complete genome.
SOURCE      mitochondrion Sarcoptes scabiei
  ORGANISM  Sarcoptes scabiei
            Eukaryota; Metazoa; Ecdysozoa; Arthropoda; Chelicerata; Arachnida;
            Acari; Acariformes; Sarcoptiformes; Astigmata; Psoroptidia;
            Sarcoptoidea; Sarcoptidae; Sarcoptinae; Sarcoptes.
REFERENCE   1
  AUTHORS   Mofiz,E., Seemann,T., Bahlo,M., Holt,D., Currie,B.J., Fischer,K.
            and Papenfuss,A.T.
  TITLE     Mitochondrial Genome Sequence of the Scabies Mite Provides Insight
            into the Genetic Diversity of Individual Scabies Infections
  JOURNAL   PLoS Negl Trop Dis 10 (2), E0004384 (2016)
   PUBMED   26872064
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 13919)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (05-OCT-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 13919)
  AUTHORS   Mofiz,E.
  TITLE     Direct Submission
  JOURNAL   Submitted (20-JUL-2015) Papenfuss Lab, Walter and Eliza Hall
            Institute, 1G Royal Parade Parkville Victoria, VIC 3052, AUSTRALIA
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to LN874268.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..13919
                     /organism="Sarcoptes scabiei"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /isolation_source="clinical patient B"
                     /host="Homo sapiens"
                     /db_xref="taxon:52283"
                     /country="Australia"
     gene            complement(51..102)
                     /gene="trnS1"
                     /locus_tag="APQ71_gt01"
                     /db_xref="GeneID:29141477"
     tRNA            complement(51..102)
                     /gene="trnS1"
                     /locus_tag="APQ71_gt01"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:29141477"
     gene            complement(101..152)
                     /gene="trnQ"
                     /locus_tag="APQ71_gt02"
                     /db_xref="GeneID:29141507"
     tRNA            complement(101..152)
                     /gene="trnQ"
                     /locus_tag="APQ71_gt02"
                     /product="tRNA-Gln"
                     /db_xref="GeneID:29141507"
     gene            complement(154..205)
                     /gene="trnI"
                     /locus_tag="APQ71_gt03"
                     /db_xref="GeneID:29141505"
     tRNA            complement(154..205)
                     /gene="trnI"
                     /locus_tag="APQ71_gt03"
                     /product="tRNA-Ile"
                     /db_xref="GeneID:29141505"
     gene            complement(297..1151)
                     /gene="ND2"
                     /locus_tag="APQ71_gp13"
                     /db_xref="GeneID:29141498"
     gene            complement(1133..1187)
                     /gene="trnE"
                     /locus_tag="APQ71_gt04"
                     /db_xref="GeneID:29141486"
     tRNA            complement(1133..1187)
                     /gene="trnE"
                     /locus_tag="APQ71_gt04"
                     /product="tRNA-Glu"
                     /db_xref="GeneID:29141486"
     gene            complement(1201..2280)
                     /gene="CYTB"
                     /locus_tag="APQ71_gp12"
                     /db_xref="GeneID:29141495"
     gene            complement(2298..2352)
                     /gene="trnL1"
                     /locus_tag="APQ71_gt05"
                     /db_xref="GeneID:29141483"
     tRNA            complement(2298..2352)
                     /gene="trnL1"
                     /locus_tag="APQ71_gt05"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:29141483"
     gene            2399..2452
                     /gene="trnL2"
                     /locus_tag="APQ71_gt06"
                     /db_xref="GeneID:29141500"
     tRNA            2399..2452
                     /gene="trnL2"
                     /locus_tag="APQ71_gt06"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:29141500"
     gene            2440..3963
                     /gene="COX1"
                     /locus_tag="APQ71_gp11"
                     /db_xref="GeneID:29141501"
     gene            4023..4706
                     /gene="COX2"
                     /locus_tag="APQ71_gp10"
                     /db_xref="GeneID:29141480"
     gene            4774..4828
                     /gene="trnD"
                     /locus_tag="APQ71_gt07"
                     /db_xref="GeneID:29141481"
     tRNA            4774..4828
                     /gene="trnD"
                     /locus_tag="APQ71_gt07"
                     /product="tRNA-Asp"
                     /db_xref="GeneID:29141481"
     gene            4828..4983
                     /gene="ATP8"
                     /locus_tag="APQ71_gp09"
                     /db_xref="GeneID:29141494"
     gene            4987..5646
                     /gene="ATP6"
                     /locus_tag="APQ71_gp08"
                     /db_xref="GeneID:29141479"
     gene            5657..6433
                     /gene="COX3"
                     /locus_tag="APQ71_gp07"
                     /db_xref="GeneID:29141478"
     gene            6444..6499
                     /gene="trnG"
                     /locus_tag="APQ71_gt08"
                     /db_xref="GeneID:29141482"
     tRNA            6444..6499
                     /gene="trnG"
                     /locus_tag="APQ71_gt08"
                     /product="tRNA-Gly"
                     /db_xref="GeneID:29141482"
     gene            6499..6819
                     /gene="ND3"
                     /locus_tag="APQ71_gp06"
                     /db_xref="GeneID:29141496"
     gene            6822..6870
                     /gene="trnR"
                     /locus_tag="APQ71_gt09"
                     /db_xref="GeneID:29141487"
     tRNA            6822..6870
                     /gene="trnR"
                     /locus_tag="APQ71_gt09"
                     /product="tRNA-Arg"
                     /db_xref="GeneID:29141487"
     gene            6869..6920
                     /gene="trnM"
                     /locus_tag="APQ71_gt10"
                     /db_xref="GeneID:29141506"
     tRNA            6869..6920
                     /gene="trnM"
                     /locus_tag="APQ71_gt10"
                     /product="tRNA-Met"
                     /db_xref="GeneID:29141506"
     gene            6921..6976
                     /gene="trnS2"
                     /locus_tag="APQ71_gt11"
                     /db_xref="GeneID:29141502"
     tRNA            6921..6976
                     /gene="trnS2"
                     /locus_tag="APQ71_gt11"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:29141502"
     gene            complement(6982..7034)
                     /gene="trnC"
                     /locus_tag="APQ71_gt12"
                     /db_xref="GeneID:29141508"
     tRNA            complement(6982..7034)
                     /gene="trnC"
                     /locus_tag="APQ71_gt12"
                     /product="tRNA-Cys"
                     /db_xref="GeneID:29141508"
     gene            7052..7109
                     /gene="trnP"
                     /locus_tag="APQ71_gt13"
                     /db_xref="GeneID:29141493"
     tRNA            7052..7109
                     /gene="trnP"
                     /locus_tag="APQ71_gt13"
                     /product="tRNA-Pro"
                     /db_xref="GeneID:29141493"
     gene            7152..7211
                     /gene="trnK"
                     /locus_tag="APQ71_gt14"
                     /db_xref="GeneID:29141504"
     tRNA            7152..7211
                     /gene="trnK"
                     /locus_tag="APQ71_gt14"
                     /product="tRNA-Lys"
                     /db_xref="GeneID:29141504"
     gene            7212..7266
                     /gene="trnN"
                     /locus_tag="APQ71_gt15"
                     /db_xref="GeneID:29141499"
     tRNA            7212..7266
                     /gene="trnN"
                     /locus_tag="APQ71_gt15"
                     /product="tRNA-Asn"
                     /db_xref="GeneID:29141499"
     gene            7282..7935
                     /gene="srRNA"
                     /locus_tag="APQ71_gr02"
                     /db_xref="GeneID:29141503"
     rRNA            7282..7935
                     /gene="srRNA"
                     /locus_tag="APQ71_gr02"
                     /product="12S ribosomal RNA"
                     /note="mt-rnr1, s-rRNA"
                     /db_xref="GeneID:29141503"
     gene            7937..7990
                     /gene="trnV"
                     /locus_tag="APQ71_gt16"
                     /db_xref="GeneID:29141492"
     tRNA            7937..7990
                     /gene="trnV"
                     /locus_tag="APQ71_gt16"
                     /product="tRNA-Val"
                     /db_xref="GeneID:29141492"
     gene            7968..8997
                     /gene="lrRNA"
                     /locus_tag="APQ71_gr01"
                     /db_xref="GeneID:29141510"
     rRNA            7968..8997
                     /gene="lrRNA"
                     /locus_tag="APQ71_gr01"
                     /product="16S ribosomal RNA"
                     /note="mt-rnr2, l-rRNA"
                     /db_xref="GeneID:29141510"
     gene            9001..9056
                     /gene="trnW"
                     /locus_tag="APQ71_gt17"
                     /db_xref="GeneID:29141484"
     tRNA            9001..9056
                     /gene="trnW"
                     /locus_tag="APQ71_gt17"
                     /product="tRNA-Trp"
                     /db_xref="GeneID:29141484"
     gene            complement(9122..10018)
                     /gene="ND1"
                     /locus_tag="APQ71_gp05"
                     /db_xref="GeneID:29141511"
     gene            complement(10084..10491)
                     /gene="ND6"
                     /locus_tag="APQ71_gp04"
                     /db_xref="GeneID:29141485"
     gene            complement(10491..10543)
                     /gene="trnT"
                     /locus_tag="APQ71_gt18"
                     /db_xref="GeneID:29141491"
     tRNA            complement(10491..10543)
                     /gene="trnT"
                     /locus_tag="APQ71_gt18"
                     /product="tRNA-Thr"
                     /db_xref="GeneID:29141491"
     gene            10548..10793
                     /gene="ND4L"
                     /locus_tag="APQ71_gp03"
                     /db_xref="GeneID:29141509"
     gene            10849..12051
                     /gene="ND4"
                     /locus_tag="APQ71_gp02"
                     /db_xref="GeneID:29141489"
     gene            12101..12154
                     /gene="trnH"
                     /locus_tag="APQ71_gt19"
                     /db_xref="GeneID:29141488"
     tRNA            12101..12154
                     /gene="trnH"
                     /locus_tag="APQ71_gt19"
                     /product="tRNA-His"
                     /db_xref="GeneID:29141488"
     gene            12142..13587
                     /gene="ND5"
                     /locus_tag="APQ71_gp01"
                     /db_xref="GeneID:29141497"
     gene            13791..13843
                     /gene="trnF"
                     /locus_tag="APQ71_gt20"
                     /db_xref="GeneID:29141490"
     tRNA            13791..13843
                     /gene="trnF"
                     /locus_tag="APQ71_gt20"
                     /product="tRNA-Phe"
                     /db_xref="GeneID:29141490"
ORIGIN      
        1 tatatatata tatatatata tatatatata tatatatata aataaataaa taaaaacatc
       61 tccctggaga aataaaggat tagaaatcct caatttgttt ttaagaaata tccattaaat
      121 tcaaatttta atatgcttta cactttttct tatttagact tataaaattt tattctatca
      181 aaataaatcc tttcggtaaa gtctaatatt aaaaaaaaca ttataaaaaa taaaaaatat
      241 gacaaaaaaa acaaaaaaaa atcttttgta ttaaaaaaaa aatattgatt ttttaaaaaa
      301 atattaataa catatttata agatcttaaa gtatagaaaa aaacaaaaaa aaatcttata
      361 ataacaaaaa aaattataat ttctttagaa tttataaatt ctataattcc tataatgatt
      421 aatatttttc taaaaaatat aggagaaaaa ggagtcccta acaaattaaa ataaacaaat
      481 atattttcat tttttttaaa aatgaaattt tttattttta aattttcaaa ctttaatata
      541 taaaaaaaaa ttattaaaga atatgaatat ataaaaaagt aaaataaaaa aatttttaat
      601 gaacaaaaaa tagataataa aaatcaacca ttattattta aagaagaaaa aataatggta
      661 gatttaaaat tattatattt atctaatata aaaccggaaa ctaaaaacct aaaaaaggat
      721 attataagta atagaaaaaa attttttaaa ttataaaaca ttataaacaa gggaataatt
      781 ttttgtcaag ttataaataa aaatataata taagaaatgt caataaaata taatattttt
      841 aaatatcaaa aatgaaaagg ccatactcct atttttatgg ttatagaaaa attaaaaatt
      901 aagattgaca aaaaattatc taaaataaaa aaactaatta aaaagaaaac agaagaaata
      961 atttgaacaa taaaatagtt gaaagaattt ttaattctta tttttttatt tttattattt
     1021 tttgttaata aagataaaaa aaatatagta ttaatttcta ttaaaaatca tattataata
     1081 aaagacgaag aagaaataca aaataatgaa gataaaaata aaaaaaatat aattctcttt
     1141 aaatccataa ttttgaaaaa attatattct ttctaaacta aaagagtcat cataaaatta
     1201 taatataatt actgttaaaa aataaaaaaa tgtaaaaaat attcccatag taacataagg
     1261 aggttctaca ggatttcttc caattcaagt taataaaaaa acgttattaa caaaaattca
     1321 gaatataaat tttttaaagg gaaaaaattt tttattaaaa gaattttttt taaaaattaa
     1381 aaaaataaaa attaaaatag aaaagactat taaaattact cccccaattt ttgaaggaac
     1441 tgaacgaaga atagcataag catataaaaa atatcattct ggttgaatat gagtaggggt
     1501 aactatatat attgcttcat tataattttc tacgtcacct aatatattag gaaatactcc
     1561 taaaataata aaaaagaaaa atatcacaaa aaaaaaagat aagaaatctt taataaaaaa
     1621 aaaaggaaaa aattttaatt tatcataatc cgatgataat cctaaagggt tagaagatcc
     1681 atgttcatga agaaaaatta aatgaaaaat aacaaataat aaaataataa aaggtaataa
     1741 aaaatgtaaa gataaaaaac gatttaaagt agattgagaa acagaaaaat taccccataa
     1801 tcaattagtt aatgaatctc ctaaataagg aatagctgat aatattctag taatgacagt
     1861 tgctccccaa aaagatattt gacctcaagg taaaacataa cctataaaag cagttattat
     1921 tcttaataaa agaataataa taccagttaa tcaaacatat tttaatttta aaaatgaatt
     1981 ataatataaa ccacgaaaaa aatgtaaata tataataatg aaaaatagag aagctccatt
     2041 tatatgaata tatcgtataa atcaaccgta atcaacatct cgtataatat gaataactga
     2101 ataaaaagaa atgttagttt ctctaacaaa atttacagat aaaaagaaac ctgtaataat
     2161 ttgacatatt aaagttattc ctaataaaaa acccatattt caaaaatatg aaattgatga
     2221 tggagtaggt aaatttaata aagaattttt taaagttaaa attaatacat tattatttat
     2281 aaaataattt ttacttatat tgtaaaaata atctaaaatc ctaaatttta agcaatatta
     2341 tgcttttaca ataatggacg taaaaaaaat tatttgcact aatcttcata aaacgacctt
     2401 taacttagca taaaaatgca aagagtttaa gcctctttta ttaatgttaa aatttctata
     2461 aatcgatgaa tatattcaac taatcataaa gatattgcaa ctttatattt tatttttggt
     2521 atatgatcag ggttcttagg agctggattt agtatgttga ttcgatatca attatctcaa
     2581 ccaataggaa tttctataaa ttctatattt tataattcag ttgtaaccgc ccatgctttt
     2641 attataattt tttttatagt aatacctatt ataataggag gatttggaaa tttattaatt
     2701 cctttaatat taggctctgc tgatatagct taccctcgat taaataatat aagtttttgg
     2761 ttacttccac catctttaac tttattacta atttctttat tgtgtggaac tggtagagga
     2821 actggctgaa ctatttatcc tcctttatct agaatcactt atcattcaaa tatgtctgta
     2881 gattttacaa ttgtaagatt acatattgct ggaatttctt ctattttaag ttctatcaat
     2941 tttattgtaa ctatttataa tataaaaata aaaggaataa gatgatcaaa cttaactctt
     3001 tttgcttgat ctgttttatt aacctctttt ttattagttt tctcattacc agtattagca
     3061 gcagctttaa caatattatt aacagatcga aatttaagaa cttcattttt tgatcctatt
     3121 ggaggaggtg atcctatttt atatcaacac ttattttgat tttttggaca cccagaagtt
     3181 tacattctta ttattcctgg atttgggata atttctcata ttattactta ttcaagtaat
     3241 aaaagagaac catttggatc tttaggtata atttatgcta tgatttctat tgcaacttta
     3301 ggttttattg tatgagctca tcatatattt actgttggat tagatgttga tactcgagct
     3361 tattttactt cagctactat aattatcgct gtccctacag gagtaaaaat ttttagttga
     3421 ttatctacaa tattaggagg aaaattagat tttaacccct ctatgtattg agcaattggc
     3481 tttgtgtttc tatttagaat gggaggtctt acgggtatta ttttatctaa ctcttcttta
     3541 gatgttagat tacacgatac ttactatgtt gtagctcact ttcattatgt tttatctata
     3601 ggtgctgttt ttgctcttat agggggtttt tctttttgat atataatgtt tacaggttat
     3661 tttttaaaac cttctataat aaaaagacaa ttttgaacaa tatttatagg agttaatata
     3721 acttttttcc ctcaacattt tttaggttta agaggtatac ctcgacggta ttctgattat
     3781 cctgataatt tctcaacttg aaatactatt tcatctttag gaactataat tacaatattc
     3841 tcaatattat tttttatgta tattttatga gattcattat caaaatataa aattatttca
     3901 tctaatggta caataaatct taatatagaa tatttttact cctatcctag ggaaattcac
     3961 actaatttag aaaggtttaa aattttttta aattatacaa aatattaata aaataaaata
     4021 aaatgacaag atgaataata atatcttttc aagattcctg ttcttttact ataatagaat
     4081 tatctttatt tcatgactat atattatcaa tcctaatagg aattgtagta ataattactt
     4141 atattttatt ttatattatt tttaataaaa atttttacaa aaatttatct gaaagaacaa
     4201 aaattgaaat tatctgatca gttgtaccag tttttatttt aattatacta gtaatacctt
     4261 caataaaagt tttatactta atagaagata ttaaaactcc ttctttaaat tttaaaattg
     4321 tggctcatca atgatattga tcttacattg tccctttttt taaaagatat ttttataaaa
     4381 ctaacagaaa agaatatttt tttcatgaat ttgattcaat tatagaaaat gaaaaaatac
     4441 ctcgtttatt aaattgtgat aaaagattaa ttattcctta taaaactaat tctcgtcttt
     4501 tattattctc tacagatgta attcattcat ttagaatacc atctataggt ttaaaagttg
     4561 atgctctccc tggtcgaatt aatcaattgt ttgttaatcc ttctcgtata ggaatatttt
     4621 ttggtcaatg ttcagaaatt tgtggggcta atcattcttt catacctatt tctataaaag
     4681 tagttgatat aaaaacatat gataatatta caaaaaattt tttattagaa aatattagtg
     4741 aaaattttac aaatattaaa aaaattatat tattaagtta tagtttataa taaaacatag
     4801 ttttgtcata actaaaaaaa taacttaatt cttcctcaaa tatttcctct accttgaatt
     4861 ttttttttag ttgttattac tttttcatta ttaatattga gtctttgctt taataatatt
     4921 tataaaagaa aagagataaa attttttgta aaaataaaaa aaactaatat aaatttatta
     4981 tgataaatga taacaaatct tttttctatt tttgatccgt cgttatatat tataaaatct
     5041 tcgtatttaa gaatgttatt tttatctttt gtaattattc cattatctat ttggttttcg
     5101 aattttaatg taatattttt ttataaaatt ataaatttaa tttctaatga aattaattat
     5161 tcaataaaac aaccaaaaaa aggtataaca aaaattattt ctatgatttt tttaatgatc
     5221 tcactattaa attttaatgc cttatttcct tttttatttt ctttaacttc tcatttaata
     5281 ttcactttac ctgttagtta ttctatatgg ttaggtatta tttttataac attaacaaaa
     5341 tctattgttg attttcttgc tcatttaatt cctataggaa ctcctattat attaattagg
     5401 tttataagat tagtagaaat catcagaaat attattcgac ctttagcatt aatattccga
     5461 ttaacagcta atataatagc aggacattta ttattaagtt taattggaaa tacaattctt
     5521 agaattaatt tatatataat gagattagga ttattcctat tatctccttt aatcgtaata
     5581 gaaataggag tatcattaat ccaagcttat gtttttttta ctttactaac cttgtataca
     5641 agagaaatag aataaaatga aaaaatttaa tcaatttcat ttagtagaac ctagaccttg
     5701 acctttaaca acttctttat gtttttttaa tataataata agtataataa ttatatttag
     5761 taaaataaat aggatatatt ttttaatttc tacaatatta ttaatttatt cagcttattt
     5821 atgaagacaa gatatccatc gagagagatg tttagaaggt tcccatcaag atattgtagt
     5881 aaaaggattt aaatttggaa tagttatttt tattttatca gaatgtttct ttttctttgc
     5941 aatattttga tcatatttac atttagctca aactcctgct atagaaattg gtggtatatg
     6001 gcctcctaaa ggaattataa cttttgatcc aaaaggaatt ccttttttta atactatcat
     6061 tttggtatct tcaggtgttt ctgttacatg atcccatcat tcattttcaa aaaaaaatta
     6121 taaagaaaga gtagtaataa tattaattac tattttttta ggtattacat ttactgcgtt
     6181 acaagctata gagtattatg taagaagatt tacaattgct tgttcttctt attcttctat
     6241 ttattactta ggaacaggat ttcatggtac tcatgtaatt attggaagga tattattaat
     6301 gatttgtttt ttacggataa tatattttca tacaagagtt aatcacacag ctatgataga
     6361 atgctctatc tgatattgac attttgttga tgtagtctga ttttttttat atcttatttt
     6421 ttattgatgg ggagtttaat taaaacttac tagtataaaa agtacaatta atttccaatt
     6481 aataagaaaa tataagttat tatattaatt ctaattatta ctattatttt gctaatatta
     6541 atattttttt ttttaattat aattttaact aaatttaata aaataaattt ctataaaaat
     6601 tccccttttg aatgtggttt tcaaaatata acaaaatttt ctacaccttt ttcttttcct
     6661 ttttttatct attctattat atttttaatt tttgatttag agatttctat tttaagttct
     6721 tttcctattt ttattttttt taaagtagaa atatgatttt taatagtttt aattataatt
     6781 attacatttt atgagtgaaa aaaaggtata ataaaatgaa tttttagcaa aaaaaaatta
     6841 gtttcgacct aaataataaa aagctaaaac aaattaaagc taaaagctta ggggctcata
     6901 atctctagaa ttatattgtt gttttaaaaa aattataact ttgaaagtta tactatactt
     6961 tacaagtatt aaaacttaaa aaatctcttt tatattttga atttgcaatt caaagtttat
     7021 aaaacttaag agatgaaaaa tattatatat tctagggtta ttctttttaa agataataac
     7081 tttgggtgtt atagaaaaaa tcccttggat aagaaaaata aaagctgtaa acttttaaaa
     7141 taaaaattat cggattttta agatatttaa tctttatgtt ttttaaacat aaaaagcatt
     7201 aagcaatttc cccaatttta gtttattaaa aacattttgt tgttaacaaa aaataataat
     7261 attggattat aaaattaaaa aaagtttatt cttaattaat aaatattttt tataggaaaa
     7321 ataaaatatt ttctaataat taaaattttt taaaaaatgt aaaagtccta aaagataaat
     7381 actagtgcca gcattcgcgg ttattctata tttttttaag tctttaaatt cttaaaaata
     7441 tacaaaataa ataaatataa ttaagtaaaa ttattttatg aaatttaata aattttaaga
     7501 aagtaaataa aaataaaact aggattagat accctattat tttttagttg tattaaagta
     7561 gtaaattata ataacaaaac ttaatttttt tggcggtttt ctatatattc acaggaactt
     7621 gtataattaa aaagataacc cacttttaat tttacctttt ttattttttt gtacggttgt
     7681 tagataaaaa tttatttttt tttctcaaaa aataaaatat ttttaattca gatcaatgtg
     7741 aagataataa aaaggattat atgagttaca aaaattagaa aaattaattt cataaatttt
     7801 taaaaatgga tgtgaaacgt aaaaatttaa taaaaataaa ttttgaatta ttcctagaaa
     7861 aagtacatat cgcccgtcac tcttataaat tagataagtc gtaacaaagt agaaatactg
     7921 gaaagtgttt cttgtttaga gaaaaaaaat cttgcttaca acaagaaaat aaaaaatctc
     7981 ttttccttaa taattataaa gaatatataa ttattcaatt ttaaaaattc attaaaaatc
     8041 aaattttagt aataaaaata aaaaaaataa aagattacct tttgtataag ggtttttaga
     8101 ataatatatg atttataatt tatagaaaat tcaagatctt taaatttttt tttaaaataa
     8161 aataaaatca aaaattttta aaggggtaat acgctatcgt ttgaatattt atctaaatag
     8221 acaaaaatta aaaatagtaa atatttttat tattattgaa caaattttta aatattcttt
     8281 aaaagtaagt ttacgttgtt ataacaaaac attttattat tatatttaat tattatatta
     8341 taaaaatatt aaattaaaaa attagtatta atattataag atttttatat ttaataaaat
     8401 taaataataa aaaataaaaa aaatttattc attaaattat tagactgttt aacaaaatct
     8461 tatttataat ttttataatc tattctgctc aatgaaatta tttaaatagc tgttaataac
     8521 actaaggtag cgaaatcatt agctacttaa ttggtaactt gtatgaaggg actaactaaa
     8581 aatttatttt ataaaaaata taaattataa aattgttgaa aataacaata tttattccaa
     8641 gacaaaaaga ccctagaatt ttaatgaaaa ttttcattta gttggggaaa taaaattaat
     8701 aaaataaaaa taaaaaaatt tataaaagaa ctttttataa agaaaaattg actaaatact
     8761 ctagggataa cagctttata ttttttaaga gaacttatta aaataaaagt ttaagacctc
     8821 gatgttggat aaaagttttt attaagcgca gaagcttaaa aaaatggatt gttcatccat
     8881 taaatcttta cttgatctga gttaaagtcg gcgtgagcca gactggtttt tatctggaat
     8941 aaaattatat aaataaacag tacgaaagga caattatttt ttcttaaaga aatttatatt
     9001 aaagttataa gttaaaaaaa actatatgtc ttcaaaacat aaaaataaaa actttggggt
     9061 agggggatta aatattaaaa ttaggttaat ataataccta attttattac ataaaaaatt
     9121 ataatttcaa agaaaaaaca aaacaaagaa acataattga aaaaggcaaa caaatttttc
     9181 aattagtgta tattaacaaa tcataacgaa tacgaggaaa tctacaacga attcaaataa
     9241 ataaaaaaca aaataaaaaa atttttaaaa atataaaatc tgaaccaaaa aaaaacaaac
     9301 ctgttaaaaa tcttataaat ataatcaaac catattctgt aataaaaatc aaagcaaaga
     9361 aaccaccccc atattcaata ttaaaaccag aaacaatttc tgattcacct tccgctaaat
     9421 caaagggagt tcgattagat tctgctataa ttattaatat tcaagaaaaa aaaaaaataa
     9481 gattaaaaaa aactaaaggt aaaaaatctt gaagctttac taaaaaaaaa aaatcaaaag
     9541 atttcaagaa aaaaaaaaaa gacataaaaa aaaaaataaa acaaatttca taagaaataa
     9601 cttgactaat aatacgatac cctcctaata aagaatactt agaatttgaa cctcaactag
     9661 taaaaataaa cctataaaca gataatctta tcaaaacaaa aacaataaaa atttttaatt
     9721 ttaaaaaaaa gtaattataa taataataat aaatagatca tattgataat ataataaaaa
     9781 atcctaataa agggcctata aaaaaaataa aaatttttag actctgaatt tttaatctat
     9841 cctttgtaaa aagttttata gaatctgaaa taggttgaag aataccgtat aataaaactt
     9901 tgttaggtcc ttttcgataa tgtattaaac ctataaattt acgctcaact aaagtaaaaa
     9961 aagcaacaga aagaagaact cctaaaaaag ataataaaaa cctaaaattt aagataataa
    10021 aaaaatcttt ggtaaaaacc aaaaaacatt ttaaaaacaa tctattaatt tctttggggg
    10081 atttattatc ctaatattta ttactttaag aaaaaaaata attacaaata taataattat
    10141 tatagttaat aatatttgat aataaaaact aattccttgt atagaataat ttatagaatc
    10201 caaaaagtaa aaaaaaataa agaaaataat aatatatata tatatattgt atttaactat
    10261 agattttttt tcttcatgga cagaaaacaa agaacaataa gaataaataa ttattatacc
    10321 agttgaataa gaaataatta ctacagatat taaaaataaa gacaaagaaa atatataaac
    10381 aaaaaaagaa aataaaacag aaaaaaaaac caaaaaaaaa gaaaacttta aaggagaaaa
    10441 attaaaaata aaaaaaaata ttataatcat taaaaataaa aataataaca atcctcttaa
    10501 atctatgatt tacaaaatca ttattttttc aaactcataa ggaataaatg acaagaatta
    10561 taatatttat attattatct tttattttta taataataaa ttcttccaat gttttaattt
    10621 taatgatatt tactgaacta ataattttag tagttttttt cataataata ataataaaaa
    10681 tatctatctt tataataata tttttcataa taataagtat ttgtataagt acttatggta
    10741 tttgtttatt tataaatata tcacgattta aaaatttttc atacatatat agattttttt
    10801 aaaatgataa taataataat cataagatct tttttattct tgttagatat tagatttata
    10861 aattgatttt taatattttc aatctacttt attattatga atttaccatc aaatttaaat
    10921 aatttatcta tcaaagctat atgactaagg gatcataata ggattttttt aatattctta
    10981 aatttatgaa tattttttat aataatgaaa ataataaaaa aaaaatcctt aaaaataata
    11041 tgatttatat tcttattaat ctgaataagc tttatagtta atagaatttt taattttttc
    11101 tttctgtttg aaataatctt ttttatcata tttttatatt taataataaa tggtaaaaca
    11161 ataaaacgta ttcaagcttc attatatata tttttcttta cttttatatt ttctttacct
    11221 ttacttatta taataataac aatattttat ttatctaatt cacaaaattt ttcaatatta
    11281 agttatattc aaattaataa taatttttta aataaaataa taatagtatt tataataatt
    11341 gtatttttag taaaacttcc catttacata tttcatatat gacttccaaa agctcatgta
    11401 gaagctcctc tagaagggtc aataatttta gcaggggttt tacttaaact tggggggtac
    11461 ggggtaattc gattttcttt tctaacaaaa aattttttta aaataacaag taacttatct
    11521 aatattttag tatttacagc aatcttagga agccttatta taagtttagt atgcttgcga
    11581 caaagtgata taaaatcaat tattgcttac tcttcagtag ttcacataag aattatactt
    11641 ataggaatta tatctttctc aagaatacaa ggtaatcaag gtagattaat aataataatt
    11701 gctcatggat ttatttcacc tttgatattc ttttcagtag attatattta taacaaaata
    11761 aattcacgaa gaatttttat tataaaatca attaattata aaatatcaaa attttacatt
    11821 atatgaatat tttgtcttat acttaatata agattcccta tttttatatc atttttttca
    11881 gaagtaatag taatagcatc tttatctaac aataatatga ttttaatttt tattctaatc
    11941 ataactttat ttttttctgc agcttataac atttttttat ttattttttg ttgtcatggt
    12001 aaatctatta taaaaaaatc cataaaaata aattctctat tcatattata ttattcttta
    12061 ataatttact ttgtattagt ctatcctatt tttatattat gatatttcag tttcaataaa
    12121 tattaacttg tggagttaaa gattaataat atcatatttt caagtttatt ttcaatttta
    12181 atactattat taagaataat atcactttat tcaagattat tttttattac ttacggaaga
    12241 ataacagtag aattattttt tcccataatt agaaataatt tttcaataat atttatatta
    12301 gactgtgtaa gaataatatt ttttttttta gtatcaacta tctcttctat aatttttctg
    12361 tattcaaaat tttatataaa tatgtacaaa aaagaaaaat ataatgaaaa aacattaatg
    12421 ctaacaatgt taatatttgt tttatctata ataattttaa ttttttctta ttcatgattt
    12481 atagtaatac taggatgaga tgggttagga atagtttcat ttctattagt aaaatttttt
    12541 aatgataaaa aaacattaga cgctagaata ttaacagtat tatctaatcg tattggagat
    12601 tgtttattca tcatttcatt tataatgttt tttcataata acgaatatag aataatatct
    12661 ttacaaaaag aaaactctat tattatatta atatttttaa ccttaggatg ttttactaaa
    12721 agagcacaaa tacccttttc agcctgatta cccgcagcta tagctgctcc tactcctgtt
    12781 tcttctttag ttcattcatc tactcttgta acagctggtg tttatcttat aattcgtttt
    12841 aattttttat tagttaattt atctaatcta attttaacag tatctctaac cactataata
    12901 ttaagaggat tctacgccgt aatagaaaaa gattttaaaa aaattgtagc aatatcaact
    12961 ctaagacaaa taagatttat aatattttca attttttcaa caacatgaat tttatctttt
    13021 ttacacatat cttttcatgc tatttttaaa agtatactat tcataaccac tggaatatta
    13081 ataacatttg ttataggtaa tcaaaattca aaatacttta gaaatttttc aatatctttt
    13141 atatctaaaa taatttttat atctgcttgt cttagactta taggtttccc tttctcacta
    13201 ggtttttatt ctaaagatat aattttaata ataaatacag aatttataga aataaaaaaa
    13261 attattttta atttatcctg tatattaact gtatgatact cattacgtat cataaaatta
    13321 ggttttttaa actttacaaa aaattattca actaaaaata gcaaagaaga aggatatttt
    13381 tttatcccgt catacataat ttttattatt tctatctttg taggaaatat ttatatgtac
    13441 atatttttgc ccccaacaaa tttattttct ttagtagaga ttcaatcagg tttaattatt
    13501 attctggggg gatttttatt attttcgtta aaaaaaaata taaaaaaata tttctatctt
    13561 aatataacaa tgatatcttt tataaatttt ttactaaaaa aaataaataa taaaattaga
    13621 aaaataaatt tcgatgttat agaaaatact attaaagaag aactaactat actaccattt
    13681 tttataataa taaaaataaa aatcataata aaaaaaataa acataaattt atttttatct
    13741 tttttattaa tattaatttt atactcatga atataacatt aattcaagaa atgctaaaag
    13801 ctaaaaagcg ttagctttga aggggctaaa ttttaaaagc atatatatat atatatatat
    13861 atatatatat ataatttaga ataaatttta tttttactat ggctattcta aattatata
//

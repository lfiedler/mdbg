LOCUS       NC_032381              13085 bp    DNA     circular INV 04-JAN-2017
DEFINITION  Amphitetranychus viennensis mitochondrion, complete genome.
ACCESSION   NC_032381
VERSION     NC_032381.1
DBLINK      BioProject: PRJNA359036
KEYWORDS    RefSeq.
SOURCE      mitochondrion Amphitetranychus viennensis (hawthorn spider mite)
  ORGANISM  Amphitetranychus viennensis
            Eukaryota; Metazoa; Ecdysozoa; Arthropoda; Chelicerata; Arachnida;
            Acari; Acariformes; Trombidiformes; Prostigmata; Eleutherengona;
            Raphignathae; Tetranychoidea; Tetranychidae; Amphitetranychus.
REFERENCE   1  (bases 1 to 13085)
  AUTHORS   Chen,D.S.
  TITLE     The mitochondrion genome of Amphitetranychus viennensis
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 13085)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (27-DEC-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 13085)
  AUTHORS   Chen,D.S.
  TITLE     Direct Submission
  JOURNAL   Submitted (22-SEP-2016) Department of Invasive Biology Control,
            Guangdong Institute of Applied Biological Resources, Xingangxilu
            105, Guangzhou, Guangdong 510260, China
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KX886344.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..13085
                     /organism="Amphitetranychus viennensis"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /db_xref="taxon:381746"
     gene            1..1536
                     /gene="COX1"
                     /locus_tag="BVB95_gp01"
                     /db_xref="GeneID:30689140"
     CDS             1..1536
                     /gene="COX1"
                     /locus_tag="BVB95_gp01"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009332308.1"
                     /db_xref="GeneID:30689140"
                     /translation="MIWLMSTNHKNIGSMYFMFSLFSGLMGTSFSLVIRMELFQPGSL
                     IQNDFIYNSMVTVHAMIMIFFMVMPAMIGGFGNWLIPLMINTTDLCFPRLNNMNFWML
                     IPSILLMISSSIKGVLNGVGWTMYPPLTSIQYFMSSSIEMMIFSLHIAGLSSITSSIN
                     FISTILLMKNKNYYLSSMTLFSLSILITTLLLLLALPVLAGAITMILMDRNFNTSFFD
                     PSGGGDPILYQHLFWFFGHPEVYILILPGFGMVSHVISYNLGKKEVFGKIGMMFAMMS
                     IGLLGFVVWAHHMFTVGMDVDTRAYFTAATMIIAIPTGIKIFSWFTTIINSHINFNIS
                     FFWALGFLIMFSIGGFTGIVASNSCLDINLHDTYYIVAHFHYVLSMGAVFAIFSGFFM
                     WVPLMYNMYFNMNLLKIHFWSSMIGINLTFFPQHFLGLMGMPRRYSDYLDSMYMWNFI
                     SSMGSMMTFFSMVIFLYTLMENFLLNKMILMSESNSNNQEFMFFYPINSHTMENNNMV
                     YLK"
     D-loop          1537..1580
                     /note="putative control region"
     gene            1581..1911
                     /gene="ND3"
                     /locus_tag="BVB95_gp02"
                     /db_xref="GeneID:30689127"
     CDS             1581..1911
                     /gene="ND3"
                     /locus_tag="BVB95_gp02"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:1911,aa:TERM)
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009332309.1"
                     /db_xref="GeneID:30689127"
                     /translation="MIFVFMIMFILMILFYMYLYFNKNNMNMNFSFSSMYECGYMNMT
                     YSRFFFSMYSFMYLILFLILDLEIFFISMMIFLNLNFLMMMSLFMVTLILIMFFEWKN
                     LTLDWFMI"
     gene            1912..1972
                     /locus_tag="BVB95_gt01"
                     /db_xref="GeneID:30689128"
     tRNA            1912..1972
                     /locus_tag="BVB95_gt01"
                     /product="tRNA-Asn"
                     /db_xref="GeneID:30689128"
     gene            complement(1979..2029)
                     /locus_tag="BVB95_gt02"
                     /db_xref="GeneID:30689141"
     tRNA            complement(1979..2029)
                     /locus_tag="BVB95_gt02"
                     /product="tRNA-Asp"
                     /db_xref="GeneID:30689141"
     gene            2036..2094
                     /locus_tag="BVB95_gt03"
                     /db_xref="GeneID:30689142"
     tRNA            2036..2094
                     /locus_tag="BVB95_gt03"
                     /product="tRNA-Leu"
                     /codon_recognized="CUN"
                     /db_xref="GeneID:30689142"
     gene            complement(2092..2143)
                     /locus_tag="BVB95_gt04"
                     /db_xref="GeneID:30689143"
     tRNA            complement(2092..2143)
                     /locus_tag="BVB95_gt04"
                     /product="tRNA-Glu"
                     /db_xref="GeneID:30689143"
     gene            2144..3141
                     /locus_tag="BVB95_gr02"
                     /db_xref="GeneID:30689144"
     rRNA            2144..3141
                     /locus_tag="BVB95_gr02"
                     /product="16S ribosomal RNA"
                     /db_xref="GeneID:30689144"
     gene            3142..3189
                     /locus_tag="BVB95_gt05"
                     /db_xref="GeneID:30689145"
     tRNA            3142..3189
                     /locus_tag="BVB95_gt05"
                     /product="tRNA-Arg"
                     /db_xref="GeneID:30689145"
     gene            complement(3191..3436)
                     /gene="ND4L"
                     /locus_tag="BVB95_gp13"
                     /db_xref="GeneID:30689146"
     CDS             complement(3191..3436)
                     /gene="ND4L"
                     /locus_tag="BVB95_gp13"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009332310.1"
                     /db_xref="GeneID:30689146"
                     /translation="MFLFLMMMIIFIMMNSKNNFFMMILLDIMMLILMLLIYMNMNNY
                     FLCNLIFLMVFSSIMGIVLIILNSRLKSNFKSNFYKE"
     gene            complement(3437..3479)
                     /locus_tag="BVB95_gt06"
                     /db_xref="GeneID:30689129"
     tRNA            complement(3437..3479)
                     /locus_tag="BVB95_gt06"
                     /product="tRNA-Pro"
                     /db_xref="GeneID:30689129"
     gene            complement(3480..3539)
                     /locus_tag="BVB95_gt07"
                     /db_xref="GeneID:30689147"
     tRNA            complement(3480..3539)
                     /locus_tag="BVB95_gt07"
                     /product="tRNA-Phe"
                     /db_xref="GeneID:30689147"
     gene            complement(3540..4323)
                     /gene="COX3"
                     /locus_tag="BVB95_gp12"
                     /db_xref="GeneID:30689148"
     CDS             complement(3540..4323)
                     /gene="COX3"
                     /locus_tag="BVB95_gp12"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:complement(3540),aa:TERM)
                     /transl_table=5
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009332311.1"
                     /db_xref="GeneID:30689148"
                     /translation="MTNNNFFLVNLSPSPLMICLILFLFINSVNNFSWMIFSYINLIC
                     VFFIFMFYLLFFYFFIMLNENFFFGKYSKKKKNILVNGFFLFIISELMIFISLFWSFI
                     HNAYSPNMFMGNFWPPKGVLVANPTSMIIFGTCILLSSSLIIMVSHNMMIKNYKYKSL
                     FYLFICLTMGMIFIDMQILEMSNYFLKLNFNFNDSIFSSSFLFTTSLHASHVILGLIG
                     LIFSFYLFKSNNNNFIFYLNFEFSIWYWHFVDYIWIGVFTLFY"
     gene            complement(4316..4942)
                     /gene="ATP6"
                     /locus_tag="BVB95_gp11"
                     /db_xref="GeneID:30689130"
     CDS             complement(4316..4942)
                     /gene="ATP6"
                     /locus_tag="BVB95_gp11"
                     /codon_start=1
                     /transl_table=5
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009332312.1"
                     /db_xref="GeneID:30689130"
                     /translation="MLNNLFSSFDVKNFLFFSFLFLNIIFILFFNLFKTYSKFMFLMK
                     KMILFLYFNINKMIFFNLIFMMIFFLNIFSLNFFSFNLTSQFSLNLTFIFLLWLPTMI
                     ISLTKMNNIFLIHLLPLGTKGLLIPMMILIELMSFFIRPLTLFLRLSINMIAGHVLMS
                     LISLMIMHNNLYYLFIMYIYMLMKFLVSFIQSYIIVTLINLYIEEIYD"
     gene            complement(4932..5072)
                     /gene="ATP8"
                     /locus_tag="BVB95_gp10"
                     /db_xref="GeneID:30689131"
     CDS             complement(4932..5072)
                     /gene="ATP8"
                     /locus_tag="BVB95_gp10"
                     /codon_start=1
                     /transl_table=5
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="YP_009332313.1"
                     /db_xref="GeneID:30689131"
                     /translation="MYQISPMNWILIFYIMNYVIMYMFLFYKKLFSMEINNNFMLKIY
                     VK"
     gene            complement(5073..5137)
                     /locus_tag="BVB95_gt08"
                     /db_xref="GeneID:30689132"
     tRNA            complement(5073..5137)
                     /locus_tag="BVB95_gt08"
                     /product="tRNA-Lys"
                     /db_xref="GeneID:30689132"
     gene            complement(5142..5771)
                     /gene="COX2"
                     /locus_tag="BVB95_gp09"
                     /db_xref="GeneID:30689149"
     CDS             complement(5142..5771)
                     /gene="COX2"
                     /locus_tag="BVB95_gp09"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009332314.1"
                     /db_xref="GeneID:30689149"
                     /translation="MTNMNSFWMQNFNNIMMKNMDIFNLSITNLIIVLSIFLMILIYF
                     FMMNRSSMIFMNESNELELFWTVIPAILILIISLPSMFLLYCYEENKFSFMDVKIMGN
                     QWYWTYEFPNKMMIESYIKSSNLIRCLNTNNSLILPSNKFIRLIISSNDVLHSWTIPS
                     LMMKMDAVPGRLNMMFLNSSKSLLLKGQCSEICGINHSFMPISLFILKL"
     gene            complement(5775..5832)
                     /locus_tag="BVB95_gt09"
                     /db_xref="GeneID:30689133"
     tRNA            complement(5775..5832)
                     /locus_tag="BVB95_gt09"
                     /product="tRNA-Tyr"
                     /db_xref="GeneID:30689133"
     gene            5833..6476
                     /locus_tag="BVB95_gr01"
                     /db_xref="GeneID:30689150"
     rRNA            5833..6476
                     /locus_tag="BVB95_gr01"
                     /product="12S ribosomal RNA"
                     /db_xref="GeneID:30689150"
     gene            6477..6528
                     /locus_tag="BVB95_gt10"
                     /db_xref="GeneID:30689151"
     tRNA            6477..6528
                     /locus_tag="BVB95_gt10"
                     /product="tRNA-Gly"
                     /db_xref="GeneID:30689151"
     gene            complement(6530..6590)
                     /locus_tag="BVB95_gt11"
                     /db_xref="GeneID:30689152"
     tRNA            complement(6530..6590)
                     /locus_tag="BVB95_gt11"
                     /product="tRNA-Thr"
                     /db_xref="GeneID:30689152"
     gene            complement(6591..7452)
                     /gene="ND1"
                     /locus_tag="BVB95_gp08"
                     /db_xref="GeneID:30689153"
     CDS             complement(6591..7452)
                     /gene="ND1"
                     /locus_tag="BVB95_gp08"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:complement(6591),aa:TERM)
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009332315.1"
                     /db_xref="GeneID:30689153"
                     /translation="MILFELLIILTMLISVMFITLFERKFLGILNYRKGPNFFILNSF
                     LHSLIDFLKLITKKTLKMNFLIKSFWITMIFFGIMMYFFISLNFPFINSFMYMYMNFF
                     FFFFIYSIMAFFFLMLSYSSNSIFSMISLYRVLIQIISYEVGLMFLFLLPNLMLNSFN
                     FYFYWNYNNKLFLFSFMMIFSLVLVSLSEMNRIPFEFLESETELVSGFNVEYMSSLFS
                     FIFLIEYGFFLSMMIMINFFFLFHYLNILFLFILIIWSRSFMPRYRYDKMLYYFWKDI
                     IMMIFFLYIFT"
     gene            complement(7453..7514)
                     /locus_tag="BVB95_gt12"
                     /db_xref="GeneID:30689134"
     tRNA            complement(7453..7514)
                     /locus_tag="BVB95_gt12"
                     /product="tRNA-Leu"
                     /codon_recognized="UUR"
                     /db_xref="GeneID:30689134"
     gene            complement(7506..7549)
                     /locus_tag="BVB95_gt13"
                     /db_xref="GeneID:30689154"
     tRNA            complement(7506..7549)
                     /locus_tag="BVB95_gt13"
                     /product="tRNA-Gln"
                     /db_xref="GeneID:30689154"
     gene            7554..7606
                     /locus_tag="BVB95_gt14"
                     /db_xref="GeneID:30689155"
     tRNA            7554..7606
                     /locus_tag="BVB95_gt14"
                     /product="tRNA-Cys"
                     /db_xref="GeneID:30689155"
     gene            7620..8684
                     /gene="CYTB"
                     /locus_tag="BVB95_gp07"
                     /db_xref="GeneID:30689156"
     CDS             7620..8684
                     /gene="CYTB"
                     /locus_tag="BVB95_gp07"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome b"
                     /protein_id="YP_009332316.1"
                     /db_xref="GeneID:30689156"
                     /translation="MKKIINSIMMIPTPSNISLMWNFGSMLGLSMLMQIISGLFLSMN
                     YCGNIMLAFNSYIYISKIIENGLILQMIHAHFSSIIFIIMYLHIFKSLMNKSFNKKMM
                     WMSGNIMMLLTMTSAFLGYVLPWGQMSFWGATVITNILSSIPFLGKNIIFWVWGSFSV
                     DNPTLNRFFSLHFMIPLLILSISMFHLMILHEKGSSNQMGFNSNKDKILFSKSFMYKD
                     MISIFLLMLVYFYFLLLFVDQHYSMAKENFFPADPLNTPIHIKPEWYFMFAYSLLRSI
                     PSKIGGIMSLFILFVFFFMLMFNKSHYSKFFFYKKIMLFSMLMFFIILTNLGYKLIEY
                     PFTQMSLYVGILMILSMNFL"
     gene            complement(8685..8729)
                     /locus_tag="BVB95_gt15"
                     /db_xref="GeneID:30689135"
     tRNA            complement(8685..8729)
                     /locus_tag="BVB95_gt15"
                     /product="tRNA-Ser"
                     /codon_recognized="UCN"
                     /db_xref="GeneID:30689135"
     gene            complement(8727..8770)
                     /locus_tag="BVB95_gt16"
                     /db_xref="GeneID:30689157"
     tRNA            complement(8727..8770)
                     /locus_tag="BVB95_gt16"
                     /product="tRNA-Ala"
                     /db_xref="GeneID:30689157"
     gene            8771..9173
                     /gene="ND6"
                     /locus_tag="BVB95_gp06"
                     /db_xref="GeneID:30689158"
     CDS             8771..9173
                     /gene="ND6"
                     /locus_tag="BVB95_gp06"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:9173,aa:TERM)
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009332317.1"
                     /db_xref="GeneID:30689158"
                     /translation="MIIFLFMMNMNLNPMNFLINFSFFLMCLMMMLMNIFNDKWMLLI
                     LMILIVGGMMIFISMMASTMKFNLIMKNKFYYLILMLIFYEIYMDNMNFSFKIILSTM
                     NNISLFLYCFLIMMNVIFLKKIMFKKQKSLKI"
     gene            9174..10331
                     /gene="ND4"
                     /locus_tag="BVB95_gp05"
                     /db_xref="GeneID:30689136"
     CDS             9174..10331
                     /gene="ND4"
                     /locus_tag="BVB95_gp05"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009332318.1"
                     /db_xref="GeneID:30689136"
                     /translation="MSLMILFTFMNNFFSMYMYMYMILMNSNFNLSIMMTFMVLMMMK
                     MIFFSLKEKFLNFFFFGKKTLIKLMTLNLIFFFFAKNYIMFYMYFELISLNIFFLVFM
                     SSFSMQRFKASIYIFIFMCMGTFPMLMMFFFLNDQNMVLIFSLGFLIKIPMLFFHLWL
                     PKAHVEANFYDSMILASLLLKLGGYGLMLMNYSSMMNMLFIYSMLGMIIISLYTLFCT
                     DMKMILAYSSIIHMNGMVLMMLSNNMETENMFTLMMISHSFSSSMMFFMVGMIYEYTY
                     SRNIMINKSTFLNFEMFFMMMFFVLFANMGTPPFMNFFSEMYMYMSLMNFNNNILLMV
                     TMMLLLTILFNLILLKNLSMGTMMKFFKIYNLKMMAMFISLEHLMYMITFM"
     gene            10332..10387
                     /locus_tag="BVB95_gt17"
                     /db_xref="GeneID:30689137"
     tRNA            10332..10387
                     /locus_tag="BVB95_gt17"
                     /product="tRNA-His"
                     /db_xref="GeneID:30689137"
     gene            10388..11944
                     /gene="ND5"
                     /locus_tag="BVB95_gp04"
                     /db_xref="GeneID:30689159"
     CDS             10388..11944
                     /gene="ND5"
                     /locus_tag="BVB95_gp04"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009332319.1"
                     /db_xref="GeneID:30689159"
                     /translation="MFFINIFQTTFMFMLLLTIFYLNLLIKTNMSLILNFYEMFMNSK
                     IHLMFILDTYSLLFFFIIMMVVLNVMSFMKNYMFKKKDMKMFIFMTMMFVLSMMFLVF
                     SFNIWTMIFGWEGLGISSFYLIFYFNNYDSWNSSIKTFFNNKFGDCFMIMSMIFMLMN
                     KNKMMIFFLMLALLTKSAQFPFMAWLPMAMAAPTPISAMVHSSTLVTAGLYIFFRLSN
                     MLISVINLNIFMNLCLISMLISGLKALMEKDMKKVIALSTLSQIGLMLFFLLMNMKLI
                     SFLYMCNHALFKSLMFINMGYMMMFNYSNQYIFNMNTSNMNLFFNFSFKISCFNLMNL
                     TFYSSFYLKEILLMNLNFSMINLMKFIIFLFNSFITMNYSLKLINFSLKLNFKIKLLN
                     KIMFNNIFTLSLMNMFSLIFSKLLMYINLFYFNSNMILLLLYLFMILMNFYILIYKSN
                     FIYLLMYLNLLMYLNFNKLIKKNMDEVELWMEMFSLSYFFFMKKKNILISINMNFFLM
                     TLFMLLVLIQ"
     gene            11947..12000
                     /locus_tag="BVB95_gt18"
                     /db_xref="GeneID:30689138"
     tRNA            11947..12000
                     /locus_tag="BVB95_gt18"
                     /product="tRNA-Trp"
                     /db_xref="GeneID:30689138"
     gene            12001..12891
                     /gene="ND2"
                     /locus_tag="BVB95_gp03"
                     /db_xref="GeneID:30689160"
     CDS             12001..12891
                     /gene="ND2"
                     /locus_tag="BVB95_gp03"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009332320.1"
                     /db_xref="GeneID:30689160"
                     /translation="MINMYMWIYMLSMMLTFMYKNFMMIWFFMEINSMMFFYMLNSIN
                     KNKEMNIIYFIIQEMSSIMIIYMYLLNNFFFLMIFFLIKMGVPPMHSWLLKISSQMNW
                     IILFIVMNLQKIIPLNLIYLNLTKIFVLFVLFSMLMSIYLVMNINNYKYLYSVTSINS
                     VSWMFLLGLMSKTYLFFYLSLYMMVNYYFIMYMYMNNNNNLFNMLNMDMFFLLIILNL
                     ISYPPSIMFMLKMKILFFLSNLMKIKYIMLFMLLMMVIFSLIFFVFFLNHFFTLIWKK
                     ILINMYMYLIIYLNMLMFFI"
     misc_feature    12007..12888
                     /gene="ND2"
                     /locus_tag="BVB95_gp03"
                     /note="NADH dehydrogenase subunit 2; Provisional; Region:
                     ND2; MTH00160"
                     /db_xref="CDD:214442"
     gene            12893..12947
                     /locus_tag="BVB95_gt19"
                     /db_xref="GeneID:30689139"
     tRNA            12893..12947
                     /locus_tag="BVB95_gt19"
                     /product="tRNA-Met"
                     /db_xref="GeneID:30689139"
     gene            12948..12990
                     /locus_tag="BVB95_gt20"
                     /db_xref="GeneID:30689161"
     tRNA            12948..12990
                     /locus_tag="BVB95_gt20"
                     /product="tRNA-Ser"
                     /codon_recognized="AGN"
                     /db_xref="GeneID:30689161"
     gene            12996..13037
                     /locus_tag="BVB95_gt21"
                     /db_xref="GeneID:30689162"
     tRNA            12996..13037
                     /locus_tag="BVB95_gt21"
                     /product="tRNA-Val"
                     /db_xref="GeneID:30689162"
     gene            13041..13085
                     /locus_tag="BVB95_gt22"
                     /db_xref="GeneID:30689163"
     tRNA            13041..13085
                     /locus_tag="BVB95_gt22"
                     /product="tRNA-Ile"
                     /db_xref="GeneID:30689163"
ORIGIN      
        1 ataatttgat taatatcaac aaatcataaa aatattggtt caatgtattt tatatttagt
       61 ttgttttcag gtttaatagg tacctctttt agattagtta ttcgtataga attatttcaa
      121 ccaggttctt taatccaaaa tgattttatt tataactcta tagtaacagt tcatgcaata
      181 attataattt tttttatagt tataccagct ataattggag gatttggaaa ttgattaatt
      241 cctttaataa ttaacacaac agatttatgt tttccccgat taaacaatat aaatttctga
      301 atattaattc cttcaatttt attaataatt tcatcttcaa ttaaaggagt tcttaatgga
      361 gtaggatgaa ctatatatcc tccattaaca tcaattcaat attttatatc ttcatcaatt
      421 gaaataataa ttttttcttt acatattgct ggtttatcct caattacaag atctattaat
      481 tttatttcta caattctatt aataaaaaat aaaaattatt atttaagaag aataacatta
      541 ttttctcttt caattttaat tactacatta cttttattat tagctttacc agttttagct
      601 ggtgccatca caataatttt aatagatcga aattttaaca catctttttt tgatcctaga
      661 ggtgggggag atcctatttt atatcaacat ttattttggt tttttggaca tccagaagta
      721 tatattttaa ttttaccagg atttggtata gtatctcatg ttattagcta taatttaggg
      781 aaaaaagaag tttttggtaa aattggaata atatttgcta taatatccat tggattatta
      841 ggatttgtag tttgagcaca tcatatattt actgtaggaa tagatgtaga tactcgagca
      901 tattttactg ccgctactat aattattgcc attccaacag gaatcaaaat ttttagatga
      961 tttactacta ttattaattc tcatattaat tttaatattt catttttttg agctttagga
     1021 tttttaatta tattttctat tggaggattt acaggaattg tagcctctaa ctcatgttta
     1081 gatattaatt tacatgatac atattatatc gtagctcatt ttcattacgt tttatctata
     1141 ggagcagttt ttgctatttt tagtggattt tttatatgag tccctttaat atataatata
     1201 tattttaaca taaatttact aaaaattcat ttttgatcaa gaataattgg tattaattta
     1261 acattttttc ctcaacattt tttaggatta atgggtatac ctcgacgata ttctgattat
     1321 ttagattcaa tatatatatg aaattttatc tcctctatag gttctataat aacttttttt
     1381 tctatagtca tttttttata cacattaata gaaaattttt tattaaataa aataatttta
     1441 atatcagaaa gaaattcaaa taaccaagaa tttatatttt tttaccctat taatagtcat
     1501 actatagaaa ataataatat agtctattta aaataaataa aaaaaaaaat taaattttta
     1561 atttaataaa taaaattata ataatttttg tatttataat tatatttatt ttaataattt
     1621 tattttatat atatctctac tttaataaaa ataatataaa cataaatttt tcatttagtt
     1681 ctatgtatga gtgtgggtat ataaatataa catactcacg attttttttc tctatatata
     1741 gatttatata tttaatttta tttcttattt tagatttaga aatttttttt atcagaataa
     1801 taattttttt aaatttaaac tttttaataa taataagttt atttatagtt accttaattt
     1861 taattatatt ttttgaatga aaaaatttaa ctttagattg attcataatt tttaaaagaa
     1921 gctttaagca ttttgttgtt aacaaaaagt cgaaaatatt tttcctttta aaattattca
     1981 agataaactg gttaagaaaa attaaattga caatttaatt aattatctta aaaaattttc
     2041 ataaagtata tttatttata ttaagtttag aacttaaagg aaaaaaatga aaattctgta
     2101 aatttgactt tttaaagaaa atgaaaattt cttttaacag aataaaaaat tatatatata
     2161 ttgtaaaaat atacatatta aataaaaata aaaacatttt tattagatat accttttgta
     2221 taaggaaaag ttaaaaattt taattattta tttttataaa tctcgaaaaa taaagaaata
     2281 aatattattt aaaaatattg aataaaaata aatttattaa tgttatatta agagttattt
     2341 tttctagtta tataaatata aattttagtt aagctaaaat aaaaaacttt taattattta
     2401 aattaaaaaa gattttaaat taattataat tattttaaat tatttattta aattataaaa
     2461 gtttaaattt ttttaaaata tataatttaa ttttggttat aataatttta ttagtagaat
     2521 taatgggaaa aaaccgttta aaaaaaatat aatgttataa aataattttt ttttttaaat
     2581 ttattatttt aatttttaac aaaatattat ttattgaata aatcaatagt aaattctgct
     2641 caatgaaaaa ttaaatagcc gattaatcgc taaggtagca taatcacttg ttttttaatt
     2701 aaaaacttga atgaaaggat aaaaaaataa taaaactaaa tgtaatatta aattgaaatt
     2761 ttaattttaa tgaaaatatt aaatttttaa tttagacgga aagaccctaa aatttcactt
     2821 tttaaaaagt tttttgggat aaaatttaat ttaaatattt taattatata ttgaaaagaa
     2881 cttttattta agaatagaaa aaaaaatact ttagggttaa caggataaat tttaattaaa
     2941 gaacaaattt attgaatttt tattacctcg atgttggttt tatataaact ttaaatcgca
     3001 atagatttat ttagttagtc tgttcgacta ataatttata acttgaactg agttcaaatc
     3061 ggtgtaagcc agattggttc ttatctaatt aatattaaat ttttaaaagt acgaaaggaa
     3121 aatttaaatt atattatata atttgattat ttaattttga tttcgaatca aaaaaaacaa
     3181 aaatcaaaat ttattcttta taaaaatttg acttaaaatt agattttaaa cgtgaattta
     3241 aaataattaa aacaatacct ataattcttc taaaaactat taaaaaaatt aaattacaaa
     3301 gaaaataatt atttatattt atataaatca acaatattaa aattaatatt ataatgtcta
     3361 aaagaattat tataaaaaaa ttattttttg aatttattat aataaaaata attattatta
     3421 ttaaaaatag aaaaattcag attaaaaaat ttttaaatcc aaattaaaaa ataaatctgt
     3481 actctttttg aatttacatc ttcaatgtaa ataatataat agagtttaat tattaaaata
     3541 ataaaacaaa gtaaaaactc caattcaaat ataatcaaca aaatgtcaat atcaaattct
     3601 aaattcaaaa tttaaataaa aaataaaatt attattattt cttttaaata aataaaaaga
     3661 aaaaattaaa ccaattaaac ctaaaattac atgagaagca tgtaaagaag tagtgaaaag
     3721 aaaagaactt ctaaaaattc tatcattaaa attaaaattt aactttaaaa aataatttct
     3781 catttctaaa atttgtatat caataaaaat tatacctata gttaaacaaa taaataaata
     3841 aaataatctt ttatatttat aatttttaat cattatatta tgactaacta taataattaa
     3901 tctagagctt aataaaatac aagtaccaaa aataattatt cttgttggat ttgctaccaa
     3961 aactcctttt ggaggtcaaa aattccctat aaatatatta ggggaataag cattatgaat
     4021 aaaacttcaa aataaagaaa taaaaattat taactcagaa ataataaata aaaaaaaacc
     4081 attaaccaaa atattttttt tttttttact atatttacca aaaaaaaaat tttcatttaa
     4141 tataataaaa aaataaaaaa ataataaata aaatataaaa ataaaaaaga cacagattaa
     4201 attaatataa gaaaaaatta ttcaagaaaa attattaact ctattaataa ataaaaataa
     4261 aattaaacaa attattaaag gagaaggact taaattaact aaaaaaaaat tattattagt
     4321 cataaatttc ttcaatatat aaattaatta aagtaacaat aatataagac tgaataaatc
     4381 ttactaaaaa ctttattaat atataaatat atataataaa taaataatat aaattattat
     4441 gtataattat taaactaatt aaacttataa gaacatgccc tgcaattata ttaatactta
     4501 aacgcaaaaa tagggttaaa ggacgaataa aaaatctcat taactcgatt aaaattatta
     4561 tagggattaa taaaccttta gttcctaatg gtaataaatg aattaaaaaa atattgttta
     4621 tcttagttaa tctaataatt atagtaggta atcataataa aaaaataaaa gttaaattta
     4681 atctaaattg tctagttaaa ttaaaactaa aaaagtttaa tctaaaaatg tttaaaaaaa
     4741 aaattattat aaaaattaaa ttaaaaaaaa ttattttatt aatattaaaa taaagaaata
     4801 aaattatttt ttttattaaa aatataaact ttctataagt tttaaataaa ttaaaaaata
     4861 aaataaaaat aatatttaaa aataaaaaag aaaaaaacaa aaaatttttt acatcaaaag
     4921 aagaaaataa attatttaac ataaattttt aatataaaat tattattaat ttctattgaa
     4981 aataattttt tataaaacaa aaatatatat ataatgacat agtttataat ataaaaaatt
     5041 aaaattcagt ttattggaga aatttgataa attcatcaaa agaaaaaatt tctatattta
     5101 acttaagaag ttaatgcttt tattcagcca tttgatgaaa attataattt taaaataaac
     5161 aaactaatag gtataaaact atgattaata ccacaaatct ctgaacattg tccttttaac
     5221 agcaatctct tgcttctatt taaaaatatt atattcaaac gaccaggtac agcatctatt
     5281 tttattatta aactagggat agttcaagaa tgaagaacat cattagatga aataattaaa
     5341 cgaataaatt tatttctagg taaaattaaa gaattatttg tatttaaaca acgaattaaa
     5401 tttctacttt taatatatct ttcaattatt attttattag gaaattcata agttcaatat
     5461 cattgattac ctataatttt tacatctatg aaagaaaact tattttcttc ataacaatat
     5521 aataaaaata ttgaaggtaa agaaataatt aaaattaaaa tagcaggaat aacagttcaa
     5581 aataattcta attcatttct ttcatttata aaaattatag atcttcgatt tattataaaa
     5641 aaatagatta aaattattaa aaaaattctt aaaacaataa ttaaatttgt aattgataaa
     5701 ttaaaaatat ctatattctt tattataata ttattaaaat tttgtatcca aaaagaattt
     5761 atatttgtca taaaatttgg aaagttgatt ttaaaattta tttacaataa attaattata
     5821 aaaaaatcaa aaaattaaat ttttattaca taagataaaa attatatata ttcatatata
     5881 aaatttaata ataaaaaaaa ttaaataatt aataatttaa ttaaatattt ttaaatagcg
     5941 tgccagctaa agcggttaaa cgattaatta aaattaaaca aaaataaaac taaaaaaata
     6001 tagtaaaagt ttatatatag taaaatattt tatatattaa tattaaaata agtaataaaa
     6061 taaataagaa ataaattgga ttagataccc aaataaataa actttatagt actaaattaa
     6121 tttaattaaa ctagaattat ttggcggtaa aatttaaatt taaagggtct tgatttataa
     6181 tagaaaacac tcaataaaaa aatcttttag ttataaattt atatatttcc gttaaattat
     6241 atataaatca aaagttaaat taattattta aataaacagg tcaatatata gttaaaacaa
     6301 aagaaaaatt tagacacaaa aaaaaattaa aagaaataga atttaatagt aatttaaata
     6361 aaaatttaaa aacaatttta aatgaatttt taattttatg tacatattgc ccatcacttt
     6421 cattttaaaa tgaaataagt tgaaacatag tagaagtata agaatatgct tctaaaattt
     6481 tctaaatacc ccgtatattc aatttccaat tgaagagtaa tgaaaataat ttatgtttaa
     6541 aattaaagaa taaatttaca aaatttatat attataaact taaaaaacag aagtaaaaat
     6601 ataaaggaaa aaaattatta taataatatc tttccaaaaa taatataata ttttatcata
     6661 tcgataacga ggtataaaag aacgagatca aatgattaaa ataaataaaa ataaaatatt
     6721 taaataatga aacaaaaaaa agaaattaat tataattatt atagataaaa aaaaaccata
     6781 ctcaattaaa aaaataaaac taaataaaga agatatatat tcaacattaa aacctgaaac
     6841 taattctgtt tctctttcta aaaattcaaa aggaattcga tttatttctc ttaaagaaac
     6901 taaaactaat ctaaaaatta ttataaatga aaataaaaat aatttgttat tataatttca
     6961 ataaaaataa aagttaaaag aatttaatat taaattaggc aaaagaaata aaaatattaa
     7021 acctacctca taactaataa tttgaattaa aactcgatat aaagaaatca tagaaaaaat
     7081 actattagaa ctatatctaa gtattaaaaa aaaaaaagcc atgattgaat aaataaaaaa
     7141 aaaaaaaaaa aaatttatat atatatatat aaaagaatta ataaaaggaa aatttaaaga
     7201 aataaaaaaa tatattataa tcccaaaaaa gattattgta attcaaaatc ttttaattaa
     7261 aaaatttatt tttaaagttt tttttgtaat taatttcaaa aagtcaatta atgaatgtaa
     7321 aaatctattt aaaataaaaa aattaggacc tttacgataa tttaaaatac ctaaaaattt
     7381 tcgttcaaaa agagtaataa atataactct aattaatatt gttaaaataa ttaataattc
     7441 aaataaaatt attttcaaaa gtgtttccac tatataaaat cttaaatttt atgcataata
     7501 tttgcttttt tgaaactaaa aaaagtttca aaaactttaa aaataaaaat tactgataga
     7561 gggggatcct tttattaact gcaatttaat aattaatttc tatcattata tataaaataa
     7621 ttaaaaaaat tattaattct attataataa ttcctactcc ttccaatatt agacttatat
     7681 gaaattttgg ttcaatatta ggattaagaa tattaataca aattatttct ggtttatttt
     7741 tatcaataaa ttattgtgga aatattatac tagcttttaa ctcgtatatt tatattagaa
     7801 agattattga aaatggatta atcttacaaa taattcatgc tcatttttct tcgattattt
     7861 ttattattat atatttacat atttttaaat ctttaataaa taaatctttt aataaaaaaa
     7921 taatatgaat aagtggaaat attataatat tattaactat aacttctgct tttttaggtt
     7981 atgttttacc ttgaggacaa atatcttttt gaggagcaac tgttattact aatattttat
     8041 catctattcc ttttttagga aaaaacatta ttttttgagt ttgaggaagt ttttctgttg
     8101 ataacccaac attaaatcga tttttttctc ttcattttat aattccatta ttaatcttat
     8161 ctatttctat atttcattta ataattcttc acgaaaaagg ttcatctaat caaataggtt
     8221 ttaattcaaa taaagataaa attcttttta gaaaaagatt tatgtataaa gatataattt
     8281 caattttttt attaatatta gtttatttct attttttatt attatttgta gatcaacatt
     8341 atagtatagc aaaagaaaat ttttttccag ctgatccttt aaatacacct attcatatta
     8401 aaccagaatg atattttatg tttgcctact ctttactacg ttcaattcca agaaaaattg
     8461 gaggaattat aagattattt attttatttg tttttttttt tatattaata tttaataaaa
     8521 gacattattc taaatttttt ttttacaaaa aaattatact atttagtata ttaatgtttt
     8581 ttattatttt aactaattta gggtataagt taattgaata tccttttaca caaatatctt
     8641 tatatgtagg aattttaatg attttatcaa taaatttttt ataaaaaagt tttttaataa
     8701 attattttca aaataatttt tataacttta agattattat ataatttgca aaattattta
     8761 tttatcttaa attattattt ttttatttat aataaatata aatttaaatc ctataaattt
     8821 tttaattaac ttttcttttt tccttatatg cttaataata atattaataa acatttttaa
     8881 tgataaatga atactattaa ttcttataat tttaattgta ggaggaataa taatctttat
     8941 ttcaataata gcttcaacta taaaatttaa tttgattata aaaaataaat tttattatct
     9001 aattttaata ttaatttttt atgaaattta tatggacaat ataaactttt catttaaaat
     9061 tattttaaga acaataaata atatttctct attcttatat tgttttttaa ttataataaa
     9121 tgtaattttt ttaaaaaaaa ttatatttaa aaaacaaaaa agattaaaaa tttatgtcat
     9181 taataattct atttactttc ataaataatt ttttttctat atatatatat atatatataa
     9241 ttctaataaa tagtaatttc aatcttagta ttataataac ttttatagtt ttaataataa
     9301 taaaaataat tttctttaga ttaaaagaaa aatttttgaa tttttttttt ttcggaaaaa
     9361 aaactttaat taaattaata acattaaatt tgattttttt tttttttgct aaaaattaca
     9421 tcatgtttta tatatacttt gaattaattt cattaaatat ttttttttta gtttttatat
     9481 caagtttttc tatacaacga tttaaagcta gaatctatat ctttattttt atatgtatag
     9541 gaacttttcc tatattaata atattttttt ttttaaatga ccaaaatata gtattaattt
     9601 tttcattagg atttttaatt aaaatcccaa tattattttt tcacctttga ttacctaaag
     9661 ctcacgtaga ggcaaatttt tacgattcaa taattttagc tagattatta ttaaagttag
     9721 gagggtatgg tttgatgtta ataaactata gaagaataat aaacatatta tttatttata
     9781 gaatattagg aataattatt atctctttat acaccctttt ttgtacagac ataaaaataa
     9841 ttttagcata ttcctcaatt attcatataa atggcatagt gcttataata ttaagaaata
     9901 atatagaaac agaaaatata tttactttaa taataattag acattctttt tcttcatcta
     9961 tgatattttt tatagtagga ataatttatg aatatacata cagacgaaat attataatca
    10021 ataaaagcac ttttttaaat ttcgaaatat ttttcataat aatatttttt gttttatttg
    10081 ccaatatagg tactcctcct tttataaatt ttttttctga aatatatata tatatatctt
    10141 taataaattt taacaacaat attttactaa tagtaaccat aatattatta ttaactattt
    10201 tatttaattt aatcttatta aaaaatttaa gtataggtac aataataaaa ttttttaaaa
    10261 tttataattt aaaaataata gcaatattta tttctttaga acatttaata tatataatta
    10321 catttatata aattgattta gtttataaaa aaatagtaaa ttgtggattt acagaaaaaa
    10381 ttcaataata ttttttatta atatttttca aacaactttt atatttatat tattactaac
    10441 aattttttac ttaaatctat taattaaaac aaatataaga ttaattttaa atttttatga
    10501 aatatttata aatagaaaaa ttcatttaat atttatttta gatacttact ctttattgtt
    10561 tttttttatt attataatag ttgtattaaa cgtaataaga tttataaaaa attatatgtt
    10621 caaaaaaaaa gacataaaaa tatttatttt tataactata atgtttgtgt tatcaataat
    10681 atttttagtg ttttcgttta acatttgaac tataattttt ggatgagaag gattaggaat
    10741 tagctcattt tatttaattt tttattttaa caattatgat tcttgaaata gaagaattaa
    10801 aacctttttt aataataaat ttggagattg ttttataatt atatctataa tctttatact
    10861 tataaataaa aataaaataa taattttttt tttaatatta gcattattaa ctaaaagagc
    10921 acaatttcct ttcatagctt gattaccaat agctatagca gctcccacac ctatttcagc
    10981 tatagtgcat agttccacat tagttactgc tgggttatat atttttttcc gattatctaa
    11041 tatattaatt agagttatta atttaaatat ttttataaat ttatgtttaa tttctatatt
    11101 aattagtgga ttaaaagctt taatagaaaa agatataaaa aaagttattg ctttatctac
    11161 tttaagacaa attggattaa tattattttt ccttttaata aatataaaat taatttcatt
    11221 tctttatata tgtaatcatg ctttatttaa atctttaata tttattaata tgggatatat
    11281 aataatattt aattactcaa atcaatatat ttttaatata aatacttcaa atataaattt
    11341 attctttaat ttttctttta agatttcttg tttcaattta ataaatctaa cattttactc
    11401 ttctttttat ttaaaagaaa ttttattaat aaatttaaat ttttcaataa ttaatctaat
    11461 aaaatttatt attttcttat ttaatagatt tatcactata aactattctt taaaattaat
    11521 taatttttct ttaaaattaa attttaaaat taaattatta aacaaaatca tatttaataa
    11581 tatttttaca ttatctttaa taaatatatt ttccttaatt tttagaaaat tattaatata
    11641 tattaattta ttttatttta attcaaatat aattttatta ttattatact tatttataat
    11701 tttaataaat ttttatatct taatttataa atcaaatttt atttacctat taatatattt
    11761 aaatttatta atatatttaa attttaataa attaattaaa aaaaatatag atgaagtaga
    11821 gttatgaata gaaatatttt ctttaagtta tttttttttt ataaaaaaaa agaatatttt
    11881 aatctctatt aatataaatt tttttttaat aaccctgttt atattgttag ttttaattca
    11941 ataatactaa atttaagtta tttaaactaa ttactttcaa agtaataaaa ttaatttaga
    12001 atcattaata tatatatatg gatttatata ttatctataa tattaacttt tatatataaa
    12061 aattttataa taatttgatt ttttatagag attaattcaa taatattctt ttatatatta
    12121 aattcaatta ataaaaataa agaaataaat attatttatt ttattattca agaaataaga
    12181 tctattataa ttatttatat atacttatta aataattttt tttttttaat aatttttttt
    12241 ttaattaaaa taggggttcc ccctatacac agttgattat taaaaatctc aagacaaatg
    12301 aattgaatta ttcttttcat tgtaatgaat ttacaaaaaa ttattccgtt aaatttaatt
    12361 tacttaaatt taacgaaaat ttttgtttta tttgtacttt tcagaatact tatatctatt
    12421 tatctagtga taaatatcaa taattacaag tatctatact cagtgacctc tattaataga
    12481 gttagatgga tatttctatt aggattaata agaaaaacat atttattttt ttatttatca
    12541 ttgtatataa tagtaaatta ttattttatt atgtatatat atataaataa taataataat
    12601 ttatttaata tgttaaatat ggatatattt tttttattaa tcattctaaa cttaatttct
    12661 taccctcctt caattatatt tatattaaaa ataaaaattt tatttttttt aagaaattta
    12721 ataaaaatca aatatattat attatttata cttcttataa tagttatttt ctcattaatt
    12781 ttttttgtat tttttttaaa tcattttttt acattaattt gaaaaaagat tttaattaat
    12841 atatatatat acttaattat ttatctaaat atattaatat tttttatcta aattaggttt
    12901 aagctattta agctattgga ttcatactcc attgaaattc tcctaaaaaa gaaatattaa
    12961 agctgctaac ttttttaaaa aacttctttt attctctaaa attgagaatt aaaatttaca
    13021 attttataaa ttttagaaaa tagtaaaact tagttattga taattaactt aaaaataatt
    13081 tacta
//

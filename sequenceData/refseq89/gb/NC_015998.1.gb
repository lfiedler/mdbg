LOCUS       NC_015998               8118 bp    DNA     circular INV 14-SEP-2011
DEFINITION  Anaticola crassicornis mitochondrion, complete genome.
ACCESSION   NC_015998
VERSION     NC_015998.1
DBLINK      Project: 73011
            BioProject: PRJNA73011
KEYWORDS    RefSeq.
SOURCE      mitochondrion Anaticola crassicornis (slender duck louse)
  ORGANISM  Anaticola crassicornis
            Eukaryota; Metazoa; Ecdysozoa; Arthropoda; Hexapoda; Insecta;
            Pterygota; Neoptera; Paraneoptera; Psocodea; Phthiraptera;
            Ischnocera; Philopteridae; Anaticola.
REFERENCE   1  (bases 1 to 8118)
  AUTHORS   Cameron,S.L., Yoshizawa,K., Mizukoshi,A., Whiting,M.F. and
            Johnson,K.P.
  TITLE     Mitochondrial genome deletions and minicircles are common in lice
            (Insecta: Phthiraptera)
  JOURNAL   BMC Genomics 12 (1), 394 (2011)
   PUBMED   21813020
  REMARK    Publication Status: Online-Only
REFERENCE   2  (bases 1 to 8118)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (09-SEP-2011) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 8118)
  AUTHORS   Cameron,S.L., Yoshizawa,K., Mizukoshi,A., Whiting,M.F. and
            Johnson,K.P.
  TITLE     Direct Submission
  JOURNAL   Submitted (08-JUN-2011) Ecosystem Sciences, Commonwealth Scientific
            & Industrial Research Organisation, PO Box 1700, Canberra, ACT
            2601, Australia
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to JN121999.
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..8118
                     /organism="Anaticola crassicornis"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /db_xref="taxon:160206"
     tRNA            1..66
                     /product="tRNA-Phe"
                     /anticodon=(pos:29..31,aa:Phe,seq:gaa)
     tRNA            complement(67..130)
                     /product="tRNA-Gly"
                     /anticodon=(pos:complement(97..99),aa:Gly,seq:tcc)
     gene            150..629
                     /gene="ND6"
                     /db_xref="GeneID:11123341"
     CDS             150..629
                     /gene="ND6"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_004842314.1"
                     /db_xref="GeneID:11123341"
                     /translation="MITICMIMMIVVTIYWLASSLVIQLFMMSLFVVVNSIVVYHSSD
                     TSMLSYLMLFSIISGLVAVLAFVMMSEPNTESSILESKSMFLLLSPTVLVYSVVMNME
                     IYHSENMLLINQFSNFSSFSTDLYLHMFLFMALVLLIMLFVVDFILMSHGGTMLPMK"
     tRNA            661..724
                     /product="tRNA-Cys"
                     /anticodon=(pos:692..694,aa:Cys,seq:gca)
     rRNA            725..1422
                     /product="s-rRNA"
                     /note="12S ribosomal RNA"
     tRNA            complement(1423..1483)
                     /product="tRNA-Glu"
                     /anticodon=(pos:complement(1450..1452),aa:Glu,seq:ttc)
     rRNA            1484..2724
                     /product="l-rRNA"
                     /note="16S ribosomal RNA"
     tRNA            2725..2786
                     /product="tRNA-Ile"
                     /anticodon=(pos:2755..2757,aa:Ile,seq:gat)
     gene            2786..4321
                     /gene="COX1"
                     /db_xref="GeneID:11123342"
     CDS             2786..4321
                     /gene="COX1"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_004842315.1"
                     /db_xref="GeneID:11123342"
                     /translation="MNKWLYSTNHKDIGILYMIFGIWSGLLGYSMSLIIRLELSEAGS
                     FINDSHIYNVMVTSHAFMMIFFMVMPLMIGGFANWLVPLMLGAPDMAFPRMNNMSFWL
                     LIPSLVLIMSSMLVDGGTGTGWTVYPPLSSLDGHSGMSVDLSIFSLHLAGVSSILGAI
                     NFISTIINMWKGSMDQLSLFCWSVLITAFLLLLSLPVLAGAITMLLLDRNVNSSFFDP
                     SGGGDPILYQHLFWFFGHPEVYILILPGFGLISHIIMDESGKKEVFGSLGMIYAMIGI
                     GVLGFVVWAHHMFTVGMDVDSRAYFTMATMIIAVPTGIKIFSWLSTFFGGRIAWSVSN
                     MWSMGFVFLFTVGGLTGIVLANSSIDIVLHDTYYVVAHFHYVLSMGAVFAFFGSFLHW
                     FPLVTGLSMNQKWLKIHFVVMFVGVNLTFFPQHMLGLGGMPRRYSDYPDIYYGWNVLS
                     SMGSQITMVSVLMMIFMLFEGFISKRLVLFTCSTPTSLEWHMGHPPSEHSNESSFHLS
                     VIK"
     misc_feature    2786..4285
                     /gene="COX1"
                     /note="cytochrome c oxidase subunit I; Provisional;
                     Region: COX1; MTH00153"
                     /db_xref="CDD:177210"
     misc_feature    order(2807..2809,3047..3049,3059..3061,3065..3067,
                     3086..3088,3209..3211,3221..3223,3275..3277,3287..3289,
                     3341..3343,3362..3364,3371..3373,3395..3397,3422..3430,
                     3443..3448,3452..3454,3482..3484,3596..3598,3605..3607,
                     3614..3619,3629..3631)
                     /gene="COX1"
                     /note="Subunit I/III interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    2819..4147
                     /gene="COX1"
                     /note="Cytochrome C and Quinol oxidase polypeptide I;
                     Region: COX1; pfam00115"
                     /db_xref="CDD:278541"
     misc_feature    order(2834..2836,3017..3019,3050..3052,3071..3073,
                     3080..3082,3245..3250,3266..3268,3278..3280)
                     /gene="COX1"
                     /note="D-pathway; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(2852..2854,2864..2866,2873..2875,2885..2887,
                     3125..3130,3971..3973,4190..4192,4199..4201)
                     /gene="COX1"
                     /note="Subunit I/VIIc interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    order(2894..2896,2903..2908,3995..3997,4004..4009,
                     4106..4108,4160..4162)
                     /gene="COX1"
                     /note="Subunit I/IV interface [polypeptide binding]; other
                     site"
                     /db_xref="CDD:238833"
     misc_feature    order(2942..2944,3452..3454,3566..3568,3575..3580,
                     3653..3661,3668..3670,3677..3679,3701..3703,3725..3727,
                     3734..3736,3755..3763,3797..3799,3851..3856,3860..3862,
                     3866..3880,4070..4075,4085..4093,4118..4123)
                     /gene="COX1"
                     /note="Subunit I/II interface [polypeptide binding]; other
                     site"
                     /db_xref="CDD:238833"
     misc_feature    order(2960..2962,3905..3907,3917..3919,4043..4045,
                     4154..4156)
                     /gene="COX1"
                     /note="Low-spin heme (heme a) binding site [chemical
                     binding]; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3119..3121,3128..3130)
                     /gene="COX1"
                     /note="Subunit I/VIIa interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3182..3184,3410..3421)
                     /gene="COX1"
                     /note="Subunit I/VIa interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3341..3343,3362..3364,3482..3484,3596..3598,
                     3605..3607,3614..3619,3629..3631)
                     /gene="COX1"
                     /note="Dimer interface; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3452..3454,3467..3472,3863..3865,3875..3880,
                     4085..4087)
                     /gene="COX1"
                     /note="Putative water exit pathway; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3491..3493,3641..3646,3899..3901)
                     /gene="COX1"
                     /note="Binuclear center (heme a3/CuB) [ion binding]; other
                     site"
                     /db_xref="CDD:238833"
     misc_feature    order(3491..3493,3503..3505,3536..3538,3641..3646,
                     3719..3721,3728..3730)
                     /gene="COX1"
                     /note="K-pathway; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3569..3571,3581..3583,4256..4258)
                     /gene="COX1"
                     /note="Subunit I/Vb interface [polypeptide binding]; other
                     site"
                     /db_xref="CDD:238833"
     misc_feature    order(3644..3646,3875..3880,4085..4090)
                     /gene="COX1"
                     /note="Putative proton exit pathway; other site"
                     /db_xref="CDD:238833"
     misc_feature    3659..3661
                     /gene="COX1"
                     /note="Subunit I/VIb interface; other site"
                     /db_xref="CDD:238833"
     misc_feature    3758..3760
                     /gene="COX1"
                     /note="Subunit I/VIc interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3902..3904,4085..4090)
                     /gene="COX1"
                     /note="Electron transfer pathway; other site"
                     /db_xref="CDD:238833"
     misc_feature    order(3998..4000,4169..4171)
                     /gene="COX1"
                     /note="Subunit I/VIIIb interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     misc_feature    order(4106..4108,4118..4120)
                     /gene="COX1"
                     /note="Subunit I/VIIb interface [polypeptide binding];
                     other site"
                     /db_xref="CDD:238833"
     tRNA            4327..4389
                     /product="tRNA-Thr"
                     /anticodon=(pos:4357..4359,aa:Thr,seq:tgt)
     tRNA            4389..4452
                     /product="tRNA-Met"
                     /anticodon=(pos:4421..4423,aa:Met,seq:cat)
     tRNA            complement(4516..4584)
                     /product="tRNA-Lys"
                     /anticodon=(pos:complement(4545..4547),aa:Lys,seq:ttt)
     misc_feature    4585..4937
                     /note="putative control region"
     tRNA            4938..5006
                     /product="tRNA-Lys"
                     /anticodon=(pos:4975..4977,aa:Lys,seq:ttt)
     tRNA            complement(5057..5123)
                     /product="tRNA-Gln"
                     /anticodon=(pos:complement(5093..5095),aa:Gln,seq:ttg)
     tRNA            5143..5202
                     /product="tRNA-Lys"
                     /anticodon=(pos:5172..5174,aa:Lys,seq:ttt)
     gene            5203..6639
                     /gene="ND4"
                     /db_xref="GeneID:11123343"
     CDS             5203..6639
                     /gene="ND4"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_004842316.1"
                     /db_xref="GeneID:11123343"
                     /translation="MMFMIGLIILMMLQLLKSVNMKEVIPSISMMFLLIFSEYSNITS
                     ELFFMDESSKNMILLTLFLYIILSMMTFDKGKFMIILLSFLIMFLFFMSSTYIMMFIM
                     FECSLLPIMLLINMYGNQPERIMAVRYMMMYTTLTSVPLFVTIIYLLNKEFSFIVFHQ
                     LTMDMPVSMFMLISLLLAFLVKIPMFLVHTWLPKAHVEAPMEGSIILAGIMLKMGVFG
                     VIRMMMEFYCLSMLTKSILMMIFLVGCILSSWISISVDDMKMNVAFSSISHMNFVMAG
                     LMTNKVLSLKSCILVMLSHGLCSALMFFLVTVYYYKTNSRSFIVSKGVMAQYPTMVMV
                     FFFTWVMNMACPPSVSFFGELMCMSSLIMYSVITLPLMMVFIIINSFFCIFSFGIMCH
                     SNVKIQFKDQINSLMDKLTTWSLLAPLMVMFISLEMIICKYIKMFTLVMGFSILEKSS
                     HQLGLIYYLADVYSEDLISLEFLYYKYI"
     misc_feature    5284..6390
                     /gene="ND4"
                     /note="NADH dehydrogenase subunit 4; Provisional; Region:
                     ND4; MTH00150"
                     /db_xref="CDD:214435"
     misc_feature    5491..6327
                     /gene="ND4"
                     /note="Proton-conducting membrane transporter; Region:
                     Proton_antipo_M; pfam00361"
                     /db_xref="CDD:278775"
     tRNA            6749..6817
                     /product="tRNA-Lys"
                     /anticodon=(pos:6786..6788,aa:Lys,seq:ttt)
     gene            complement(6870..7763)
                     /gene="ND1"
                     /db_xref="GeneID:11123344"
     CDS             complement(6870..7763)
                     /gene="ND1"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_004842317.1"
                     /db_xref="GeneID:11123344"
                     /translation="MVLIFCQIFGIIVMILLSVAFFSLFERKMMGLIQMRKGPGKVLI
                     LGLFQPMADAMKLIAKNNSSPISSPPMIYWLIPMKFMMISLLYWMPLPVKGSWLYMQS
                     SALFIMFSMSLSVYMIIFIGWFSNSKYSLIGGMRSVIQTISYEIVLSFGLFCMCLMCC
                     SINLMDIVKFQQLVWLMLPLMPMLLPLLISVLAETNRTPFDLTEGESELVSGFCVEYG
                     GLSYTLIFLSENASMLFMAVMVSIFMSNSSQMMIMLMIILFVWIRATLPRIRFDSLMQ
                     MCWVFILPSIMAMFMTIIMLM"
     misc_feature    complement(6873..7763)
                     /gene="ND1"
                     /note="NADH dehydrogenase; Region: NADHdh; cl00469"
                     /db_xref="CDD:294320"
     gene            complement(7764..8090)
                     /gene="ND3"
                     /db_xref="GeneID:11123345"
     CDS             complement(7764..8090)
                     /gene="ND3"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_004842318.1"
                     /db_xref="GeneID:11123345"
                     /translation="MWMLVSSVLIIMLMMFSSMFMEDELSINSDTQFECGMESMHPNT
                     VPMYMHFFMVAILFLVFDMEFAISLPLINIMSLITQYMFWWLFIVILMLGLAIEILYG
                     STNWKE"
     misc_feature    complement(7785..>7994)
                     /gene="ND3"
                     /note="NADH-ubiquinone/plastoquinone oxidoreductase, chain
                     3; Region: Oxidored_q4; pfam00507"
                     /db_xref="CDD:278908"
ORIGIN      
        1 atttacagag ctcgatgagc gtagcattga agatgctaag gtttataatg aattataatg
       61 taaatatatt tacccccact gagggtctat ctaattggaa gttagagtta cttctatata
      121 ataggtaaat ttattaaaca aaatttgtga ttattaccat ttgtataatt ataatgattg
      181 ttgtaacaat ttattgattg gcttcatcat tagtaatcca gttatttata atatctttat
      241 ttgttgtggt taattccatt gtagtttatc ataggagaga tacctcaatg ctttcatatt
      301 taatgctttt ttcaattatt agaggattgg tagctgtatt agcatttgtg ataatatcag
      361 aacctaatac tgagtcaaga attcttgaaa gtaaaagaat atttctattg cttagtccaa
      421 cagtcttagt ttactctgtt gttataaaca tggagattta tcatagagaa aatatgcttt
      481 tgatcaatca attttctaat tttaggtctt tttcaaccga tttatattta catatatttt
      541 tgtttatagc tttggttctt ttaattatgc tgtttgtcgt tgactttatt ttgatgtctc
      601 atggtggtac aatgcttcca ataaagtaaa aattacgtta attactaatt tgcaaccatt
      661 agttcaatag ttaattttat aacattagat tgcaaatcta aagaaaagtt tatcttttgg
      721 gcttatctaa tacccaattg tagaaagctc agaagcgttt aagcttatta aatataagtt
      781 tggtaaataa aagaattact aaagtgatta tagctgtagt ttaaacacat ggtcagtata
      841 aaaaagtgcc agcatacgcg gtcatacttt ttgatccttg agctgttcag tatgtttata
      901 ttagtaatta aattggtata agttttatat aagttggtag aatgattata taagttttaa
      961 tcaatatatg aaatataata aaccgtatac ataaactagg attagatacc ctactattca
     1021 cgaatattaa cagcatatgg attaaaattt acaagtttga aaaatttaat tgggtggcaa
     1081 tttaaagtca taatagagga atatgtttat taatcgatat tacacgaaca tcttaccttt
     1141 tattgaatta tcatttcata cacctccgtc tgaagagttt atataaagct ctaaaaccta
     1201 caaggttgta agtcaggtca aggcatggac tattaaaagg attaaatgta ttctattttt
     1261 taagtcaggt ttattcttgt aaaagaataa ttaagaaaaa tttataagta atattttata
     1321 attaaaagaa tatgaataag taactttaca tgtacagatt gcccgtcact ccttatagga
     1381 taagtcgtaa catagttagc ctaccggaag gtaggctaag tttactacta gaatttacat
     1441 ctattcattg aaaatgaatt gttttacttt aaactatagt aataagatta aataaattta
     1501 gtggtttaat tattacattg atgttattat tcaatattca aaagtctttt gttgattgaa
     1561 ttattattaa gaattattat tcatataaaa cagaaatgga aattagatga tagaaaagaa
     1621 taagtctcaa taggcgtacc ttttgtatca gggcttaagg attaaataat tgaatcaatt
     1681 atcccgaatt caaaagatct tccattaaca atattcttag tgtttaatta ttaaacttta
     1741 tgtattggat gatgctacaa gttaatcggt tttgatgata tctggttctt tgataattgg
     1801 tataatcaaa ttggttgata gcttgaccct aattaattag ggaactatta ttaatcaata
     1861 gttcattggg gataagctca ttgaatataa atttttactc atttattcta aaagttaaat
     1921 aaatagaata tctaattgat ttattattaa aaattatgat tataaaagaa tgacctaaaa
     1981 ctttattgga ttaattaaaa tcttatgatt agtaattgat ggttatatat aataaaggaa
     2041 ctcggcaaat taggtttccg cctgtttaat aaaaacatcg ccttttaata gtataaaagg
     2101 ttaattctgc tcaatgcctc tagtaaatag ctgcagtatc ttaactgtac taaggtagca
     2161 taataatttg gcctttaatt ggggtctgga atgaatgaat tcacgagaga ctaactgtct
     2221 ctactgtaat aaatgaattt tatttttaag taaaaaagct taattgagat ggagggacga
     2281 caagacccta tagatcttta ctactcaata taaactaaat gtattgggaa ggttgtctgg
     2341 gatggaccta aaaaacgctt aactttagat agtctaacaa tcaatcattt taattaaatg
     2401 atcctcttag agataaaaag agaaagttac cttagggata acagcacaat tatccctatt
     2461 tagaccattt aaatagtgaa ggttgtgacc tcgatgttga attaagttta ctgataaatg
     2521 tagaaagtta taaagttagt ctgctcgact atttaatact tacatgattt gagtttagac
     2581 cgacgtgagt caggtcagat tctatcttcc atcattactc catatttgta cgaaaggact
     2641 ctatgggggt ttaatataaa ctttggggtt ttagtttatt taaaaattta ctctttgcaa
     2701 gagtgagatc tattaagaag ccttaataaa gtgcctgatg aataggatcg ttttgatgag
     2761 acgaacatgg ctaacaccct ttattatgaa taaatgatta tactcaacta atcataaaga
     2821 tattggaatt ttatatataa tttttggaat ctgatcaggg cttctagggt atagaataag
     2881 gttaattatt cgattagagt taagtgaagc tggaagattc atcaatgaca gacatattta
     2941 taatgtaatg gtaacatctc atgctttcat aataattttt tttatagtta tacctttaat
     3001 gattggagga tttgctaact gattggtgcc tctaatactt ggagctcctg atatagcgtt
     3061 ccctcgtatg aataatataa gattctggct attaattcct tctctagtat tgattatatc
     3121 aagaatatta gtagatgggg gtactggaac aggttgaaca gtttatccac ctttatcaag
     3181 tcttgacggt cattctggta tatcagtaga tttatcaatt ttctctttac atttagcggg
     3241 agttagctca attctaggtg caattaattt tattagcact attatcaaca tatgaaaagg
     3301 ttctatagat caattatcat tattttgctg atcagttctt atcacagctt ttcttttact
     3361 tttatctcta ccagttttgg caggagctat tacaatgtta ttgcttgacc gtaatgttaa
     3421 ttcatccttt tttgacccgt ctggtggggg agatcctatt ttataccaac atttgttttg
     3481 attctttggt cacccagaag tttatatttt gattttacct ggttttggtt taatctctca
     3541 cattattatg gacgaaaggg gtaaaaagga ggtatttggt tctcttggaa taatttatgc
     3601 catgatcggt attggtgtat tggggtttgt tgtgtgagcc catcatatgt tcactgtagg
     3661 aatagatgtt gacagacggg cttacttcac tatagcaact ataattattg ctgttccaac
     3721 aggtattaaa atttttagct ggttatctac attctttggg ggacgaatcg cctggtctgt
     3781 gtccaatata tgaagaatag gatttgtatt cttatttact gtaggtggtt taacaggaat
     3841 cgttttagct aattcttcaa ttgatatcgt tctacacgat acctattatg ttgtagctca
     3901 tttccattat gttttatcta taggtgcagt gttcgctttt tttggtagat tccttcactg
     3961 gtttccttta gttacaggac tatcaataaa ccaaaaatga ttaaaaattc actttgttgt
     4021 tatatttgta ggagtgaatt taactttctt tcctcagcat atattaggat taggaggaat
     4081 gccccgacgt tatagagatt atccagatat ttactatggg tgaaatgtat tgtcttcaat
     4141 agggtcacaa attactatag ttagagtttt aataataatt tttatattat tcgaagggtt
     4201 tattagaaaa cgtttggttt tatttacttg ttctacccca acatctttag aatgacatat
     4261 gggccatcca ccaagagaac attctaatga atcatcattc cacttgtcag ttattaagta
     4321 actgtaagtt aaatagttta tataaaatat ccattttgta aatggatgaa aggagaaatc
     4381 ctttaactac gatagtaagc taaacttaag ctcttagact cataatctaa taatgaataa
     4441 tttcctttcg taaataaagt agtaagttaa aaaaactagt gccgttcaaa ggcaaaattg
     4501 agtaatccta ttttatttac tgaagagtta accattattg aattaaaaat tcaatgcttc
     4561 ttaaagcttt ttagttctca gtaattacct tatataggta acagttgacc ttaaatgact
     4621 ggtctatgat gatagtatta gtgatatgtc ctgattttgt ggtgggcctc ccctataggt
     4681 tgtgtggagg cccctaaata tatttataat aaagaaattc cagcctaata aggtcttcag
     4741 agtatacatc agctagataa tagattaaac ctaattgatg agaggaaaaa tcatggtagg
     4801 attccctctc ataacaatta gtaagttaat aaactagtgc tgtccaaagg caaaatgaga
     4861 ttcatcaggt tcacctatta ctaatactat catcatagac cagtcattta aggtcaactg
     4921 ttacctatat aaggtaatta ctgagaacta aaaagcttta agaagcattg aatttttaat
     4981 tcaataatgg ttaactcttc agtaaataga attaagattg attagattgc ttatcattat
     5041 tgttgtataa gctagtgtgt gataggttaa attaattaac ccaatttagg ttcaaagcct
     5101 aatgttttaa aaactttaac acatgttaaa taaaatcaat agtactgaaa agctttaaga
     5161 agcattgaat ttttaattca atgatggtta atccttcagt aaataatatt tatgattggt
     5221 ctaattattt taataatact tcagttgctg aagagtgtta atataaaaga agtaattcca
     5281 agaattagaa taatattttt attgattttc tcagaatact ctaacattac gtccgaacta
     5341 ttctttatag atgaatcttc taaaaatata attttattaa cattattttt atacattatt
     5401 ttaaggataa tgacttttga taaaggtaaa tttatgatta ttttactatc ttttttaatc
     5461 atgtttttat tttttatatc atctacttat attataatat ttattatatt tgagtgtaga
     5521 cttttaccaa ttatactcct tattaatata tatggtaatc aaccagagcg aattatagct
     5581 gttcggtata taataatata tacaacttta acttctgttc ctttatttgt tacaattatt
     5641 tatctcttaa ataaagagtt ttcatttatt gtttttcatc aacttacgat agacatgcct
     5701 gtaaggatat ttatattaat cagcctcctg ctagcatttt tagtcaaaat tccaatgttt
     5761 ttagttcaca cttgattgcc taaagctcat gttgaagcac ctatagaagg atcgattatt
     5821 ttggcaggaa ttatattaaa aataggagtt tttggcgtaa ttcggataat aatggaattt
     5881 tactgcttat caatgttaac gaaaagcatt ttaataataa tttttttagt tgggtgtatt
     5941 ttatctagtt gaatttctat tagggtagat gatatgaaaa tgaatgtggc attttcttct
     6001 attagacata taaattttgt aatggcagga ctaataacta ataaagtttt gtccttaaaa
     6061 tcatgcattt tagtaatatt atctcacggg ttgtgttcag ctttgatatt ttttttagtg
     6121 acagtttatt actataaaac taactcccga aggtttattg ttaggaaggg tgttatagct
     6181 caatatccga caatagtaat agtttttttc tttacttgag tgataaatat ggcctgtcca
     6241 ccttctgttt cattttttgg tgagttgata tgtatatcat ctttaattat atattctgta
     6301 attacattac cattaataat ggtattcatt attattaatt cattcttttg cattttttcc
     6361 tttgggatta tatgtcactc taatgtaaaa attcagttta aagatcaaat caactcatta
     6421 atggataaac taacgacctg atctttatta gctccattaa tggtaatgtt tatttcttta
     6481 gaaataatca tttgtaaata tattaaaata tttacattag taataggatt ctcaatcctt
     6541 gagaagtcct ctcatcaatt aggtttaatc tattatctag ctgatgtata ctctgaagac
     6601 cttattaggc tggaatttct ttattataaa tatatttagg ggcctccaca caacctatag
     6661 gggaggccca ccacaaaatc aggacatatc actaatacta tcatcataga ccagtcattt
     6721 aaggtcaact gttacctata taaggtaatt actgagaact aaaaagcttt aagaagcatt
     6781 gaatttttaa ttcaataatg gttaactctt cagtaaataa aataggatta ctcaattttg
     6841 cctttggaca gcactagttt tattctattt tatattaata taataattgt tataaatata
     6901 gccatgatag atggtagaat aaatactcaa catatttgta ttagtgaatc aaaccgaatt
     6961 cggggaagtg tagcccgaat tcatacaaat aaaataatta taagtataat catcatttgt
     7021 gatgagtttg atataaaaat tcttaccatc actgctatga ataatatcga agcattttct
     7081 cttaagaaaa tgagcgtata cgacaaccct ccatattcaa cacaaaatcc tgatactagt
     7141 tctctttctc cttcagtcag gtcaaaaggt gttcggttag tttcagctaa tactgaaatt
     7201 aacaagggta aaagtatagg tattagaggt agtattaatc aaactagctg ctggaattta
     7261 acaatatcta ttaaattaat agaacagcac attaagcaca tgcaaaataa accaaatgat
     7321 agaacaattt catatgaaat tgtctgaatt actgagcgta ttccgccaat taaagagtac
     7381 ttagaatttg agaatcatcc aataaagatg attatataaa cagataaact tatactaaat
     7441 ataataaata aggctctcct ttgcatgtac agtcaacttc cttttactgg caaaggtatt
     7501 caatacaaga gggaaattat tataaatttt attgggatta atcagtaaat tataggaggt
     7561 cttctaattg gtgaagaatt attcttagca attaatttta ttgcatctgc tatcggttga
     7621 aaaagaccta aaattagaac tttcccaggt cccttacgta tttggattaa tcctattatt
     7681 tttcgttcga ataacgaaaa aaatgcaact gataaaagaa ttattacaat aatcccgaag
     7741 atttgacaaa aaattagcac catctattct ttccagttag tggatccgta taagatttcg
     7801 attgctagac ctaatataag aatgacaata aatagtcatc aaaatatata ttgggtgatt
     7861 aaacttataa tgttaattag aggtagtcta atagcaaatt ctatatcaaa aacaagaaac
     7921 aaaatggcta ctataaaaaa atgtatgtat attggaaccg tattaggatg tattgattct
     7981 ataccacact caaactgtgt atctgagtta attgataatt catcttctat aaatatagat
     8041 ctaaatatta ttaatataat aattaagact gaggatacta acattcatat tgctaatgga
     8101 tacaaataca cattcttt
//

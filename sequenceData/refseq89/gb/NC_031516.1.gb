LOCUS       NC_031516              13794 bp    DNA     circular INV 17-NOV-2016
DEFINITION  Triodontophorus serratus mitochondrion, complete genome.
ACCESSION   NC_031516
VERSION     NC_031516.1
DBLINK      BioProject: PRJNA350007
KEYWORDS    RefSeq.
SOURCE      mitochondrion Triodontophorus serratus
  ORGANISM  Triodontophorus serratus
            Eukaryota; Metazoa; Ecdysozoa; Nematoda; Chromadorea; Rhabditida;
            Strongylida; Strongyloidea; Strongylidae; Strongylinae;
            Triodontophorus.
REFERENCE   1  (bases 1 to 13794)
  AUTHORS   Gao,J.-F.
  TITLE     Comparative analyses of the complete mitochondrial genomes of two
            equine intestinal parasite Triodontophorus serratus and
            Triodontophorus nipponicus and phylogenetic implications
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 13794)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (21-OCT-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   3  (bases 1 to 13794)
  AUTHORS   Gao,J.-F.
  TITLE     Direct Submission
  JOURNAL   Submitted (03-MAY-2016) Heilongjiang Institute of Veterinary
            Science, Department of Parasitology, Shenjian Road, Longsha
            District, Qiqihaer, Heilongjiang 161005, China
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KX185154.
            
            ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..13794
                     /organism="Triodontophorus serratus"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /host="horse"
                     /db_xref="taxon:54002"
                     /country="China"
     gene            1..1578
                     /gene="COX1"
                     /locus_tag="BK421_gp01"
                     /db_xref="GeneID:29991960"
     CDS             1..1578
                     /gene="COX1"
                     /locus_tag="BK421_gp01"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009310867.1"
                     /db_xref="GeneID:29991960"
                     /translation="MNMYKKYQGGLSVWLESSNHKDIGTLYFLFGLWSGMVGTSLSLI
                     IRLELAKPGLLLGNGQLYNSIITAHAILMIFFMVMPSMIGGFGNWMLPLMLGAPDMSF
                     PRLNNLSFWLLPTAMFLILDSCFVDMGSGTSWTIYPPLSTLGHPGSSVDLAIFSLHCA
                     GLSSILGGINFMCTTKNLRSSSISLEHMSLFVWTVFVTVFLLVLSLPVLAGAITMLLT
                     DRNLNTSFFDPSTGGNPLIYQHLFWFFGHPEVYILILPAFGIISQSTLYLTGKKEVFG
                     SLGMVYAILSIGLIGCVVWAHHMYTVGMDLDSRAYFTAATMVIAVPTGVKVFSWLATL
                     FGMKMIFQPLLLWVLGFIFLITIGGLTGVVLSNSSLDIILHDTYYVVSHFHYVLSLGA
                     VFGIFTGVNLWWTFMTGYVYNKLMMSVVFMLMFIGVNLTFFPLHFAGLHGFPRKYLDY
                     PDVYSVWNVMSSYGSMISVFALFLFLYTLISSFSNNKVMMNDFFINSSPEYSLSSYVF
                     GHSYQSEIYFSCSVMKS"
     gene            1587..1640
                     /locus_tag="BK421_gt01"
                     /db_xref="GeneID:29991985"
     tRNA            1587..1640
                     /locus_tag="BK421_gt01"
                     /product="tRNA-Cys"
                     /db_xref="GeneID:29991985"
     gene            1642..1700
                     /locus_tag="BK421_gt02"
                     /db_xref="GeneID:29991961"
     tRNA            1642..1700
                     /locus_tag="BK421_gt02"
                     /product="tRNA-Met"
                     /db_xref="GeneID:29991961"
     gene            1709..1768
                     /locus_tag="BK421_gt03"
                     /db_xref="GeneID:29991962"
     tRNA            1709..1768
                     /locus_tag="BK421_gt03"
                     /product="tRNA-Asp"
                     /db_xref="GeneID:29991962"
     gene            1771..1827
                     /locus_tag="BK421_gt04"
                     /db_xref="GeneID:29991963"
     tRNA            1771..1827
                     /locus_tag="BK421_gt04"
                     /product="tRNA-Gly"
                     /db_xref="GeneID:29991963"
     gene            1828..2523
                     /gene="COX2"
                     /locus_tag="BK421_gp12"
                     /db_xref="GeneID:29991964"
     CDS             1828..2523
                     /gene="COX2"
                     /locus_tag="BK421_gp12"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009310868.1"
                     /db_xref="GeneID:29991964"
                     /translation="MGNYFQGYNLNFSNSLFSSYMDWFHSFNCSLLLGVLVFVALLFV
                     YLMSNNYYFKSKKIEYQFGELLCSVFPTLILLMQMIPSLSLLYYYGLMNLDSNLTIKV
                     TGHQWYWSYEFSDIPGLEFDSYMKSLYQLNLGEPRLLEVDNRCVVPCDTNIRFCITSA
                     DVIHSWALPSMSIKLDAMSGILSTLCYSFPMVGVFYGQCSEICGANHSFMPIAVEVTL
                     LDNFKSWCYLNMD"
     gene            2524..2578
                     /locus_tag="BK421_gt05"
                     /db_xref="GeneID:29991986"
     tRNA            2524..2578
                     /locus_tag="BK421_gt05"
                     /product="tRNA-His"
                     /db_xref="GeneID:29991986"
     gene            2579..3539
                     /locus_tag="BK421_gr02"
                     /db_xref="GeneID:29991965"
     rRNA            2579..3539
                     /locus_tag="BK421_gr02"
                     /product="large subunit ribosomal RNA"
                     /db_xref="GeneID:29991965"
     gene            3545..3880
                     /gene="ND3"
                     /locus_tag="BK421_gp11"
                     /db_xref="GeneID:29991966"
     CDS             3545..3880
                     /gene="ND3"
                     /locus_tag="BK421_gp11"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009310869.1"
                     /db_xref="GeneID:29991966"
                     /translation="MLNLLFVAMFAVLLLLILYMTNFVMSVKKNDLLKVGAFESGFLS
                     VGKIQNSFSIHFFVMMLMFVIFDLKIMMFLGLLISDISSSVSFMMLMLFIFGGFYMEW
                     WYGKLVWVV"
     gene            3890..5473
                     /gene="ND5"
                     /locus_tag="BK421_gp10"
                     /db_xref="GeneID:29991987"
     CDS             3890..5473
                     /gene="ND5"
                     /locus_tag="BK421_gp10"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009310870.1"
                     /db_xref="GeneID:29991987"
                     /translation="MNIMVYLMSMKFVVLMLMILVVPMMKLSLMLLEWDFLSIKFNFY
                     FNSILFSFILGLVTLSVLIFSTYYLHSELNFNYYYFVLLIFVGSMFMLNYSSSIFTML
                     LSWDLLGISSFFLVLFYNNWDSNSGAMNTALTNRIGDFFIFSFFSGILFYGYYFLSFE
                     MLCSFMVLLLLLTSFTKSAQFPFSSWLPKAMSAPTPVSSLVHSSTLVTAGLILLMNFS
                     KVMMSNNVMIVILLIGLFTMFFSSVAAMVEEDMKKVVALSTLSQMGFSMTTLGLGMSF
                     VAFIHLVSHALFKSCLFMQVGYIIHSNYGQQDGRGYGNNGNLPIFMQLQLLITLFCLC
                     GLVFSSGMVSKDLILELFFMNNYMMILSLMFFISIFLTFGYSYRLWKSFFLSFSKVVS
                     VFSTTMVMNFLSLGLVIFSIMFLWWLNYNLLVMPSMFLYLDFYVPLLYVILIILFSYM
                     IFKLLVKEFVYKFLVDYLAKNVIYKVKNFKFMDNNLNKFGYMGFNFLGLFGTLFTGYM
                     STFKYNNLVILMFFLFLFL"
     gene            5497..5553
                     /locus_tag="BK421_gt06"
                     /db_xref="GeneID:29991988"
     tRNA            5497..5553
                     /locus_tag="BK421_gt06"
                     /product="tRNA-Ala"
                     /db_xref="GeneID:29991988"
     misc_feature    5554..5831
                     /note="AT-region"
     gene            5832..5884
                     /locus_tag="BK421_gt07"
                     /db_xref="GeneID:29991967"
     tRNA            5832..5884
                     /locus_tag="BK421_gt07"
                     /product="tRNA-Pro"
                     /db_xref="GeneID:29991967"
     gene            5934..5989
                     /locus_tag="BK421_gt08"
                     /db_xref="GeneID:29991968"
     tRNA            5934..5989
                     /locus_tag="BK421_gt08"
                     /product="tRNA-Val"
                     /db_xref="GeneID:29991968"
     gene            5990..6424
                     /gene="ND6"
                     /locus_tag="BK421_gp09"
                     /db_xref="GeneID:29991969"
     CDS             5990..6424
                     /gene="ND6"
                     /locus_tag="BK421_gp09"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009310871.1"
                     /db_xref="GeneID:29991969"
                     /translation="MLVFFLGVSLLSGVFSYMNMDPMKSSFFLILSMLTCMPMLSFSG
                     YVWFSYFICLLFLSGIFVILVYFSSLSKINLVKSYLVFLSLLLSLLVIKISYSNMFFN
                     VSLNVFYYSIFWLLIIYILVILLFFMNFTSYFLNFSGALRKV"
     gene            6439..6672
                     /gene="ND4L"
                     /locus_tag="BK421_gp08"
                     /db_xref="GeneID:29991989"
     CDS             6439..6672
                     /gene="ND4L"
                     /locus_tag="BK421_gp08"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009310872.1"
                     /db_xref="GeneID:29991989"
                     /translation="MIFMFISLFMLFFKWHRFIFILIALEFMMMSLFMKFMGLMTEMM
                     FFYFMCFSVISSILGMIVMVGGMKFYGSDQCIY"
     gene            6681..6737
                     /locus_tag="BK421_gt09"
                     /db_xref="GeneID:29991990"
     tRNA            6681..6737
                     /locus_tag="BK421_gt09"
                     /product="tRNA-Trp"
                     /db_xref="GeneID:29991990"
     gene            6739..6793
                     /locus_tag="BK421_gt10"
                     /db_xref="GeneID:29991970"
     tRNA            6739..6793
                     /locus_tag="BK421_gt10"
                     /product="tRNA-Glu"
                     /db_xref="GeneID:29991970"
     gene            6794..7494
                     /locus_tag="BK421_gr01"
                     /db_xref="GeneID:29991971"
     rRNA            6794..7494
                     /locus_tag="BK421_gr01"
                     /product="small subunit ribosomal RNA"
                     /db_xref="GeneID:29991971"
     gene            7495..7549
                     /locus_tag="BK421_gt11"
                     /db_xref="GeneID:29991972"
     tRNA            7495..7549
                     /locus_tag="BK421_gt11"
                     /product="tRNA-Ser"
                     /codon_recognized="UCN"
                     /db_xref="GeneID:29991972"
     gene            7548..7603
                     /locus_tag="BK421_gt12"
                     /db_xref="GeneID:29991973"
     tRNA            7548..7603
                     /locus_tag="BK421_gt12"
                     /product="tRNA-Asn"
                     /db_xref="GeneID:29991973"
     gene            7604..7658
                     /locus_tag="BK421_gt13"
                     /db_xref="GeneID:29991974"
     tRNA            7604..7658
                     /locus_tag="BK421_gt13"
                     /product="tRNA-Tyr"
                     /db_xref="GeneID:29991974"
     gene            7659..8531
                     /gene="ND1"
                     /locus_tag="BK421_gp07"
                     /db_xref="GeneID:29991975"
     CDS             7659..8531
                     /gene="ND1"
                     /locus_tag="BK421_gp07"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009310873.1"
                     /db_xref="GeneID:29991975"
                     /translation="MFLNFLMIILMMVFILQAIAFVTLHERHLLGSSKNRLGPTKVSF
                     MGVLQALLDGLKLLEKEQMMPLNSSNLSFLLVLGISFVVMYLEWYVLPYFFDFLSFEY
                     SLMFFLCLIGFSVYTTLISGIVSKSKYGIVGALRAGSQSVSYEIAFSLYLLAVMIHYS
                     MFSFVKKFTLSLLIIYLPFLVMIIGELSRAPFDFVEGESELVSGYNVEYTSVAFVSLF
                     LSEYGSLIFFSVMTSMLFFKFSMVVSFVMFSVIVFIRSSYPRFRYDKMMTMFWFKFLP
                     ISLIFLFYFFALFV"
     gene            8540..9139
                     /gene="ATP6"
                     /locus_tag="BK421_gp06"
                     /db_xref="GeneID:29991991"
     CDS             8540..9139
                     /gene="ATP6"
                     /locus_tag="BK421_gp06"
                     /codon_start=1
                     /transl_table=5
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009310874.1"
                     /db_xref="GeneID:29991991"
                     /translation="MNQVFLLDIFMFVFLLQFMLYFKEGMMNSLVKKFLGGLVNVFSY
                     SNVLPMSSIISFFTFIVLLICCFGGYFTYSFCPCGMVEFTFVYAMVAWMSTLLTFISS
                     EKFSVYMSKGGDSYLKTFSMLLVEIVSEFSRPLALTVRLTVNIMVGHLISMMLYMGIE
                     SYMGEKYVWLSILAIMMECFVFFIQSYIFSRLIYLYLNE"
     gene            9152..9214
                     /locus_tag="BK421_gt14"
                     /db_xref="GeneID:29991992"
     tRNA            9152..9214
                     /locus_tag="BK421_gt14"
                     /product="tRNA-Lys"
                     /db_xref="GeneID:29991992"
     gene            9249..9303
                     /locus_tag="BK421_gt15"
                     /db_xref="GeneID:29991976"
     tRNA            9249..9303
                     /locus_tag="BK421_gt15"
                     /product="tRNA-Leu"
                     /codon_recognized="UUR"
                     /db_xref="GeneID:29991976"
     gene            9304..9357
                     /locus_tag="BK421_gt16"
                     /db_xref="GeneID:29991977"
     tRNA            9304..9357
                     /locus_tag="BK421_gt16"
                     /product="tRNA-Ser"
                     /codon_recognized="AGN"
                     /db_xref="GeneID:29991977"
     gene            9358..10203
                     /gene="ND2"
                     /locus_tag="BK421_gp05"
                     /db_xref="GeneID:29991978"
     CDS             9358..10203
                     /gene="ND2"
                     /locus_tag="BK421_gp05"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009310875.1"
                     /db_xref="GeneID:29991978"
                     /translation="MYFFLMMLVVFLSLLSLITNNMLVWWSVFLLMTLVFFMLNKKTN
                     SFSSLFNYFVMQESLGLLFLMFSYSYFQLLIVMFKIGMAPFHFWIFSVTNGVVGFNLM
                     WFLTFQKLPFLLIFLQMMIFKLVFVLMLGLFFCLFQMLLMKTYKNLLILSSTESFNWI
                     TLGFLVSFFNVMFIFVYYFVLMMFVISKFEMMNVNNLIGWETMLVFMNLPFSVNFFVK
                     IFSLSEILKMQGVGILLLLFMMFFSALSLSFWMVNLSTKYYKMFKYNKNIFMFVVPMT
                     IMILV"
     gene            10215..10273
                     /locus_tag="BK421_gt17"
                     /db_xref="GeneID:29991993"
     tRNA            10215..10273
                     /locus_tag="BK421_gt17"
                     /product="tRNA-Ile"
                     /db_xref="GeneID:29991993"
     gene            10274..10327
                     /locus_tag="BK421_gt18"
                     /db_xref="GeneID:29991979"
     tRNA            10274..10327
                     /locus_tag="BK421_gt18"
                     /product="tRNA-Arg"
                     /db_xref="GeneID:29991979"
     gene            10338..10392
                     /locus_tag="BK421_gt19"
                     /db_xref="GeneID:29991980"
     tRNA            10338..10392
                     /locus_tag="BK421_gt19"
                     /product="tRNA-Gln"
                     /db_xref="GeneID:29991980"
     gene            10404..10459
                     /locus_tag="BK421_gt20"
                     /db_xref="GeneID:29991981"
     tRNA            10404..10459
                     /locus_tag="BK421_gt20"
                     /product="tRNA-Phe"
                     /db_xref="GeneID:29991981"
     gene            10460..11572
                     /gene="CYTB"
                     /locus_tag="BK421_gp04"
                     /db_xref="GeneID:29991982"
     CDS             10460..11572
                     /gene="CYTB"
                     /locus_tag="BK421_gp04"
                     /codon_start=1
                     /transl_table=5
                     /product="cytochrome b"
                     /protein_id="YP_009310876.1"
                     /db_xref="GeneID:29991982"
                     /translation="MKNKNNIMNFVTSMLVTLPTSKSLTVGWNFGSMLGMVLMFQILT
                     GTFLAFYYTADGSAAFGAVQYIMYEVNYGWIFRLFHSNGASLFFIFLYLHIFKALFMM
                     SYRMKHVWSSGLTIYLLVMMEAFMGYVLVWAQMSFWAAVVITSLLSVIPVWGPMIVMW
                     IWSGFGVTSATLKFFFVIHFLLPWGLLVLILLHLIFLHSTGSTSSLYCHGDYDKISFG
                     PEFWNKDAYNVVFWLVFVVFTLLYPYSLGDPEMFIEADPMMSPVHIVPEWYFLFAYAI
                     LRAIPNKVLGVLALLMSIVIFYLFIFINNYTSCLNKLNKFLVYSFIISAVFLSWLGQC
                     LVEEPYTMLSPVFSVIYFVLILMIMMMYYFSKKLFI"
     gene            11576..11632
                     /locus_tag="BK421_gt21"
                     /db_xref="GeneID:29991994"
     tRNA            11576..11632
                     /locus_tag="BK421_gt21"
                     /product="tRNA-Leu"
                     /codon_recognized="CUN"
                     /db_xref="GeneID:29991994"
     gene            11633..12398
                     /gene="COX3"
                     /locus_tag="BK421_gp03"
                     /db_xref="GeneID:29991983"
     CDS             11633..12398
                     /gene="COX3"
                     /locus_tag="BK421_gp03"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:12398,aa:TERM)
                     /transl_table=5
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009310877.1"
                     /db_xref="GeneID:29991983"
                     /translation="MYHNFHILSLSSYAYYMFFASLGMTSSLVIFFKYGLIVPFIFSL
                     FTVLLISFAWGKDISMEGLSGYHNFFVMDGFKFGVVLFIFSEFMFFFSIFWVFFDAAL
                     VPVHELGESWSPMGMHLVNPFGVPLLNTIILLSSGVSVTWAHHSLLSNKSCTNSMVLT
                     CVLAAYFTSIQLMEYKEASFSISDGIFGSIFYLSTGFHGIHVLCGGLFLGFNLLRLLK
                     SHFNYNHHLGMEFAILYWHFVDVVWLFLFVFVYWWSY"
     gene            12399..12455
                     /locus_tag="BK421_gt22"
                     /db_xref="GeneID:29991995"
     tRNA            12399..12455
                     /locus_tag="BK421_gt22"
                     /product="tRNA-Thr"
                     /db_xref="GeneID:29991995"
     gene            12456..13685
                     /gene="ND4"
                     /locus_tag="BK421_gp02"
                     /db_xref="GeneID:29991984"
     CDS             12456..13685
                     /gene="ND4"
                     /locus_tag="BK421_gp02"
                     /codon_start=1
                     /transl_table=5
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009310878.1"
                     /db_xref="GeneID:29991984"
                     /translation="MVDFLLLSFLFFFKPLMFFTFIIVFSFVLFKNISWSGLFYVVDS
                     NVFILLIFMMIFIYGMVLISEKNFNLIMLSSLLIMICLFFFISSNMLMLYMYFELSMF
                     PILIMILGYGSQIEKINSSYYLLFYAAICSFPFLFIYYKSMFYFTTCYFDFNISWELF
                     FILSLSFMMKFPVYFLHLWLPKAHVEAPTTASMLLAGLLLKLGTAGYLRILGSMNFVY
                     NNFWVTIALMGMILASFSCVFQSDAKSLAAYSSVTHMSFLLLTMIYIMMSSKIGGLMM
                     MLAHGYTSTLMFYVIGEFYHTTSTRMVYFLNSFFTSSIFFGIIFSLVFLSNSGVPPSL
                     SFLSEFMIIVNSVVISKMLFFMIFIYFMISFYYSLFLIVCMMMGKNYYSFNNFNIGVT
                     VSTVIMMFNIFWLSMFY"
     misc_feature    13686..13794
                     /note="AT-region"
ORIGIN      
        1 attaatatat ataaaaaata tcaaggaggg ttgtcagtat gattggagag atctaatcat
       61 aaggatattg gtactcttta ttttttgttt ggtttgtggt caggaatagt aggtactagt
      121 ttatcgttaa ttattcgttt ggaattagca aaaccaggat tattattggg gaacggtcag
      181 ttatataatt ctattattac tgctcatgct attttaataa ttttttttat agtaatacca
      241 aggataattg gtggttttgg taattgaatg ttaccactaa tgttaggtgc acctgatata
      301 agatttcctc gtttgaataa tttaagattt tggttattac caactgctat atttttaatt
      361 ttggattctt gttttgtgga tatgggttca gggactaggt gaactattta tccaccatta
      421 agaacattag gtcacccagg taggagtgtg gatttagcta tttttagttt acattgtgca
      481 gggttaagtt ctattttagg aggtattaat tttatgtgta ctacaaaaaa tttacgtaga
      541 agttctattt ctttagagca tataagattg tttgtgtgaa ctgtttttgt aactgtattt
      601 ttattagtat tgtctttacc ggtgttggct ggggctatta ctatactgtt aacagatcgt
      661 aatttgaata catctttttt tgatcctaga actggtggta accctttaat ttatcagcat
      721 ttattttgat tttttggtca tccagaggtt tatattttga ttttaccagc gtttggaatt
      781 attagtcagt ctacgttata tttaactggt aaaaaagagg tttttggttc tttaggtata
      841 gtatatgcta ttttaagaat tggtttaatt ggatgcgtgg tttgagcaca tcatatgtat
      901 acagtgggta tggatctaga ttctcgtgcg tattttacag cggctactat agtaattgcg
      961 gtacctacgg gggtaaaagt ttttagatga ttagctacgt tgtttggtat aaaaataatt
     1021 tttcaacctt tgttattatg ggttttgggt tttatttttt taattactat tggaggttta
     1081 acaggtgtgg ttttatcaaa ttctagattg gatattattt tacatgatac ttattatgtg
     1141 gttagtcatt ttcattatgt tctaagtttg ggtgctgttt ttggaatttt tacaggggtt
     1201 aatctttgat gaacatttat aacgggttat gtttataata agttaataat atctgttgtg
     1261 tttatattaa tatttattgg tgttaattta acattttttc cattacattt tgcgggttta
     1321 cacggatttc ctcgtaagta tttggattac ccggatgtgt attctgtatg aaatgttata
     1381 tcttcatatg gatcaataat tagggtgttt gctttgtttt tgtttttata tactttaatc
     1441 aggtcattta gaaacaacaa agtaataata aatgattttt ttatcaatag aagtcctgaa
     1501 tatagtttaa gtagatatgt gtttggtcat aggtatcaat cggaaattta ttttagatgt
     1561 tctgttataa aatcataaaa aaattaaaat tcttagtata attagtatat ttaattgcaa
     1621 attaaaaggt taagagtttt aaataagata ggataagtaa gtctgttagg ttcataccct
     1681 aagggtgttt tctcttatta taaagttaaa agttttagta taataattag tataaactat
     1741 tgtcaatagt taggtttatt aaactttaag agttctttag tataatttag tatgtttgat
     1801 ttccaatcaa ggggtagtaa aggaattatt ggtaattatt ttcaaggtta taatttaaat
     1861 ttttctaata gattattttc tagttatata gattgatttc atagatttaa ttgtaggttg
     1921 ttattgggtg ttttagtttt tgtggcttta ttatttgttt atttaataag gaataattat
     1981 tattttaaaa gaaaaaaaat tgagtatcaa tttggtgaat tgttgtgtag agtgtttcct
     2041 actttaattt tgttaataca aataattcca tctttaagat tgctgtatta ttatggtttg
     2101 ataaatttag atagtaattt aacaattaag gttacaggcc atcaatgata ttgaagttat
     2161 gaatttaggg atatcccggg gttggaattt gattcttata taaagtcatt atatcaatta
     2221 aatttgggtg aacctcgttt gttagaagta gataatcgtt gtgtggtgcc ttgtgatact
     2281 aatattcgtt tttgtattac atcagcagat gtaattcatt cttgggcttt accatctata
     2341 tctattaagt tagatgcaat aaggggtatt ttaaggacgt tgtgttatag gtttccaatg
     2401 gtgggagttt tttatggtca atgttcagaa atttgtgggg ctaatcacag atttatacca
     2461 attgctgtgg aggttacttt gttggataat tttaagaggt ggtgctattt aaatatggat
     2521 taaagctact tagtttataa gaaaattttt gtttgtggta caaaagatgt agtagctata
     2581 aaatataagt gtataaattt aagtattgca tactgtttaa aaaaaatttt attgataata
     2641 aaatatagaa ttaaaaggta gaaaatatat tatttttatt gtttcataat aaaaattatg
     2701 tatattagag ttgaaaagtc gacttttaag atatctgttt tatttgttat attagaaaat
     2761 tatgtagttg tatgaaaggt ttataaaatg taaaagttga agtatttact agtttagttt
     2821 tataactgtt tataaaatat gtattatagt tagatataat taagtttatt aaaatattaa
     2881 taaattaatt gttttaaaat tagtaattgt aaaaattaat tattattaat taatataata
     2941 aaataactaa atagttaatc aactgttttt taaagactta gatttttaaa taaagtttgc
     3001 ttctgcccaa tgaaaaatta aatggcagtc ttagcgtgag gacattaagg tagcaaaata
     3061 atttgtgctt ttattgagtt ccagtatgaa tgaagttatt ggttaattat ctttttattt
     3121 tatatatgaa tttttagtta gtataaaaaa atattagtat aataatacaa agataagtct
     3181 tcggaaattc ttttttaatg ttaatattta tttattgata ttaaaaattt tctagggcag
     3241 aattttatat aatgtaatag tctattatta attataagaa ttactccgga gttaacagaa
     3301 aatcatatct aatctagtac ttatagtaag ataagtttta catcgatgtt gtatttaagt
     3361 ttattaaaaa aggagaaaat ttaagtttta agactgttct tcttttaatt aatttaacgt
     3421 gatattagtt taattcattg tgagatagaa ttgtttatct tgattattat taaatttaag
     3481 ttttaatagt acgaaaggaa aattaaaaaa agtttaaaac tttaaaagaa aataaatttt
     3541 cttaatgtta aatttgttat ttgtagctat atttgcagtt ttgttgttat taattttata
     3601 tataactaat tttgtaataa gagtaaaaaa aaatgattta ttaaaagttg gagcttttga
     3661 aagaggattt ctaagagtag gtaaaattca gaattcattt agtattcatt tttttgtaat
     3721 aatattaatg tttgtaattt ttgatttaaa aattataata tttttggggt tattaatttc
     3781 tgatattagt tcatctgtaa ggtttatgat gttaatatta tttatttttg gtggatttta
     3841 tatagaatga tgatacggta agttagtttg agtagtttaa gtaataaaga ttaatattat
     3901 ggtttatttg atgtctataa aatttgtagt tttaatgtta ataattttag ttgtgccgat
     3961 aataaagttg aggttaatat tattagagtg agatttttta tctattaagt ttaattttta
     4021 ttttaataga attttatttt catttatttt aggtttggta acattaagag tgctaatttt
     4081 tagtacttat tatttacata gtgagttaaa ttttaattat tattattttg tattattaat
     4141 ttttgttggt agaatattta tattaaatta tagtagaagt atttttacta tattgttaag
     4201 ttgagatttg ttgggtatct caagtttttt tttggtttta ttttataata attgagacag
     4261 gaattcaggt gctataaaca cagctttaac gaatcgtatt ggtgattttt ttatttttag
     4321 tttttttagg ggtattttgt tttatggtta ttatttttta agatttgaaa tattatgtag
     4381 atttatagtt ttattattgt tattaacatc ttttactaaa agggctcagt ttccttttag
     4441 gagttggctc ccaaaagcta taagtgcacc tacacctgtc aggtcattgg tccatagaag
     4501 aacgttggtt acggctggtt tgattttgtt aataaatttt agtaaggtta taataagtaa
     4561 taatgttata attgttattt tattaattgg tttatttaca atgttttttt ctagagttgc
     4621 tgctatagtt gaagaagata taaaaaaagt agtagcatta agtactttat cacagatagg
     4681 gttttctata acaacgttgg gattaggtat aagatttgtg gcgtttattc atttggttag
     4741 gcatgctttg tttaaaaggt gcttgtttat acaagttggt tatattattc atagtaatta
     4801 tggtcaacag gatggacgtg gatatggaaa taatggaaat ttaccgattt ttatgcaatt
     4861 acagttatta attactttat tttgtttgtg tggtttggta ttctctagag gtatagtaag
     4921 taaagattta attttagaat tattttttat aaataattat ataataattt taagtcttat
     4981 gttttttatt tctatttttt taacttttgg ttatagatat cgtttgtgaa aaagtttttt
     5041 tttaagtttt agaaaagtag ttagagtttt tagaactact atggtaataa attttttaag
     5101 gttaggttta gtaatttttt caattatatt tttatgatga ttaaattata atttattagt
     5161 tataccaagt atatttttat atttagattt ttatgtaccg ctattatatg ttattttaat
     5221 tattttattt agctatataa tttttaaatt gttagttaaa gaatttgtat ataaattttt
     5281 ggtagattat ttagctaaaa atgtaattta taaagtaaaa aattttaagt ttatagataa
     5341 taatttaaat aaatttggtt atataggatt caatttttta gggttgtttg gaacgctttt
     5401 tacgggctat ataagtacat ttaagtataa taatttagtt attttaatat tttttttatt
     5461 tttgttttta taactaaatt atatattaat tattaaggga ctataattta agtttaaaat
     5521 atgtaatttg catttacaaa atttttagtt ctattttaat ttttattata ataaataata
     5581 tatataaata tattatatta aattaatata atatattata taaaaatgaa atcaaaatga
     5641 tttcttatta tataaaaaat aaaagtttaa acacaattaa ataattattt ataaatacga
     5701 ataatttgaa atgagtattt aattagaata atggtgattg tgtttaactt ttatttgtat
     5761 atatctaata tatatataat atattatata tataattatt atatttaaat gtgctagtta
     5821 tgttttaatt tcagtgttta gtttatataa aatataatat ttgggttatt aagatttaga
     5881 gtcactgact actgtacagg ttgtttagat ttaggttgtt tagattttaa tatgaacttt
     5941 tagtttaatt atagaatttt tcatttacac tgaaagggtt aaaagtttta ttttagtatt
     6001 ttttttaggt gtatcattgt taagtggtgt ttttagttat ataaatatag atcctataaa
     6061 aagtagattt tttttaattt tgagaatgtt aacttgtatg ccaatattgt catttagtgg
     6121 ttatgtgtga ttttcatatt ttatttgttt gttgtttttg agaggtattt ttgtaatttt
     6181 agtatatttt tctagattat ctaaaattaa tttggtaaaa agatacttag tgtttttgag
     6241 attattatta agattgttgg ttattaagat ttcttatagc aatatgtttt ttaatgttag
     6301 tttaaatgta ttttattata gaattttttg attattaatt atttatattt tagtaatttt
     6361 attgtttttt ataaatttta caagttattt tttaaatttt tcgggggctt tacgaaaagt
     6421 ttaaataagt atcttaaaat tatttttata tttattagat tgtttatgtt attttttaag
     6481 tggcatcgtt ttatttttat tttaattgct ttagaattta taataataag tttatttata
     6541 aagtttatag gtttaataac tgaaataata tttttttatt ttatatgttt ttcggttatt
     6601 tctagaattt tgggaataat tgtaatggtg gggggtataa aattttatgg aagtgatcaa
     6661 tgtatttatt agtaattcaa atagatttaa gttaagttaa actattaatt ttcaaaatta
     6721 aaaatattta atctatatga ggttttagta taaaatagta taatttattt tcaataaata
     6781 ggttaaaatc ttatttaaag tatactttta tagatttaaa atttgattat ggtttaaaaa
     6841 tagttaaaat agtattattt aattataatt tataaagtgg gcaataaaaa tttaccccgg
     6901 taattattta tttgtaaaat acttgttcca gaataatcgg ctaggcttgt agaaatttaa
     6961 actttatttt gttttaatta taattattga atggtaaaat ttattaaatc ataggttaaa
     7021 ttttgatttt ttaaaataat gggttataat tttaaatttt gataaacgaa ttagattagt
     7081 acctaattag acaaaaatta aaaaagcaga agtaaagtag aatttaaact gaaagaatat
     7141 tggcagattt tctaaattat ctttggaggt tgagtaataa ttgagaaccc tcattaacta
     7201 cttttatatt tgtctcatgt atgatcgttt attttatact taaggtatat aataaaaaat
     7261 atttatttaa taaaatagat atatacttgg tttatgtagg agtaaaattt gacctacaat
     7321 aattaaagtt gtggatttgt gttagtgata gcacaataaa aatgtaaaag acagtaagaa
     7381 aatttttatg attttttgaa gattatttag atgtggtaca aatcatccat caattgccta
     7441 aaggggagta agttgtagta aagtagattt aggggaacct taatctagta aataaactat
     7501 taataatttt gttttgaaaa caaaatgtat gatttatctt gtagttttaa gggttagttt
     7561 aagttaagaa ttgttggctg ttaatcaaaa ggtttactct taaaagaatg tagtttaaat
     7621 aaaaatatta aattgtaaat ctaaagttaa gattcttgtt gtttttaaat tttttgataa
     7681 ttattttaat aatggtattt attttacagg caattgcttt tgttacttta cacgagcgtc
     7741 atttattggg taggagaaaa aatcgtttgg gacccactaa ggtaagtttt atgggggtat
     7801 tacaggcttt attggatggt cttaaattat tagaaaaaga gcaaataata cctttaaatt
     7861 cttctaattt atctttttta ttggtgctgg gtatttcttt tgtagtaata tatctggagt
     7921 gatatgtgtt gccttatttt ttcgactttt taagttttga gtattcgtta atgttttttt
     7981 tgtgtttaat tggtttttca gtttatacaa cgttaattag agggattgtt agtaaatcta
     8041 aatacgggat tgtaggtgct ttgcgtgctg gaagacagag tgtttcttat gaaattgctt
     8101 tttctttgta tttattagct gttataattc attatagtat atttagtttt gttaagaagt
     8161 ttactttgag tttattaatt atttatttac catttttggt aatgattatt ggtgaattaa
     8221 gtcgggctcc ttttgatttt gttgagggag aaagagagtt agtgagtggt tataatgttg
     8281 agtatactag tgtggctttt gtatcgttgt ttttgaggga gtacggaagt ttaatttttt
     8341 ttagggtaat aacttcaata ttgtttttta aattttctat agttgtaaga tttgtaatat
     8401 tttctgttat tgtttttatt cgtagatctt atccgcgttt tcgttatgat aaaataataa
     8461 ctatgttttg atttaagttt ttacctattt cgttaatttt tttattttat ttttttgcat
     8521 tgtttgttta aattttgtaa ttaatcaagt gtttttatta gatattttta tatttgtgtt
     8581 tttattacag tttatattat attttaagga gggtataata aatagattag taaaaaagtt
     8641 tttaggaggt ttagtaaatg tttttaggta tagtaatgtg ttacctataa gatcaattat
     8701 ttcttttttt acttttattg ttttgttaat ttgttgtttt ggaggatatt ttacttattc
     8761 tttttgtcct tgtggaatag tggagtttac ttttgtatat gctatagtgg cgtgaataag
     8821 aacgttgtta acttttattt ctagagaaaa gttttcagtt tatataagaa aagggggtga
     8881 tagttattta aaaacattta gtatattgtt agtagaaatt gtaagggagt tttcacgtcc
     8941 attagcttta actgtacgtt taacagtcaa tattatagtt ggacatttaa ttagtataat
     9001 attatatatg ggaattgaaa gttatatagg tgagaaatat gtgtggttaa gtattttagc
     9061 tattataatg gaatgttttg tattttttat tcaaagttat attttttcgc gtttaattta
     9121 tttgtatttg aatgaataaa gttgtattgt tggggtgtta acttaaaatt aaagtgtcaa
     9181 atttttaatt tgaaaatgta ttatgcacat ctcagttgta atttaaagta aaattttgta
     9241 tatttattgt taatatagca taagaagtgc atttgtttta agcgcaaaag atataagtta
     9301 actaatgagt ttaatacaag tcttctaaat ttgttttagg ttataacctg ctcatttttg
     9361 tatttttttt taataatatt agttgtattt ttaagtttat taagattaat tacaaataat
     9421 atgctggtgt ggtgaagtgt atttttgtta ataactttag tattttttat gttaaataaa
     9481 aagactaata gttttagaag tttattcaat tattttgtta tgcaggaaag tttgggtttg
     9541 ttgtttttaa tattttcata taggtatttt cagttattaa ttgtaatgtt taagattggt
     9601 atagctcctt ttcatttttg aatttttagt gttacgaatg gtgttgtagg ttttaattta
     9661 atatgatttt taacatttca aaaattacct tttttattaa ttttcttgca aataataatt
     9721 tttaagttag tttttgtgtt aatgttaggt ttattttttt gtttgtttca aatattgtta
     9781 ataaaaactt ataaaaattt attaatttta tcatctacag agtcatttaa ttgaattaca
     9841 ttagggtttt tagtgtcttt ttttaatgta atatttattt ttgtttatta ttttgtttta
     9901 ataatatttg ttatttctaa gtttgaaata ataaatgtta ataatttaat tggttgagaa
     9961 actatacttg tatttataaa tttacccttt agagttaatt tttttgtaaa gattttttct
    10021 ttaagggaaa ttttaaaaat acaaggggtg ggtattttgt tattactgtt tataatgttt
    10081 ttttcagctt tatcattaag tttttgaata gtaaatttaa gaacaaaata ttataaaatg
    10141 tttaaatata ataaaaatat ttttatattt gttgtcccaa taacaattat aatcttggta
    10201 taatatttta ataaatttca ttagtaaaaa ttattatgtt atcttgataa ggtaaagttc
    10261 ttaggatgaa ataaaatgtt aaatagattt actatgtttg attacggttc aaaaagatta
    10321 acattttagt taatgagtgt aatttagttt agatagaata tttatttttg gtgtaaaagg
    10381 gtttaattac aaaaagttgt gtaatttcat tagtttaatt ttaaaatata actctgaaga
    10441 ggttaagatt tatgaaataa taaaaaataa aaacaatatt ataaattttg ttacttcaat
    10501 gttagttacg ttacctacta gaaagagttt aacagtaggc tgaaattttg gaagaatatt
    10561 agggatagtg ttaatatttc aaattttaac gggtactttt ttagcttttt attatacagc
    10621 tgatggaagt gcagcttttg gggctgtaca gtatattata tatgaagtta attatggttg
    10681 aatttttcgt ttatttcatt ctaatggtgc tagattattt tttatttttt tgtatttaca
    10741 tatttttaaa gctttattta taataagata tcggataaaa catgtctggt catcaggttt
    10801 aactatttat ttattagtaa taatagaagc ttttatgggc tatgtattag tatgagctca
    10861 gataagtttt tgggcagcag tggttattac aagtttatta agtgtaattc ctgtgtgagg
    10921 tcctataatt gtaatatgaa tttgaagtgg ttttggggtt actagtgcta cattaaaatt
    10981 tttttttgta attcattttt tgttaccgtg aggtttatta gtgttaattc ttttacattt
    11041 aattttttta catagaacgg gcagaacgtc tagattgtat tgtcatgggg attatgataa
    11101 aattagtttt ggacctgaat tttggaataa agatgcatat aatgttgtat tttgattggt
    11161 atttgtagtt tttacattat tatatcctta tagtttaggt gatcctgaaa tatttattga
    11221 ggcggatcct ataataagac cagttcatat tgtaccagaa tgatattttt tgtttgctta
    11281 tgcaatttta cgtgcaatcc ctaataaagt tttaggagtt ttagctttat taataagaat
    11341 tgtaattttt tatttattta tttttattaa taattataca tcttgtttaa ataagttaaa
    11401 taagttttta gtttataggt ttattatttc ggcggtgttt ttgagttgat taggacaatg
    11461 tttggttgaa gagccttaca ctatattaag ccctgtattt tctgtaattt attttgtgtt
    11521 aattttaata attatgataa tgtattattt tagaaaaaaa ttatttattt aataggcata
    11581 aatagtatat aaaatatatt agatttaggt tctaaagaag tatagttatg ttatttatca
    11641 taattttcat attttaaggt tatcaagtta tgcttattat atattttttg cttcattagg
    11701 tataacaaga tctttagtaa ttttttttaa gtatggttta attgtacctt ttatttttag
    11761 gttgtttaca gtattattaa tttcttttgc ttgaggaaaa gatatttcta tggagggatt
    11821 gaggggttat cataattttt ttgtaataga tggttttaag tttggtgtag tattgtttat
    11881 ttttagagag tttatgtttt tttttagaat tttttgggta ttttttgatg ctgctttagt
    11941 gccagtacat gaattgggtg agagctggtc accaataggt atacatttgg ttaatccatt
    12001 tggtgtacca ttgttaaata caattatttt acttaggagt ggtgtttcgg taacttgagc
    12061 ccatcatagt ttattaagta acaaaaggtg tactaatagc atggtgttaa cttgtgtttt
    12121 agctgcatat tttactagaa ttcagttaat agaatataaa gaagcaagtt tttcaatttc
    12181 ggatggaatc tttggtagaa ttttttattt atctaccggg tttcatggaa ttcatgtgtt
    12241 atgtggcggg ttatttttag gttttaattt attacgtttg ttaaagtcac attttaatta
    12301 taatcatcat ttaggtatag agtttgcaat tttgtattgg cattttgtag atgttgtttg
    12361 gttattttta tttgtttttg tgtattgatg atcatattgc tatattagtt taaataaaga
    12421 atgtataact tgtaattata gggaattata tagctttggt agacttttta ttattaagtt
    12481 ttttattttt ttttaagcct ttaatatttt ttacatttat tattgtgttt aggtttgtat
    12541 tgtttaagaa tatttcatga agtggacttt tttatgtggt tgattcaaat gtatttattt
    12601 tattaatttt tataataatt tttatttatg gtatagtatt aattagtgaa aaaaatttta
    12661 atttaattat attaaggagg ttgttaatta taatttgttt gttttttttt atttcaagta
    12721 atatactaat actgtatata tattttgaat tatcaatatt tccaatttta atcataattt
    12781 taggttatgg ttcacaaatt gaaaagatta attcaagata ttatttatta ttctatgcag
    12841 ctatttgttc attcccattt ttgtttattt attataaaag aatattttat tttacgacat
    12901 gttattttga ttttaatatt tcatgagagt tgttttttat tttaagttta agatttataa
    12961 taaagtttcc tgtgtatttt ttacatttat gattaccgaa ggctcatgtt gaggccccga
    13021 ctacagcgag tatattatta gcgggtttat tattaaaatt gggtacagct ggttatttac
    13081 gaattttagg atcaataaat tttgtttata ataatttctg agtaactatt gctttaatag
    13141 ggataatttt ggcttctttt agatgtgtat ttcagagtga tgctaaatcg ttagcggctt
    13201 attcatctgt aacacatata agttttttat tgttaaccat aatttatatt ataataagta
    13261 gtaaaattgg aggattgata ataatattgg ctcacggtta tacttctact ttaatatttt
    13321 atgtaattgg tgaattttat catactacgt caacacgtat ggtgtatttt ttgaatagtt
    13381 tttttacatc cagtattttt ttcgggatta ttttttcatt ggtattcttg tcgaataggg
    13441 gtgtaccccc atcattatca tttttatcag aatttataat tattgtaaat agggttgtaa
    13501 ttagaaaaat attatttttt ataattttta tttattttat aatttcattt tattattcat
    13561 tatttttaat tgtatgtata ataatgggta aaaattatta tagttttaat aattttaata
    13621 ttggggttac tgtttcaaca gttattataa tatttaatat tttttgatta tcaatatttt
    13681 attaaatatt ataattatta gtataaaatg gataatataa tctatttccg attatatttc
    13741 cttttttttt aggttataag agtaaaattt ttattggaag gaaaaatctc ttat
//

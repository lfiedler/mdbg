LOCUS       NC_030528              14833 bp    DNA     circular INV 21-JUL-2016
DEFINITION  Fasciolopsis buski mitochondrion, complete genome.
ACCESSION   NC_030528
VERSION     NC_030528.1
DBLINK      BioProject: PRJNA328748
KEYWORDS    RefSeq.
SOURCE      mitochondrion Fasciolopsis buski
  ORGANISM  Fasciolopsis buski
            Eukaryota; Metazoa; Platyhelminthes; Trematoda; Digenea;
            Plagiorchiida; Echinostomata; Echinostomatoidea; Fasciolidae;
            Fasciolopsis.
REFERENCE   1  (bases 1 to 14833)
  CONSRTM   NCBI Genome Project
  TITLE     Direct Submission
  JOURNAL   Submitted (12-JUL-2016) National Center for Biotechnology
            Information, NIH, Bethesda, MD 20894, USA
REFERENCE   2  (bases 1 to 14833)
  AUTHORS   Ma,J., He,J.J. and Zhu,X.Q.
  TITLE     Direct Submission
  JOURNAL   Submitted (01-MAY-2016) Department of Parasitology Lanzhou
            Veterinary Research Institute, Chinese Academy of Agricultural
            Sciences, 1 Xujiaping, Yanchangbu, Lanzhou, Gansu Province 730046,
            The People's Republic of China
COMMENT     REVIEWED REFSEQ: This record has been curated by NCBI staff. The
            reference sequence is identical to KX169163.
            
            ##Assembly-Data-START##
            Sequencing Technology :: Sanger dideoxy sequencing
            ##Assembly-Data-END##
            COMPLETENESS: full length.
FEATURES             Location/Qualifiers
     source          1..14833
                     /organism="Fasciolopsis buski"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /db_xref="taxon:27845"
     gene            1..645
                     /gene="COX3"
                     /locus_tag="BBW50_gp01"
                     /db_xref="GeneID:28255077"
     CDS             1..645
                     /gene="COX3"
                     /locus_tag="BBW50_gp01"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="YP_009262357.1"
                     /db_xref="GeneID:28255077"
                     /translation="MSWLPISIAWVVMIGILSIFMWKLWSLFIFFLLVLLVVVWLIKE
                     SIYSDKHHAVGFWLFIISEVVAFGTLFCLCVMTVEDDIDSISSPLELPLLGCFVLTGS
                     SITVTTYHHFIGSAYSSPFLLLTIILGSSFIGLQIFEFYECGCDITFCVYDAVCFCTV
                     GLHFLHVLGGLVALSLLYVCGDGSVPQPNVDFVVWYWHFVDYIWLFVYLIIYLS"
     gene            657..724
                     /locus_tag="BBW50_gt01"
                     /db_xref="GeneID:28255065"
     tRNA            657..724
                     /locus_tag="BBW50_gt01"
                     /product="tRNA-His"
                     /db_xref="GeneID:28255065"
     gene            725..1834
                     /gene="CYTB"
                     /locus_tag="BBW50_gp12"
                     /db_xref="GeneID:28255078"
     CDS             725..1834
                     /gene="CYTB"
                     /locus_tag="BBW50_gp12"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome b"
                     /protein_id="YP_009262358.1"
                     /db_xref="GeneID:28255078"
                     /translation="MVSVLRGNLVDLPTNISLSYFWCGGFMISSFLVLQIVSGVILSF
                     LYVADSSMSFGCVLDFTSESFFLWLVRYMHVWGVTFIFLLFFIHMGRALYYSSYTKLG
                     VWNVGFVLYLIMMVEAFLGYILPWHQMSYWAATVLTSVLNSVPVVGGVLYKFVVGGFS
                     VSNVTLVRVFSAHVCLAFVIVGLSVIHLFYLHKSGSNNPLFVSGGYSDIVLFHPLFTN
                     KDGFVLVCLLFLCCLLMLLCPDMVLDVESYIHADPMVTPVSIKPEWYFLAFYAMLRSI
                     ESKVGGLVLVVVFLFILWLPSFNSSCSYSVIRQIVFWFGVSLFFLLSYLGACHPEFPF
                     VLISKVSSIGIVFLLGLFKGLWVVPYSGGFPRVMI"
     gene            1844..2116
                     /gene="ND4L"
                     /locus_tag="BBW50_gp11"
                     /db_xref="GeneID:28255066"
     CDS             1844..2116
                     /gene="ND4L"
                     /locus_tag="BBW50_gp11"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="YP_009262359.1"
                     /db_xref="GeneID:28255066"
                     /translation="MGGSLVSFLYIGGLLIIFGFFLSLGRLLNCLIVVENFNVLLLFV
                     CLLCQWDEFRIMFIALMVIFTIEVMLGLVVLTRLWDFSSLIGTVGV"
     gene            2077..3357
                     /gene="ND4"
                     /locus_tag="BBW50_gp10"
                     /db_xref="GeneID:28255067"
     CDS             2077..3357
                     /gene="ND4"
                     /locus_tag="BBW50_gp10"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="YP_009262360.1"
                     /db_xref="GeneID:28255067"
                     /translation="MGFQKFDWYCWCVGILGCVILFGLWLIGVVGVFILGIYSLEFGV
                     FVFDFVSFILCMLSLFLGVSIFFVCGDLSADSKIMLFLSIICSMLCYSCVNVLWFWVF
                     YEMSILPLLYLLICESPYSERFIASWYLLGYVVFSSLPMLLCVFYVGSLLGSYSFQAW
                     DIGFIQFNGFLVSIVLAVMFITKIPLPPFHVWLPIVHAEASSPVSMCLSGYIMKLGLL
                     GVLRFCYGLLSDYIFSSVYVMAGLGISLLFFMSACRELDGKRWLAFLSLSHIVVVAVC
                     FSVCSFEGVLLSLVYSLGHGLSAGVVFLLLWMIYEVSGSRNWLVLKGCVSSSLLIRCL
                     LVVCICTTASIPPTLNFFSEIVILCEGGYMYGGLCVILLSLYLFAGGLVPVFLVGCLL
                     SRHYSIGFGCGYVNSYVGGLVLLGVWCYLLFLVL"
     gene            3363..3426
                     /locus_tag="BBW50_gt02"
                     /db_xref="GeneID:28255068"
     tRNA            3363..3426
                     /locus_tag="BBW50_gt02"
                     /product="tRNA-Gln"
                     /db_xref="GeneID:28255068"
     gene            3437..3500
                     /locus_tag="BBW50_gt03"
                     /db_xref="GeneID:28255079"
     tRNA            3437..3500
                     /locus_tag="BBW50_gt03"
                     /product="tRNA-Phe"
                     /db_xref="GeneID:28255079"
     gene            3525..3592
                     /locus_tag="BBW50_gt04"
                     /db_xref="GeneID:28255080"
     tRNA            3525..3592
                     /locus_tag="BBW50_gt04"
                     /product="tRNA-Met"
                     /db_xref="GeneID:28255080"
     gene            3593..4111
                     /gene="ATP6"
                     /locus_tag="BBW50_gp09"
                     /db_xref="GeneID:28255081"
     CDS             3593..4111
                     /gene="ATP6"
                     /locus_tag="BBW50_gp09"
                     /codon_start=1
                     /transl_table=9
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="YP_009262361.1"
                     /db_xref="GeneID:28255081"
                     /translation="MFFSRLSVVFGYVRSLVLGGWGDYFYRLCLFFLLLSFLFLRIPY
                     IFGVLGFGLFLILVISPMFLSLFLGRLLDQGPFVFFSGFVPSGTPLWIAPFVCLAETL
                     SYVVRPIILMIRPFVNLTIGAMGGVALGAMSLEFGVWVLLFMVVLFFYEIFVALVHWF
                     IVCNILSFSEEH"
     gene            4123..4995
                     /gene="ND2"
                     /locus_tag="BBW50_gp08"
                     /db_xref="GeneID:28255069"
     CDS             4123..4995
                     /gene="ND2"
                     /locus_tag="BBW50_gp08"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="YP_009262362.1"
                     /db_xref="GeneID:28255069"
                     /translation="MSRGIAVSSFGILGLVCFSLLIFSSVNLSFFWLFLELSTLSVVP
                     LFFVWSDYNCLPGLFSYIIASGVSSAFILCGVLSSDLLFVLIIGLLLKFGLFPLWGWV
                     YSVSLCSNWLVVWSMSTFLKAPVFFLPFFLSSGGYFLVSVLCCLSLLILSGLFWFYSF
                     GWFYCWCHMMLSSSAVLVSMSFVLSSDILLFIFIVYCFWCSFVILFFSVCSSIGGLEG
                     GVSSYFMFCFLLLSFPISLSLFYKLVIVFGVFSCWFPVLFCWVVYSISEQVYLVKFVV
                     SYDLPKSIFGLIFE"
     gene            5003..5065
                     /locus_tag="BBW50_gt05"
                     /db_xref="GeneID:28255070"
     tRNA            5003..5065
                     /locus_tag="BBW50_gt05"
                     /product="tRNA-Val"
                     /db_xref="GeneID:28255070"
     gene            5081..5143
                     /locus_tag="BBW50_gt06"
                     /db_xref="GeneID:28255082"
     tRNA            5081..5143
                     /locus_tag="BBW50_gt06"
                     /product="tRNA-Ala"
                     /db_xref="GeneID:28255082"
     gene            5145..5210
                     /locus_tag="BBW50_gt07"
                     /db_xref="GeneID:28255083"
     tRNA            5145..5210
                     /locus_tag="BBW50_gt07"
                     /product="tRNA-Asp"
                     /db_xref="GeneID:28255083"
     gene            5213..6115
                     /gene="ND1"
                     /locus_tag="BBW50_gp07"
                     /db_xref="GeneID:28255084"
     CDS             5213..6115
                     /gene="ND1"
                     /locus_tag="BBW50_gp07"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="YP_009262363.1"
                     /db_xref="GeneID:28255084"
                     /translation="MVLGFELVYLVFSSVLAFVIIMVFVAFFILGERKILGYMQIRKG
                     PNKVGLWGLLQSFADLLKLVIKFKFVFFQNRSWLSWWGVYLLVLLSCGYCIFFFEGMS
                     GLVCSNYMLWFLVLTSMTGYSLLSVGWGCYNKFSLLSCVRSAFGSVSFEACFMCVVLI
                     LGVLWGGYGVVVFFDYFGAVFLVFPLVYGLWLVGILCECNRTPLDYAEAESELVSGLN
                     MEYCNVPFTCLFACEYLIMVVFSWLSSLFFWGGWFLLGMSLVHVVFFVWARATLPRVR
                     YDYFVEFMWRYVLLILVFSLFCIL"
     gene            6117..6193
                     /locus_tag="BBW50_gt08"
                     /db_xref="GeneID:28255071"
     tRNA            6117..6193
                     /locus_tag="BBW50_gt08"
                     /product="tRNA-Asn"
                     /db_xref="GeneID:28255071"
     gene            6197..6266
                     /locus_tag="BBW50_gt09"
                     /db_xref="GeneID:28255085"
     tRNA            6197..6266
                     /locus_tag="BBW50_gt09"
                     /product="tRNA-Pro"
                     /db_xref="GeneID:28255085"
     gene            6267..6329
                     /locus_tag="BBW50_gt10"
                     /db_xref="GeneID:28255086"
     tRNA            6267..6329
                     /locus_tag="BBW50_gt10"
                     /product="tRNA-Ile"
                     /db_xref="GeneID:28255086"
     gene            6332..6399
                     /locus_tag="BBW50_gt11"
                     /db_xref="GeneID:28255087"
     tRNA            6332..6399
                     /locus_tag="BBW50_gt11"
                     /product="tRNA-Lys"
                     /db_xref="GeneID:28255087"
     gene            6400..6756
                     /gene="ND3"
                     /locus_tag="BBW50_gp06"
                     /db_xref="GeneID:28255088"
     CDS             6400..6756
                     /gene="ND3"
                     /locus_tag="BBW50_gp06"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="YP_009262364.1"
                     /db_xref="GeneID:28255088"
                     /translation="MLFFFCLGVLFMMVFVLVVVFHMFLWNLDLNVFSGERSWVSSFE
                     CGFLSQRLVENPFSYTYFILLVFFVVFDLEVSLLLNMPYQGVLYKNLLCYLFFFALVG
                     FGFLVEIRRGYVSWIY"
     gene            6764..6823
                     /locus_tag="BBW50_gt12"
                     /db_xref="GeneID:28255072"
     tRNA            6764..6823
                     /locus_tag="BBW50_gt12"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:28255072"
     gene            6838..6902
                     /locus_tag="BBW50_gt13"
                     /db_xref="GeneID:28255089"
     tRNA            6838..6902
                     /locus_tag="BBW50_gt13"
                     /product="tRNA-Trp"
                     /db_xref="GeneID:28255089"
     gene            6906..8447
                     /gene="COX1"
                     /locus_tag="BBW50_gp05"
                     /db_xref="GeneID:28255090"
     CDS             6906..8447
                     /gene="COX1"
                     /locus_tag="BBW50_gp05"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="YP_009262365.1"
                     /db_xref="GeneID:28255090"
                     /translation="MSYFGWLFTLDHKRIGFVYLVVGLWSGFLGLSLSMLIRLNYLDP
                     YYNLISPEVYNYLVTGHGIVMIFFFLMPVLIGGFGNYLLPLLLGIGDLNLPRVNALSV
                     WLLLPSCVCLALSLIGGAGVGWTFYPPLSASGYSGWGVDFMMFSLHLAGVSSIFSSIN
                     FICTIVEVMFEEGTGRLSILVWAYLFTSILLLLSLPVLAAAITMLLFDRSFGSAFFDP
                     MGGGDPVLFQHLFWFFGHPEVYVLILPGFGVISHICVTLTNNDSLFGYYGLVLAMAAI
                     VCLGSIVWAHHMFMVGLDVHTAVFFSSVTMVISIPTGIKVFSWLIMLGGGSFVRIWDP
                     IVWWIVGFVVLFTIGGVTGIMLSASILDTLLHDTWFVVAHFHYVLSLGSYSSVVISFI
                     WWWPIITGYSLNVSLLQGHWIVSMFGFNLCFFPMHYLGMSGLPRRVCVYDPDFYWLNV
                     LSSLGGVVSTVSAFFLFFVLWESCVIGNRVLSVWGSGSLILNVVTLPSPQHNGYMTGG
                     CRWCA"
     gene            8478..8537
                     /locus_tag="BBW50_gt14"
                     /db_xref="GeneID:28255073"
     tRNA            8478..8537
                     /locus_tag="BBW50_gt14"
                     /product="tRNA-Thr"
                     /db_xref="GeneID:28255073"
     gene            8537..9525
                     /locus_tag="BBW50_gr02"
                     /db_xref="GeneID:28255091"
     rRNA            8537..9525
                     /locus_tag="BBW50_gr02"
                     /product="large subunit ribosomal RNA"
                     /db_xref="GeneID:28255091"
     gene            9533..9597
                     /locus_tag="BBW50_gt15"
                     /db_xref="GeneID:28255092"
     tRNA            9533..9597
                     /locus_tag="BBW50_gt15"
                     /product="tRNA-Cys"
                     /db_xref="GeneID:28255092"
     gene            9597..10371
                     /locus_tag="BBW50_gr01"
                     /db_xref="GeneID:28255093"
     rRNA            9597..10371
                     /locus_tag="BBW50_gr01"
                     /product="small subunit ribosomal RNA"
                     /db_xref="GeneID:28255093"
     gene            10371..10964
                     /gene="COX2"
                     /locus_tag="BBW50_gp04"
                     /db_xref="GeneID:28255094"
     CDS             10371..10964
                     /gene="COX2"
                     /locus_tag="BBW50_gp04"
                     /codon_start=1
                     /transl_table=9
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="YP_009262366.1"
                     /db_xref="GeneID:28255094"
                     /translation="MVSINVLYMDLITYVLIICAFIPVWVFVVLGWQLFSSNTVASHN
                     NESDVVEFFWTVVPSTCVIALCYYNLNCISYDMLDIPSKIVKVVGRQWYWTYEVEGEG
                     EEYDSVMSDFVSGVDKPLRMIRGSFYQFMVTSSDVIHSFSVPEFNIKSDAIPGRLNYI
                     SYCPAQLGVYVGYCTELCGVGHAYMPIVIEVVMPEES"
     gene            11020..11472
                     /gene="ND6"
                     /locus_tag="BBW50_gp03"
                     /db_xref="GeneID:28255074"
     CDS             11020..11472
                     /gene="ND6"
                     /locus_tag="BBW50_gp03"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="YP_009262367.1"
                     /db_xref="GeneID:28255074"
                     /translation="MLVSILIGFYFTCIWGFSLISHPVVYCILLLGAALSISGLSYLV
                     LGFSWYLAIFCLVYVGGVYVLFIFVSIHNPNPLPNISGSVIGLFVLLCAFLCLFSFAF
                     FSFPSLGDESFYLCSFFEGVSYCLFCLVLMLGFICVSVVVSSKNLFFR"
     gene            11479..11535
                     /locus_tag="BBW50_gt16"
                     /db_xref="GeneID:28255075"
     tRNA            11479..11535
                     /locus_tag="BBW50_gt16"
                     /product="tRNA-Tyr"
                     /db_xref="GeneID:28255075"
     gene            11542..11604
                     /locus_tag="BBW50_gt17"
                     /db_xref="GeneID:28255095"
     tRNA            11542..11604
                     /locus_tag="BBW50_gt17"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:28255095"
     gene            11606..11665
                     /locus_tag="BBW50_gt18"
                     /db_xref="GeneID:28255096"
     tRNA            11606..11665
                     /locus_tag="BBW50_gt18"
                     /product="tRNA-Ser"
                     /db_xref="GeneID:28255096"
     gene            11682..11746
                     /locus_tag="BBW50_gt19"
                     /db_xref="GeneID:28255097"
     tRNA            11682..11746
                     /locus_tag="BBW50_gt19"
                     /product="tRNA-Leu"
                     /db_xref="GeneID:28255097"
     gene            11744..11810
                     /locus_tag="BBW50_gt20"
                     /db_xref="GeneID:28255098"
     tRNA            11744..11810
                     /locus_tag="BBW50_gt20"
                     /product="tRNA-Arg"
                     /db_xref="GeneID:28255098"
     gene            11809..13380
                     /gene="ND5"
                     /locus_tag="BBW50_gp02"
                     /db_xref="GeneID:28255099"
     CDS             11809..13380
                     /gene="ND5"
                     /locus_tag="BBW50_gp02"
                     /codon_start=1
                     /transl_table=9
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="YP_009262368.1"
                     /db_xref="GeneID:28255099"
                     /translation="MLLIFLGIFGLYVFWYVGQLYWLGNFSVLAHDSFSSFGFMLDDV
                     SMGCVFMLLCCGGVALVYCFHYFSGGVEGSLLFPLMVWFLGVMCILVFSSSLVFSLIF
                     WEYLGLVSFFLILFYSNSSSLRASLITLFASRFGDVSMFVIIMCVSWWIGTSGAGFIF
                     LYLLVVLTKSAGFPFISWLLEAMRAPTPVSSLVHSSTLVAAGVWFVLRYSSYSSWWLD
                     FFLVYFCLLSIIITAVAATVFMDLKKIVALSTCNNVSWCLLFFVCGDVFLALLQLITH
                     GLCKCYLFMSVGDLMCQSGSSQSSVGVYLGRYSGVYLPVIQSFLVLSLCGLPFLGVFF
                     SKHCFFSGLLGVMSLGGCLLFLICLFLSYVYSVRFVLLLVGSVGGLSFGYFSGFMIIC
                     PLVVLGSVLNFLVSLGTVEVISLSSIWSVVVLSLQGVGSLVGWWFYYLCMGYGRWSSL
                     VWGSEGFVSLFYSWFLVLSSVCVSSFYRWEIYFLDMFFSLGSVYRWSFYRGSLFSLSF
                     MILGMMGVFFASYIF"
     gene            13384..13447
                     /locus_tag="BBW50_gt21"
                     /db_xref="GeneID:28255076"
     tRNA            13384..13447
                     /locus_tag="BBW50_gt21"
                     /product="tRNA-Gly"
                     /db_xref="GeneID:28255076"
     gene            13458..13519
                     /locus_tag="BBW50_gt22"
                     /db_xref="GeneID:28255100"
     tRNA            13458..13519
                     /locus_tag="BBW50_gt22"
                     /product="tRNA-Glu"
                     /db_xref="GeneID:28255100"
     misc_feature    13520..14833
                     /note="AT-loop"
ORIGIN      
        1 atgagttggt tacctattag tatagcttga gtggtgatga ttgggatttt gagtatattt
       61 atgtgaaagt tgtggagttt gtttatattt tttttgttgg ttttattagt ggtagtttgg
      121 ttaattaagg agtctattta ttctgataag catcatgcgg taggtttttg gttgtttatt
      181 attagtgagg ttgtggcttt tggtactctt ttttgtttgt gtgtaatgac ggtggaggat
      241 gatattgatt ctatttctag tcctttagag cttcctttgt taggttgttt tgttttgacg
      301 ggttcttcta taactgtgac tacttatcat cattttattg gttcggctta tagatcgccg
      361 tttttgcttt tgactataat tcttggtagg aggtttatag gtttgcagat ttttgagttt
      421 tatgagtgtg gttgcgatat aactttttgt gtgtatgacg ctgtttgttt ttgtacggtg
      481 ggtttacatt ttttacatgt tttagggggt ttggttgcgt tgagtttatt gtatgtttgt
      541 ggtgatggta gtgttcctca acctaatgtt gattttgtgg tttggtattg acattttgtt
      601 gattatattt ggttgttcgt ttacttgata atctatttgt cttaggtgtt ttatggtctt
      661 ctgtaggtta ttttgataaa tcgttagttt gtggtactga agaattcgtt tatgaatgga
      721 ggagatggtt tctgttttgc gtggtaattt ggtggatttg cctactaata tttcgttgag
      781 ttatttttga tgtgggggtt ttatgattag taggtttttg gtgttgcaga ttgtttctgg
      841 ggtaattttg tcttttttgt atgttgcaga tagttctatg aggtttggtt gtgttcttga
      901 ttttacttct gagaggtttt ttttatggct tgttcgatat atgcatgtgt gaggggttac
      961 ttttattttc ttgttatttt ttatacatat gggtcgtgct ttgtattatt cgagttacac
     1021 taagctgggg gtttgaaatg ttggatttgt tttgtatttg attatgatgg ttgaggcttt
     1081 tttgggttat attttgcctt gacatcagat gtcttattgg gctgctactg ttttgacttc
     1141 tgtgctcaaa agagtaccgg tggttggtgg tgtgttgtat aagtttgtgg ttggggggtt
     1201 ttctgtgagt aatgttactt tagttcgtgt tttttctgcc catgtttgtt tggcttttgt
     1261 tattgtaggg ttgagagtta ttcatttgtt ttatttacat aagaggggtt ctaataatcc
     1321 gttgtttgtt tctggaggtt atagtgatat tgtgttattt catccgttgt ttactaataa
     1381 ggatggattt gttttggtat gtcttttgtt tttgtgttgt ttgttaatgt tattgtgtcc
     1441 ggatatggtt ttagatgttg agagttatat tcatgcggat cctatggtta caccggtttc
     1501 tataaagcct gagtggtatt ttttggcgtt ttatgctatg ttgcgttcta tagagtctaa
     1561 ggtgggaggt ctggttttag tggttgtgtt tttgtttatt ttatggttac cgtcctttaa
     1621 aagttcgtgt agttatagag ttattcgtca gattgttttt tggtttggtg tttctctatt
     1681 ctttttattg agttatttag gtgcttgtca tcctgagttt ccttttgtat tgattagtaa
     1741 ggtttcgaga attggtattg tttttttatt agggttgttt aaggggttgt gggttgttcc
     1801 ttattctggc ggttttcctc gtgttatgat ttagattgag ttaatgggag gtagtttagt
     1861 taggttttta tacataggtg ggttgttgat aatttttggt ttttttttgt ctttgggtcg
     1921 tttacttaat tgtttgattg ttgtggagaa ttttaaagta ttattattgt ttgtgtgtct
     1981 tttgtgtcag tgagatgaat ttcggattat gtttattgct ttaatggtga tttttactat
     2041 tgaagttatg ttagggttgg tagttttgac tcggttatgg gatttcagaa gtttgattgg
     2101 tactgttggt gtgtaggtat tttgggttgt gttattttgt ttggtttatg gttgataggt
     2161 gttgttggtg tgtttatatt gggtatatat tcgttggaat ttggagtctt tgtgtttgat
     2221 tttgttaggt ttatactttg tatgttgtct ctatttttag gtgtttctat atttttcgtt
     2281 tgtggtgatt tgagtgcgga ttctaagatt atgttgtttc ttagaattat ttgttctatg
     2341 ctttgttata gctgtgttaa agttttatga ttttgggttt tttatgagat gtctatactt
     2401 cctttgctct atttgttgat ttgtgagtct ccgtattctg agcgttttat tgcttcttga
     2461 tatttgttag gttatgttgt gtttaggagt ttgcctatgt tactttgtgt tttttatgtt
     2521 ggtagtttgt taggtagtta taggtttcaa gcctgggata taggttttat tcaatttaaa
     2581 gggtttttgg tgtctattgt tttagctgtg atgtttatta ctaagatacc cttgccccca
     2641 tttcatgttt ggttgcctat agttcatgct gaggccagta gtcctgtttc tatgtgcctt
     2701 agcggctata ttatgaagtt ggggttgctt ggtgttttgc gtttttgtta tgggttgttg
     2761 tcggattata tttttagttc tgtttatgtg atggcgggtt tagggatttc tttacttttt
     2821 tttatgtctg cttgtcgtga gttggatggt aagcgttggt tagcgttttt aagactttct
     2881 catatagttg ttgttgcggt ttgttttagg gtatgtagtt ttgagggggt tttattgtct
     2941 ttggtttatt ctctgggaca tggtttgtct gctggggtgg tttttttatt gctttggatg
     3001 atttatgagg tttctggtag acgtaattgg ttggttttaa agggttgtgt ttctagaagt
     3061 ttgttgattc gttgtttact ggttgtatgt atttgtacta cagcgtctat acctccgaca
     3121 ttgaattttt tttctgagat tgtaatattg tgtgagggag gttatatgta tggagggttg
     3181 tgtgttattt tactgtcatt gtatttgttt gcggggggcc ttgttcctgt gtttttagtt
     3241 ggttgtttgt tatctcgtca ttattctata ggttttggtt gtggttatgt taatagttat
     3301 gtgggaggtc tggttttatt aggggtttga tgttatttac ttttcttagt gttataattt
     3361 gttgtaacaa ggtgttattt gcatattatg ttttggtcgt aaaggtaatt ggttagttgg
     3421 ttacaggttt attacttcct ttctagctta agtttaaagt gatagtttga agagctatag
     3481 ttacgtgaat gtggtaggag tttattttat ttatagaggg gaaggggaga ggtaagttaa
     3541 tattttataa actgtgtggt tcatgttcat gtaatgcgtt ttgccctctc ttatgttttt
     3601 ttctcgattg tcggtggtgt ttggttatgt gcggagtttg gtgttaggag gttgaggaga
     3661 ttatttttat cgtttgtgtt tgtttttttt gttgttaaga tttttgtttt tgcgtattcc
     3721 gtatattttt ggtgtactag ggtttggatt atttttgatt ttggtaatta gtccgatgtt
     3781 tttatctcta tttttaggtc gtttattaga tcaggggcct tttgtatttt ttagtgggtt
     3841 tgttccgtca ggcacacctt tgtgaattgc tccgtttgtt tgtttagctg aaacattgag
     3901 ttatgtggtt cgtcctatta ttttgatgat tcgtcctttt gttaatttga ctataggggc
     3961 tatgggtggt gtagctttgg gagcaatgag tttggaattt ggagtttggg ttttgttatt
     4021 catggttgta ttattttttt atgaaatttt tgtagcgttg gtgcattggt ttattgtttg
     4081 taatattctt tctttttcag aggaacacta gttatattat ccatgagtcg tggtattgct
     4141 gtgaggagtt ttgggatttt aggtttggtt tgttttaggt tgttaatttt ttcttctgtg
     4201 aatttatcat ttttttgatt atttttagaa ttatctactc tctcggttgt tcctttattt
     4261 tttgtttgga gggattataa ttgtttgcct ggtttattta gttatataat tgcttcgggc
     4321 gtttcttctg cttttattct ttgtggggtt ttaagcagag atcttttgtt tgtattgata
     4381 atagggttgt tattaaagtt tgggttattt ccactttgag gttgggttta tagggttagg
     4441 ctatgttcta actgattggt agtttgatct atgtctactt ttttaaaggc tcctgtcttt
     4501 ttccttcctt tctttttgtc tagggggggt tattttttgg ttagtgtttt gtgttgttta
     4561 agattgttaa ttttgtctgg tttgttttgg ttttatagtt ttggttggtt ttattgttgg
     4621 tgtcatatga tgttgtcttc tagggctgtt ttggtttcta tgtcttttgt tttgtcttct
     4681 gatattttgt tgtttatttt tattgtttat tgtttttggt gttcttttgt gattttgttt
     4741 tttagagttt gtagtagaat aggaggattg gaaggtgggg ttagttctta ttttatgttt
     4801 tgttttttgt tgttatcttt ccctatttct ctttctttat tttataagtt ggttattgtt
     4861 tttggtgttt ttaggtgttg gtttcctgtt cttttttgtt gggtagttta tagtatttct
     4921 gagcaggttt atttggttaa gtttgttgtg agttatgatt tgcctaagag tatttttggt
     4981 ttgatctttg agtagttttg tggataaggt agtttaattt aaaatttcta tcttacacgt
     5041 agaagatgga tttgttctct tattatgtgt ttgttgtttt aacgatatag tttaatatta
     5101 gatgtttgtt ctgcgaacat tcggtagagg tttcttgtcg ttattcatct ctagtttatt
     5161 ttgagaatgt taacttgtcg tgttattggt gtgtctttgt gtgcgaggtg aggtggtttt
     5221 aggttttgag ttggtttatt tggtttttag tagggttttg gcatttgtta taattatggt
     5281 ttttgtggct ttttttatac ttggagagcg taagattttg ggttatatgc agatacgtaa
     5341 gggtcctaat aaggtggggt tgtggggttt gttgcaaagg tttgctgact tgttaaagtt
     5401 ggttattaag tttaagtttg ttttttttca gaatcgtagt tgattgtctt gatgaggtgt
     5461 atatttatta gttttgttgt cttgtgggta ttgtattttt ttttttgaag ggatgtctgg
     5521 tttggtttgt agtaaatata tgttgtggtt tttggtttta actaggatga ctgggtatag
     5581 tttgttgagt gttggttggg gttgttataa taagttttct ttgcttagtt gtgttcggtc
     5641 ggcttttggg tctgttaggt ttgaggcttg ttttatgtgt gtagttttga ttttgggtgt
     5701 actttggggt ggctatggtg ttgtggtttt ttttgattat tttggggctg tttttttggt
     5761 atttcctttg gtttatggtt tatggcttgt gggtatttta tgtgaatgta atcgtactcc
     5821 tcttgattat gcggaggctg agagtgagtt ggttagaggt ttgaatatgg agtattgtaa
     5881 agttcctttt acttgtttat ttgcttgtga gtatttgatt atggttgttt tttcttggct
     5941 atcttctcta tttttttggg gaggatgatt tttgttaggt atgtccttag ttcatgttgt
     6001 tttttttgtt tgggctcgtg ctactttacc tcgtgttcgt tatgattatt ttgtagagtt
     6061 tatgtggcgt tatgttcttt tgattttggt tttttctttg ttttgtattc tttaatgtgg
     6121 tttgtggtgc gtgtaggtta tgtttttaaa tcgtaaggct gttaaccttg agaagatgtt
     6181 tgttctgcgg acggttcagc attgtagttt aagtttttag aatcttaatt ttggggatta
     6241 ggggtctctt gtttggagtt ggctggccgg tagggctgct ttagcaggtt gctgtgatat
     6301 agcaatggta agattttttc ttcgtcggta tctggaggta gcttaaaggt ttaaagctta
     6361 gaattcttac ttctgggata tcggttatga ttctctggga tgcttttttt cttttgtttg
     6421 ggtgttttgt ttatgatggt atttgttttg gtggttgtat ttcatatgtt tttgtgaaat
     6481 ttggatttga aagttttttc tggagagcgt tcctgagtta gttcttttga gtgtgggttt
     6541 ttatctcagc gtttggttga gaaacctttt agttatactt attttatatt gttggttttt
     6601 tttgttgtgt ttgatttaga ggtttctttg ttattgaata tgccatatca aggtgtttta
     6661 tataagaaat tgttgtgtta tttgtttttt tttgctttgg taggttttgg ttttttggtt
     6721 gagattcgtc gtgggtatgt ttcatgaatt tattagtgat tttggagttt atttggttgt
     6781 cgctgctaac gatgatttgg gtatttattt tatccgagct cctggttaat tagtttgagt
     6841 aatttaggtt attttgactg tttgttttca aaacaaaagg tggcttttgt ggctggttgc
     6901 tgatagtgag atattttggt tggttgttta ctttagatca taagcgtatt ggttttgttt
     6961 atttggtagt tggtttgtgg agagggtttt tgggtttgtc gttgagtatg ttgattcgtt
     7021 taaaatattt ggatccttat tataatttga tttctccgga ggtgtataat tatttggtta
     7081 caggtcatgg tatagtgatg attttttttt ttctgatgcc tgtgttgata ggtggttttg
     7141 gtaattattt gttaccgttg ttgttaggga taggtgattt gaatttacct cgtgtaaatg
     7201 ctttgagagt gtggttgtta ttgccatcct gtgtttgttt ggctttgagt ttgatagggg
     7261 gtgctggtgt tggttgaact ttttatcctc ctctgtctgc ttcaggttat tctggttggg
     7321 gggtggattt tatgatgttt tctttgcatt tggccggggt atctagtatt tttagttcta
     7381 ttaaatttat ttgtactata gttgaggtta tgtttgagga aggtacaggt cgtttgagta
     7441 ttttggtttg ggcttatttg tttacttcta ttttattgct tttatctttg cctgtattag
     7501 ctgctgctat aactatgttg ttgtttgatc gtagttttgg gtctgcattt tttgatccta
     7561 tggggggggg agacccggtg ttgtttcagc atttgttttg gttttttggg catcccgagg
     7621 tgtatgtttt gattttgcca ggttttggtg tgattagaca tatttgtgtt actttgacta
     7681 ataaagattc tttgtttggt tattatgggc ttgttttggc tatggctgct atagtttgtt
     7741 tgggtagtat agtttgggca catcatatgt ttatggttgg tttggatgtg catactgctg
     7801 ttttttttag ttctgttact atggtgatta gtattcccac aggtattaag gttttttctt
     7861 ggttgataat gttaggtggg ggtagatttg ttcgtatttg ggatcccata gtgtgatgaa
     7921 ttgtggggtt tgttgtgttg tttactatag ggggggttac tggtattatg ctttctgctt
     7981 ctattttgga tactttgttg catgatactt ggtttgttgt ggctcatttt cattatgttc
     8041 tttctttagg ttcttatagg agtgttgtta tatcttttat ttgatggtgg ccaataatta
     8101 ctggttatag tttaaaagtt tctttgttgc aaggtcattg gattgtttct atgtttggtt
     8161 ttaatttgtg tttttttccg atgcattatt tgggtatgtc tgggcttcct cgccgggtgt
     8221 gtgtttatga tcctgatttt tattgactta atgtgttatc aagtttaggg ggtgtggtat
     8281 caactgtaag ggcctttttt cttttttttg ttttatggga gtcttgtgtg ataggtaatc
     8341 gggtactttc tgtttggggt tccggttctt tgattttgaa tgttgttact ttacctagac
     8401 ctcagcataa aggttatatg actgggggtt gtcggtggtg tgcttagtgt ggtttgtttg
     8461 tactggtttg gtttagggga gaattagttt aggttagaat tttgtttttg taaaacagcg
     8521 gtacctttgg tattctccag tttgtgtttt gagattgttg ttttttgtgg gtttctttat
     8581 ttgtggtacc ttttgcatca tgatttgttg agttgttctt tagattgtag ggtctcgaag
     8641 ggtactgatt tttgttatgc ttggttagtt tatatgatgg aagtaggtaa ctttagttta
     8701 aagtatatca aggttgtgat aagtgattcg gagtatctta tatctagtgg aggaattgtt
     8761 tattggttta tagcgctttg gtgatgatat caaggtgttg ttaagattca gtttggtttt
     8821 gttatactat cttggattaa aattctcctt gttttaggtg tgtgttataa ttcaattttg
     8881 ttttgtgttg agtttgttgt agctttcttt catgttaatt tttcttgata atgatagaat
     8941 aagtaggtgt tttattactt ttattctttc ttggttgttt tcggtctgtt tattaaaaac
     9001 atttccattt gtatttatag atggtagtgc ctgcccagtg ctttgtatgt aaatggccgc
     9061 agtattttga ctgtgctaag gtagcataat tagttgcctc ataattggag gattgtttga
     9121 aaggtttgac ttgagaatta ggctagatta gaggtttgac tgaaattgaa tttgaggtgc
     9181 agattcctcg ttgtgttaat aagacggaaa gaccccgaga tctttatgtt gtgatatttg
     9241 tttggggcaa aagagtatat tttatattgt tttggatcct aaaggattat aggtttaagt
     9301 tacctcgggg ataactaggt aaaaaataag gagaggtctg atcgatttat tttattgcta
     9361 tctcgatgtt gacttgggaa taatgaggag gtgtaggagt ttccttgtta ggtctgttcg
     9421 accttgtttc cctcatgagt tgagttaaga ccggcgtgag ccaggtcggt tcttatctat
     9481 tagttacacg aattagtacg aaaggattgt ttgtgttcta tgtataggaa tgtgggcgtg
     9541 acgcttttgg cgtattactc tgcaaaggta ttataggtat ttttatgccc tcgcctttaa
     9601 tctatttaac tctggttgtg gaagatatga aagcaggttt acataagatt gttatttttt
     9661 tggttttgca tatgaaggtt gtgtgctatt atcttagtaa ctaaaatttt aagatggggg
     9721 aaaccccgat tgggctgttt ttgtgaaagt ggtggattct cagtgccagc atccgcggtt
     9781 attctgttag cttttatctt cttttgttgt tggtataaaa tgtttagaat ttagggtttg
     9841 tcagaattgg gtagaatttt tctggagttg aaataagatt ttaaggattt tctgagctta
     9901 ctattataaa aggggattag atacccccgt atatgtatga tttgactttg tgtccatagt
     9961 ataaactaaa agggtttggc agctggtaaa attctaccgg gggaacgtgt gtcgtaatag
    10021 atagtccgct tagtatttaa ccttgattat gtttgttagt gtatatccgg tttaagatct
    10081 gtgatttgtg tctataggtg taatttttgt ttatttaagc caggtcaatg tgctgctaat
    10141 atcaagggga ttatgcgtta ctttgttaaa aggatttact gagaatacgg taattgtatt
    10201 aaggactcgg aagtaggctt agtttgttgt agttaattct ttaggtcgaa tgtgatttta
    10261 gctggggtac acaccgcccg tcacccagga cgttagtcgt ggtaagtcgt aacatggtaa
    10321 tactagggga acctggtgtt agttagcttg aggtttattc tttaaggttg atggtttcta
    10381 tcaatgtatt gtatatggat ttaattactt atgttttgat tatttgtgct tttattcctg
    10441 tttgggtttt tgttgtatta ggttggcagt tattttcttc taaaactgta gcatctcaca
    10501 ataatgagag ggatgtggtt gagttttttt ggactgtggt cccatccacg tgtgttattg
    10561 ctctatgcta ttataatctt aattgcattt cttatgatat gctagatata cccagtaaga
    10621 ttgtgaaggt ggttggtcgt cagtggtatt ggacttatga ggttgaggga gagggtgaag
    10681 agtatgattc agttatgagg gattttgttt ctggtgtaga taagccttta cgtatgattc
    10741 gtggcaggtt ttatcaattt atggtaactt cttcagatgt aattcattct ttttctgtac
    10801 cagagtttaa tataaagagt gatgcaatac ccggacggtt gaattatatt tcttattgtc
    10861 cggctcagct tggtgtatat gttgggtatt gtacggagtt atgcggtgtt gggcatgctt
    10921 acatgccaat tgttattgag gtggttatgc cagaggaaag gtaaggtttt ttgattttac
    10981 tttgtattga tggtgtttta ttaggtaggt taggaaagga tgttagtttc aattttgata
    11041 ggcttttatt ttacatgtat ttgaggattt tcgttaattt ctcatccggt tgtgtattgt
    11101 atattattgt taggcgctgc tttaaggatt tctggattga ggtatttagt attggggttt
    11161 agttgatatt tagcaatttt ttgtttggtg tatgttgggg gggtgtatgt tttgtttatt
    11221 tttgtttcta ttcataatcc taaaccgttg cctaaaataa ggggtagcgt tataggactt
    11281 ttcgtgttgt tgtgtgcctt tttgtgtttg ttctcttttg cctttttttc ctttccttct
    11341 ttgggggatg agaggtttta tttgtgttct ttttttgaag gggtttctta ttgtttattt
    11401 tgtttggttc ttatgttggg ttttatatgt gttagggttg ttgttagtag taagaatttg
    11461 ttttttcgtt aggtttaggc tgatttagca taagtttagt gtgtgggatt gtagcttccg
    11521 aggtgagtta ttttaatcag gtttgaggtg ccagattata tgggtatggt ttaggacctt
    11581 attatgacat tgaagtctct caagcgggta taggttactg taggtgattt gaagtcattt
    11641 atttcatatt gagttatggt gcctgctatt ttgttagtgg tgtataggtg ccagattata
    11701 tgggttggtt ttaagcatca aatatggggt tttcccccta tatggtttga tatcctttgg
    11761 ggattacgtt tcggccgtag ttttggagtt tgtttacttc tatcaatagt gttgttaatt
    11821 tttttgggga tctttggttt atatgtgttt tgatatgttg gccagttgta ttgattgggt
    11881 aaatttagcg ttcttgctca tgataggttt agtagttttg gttttatgct ggatgatgtg
    11941 agtatgggtt gtgtttttat gttattgtgt tgtggtgggg tagccttggt ttattgtttt
    12001 cattatttta gtggtggggt ggaagggtca ttgttgtttc ctttgatggt ttgattttta
    12061 ggcgttatgt gtatattggt atttagcagc tctttggtat tttctttaat tttttgggag
    12121 tatttaggtt tagttaggtt ttttttaatt ttgttttatt ctaaaagtag tagtttgcgt
    12181 gcttctttaa taactctttt tgcttctcgg tttggagatg tttctatgtt tgttattata
    12241 atgtgtgtga gttgatgaat tggtacgtct ggggcaggtt ttattttttt gtatttgtta
    12301 gtggttttga ctaagagtgc tggttttccg tttatttctt ggttgttaga ggctatgcgg
    12361 gctcctactc cggtgagttc tttggttcat tcttctacac tggttgctgc gggtgtttga
    12421 tttgtgttac gttataggag ttatagtagt tggtggttag attttttttt agtgtacttt
    12481 tgtttattgt ctattattat tactgctgtg gcggcaactg tttttatgga tttaaagaag
    12541 attgttgctt tgtctacttg taaaaaagtc tcttggtgtc tattgttttt tgtttgtggt
    12601 gatgtgtttt tggctttgct tcaattaatt acacatggtc tttgtaagtg ttatttgttt
    12661 atgagggttg gtgatttaat gtgccagtcc gggtctagac agagttcggt aggagtatat
    12721 ttaggtcgtt atagtggtgt gtatttaccg gttattcaaa ggtttttggt attgtctttg
    12781 tgcggtttgc cgtttttggg ggtttttttt agtaagcact gttttttttc tggtttattg
    12841 ggtgttatga gtttaggcgg ttgtttattg tttcttattt gtttgttttt gtcttatgtt
    12901 tattctgtgc ggtttgtttt attgttggtt ggtagagtag gaggtttgag gtttggttat
    12961 tttaggggat ttatgatcat atgtcctttg gttgttttgg gcagtgtttt gaaatttttg
    13021 gtgagcttgg gtactgttga ggttattagt ttatctagga tttgatctgt tgttgtattg
    13081 agtttacaag gtgttgggag tttagtgggt tgatgatttt attatttgtg tatgggctat
    13141 ggtcgatgaa gttctcttgt ttgagggagt gaggggtttg tttctttgtt ttatagttgg
    13201 tttcttgttt taagaagagt ttgtgtttct tctttctatc gttgagagat ctattttttg
    13261 gacatgtttt ttagcttggg cagggtttat cgttgaagat tttatcgagg ttctttgttt
    13321 tctttaagtt ttatgatttt agggatgatg ggtgtgtttt ttgcttctta tattttttag
    13381 tgaatgtcaa tagtatagtt tagtatgctg tctttccaag tcagaggtct atttgttagt
    13441 tgatgtatgt taggtttgcc gattaggtgt tatacatatc agtttttcgt gttggggtgg
    13501 atttttaggt cattcggcag tttttatttt tatgtaatgt ttttatgttg attaaaaaat
    13561 tggtttaaaa tttttaaatc tgggtttgtt taccgtttat tagaatgttt gagtttgtgt
    13621 gattttgctt tgtatttttt gtgttgtaat actaccctct tttgatattt taggtatttt
    13681 gtatatttgt gtttatggtg gtgtggttga gccgggttta tcgaaattgt gaaaattcat
    13741 aagcaggcgg ttaattaaat attgatatta ttattatata gatgcctata atgtggtttg
    13801 aaaataaagt gtatttgatt gtgtgttgga taggataggt atgttgggta tttgttttgg
    13861 tggattggat tgtgcctagg ggctgagtgt taatgataat gggatgtgta ttatatgggt
    13921 gattgtgtgt tggataggat aggtatgttg ggtatttgtt ttggtggatt ggattgtgcc
    13981 taggggctga gtgttaatga taatgggatg tgtattatat gggtgattgt gtgttggata
    14041 ggataggtat gttgggtatt tgttttggtg gattggattg tgcctagggg ctgagtgtta
    14101 atgataatgg gatgtgtatt atatgggtga ttgtgtgttg gataggatag gtatgttggg
    14161 tatttgtttt ggtggattgg attgtgccta ggggctgagt gttaatgata atgggatgtg
    14221 tattatatgg gtgattgtgt gttggatagg ataggtatgt tgggtatttg ttttggtgga
    14281 ttggattgtg cctaggggct gagtgttaat gataatggga tgtgtattat atgggtgatt
    14341 gtgtgttgga taggataggt atgttgggta tttgttttgg tggattggat tgtgcctagg
    14401 ggctgagtgt taatgataat gggatgtgta ttatatgggt gattgtgtgt tggataggat
    14461 aggtatgttg ggtatttgtt ttggtggatt ggattgtgcc taggggctga gtgttaatga
    14521 taatgggatg tgtattatat gggtgattgt gtgttggata ggataggtat gttgggtatt
    14581 tgttttggtg gattggattg tgcctagggg ctgagtgtta atgataatgg gatgtgtatt
    14641 atatgggtga ttgtgtgttg gataggatag gtatgttggg tatttgtttt ggtggattgg
    14701 attgtgccta ggggctgagt gttaatgata atgggatgtg tattatatta tataattgtt
    14761 tctttagttc ttgaaatagg ggtttccctc ctcatttatg gtgaggtttt tactttggtg
    14821 gttttcgaga aat
//

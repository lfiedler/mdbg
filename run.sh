#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`
SRC=${BIN}/programs/core
DB_COPY_FILE=
PSQL_HOST=127.0.0.1
PSQL_PORT=5432
PSQL_PW=MITOS
ID=
FASTA_FILE=
COMMAND=
VALID_OPTS=h; 
# Options requiring an argument
VALID_LONG_OPTS+=,psql-host:;
VALID_LONG_OPTS+=,psql-port:;
VALID_LONG_OPTS+=,psql-pw:;
VALID_LONG_OPTS+=,study-dendograms:;
VALID_LONG_OPTS+=,create-annotations-fasta:;
VALID_LONG_OPTS+=,show-annotations:;

# No argument options
VALID_LONG_OPTS+=,help;
VALID_LONG_OPTS+=,create-db-from-copy;
VALID_LONG_OPTS+=,create-db;
VALID_LONG_OPTS+=,create-annotations;
VALID_LONG_OPTS+=,evaluate-annotations;


show_psql_accession_data() {
    echo $PSQL_HOST $PSQL_PORT $PSQL_PW
}
create_db() {
    #${SRC}/createDB.sh
    echo "create db "
    show_psql_accession_data
}

create_db_from_copy() {
    ${BIN}/createDBFromCopy.sh ${PSQL_HOST} ${PSQL_PORT} ${PSQL_PW} ${PSQL_HOST} ${DB_COPY_FILE}
}

create_annotations() {
    ${SRC}/createAnnotations.sh ${FUNCNAME[0]}
    
}

show_annotations() {
    local VAL="${FUNCNAME[0]} ${ID}"
    ${SRC}/createAnnotations.sh ${VAL}
}

create_annotations_fasta() {
    local VAL="${FUNCNAME[0]} ${FASTA_FILE}"
    ${SRC}/createAnnotations.sh ${VAL}
}

evaluate_annotations() {
    ${SRC}/createAnnotations.sh ${FUNCNAME[0]}
}

study_dendograms() {
    sed -E -i -e "s#.*(/graphFiles/).*(/propertyCluster/.*)#\"${BIN}\1${ID}\2#" ${BIN}/cassandra/scripts/init-cluster.groovy
    ${BIN}/cassandra/bin/gremlin.sh -i ${BIN}/cassandra/scripts/init-cluster.groovy
}

exit_abnormal() {                              # Function: Exit with error.
  usage
  exit 1
}

exit_normal() {                              # Function: Exit without error.
  usage
  exit 0
}

usage() {
    echo "Usage: $0 [options] {create-db|create-annotations|evaluate-annotations}" >&2
    echo " --create-db:  Create the psql databse" >&2
    echo " --create-db-from-copy:  Create the psql databse from specified sql file" >&2
    echo " --create-annotations:  Create annotations" >&2
    echo " --show-annotations:  Show annotations of provided accession id (annotations must have been generated beforehand!)" >&2
    echo " --create-annotations-fasta:  Create annotations for sequence in provided fasta file" >&2
    echo " --evaluate-annotations:  Compare annotations with RefSeq" >&2
    echo " --study-dendograms:  Study dendograms with provided accession id in gremlin console" >&2
    echo " --psql-host:  Specify postgresql host (default localhost)" >&2
    echo " --psql-port:  Specify postgresql hostport (default 5432)" >&2
    echo " --psql-host:  Specify postgresql password (default no password)" >&2
}

# By calling getopt with '--options' we activate the extended mode which can
# also handle spaces and other special characters in the parameters
# this will extract the parameters as list in "PARSED"
PARSED=$(getopt --options=${VALID_OPTS} --longoptions=${VALID_LONG_OPTS} --name "$0" -- "$@")


# Set the positional parameters. By using eval we achieve, that the quoted
# output of getopt is interpreted another time by the shell (see man getopt)
eval set -- "$PARSED"

while true; do
    case "$1" in
     --psql-host)
	PSQL_HOST=$2
        echo "Setting psql host to ${PSQL_HOST}" 
        shift 2
        ;;
    --psql-port)
	PSQL_PORT=$2
        echo "Setting psql port to ${PSQL_PORT} "
        shift 2
        ;;
     --psql-pw)
	PSQL_PW=$2
        echo "Setting psql pw to ${PSQL_PW} "
        shift 2
        ;;  
    -h|--help)
        # print help message and exit
        echo "Help"
	#usage
        exit_normal
        ;;
    --study-dendograms)
	ID=$2
	echo "Study dendograms of ${ID}  with gremlin console "
	COMMAND=study_dendograms
	shift 2
	;;
    --create-db-from-copy)
	echo "Creating db from copy " $2
	DB_COPY_FILE=$2
	COMMAND=create_db_from_copy
	shift 1
	;;
    --create-db)
        echo "Creating db"
        COMMAND=create_db
        shift 1
        ;;
    --create-annotations)
        echo "Creating annotations"
        COMMAND=create_annotations
        shift 1
        ;;
    --show-annotations)
	ID=$2
        echo "Show annotations for ${ID}"
        COMMAND=show_annotations
        shift 2
        ;;    
    --create-annotations-fasta)
	FASTA_FILE=$2
        echo "Creating annotations for " ${FASTA_FILE}
        COMMAND=create_annotations_fasta
        shift 2
        ;;    
    --evaluate-annotations)
        echo "Evaluating annotations"
        COMMAND=evaluate_annotations
        shift 1
        ;;
    --)
        shift 1
        break
        ;;
  	*)  echo "How could that happen? " $1
        exit_abnormal;;
  esac
done


if [ -n "$COMMAND" ]; then
    $COMMAND
else
    usage
    exit 1
fi

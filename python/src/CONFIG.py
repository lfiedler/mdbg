"""
@author: M. Bernt

This is a confidential release. Do not redistribute without 
permission of the author (bernt@informatik.uni-leipzig.de).

defines: 
logger
    the main log.

database
    DBHOST: DB Host (can always be 127.0.0.1 .. use a ssh tunnel to port 3306)
    DBNAME: DB name
    DBUSER: DB user with write permission
    DBPWD: DB password for DBUSER
	DBREADUSER: DB user with read permission
	DBREADPWD: DB password for DBREADUSER 

paths:
	HOME: path to home directory of developer
	WORKSPACEPATH: path to workspace directory of developer
    DATAPATH: path to the directory where the fasta and genebank files are
	TMPPATH: path to temporary directory
	LOGPATH: path to formatdb log
    BLASTALLPATH: path to blastall program
	LALIGNPATH: path to lalign program

    
dbfill:
    TRANSLATIONFILEPATH: path to translation config file for GenBank parsing
    IGNOREDGENBANKFEATURES: featurestypes, which are ignored, when genbank data is inserted in db
    ASFEATURES: featuretype, which are treated as assigned sequences

uassearch:
    SSEARCH: path to the ssearch executable from the fasta package and parameters
    UASEARCHMINLEN: minimal length of the uas/ovl (only uas/ovl >= minlen are taken into account)
    UASSEARCHFOR: list of elements to search for (e.g. uas, ols)

lalign:
	LALIGNSEPARATINGSTRING: string, separating records in lalign-output


misc:
    TMPDIR: directory for temporary files

Meaning of activation code:
0 : current version
1 : deactivated, marked as deleted
2 : deactived, marked for merging

"""

# the root of the mtdb project
MTDB = "/home/pacosy/maze/workspace/mitos-dev/"
# the src folder of the mtdb project
SRCPATH = MTDB + "src/"
# the data folder (containing featureProt...) of the mtdb project
DATAPATH = MTDB + "data/"

# ?
ANALYSEPATH = MTDB + "analyse/"
# ?
BACKUPPATH = MTDB + "backup/"


#######################################################################
# WEBSERVER
#######################################################################

# switch server maintenance mode
# False: no maintenance = normal mode
# a string that should give the date and time when MITOS is expected back online
MAINTENANCE = False
# MAINTENANCE = "27. May 2013 Lunchtime (German time)"

# that the path where the sequences are processed
# - the queue client works in this directory
# WRKPATH = PROJECTPATH + "work/"
WRKPATH = "/home/maze/workspace/mitos-output/"

# the path where:
# - the webserver stores the jobs
# - the queue is running (i.e. the queue takes jobs from here and puts the results there)
TOMCATPATH = "/var/www/mitos-output/"

# the url of mitos webserver: url+path (without http)
WEBPATH = "mitos2.bioinf.uni-leipzig.de"
# mailserver to be used
EMAILSERVER = "bierdepot.bioinf.uni-leipzig.de"
# webserver souce directory (text messages ...)
MITOSPATH = SRCPATH + "webserver/"

# mitos maximum sequence size
MITOS_MAXSEQSIZE = 500000
MITOS_MINSEQSIZE = 50

# mitos maximum number of sequences per multifasta upload
MITOS_MAXNRSEQ = 10000

# add html code for tracking here will be inserted in all but the wait page
MITOS_TRACKING_CODE = """

<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://piwikmitos.bioinf.uni-leipzig.de/" : "http://piwikmitos.bioinf.uni-leipzig.de/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://piwikmitos.bioinf.uni-leipzig.de/piwik.php?idsite=2" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
"""

MITOS_CONTACTMAIL = "mitos@bioinf.uni-leipzig.de"
MITOS_SENDMAIL = "mitos-donotreply@bioinf.uni-leipzig.de"

# get copy "save" email coding from MITOS_CONTACTMAIL
# js encode
jsmail = MITOS_CONTACTMAIL.split( "@" )
jsmail[0] = "%" + "%".join( "{0:x}".format( ord( c ) ) for c in jsmail[0] )  # hex with %
jsmail[1] = "&#x" + ";&#x".join( "{0:x}".format( ord( c ) ) for c in jsmail[1] ) + ";"  # hex as in html
# html encode
htmlmail = MITOS_CONTACTMAIL.split( "@" )
htmlmail[0] = "&#x" + ";&#x".join( "{0:x}".format( ord( c ) ) for c in htmlmail[0] ) + ";"  # hex
htmlmail[1] = "&#" + ";&#".join( "{0}".format( ord( c ) ) for c in htmlmail[1] ) + ";"  # ord

MITOS_CONTACTMAILCRYPT = """<script type=\'text/javascript\'>
var pref = \'&#109;a\' + \'i&#108;\' + \'&#116;o\';
var attribut = 'hr' + 'ef' + '=';
var first = '{jsmaila}';
var at = '%40';
var last = '{jsmailb}';
var first2 = '{htmlmaila}';
var at2 = '&#x40;';
var last2 = '{htmlmailb}';
document.write( '<a ' + attribut + '\\'' + pref + ':' + first + at + last + '\\'>' );
document.write( first2 + at2 + last2 ); 
document.write( '<\/a>' );
</script>
<noscript><div style='display:none; '>are-</div><div style='display:inline; '>{htmlmaila}</div> <div style='display:none; '>-xya34</div><div style='display:inline; '>[at]</div> <div style='display:none; '>ddks-</div> <div style='display:inline; '>{htmlmailb}</div></noscript>
""".format( jsmaila = jsmail[0], jsmailb = jsmail[1], htmlmaila = htmlmail[0], htmlmailb = htmlmail[1] )

#######################################################################
# QUEUEING
#######################################################################

# IP, port, and pass for the queue server
QUEUE_IP = '127.0.0.1'
QUEUE_PORT = 50001
QUEUE_KEY = 'abracadabra'

# Mail adress spammed with error messages and mailserver to be used
QUEUE_ADM = "mitos@bioinf.uni-leipzig.de"
QUEUE_MAILSERVER = "bierdepot.bioinf.uni-leipzig.de"

# in which file the status and log should be written
QUEUE_STATUSFILE = "/var/www/mitos-output/mitos.status"
QUEUE_LOGFILE = "/var/www/mitos-output/mitos.log"

# queue size that triggers a warning email
QUEUE_MAXSIZE_WARN = 100

#######################################################################
# DB
#######################################################################

DBHOST = "127.0.0.1"
# 139.18.75.67

# DBHOST = ""
DBNAME = "mtdb"

DBUSER = "mtdb"
DBPWD = "IZBIInf109"
DBREADUSER = "mtdb_read"
DBREADPWD = "BaseRead876"

#######################################################################
# external tools
#######################################################################

ARWENPATH = "/home/maze/progz/rna/arwen/arwen"

TRNASCANPATH = "/home/maze/progz/tRNAscan/tRNAscan-SE-1.23/"
TRNASCAN = TRNASCANPATH + "tRNAscan-SE"

MITFI = SRCPATH + "mitfi/parser/mitfi_import-1.1.jar"
MITFIIMP = SRCPATH + "mitfi/parser/improve_infernal_structure.jar"
MITFICODE = SRCPATH + "mitfi/parser/mitfiGeneticCodes"
MITFIPATH = SRCPATH + "mitfi/modelle/"

VIENNAPATH = MTDB + "bin/"

BIN = MTDB + "bin/"

# number of cpus (passed to blastx/n and cmsearch)
NCPU = 1

# the java binary (also additional parameters may be added here)
JAVA = ["java", "-Xmx512m"]

#######################################################################
# logging defaults
#######################################################################

# logger = logging.getLogger()
LOGFMT = "%(asctime)s [%(process)d] - %(levelname)s - %(message)s"
# logger.setLevel( logging.DEBUG )

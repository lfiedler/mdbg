'''
@author: M. Bernt

This is a confidential release. Do not redistribute without 
permission of the author (bernt@informatik.uni-leipzig.de).

remove a certain gene from the sequence and 
'''

from optparse import OptionParser, OptionGroup
from os.path import isfile, isdir, splitext, exists, basename
from os import listdir, mkdir
from sys import exit, stderr, stdout

from gb import gbfromfile

usage = """%prog [options] fasta files/fasta directory"
    predicts proteins with blastx, and writes the aminoacid sequences  

genetic code: 
- if genbank file with same name is found the code specified there is assumed
- otherwise the genetic code specified with --code is assumed
circularity is assumed to circular per default: 
- if genbank file with same name is found the circularity is taken from there
- otherwise default circularity is assumed (can be set to linear with --linear)      
"""
parser = OptionParser( usage )
parser.add_option( "-n", "--aname", action = "append", type = "string", metavar = "NAME", help = "get all features with name NAME" )
( options, args ) = parser.parse_args()


if len( args ) != 1:
    stderr.write( "!= 1 file given\n" )
    exit()

gb = gbfromfile( args[0] )
features = gb.getfeatures( anames = options.aname )
features = sorted( features, key = lambda x: x.start )

print("> ", gb.accession, " - ", str( options.aname ))
s = 0
for f in features:
#    print f

    if f.start > 0:
        ss = gb.sequence.subseq( s, f.start - 1, 1 )
        stdout.write( "%s" % ss )
    s = f.stop + 1

if s < gb.size:
    ss = gb.sequence.subseq( s, gb.size - 1, 1 )
    stdout.write( "%s" % ss )

stdout.write( "\n" )
# for f in files:
#    stderr.write( "%s\n" % f )
#
#    gb = None
#    if exists( f + ".gb" ):
#        gb = gbfromfile( f + ".gb" )
#
#    if options.code != None:
#        code = options.code
#    elif gb != None:
#        code = gb.transl_table
#    else:
#        stderr.write( "no genbank file and no code specified for %s -> skipping\n" % f )
#        continue
#
#    if options.circular != None:
#        circ = options.circular
#    elif gb != None:
#        circ = gb.circular
#    else:
#        stderr.write( "no genbank file and no circularity specified for %s -> assuming circular\n" % f )
#        circ = True
#
#    seq = sequence_info_fromfile( f + ".fas", circular = circ )
#    if len( seq ) > 1:
#        stderr.write( "%d fasta sequeces found in %s -> only taking first\n" % ( len( seq ), f ) )
#    if len( seq ) == 0:
#        stderr.write( "no fasta sequences found in %s -> skipping\n" % ( f ) )
#        continue
#    seq = seq[0]
#
#    if gb != None:
#        name = gb.name
#        acc = gb.accession
#    else:
#        name = seq['name']
#        acc = seq['id']
#
#    seq = seq['sequence']
#
#    base = basename( f )
#    brpath = "%s/%s/" % ( directory, base )
#
#    # check for too many N's in the sequence
#    freq = seq.nucleotide_frequency( osb = False )
#    freqsum = 0.0
#    for l in freq:
#        if l in Data.IUPACData.unambiguous_dna_letters:
#            continue
#        freqsum += freq[l]
#    if freqsum > 0.05:
#        stderr.write( "%s %f%% non standard bases -> abort\n" % ( acc, freqsum ) )
#        exit()
#    elif freqsum > 0:
#        stderr.write( "%s %f%% non standard bases\n" % ( acc, freqsum ) )
#
#    # start blast if blast output directory does not exist
#    if 1:
#        if not exists( brpath + "/blast/prot/" ):
#            mkdir( '%s/%s' % ( directory, base ) )
#            brpath = singleblastx( f + ".fas", code, brpath )
#        else:
#            brpath = brpath + "/blast/prot/"
#        protlist = blastx( brpath, cutoff = cutoff, minevalue = minevalue, \
#                    acc = None, code = code, fastafile = f + ".fas", \
#                    sqn = acc, circular = circ, plot = False, debug = False, \
#                    scoresel = options.scoresel, pavg = options.pavg, havg = options.havg,
#                    prntih = True )
#
# ##    except:
# ##        stderr.write( "%s: BLAST error\n" % ( f ) )
# ##        continue
# #
# ##    dump = open( brpath + ".out", "w" )
# #    protdict = dict()
# #    for p in range( len( protlist ) ):
# ##        stdout.write( "%s\n" % protlist[p] )
# #        if protlist[p].copy == None:
# #            copy = 0
# #        else:
# #            copy = protlist[p].copy
# #        if protlist[p].part == None:
# #            part = 0
# #        else:
# #            part = protlist[p].part
# #
# #        if not protlist[p].name in protdict:
# #            protdict[ protlist[p].name ] = dict()
# #        if not copy in protdict[protlist[p].name]:
# #            protdict[ protlist[p].name ][copy] = dict()
# #        protdict[protlist[p].name][copy][part] = p
# #
# ##    dump.close()
# #
# #    # write tabular file indicating which gene was found
# #    for n in sorted( mito.prot ):
# #        if n in protdict:
# #            tab.write( "%d," % ( len( protdict[n] ) ) )
# #        else:
# #            tab.write( "0," )
# #    tab.write( " %s " % f )
# #    if exists( f + ".gb" ):
# #        tab.write( "%s" % ( ",".join( gb.taxonomy[2:5] ) ) )
# #    tab.write( "\n" )
# #    tab.flush()
# #
# #    for name in protdict.keys():
# #        if len( protdict[name] ) == 1:
# #            continue
# #
# #        bestcopy = []
# #        bestval = 0 # best score
# #        for copy in protdict[name].keys():
# #            v = 0.0
# #            l = 0
# #            for part in protdict[name][copy].keys():
# #                l += protlist[ protdict[name][copy][part] ].length( circ, len( seq ) )
# #                v += protlist[ protdict[name][copy][part] ].score
# #
# #            if options.havg:
# #                v /= l
# #
# #            if v >= bestval:
# #                if v > bestval:
# #                    bestval = v
# #                    bestcopy = []
# #                bestcopy.append( copy )
# #
# #        for copy in protdict[name].keys():
# #            if not copy in bestcopy:
# #                del protdict[name][copy]
# #
# #    # write bed file
# ##    print "==========="
# #    bedlist = []
# #    for name in protdict.keys():
# #        for copy in protdict[name].keys():
# #            for part in protdict[name][copy].keys():
# #                bedlist.append( protlist[ protdict[name][copy][part] ] )
# #    bedlist.sort( key = lambda x:x.start )
# #    bedwriter( bedlist, base, outfile = "%s/%s/%s.bed" % ( directory, base, base ) )
# #    del bedlist
# ##    print "==========="
# #
# #    # write fasta files
# #    for name in protdict:
# #        copy = choice( protdict[name].keys() )  # choose a random of the remaining copies
# #        p = protlist[ protdict[name][copy][0] ]
# #        ntseq = sequence( "" )
# #        for part in protdict[name][copy]:
# #            q = protlist[ protdict[name][copy][part] ]
# #            # get nucleotide sequence
# #            ntseq += seq.subseq( q.start, q.stop, q.strand )
# #
# #        # write nucleotid sequence
# #        if not p.name in ntfiles:
# #            ntfiles[p.name] = open( "%s/%s.nt.fas" % ( directory, p.name ), "a" )
# #        ntfiles[p.name].write( ">%s %s %d %d %d (%d)\n" % ( acc, p.name, p.start, p.stop, p.strand, len( protdict[name][copy] ) ) )
# #        ntfiles[p.name].write( "%s\n" % ( ntseq.data ) )
# #        ntfiles[p.name].flush()
# #
# #        if len( ntseq ) % 3 != 0:
# #            stderr.write( "%s %s %d%%3=%d\n" % ( f, p, len( ntseq ), len( ntseq ) % 3 ) )
# #
# #        # get aminoacid translation
# #        # and remove final stop if there is one (internal stops are left in the seq
# #        aaseq = ntseq.translate( table = code, stop_symbol = "*", to_stop = False )
# #        if aaseq[-1] == '*':
# #            aaseq = aaseq[:-1]
# #
# #        stpcnt = aaseq.count( "*" )
# #        stppos = aaseq.find( "*" )
# #        if stpcnt != 0 and stppos != len( aaseq ) - 1:
# #            stderr.write( "%s %s has internal stop codons\t" % ( f, p.name ) )
# #            stderr.write( "%s\t" % ( str( ntseq ) ) )
# #            stderr.write( "%s\n" % ( str( aaseq ) ) )
# #
# #        # write aminoacid sequence
# #        if not p.name in aafiles:
# #            aafiles[p.name] = open( "%s/%s.aa.fas" % ( directory, p.name ), "a" )
# #        aafiles[p.name].write( ">%s %s %d %d %d (%d)\n" % ( acc, p.name, p.start, p.stop, p.strand, len( protdict[name][copy] ) ) )
# #        aafiles[p.name].write( "%s\n" % ( aaseq.data ) )
# #        aafiles[p.name].flush()
# #
# #        # TODO if there is an internal stop codon then mark as pseudo -> in the blast x function
# #
# #        # TODO use length statistic of the data base to determine too short/long gene
#
#
#
#    i += 1


'''
@author: M. Bernt

This is a confidential release. Do not redistribute without 
permission of the author (bernt@informatik.uni-leipzig.de).
'''

from operator import itemgetter
import sys
import extprog
import subprocess
import re
import os
import glob
import logging

from feature import trnafeature
from . import trna_nameaamap, L1, L2, S1, S2, codon

from Bio.Data.IUPACData import ambiguous_dna_values

from gb import gbfromfile
import CONFIG

from .arwenscan import arwenscan

# class trnascanfromdb( arwenscan ):
#     def __init__( self, acc ):
#         """
#         \param acc Accession number string
#         """
#
#
#         dbh = db.get_dbConnection( 'r' )
#
#         cursor = dbh.cursor()
#         cursor.execute( "BEGIN" )
#
#         speciesID = db.getspeciesid( acc, cursor )
#
#         if speciesID == None:
#             logging.warning( "Accession number %s not found in database" % acc )
#
#         else:
#             arwenscan.__init__( self )
#             self.accession = acc
#
#             db.getspeciesdatafromdb( self, cursor )
#             db.gettrnascanfeaturesfromdb( self, cursor )

class trnascanparm( extprog.parm ):
    def __init__( self, name, type, range = [] ):
        extprog.parm.__init__( self, name, type, range )
        self.infix = " "
        self.prefix = "-"

class StderrError( Exception ):
    def __init__( self, prog, stderr ):
        self.prog = prog
        self.stderr = stderr
    def __str__( self ):
        return "%s has returned an error message:\n %s\n" % ( repr( self.prog ), repr( self.stderr ) )

def getGencodeFromTranl_tableNumber( tableno ):
    trnadir = "%s" % ( CONFIG.TRNASCANPATH )
    table2files = {'2':trnadir + 'gcode.vertmito', \
                   '4':trnadir + 'gcode.invmito', \
                   '5':trnadir + 'gcode.invmito', \
                   '9':trnadir + 'gcode.echdmito', \
                   '13':trnadir + 'gcode.invmito' }
    return table2files[tableno]

def singletrnascan( file, **keywords ):
    """
    call trnascan
    \param file fastafile for a species, e.g. NC_010202.fas

    -O  
        search for organellar (mitochondrial/chloroplast) tRNAs

        This parameter bypasses the fast first-pass scanners that are
        poor at detecting organellar tRNAs and runs Cove analysis only.
        Since true organellar tRNAs have been found to have  Cove  scores
        between  15  and 20 bits, the search cutoff is lowered from 20 to
        15 bits.  Also, pseudogene checking is disabled since it is  only
        applicable  to  eukaryotic  cytoplasmic  tRNA pseudogenes.  Since
        Cove-only mode is used, searches will be very slow (see -C option
        below) relative to the default mode.
        
    -f  <file>
        save results and Cove tRNA secondary structures  to <file>

        This option saves results and secondary structure information (as
        predicted by the coves program) in <file>.  Use '$' in  place  of
        <file> to send to standard output. An example of the output for-
        mat for one tRNA appears below:

        CELF22B7.trna4 (26992-26920)    Length: 73 bp Type: Phe       Anticodon: GAA at 34-36 (26959-26957)   Score: 73.88
                   *    |    *    |    *    |    *    |    *    |    *    |     *     |
        Seq:   GCCTCGATAGCTCAGTTGGGAGAGCGTACGACTGAAGATCGTAAGGtCACCAGTTCGATCCTGGTTCGGGGCA
        Str:   >>>>>>>..>>>>........<<<<.>>>>>.......<<<<<.....>>>>>.......<<<<<<<<<<<<.
        
    -q  
        quiet mode (credits & run  option  selections  suppressed)

        This  option suppresses the program credits and run option selec-
        tions normally printed to standard error at the beginning of each
        run.
    -b
        brief output format (no column headers)

        This option eliminates column headers that appear by default when
        writing results in tabular output format.  Useful if results  are
        to be parsed or sent to another program.
        
    -X  <score>
    
        set Cove cutoff  score  for  reporting  tRNAs  (default=20)

        This  option  allows  the  user to specify a different Cove score
        threshold for reporting tRNAs.  It is not recommended that novice
        users  change  this cutoff, as a lower cutoff score will increase
        the number of pseudogenes and other false positives found by  tR-
        NAscan-SE  (especially when used with the "Cove only" scan mode).
        Conversely, a higher cutoff than 20.0 bits will likely cause true
        tRNAs  to  be missed by tRNAscan (numerous "real" tRNAs have been
        found just above the 20.0 cutoff).  Knowledgable users  may  wish
        to  experiment with this parameter to find unusual tRNAs or pseu-
        dogenes beyond the normal range of detection, keeping the preced-
        ing caveats in mind.

    -g  <file>
        use alternate genetic codes specified in <file> for determining tRNA type

        By  default,  tRNAscan-SE  uses a standard universal codon->amino
        acid translation table that is specified at the end  of  the  tR-
        NAscan-SE.src source file.  In many mitochondrial and a number of
        other microbial organisms, there are exceptions to this universal
        translation  code.  This option allows the user to specify excep-
        tions to the universal code.  Several alternate translation  code
        files are included in this package for convenience:
        
        gcode.cilnuc
            for Ciliates, Dasycladacean, & Hexamita nuclear tRNAs
        gcode.echdmito
            for Echinoderm mitochondrial tRNAs
        gcode.invmito
            for Invertibrate mitochondrial tRNAs
        gcode.othmito
            for Mold, Protozoans, & Coelenterate mitochondrial tRNAs
        gcode.vertmito
            for Vertibrate mitochondrial tRNAs
        gcode.ystmito
            for Yeast mitochondrial tRNAs
            
    -Q
        do not prompt user before overwriting  pre-existing files

        By default, if an output result file to be written to already ex-
        ists, the user is prompted whether the file should be  over-writ-
        ten  or  appended  to.   Using this options forces overwriting of
        pre-existing files without an interactive  prompt.   This  option
        may  be handy for batch-processing and running tRNAscan-SE in the
        background.



    """

    cfile = None  # constraints file

    par = [ trnascanparm( 'O', 'flag' ), trnascanparm( 'g', 'file' ), trnascanparm( 'b', 'flag' ), \
           trnascanparm( 'q', 'flag' ), trnascanparm( 'X', 'int' ), trnascanparm( 'f', 'file' ), \
           trnascanparm( 'Q', 'flag' ) ]
    cl = extprog.cmdline( keywords, par )

    pars = str( cl ) + file
    # print pars

    env = os.environ.copy()
    env['PATH'] += ":" + CONFIG.TRNASCANPATH

#    print( "tRNAscan\n %s %s\n" % ( CONFIG.TRNASCAN, pars ) )
    p = subprocess.Popen( "%s %s" % ( CONFIG.TRNASCAN, pars ), shell = True, \
                          stdout = subprocess.PIPE, stderr = subprocess.PIPE, \
                          stdin = subprocess.PIPE, close_fds = True, cwd = CONFIG.TRNASCANPATH, env = env )
    if cfile != None:
        p.stdin.write( cfile.read() )

    if cl.get( "C" ) == True:
        cfile.close()

    err = ""
    for i in p.stderr.readlines():
        if i == "" or re.match( "^\\d+ sequences; length of alignment \\d+\.$", i ) != None:
            continue
        else:
            err += i + "\n"

    if err != "":
        raise extprog.StderrError( "trnascan %s" % ( pars ), err )


def trnascan( dir ):
    """
    see singletrnascan
    """

    files = glob.glob( dir + '*.fas' )
    scanpath = dir + 'tRNAScan'
    if not os.path.exists( scanpath ):
        os.mkdir( scanpath )

    i = 0
    for file in files:
        i += 1
#         print file
#         print "%s/%s" % ( i, len( files ) )
        acc = os.path.basename( file ).split( '.' )[0]
        gbfile = dir + acc + '.gb'
        gbdata = gbfromfile( gbfile )
        outfile = dir + 'tRNAScan/' + os.path.basename( file ) + '.ss'
        try:
            gencode = getGencodeFromTranl_tableNumber( str( gbdata.transl_table ) )
        except:
            logging.debug( "Could not determine genetic code for %s" % ( file ) )
            gencode = None

        if gencode != None:
            singletrnascan( file, Q = True, O = True, b = True, X = 5, q = True, f = outfile, g = gencode )
        else:
            singletrnascan( file, Q = True, O = True, b = True, X = 5, q = True, f = outfile )


def parse( fh, code ):
    """
    parse a tRNAscan output file
    - skip tRNAs reported with introns
    - skip tRNAs reported with unknown anticodon
    
    @param[in] fh a file handle 
    @param genetic code (table number), if not given the tRNAs given in the file are just taken
    @return list of tRNA features
    """

    trnas = []

    intronstart = 0
    intronstop = 0
    skip = False
    start = None
    stop = None
    strand = None
    score = None
    seq = None
    dotbracket = None
    name = None
    anticodonpos = None
    anticodon = None

    for l in fh.readlines():
        l = l.lstrip().rstrip().split()

        # empty line marks start of a new feature
        if not l:
            if skip == False:
                trnas.append( trnafeature( name = name, type = "tRNA", \
                        start = start, stop = stop, strand = strand, \
                        method = "tRNAscan", score = score, sequence = seq, \
                        struct = dotbracket, anticodonpos = anticodonpos, \
                        anticodon = anticodon ) )
            intronstart = 0
            intronstop = 0
            skip = False
            continue
            # datensatz fertig, schreiben

        # if l[0].startswith('NC_'): #USE THIS LINE
        if l[0].startswith( 'NC_' ) or l[0].startswith( 'As_mt_' ) or l[0].startswith( 'Bc_mt' ) or l[0].startswith( 'Ec1-6' ) or l[0].startswith( 'Gg_cons' ) or l[0].startswith( 'Hf_mt' ) or l[0].startswith( 'On_mt' ):
#            number = l[0].split( '.' )[-1][4:]

            # trnascan: positions are counted from 1 the first and the last
            # position belongs to the trna
            start = int( l[1].split( '-' )[0][1:] ) - 1
            stop = int( l[1].split( '-' )[1][:-1] ) - 1

            # is start < stop then the sequence if on the reverse complement
            if start > stop:
                start, stop = stop, start
                strand = -1
            else:
                strand = 1

        elif l[0].startswith( 'Type:' ):
            if l[3] == "???":
                skip = True

            anticodon = codon( l[3], "anticodon" )
            anticodonpos = int( l[5].split( '-' )[0] )

            if code != None:
                name = anticodon.get_aa( code )
            else:
                if l[1] in trna_nameaamap:
                    name = trna_nameaamap[l[1]]
                else:
                    name = l[1]

            score = float( l[8] )

            if name == "S":
                if  anticodon == S1:
                    name = "S1"
                elif anticodon == S2:
                    name = "S2"
                else:
                    name = "S"
                    logging.warning( "warning non standard Ser %s" % l[3] )

            if name == "L":
                if anticodon == L1:
                    name = "L1"
                elif anticodon == L2:
                    name = "L2"
                else:
                    name = "L"
                    logging.warning( "warning non standard Leu %s" % l[3] )

            continue

        elif l[0].startswith( 'Possible' ):
            skip = True
            intronstart = int( l[2].split( '-' )[0][1:] ) - 1
            intronstop = int( l[2].split( '-' )[1][:-1] ) - 1
            if intronstart > intronstop:
                intronstart, intronstop = intronstop, intronstart
            continue

        elif l[0].startswith( '*' ):
            continue

        elif l[0].startswith( 'Seq' ):
            seq = l[1]
            continue

        elif l[0].startswith( 'Str' ):
            # read struckture
            dotbracket = re.sub( '>', '(', re.sub( '<', ')', l[1] ) )

            # printing anticodon position in struckture
#            dotbracket = dotbracket[:anticodonpos-1]+"AAA"+dotbracket[anticodonpos+2:]
            continue

        else:
            logging.error( "unexpected line in tRNAscan output {line}".format( line = l ) )
            sys.exit()

#    trnas.sort( key = lambda x : x.start )

    return trnas

# def analyze( tRNA, feature, length ):
#    """
#    call analyze
#    \param tRNA single tRNAscan-hit
#    \param feature single tRNA-feature
#    \param length length of the tRNA
#
#    \return hitsGB percentual coverage from tRNA-scan at the original genbank-location
#    \return factor ratio between the lengths of tRNA-scan and original genbank-tRNA-length
#    """
# #    print tRNA
# #    print feature
# #    print "genbank-start: %s, genbank-end: %s" % (feature.start, feature.stop)
# #    print "tRNA-start:\t%s, tRNA-end:\t%s" % (tRNA['start'], tRNA['end'])
#
#    #determine length of feature
#    if  abs( feature.stop - feature.start ) > length / 2:
#        length_f = length - feature.start + feature.stop
#        #create new feature.stop: length + feature.stop, required for later calculation
#        feature.stop = length + feature.stop
#    else:
#        length_f = feature.stop - feature.start
#
#    #determine length of tRNA
#    if  abs( tRNA['end'] - tRNA['start'] ) > length / 2:
#        length_tRNA = length - tRNA['start'] + tRNA['end']
#        #create new tRNA-end: length + tRNA['end'], required for later calculation
#        tRNA['end'] = length + tRNA['end']
#    else:
#        length_tRNA = tRNA['end'] - tRNA['start']
#
#    #calculate ratio between length of feature and length of tRNA
#    #this ratio is needed to normalize the genbankhit-ratio
#    if length_f > length_tRNA:
#        factor = float( length_tRNA ) / length_f
#    else:
#        factor = float( length_f ) / length_tRNA
#
#    #calculate the percentual coverage of the tRNA-hit at the genbank-location
#    if tRNA['start'] < feature.start:
#        if tRNA['end'] < feature.start:
#            hitsGB = 0
#            #print "hits at genbank-location: 0%\n"
#
#        elif tRNA['end'] == feature.start:
#            hitsGB = float( 1 / length_f )
#            #print "hits at genbanklocation: 1/%s=%.2f\n" % (length_f, hitsGB)
#
#        elif tRNA['end'] < feature.stop:
#            hitsGB = float( tRNA['end'] - feature.start ) / length_f
#            #print "hits at genbank-location: %s/%s=%.2f\n" % ((tRNA['end']-feature.start), length_f, hitsGB)
#
#        elif tRNA['end'] == feature.stop:
#            hitsGB = 1
#            #print "hits at genbank-location: 100%\n"
#
#        elif tRNA['end'] > feature.stop:
#            hitsGB = 1
#            #print "hits at genbank-location: 100%\n"
#
#        else:
#            sys.stderr.write( "[trnascan] something wrong with start- and endpositions\nanalyze() in trnascan.py" )
#
#    elif tRNA['start'] == feature.start:
#        if tRNA['end'] < feature.stop:
#            hitsGB = float( feature.stop - tRNA['end'] ) / length_f
#            #print "hits at genbank-location: %s/%s=%.2f" % (length_tRNA, length_f, hitsGB)
#
#        elif tRNA['end'] == feature.stop:
#            hitsGB = 1
#            #print "100%"
#
#        elif tRNA['end'] > feature.stop:
#            hitsGB = 1
#            #print "100%"
#
#        else:
#            sys.stderr.write( "[trnascan] something wrong with start- and endpositions\nanalyze() in trnascan.py" )
#
#    elif tRNA['start'] > feature.start:
#        if tRNA['start'] > feature.stop:
#            hitsGB = 0
#            #print "hits at genbank-location: 0%\n"
#
#        elif tRNA['start'] == feature.stop:
#            hitsGB = float( tRNA['end'] - tRNA['start'] ) / length_f
#            #print "hits at bg: %s/%s=%.2f" % ((tRNA['end'] - tRNA['start']), length_f, hitsGB)
#
#        elif tRNA['start'] < feature.stop:
#            hitsGB = float( feature.stop - tRNA['start'] ) / length_f
#            #print "hits at genbank-location: %s/%s=%.2f\n" % ((feature.stop-tRNA['start']), length_f, hitsGB)
#
#        else:
#            sys.stderr.write( "[trnascan] something wrong with start- and endpositions\nanalyze() in trnascan.py" )
#
#    else:
#        sys.stderr.write( "[trnascan] something wrong with start- and endpositions\nanalyze() in trnascan.py" )
#
#    #normalized = float(hitsGB)*factor
#    #print "normalized: %f*%f=%f\n------------------\n" % (hitsGB, factor, normalized)
#
#    return hitsGB, factor



# def compare_nt( n1, n2 ):
#    """
#    test two nucleotides for equality
#    """
#    if n1 in ambiguous_dna_values[n2] or n2 in ambiguous_dna_values[n1]:
#        return True
#    else:
#        return False
#
# def compare_seq( s1, s2 ):
#    """
#    test two nucleotide sequences for equality
#    """
#    if len( s1 ) != len( s2 ):
#        return False
#
#    for i in range( len( s1 ) ):
#        if not compare_nt( s1[i], s2[i] ):
#            return False
#
#    return True

'''
@author: M. Bernt

This is a confidential release. Do not redistribute without 
permission of the author (bernt@informatik.uni-leipzig.de).
'''

from operator import itemgetter
import uas.fasta
import sys

names = {'Ala':'A', 'Arg':'R', 'Asn':'N', 'Asp':'D', 'Cys':'C', 'Glu':'E', 'Gln':'Q', 'Gly':'G', 'His':'H', \
       'Ile':'I', 'Leu':'L', 'Lys':'K', 'Met':'M', 'Phe':'F', 'Pro':'P', 'Ser':'S', 'Thr':'T', 'Trp':'W', \
       'Tyr':'Y', 'Val':'V' }

def parse( fname, skipintron = True ):
    trnas = []

    f = open( fname )
    lines = f.readlines()

    for l in lines:
        l = l.lstrip().rstrip().split()
        if l[6] != '0' or l[7] != '0':
            continue

        if l[2] <= l[3]:
            start = int( l[2] )
            end = int( l[3] )
            strand = 1
        else:
            start = int( l[3] )
            end = int( l[2] )
            strand = -1

        if l[4] in names:
            name = names[l[4]]
        else:
            name = l[4]

        if name == "S":
            if uas.fasta.compare_seq( l[5], "NCT" ):
                name = "S1"
            elif uas.fasta.compare_seq( l[5], "NGA" ):
                name = "S2"
            else:
                name = "S"
                sys.stderr.write( "warning non standard Ser %s" % l[5] )

        if name == "L":
            if uas.fasta.compare_seq( l[5], "NAG" ):
                name = "L1"
            elif uas.fasta.compare_seq( l[5], "YAA" ):
                name = "L2"
            else:
                name = "L"
                sys.stderr.write( "warning non standard Leu %s\n" % l[5] )

        trnas.append( {'no': int( l[1] ), 'start':start, 'end':end, 'strnad':reverse, 'name':name, 'acod':l[5], 'istart':int( l[6] ), 'iend':int( l[7] ), 'score':float( l[8] )} )

    trnas.sort( key = itemgetter( 'start' ) )
    f.close()
    return trnas



#!/bin/bash

mvn clean package -Denvironment.type=env

export MAVEN_OPTS="-Xss600m -Xms1G -Xmx30G -XX:-UseGCOverheadLimit"

mvn exec:exec -Dexec.args="-classpath %classpath de.uni_leipzig.informatik.pacosy.mitos.core.util.tools.ControlParameterProgramExecutor"



#!/bin/bash
args=("$@")
ARGS=${args[@]}

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`


cd ${BIN}
mvn clean package -Denvironment.type=env
export MAVEN_OPTS="-Xss600m -Xms1G -Xmx30G -XX:-UseGCOverheadLimit"
mvn exec:exec -Dexec.args="-classpath %classpath de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator ${ARGS}"

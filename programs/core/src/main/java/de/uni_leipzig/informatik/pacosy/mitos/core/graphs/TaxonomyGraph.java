package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.RecordMapAnalyzer;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Scope;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Column;
import org.apache.tinkerpop.gremlin.structure.T;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.attribute.Text;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.core.schema.Mapping;
import org.janusgraph.graphdb.database.management.ManagementSystem;

import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.desc;
import static org.apache.tinkerpop.gremlin.process.traversal.Operator.sum;

public class TaxonomyGraph extends Graph {

    public enum TAXONOMY_NODE_ATTRIBUTES implements NODE_ATTRIBUTES {
        //FLags
        LCA("lca"),
        PERSISTED("persisted"),
        //common attributes of each node
        TAXID("taxId"),
        DIVID("divId"), //taxonomy database division id
        GENID("genId"), //GenBank genetic code id
        RANK("rank"), //rank of this node (superkingdom, kingdom, ...)
        //class names
        AUTHORITY("authority"),
        ACRONYM("acronym"),
        GENBANK_SYNONYM("genbank_synonym"),
        BLAST_NAME("blast_name"),
        SCIENTIFIC_NAME("scientific_name"),
        IN_PART("in_part"),
        INCLUDES("includes"),
        ANAMORPH("anamorph"),
        GENBANK_ACRONYM("genbank_acronym"),
        SYNONYM("synonym"),
        EQUIVALENT_NAME("equivalent_name"),
        TYPE_MATERIAL("type_material"),
        TELEOMORPH("teleomorph"),
        COMMON_NAME("common_name"),
        GENBANK_COMMON_NAME("genbank_common_name");

        public final String identifier;
        public String getIdentifier() {
            return identifier;
        }
        TAXONOMY_NODE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum TAXONOMY_SUBGRAPH_NODE_ATTRIBUTES  implements NODE_ATTRIBUTES {

        DEPTH("depth"),
        LEAVE("leave");
        public final String identifier;
        public String getIdentifier() {
            return identifier;
        }
        TAXONOMY_SUBGRAPH_NODE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum TAXONOMY_SUBGRAPH_EDGE_ATTRIBUTES  implements EDGE_ATTRIBUTES {
        HOP_COUNT("hop_count");
        public final String identifier;
        public String getIdentifier() {
            return identifier;
        }
        TAXONOMY_SUBGRAPH_EDGE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum TAXONOMY_SUBGRAPH_EDGE_LABEL   {
        MERGE_EDGE("merge_edge");
        public final String identifier;

        TAXONOMY_SUBGRAPH_EDGE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum TAXONOMY_EDGE_LABEL   {
        HAS_PARENT_EDGE("has_parent");
        public final String identifier;

        TAXONOMY_EDGE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }
//180000
    private static final List<String> TAXONOMY_GROUP_NAME_IDENTIFIERS = new ArrayList<>(Arrays.asList(
            TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier,
            TAXONOMY_NODE_ATTRIBUTES.BLAST_NAME.identifier,
            TAXONOMY_NODE_ATTRIBUTES.COMMON_NAME.identifier,
            TAXONOMY_NODE_ATTRIBUTES.SYNONYM.identifier,
            TAXONOMY_NODE_ATTRIBUTES.EQUIVALENT_NAME.identifier,
            TAXONOMY_NODE_ATTRIBUTES.GENBANK_COMMON_NAME.identifier,
            TAXONOMY_NODE_ATTRIBUTES.GENBANK_SYNONYM.identifier,
            TAXONOMY_NODE_ATTRIBUTES.GENBANK_ACRONYM.identifier,
            TAXONOMY_NODE_ATTRIBUTES.AUTHORITY.identifier,
            TAXONOMY_NODE_ATTRIBUTES.ACRONYM.identifier,
            TAXONOMY_NODE_ATTRIBUTES.IN_PART.identifier,
            TAXONOMY_NODE_ATTRIBUTES.INCLUDES.identifier,
            TAXONOMY_NODE_ATTRIBUTES.ANAMORPH.identifier,
            TAXONOMY_NODE_ATTRIBUTES.TYPE_MATERIAL.identifier,
            TAXONOMY_NODE_ATTRIBUTES.TELEOMORPH.identifier

    ));

    public class TaxonomicGroup {

        private String identifier;
        private String name;

        public String getScientificName() {
            if(identifier.equals(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier)) {
                return name;
            }
            else {
                return (String) g.V().has(identifier,name).
                        values(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier).next();
            }
        }
        public String getIdentifier() {
            return identifier;
        }

        public void setIdentifier(String identifier) {
            this.identifier = identifier;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public TaxonomicGroup(String identifier, String name) {
            this.identifier = identifier;
            this.name = name;
            init();
        }

        public TaxonomicGroup(TAXONOMY_NODE_ATTRIBUTES identifier, String name) {
            this.identifier = identifier.identifier;
            this.name = name;
            init();
        }

        private void init() {
            if(!TAXONOMY_GROUP_NAME_IDENTIFIERS.contains(identifier)) {
                LOGGER.error("Invalid identifier " + identifier);
                System.exit(-1);
            }
        }

        public String toString() {
            return identifier+"="+name;
        }


    }

    private static final int BULK_CHOP_SIZE = 10000;
    private Dataset<RecordTaxidMappingDT> recordTaxidMapping;
    public static String KEYSPACE = "taxonomy";


    TaxonomyGraph() {
        super();
    }

    @Override
    protected void initialize() {
        super.initialize();
        initializeRecordTaxidMapping();
    }


    private void initializeRecordTaxidMapping() {
        recordTaxidMapping = recordAttributes.select(ColumnIdentifier.RECORD_ID, ColumnIdentifier.TAXID).
                as(Encoders.bean(RecordTaxidMappingDT.class)).cache();
    }

    public void createSchema() {

        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // vertex attributes
        final PropertyKey lca = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.LCA.identifier).dataType(Boolean.class).make();
        final PropertyKey persisted = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier).dataType(Boolean.class).make();
        final PropertyKey taxid = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).dataType(Integer.class).make();
        final PropertyKey divid = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.DIVID.identifier).dataType(Integer.class).make();
        final PropertyKey genid = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.GENID.identifier).dataType(Integer.class).make();
        final PropertyKey rank = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.RANK.identifier).dataType(String.class).make();
        final PropertyKey authority = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.AUTHORITY.identifier).dataType(String.class).make();
        final PropertyKey acronym = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.ACRONYM.identifier).dataType(String.class).make();
        final PropertyKey genbank_synonym = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.GENBANK_SYNONYM.identifier).dataType(String.class).make();
        final PropertyKey blast_name = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.BLAST_NAME.identifier).dataType(String.class).make();
        final PropertyKey scientific_name = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier).dataType(String.class).make();
        final PropertyKey inpart = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.IN_PART.identifier).dataType(String.class).make();
        final PropertyKey includes = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.INCLUDES.identifier).dataType(String.class).make();
        final PropertyKey anamorph = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.ANAMORPH.identifier).dataType(String.class).make();
        final PropertyKey genbank_acronym = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.GENBANK_ACRONYM.identifier).dataType(String.class).make();
        final PropertyKey synonym = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.SYNONYM.identifier).dataType(String.class).make();
        final PropertyKey equivalent_name = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.EQUIVALENT_NAME.identifier).dataType(String.class).make();
        final PropertyKey type_material = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.TYPE_MATERIAL.identifier).dataType(String.class).make();
        final PropertyKey teleomorph = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.TELEOMORPH.identifier).dataType(String.class).make();
        final PropertyKey common_name = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.COMMON_NAME.identifier).dataType(String.class).make();
        final PropertyKey genbank_common_name = management.makePropertyKey(TAXONOMY_NODE_ATTRIBUTES.GENBANK_COMMON_NAME.identifier).dataType(String.class).make();

        // vertex indecees
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.LCA.identifier, Vertex.class).addKey(lca).buildCompositeIndex();
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier, Vertex.class).addKey(persisted).buildCompositeIndex();
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier, Vertex.class).addKey(taxid).unique().buildCompositeIndex();
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.DIVID.identifier, Vertex.class).addKey(divid).buildCompositeIndex();
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.GENID.identifier, Vertex.class).addKey(genid).buildCompositeIndex();
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.RANK.identifier, Vertex.class).addKey(rank).buildCompositeIndex();

        // names
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.AUTHORITY.identifier, Vertex.class).addKey(authority, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.ACRONYM.identifier, Vertex.class).addKey(acronym, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.GENBANK_SYNONYM.identifier, Vertex.class).addKey(genbank_synonym, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.BLAST_NAME.identifier, Vertex.class).addKey(blast_name, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier, Vertex.class).addKey(scientific_name, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.IN_PART.identifier, Vertex.class).addKey(inpart, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.INCLUDES.identifier, Vertex.class).addKey(includes, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.ANAMORPH.identifier, Vertex.class).addKey(anamorph, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.GENBANK_ACRONYM.identifier, Vertex.class).addKey(genbank_acronym, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.SYNONYM.identifier, Vertex.class).addKey(synonym, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.EQUIVALENT_NAME.identifier, Vertex.class).addKey(equivalent_name, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.TYPE_MATERIAL.identifier, Vertex.class).addKey(type_material, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.TELEOMORPH.identifier, Vertex.class).addKey(teleomorph, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.COMMON_NAME.identifier, Vertex.class).addKey(common_name, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");
        management.buildIndex(TAXONOMY_NODE_ATTRIBUTES.GENBANK_COMMON_NAME.identifier, Vertex.class).addKey(genbank_common_name, Mapping.TEXTSTRING.asParameter()).buildMixedIndex("search");


        management.makeEdgeLabel("has_parent").multiplicity(Multiplicity.SIMPLE).make();

        management.commit();

        System.out.println("Done creating indecees");

        try {
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.LCA.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.DIVID.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.GENID.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.RANK.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.AUTHORITY.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.ACRONYM.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.GENBANK_SYNONYM.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.BLAST_NAME.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.IN_PART.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.INCLUDES.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.ANAMORPH.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.GENBANK_ACRONYM.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.SYNONYM.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.EQUIVALENT_NAME.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.TYPE_MATERIAL.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.TELEOMORPH.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.COMMON_NAME.identifier).call();
            ManagementSystem.awaitGraphIndexStatus(graph, TAXONOMY_NODE_ATTRIBUTES.GENBANK_COMMON_NAME.identifier).call();


            System.out.println("Done updating indecees");

        } catch (InterruptedException  i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

    }

    protected boolean schemaGenerationSuccessful() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        List<JanusGraphIndex> vertexIndecees = (List)management.getGraphIndexes(Vertex.class);
        for(JanusGraphIndex i : vertexIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        LOGGER.info("All indecees were successfully generated!");
        return true;
    }

    public void persist(Map<Integer, Map<String,Object>> nodes, Map<Integer,Integer> edges, Map<Integer,Map<String,String>> names) {
        long startTime = System.currentTimeMillis();
        g = graph.traversal();

        System.out.println("Nodes: " + nodes.size());
        int counter = 0;
        for(Map.Entry<Integer, Map<String,Object>> e: nodes.entrySet()) {


            Vertex v = g.addV().property(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,e.getKey()).
                    property(TAXONOMY_NODE_ATTRIBUTES.RANK.identifier,e.getValue().get(TAXONOMY_NODE_ATTRIBUTES.RANK.identifier)).
                    property(TAXONOMY_NODE_ATTRIBUTES.DIVID.identifier,e.getValue().get(TAXONOMY_NODE_ATTRIBUTES.DIVID.identifier)).
                    property(TAXONOMY_NODE_ATTRIBUTES.GENID.identifier,e.getValue().get(TAXONOMY_NODE_ATTRIBUTES.GENID.identifier)).next();
            Map<String,String> n = names.get(e.getKey());
            if(n != null) {
                for(Map.Entry<String,String> vals: n.entrySet()) {
                    g.V(v).property(vals.getKey(),vals.getValue()).iterate();
                }
            }
            if(counter % BULK_CHOP_SIZE == 0) {
                g.tx().commit();
                System.out.println(counter);
                System.out.println(((double)counter)/nodes.size());
            }
            counter++;

        }
        g.tx().commit();
        System.out.println("Done with nodes");
        counter = 0;
        for(Map.Entry<Integer,Integer> e: edges.entrySet()) {
            g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,e.getKey()).as("v1").V().
                    has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,e.getValue()).as("v2").
                    addE("has_parent").from("v1").to("v2").iterate();
            if(counter % BULK_CHOP_SIZE == 0) {
                g.tx().commit();
                System.out.println(counter);
                System.out.println(((double)counter)/nodes.size());
            }
            counter++;
        }
        g.tx().commit();

        g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1).as("v").outE().filter(__.inV().where(P.eq("v"))).drop().iterate();
        g.tx().commit();
        long endtime = System.currentTimeMillis();

        System.out.println("Done with persistence: " + (endtime-startTime));
    }


    /**
     * Mark Subtree that belongs to persisted records
     */
    public void markPersistedSubgraph() {

        markLowestCommonAncestor();
        markSubgraphNodes();

    }

    /**
     * Mark the Lowest common ancestors of all nodes
     * with TAXONOMY_NODE_ATTRIBUTES.LCA property
     */
    private void markLowestCommonAncestor() {
        List<Integer> taxids = getTaxidsOfRecords();
        markLowestCommonAncestor(g,taxids);
    }

    /**
     * Mark the Lowest common ancestors of nodes with taxid within taxids
     * with TAXONOMY_NODE_ATTRIBUTES.LCA property
     * @param taxids
     */
    private void markLowestCommonAncestor(GraphTraversalSource g, List<Integer> taxids) {
        g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(getLowestCommonAncestor(taxids))).
                property(TAXONOMY_NODE_ATTRIBUTES.LCA.identifier,true).iterate();
        g.tx().commit();
    }

    /**
     * Mark the Lowest common ancestors of all nodes
     * with TAXONOMY_NODE_ATTRIBUTES.LCA property
     */
    private void markLowestCommonAncestor(GraphTraversalSource g) {
        List<Integer> taxids = getLeaveTaxIds(g);
        markLowestCommonAncestor(g,taxids);
    }

    /**
     * Mark the taxonomic subgraph spawned by all nodes with taxid within taxids with TAXONOMY_NODE_ATTRIBUTES.TAXID property
     */
    private void markSubgraphNodes() {
        List<Integer> taxids = getTaxidsOfRecords();
        g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                property(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true).
                repeat(__.out().property(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true)).
                until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1)).iterate();
        g.tx().commit();
    }

    /**
     * Mark the taxonomic subgraph spawned by  nodes with taxid within taxids with TAXONOMY_NODE_ATTRIBUTES.TAXID property
     * @param taxids
     */
    private void markSubgraphNodes(GraphTraversalSource g, List<Integer> taxids) {
        g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                property(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true).
                repeat(__.out().property(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true)).
                until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1)).iterate();
        g.tx().commit();
    }

    public Dataset<RecordTaxidMappingDT> getTaxidFileterdRecordTaxidMapping(Collection<?> ids) {
        return recordTaxidMapping.filter((FilterFunction<RecordTaxidMappingDT>)(r -> ids.contains(r.getTaxid())));
    }

    public Dataset<RecordTaxidMappingDT> getRecordFileterdRecordTaxidMapping(Collection<?> ids) {
        return recordTaxidMapping.filter((FilterFunction<RecordTaxidMappingDT>)(r -> ids.contains(r.getRecordId())));
    }

    public Dataset<Integer> getTaxidsOfRecordsDS() {
        return recordTaxidMapping.select(ColumnIdentifier.TAXID).distinct().as(Encoders.INT());
    }

    public List<Integer> getTaxidsOfRecords() {
        return getTaxidsOfRecordsDS().collectAsList();
    }

    public Dataset<Integer> getTaxidsOfRecordsDS(Collection recordIds) {
        return getRecordFileterdRecordTaxidMapping(recordIds).
                select(ColumnIdentifier.TAXID).distinct().as(Encoders.INT());
    }

    public List<Integer> getTaxidsOfRecords(Collection recordIds) {
        return getTaxidsOfRecordsDS(recordIds).collectAsList();
    }

    public Dataset<Integer> getRecordsOfTaxidsDS() {
        return recordTaxidMapping.select(ColumnIdentifier.RECORD_ID).as(Encoders.INT());
    }

    public List<Integer> getRecordsOfTaxids() {
        return getRecordsOfTaxidsDS().collectAsList();
    }

    public Dataset<Integer> getRecordsOfTaxidsDS(Collection<Integer> taxIds) {
        return getTaxidFileterdRecordTaxidMapping(taxIds).
                select(ColumnIdentifier.RECORD_ID).as(Encoders.INT());
    }

    public List<Integer> getRecordsOfTaxids(Collection<Integer> taxIds) {
        return getRecordsOfTaxidsDS().collectAsList();
    }

    public void getBranchtoNode(Integer taxid) {

        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1)).
                path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());

        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1)).path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());
        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());
        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());
//        List<Integer> taxids = new ArrayList<>(Arrays.asList(1,33213));
//        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids))).
//                filter(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxids.get(1))).path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());
//        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids))).
//                filter(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxids.get(1))).path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());
//        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid).repeat(__.out("has_parent")).times(50)
//                .path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).unfold().toList());


    }

    public void filterForTaxonomicGroup(String name, String identifier, Map<Integer,Integer> recordTaxidMapping) {
        Set<Integer> taxids = new HashSet<>(recordTaxidMapping.values());
        Integer taxid = (Integer)g.V().has(identifier,name).values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next();
        //g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).as("s").repeat(__.out("has_parent")).until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxid))
    }

    public void test() {

        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.LCA.identifier,true).values(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier,TAXONOMY_NODE_ATTRIBUTES.RANK.identifier,TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).fold().toList());
        List l = g.V().has(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true).local(__.values(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier,TAXONOMY_NODE_ATTRIBUTES.RANK.identifier,TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).fold()).toList();
        l.forEach(t -> System.out.println(t));

        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1).as("v").outE().filter(__.inV().where(P.eq("v"))).hasNext());
        System.out.println();
        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(7739,6669,8797)).valueMap().toList());

        System.out.println(g.V().count().next());
        System.out.println(g.V().has("lca",true).values("scientific_name","rank","taxId").fold().toList());
        System.out.println(g.V().has("persisted",true).values("scientific_name","rank","taxId").fold().toList());
//        Edge e = (Edge) g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,1).as("v").outE().filter(__.inV().where(P.eq("v"))).next();
//
//        System.out.println(g.E(e).inV().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next());
//        System.out.println(g.E(e).outV().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next());
        //System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME.identifier,"Metazoa").valueMap().next());
//        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,7).valueMap().next());
//        System.out.println(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,7).out().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next());
    }

    public List getPersistedNodes() {
        return g.V().has(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true).id().toList();
    }

    private GraphTraversal getLowestCommonAncestorTraversal(GraphTraversal<Vertex,Vertex> graphTraversal, List<Integer> taxids) {
        return graphTraversal.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier, taxids.get(0)).
                repeat(__.out("has_parent")).emit().as("x").
                V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids.subList(1,taxids.size()))).
                repeat(__.out("has_parent")).emit(__.where(P.eq("x"))).
                group().
                by(__.select("x")).
                by(__.path().count(Scope.local).fold()).
                unfold().filter(__.select(Column.values).count(Scope.local).is(taxids.size()-1)).
                order().by(__.select(Column.values).
                unfold().sum()).
                select(Column.keys).limit(1).unfold();
    }

    private GraphTraversal getRootTraversal(GraphTraversalSource g) {
        return getRootTraversal(g.V());
    }

    private GraphTraversal getRootTraversal(GraphTraversal graphTraversal) {
        return graphTraversal.filter(__.out().count().is(P.eq(0)));
    }

    public List getRootTaxIds() {
        return getRootTaxIds(g);
    }

    public List getRootTaxIds(GraphTraversalSource g) {
        return getRootTraversal(g).values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
    }

    /**
     * Get taxId of parent from node with taxId
     * @param taxId
     * @return
     */
    public Integer getParent(Integer taxId) {
        return getParent(g,taxId);
    }

    /**
     * Get taxId of parent from node with taxId
     * @param g
     * @param taxId
     * @return
     */
    public Integer getParent(GraphTraversalSource g, Integer taxId) {
        List parentTaxid = g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxId).
                out().
                values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
        if(parentTaxid.size() > 1) {
            LOGGER.error("Encountered node with more than one parent: " + taxId);
            System.exit(-1);
        }
        else if(parentTaxid.size() == 0) {
            return null;
        }
        return (Integer) parentTaxid.get(0);
    }

    /**
     * Get taxId of predecessor being steps away from node with taxId
     * @param taxId
     * @return
     */
    public Integer getPredecessor(Integer taxId, int steps) {
        List parentTaxid = g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxId).repeat(__.out()).times(steps).
                values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
        if(parentTaxid.size() > 1) {
            LOGGER.error("Encountered node with more than one parent: " + taxId);
            System.exit(-1);
        }
        else if(parentTaxid.size() == 0) {
            return null;
        }
        return (Integer) parentTaxid.get(0);
    }

    /**
     * Get taxId of predecessor being steps away from node with taxId
     * @param g
     * @param taxId
     * @return
     */
    public Integer getPredecessor(GraphTraversalSource g, Integer taxId, int steps) {
        List parentTaxid = g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxId).repeat(__.out()).times(steps).
                values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
        if(parentTaxid.size() > 1) {
            LOGGER.error("Encountered node with more than one parent: " + taxId);
            System.exit(-1);
        }
        else if(parentTaxid.size() == 0) {
            return null;
        }
        return (Integer) parentTaxid.get(0);
    }

    /**
     * retrurn taxids of lowest common ancestors of all nodes
     * @return
     */
    public List getLowestCommonAncestor() {

        return g.V().has(TAXONOMY_NODE_ATTRIBUTES.LCA.identifier,true).values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();

    }

    /**
     * retrurn taxids of lowest common ancestors
     * for taxonomic subgraph spawned by nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getLowestCommonAncestor(List<Integer> taxids) {

        return getLowestCommonAncestor(g,taxids);

    }

    /**
     * retrurn taxids of lowest common ancestors
     * for taxonomic subgraph spawned by nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getLowestCommonAncestor(GraphTraversalSource g, List<Integer> taxids) {

        return getLowestCommonAncestorTraversal(g.V(),taxids).values("taxId").toList();
    }

    /**
     * Retrurn taxids of lowest common ancestors
     * for taxonomic subgraph spawned by nodes with taxid within taxids and contained in nodesToInclude
     * @param taxids
     * @param nodesToInclude
     * @return
     */
    public List getLowestCommonAncestor(List<Integer> taxids, List nodesToInclude) {
        return getLowestCommonAncestor(g,taxids,nodesToInclude);

    }

    /**
     * Retrurn taxids of lowest common ancestors
     * for taxonomic subgraph spawned by nodes with taxid within taxids and contained in nodesToInclude
     * @param taxids
     * @param nodesToInclude
     * @return
     */
    public List getLowestCommonAncestor(GraphTraversalSource g, List<Integer> taxids, List nodesToInclude) {
        return getLowestCommonAncestorTraversal(g.V(nodesToInclude),taxids).values("taxId").toList();
    }

    /**
     * Get ids of nodes that are part of a bottom up tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getUpwardsBranchtoNodes(List<Integer> taxids) {
        return  getUpwardsBranchtoNodes(g,taxids);
    }

    /**
     * Get ids of nodes that are part of a bottom up tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getUpwardsBranchtoNodes(GraphTraversalSource g, List<Integer> taxids) {

        return  g.V().has("taxId",P.within(taxids)).aggregate("t").by(T.id).
                repeat(__.out("has_parent").aggregate("t").by(T.id)).
                until(__.has("taxId",1)).cap("t").unfold().toList();
    }

    /**
     * Get ids of nodes contained in nodesToInclude that are part of a bottom up tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getUpwardsBranchtoNodes(List<Integer> taxids, List nodesToInclude) {

        return  getUpwardsBranchtoNodes(g,taxids,nodesToInclude);
    }

    /**
     * Get ids of nodes contained in nodesToInclude that are part of a bottom up tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getUpwardsBranchtoNodes(GraphTraversalSource g, List<Integer> taxids, List nodesToInclude) {

        return  g.V(nodesToInclude).has("taxId",P.within(taxids)).aggregate("t").by(T.id).
                repeat(__.out("has_parent").aggregate("t").by(T.id)).
                until(__.has("taxId",1)).cap("t").unfold().toList();
    }

    private GraphTraversal<?,?> getDownwardsTraversal(GraphTraversal graphTraversal) {
        return graphTraversal.repeat(__.in()).until(__.in().count().is(0));
    }

    public List getDownwardsPathestoNodes(List<Integer> taxids) {
        return getDownwardsPathestoNodes(g,taxids);
    }

    public List getDownwardsPathestoNodes(GraphTraversalSource g, List<Integer> taxids) {
        return getDownwardsTraversal(g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids))).
                path().by(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
    }

    /**
     * Get ids of nodes that are part of a top down tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getDownwardsBranchtoNodes(List<Integer> taxids) {

        return getDownwardsBranchtoNodes(g,taxids);
    }

    /**
     * Get ids of nodes that are part of a top down tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getDownwardsBranchtoNodes(GraphTraversalSource g, List<Integer> taxids) {

        return g.V().has("taxId",P.within(taxids)).aggregate("t").by(T.id).
                repeat(__.in("has_parent").aggregate("t").by(T.id)).cap("t").unfold().toList();
    }

    /**
     * Get ids of nodes contained in nodesToInclude that are part of a top down tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getDownwardsBranchtoNodes(List<Integer> taxids, List nodesToInclude) {
        return getDownwardsBranchtoNodes(g,taxids,nodesToInclude);
    }

    /**
     * Get ids of nodes contained in nodesToInclude that are part of a top down tree traversal,
     * starting at nodes with taxid within taxids
     * @param taxids
     * @return
     */
    public List getDownwardsBranchtoNodes(GraphTraversalSource g, List<Integer> taxids, List nodesToInclude) {

        return g.V(nodesToInclude).has("taxId",P.within(taxids)).aggregate("t").by(T.id).
                repeat(__.in("has_parent").aggregate("t").by(T.id)).cap("t").unfold().toList();
    }


    public List getNodesToRecords(Collection recordIds) {

        return filterNodeProperty(TAXONOMY_NODE_ATTRIBUTES.TAXID,getTaxidsOfRecords(recordIds));


    }

    //    private String getTaxonomicGroupNameExactMatch(GraphTraversalSource g, String identifier, String name) {
    //        List taxids;
    //        if(!TAXONOMY_GROUP_NAME_IDENTIFIERS.contains(identifier)) {
    //            LOGGER.error("Invalid group identifier " + identifier);
    //            LOGGER.error("Must be within: " + TAXONOMY_GROUP_NAME_IDENTIFIERS);
    //            return null;
    //        }
    //        taxids = g.V().has(identifier, name).values("taxId").toList();
    //        if(taxids.size() == 0) {
    //            LOGGER.warn("No matching taxonomic group found for " + identifier + "=" + name);
    //        }
    //        else if(taxids.size() > 1) {
    //            LOGGER.warn("Multiple matches found");
    //        }
    //
    //        return taxids;
    //    }
    /**
     * For the provided taxId get the scientific name
     * If the specified group is invalid or if there is no such group for this taxid, null is returned
     * @param taxId
     * @return
     */
    public String getTaxonomicName(Integer taxId) {
        return getTaxonomicName(g,taxId);
    }

    /**
     * For the provided taxId get the scientific name
     * If the specified group is invalid or if there is no such group for this taxid, null is returned
     * @param taxId
     * @param g
     * @return
     */
    public String getTaxonomicName(GraphTraversalSource g, Integer taxId) {
        return getTaxonomicName(g,taxId,TAXONOMY_NODE_ATTRIBUTES.SCIENTIFIC_NAME);
    }

    /**
     * For the provided taxId Get taxonomic name of specified taxonomic group (via taxonomy_node_attribute)
     * If the specified group is invalid or if there is no such group for this taxid, null is returned
     * @param taxId
     * @param taxonomy_node_attribute
     * @return
     */
    public String getTaxonomicName(Integer taxId, TAXONOMY_NODE_ATTRIBUTES taxonomy_node_attribute) {
        return getTaxonomicName(g,taxId,taxonomy_node_attribute);
    }


    /**
     * For the provided taxId Get taxonomic name of specified taxonomic group (via taxonomy_node_attribute)
     * If the specified group is invalid or if there is no such group for this taxid, null is returned
     * @param taxId
     * @param taxonomy_node_attribute
     * @param g
     * @return
     */
    public String getTaxonomicName(GraphTraversalSource g, Integer taxId, TAXONOMY_NODE_ATTRIBUTES taxonomy_node_attribute) {
        String taxonomicName = taxonomy_node_attribute.identifier;
        if(!TAXONOMY_GROUP_NAME_IDENTIFIERS.contains(taxonomicName)) {
            LOGGER.error("Invalid taxonomic class");
            return null;
        }
        List<String> names =  g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,taxId).values(taxonomicName).
                map(s -> s.get().toString()).toList();
        if(names.size() == 0) {
            return null;
        }
        return names.get(0);
    }

    private List getTaxonomicGroupNameAsIs(GraphTraversalSource g, String identifier, String name, Function<String,P<String>> stringFunction) {
//        System.out.println(name);
        return g.V().has(identifier,stringFunction.apply(name)).values(identifier).toList();
    }

    private List getTaxonomicGroupName(GraphTraversalSource g, String identifier, String name,
                                       Function<String,String> nameMapper, Function<String,P<String>> stringFunction) {

        String nameToTry;
        nameToTry = name;
        List discovered = getTaxonomicGroupNameAsIs(g,identifier,nameMapper.apply(nameToTry),stringFunction);


        int index = nameToTry.lastIndexOf(" ");
        while (discovered.size() == 0 && index != -1) {
            nameToTry = nameToTry.substring(0,index);
//            System.out.println(nameToTry);
            index = nameToTry.lastIndexOf(" ");
            discovered = getTaxonomicGroupNameAsIs(g,identifier,nameMapper.apply(nameToTry),stringFunction);
        }
        return discovered;
    }

    public String getTaxonomicGroupName(String identifier, String name) {
        return getTaxonomicGroupName(g,identifier,name);
    }

    public String getTaxonomicGroupName(GraphTraversalSource g, String identifier, String name) {


        List names = getTaxonomicGroupName(g,identifier,name,n-> Auxiliary.getCaseInsensitiveRegex(n), t -> Text.textRegex(t));
        if(names.size() == 0) {
            return null;
        }
        return (String)names.get(0);
    }

    public TaxonomicGroup getTaxonomicGroupName(String name) {
        return getTaxonomicGroupName(g,name);
    }

    public TaxonomicGroup getTaxonomicGroupName(GraphTraversalSource g, String name) {
        String nameToTry = null;

        for(int i = 0; i < TAXONOMY_GROUP_NAME_IDENTIFIERS.size(); i++) {
            nameToTry = getTaxonomicGroupName(g,TAXONOMY_GROUP_NAME_IDENTIFIERS.get(i),name);
            if(nameToTry != null) {
                return new TaxonomicGroup(TAXONOMY_GROUP_NAME_IDENTIFIERS.get(i),nameToTry);
            }
        }
        return null;
    }

    public List getTaxonomicGroupNameFuzzy(String identifier, String name) {

        return getTaxonomicGroupName(g,identifier,name,String::toString, t -> Text.textFuzzy(t));
    }

    public List getTaxonomicGroupNameFuzzy(GraphTraversalSource g, String identifier, String name) {

        return getTaxonomicGroupName(g,identifier,name,String::toString, t -> Text.textContainsFuzzy(t));
    }

    public List getTaxonomicGroupNameFuzzy(String name) {
        return getTaxonomicGroupNameFuzzy(g,name);
    }

    public List getTaxonomicGroupNameFuzzy(GraphTraversalSource g, String name) {
        List nameToTry = null;
        for(int i = 0; i < TAXONOMY_GROUP_NAME_IDENTIFIERS.size(); i++) {
            nameToTry = getTaxonomicGroupNameFuzzy(g,TAXONOMY_GROUP_NAME_IDENTIFIERS.get(i),name);
            if(nameToTry.size() != 0) {
                ArrayList<TaxonomicGroup> taxonomicGroups = new ArrayList<>();
                for(Object n : nameToTry) {
                    taxonomicGroups.add(new TaxonomicGroup(TAXONOMY_GROUP_NAME_IDENTIFIERS.get(i),(String)n));
                }
                return taxonomicGroups;
            }
        }
        return null;
    }


    /**
     * Get taxid to provided taxonmic group
     * @param group
     * @return
     */
    public Integer getTaxIdForTaxonomicGroup(TaxonomicGroup group) {
        return getTaxIdForTaxonomicGroup(g,group);
    }

    /**
     * Get taxid to provided taxonmic group
     * @param g
     * @param group
     * @return
     */
    public Integer getTaxIdForTaxonomicGroup(GraphTraversalSource g, TaxonomicGroup group) {
        String nameToTry = getTaxonomicGroupName(g,group.identifier,group.name);
        if(nameToTry != null) {
            return (Integer) g.V().has(group.identifier,group.name).
                    values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next();
        }
//        nameToTry = name;
//        List taxids = getTaxIdForTaxonomicGroupExactMatch(g, identifier,nameToTry);
//        int index = nameToTry.lastIndexOf(" ");
//        while (taxids.size() == 0 && index != -1) {
//            nameToTry = nameToTry.substring(0,index);
//            System.out.println(nameToTry);
//            index = nameToTry.lastIndexOf(" ");
//            taxids = getTaxIdForTaxonomicGroupExactMatch(g, identifier,nameToTry);
//        }


        return null;
    }

    /**
     * Get taxid to taxonmic group with unkown identifier and provided name
     * @param name
     * @return
     */
    public Integer getTaxIdForTaxonomicName(String name) {

        return getTaxIdForTaxonomicName(g,name);
    }

    /**
     * Get taxid to taxonmic group with unkown identifier and provided name
     * @param g
     * @param name
     * @return
     */
    public Integer getTaxIdForTaxonomicName(GraphTraversalSource g, String name) {

        TaxonomicGroup taxonomicGroup = getTaxonomicGroupName(g,name);
        if(taxonomicGroup != null) {
            return (Integer) g.V().has(taxonomicGroup.identifier,taxonomicGroup.name).
                    values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).next();
        }
        return null;
    }

    /**
     * Get taxid to taxonmic group with unkown identifier and provided names
     * @param names
     * @return
     */
    public List<Integer> getTaxidsForTaxonomicName(Collection<String> names) {
        return getTaxidsForTaxonomicName(g,names);
    }

    /**
     * Get taxid to taxonmic group with unkown identifier and provided names
     * @param g
     * @param names
     * @return
     */
    public List<Integer> getTaxidsForTaxonomicName(GraphTraversalSource g, Collection<String> names) {
        List<Integer> taxids = new ArrayList<>();
        for(String name : names) {
            if(name != null){
                Integer taxid = getTaxIdForTaxonomicName(g,name);
                if(taxid != null) {
                    taxids.add(taxid);
                }
            }
        }
        return taxids;
    }

    /**
     * Get taxids to provided taxonmic groups
     * @param groups
     * @return
     */
    public List<Integer> getTaxidsForTaxonomicGroup(Collection<TaxonomicGroup> groups) {
        return getTaxidsForTaxonomicGroup(g,groups);
    }

    /**
     * Get taxids to provided taxonmic groups
     * @param g
     * @param groups
     * @return
     */
    public List<Integer> getTaxidsForTaxonomicGroup(GraphTraversalSource g, Collection<TaxonomicGroup> groups) {
        List<Integer> taxids = new ArrayList<>();

        for(TaxonomicGroup group: groups) {
            if(group.name != null) {
                Integer taxid = getTaxIdForTaxonomicGroup(g,group);
                if(taxid != null) {
                    taxids.add(taxid);
                }
            }

        }
        return taxids;
    }




    /**
     * Get leave nodes
     * @param g
     * @return
     */
    public GraphTraversal<Vertex,Vertex> getLeaveTraversal(GraphTraversalSource g) {
        return g.V().filter(__.in().count().is(P.eq(0)));
    }

    /**
     * Get taxIds of leave nodes
     * @return
     */
    public List getLeaveTaxIds() {
        return getLeaveTaxIds(g);
    }

    /**
     * Get taxIds of leave nodes
     * @param g
     * @return
     */
    public List getLeaveTaxIds(GraphTraversalSource g) {
        return getLeaveTraversal(g).values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
    }

    /**
     * Get Mark leave nodes in subtree with "leave" property
     * @return
     */
    public void markLeaveNodes(GraphTraversalSource g) {
        getLeaveTraversal(g).property(TAXONOMY_SUBGRAPH_NODE_ATTRIBUTES.LEAVE.identifier,true).iterate();
    }


    /**
     * Get list of all contained taxids in subgraph g within provided taxids collection
     * @param taxids
     * @return
     */
    public List getContainedTaxids(Collection<Integer> taxids) {
        return getContainedTaxids(g,taxids);
    }

    /**
     * Get list of all contained persisted taxids in subgraph g within provided taxids collection
     * @param g
     * @param taxids
     * @return
     */
    public List getContainedTaxids(GraphTraversalSource g, Collection<Integer> taxids) {
        return g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,
                P.within(taxids)).values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
    }

    /**
     * Get list of all contained persisted records in subgraph g
     * @param g
     * @return
     */
    public List<Integer> getContainedRecords(GraphTraversalSource g) {
        // get all persisted taxids within subgraph g
        List taxids = getContainedTaxids(g,getTaxidsOfRecords());
        return getRecordsOfTaxids(taxids); //get the recordIds of these taxids
    }

    /**
     * Add Edges with label "merge_edge" and attribute "hop_count" between simple path start and end nodes
     * @param g
     */
    public void addMergeableBranches(GraphTraversalSource g) {
        g.withSack(0).V().filter(__.in().count().is(P.eq(0))).as("v1").
                repeat(__.out().sack(sum).by(__.constant(1))).
                until(__.in().count().is(P.gt(1))).
                        addE(TAXONOMY_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).
                        from("v1").
                        property(TAXONOMY_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier, __.sack()).

                        iterate();

        g.withSack(0).V().filter(__.in().count().is(P.gt(1))).as("v1").
                repeat(__.out().sack(sum).by(__.constant(1))).
                until(__.in().count().is(P.gt(1))).
                addE(TAXONOMY_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).
                from("v1").
                property(TAXONOMY_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier, __.sack()).
                iterate();
    }

    public void addTreeDepth(GraphTraversalSource g) {
        getRootTraversal(g.withSack(0).V()).
                property(TAXONOMY_SUBGRAPH_NODE_ATTRIBUTES.DEPTH.identifier,__.sack()).
                sack(sum).by(__.constant(1)).
                repeat(__.in().
                        property(TAXONOMY_SUBGRAPH_NODE_ATTRIBUTES.DEPTH.identifier,
                                __.sack()).sack(sum).by(__.constant(1))).
                iterate();
    }

    /**
     * Merge subgraph g1 and g2 into new Subgraph
     * @param g1
     * @param g2
     * @return New subgraph
     */
    public GraphTraversalSource mergeSubTrees(GraphTraversalSource g1, GraphTraversalSource g2) {

        String tempFileName = "/tmp/partGraph.xml";


        // get common taxids -> would lead to duplicate nodes
        List commonTaxids = g1.V().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList();
        commonTaxids.retainAll(g2.V().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).toList());


        // keys are taxids contained in commontaxids, values are taxids of predecessor nodes not contained in commontaxids
        Map<Object,Object> taxIdMapIn = (Map)g2.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(commonTaxids)).
                filter(__.in().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.without(commonTaxids))).
                group().
                by(__.values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier)).
                by(__.in().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).fold()).next();

        // keys are taxids contained in commontaxids, values are taxids of successor nodes not contained in commontaxids
        Map<Object,Object> taxIdMapOut = (Map)g2.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(commonTaxids)).
                filter(__.out().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.without(commonTaxids))).
                group().
                by(__.values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier)).
                by(__.out().values(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier).fold()).next();


        // create a deep copy of g2
        GraphTraversalSource g3 = createDeepCopySubGraph(g2);
        // drop common taxids
        g3.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(commonTaxids)).
                drop().iterate();

        writeToGraphML(tempFileName,g1);
        g3.io(tempFileName).read().iterate();
//        System.out.println(taxIdMapIn);
//        System.out.println(taxIdMapOut);

        for(Map.Entry<Object,Object> entry: taxIdMapIn.entrySet()) {
            Vertex v = g3.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,entry.getKey()).next();
            g3.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within((List)entry.getValue())).
                    addE(TAXONOMY_EDGE_LABEL.HAS_PARENT_EDGE.identifier).to(v).iterate();
        }

        for(Map.Entry<Object,Object> entry: taxIdMapOut.entrySet()) {
            Vertex v = g3.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,entry.getKey()).next();
            g3.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within((List)entry.getValue())).
                    addE(TAXONOMY_EDGE_LABEL.HAS_PARENT_EDGE.identifier).from(v).iterate();
        }
        return g3;

    }

    /**
     * Create a collapsed subtree with only "merge_edge" edges
     * @param g
     * @return
     */
    public GraphTraversalSource createCollapsedSubtree(GraphTraversalSource g) {
        addMergeableBranches(g);
        return ((TinkerGraph) g.E().hasLabel(TAXONOMY_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).
            subgraph("subtree").cap("subtree").next()).traversal();
    }

    /**
     * Decollapse all previously collapsed branches
     * @param g
     * @return
     */
    public GraphTraversalSource createDecollapsedSubTree(GraphTraversalSource g) {
        return createBottomUpSubtreeToRootFromTaxids(getLeaveTaxIds(g),getRootTaxIds(g));
    }

    /**
     * Create a subtree by traversing all nodes with taxid within taxids downwards
     * @param taxids
     * @return
     */
    public GraphTraversalSource createTopDownSubtreeFromTaxids(List<Integer> taxids) {
        return createTopDownSubtreeFromTaxids(g,taxids);
    }

    /**
     * Create a subtree by traversing all nodes  with taxid within taxids downwards
     * @param g
     * @param taxids
     * @return
     */
    public GraphTraversalSource createTopDownSubtreeFromTaxids(GraphTraversalSource g, List<Integer> taxids) {
        return ((TinkerGraph) g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                repeat(__.inE().subgraph("subTree").outV()).cap("subTree").next()).traversal();
    }

//    /**
//     * Create subtree by traversing all nodes with taxonomic groups as given via names downwards
//     * @param names List of scientific names
//     * @return
//     */
//    public GraphTraversalSource createTopDownSubtreeForTaxonomicGroup( List<String> names) {
//        return createTopDownSubtreeForTaxonomicGroup(g,names);
//    }
//
//    /**
//     * Create subtree by traversing all nodes with taxonomic groups as given via names downwards
//     * @param g
//     * @param names List of scientific names
//     * @return
//     */
//    public GraphTraversalSource createTopDownSubtreeForTaxonomicGroup(GraphTraversalSource g, List<String> names) {
//        List<Integer> taxids = getTaxidsForTaxonomicGroup(names);
//        return createTopDownSubtreeFromTaxids(g,taxids);
//    }
//
//    /**
//     * Create subtree by traversing all nodes with taxonomic groups as given via names downwards
//     * @param names Map where keys are taxonomic group identifiers (e.g. scientific name) and values are corresponding name
//     * @return
//     */
//    public GraphTraversalSource createTopDownSubtreeForTaxonomicGroup(Map<String,String> names) {
//        return createTopDownSubtreeForTaxonomicGroup(g,names);
//    }
//
//    /**
//     * Create subtree by traversing all nodes with taxonomic groups as given via names downwards
//     * @param g
//     * @param names Map where keys are taxonomic group identifiers (e.g. scientific name) and values are corresponding name
//     * @return
//     */
//    public GraphTraversalSource createTopDownSubtreeForTaxonomicGroup(GraphTraversalSource g, Map<String,String> names) {
//        List<Integer> taxids = getTaxidsForTaxonomicGroup(names);
//        return createTopDownSubtreeFromTaxids(g,taxids);
//    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until lca node.
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeFromTaxids(List<Integer> taxids) {
        return createBottomUpSubtreeFromTaxids(g,taxids);
    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until lca node.
     * @param g
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeFromTaxids(GraphTraversalSource g, List<Integer> taxids) {
        List endTaxids = getLowestCommonAncestor(g,taxids);
        return ((TinkerGraph) g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                repeat(__.outE().subgraph("subTree").inV()).
                until(__.has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(endTaxids))).
                cap("subTree").next()).traversal();
    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until root node.
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeToRootFromTaxids( Collection<Integer> taxids) {
        return createBottomUpSubtreeToRootFromTaxids(g,taxids);
    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until root node.
     * @param g
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeToRootFromTaxids(GraphTraversalSource g, Collection<Integer> taxids) {
        return ((TinkerGraph) g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                repeat(__.outE().subgraph("subTree").inV()).
                until(__.out().count().is(0)).
                cap("subTree").next()).traversal();
    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until root nodes given via rootTaxids.
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeToRootFromTaxids(Collection<Integer> taxids, Collection<Integer> rootTaxids) {
        return createBottomUpSubtreeToRootFromTaxids(g, taxids,rootTaxids);
    }

    /**
     * Create a subgraph spawned by nodes with taxid within taxids. Traverse upwards until root nodes given via rootTaxids.
     * @param g
     * @param taxids
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeToRootFromTaxids(GraphTraversalSource g, Collection<Integer> taxids, Collection<Integer> rootTaxids) {
        return ((TinkerGraph) g.V().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(taxids)).
                repeat(__.outE().subgraph("subTree").inV()).
                until(__.out().has(TAXONOMY_NODE_ATTRIBUTES.TAXID.identifier,P.within(rootTaxids))).
                cap("subTree").next()).traversal();
    }


    /**
     * Create a subgraph spawned by nodes whose taxid corresponds with a recordid wihtin recordIds
     * @param recordIds
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeFromRecords(Collection<Integer> recordIds) {

        return createBottomUpSubtreeFromRecords(g,recordIds);
    }

    /**
     * Create a subgraph spawned by nodes whose taxid corresponds with a recordid wihtin recordIds
     * @param g
     * @param recordIds
     * @return
     */
    public GraphTraversalSource createBottomUpSubtreeFromRecords(GraphTraversalSource g, Collection<Integer> recordIds) {
        return createBottomUpSubtreeFromTaxids(g, getTaxidsOfRecords(recordIds));

    }

    /**
     * Create a subtree spawned by nodes with persisted taxids
     * @return
     */
    public GraphTraversalSource createSubtreeFromPersistedNodes() {
        return createSubtreeFromPersistedNodes(g);
    }

    /**
     * Create a subtree spawned by nodes with persisted taxids
     * @param g
     * @return
     */
    public GraphTraversalSource createSubtreeFromPersistedNodes(GraphTraversalSource g) {
        return ((TinkerGraph) g.V().has(TAXONOMY_NODE_ATTRIBUTES.PERSISTED.identifier,true).
                outE().subgraph("subTree").
                cap("subTree").next()).traversal();
    }

    /**
     * Create a subtree of nodes with persisted taxids and taxId which are successors to a common parent.
     * This common parent is determined by traversing upwards steps many steps from taxId in a collapsed merged tree of persisted taxids and taxId.
     * @param taxId
     * @param steps
     * @return
     */
    public GraphTraversalSource createSubtreeFromPersistedNodes(Integer taxId, int steps) {
        GraphTraversalSource s = createBottomUpSubtreeFromTaxids(Collections.singletonList(taxId));
        GraphTraversalSource subTree = createCollapsedSubtree( mergeSubTrees(createSubtreeFromPersistedNodes(),
                s));
        subTree = createTopDownSubtreeFromTaxids(subTree,Collections.singletonList(getPredecessor(subTree,taxId,steps)));

//        writeToGraphML("completeTaxonomySubtree" + taxId+ ".xml",subTree);
        return subTree;

    }

    /**
     * Create a subtree of nodes with persisted taxids and taxId which are successors to a common parent.
     * This common parent is determined by traversing upwards one step from taxId in a collapsed merged tree of persisted taxids and taxId.
     * @param taxId
     * @return
     */
    public GraphTraversalSource createSubtreeFromPersistedNodes(Integer taxId) {
        return createSubtreeFromPersistedNodes(taxId,1);
    }

//    /**
//     * Create subtree spawned by nodes with taxonomic groups as given via names
//     * @param names Map where keys are taxonomic group identifiers (e.g. scientific name) and values are corresponding name
//     * @return
//     */
//    public GraphTraversalSource createBottomUpSubtreeForTaxonomicGroup( Map<String,String> names) {
//
//        return createBottomUpSubtreeForTaxonomicGroup(names);
//    }
//
//    /**
//     * Create subtree spawned by nodes with taxonomic groups as given via names
//     * @param g
//     * @param names Map where keys are taxonomic group identifiers (e.g. scientific name) and values are corresponding name
//     * @return
//     */
//    public GraphTraversalSource createBottomUpSubtreeForTaxonomicGroup(GraphTraversalSource g, Map<String,String> names) {
//        List<Integer> taxids = getTaxidsForTaxonomicGroup(names);
//
//        return createBottomUpSubtreeFromTaxids(g,taxids);
//    }
//
//    /**
//     * Create subtree spawned by nodes with taxonomic groups as given via names
//     * @param names List of names
//     * @return
//     */
//    public GraphTraversalSource createBottomUpSubtreeForTaxonomicGroup(List<String> names) {
//
//        return createBottomUpSubtreeForTaxonomicGroup(g,names);
//    }
//
//    /**
//     * Create subtree spawned by nodes with taxonomic groups as given via names
//     * @param g
//     * @param names List of names
//     * @return
//     */
//    public GraphTraversalSource createBottomUpSubtreeForTaxonomicGroup(GraphTraversalSource g, List<String> names) {
//        List<Integer> taxids = getTaxidsForTaxonomicGroup(names);
//        return createBottomUpSubtreeFromTaxids(g,taxids);
//    }
//




//    private void showPath(int taxId, String property) {
////        System.out.println(g.V().has("taxId",taxId).repeat(__.out("has_parent")).until(__.has("taxId",834)).path().unfold().toList());
//        System.out.println(g.V().has("taxId",taxId).store("t").by(property).repeat(__.out("has_parent").store("t").by(property)).cap("t").unfold().toList());
////        System.out.println(g.V().has("taxId", taxId).repeat(__.out("has_parent")).path().by("taxId").next());
////        System.out.println();
////        System.out.println(g.V().has("taxId", taxId).repeat(__.out("has_parent")).path().by("scientific_name").next());
//    }
//
//    public void showPath(int taxId, String property) {
//        if(gSubTree == null) {
//            showPath(taxId,property,g);
//        }
//        else {
//            showPath(taxId,property,gSubTree);
//        }
//
//    }
//
//    public void getSubTreeSummary() {
////        System.out.println(gSubTree.V().has("lca",true).values("scientific_name","rank","taxId").fold().toList());
////        System.out.println(gSubTree.V().not(__.has("persisted",true)).values("scientific_name","rank","taxId").fold().toList());
////        System.out.println(gSubTree.V().count().next());
////        List ranks = gSubTree.V().values("rank").dedup().toList();
////        List ranksTotal = g.V().values("rank").dedup().toList();
////        Map distribution = gSubTree.V().group().by("rank").by("scientific_name").next();
////        Map distribution = gSubTree.V().has("rank","phylum").groupCount().by(__.values("scientific_name")).next();
////        distribution.forEach((u,v) -> System.out.println(u + " " + v));
//        List roots = gSubTree.V().where(__.out().count().is(0)).valueMap().toList();
//        List children = gSubTree.V().where(__.in().count().is(0)).values("taxId").toList();
//        //Map outCount = gSubTree.V().groupCount().by(__.out().count()).next();
//        //Map inCount = gSubTree.V().groupCount().by(__.in().count()).next();
//
////        outCount.forEach((u,v) -> System.out.println(u + " " + v));
////        System.out.println();
////        inCount.forEach((u,v) -> System.out.println(u + " " + v));
////        ranksTotal.removeAll(ranks);
////        System.out.println("Ranks: "+ ranks );
////        System.out.println("TotalRanks: "+ ranksTotal );
//        System.out.println("Roots:("+ roots.size() + ") " + roots);
//        System.out.println("Children(:" + children.size() + ")" );
//
//    }

    public void getTriplet(GraphTraversalSource g, Integer taxId1, Integer taxId2, Integer taxId3) {

    }

    public String getKeyspace() {
        return KEYSPACE;
    }

    public void getCountDistribution(List<String> taxonomicGroups, List<RecordDT> records) {

        List<Integer> collectTaxids = new ArrayList<>(records.stream().map(r -> r.getTaxid()).collect(Collectors.toList()));
//        System.out.println(collectTaxids);
        GraphTraversalSource g = createBottomUpSubtreeToRootFromTaxids(collectTaxids);
        System.out.println(g.V().count().next());
        for(String taxonomicGroupName: taxonomicGroups) {
            List<Integer> taxids = getContainedTaxids(
                    createTopDownSubtreeFromTaxids(g,Collections.singletonList(getTaxIdForTaxonomicName(taxonomicGroupName)))
                    ,collectTaxids);
            System.out.println(taxonomicGroupName + ": " + taxids.size());
        }
    }
    public static void getCountDistribution(List<String> taxonomicGroups) {
        TaxonomyGraph taxonomyGraph =
                (TaxonomyGraph) new Graph.Builder(TaxonomyGraph.KEYSPACE,
                        GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                        startOption(LaunchExternal.LAUNCH_OPTION.VOID).build();
        GraphTraversalSource g = taxonomyGraph.createSubtreeFromPersistedNodes();
        List<Integer> persistedTaxids = new ArrayList<>(taxonomyGraph.getTaxidsOfRecords());
//        List<Integer> containedTaxids = new ArrayList<>(taxonomyGraph.getContainedTaxids(g,persistedTaxids));
//        System.out.println(persistedTaxids.size());
//        persistedTaxids.removeAll(containedTaxids);
//        System.out.println(persistedTaxids.size());
//        Auxiliary.printSeq(persistedTaxids);
//        System.out.println(taxonomyGraph.getContainedTaxids(g,taxonomyGraph.getTaxidsOfRecords()).size());

        for(String taxonomicGroupName: taxonomicGroups) {
            List<Integer> taxids = taxonomyGraph.getContainedTaxids(
                    taxonomyGraph.createTopDownSubtreeFromTaxids(g,Collections.singletonList(taxonomyGraph.getTaxIdForTaxonomicName(taxonomicGroupName)))
                    ,persistedTaxids);
            System.out.println(taxonomicGroupName + ": " + taxids.size()+ "\t\t " + Auxiliary.withMathRound(((double)taxids.size())/persistedTaxids.size()*100,2));
        }

    }

    public Map<String,List<Integer>> getTaxidsDistribution(List<String> taxonomicGroups) {
        Map<String,List<Integer>> taxonomicMap = new HashMap<>();
        List<Integer> persistedTaxids = new ArrayList<>(getTaxidsOfRecords());
        GraphTraversalSource g = createBottomUpSubtreeToRootFromTaxids(persistedTaxids);

        for(String taxonomicGroupName: taxonomicGroups) {
            List<Integer> taxids = getContainedTaxids(
                    createTopDownSubtreeFromTaxids(g,Collections.singletonList(getTaxIdForTaxonomicName(taxonomicGroupName)))
                    ,persistedTaxids);
            taxonomicMap.put(taxonomicGroupName,taxids);
            System.out.println(taxonomicGroupName + ": " + taxids.size());
        }
        return taxonomicMap;
    }

    public  void persistPersistedNodesSubgraph(String fileName) {
        GraphTraversalSource g = createSubtreeFromPersistedNodes();
        Graph.writeToGryo(fileName,g);
    }

    public Dataset<TaxonomicGroupMappingDT> createTaxonomicGroupMapping(Map<String,List<Integer>> taxonomicDistribution) {
        List<Dataset<TaxonomicGroupMappingDT>> taxonomicGroupMappings = new ArrayList<>();
        Dataset<RecordDT> records = null;
        try {
            records = RecordTable.getInstance().getTableEntries();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        for(Map.Entry<String,List<Integer>> taxonomicGroup: taxonomicDistribution.entrySet()) {
            List<Integer> taxids = taxonomicGroup.getValue();
            String taxonomicGroupName = taxonomicGroup.getKey();
            Dataset<RecordDT> filteredRecords = records.filter(col(ColumnIdentifier.TAXID).isInCollection(taxids));
            Dataset<TaxonomicGroupMappingDT> m = filteredRecords.map((MapFunction<RecordDT,TaxonomicGroupMappingDT>) r ->
                            new TaxonomicGroupMappingDT(r.getRecordId(),r.getName(),taxonomicGroupName,r.getTaxid()),
                    TaxonomicGroupMappingDT.ENCODER);
            m.show();
            taxonomicGroupMappings.add(m);
            LOGGER.info("Done with " + taxonomicGroup.getKey());
        }
        return SparkComputer.union(taxonomicGroupMappings,TaxonomicGroupMappingDT.ENCODER);
    }


}

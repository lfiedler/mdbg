package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;

public interface StrandInterface {

    boolean isStrand();
    void setStrand(boolean strand);

    static <T extends StrandInterface> FilterFunction<T> strandFilter(boolean strand, Class<T> tClass) {
        return (strand ? (k -> k.isStrand()) : (k -> !k.isStrand()));
    }

    static <T extends StrandInterface> MapFunction<T,Boolean> strandGroupKey() {
        return  (MapFunction<T, Boolean>) value -> value.isStrand();
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.List;

public class PositionCountRecordsDT extends PositionCountDT {


    private List<Integer> records;

    public static final Encoder<PositionCountRecordsDT> ENCODER = Encoders.bean(PositionCountRecordsDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        RECORDS("records"),
        COUNT("count"),
        POSITION("position");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public PositionCountRecordsDT(int position, List<Integer> records) {
        this.position = position;
        this.records = records;
    }

    public PositionCountRecordsDT(int position, int count, List<Integer> records) {
        super(position,count);
        this.records = records;
    }

    public PositionCountRecordsDT() {

    }

    public List<Integer> getRecords() {
        return records;
    }

    public void setRecords(List<Integer> records) {
        this.records = records;
    }


}

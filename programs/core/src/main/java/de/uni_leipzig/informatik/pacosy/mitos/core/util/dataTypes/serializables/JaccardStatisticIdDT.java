package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class JaccardStatisticIdDT extends JaccardStatisticDT{
    protected int recordId;
    public static final Encoder<JaccardStatisticIdDT> ENCODER = Encoders.bean(JaccardStatisticIdDT.class);

    public JaccardStatisticIdDT() {
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}

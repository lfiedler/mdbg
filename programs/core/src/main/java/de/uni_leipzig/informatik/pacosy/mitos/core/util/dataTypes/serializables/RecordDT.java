package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class RecordDT extends RecordTaxidMappingDT  {
    protected int length;
    protected boolean topology;

    protected boolean visited;
    protected boolean mitosFlag = true;
    protected int version;
    protected String name;
    protected int aContent;
    protected int cContent;
    protected int gContent;
    protected int tContent;
    protected int ambigCount;
    public static final Encoder<RecordDT> ENCODER = Encoders.bean(RecordDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        RECORDID("\"recordId\""),
        NAME("name"),
        VERSION("version"),
        TAXID( "\"taxId\""),
        LENGTH("length"),
        TOPOLOGY("topology"),
        A_CONTENT("\"aContent\""),
        C_CONTENT("\"cContent\""),
        G_CONTENT("\"gContent\""),
        T_CONTENT("\"tContent\""),
        AMBIG_COUNT("\"ambigCount\""),
        VISITED("visited"),
        MITOS_FLAG("\"mitosFlag\"");

        public final String identifier;


        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RecordDT() {
        super();
        recordId = -1;

    }

    public RecordDT(String name) {
        recordId = mapToRecordId(name);
        this.name = name;
    }

    public RecordDT(int recordId) {
        this.recordId = recordId;
    }

    public static int mapToRecordId(String name) {
        String [] parts = name.replaceAll("_0+","_").split("_");
        return Integer.parseInt(parts[1]);
    }
    // Getter and setter

    public int getAmbigCount() {
        return ambigCount;
    }

    public void setAmbigCount(int ambigCount) {
        this.ambigCount = ambigCount;
    }

    public int getaContent() {
        return aContent;
    }

    public void setaContent(int aContent) {
        this.aContent = aContent;
    }

    public int getcContent() {
        return cContent;
    }

    public void setcContent(int cContent) {
        this.cContent = cContent;
    }

    public int getgContent() {
        return gContent;
    }

    public void setgContent(int gContent) {
        this.gContent = gContent;
    }

    public int gettContent() {
        return tContent;
    }

    public void settContent(int tContent) {
        this.tContent = tContent;
    }

    public boolean isMitosFlag() {
        return mitosFlag;
    }

    public void setMitosFlag(boolean mitosFlag) {
        this.mitosFlag = mitosFlag;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        recordId = mapToRecordId(name);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isTopology() {
        return topology;
    }

    public void setTopology(boolean topology) {
        this.topology = topology;
    }

    public RecordDT getRecord(Dataset<RecordDT> recordDataset) {
        return getRecord(recordDataset,recordId);
    }

    public static RecordDT getRecord(Dataset<RecordDT> recordDataset, int record) {
        return SparkComputer.getFirstOrElseNull(recordDataset.
                filter((FilterFunction<RecordDT>)(r -> r.getRecordId() == record)));

    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        }
        if(!(o instanceof  RecordDT)) {
            return false;
        }
        RecordDT recordDT = (RecordDT) o;

        return recordDT.getRecordId() == this.getRecordId();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RecordId:" + recordId + "\t");
        stringBuilder.append("Name:" + name+ "\t");
        stringBuilder.append("Version:" + version+ "\t");
        stringBuilder.append("Taxid: " + taxid+ "\t");
        stringBuilder.append("Length: " + length+ "\t");
        stringBuilder.append("Topology: " + topology+ "\t");
        stringBuilder.append("A content: " + aContent+ "\t");
        stringBuilder.append("C content: " + cContent+ "\t");
        stringBuilder.append("G content: " + gContent+ "\t");
        stringBuilder.append("T content: " + tContent+ "\t");
        stringBuilder.append("Ambig count: " + ambigCount+ "\t");
        stringBuilder.append("Mitosflag: " + mitosFlag);

        return stringBuilder.toString();
    }



}

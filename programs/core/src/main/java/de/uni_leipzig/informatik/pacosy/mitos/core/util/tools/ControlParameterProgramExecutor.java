package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.BandedAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.Sequence;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAlignerFactory;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapAnalyzer;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.TaxonomyGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.TaxonomyParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.PropertiesTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.AlignCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.InputParser;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.misc.AmbiguousMatchException;

import javax.lang.model.type.UnknownTypeException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.exit;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.explode;

public class ControlParameterProgramExecutor {

    static private final String TAXONOMY_NAMES_FILENAME = "names.dmp";

    static private final String TAXONOMY_NODES_FILENAME = "nodes.dmp";

    static private final String TAXONOMY_MERGE_FILENAME = "merged.dmp";

    static private final String TAXONOMY_DIVISION_FILENAME = "division.dmp";

    public enum ACTION {

        MOVE_FILES,
        CREATE_RANDOM_EDGE_WEIGHTS,
        PERSIST_GRAPH_DB_MINIMAL,
        PERSIST_MAPPING_DATA,
        STORE_RECORDS_PSQL,
        CREATE_SEQUENCE_MAP_GRAPH,
        GET_COMPOSITION_SCORES,
        GET_NUCLEOTIDE_COUNT,
        GET_SEQUENCE_COUNT_STATISTICS,
        GROUP_BY_TAXONOMIC_GROUP,
        MOVE_AMBIG_SEQUENCES,
        GET_AMBIG_SEQUENCES_STATISTICS,
        MOVE_PERSISTED_SEQUENCES,
        PERSIST_RECORD_PROPERTIE_PSQL,
        PERSIST_GENOME_PSQL,
        PERSIST_KMERS_PSQL,
        PERSIST_MANUALLY,
        PERSIST_TAXONOMY,
        PERSIST_NODEPAIR,
        RECORD_CHECK_ONLY,
        RECORD_PERSIST_PSQL_ONLY,
        RECORD_PERSIST_PDB_ONLY,
        RECORD_PERSIST,
        RECORD_DELETE,
        DELETE_ALL,
        CREATE;
    }

    private static Logger LOGGER = LoggerFactory.getLogger(ControlParameterProgramExecutor.class);

    private String refseq;

    private int amount = -1;

    private int k;

    private SequenceParser.AmbigFilter ambigFilter;

    private List<String> ids;

    private DbgGraph dbgGraph;

    private TaxonomyGraph taxonomyGraph;

    private DataDirectoryParser dataDirectoryParser;

    private boolean mitosFlag;

    private ACTION action;

    private List<String> skipDirectories;

    private List<String> directories;

    private enum SEQUENCE_FILES {
        BED("bed"),
        FASTA("fasta"),
        GB("gb"),
        RECORD("record");

        public final String identifier;
        SEQUENCE_FILES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum STATISTIC_FILE_NAMES {
        PLUS_RANDOM_RANGE_LENGTH("plusRandomRangeLength.csv"),
        MINUS_RANDOM_COUNT("minusRandomCount.csv"),
        PLUS_NUCLEOTIDE_COUNT("plusNucleotideCount.csv"),
        MINUS_NUCLEOTIDE_COUNT("minusNucleotideCount.csv"),
        SEQUENCE_TAXONOMY_COUNT_STATISTICS("countStatistic.txt");

        public final String identifier;
        STATISTIC_FILE_NAMES(String identifier) {
            this.identifier = identifier;
        }
    }

    public ControlParameterProgramExecutor(String[] args) {

        PersistenceInputParser persistenceInputParser =
                new PersistenceInputParser(args);

    }


    public ControlParameterProgramExecutor(String fileName) {

        PersistenceInputParser persistenceInputParser =
                new PersistenceInputParser(new String[] {"-f",fileName});


    }

    public ControlParameterProgramExecutor(boolean getHelp) {

        if(getHelp) {
            PersistenceInputParser persistenceInputParser =
                    new PersistenceInputParser(new String[] {"--help"});
        }


    }

    public void persistKmersToSQL(int k) {


        List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);

        Dataset<RecordDT> recordData = null;
        try {
            KmersTable.getInstance().dropPrimaryKey();
            recordData = RecordTable.getInstance().getTableEntries().cache();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



        KmersTable.getInstance().setK(k);
        Dataset<Integer> kmerRecords = null;
        LOGGER.info("Using k "+ KmersTable.getInstance().getK());
        try {
            kmerRecords = SparkComputer.createDataFrame(KmersTable.getInstance().getPersistedRecordIds(), Encoders.INT());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        // get all recordIds whose genomes are not yet persisted
        List<String> records = recordData.join(kmerRecords,col(ColumnIdentifier.VALUE).
                equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT_ANTI).
                select(ColumnIdentifier.NAME).as(Encoders.STRING()).collectAsList();
        // get corresponding fasta files
        files = dataDirectoryParser.filterFilesInclude(files,records);
        int counter = files.size();

        for(File fastaFile: files) {

            LOGGER.info(fastaFile.getName() + " still "+ counter);

            int id = RecordDT.mapToRecordId(
                    fastaFile.getName().substring(0,fastaFile.getName().indexOf(".")));
            RecordDT record = RecordDT.getRecord(recordData,id);
//            Auxiliary.printMap(record);

            Map<String,Integer []> kmerMap = SequenceParser.getKmerMap(
                    fastaFile,
                    k,
                    record.isTopology(),
                    true, SequenceParser.AmbigFilter.NO_FILTER
            );
//            Auxiliary.printMap(kmerMap,s -> s,
//                    pos -> {
//                String string = "";
//                for(int i = 0; i < pos.length; i++) string += pos[i] + ",";
//                return string;}
//                );
//            Auxiliary.printMap(kmerMap);
            try {
                KmersTable.getInstance().addKmers(true,id,kmerMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.exit(-1);
            }

            kmerMap= SequenceParser.getKmerMap(
                    fastaFile,
                    k,
                    record.isTopology(),
                    false, SequenceParser.AmbigFilter.NO_FILTER
            );
//            Auxiliary.printMap(kmerMap);
            try {
                KmersTable.getInstance().addKmers(false,id,kmerMap);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.exit(-1);
            }
            counter--;
        }
        try {
            KmersTable.getInstance().createPrimaryKey();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void persistGenomesToSQL() {
        List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);



        Dataset<RecordDT> recordData = null;
        Dataset<Integer> persistedRecords = null;
        try {
            GenomeTable.getInstance().dropPrimaryKey();
            recordData = RecordTable.getInstance().getTableEntries().cache();
            persistedRecords =SparkComputer.createDataFrame(GenomeTable.getInstance().getPersistedRecordIds(), Encoders.INT());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        // get all recordIds whose genomes are not yet persisted
        List<String> records = recordData.join(persistedRecords,col(ColumnIdentifier.VALUE).
                equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT_ANTI).
                select(ColumnIdentifier.NAME).as(Encoders.STRING()).collectAsList();

        // get corresponding fasta files
        files = dataDirectoryParser.filterFilesInclude(files,records);
        int counter = files.size();

        for(File fastaFile: files) {
            LOGGER.info("Still "+ counter);
            int id = RecordDT.mapToRecordId(
                    fastaFile.getName().substring(0,fastaFile.getName().indexOf(".")));
            try {
                GenomeTable.getInstance().addGenome(id,FileIO.readStringFromFile(fastaFile,'>',false));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.exit(-1);
            }
            counter--;
        }
        try {
            GenomeTable.getInstance().createPrimaryKey();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void persistToGraphDataBaseMinimal() {
        List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);
        Collections.sort(files);
        List<RecordDT> records = SparkComputer.read(ProjectDirectoryManager.getRECORD_DATA_DIR() + "/RECORDS", RecordDT.ENCODER).collectAsList();
        Map<Integer,RecordDT> recordMap = new HashMap<>();
        for(RecordDT recordDT: records) {
            recordMap.put(recordDT.getRecordId(),recordDT);
        }
        int counter = files.size();
        DbgGraph dbgGraph = (DbgGraph)
                new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                        startOption(LaunchExternal.LAUNCH_OPTION.VOID).build();

        for(File fastaFile: files) {
//            if(counter != 8015) {
                int id = RecordDT.mapToRecordId(
                        fastaFile.getName().substring(0,fastaFile.getName().indexOf(".")));
                LOGGER.info("Still "+ counter + " At record " + id);

                RecordDT record = recordMap.get(id);
                SequenceParser.parseMinimal(dbgGraph,id,record.isTopology(),fastaFile);
                dbgGraph.setPartitionAttributes();

//            }
            counter--;
//            if(counter < 8010) {
//                return;
//            }

        }
    }


    public void moveFiles() {
        List<File> gbfiles =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.GB);
        List<File> bedfiles =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.BED);
        List<File> fastafiles =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);

        System.out.println(dataDirectoryParser.sequenceDataDirectory);
        File fastaDir = new File(dataDirectoryParser.sequenceDataDirectory + "/fasta");
        File bedDir = new File(dataDirectoryParser.sequenceDataDirectory + "/bed");
        File gbDir = new File(dataDirectoryParser.sequenceDataDirectory + "/gb");
        fastaDir.mkdirs();
        bedDir.mkdirs();
        gbDir.mkdirs();


//        for(File file: fastafiles) {
//            try {
//                System.out.println(file.toPath());
//                Files.move(file.toPath(),Paths.get(fastaDir.getAbsolutePath() + "/"+ file.getName()));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        for(File file: bedfiles) {
            try {
                System.out.println(file.toPath());
                Files.move(file.toPath(),Paths.get(bedDir.getAbsolutePath() + "/"+ file.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for(File file: gbfiles) {
            try {
                System.out.println(file.toPath());
                Files.move(file.toPath(),Paths.get(gbDir.getAbsolutePath() + "/"+ file.getName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        fastafiles.stream().forEach(file -> {
//            try {
//                Files.move(file.toPath(),Paths.get(fastaDir.getAbsolutePath()));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//        gbfiles.stream().forEach(file -> {
//            try {
//                dataDirectoryParser.moveFileToSubDir(file,dataDirectoryParser.sequenceDataDirectory+"/gb",SEQUENCE_FILES.GB);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//        bedfiles.stream().forEach(file -> {
//            try {
//                dataDirectoryParser.moveFileToSubDir(file,dataDirectoryParser.sequenceDataDirectory+"/bed",SEQUENCE_FILES.BED);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });
//        fastafiles.stream().forEach(file -> {
//            try {
//                dataDirectoryParser.moveFileToSubDir(file,dataDirectoryParser.sequenceDataDirectory+"/fasta",SEQUENCE_FILES.FASTA);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        });

    }

    public void persistRecordPorpertiesToSQL() {
        List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.BED);

        Dataset<RecordDT> recordData = null;
        Dataset<Integer> persistedRecords = null;
        try {
            RecordPropertyTable.getInstance().dropPrimaryKey();
            recordData = RecordTable.getInstance().getTableEntries().cache();
            persistedRecords =SparkComputer.createDataFrame(RecordPropertyTable.getInstance().getPersistedRecordIds(), Encoders.INT());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        // get all recordIds whose properties are not yet persisted
        List<String> records = recordData.join(persistedRecords,col(ColumnIdentifier.VALUE).
                equalTo(col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.LEFT_ANTI).
                select(ColumnIdentifier.NAME).as(Encoders.STRING()).collectAsList();

        // get corresponding bed files
        files = dataDirectoryParser.filterFilesInclude(files,records);
        int counter = files.size();

        for(File bedFile: files) {
//            LOGGER.info("Still "+ counter);
            int id = RecordDT.mapToRecordId(
                    bedFile.getName().substring(0,bedFile.getName().indexOf(".")));

                LOGGER.info("At "+id + " (" + counter + ")");
                try {
                    SequenceParser sequenceParser = new SequenceParser(bedFile);
                    sequenceParser.parseFeaturesFromBED();

                    if(sequenceParser.getPropertiesPos().size() > 0) {
//                        SequenceParser.printFeatures(new TreeMap<>(sequenceParser.getPropertiesPos()));
                        RecordPropertyTable.getInstance().addProperties(true,id,sequenceParser.getPropertiesPos());
                    }
//                    System.out.println();
                    if(sequenceParser.getPropertiesNeg().size() > 0) {
//                        SequenceParser.printFeatures(new TreeMap<>(sequenceParser.getPropertiesNeg()));
                        RecordPropertyTable.getInstance().addProperties(false,id,sequenceParser.getPropertiesNeg());
                    }


                } catch (Exception throwables) {
                    throwables.printStackTrace();
                    System.exit(-1);
                }


            counter--;
        }

        try {
            RecordPropertyTable.getInstance().removeDuplicates();
            RecordPropertyTable.getInstance().createPrimaryKey();
            RecordPropertyTable.getInstance().renameOrfs();
            LOGGER.info("Done with pk");
            RecordPropertyTable.getInstance().addVoidProperties();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void persistMappingData() {
        List<File> files =  dataDirectoryParser.
                getAllFiles(new ArrayList<>(Arrays.asList("ambig","persisted")),SEQUENCE_FILES.FASTA);
        LOGGER.info("Creating mapping data for " + files.size() + " files");
        for(File file: files) {
            LOGGER.info(file.getName());
            SequenceMapCreator sequenceMapCreator = new SequenceMapCreator.Builder(file).build();
            sequenceMapCreator.generateParquets();
        }
    }


    public void createSequenceMapGraph(File fastaFile, int kSmall, boolean topology) {

        boolean strand = true;
//        Map<String,Integer []> kplusOneMerMap = SequenceParser.getKmerMap(
//                fastaFile,
//                16,
//                topology,
//                strand, SequenceParser.AmbigFilter.SKIP_FILTER
//        );
//        DbgGraph dbgGraph = (DbgGraph)
//                new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
//                        startOption(LaunchExternal.LAUNCH_OPTION.START).build();
//        Dataset<RecordsDT> positionDistributionDTDataset =
//                dbgGraph.getRecordDistribution(kplusOneMerMap);
//        positionDistributionDTDataset.show();
//        Dataset<PositionDistributionDT> positionDistributionDTDataset =
//                dbgGraph.getRecordCountPositionDistribution(kplusOneMerMap);
//        positionDistributionDTDataset.show();
//        Auxiliary.printMap(kplusOneMerMap);
//
//        List<MappedKmerDT> mappedKmerDTS = SequenceParser.getMappedKmers(
//                fastaFile,
//                kSmall,
//                topology,
//                strand, SequenceParser.AmbigFilter.SKIP_FILTER
//        );
//
//        Dataset<MappedKmerDT> mappedKmersDataset = SparkComputer.createDataFrame(mappedKmerDTS,MappedKmerDT.class);
//
//
//        mappedKmersDataset.show();
//        SparkComputer.persistDataFrame(mappedKmersDataset,"/home/lisa/mappedKmers.parquet");
//        KmersTable.getInstance().setK(kSmall);
//        Dataset<KmerDT> kmerDataset =
//                KmersTable.getInstance().getKmerMatches(strand,
//                        mappedKmersDataset.select(KmerDT.getKmerName()).distinct().as(Encoders.STRING()).collectAsList());
//        kmerDataset.show();
//        SparkComputer.persistDataFrame(kmerDataset,"/home/lisa/extractedKmers.parquet");

        Dataset<KmerDT> kmerDataset =SparkComputer.read("/home/lisa/extractedKmers.parquet",KmerDT.class);
        kmerDataset.show();
//
//        SparkComputer.showAll(kmerDataset.groupBy(ColumnIdentifier.KMER).agg(
//                countDistinct(ColumnIdentifier.RECORD_ID).as("record count")
//        ));


 // TODO: join this with the actual mapping 
    }


    public static void createDB() {
        /*** Persist psql only ***/
        new ControlParameterProgramExecutor(new String[]
                {"--action","RECORD_PERSIST_PSQL_ONLY","--k","16", "--ambig-filter","NO_FILTER","--refseq","refseq89"});
        /*** Persist genome in psql **/
        new ControlParameterProgramExecutor(new String[]
                {"--action","PERSIST_GENOME_PSQL","--refseq","refseq89"});
        System.out.println("done with genomes");
        /*** Persist kmers in psql **/
        new ControlParameterProgramExecutor(new String[]
                {"--action","PERSIST_KMERS_PSQL","--refseq","refseq89","--k",DbgGraph.K+""});
        System.out.println("done with kmers");
        /*** Persist record properties in psql **/
        new ControlParameterProgramExecutor(new String[]
                {"--action","PERSIST_RECORD_PROPERTIE_PSQL","--refseq","refseq89"});
    }

    public static Dataset<Row> fetchRandomWeights(boolean strand) {
        String path = ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" ;
        path += strand ?  STATISTIC_FILE_NAMES.PLUS_RANDOM_RANGE_LENGTH: STATISTIC_FILE_NAMES.MINUS_RANDOM_COUNT;
        return SparkComputer.read(path,DataTypes.createStructType(KmersTable.getWeightStatisticsStructFields()));
    }

    private Dataset<Row>  createRandomEdgeWeights(final Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideDistribution,
                                                  int amount, int sequenceLength, boolean strand) {




        KmersTable kmersTable = KmersTable.getInstance();

        int k = DbgGraph.K;
        kmersTable.setK(k);
        long seed = System.currentTimeMillis();
        long randNum;

        StringBuilder s = new StringBuilder();
        List<Row> weightResults = new ArrayList<>();
        long cummulativeSum = 0;
        Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideDistributionCopy =
                nucleotideDistribution.entrySet().stream().
                        collect(Collectors.toMap( n -> n.getKey(), n -> n.getValue()));
        MersenneTwister mersenneTwister = new MersenneTwister(seed);
        long nucleotideCount = nucleotideDistributionCopy.values().stream().mapToLong( n -> n).sum();
        for(int i = 0; i < amount; i++) {
            for(int lengthCounter = 0; lengthCounter < sequenceLength; lengthCounter++) {
                randNum = (long) (mersenneTwister.nextDouble()*nucleotideCount);
                cummulativeSum = 0;
                for(SequenceAligner.NUCLEOTIDE n: nucleotideDistributionCopy.keySet()) {
                    if(randNum < cummulativeSum+nucleotideDistributionCopy.get(n)) {

                        s.append(n.identifier);
                        break;
                    }
                    cummulativeSum+=nucleotideDistributionCopy.get(n);
                }
            }
            List<String> kmers = SequenceParser.getKmers(s.toString(),k,strand,true);
//            System.out.println(kmers.size() + " " + s.length());
            weightResults.addAll(kmersTable.getWeightStatistic(strand,kmers).collectAsList());

            s.setLength(0);
        }
        return Spark.getInstance().createDataFrame(weightResults,DataTypes.createStructType(KmersTable.getWeightStatisticsStructFields()));
    }


    private void   createRandomMappings(final Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideDistribution,
                                                    int sequenceLength, boolean strand, File resultFile,
                                                 String header) {
        LOGGER.info("Using length: " + sequenceLength);
        KmersTable kmersTable = KmersTable.getInstance();

        int k = DbgGraph.K;
        kmersTable.setK(k);
        long seed = System.currentTimeMillis();
        long randNum;
        long cummulativeSum = 0;
        StringBuilder s = new StringBuilder();

        int amount = 8015;
        Long currentCount;
        int counter = 0;
        String nucleotide;
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(resultFile,false));
            writer.write("#seed="+ seed + "\n");
            writer.write(header+ "\n");

            Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideDistributionCopy =
                    nucleotideDistribution.entrySet().stream().
                            collect(Collectors.toMap( n -> n.getKey(), n -> n.getValue()));
            MersenneTwister mersenneTwister = new MersenneTwister(seed);
            long nucleotideCount = nucleotideDistributionCopy.values().stream().mapToLong( n -> n).sum();
            while (nucleotideCount > 0 && counter < amount) {
                randNum = (long) (mersenneTwister.nextDouble()*nucleotideCount);
                cummulativeSum = 0;
                for(SequenceAligner.NUCLEOTIDE n: nucleotideDistributionCopy.keySet()) {
                    if(randNum < cummulativeSum+nucleotideDistributionCopy.get(n)) {
                        s.append(n.identifier);
                        nucleotideCount--;
                        currentCount = nucleotideDistributionCopy.get(n);
                        if(currentCount == null) {
                            currentCount = 0L;
                        }
                        nucleotideDistributionCopy.put(n,currentCount-1);
                        break;
                    }
                    cummulativeSum+=nucleotideDistributionCopy.get(n);
                }
                // store sequence if length is achieved
                if(s.length() == sequenceLength) {

                    Dataset<PositionCountIdentifierDT> sequenceKmers =
                            SparkComputer.createDataFrame(SequenceParser.getMappedKmers(s.toString(),k,
                                    true,strand, SequenceParser.AmbigFilter.NO_FILTER),
                                    PositionCountIdentifierDT.ENCODER);

                    List<String> sequenceKmersList = sequenceKmers.
                            select(ColumnIdentifier.IDENTIFIER).
                            distinct().as(Encoders.STRING()).collectAsList();
                    Dataset<KmerDT> recordKmers = kmersTable.getKmerMatches(true,sequenceKmersList);

                    Dataset<Row> positionMapping = SequenceMapAnalyzer.createPositionMapping(sequenceKmers,recordKmers);
                    Dataset<Row> rangeLengths =  SequenceMapAnalyzer.createRangesLengthsForRecords(positionMapping);
//                    SparkComputer.showAll(rangeLengths.groupBy(ColumnIdentifier.LENGTH).count());
                    Dataset<Row> rangeStatistcs = rangeLengths.select(ColumnIdentifier.LENGTH).
                            groupBy().
                            agg(max(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MAX),
                                    mean(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MEAN));
//                    rangeStatistcs.show();
                    Row rangeStatisticRow = rangeStatistcs.first();
                    LOGGER.info(rangeStatisticRow.getInt(0)+","+rangeStatisticRow.getDouble(1));
                    writer.write(rangeStatisticRow.getInt(0)+","+rangeStatisticRow.getDouble(1)+"\n");

                    counter++;
                    //reset stringbuilder
                    s.setLength(0);
                    System.out.println(counter);
                }

            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void createRandomEdgeWeights(int runs) {
        int sequenceLength;
        try {
            sequenceLength = (int)RecordTable.getInstance().getRecordSequenceMeanLength();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new RuntimeException(throwables.getMessage());
        }

        int amount = 10000;
        List<Map<SequenceAligner.NUCLEOTIDE,Long>> nucleotideCounts = getNucleotideCounts();
        createRandomMappings(nucleotideCounts.get(0),sequenceLength,true,new File(ProjectDirectoryManager.getSTATISTICS_DIR()+ "/" +
                STATISTIC_FILE_NAMES.PLUS_RANDOM_RANGE_LENGTH.identifier),"max,mean");
//        Map<Long,Long> plusMap = createRandomEdgeWeights(nucleotideCounts.get(0),sequenceLength,true);
//        Auxiliary.printMap(plusMap);
//        System.out.println("Plus map");
//        FileIO.writeMapToFile(plusMap,
//                ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" + STATISTIC_FILE_NAMES.PLUS_RANDOM_COUNT.identifier,
//                false,
//                "maxWeight");
//        Map<Long,Long> minusMap = createRandomEdgeWeights(nucleotideCounts.get(1),sequenceLength,false);
//        System.out.println("Minus map");
//        Auxiliary.printMap(minusMap);
//        FileIO.writeMapToFile(minusMap,
//                ProjectDirectoryManager.getEDGE_WEIGHTS_DIR() + "/" + STATISTIC_FILE_NAMES.PLUS_RANDOM_COUNT.identifier,
//                false,
//                "maxWeight");
    }

    public void createCompositionScores(int runs, int gapOpen, int gapExtend, int length) {
        List<Map<SequenceAligner.NUCLEOTIDE,Long>> nucleotideCounts = getNucleotideCounts();
        AlignCost alignCost = SequenceAlignerFactory.LEVEL_6;
        BandedAligner bandedAligner =
                SequenceAlignerFactory.createBandedSequenceAligner(alignCost.getGapCostOptions().get(0),
                alignCost.getScoreCost());
//        int sequenceLength = 0;
//        try {
//            sequenceLength = (int) RecordTable.getInstance().getRecordSequenceMeanLength();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        bandedAligner.getShuffledScores(nucleotideCounts,sequenceLength,runs,"");
//        bandedAligner.getShuffledScoresNoTraceBack(nucleotideCounts.get(0),100,runs,true,"");
//        bandedAligner.getShuffledScoresNoTraceBack(nucleotideCounts.get(1),100,runs,false,"");
//        bandedAligner.getShuffledScores(nucleotideCounts.get(0),500,runs,true,"");
//        bandedAligner.getShuffledScores(nucleotideCounts.get(1),500,runs,false,"");
//        bandedAligner.getShuffledScores(nucleotideCounts.get(0),length,runs,true,"");
        bandedAligner.getShuffledScores(nucleotideCounts.get(1),length,runs,false,"");
    }

    public void createSequenceStatistics() {
        String baseDirectory = dataDirectoryParser.sequenceDataDirectory;
        List<DataDirectoryParser.SubDirectory> subDirectories = new ArrayList<>();
        dataDirectoryParser.traverseSubDirPath(subDirectories,baseDirectory);

        Map<String,Integer> countDistribution = new TreeMap<>();
        Set<String> parentDirectories = new HashSet<>();
        for(DataDirectoryParser.SubDirectory subDirectory: subDirectories) {
            parentDirectories.addAll(subDirectory.parentDirectories);
            countDistribution.put(subDirectory.directoryName,subDirectory.amount);
        }
        for(String parentDir: parentDirectories) {
            countDistribution.put(subDirectories.stream().
                            filter(s -> s.parentDirectories.contains(parentDir)).
                            limit(1).
                            map(s -> s.getParents(parentDir)).
                            collect(Collectors.joining(""))
                    ,subDirectories.stream().
                            filter(s -> s.parentDirectories.contains(parentDir)).
                            mapToInt(s -> s.amount).sum());
        }



        Auxiliary.printMap(countDistribution);
        FileIO.writeMapToFile(countDistribution,
                dataDirectoryParser.sequenceDataDirectory + "/" + STATISTIC_FILE_NAMES.SEQUENCE_TAXONOMY_COUNT_STATISTICS.identifier,
                false,"",": ");


//        List<String> ids = dataDirectoryParser.getAllIdsRecursively(Collections.singletonList("persisted"));
//        List<String> directories = new ArrayList<>();
//        dataDirectoryParser.traverseSequenceSubDirPath(directories,dataDirectoryParser.sequenceDataDirectory,SEQUENCE_FILES.FASTA);
//        System.out.println(ids.size());
//        try {
//            for(String id : ids) {
//
//                File bedFile = null, fastaFile = null, gbFile = null;
//                try {
//                    fastaFile = dataDirectoryParser.findFastaFile(id,directories);
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                    System.exit(-1);
//                }
//                try {
//                    bedFile = dataDirectoryParser.findBedFile(id,directories);
//                } catch (FileNotFoundException e) {
//                    LOGGER.info("No bed file for sequence " + id);
//                }
//                try {
//                    gbFile = dataDirectoryParser.findGBFile(id,directories);
//                } catch (FileNotFoundException e) {
//                    LOGGER.info("No gb file for sequence " + id);
//                }
//
//            }
//        } catch (AmbiguousMatchException a) {
//            a.printStackTrace();
//            System.exit(-1);
//        }


    }

    public void movePersistedequences(){

        ids = dataDirectoryParser.getPersistedRecords();
        try {
            for(String id : ids) {
                File bedFile = null, fastaFile = null, gbFile = null;
                try {
                    fastaFile = dataDirectoryParser.findFastaFile(id);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                try {
                    bedFile = dataDirectoryParser.findBedFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No bed file for sequence " + id);
                }
                try {
                    gbFile = dataDirectoryParser.findGBFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No gb file for sequence " + id);
                }
                dataDirectoryParser.movePersistedSequenceFiles(bedFile,fastaFile,gbFile);
            }
        } catch (AmbiguousMatchException a) {
            a.printStackTrace();
            System.exit(-1);
        }


    }

    public void moveAmbigSequences(){

        ids = dataDirectoryParser.getAllIds();

        try {
            for(String id : ids) {
                System.out.println("Id: " + id);

                File bedFile = null, fastaFile = null, gbFile = null;
                try {
                    fastaFile = dataDirectoryParser.findFastaFile(id);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                try {
                    bedFile = dataDirectoryParser.findBedFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No bed file for sequence " + id);
                }
                try {
                    gbFile = dataDirectoryParser.findGBFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No gb file for sequence " + id);
                }

                if(!SequenceParser.checkSequenceForAmbiguousNucleotides(fastaFile)) {
                    dataDirectoryParser.moveAmbigSequenceFiles(bedFile,fastaFile,gbFile);
                }

            }
        } catch (AmbiguousMatchException a) {
            a.printStackTrace();
            System.exit(-1);
        }


    }

    public void storeRecordsToSQL() {
        ids = dataDirectoryParser.getAllIds();
        LOGGER.info(ids.size()+ "");
        for(String id : ids) {
            LOGGER.info(id);
            RecordDT recordDT = new RecordDT(id);
            recordDT.setVisited(false);
            try {
                RecordTable.getInstance().persistID(recordDT);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public void getAmbigSequencesStatistics(){

        ids = dataDirectoryParser.getAllIds();

        try {
            for(String id : ids) {
//                System.out.println("Id: " + id);

                File bedFile = null, fastaFile = null, gbFile = null;
                try {
                    fastaFile = dataDirectoryParser.findFastaFile(id);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
//                try {
//                    bedFile = dataDirectoryParser.findBedFile(id);
//                } catch (FileNotFoundException e) {
////                    LOGGER.info("No bed file for sequence " + id);
//                }
//                try {
//                    gbFile = dataDirectoryParser.findGBFile(id);
//                } catch (FileNotFoundException e) {
////                    LOGGER.info("No gb file for sequence " + id);
//                }
//
//                if(!SequenceParser.checkSequenceForAmbiguousNucleotides(fastaFile)) {
//                    dataDirectoryParser.moveAmbigSequenceFiles(bedFile,fastaFile,gbFile);
//                }
                SequenceParser.getSequenceAmbiguousNucleotides(fastaFile);
//                System.out.println("Ambig count: " + SequenceParser.getSequenceAmbiguousNucleotides(fastaFile));
            }
        } catch (AmbiguousMatchException a) {
            a.printStackTrace();
            System.exit(-1);
        }


    }

    public List<Map<SequenceAligner.NUCLEOTIDE,Long>> getNucleotideCounts( ) {
        Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideCounts = new HashMap<>();
        Map<SequenceAligner.NUCLEOTIDE,Long> minusNucleotideCounts = new HashMap<>();
        final String plusNucleotideCountFileName = ProjectDirectoryManager.getSTATISTICS_DIR() +
                "/" + STATISTIC_FILE_NAMES.PLUS_NUCLEOTIDE_COUNT.identifier;
        final String minusNucleotideCountFileName = ProjectDirectoryManager.getSTATISTICS_DIR() + "/" +
                STATISTIC_FILE_NAMES.MINUS_NUCLEOTIDE_COUNT.identifier;
        if(new File(plusNucleotideCountFileName).exists()) {
            FileIO.readMapFromFile(plusNucleotideCountFileName,nucleotideCounts,true,",",
                    0,Long.MAX_VALUE, SequenceAligner.NUCLEOTIDE.class,Long.class);
        }
        else {
            //        List<File> files =  dataDirectoryParser.getAllFiles(Collections.singletonList("ambig"),SEQUENCE_FILES.FASTA);
//        List<String> records = dataDirectoryParser.getPersistedRecords(Collections.singletonList("ambig"));
            List<File> files =  dataDirectoryParser.getAllFiles(null,SEQUENCE_FILES.FASTA);
//        List<String> records = dataDirectoryParser.getPersistedRecords(null,false);
//        files = dataDirectoryParser.filterFilesInclude(files,records);


            for(File fastaFile: files) {
//            System.out.println(fastaFile.getAbsolutePath());
                SequenceParser.getNucleotideCount(fastaFile,nucleotideCounts);
            }



            FileIO.writeMapToFile(nucleotideCounts,
                    plusNucleotideCountFileName,
                    false,"nucleotide, amount",",");
        }
        System.out.println("Plus strand");
        Auxiliary.printMap(nucleotideCounts);
        if(new File(minusNucleotideCountFileName).exists()) {
            FileIO.readMapFromFile(minusNucleotideCountFileName,minusNucleotideCounts,true,",",
                    0,Long.MAX_VALUE, SequenceAligner.NUCLEOTIDE.class,Long.class);
        }
        else {
            for(Map.Entry<SequenceAligner.NUCLEOTIDE,Long> entry: nucleotideCounts.entrySet()) {
                minusNucleotideCounts.put(SequenceAligner.NUCLEOTIDE.get(SequenceParser.complement(entry.getKey().identifier)),entry.getValue());
            }

            FileIO.writeMapToFile(minusNucleotideCounts,
                    minusNucleotideCountFileName,
                    false,"nucleotide, amount",",");
        }
        System.out.println("Minus strand");
        Auxiliary.printMap(minusNucleotideCounts);

        return new ArrayList<>(Arrays.asList(nucleotideCounts,minusNucleotideCounts));

    }

    public void collectTaxonomyGroupMemmbers(List<String> taxonomicRoots) {

        ids = dataDirectoryParser.getAllIds();

        Map<String, Integer> taxonomicMap = new HashMap<>();

        for(String id : ids) {
//            System.out.println("Id: " + id);

            try {
//                File fastaFile = dataDirectoryParser.findFastaFile(id);
                File gbFile = dataDirectoryParser.findGBFile(id);
                taxonomicMap.put(id, SequenceParser.getTaxId(gbFile));


//                String name = SequenceParser.getScientificName(fastaFile,taxonomyGraph);
//                taxonomicMap.put(id,name);
//                if(name != null) {
//
//                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(-1);
            }

        }

        Auxiliary.printMap(taxonomicMap);
        Set<Integer> allTaxids = new HashSet<>(taxonomicMap.values());
        Integer taxidsAmount = allTaxids.size();
        GraphTraversalSource subtreeFromTaxids = taxonomyGraph.createBottomUpSubtreeFromTaxids(new ArrayList<>(taxonomicMap.values()));

//        taxonomyGraph.writeToGraphML(refseq + "CollapsedTaxonomy.xml",taxonomyGraph.createCollapsedSubtree(subtreeFromTaxids));
        for(String root: taxonomicRoots) {
            System.out.println("Root: " + root);
            GraphTraversalSource g = taxonomyGraph.createTopDownSubtreeFromTaxids(
                    taxonomyGraph.createDeepCopySubGraph(subtreeFromTaxids),
                    taxonomyGraph.getTaxidsForTaxonomicName(Collections.singletonList(root)));
            List<Integer> taxids = taxonomyGraph.getContainedTaxids(g,allTaxids);
            System.out.println(taxids.size() + " of " + taxidsAmount +  "("+100* (taxids.size()/((double)taxidsAmount)) + ")");
            allTaxids.removeAll(taxids);
            Map<String,Integer> currentTaxonomicMap = new HashMap<>(taxonomicMap);
            currentTaxonomicMap.values().retainAll(taxids);
            for(String id: currentTaxonomicMap.keySet()) {
                File bedFile = null, fastaFile = null, gbFile = null;
                try {
                    fastaFile = dataDirectoryParser.findFastaFile(id);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                try {
                    bedFile = dataDirectoryParser.findBedFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No bed file for sequence " + id);
                }
                try {
                    gbFile = dataDirectoryParser.findGBFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No gb file for sequence " + id);
                }
                dataDirectoryParser.moveSequenceFilesByTaxonicGroup(bedFile,fastaFile,gbFile,root);
            }

//            taxonomyGraph.writeToGraphML(refseq + root + "CollapsedTaxonomy.xml",taxonomyGraph.createCollapsedSubtree(g));
        }

        if(allTaxids.size() > 0) {
            Map<String,Integer> currentTaxonomicMap = new HashMap<>(taxonomicMap);
            currentTaxonomicMap.values().retainAll(allTaxids);
            for(String id: currentTaxonomicMap.keySet()) {
                File bedFile = null, fastaFile = null, gbFile = null;
                try {
                    fastaFile = dataDirectoryParser.findFastaFile(id);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                try {
                    bedFile = dataDirectoryParser.findBedFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No bed file for sequence " + id);
                }
                try {
                    gbFile = dataDirectoryParser.findGBFile(id);
                } catch (FileNotFoundException e) {
//                    LOGGER.info("No gb file for sequence " + id);
                }
                dataDirectoryParser.moveSequenceFilesByTaxonicGroup(bedFile,fastaFile,gbFile,"Other");
            }

//            GraphTraversalSource g = taxonomyGraph.createBottomUpSubtreeFromTaxids(new ArrayList<>(allTaxids));
//            System.out.println(allTaxids.size() + " of " + taxidsAmount +  "("+ 100*(allTaxids.size()/((double)taxidsAmount)) + ")");
//            taxonomyGraph.writeToGraphML(refseq +   "RemainingCollapsedTaxonomy.xml",taxonomyGraph.createCollapsedSubtree(g));
        }

        dataDirectoryParser.removeSequenceDirsIfEmpty();

    }


    public void execute() {

        if(action == ACTION.DELETE_ALL) {
            dbgGraph.entireDeletion();
            try {
                PropertiesTable.getInstance().removeAll();
                RecordTable.getInstance().removeAll();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else if(action == ACTION.CREATE) {
            dbgGraph.createSchema();
        }
        else {
            boolean idsProvided = true;
            if(ids == null) {
                idsProvided = false;
                ids = dataDirectoryParser.getUnpersistedRecords(amount);
            }
            directories = new ArrayList<>();
            dataDirectoryParser.traverseSequenceSubDirPath(directories,dataDirectoryParser.sequenceDataDirectory,SEQUENCE_FILES.FASTA);
            Iterator<String> it = directories.iterator();
            while (it.hasNext()) {
                String dir = it.next();
                if(skipDirectories != null) {
                    for(String skip: skipDirectories) {
                        if(dir.contains(skip)) {
                            it.remove();
                        }
                    }
                }


            }
//            Auxiliary.printSeq(directories);

            if(action == ACTION.RECORD_DELETE) {
                List<Integer> recordIds = ids.stream().
                        map(r -> RecordDT.mapToRecordId((String)r)).collect(Collectors.toList());

                recordIds.forEach(id -> {
                    try {
                        RecordTable.getInstance().remove(id);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
                executePSQL(true);
                dbgGraph.removeRecords(recordIds);

            }
            else {
                boolean embeded;


                if(action == ACTION.RECORD_PERSIST_PDB_ONLY || action == ACTION.RECORD_CHECK_ONLY) {
                    executePDB();
                    return;
                }
                if(action == ACTION.RECORD_PERSIST_PSQL_ONLY) {
                    executePSQL(false);
                    return;
                }
                else {
                    int counter = ids.size();
                    File bedFile = null, fastaFile = null, gbFile = null;

                    for(String id : ids) {
                        LOGGER.info("At record: " + id + "(Still " + counter + ")");
                        counter--;
                        RecordDT record = new RecordDT(id);
                        record.setMitosFlag(mitosFlag);

                        try {
                            bedFile = dataDirectoryParser.findBedFile(id,directories);
                            fastaFile = dataDirectoryParser.findFastaFile(id,directories);
                            gbFile = dataDirectoryParser.findGBFile(id,directories);

                            SequenceParser sequenceParser = new SequenceParser
                                    (k,record,bedFile,fastaFile,gbFile,ambigFilter,dbgGraph);
                            embeded = sequenceParser.parse();
                            if(embeded) {
                                dbgGraph.commit();
                                dbgGraph.setPartitionAttributes();
//                                record.print();
                                record.setVisited(true);
                                RecordTable.getInstance().persist(record);


                                //sequenceParser.printPropertySets();
                                PropertiesTable.getInstance().persistProteins(sequenceParser.getProteins());
                                PropertiesTable.getInstance().persistTRNAs(sequenceParser.getTrnas());
                                PropertiesTable.getInstance().persistReplicationOrigins(sequenceParser.getRepOrigins());
                                PropertiesTable.getInstance().persistRRNAs(sequenceParser.getRrnas());
                                PropertiesTable.getInstance().persistOtherTypes(sequenceParser.getOthers());
                            }
                            else {
                                dataDirectoryParser.moveAmbigSequenceFiles
                                        (sequenceParser.getBedFile(),sequenceParser.getFastaFile(),sequenceParser.getGbFile());
                            }
                            sequenceParser = null;
                        } catch (FileNotFoundException | SQLException | AmbiguousMatchException f) {
                            f.printStackTrace();
                            System.exit(-1);
                        }

                    }

                }

            }
        }

    }

    public void executeManually() {

        try (Stream<Path> walk = Files.walk(Paths.get(dataDirectoryParser.sequenceDataDirectory  + "/"+ SEQUENCE_FILES.FASTA.identifier +"/"),1))
        {

            // get all ids in fasta directory
            List<String> ids = walk.filter(Files::isRegularFile)
                    .map(x -> x.toString().substring(x.toString().
                            lastIndexOf("/")+1,x.toString().indexOf("."))).
                            collect(Collectors.toList());

            for(String id : ids){
                RecordDT record = new RecordDT(id);
                SequenceParser sequenceParser = new SequenceParser
                        (k,record, dataDirectoryParser.findBedFile(id),null,null,ambigFilter,dbgGraph);
                sequenceParser.parseFromFile(dataDirectoryParser.findRecordFile(id));
                dbgGraph.commit();
                dbgGraph.setPartitionAttributes();
                try {
                    RecordTable.getInstance().persist(record);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void executePSQL(boolean remove) {
        boolean embeded;
        int counter = ids.size();
        File fastaFile = null, gbFile = null;

        for(String id : ids) {
            LOGGER.info("At record: " + id + "(Still " + counter + ")");
            counter--;
            RecordDT record = new RecordDT(id);
            record.setMitosFlag(mitosFlag);
            try {
//                bedFile = dataDirectoryParser.findBedFile(id,directories);
                fastaFile = dataDirectoryParser.findFastaFile(id,directories);
                gbFile = dataDirectoryParser.findGBFile(id,directories);

                SequenceParser sequenceParser = new SequenceParser
                        (record,fastaFile,gbFile,ambigFilter);
                embeded = sequenceParser.parseForRecordOnly();
                if(remove && embeded) {
                    RecordTable.getInstance().remove(record.getRecordId());
//                    PropertiesTable.getInstance().removeProteins(sequenceParser.getProteins());
//                    PropertiesTable.getInstance().removeTRNAs(sequenceParser.getTrnas());
//                    PropertiesTable.getInstance().removeReplicationOrigins(sequenceParser.getRepOrigins());
//                    PropertiesTable.getInstance().removeTRNAs(sequenceParser.getRrnas());
//                    PropertiesTable.getInstance().persistOtherTypes(sequenceParser.getOthers());
                }
                else if(embeded) {
                    RecordTable.getInstance().persist(record);
                    //sequenceParser.printPropertySets();
//                    PropertiesTable.getInstance().persistProteins(sequenceParser.getProteins());
//                    PropertiesTable.getInstance().persistTRNAs(sequenceParser.getTrnas());
//                    PropertiesTable.getInstance().persistReplicationOrigins(sequenceParser.getRepOrigins());
//                    PropertiesTable.getInstance().persistRRNAs(sequenceParser.getRrnas());
//                    PropertiesTable.getInstance().persistOtherTypes(sequenceParser.getOthers());
                }
                else {
                    dataDirectoryParser.moveAmbigSequenceFiles
                            (sequenceParser.getBedFile(),sequenceParser.getFastaFile(),sequenceParser.getGbFile());
                }
                sequenceParser = null;
            } catch (FileNotFoundException | SQLException | AmbiguousMatchException f) {
                f.printStackTrace();
                System.exit(-1);
            }

        }
//        try {
//            Record.updateTaxids();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }

    public void executePDB() {

        boolean embeded;
        int counter = ids.size();
        File bedFile = null, fastaFile = null, gbFile = null;

        for(String id : ids) {
            LOGGER.info("At record: " + id + "(Still " + counter + ")");
            counter--;
            RecordDT record = new RecordDT(id);
            record.setMitosFlag(mitosFlag);
            try {
                bedFile = dataDirectoryParser.findBedFile(id,directories);
                fastaFile = dataDirectoryParser.findFastaFile(id,directories);
                gbFile = dataDirectoryParser.findGBFile(id,directories);

                SequenceParser sequenceParser = new SequenceParser
                        (k,record,bedFile,fastaFile,gbFile,ambigFilter,dbgGraph);
                if(action == ACTION.RECORD_CHECK_ONLY){
                    sequenceParser.parseCheck();
                }
                else {
                    embeded = sequenceParser.parse();
                    if(embeded) {
                        dbgGraph.commit();
                        dbgGraph.setPartitionAttributes();
                    }
                    else {
                        dataDirectoryParser.moveAmbigSequenceFiles
                                (sequenceParser.getBedFile(),sequenceParser.getFastaFile(),sequenceParser.getGbFile());
                    }

                }
                sequenceParser = null;
            } catch (FileNotFoundException | AmbiguousMatchException f) {
                f.printStackTrace();
                System.exit(-1);
            }

        }

        dbgGraph.printPatitionAttributes();
    }

    public void executeTaxonomy() {


        taxonomyGraph.entireDeletion();
        taxonomyGraph.createSchema();
        LOGGER.info("Done with Schema creation");
        String taxonomyDirectory = ProjectDirectoryManager.getTAXONOMIC_DATA();
        File namesFile = new File(taxonomyDirectory + "/" + TAXONOMY_NAMES_FILENAME);
        if(!namesFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_NAMES_FILENAME + " does not exist");
            System.exit(-1);
        }
        File nodesFile = new File(taxonomyDirectory + "/" + TAXONOMY_NODES_FILENAME);
        if(!nodesFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_NODES_FILENAME + " does not exist");
            System.exit(-1);
        }
        File mergeFile = new File(taxonomyDirectory + "/" + TAXONOMY_MERGE_FILENAME);
        if(!mergeFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_MERGE_FILENAME + " does not exist");
            System.exit(-1);
        }
        File divisionFile = new File(taxonomyDirectory + "/" + TAXONOMY_DIVISION_FILENAME);
        if(!divisionFile.exists()) {
            LOGGER.error("File " + taxonomyDirectory + "/" + TAXONOMY_DIVISION_FILENAME + " does not exist");
            System.exit(-1);
        }
        TaxonomyParser taxonomyParser = new TaxonomyParser(nodesFile,namesFile,mergeFile,divisionFile);
        taxonomyParser.parseNodes();
        taxonomyParser.parseNames();
        taxonomyParser.parseMergedTaxids();
        taxonomyParser.parseDivisions();

        // SQL persistence
        try {
            TaxonomyTable.getInstance().dropTable();
            TaxonomyTable.getInstance().persistRanks(new ArrayList<>(taxonomyParser.getRanks()));
            TaxonomyTable.getInstance().persistDivisions(taxonomyParser.getDivisions());
            System.out.println("done");
            TaxonomyTable.getInstance().persistMergedTaxids(taxonomyParser.getMergeTaxids());
            TaxonomyTable.getInstance().persistTaxids(
                    new ArrayList<>(taxonomyParser.getNodes().keySet()));
            RecordTable.getInstance().updateTaxids();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        taxonomyGraph.persist(taxonomyParser.getNodes(),taxonomyParser.getEdges(),taxonomyParser.getNames());
        System.out.println("Starting marking");
        taxonomyGraph.markPersistedSubgraph();


    }

    private class DataDirectoryParser {

        private class SubDirectory {

            private String directoryName;
            private String baseName;
            private Integer amount;
            private ArrayList<String> parentDirectories;

            public SubDirectory(String directoryName,Integer amount) {
                this.directoryName = directoryName;
                this.amount = amount;
                this.parentDirectories = new ArrayList<>(Arrays.asList(directoryName.split("->")));
                this.baseName = parentDirectories.get(parentDirectories.size()-1);
                parentDirectories.remove(parentDirectories.size()-1);
            }

            public String toString() {
                return directoryName +": " + amount; //+ "\n" + parentDirectories;
            }

            public String getParents(String directoryName){
                String path = "";
                for(int i = 0; i < parentDirectories.indexOf(directoryName); i++) {
                    path = path + parentDirectories.get(i) + "->";
                }
                path = path + directoryName;
                return path;
            }
        }

        private String sequenceDataDirectory;

        public DataDirectoryParser() {

        }

        public String getSequenceDataDirectory() {
            return sequenceDataDirectory;
        }

        public void setSequenceDataDirectory(String sequenceDataDirectory) {
            this.sequenceDataDirectory = sequenceDataDirectory;
        }

        public void traverseSubDirPath(List<SubDirectory> routes) {
            traverseSubDirPath(routes,sequenceDataDirectory);
        }

        private void traverseSubDirPath(List<SubDirectory> routes, String baseDir) {
            try {
                List<String> subDirectories = getSubdirectories(baseDir).
                        map(x -> x.toString()).collect(Collectors.toList());
                if(subDirectories.size() == 0) {
//                Map<String,Integer> m = new HashMap<>();
//                m.put(baseDir.replaceFirst(dataDirectoryParser.sequenceDataDirectory+ "/","").replace("/","->"),dataDirectoryParser.getAllIds(baseDir).size());
                    routes.add(new SubDirectory(baseDir.replaceFirst(sequenceDataDirectory+ "/","").
                            replace("/","->"),
                            getAllIds(baseDir).size()));
//                routes.add(new HashMap<>(baseDir);
//                dataDirectoryParser.getAllIds(baseDir);
//                System.out.println(baseDir);
                    return;
                }
                for(String subDir: subDirectories) {
                    traverseSubDirPath(routes,subDir);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void traverseSequenceSubDirPath(List<String> routes, SEQUENCE_FILES fileType) {
            traverseSequenceSubDirPath(routes,sequenceDataDirectory,fileType);
        }

        private void traverseSequenceSubDirPath(List<String> routes, String baseDir, SEQUENCE_FILES fileType) {
            try {
                List<String> subDirectories = getSequenceSubdirectories(baseDir,fileType).
                        map(x -> x.toString()).collect(Collectors.toList());
                if(subDirectories.size() == 0) {
//                    routes.add(baseDir.substring(0,baseDir.lastIndexOf("/")));
                    if(baseDir.endsWith(fileType.identifier)) {
//                        routes.add(baseDir);
                        routes.add(baseDir.substring(0,baseDir.lastIndexOf("/")));
                    }
                    return;
                }
                for(String subDir: subDirectories) {
                    traverseSequenceSubDirPath(routes,subDir,fileType);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public Stream<Path> getSubdirectories(String baseDirectory) throws IOException {
            return Files.walk(Paths.get(baseDirectory),1).filter(
                    f ->
                            (
                                    Files.isDirectory(f) && !( f.toString().equals(baseDirectory) ||
                                            f.toString().endsWith(SEQUENCE_FILES.BED.identifier) ||
                                            f.toString().endsWith(SEQUENCE_FILES.GB.identifier) ||
                                            f.toString().endsWith(SEQUENCE_FILES.FASTA.identifier) )
                            )
            );
//            File baseDir = new File(baseDirectory);
//            if(!baseDir.isDirectory()) {
//                LOGGER.warn(baseDirectory + " is no directory");
//            }
//            else {
//                File [] subdirs = baseDir.listFiles();
//
//            }
        }

        public Stream<Path> getSequenceSubdirectories(String baseDirectory, SEQUENCE_FILES fileType) throws IOException {
            return Files.walk(Paths.get(baseDirectory),1).filter(
                    f ->
                            (
                                    Files.isDirectory(f) &&!( f.toString().equals(baseDirectory)) //&& f.toString().equals(fileType.identifier)
                            )
            );
//            File baseDir = new File(baseDirectory);
//            if(!baseDir.isDirectory()) {
//                LOGGER.warn(baseDirectory + " is no directory");
//            }
//            else {
//                File [] subdirs = baseDir.listFiles();
//
//            }
        }

        public Stream<Path> getFileStream(String baseDirectory, SEQUENCE_FILES fileType) throws IOException {
//            Stream<Path> walk = Files.walk(Paths.get(sequenceDataDirectory + "/" + SEQUENCE_FILES.FASTA.identifier +"/"),1).filter(Files::isRegularFile).filter(x -> x.toString().contains("mitochondrion"));
            return Files.walk(Paths.get(baseDirectory + "/" + fileType.identifier +"/"),1).
                    filter(Files::isRegularFile);
        }

        public Stream<Path> getFastaFileStream(String baseDirectory) throws IOException {
//            Stream<Path> walk = Files.walk(Paths.get(sequenceDataDirectory + "/" + SEQUENCE_FILES.FASTA.identifier +"/"),1).filter(Files::isRegularFile).filter(x -> x.toString().contains("mitochondrion"));
            return getFileStream(baseDirectory,SEQUENCE_FILES.FASTA);
        }

        public List<String> filterFileNamesExclude(List<String> directories, List<String> excludes) {
            return directories.stream().
                    filter(d ->
                            ! excludes.stream().anyMatch(e -> d.contains(e))).
                    collect(Collectors.toList());
        }

        public List<File> filterFilesInclude(List<File> directories, List<String> includes) {
            return directories.stream().
                    filter(d ->
                            includes.stream().anyMatch(e -> d.getAbsolutePath().contains(e))).
                    collect(Collectors.toList());
        }

        public List<File> filterFilesExclude(List<File> directories, List<String> excludes) {
            return directories.stream().
                    filter(d ->
                            ! excludes.stream().anyMatch(e -> d.getAbsolutePath().contains(e))).
                    collect(Collectors.toList());
        }

        public List<File> getAllFiles(List<String> excludes, SEQUENCE_FILES fileType) {
            List<File> files = new ArrayList<>();
            List<String> directories = new ArrayList<>();
            traverseSequenceSubDirPath(directories,fileType);
            if(excludes != null && excludes.size() > 0) {
                directories = directories.stream().
                        filter(d ->
                                ! excludes.stream().anyMatch(e -> d.contains(e))).
                        collect(Collectors.toList());
            }

            Auxiliary.printSeq(directories);
            for(String directory: directories) {
                try {
                    files.addAll(getFileStream(directory, fileType).map(x -> x.toFile()).collect(Collectors.toList()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return files;
        }


//        public Stream<Path> getFastaFileStream() throws IOException {
////            Stream<Path> walk = Files.walk(Paths.get(sequenceDataDirectory + "/" + SEQUENCE_FILES.FASTA.identifier +"/"),1).filter(Files::isRegularFile).filter(x -> x.toString().contains("mitochondrion"));
//            return Files.walk(Paths.get(sequenceDataDirectory + "/" + SEQUENCE_FILES.FASTA.identifier +"/"),1).
//                    filter(Files::isRegularFile);
//        }

//        List<File> getAllFastaFiles(List<String> excludes) {
//            List<File> fastaFiles = new ArrayList<>();
//            List<String> directories = new ArrayList<>();
//            traverseSequenceSubDirPath(directories,sequenceDataDirectory,SEQUENCE_FILES.FASTA);
//            Auxiliary.printSeq(directories);
////            Iterator<String> it = directories.iterator();
////            while (it.hasNext()) {
////                String dir = it.next();
////                for(String exclude: excludes) {
////                    if(dir.contains(exclude))  {
////                        it.remove();
////                    }
////
////                }
////                ids.addAll(getAllIds(dir));
////            }
//            for(String dir: directories) {
//                for(String exclude: excludes) {
//                    if(!dir.contains(exclude))  {
//                        ids.addAll(getAllIds(dir));
//                    }
//
//                }
//
//            }
//            return ids;
//        }

        List<String> getAllIds(List<String> excludes) {
            List<String> ids = new ArrayList<>();
            List<String> directories = new ArrayList<>();
            traverseSequenceSubDirPath(directories,sequenceDataDirectory,SEQUENCE_FILES.FASTA);
            Auxiliary.printSeq(directories);
//            Iterator<String> it = directories.iterator();
//            while (it.hasNext()) {
//                String dir = it.next();
//                for(String exclude: excludes) {
//                    if(dir.contains(exclude))  {
//                        it.remove();
//                    }
//
//                }
//                ids.addAll(getAllIds(dir));
//            }
            for(String dir: directories) {
                if(excludes == null || excludes.isEmpty()) {
                    ids.addAll(getAllIds(dir));
                }
                else {
                    for(String exclude: excludes) {
                        if(!dir.contains(exclude))  {
                            ids.addAll(getAllIds(dir));
                        }

                    }
                }


            }
            return ids;
        }

        /**
         * Get NCBI identifier of all records stored in the provided refseq directory
         * @return
         */
        List<String> getAllIds(String baseDirectory) {
            List<String> ids = null;
            try (Stream<Path> walk = getFastaFileStream(baseDirectory))
            {
                // get all ids in fasta directory
                ids = walk.
                        map(x -> x.toString().substring(x.toString().
                                lastIndexOf("/")+1,x.toString().indexOf("."))).
                        filter(s -> !s.contains("mitochondrion") && s.length() > 0).
                        collect(Collectors.toList());

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            return ids;
        }

        /**
         * Get NCBI identifier of all records stored in the provided refseq directory
         * @return
         */
        List<String> getAllIds() {
//            List<String> ids = null;
//            try (Stream<Path> walk = getFastaFileStream())
//            {
//
//
//                // get all ids in fasta directory
//                ids = walk.
//                        map(x -> x.toString().substring(x.toString().
//                                lastIndexOf("/")+1,x.toString().indexOf("."))).
//                                filter(s -> !s.contains("mitochondrion") && s.length() > 0).
//                                collect(Collectors.toList());
//
//            } catch (IOException e) {
//                e.printStackTrace();
//                System.exit(-1);
//            }
//            return getAllIds(sequenceDataDirectory);
            return getAllIds(Collections.emptyList());
        }

        List<String> getUnpersistedRecords(int amount) {
            List<String> idsToPersist = null;
            //File dir = new File(dataConfiguration.getBedDir())

            try
            {

                // get all ids in fasta directory
                List<String> fastaIds = getAllIds(skipDirectories);

                // get persisted ids
                List<String> persistedIds = RecordTable.getInstance().getPersistedRecordNames(true);

                Set<String> ids = new TreeSet<>(fastaIds);
                ids.removeAll(persistedIds);
                idsToPersist = new ArrayList<>(ids);
                if(amount != -1) {
                    if(amount > idsToPersist.size()) {
                        LOGGER.warn("Only " + idsToPersist.size() + " records remaining.");
                    }
                    else {
                        idsToPersist = idsToPersist.subList(0,amount);
                    }
                }

            } catch (SQLException s) {
                s.printStackTrace();
                System.exit(-1);
            }
            return idsToPersist;
        }

        List<String> getPersistedRecords (List<String> excludes,boolean onlyVisited){
            List<String> ids = null;
            //File dir = new File(dataConfiguration.getBedDir())

            try
            {

                // get all ids in fasta directory
                ids = getAllIds(excludes);

                // get persisted ids
                List<String> persistedIds = RecordTable.getInstance().getPersistedRecordNames(onlyVisited);

                ids.retainAll(persistedIds);


            } catch (SQLException s) {
                s.printStackTrace();
                System.exit(-1);
            }
            return ids;
        }

        List<String> getPersistedRecords (List<String> excludes){
            return getPersistedRecords(excludes,true);
        }

        List<String> getPersistedRecords (){
            List<String> ids = null;
            //File dir = new File(dataConfiguration.getBedDir())

            try
            {

                // get all ids in fasta directory
                ids = getAllIds();

                // get persisted ids
                List<String> persistedIds = RecordTable.getInstance().getPersistedRecordNames(true);

                ids.retainAll(persistedIds);


            } catch (SQLException s) {
                s.printStackTrace();
                System.exit(-1);
            }
            return ids;
        }

        void removePersistedRecords(List<String> idsToCheck) {
            // copy ids
            List<String> check = new ArrayList<>(idsToCheck);

            try {
                // get persisted ids
                List<String> persistedIds = RecordTable.getInstance().getPersistedRecordNames(true);
                // remove already persisted records
                idsToCheck.removeAll(persistedIds);

                check.removeAll(idsToCheck);
                for(String record : check) {
                    LOGGER.warn("Record " + record + " is persisted already. Skipping this record.");
                }

            } catch (SQLException s) {
                s.printStackTrace();
                System.exit(-1);
            }
        }

        void removeMissingRecords(List<String> idsToCheck) {
            String id = null;
            for(Iterator<String> it = idsToCheck.iterator(); it.hasNext(); ) {
                try {
                    id = it.next();
                    findFastaFile(id);
                    findGBFile(id);
                } catch(FileNotFoundException | AmbiguousMatchException e) {
                    LOGGER.warn("Record " + id + " does not have adequate fasta or gb files. Skipping this record.");
                    it.remove();
                }
            }
        }

        void removeUnpersistedRecords(List<String> idsToCheck) {

            // copy ids
            List<String> check = new ArrayList<>(idsToCheck);
            try {

                // get persisted ids
                List<String> persistedIds = RecordTable.getInstance().getPersistedRecordNames(true);
                // only keep ids that are already persisted
                idsToCheck.retainAll(persistedIds);

                check.removeAll(idsToCheck);
                for(String record : check) {
                    LOGGER.warn("Record " + record + " not persisted so far. Skipping this record.");
                }

            } catch (SQLException s) {
                s.printStackTrace();
                System.exit(-1);
            }

        }

        // file related functions
        private  void checkFileFormat(String fileName, String type) throws IllegalArgumentException, UnknownTypeException {
            String extension = fileName.substring(fileName.lastIndexOf(".")+1);
            switch (type) {
                case "bed":
                    if(!extension.equals("bed")) {
                        throw new IllegalArgumentException("Wrong bed file format " + extension);
                    }
                    break;
                case "genbank":
                    if(!extension.equals("gb")) {
                        throw new IllegalArgumentException("Wrong genbank file format " + extension);
                    }
                    break;
                case "fasta":
                    if(!extension.equals("fna")) {
                        throw new IllegalArgumentException("Wrong bed file format " + extension);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unknown file format " + type);

            }
        }

        private String getSequenceFileExtension(SEQUENCE_FILES fileType) {
            switch (fileType) {
                case BED:
                    return ".bed";
                case FASTA:
                    return ".fna";
                case GB:
                    return ".gb";
                case RECORD:
                    return ".txt";
            }
            return null;
        }

        private void removeDirIfEmpty(SEQUENCE_FILES fileType) {
            File dir = new File(sequenceDataDirectory  + "/"+ fileType.identifier);
            if(!dir.isDirectory()) {
                LOGGER.warn(dir + " is no directory");
            }
            else if(dir.list().length == 0) {
                dir.delete();
            }
        }

        private void removeSequenceDirsIfEmpty() {
            removeDirIfEmpty(SEQUENCE_FILES.BED);
            removeDirIfEmpty(SEQUENCE_FILES.FASTA);
            removeDirIfEmpty(SEQUENCE_FILES.GB);
        }

        private File findFile(String id, SEQUENCE_FILES fileType, List<String> directories) throws FileNotFoundException, AmbiguousMatchException{
            File file = null;
            for(String dir: directories) {
                try {
                    file = findFile(id,fileType,dir);
                    break;
                } catch (FileNotFoundException e) {

                }
            }

            if(file == null) {
                throw new FileNotFoundException("No "+ fileType.identifier+" file discovered for id " + id);
            }
            return  file;
        }

        private File findFile(String id, SEQUENCE_FILES fileType, String baseDirectory) throws FileNotFoundException, AmbiguousMatchException{
            File dir = new File(baseDirectory  + "/"+ fileType.identifier);
            if(!dir.isDirectory()) {
                throw new FileNotFoundException("Directory " + dir.getName() + " does not exist");
            }
            FileFilter fileFilter = new WildcardFileFilter( id + "*" + getSequenceFileExtension(fileType));
            File[] files = dir.listFiles(fileFilter);
            if(files.length == 0) {
                LOGGER.debug("No "+ fileType.identifier+" file discovered for id " + id);
                throw new FileNotFoundException("No "+ fileType.identifier+" file discovered for id " + id);
            } else if(files.length > 1) {
                LOGGER.error("Multiple "+ fileType.identifier+" file matches for id " + id);
                throw new AmbiguousMatchException("Multiple "+ fileType.identifier+" file matches for id " + id);
            }
            LOGGER.debug("Successfully discovered "+ fileType.identifier+" file "+ files[0].getName() +" for id " + id);
            return  files[0];
        }

        private File findFile(String id, SEQUENCE_FILES fileType) throws FileNotFoundException, AmbiguousMatchException{
            return  findFile(id,fileType,sequenceDataDirectory);
        }

        private File findFastaFile(String id) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.FASTA);

        }

        private File findGBFile(String id) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.GB);

        }

        private File findBedFile(String id) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.BED);

        }

        private File findRecordFile(String id) throws FileNotFoundException,  AmbiguousMatchException {

            return findFile(id,SEQUENCE_FILES.RECORD);

        }

        private File findFastaFile(String id, List<String> directories) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.FASTA,directories);

        }

        private File findGBFile(String id, List<String> directories) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.GB,directories);

        }

        private File findBedFile(String id, List<String> directories) throws FileNotFoundException, AmbiguousMatchException {

            return  findFile(id,SEQUENCE_FILES.BED,directories);

        }

        private File findRecordFile(String id, List<String> directories) throws FileNotFoundException,  AmbiguousMatchException {

            return findFile(id,SEQUENCE_FILES.RECORD,directories);

        }

        private void moveFileToSubDir(File file, String subdirname) throws IOException {

            File dir = new File(file.getAbsolutePath().replaceAll(file.getName(),"") + subdirname);
            if(!dir.exists()) {
                dir.mkdirs();

            }
            if(Files.move(file.toPath(),Paths.get(dir.getAbsolutePath()+ "/"+ file.getName())) != null) {
                LOGGER.debug("Successfully moved " + file.getName());
            }
        }

        private void moveFileToSubDir(File file, String subdirname, SEQUENCE_FILES filetype) throws IOException {

            File dir = new File(file.getAbsolutePath().replaceAll(filetype.identifier + "/" + file.getName(),"") + subdirname + "/" + filetype.identifier);
            if(!dir.exists()) {
                dir.mkdirs();
            }
            if(Files.move(file.toPath(),Paths.get(dir.getAbsolutePath()+ "/"+ file.getName())) != null) {
                LOGGER.debug("Successfully moved " + file.getName());
            }
        }

        private void moveSequenceFilesByTaxonicGroup(File bedFile, File fastaFile, File gbFile, String taxonomicGroupName) {
            try {
                if(bedFile != null) {
                    moveFileToSubDir(bedFile,taxonomicGroupName,SEQUENCE_FILES.BED);
                }
                if(fastaFile != null) {
                    moveFileToSubDir(fastaFile,taxonomicGroupName,SEQUENCE_FILES.FASTA);
                }
                if(gbFile != null) {
                    moveFileToSubDir(gbFile,taxonomicGroupName,SEQUENCE_FILES.GB);
                }

            } catch (IOException i) {
                LOGGER.error(i.getMessage());
            }
        }

        private void moveAmbigSequenceFiles(File bedFile, File fastaFile, File gbFile) {
            try {
                if(bedFile != null) {
                    moveFileToSubDir(bedFile,"ambig",SEQUENCE_FILES.BED);
                }
                if(fastaFile != null) {
                    moveFileToSubDir(fastaFile,"ambig",SEQUENCE_FILES.FASTA);
                }
                if(gbFile != null) {
                    moveFileToSubDir(gbFile,"ambig",SEQUENCE_FILES.GB);
                }

            } catch (IOException i) {
                LOGGER.error(i.getMessage());
            }
        }

        private void movePersistedSequenceFiles(File bedFile, File fastaFile, File gbFile) {
            try {
                if(bedFile != null) {
                    moveFileToSubDir(bedFile,"persisted",SEQUENCE_FILES.BED);
                }
                if(fastaFile != null) {
                    moveFileToSubDir(fastaFile,"persisted",SEQUENCE_FILES.FASTA);
                }
                if(gbFile != null) {
                    moveFileToSubDir(gbFile,"persisted",SEQUENCE_FILES.GB);
                }

            } catch (IOException i) {
                LOGGER.error(i.getMessage());
            }
        }
    }

    private class PersistenceInputParser extends InputParser {

        protected PersistenceInputParser(String[] args) { super(args);
        }

        protected void setArguments() {
            this.parser = ArgumentParsers.newFor("Graph pesistence").
                    build().description("Specify parameters for graph persistence");



            parser.addArgument("--action").help("Specify what is to be done.\n" +
                    "Options are: RECORD_CHECK_ONLY (only check the specified records) \n" +
                    "GET_NUCLEOTIDE_COUNT (Get nucleotide counts of sequencefiles) \n" +
                    "GET_SEQUENCE_COUNT_STATISTICS (Get count statistic of sequencefiles) \n" +
                    "GROUP_BY_TAXONOMIC_GROUP (Group sequence files based on taxonomy) \n" +
                    "MOVE_AMBIG_SEQUENCES (Move abiguous sequences in refseq dir) \n" +
                    "MOVE_PERSISTED_SEQUENCES (Move persisted sequences in refseq dir) \n" +
                    "MAP_SEQUENCE (MAP Sequence to db) \n" +
                    "RECORD_PERSIST_PSQL_ONLY (only persist record in psql db) \n" +
                    "RECORD_PERSIST_PDB_ONLY (only persist record in graph db) \n" +
                    "RECORD_PERSIST (persist in both databases).\n" +
                    "RECORD_DELETE (delete specified records).\n" +
                    "PERSIST_TAXONOMY (persist taxonomy, requires --taxonomy-dir to be specified).\n" +
                    "PERSIST_NODEPAIR (persist nodepairs, requires --nodePair-file to be specified).\n" +
                    "DELETE_ALL (delete the entire de brujin graph and recreate the schema).\n" +
                    "CREATE (create new de brujin graph).\n" +
                    "PERSIST_MANUALLY (Manually persist de brujin graph. " +
                    "Requires --data-configuration-file, --database-name and --keyspace to be specified).\n" +
                    "Default is RECORD_PERSIST").type(ACTION.class).required(true);
            parser.addArgument("--refseq").help("Name of the refseq version");
            parser.addArgument("--amount").
                    help("Amount of records to be persisted. If not specified all remaining (non persistent) records are persisted.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--records").help("Specifiy records explicity").nargs("*");
            parser.addArgument("--fasta-file").help("Fasta file to be used for sequence mapping.");
            parser.addArgument("--scientific-names").nargs("*").help("Scientific name for taxonomy");
            parser.addArgument("--k").
                    help("Size of a kmer. Specifies the 'length' of a node. An edge then has a 'length' of k+1.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--k-small").
                    help("Size of a psql kmer. Specifies the 'length' of a node. An edge then has a 'length' of k+1.").
                    type(Integer.class).setDefault(-1);
            parser.addArgument("--no-mitos-annotation").
                    help("Give notification that provided bed files are not mitos annotated.\nOtherwise mitos annotation is assumed.").
                    type(Boolean.class).action(Arguments.storeTrue()).setDefault(false);
            parser.addArgument("--keyspace").help("Name of the graph database keyspace.");
            parser.addArgument("--ambig-filter").help("Specify how ambiguous records are to be treated." +
                    "\nOptions are: NO_FILTER (replace all ambiguous nucleotides by their possible nucleotides)" +
                    "\nSKIP_FILTER (skip ambiguous records)\nCUT_FILTER (cut out all affected kmers)." +
                    "\nIf no filter is specified, SKIP_FILTER is applied.").
                    choices("NO_FILTER","SKIP_FILTER","CUT_FILTER").setDefault("SKIP_FILTER");
            parser.addArgument("--linear-topology").type(Boolean.class).setDefault(false);
            parser.addArgument("--move-ambig").type(Boolean.class).setDefault(true);
            parser.addArgument("--skip-directories").nargs("*").help("basename of directories not to consider in recursive searches");
            parser.addArgument("--gap-open").type(Integer.class).setDefault(-1);
            parser.addArgument("--gap-extend").type(Integer.class).setDefault(-1);
            parser.addArgument("--runs").type(Integer.class).setDefault(50);
            parser.addArgument("--length").type(Integer.class).setDefault(50);

        }

        private void requireKmerLengthExit() {
            if(k == -1) {
                LOGGER.error("Kmer length must be provided. Use --k.");
                exit(-1);
            }
        }

        private void requireSmallKmerLengthExit(Integer kSmall) {
            if(kSmall == -1) {
                LOGGER.error("Small Kmer length must be provided. Use --k-small.");
                exit(-1);
            }
        }

        private void requireFastaFile(String fastaFile) {
            if(fastaFile == null) {
                LOGGER.error("fasta file must be provided. " +
                        "Use --fasta-file.");
                exit(-1);
            }
        }

        private void requireKeySpaceExit(String keyspace) {
            if(keyspace == null) {
                LOGGER.error("Keyspace must be provided. Use --keyspace (e.g. debrujin) to specify cassandra keyspace to use.");
                exit(-1);
            }
        }

        private void requireRefSeqExit() {
            if(refseq == null) {
                LOGGER.error("Refseq directory name must be specified. Use --refseq (e.g. refseq98)." );
                exit(-1);
            }
        }

        private void requireScientificNameExit(List<String> scientificName) {
            if(scientificName == null) {
                LOGGER.error("Scientific name must be specified. Use --scientific-name (e.g. Metazoa)." );
                exit(-1);
            }
        }

        @Override
        protected void executeRoutines(Namespace res) {
            dataDirectoryParser = new DataDirectoryParser();
            refseq = res.getString("refseq");
            String keyspace = res.getString("keyspace");

            if(keyspace != null) {
                LOGGER.info("Using keyspace " + keyspace);
            }
            k = res.getInt("k");
            if(k != -1) {
                LOGGER.info("Using kmer size of " + k);
            }
            if(refseq != null) {
                LOGGER.info("Using Refseq:  " + refseq);
                dataDirectoryParser.setSequenceDataDirectory(ProjectDirectoryManager.getSEQUENCE_DATA()+ "/" + refseq);
            }
            action = ACTION.valueOf(res.getString("action"));
            LOGGER.info("Action to perform: " + action);
            switch (action) {
                case MOVE_FILES:
                    requireRefSeqExit();
                    break;
                case PERSIST_GRAPH_DB_MINIMAL:
                    requireRefSeqExit();
                    break;
                case CREATE_RANDOM_EDGE_WEIGHTS:
                    requireRefSeqExit();
                    break;
                case PERSIST_MAPPING_DATA:
                    requireRefSeqExit();
                    break;
                case STORE_RECORDS_PSQL:
                    requireRefSeqExit();
                    break;
                case CREATE_SEQUENCE_MAP_GRAPH:
                    requireSmallKmerLengthExit(res.getInt("k_small"));
                    requireFastaFile(res.getString("fasta_file"));
                    break;
                case PERSIST_RECORD_PROPERTIE_PSQL:
                    requireRefSeqExit();
                    break;
                case PERSIST_GENOME_PSQL:
                    requireRefSeqExit();
                    break;
                case PERSIST_KMERS_PSQL:
                    requireRefSeqExit();
                    requireKmerLengthExit();
                    break;
                case GET_COMPOSITION_SCORES:
                    requireRefSeqExit();
                    break;
                case GET_AMBIG_SEQUENCES_STATISTICS:
                    requireRefSeqExit();
                    break;
                case GET_NUCLEOTIDE_COUNT:
                    requireRefSeqExit();
                    break;
                case GET_SEQUENCE_COUNT_STATISTICS:
                    requireRefSeqExit();
                    break;
                case GROUP_BY_TAXONOMIC_GROUP:
                    requireRefSeqExit();
                    requireScientificNameExit((List<String>)res.get("scientific_names"));
                    break;
                case MOVE_AMBIG_SEQUENCES:
                    requireRefSeqExit();
                    break;
                case MOVE_PERSISTED_SEQUENCES:
                    requireRefSeqExit();
                    break;
                case RECORD_CHECK_ONLY:
                    requireKmerLengthExit();
                    requireRefSeqExit();
                    LOGGER.info("Selected Action: " + "RECORD_CHECK_ONLY (only check the specified records)");
                    break;
                case RECORD_PERSIST_PSQL_ONLY:
                    requireRefSeqExit();
                    LOGGER.info("Selected Action: " + "RECORD_PERSIST_PSQL_ONLY (only persist record in psql db)");
                    break;
                case RECORD_PERSIST_PDB_ONLY:
                    requireKmerLengthExit();
                    requireRefSeqExit();
                    LOGGER.info("Selected Action: " + "RECORD_PERSIST_PDB_ONLY (only persist record in graph db)");
                    break;
                case RECORD_PERSIST:
                    requireKmerLengthExit();
                    requireRefSeqExit();
                    LOGGER.info("Selected Action: " + "RECORD_PERSIST (persist in both databases).");
                    break;
                case PERSIST_TAXONOMY:
                    LOGGER.info("Selected Action: " + "PERSIST_TAXONOMY (persist taxonomy).");
                    break;
                case RECORD_DELETE:
                    requireKmerLengthExit();
                    requireRefSeqExit();
                    LOGGER.info("Selected Action: " + "RECORD_DELETE (delete specified records).");
                    break;
                case DELETE_ALL:
                    LOGGER.info("Selected Action: " + "DELETE_ALL (delete the entire de brujin graph and recreate the schema).");
                    break;
                case CREATE:
                    LOGGER.info("Selected Action: " + "CREATE (create new de brujin graph).");
                    break;
                case PERSIST_MANUALLY:
                    LOGGER.info("Selected Action: " + "PERSIST_MANUALLY.");
                    if(res.getString("data_configuration_file") == null) {
                        LOGGER.error("Option PERSIST_MANUALLY requires --data-configuration-file to be specified" );
                        exit(-1);
                    }
                    else {
                        LOGGER.info("Using data configuration file: " + res.getString("data_configuration_file"));
                    }
                    requireKeySpaceExit(keyspace);
                    requireKmerLengthExit();
                    break;
                default:
                    LOGGER.error("Invalid Action " + action);
                    System.exit(-1);
            }

            if(action == ACTION.MOVE_FILES) {
                moveFiles();
            }
            if(action == ACTION.PERSIST_GRAPH_DB_MINIMAL) {
                persistToGraphDataBaseMinimal();
            }
            if(action == ACTION.CREATE_RANDOM_EDGE_WEIGHTS) {
                createRandomEdgeWeights(res.getInt("runs"));
            }
            if(action == ACTION.PERSIST_MAPPING_DATA) {
                persistMappingData();
            }
            if(action == ACTION.STORE_RECORDS_PSQL) {
                storeRecordsToSQL();
            }
            if(action == ACTION.CREATE_SEQUENCE_MAP_GRAPH) {
                File fastaFile = new File(res.getString("fasta_file"));
                if(!fastaFile.exists()) {
                    LOGGER.error("Fastafile does not exist");
                    System.exit(-1);
                }
                createSequenceMapGraph(fastaFile,res.getInt("k_small"),!res.getBoolean("linear_topology"));
            }
            if(action == ACTION.PERSIST_GENOME_PSQL) {
                persistGenomesToSQL();
            }
            if(action == ACTION.PERSIST_RECORD_PROPERTIE_PSQL) {
                persistRecordPorpertiesToSQL();
            }
            if(action == ACTION.PERSIST_KMERS_PSQL) {
                persistKmersToSQL(res.getInt("k"));
            }
            if(action == ACTION.GET_COMPOSITION_SCORES) {
                createCompositionScores(res.getInt("runs"), res.getInt("gap_open"),res.getInt("gap_extend"),res.getInt("length"));
            }
            if(action == ACTION.GET_NUCLEOTIDE_COUNT) {
                getNucleotideCounts();
            }
            else if(action == ACTION.GET_AMBIG_SEQUENCES_STATISTICS) {
                getAmbigSequencesStatistics();
            }
            else if(action == ACTION.GET_SEQUENCE_COUNT_STATISTICS) {
                createSequenceStatistics();
            }
            else if(action == ACTION.GROUP_BY_TAXONOMIC_GROUP) {
                taxonomyGraph = (TaxonomyGraph)
                        new Graph.Builder(TaxonomyGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                                startOption(LaunchExternal.LAUNCH_OPTION.VOID).build();
                collectTaxonomyGroupMemmbers((List<String>)res.get("scientific_names"));
            }
            else if(action == ACTION.MOVE_AMBIG_SEQUENCES) {
                moveAmbigSequences();
            }
            else if(action == ACTION.MOVE_PERSISTED_SEQUENCES) {
                movePersistedequences();
            }

            else if(action == ACTION.RECORD_CHECK_ONLY ||
                    action == ACTION.RECORD_PERSIST ||
                    action == ACTION.RECORD_PERSIST_PDB_ONLY ||
                    action == ACTION.RECORD_PERSIST_PSQL_ONLY ||
                    action == ACTION.RECORD_DELETE ||
                    action == ACTION.DELETE_ALL ||
                    action == ACTION.CREATE ||
                    action == ACTION.PERSIST_MANUALLY) {

                if(action != ACTION.PERSIST_MANUALLY) {


                    if(action != ACTION.CREATE && action != ACTION.DELETE_ALL) {
                        ids = (List<String>)res.get("records");
                        if(res.getInt("amount") != -1 && ids == null) {
                            LOGGER.info("Persisting "+ res.getInt("amount") +" records.");
                            amount = res.getInt("amount");
                        }
                        else if(res.getInt("amount") == -1  && ids == null){
                            LOGGER.info("Persisting all remaining records.");
                        }
                        else {
                            LOGGER.info("Working with specified records: " + ids);
                        }

                    }
                }



                if(action != ACTION.CREATE && action != ACTION.DELETE_ALL && action != ACTION.RECORD_DELETE) {



                    ambigFilter = SequenceParser.AmbigFilter.valueOf(res.getString("ambig_filter"));

                    LOGGER.info((res.getBoolean("no_mitos_annotation") ?
                            "Bed files are not mitos annotated" : "Bed files are mitos annotated"));
                    mitosFlag = !res.getBoolean("no_mitos_annotation");
                }

                if(action != ACTION.RECORD_PERSIST_PSQL_ONLY) {
                    dbgGraph = (DbgGraph)
                            new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                                    startOption(LaunchExternal.LAUNCH_OPTION.START).build();
                }

                if(action != ACTION.PERSIST_MANUALLY) {
                    execute();
                }
                else {
                    executeManually();
                }

            }
            else if(action == ACTION.PERSIST_TAXONOMY) {
                taxonomyGraph = (TaxonomyGraph)
                        new Graph.Builder(TaxonomyGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                                startOption(LaunchExternal.LAUNCH_OPTION.START).build();
                executeTaxonomy();
            }


        }

    }

    public static void main(String[] args) {

        createDB();
/*** Persist Graphdatabase minimal ***/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","PERSIST_GRAPH_DB_MINIMAL",
//                        "--k","16",
//                        "--ambig-filter","NO_FILTER",
//                        "--refseq","refseq89"});
/*** Persist ***/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","RECORD_PERSIST",
//                        "--k","16",
//                        "--ambig-filter","NO_FILTER",
//                        "--refseq","refseq89",
//                        "--keyspace","debruijn",
//                        "--amount","1000"});
/*** Persist pdb only ***/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","RECORD_PERSIST_PDB_ONLY",
//                        "--k","16",
//                        "--ambig-filter","NO_FILTER",
//                        "--refseq","refseq89",
//                        "--keyspace","debruijn",
//                        "--records","NC_000879"});

//all
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","RECORD_PERSIST_PDB_ONLY",
//                        "--k","16",
//                        "--ambig-filter","NO_FILTER",
//                        "--refseq","refseq89",
//                        "--keyspace","debruijn",
//                        "--records","NC_000879"});
/*** Persist from args file ***/

//        if(args.length == 0 ) {
//            System.out.println("File must be provided as argument");
//            System.exit(-1);
//        }
//        System.out.println(args[0]);
//        List<String> records = new ArrayList<>();
//        FileIO.readSeqFromFile(args[0],records,false,0,Long.MAX_VALUE,String.class);
//        List<String> commands= new ArrayList<>(Arrays.asList("--action","RECORD_PERSIST_PDB_ONLY","--k","16",
//        "--ambig-filter","SKIP_FILTER","--keyspace","debruijn","--refseq","refseq89","--records"));
//        commands.addAll(records);
//        ControlParameterProgramExecutor dataPersister = new ControlParameterProgramExecutor(commands.toArray(new String[commands.size()]));




//        Auxiliary.printSeq(records);
//        List<String> ids = DbgGraph.getIds("/home/lisa/Desktop/missingRecords.txt");
//        List<String> arguments = new ArrayList<>(Arrays.asList("--action","RECORD_PERSIST",
//                "--k","16","--database-name","db","--records"));
//        arguments.addAll(ids);
//        ControlParameterProgramExecutor dataPersister = new ControlParameterProgramExecutor(arguments.toArray(new String[arguments.size()]));
/*** Ambig sequence statistics **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GET_AMBIG_SEQUENCES_STATISTICS","--refseq","refseq89/ambig"});

/*** Move sequences **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","MOVE_PERSISTED_SEQUENCES","--refseq","refseq99", "--psql-configuration-file","psql.properties"});

//        new ControlParameterProgramExecutor(new String[]
//                {"--action","MOVE_AMBIG_SEQUENCES","--refseq","refseq99"});
/*** Group By Taxonomic Group **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Viridiplantae","Opisthokonta","--refseq","refseq99","--psql-configuration-file","psql.properties"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Metazoa","Fungi","--refseq","refseq99/Opisthokonta","--psql-configuration-file","psql.properties"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Porifera","Eumetazoa","--refseq","refseq99/Opisthokonta/Metazoa","--psql-configuration-file","psql.properties"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Bilateria","Cnidaria","Ctenophora","Placozoa ","--refseq","refseq99/Opisthokonta/Metazoa/Eumetazoa","--psql-configuration-file","psql.properties"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Deuterostomia","Protostomia","Xenacoelomorpha","--refseq","refseq99/Opisthokonta/Metazoa/Eumetazoa/Bilateria","--psql-configuration-file","psql.properties"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Chordata","Echinodermata","Hemichordata","--refseq","refseq99/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia","--psql-configuration-file","psql.properties"});


        //refseq89
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Viridiplantae","Opisthokonta","--refseq","refseq89"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Metazoa","Fungi","--refseq","refseq89/Opisthokonta"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Porifera","Eumetazoa","--refseq","refseq89/Opisthokonta/Metazoa"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Bilateria","Cnidaria","Ctenophora","Placozoa ","--refseq","refseq89/Opisthokonta/Metazoa/Eumetazoa"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Deuterostomia","Protostomia","Xenacoelomorpha","--refseq","refseq89/Opisthokonta/Metazoa/Eumetazoa/Bilateria"});

//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Chordata","Echinodermata","Hemichordata","--refseq","refseq89/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia"});

//        new ControlParameterProgramExecutor(new String[]
//        {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Viridiplantae","Opisthokonta","--refseq","refseq89/ambig"});

//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Metazoa","Fungi","--refseq","refseq89/ambig/Opisthokonta"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Porifera","Eumetazoa","--refseq","refseq89/ambig/Opisthokonta/Metazoa"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Bilateria","Cnidaria","Ctenophora","Placozoa ","--refseq","refseq89/ambig/Opisthokonta/Metazoa/Eumetazoa"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Deuterostomia","Protostomia","Xenacoelomorpha","--refseq","refseq89/ambig/Opisthokonta/Metazoa/Eumetazoa/Bilateria"});
//
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GROUP_BY_TAXONOMIC_GROUP","--scientific-names","Chordata","Echinodermata","Hemichordata","--refseq","refseq89/ambig/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia"});
/*** Nucleotide count statistics **/
//        new ControlParameterProgramExecutor(new String[]
//               {"--action","GET_NUCLEOTIDE_COUNT","--refseq","refseq89","--psql-configuration-file","psql.properties"});
/*** Create sequence map graph **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","CREATE_SEQUENCE_MAP_GRAPH","--refseq","refseq89","--k-small","11","--fasta-file",
//                "/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/sequenceData/refseq99/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia/Chordata/fasta/NC_026039.1.fna"});

/*** Move files **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","MOVE_FILES","--refseq","refseq89"});

///*** Persist psql only ***/
////        new ControlParameterProgramExecutor(new String[]
////                {"--action","RECORD_PERSIST_PSQL_ONLY","--k","16", "--ambig-filter","NO_FILTER","--refseq","refseq89"});
///*** Persist genome in psql **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","PERSIST_GENOME_PSQL","--refseq","refseq89"});
//        System.out.println("done with genomes");
///*** Persist kmers in psql **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","PERSIST_KMERS_PSQL","--refseq","refseq89","--k",DbgGraph.K+""});
//        System.out.println("done with kmers");
///*** Persist record properties in psql **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","PERSIST_RECORD_PROPERTIE_PSQL","--refseq","refseq89"});
//
//        System.out.println("done with properties");
/*** Persist mapping data **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","PERSIST_MAPPING_DATA","--refseq","refseq99"});
/*** Store records to psql **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","STORE_RECORDS_PSQL","--refseq","refseq89/ambig"});

/*** Composition scores **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GET_COMPOSITION_SCORES","--refseq","refseq89","--runs","1","--length","50"});
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GET_COMPOSITION_SCORES","--refseq","refseq89","--runs","1","--length","100"});
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","GET_COMPOSITION_SCORES","--refseq","refseq89","--runs","1","--length","1000"});
/*** Random edges **/
//        new ControlParameterProgramExecutor(new String[]
//                {"--action","CREATE_RANDOM_EDGE_WEIGHTS","--refseq","refseq89","--runs","1"});
/*** Sequence count statistics **/
//        new ControlParameterProgramExecutor(new String[]
//               {"--action","GET_SEQUENCE_COUNT_STATISTICS","--refseq","refseq99"});

//        new ControlParameterProgramExecutor(new String[]
//               {"--action","GET_SEQUENCE_COUNT_STATISTICS","--refseq","refseq89/Opisthokonta"});
/*** Persist taxonomy **/
//new ControlParameterProgramExecutor(new String[]
// {"--action","PERSIST_TAXONOMY"});



/*** Create db ***/
//        ControlParameterProgramExecutor dataPersister = new ControlParameterProgramExecutor(new String[]
//                {"--action","CREATE","--keyspace","debruijn","--refseq","refseq89"});
/*** Delete db ***/
//        ControlParameterProgramExecutor dataPersister = new ControlParameterProgramExecutor(new String[]
//                {"--action","DELETE_ALL","--keyspace","debruijn","--refseq","refseq89"});
/*** Map Sequence**/
//        new ControlParameterProgramExecutor(new String[]
//        {"--action","MAP_SEQUENCE",
//                "--fasta-file","/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/sequenceData/refseq99/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia/Chordata/fasta/NC_024698.1.fna",
//                "--psql-configuration-file","psql.properties"});

//        new ControlParameterProgramExecutor(new String[]
//                {"--action","MAP_SEQUENCE",
//                        "--fasta-file","/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/sequenceData/refseq99/Opisthokonta/Metazoa/Eumetazoa/Bilateria/Deuterostomia/Chordata/fasta/NC_026039.1.fna",
//                        "--psql-configuration-file","psql.properties"});

//        new ControlParameterProgramExecutor(new String[]
//                {"--action","MAP_SEQUENCE",
//                        "--fasta-file","NC_038220.1.fna",
//                        "--psql-configuration-file","psql.properties"});

        System.exit(0);
    }
}

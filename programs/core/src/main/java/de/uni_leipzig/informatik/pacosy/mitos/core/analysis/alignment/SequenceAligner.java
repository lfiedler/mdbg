package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapAnalyzer;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.EvalueParameterDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ScoredPositionPositionKmerMappingDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.random.MersenneTwister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public abstract class SequenceAligner {


    public enum NUCLEOTIDE{

        A('A'),
        C('C'),
        T('T'),
        G('G'),
        U('U'),
        Y('Y'),
        R('R'),
        S('S'),
        W('W'),
        K('K'),
        M('M'),
        B('B'),
        V('V'),
        D('D'),
        H('H'),
        N('N')
        ;

        public final char identifier;

        public static NUCLEOTIDE get(char n) {
            for(NUCLEOTIDE nuc : NUCLEOTIDE.values()) {
                if(nuc.identifier == n) {
                    return nuc;
                }
            }
            return null;
        }

        NUCLEOTIDE(char identifier) {
            this.identifier = identifier;
        }
    }
    protected static final Logger LOGGER = LoggerFactory.getLogger(SequenceAligner.class);
    protected JukesCantorScoreMatrix scoreMatrix;
    protected Sequence s1;
    protected Sequence s2;

    protected List<CellPosition> bestPositions;
    protected List<Alignment> alignments;

    protected int bestScore;

    protected int rowCount;             // number of rows
    protected int columnCount;             // number of columns

    protected final int openGapCost;
    protected final int extendGapCost;
    protected final int gapSum;
    protected final String plusScoresFileName;
    protected final String minusScoresFileName;
    protected EvalueParameterDT evaluePlusParameter;
    protected EvalueParameterDT evalueMinusParameter;
    protected double eValue;

    @FunctionalInterface
    interface IntegerTernaryFunction<T> {
        T run(Integer d1, Integer d2, Integer d3);
    }

    @FunctionalInterface
    interface IntegerTernaryConsumer {
        void consume(Integer d1, Integer d2, Integer d3);
    }

    public class AlignmentPair {
        private char alignmentS1;
        private char alignmentS2;
        private char connection;
        private int pos1;
        private int pos2;


        public AlignmentPair(AlignmentPair alignmentPair) {
            this.alignmentS1 = alignmentPair.alignmentS1;
            this.alignmentS2 = alignmentPair.alignmentS2;
            this.connection = alignmentPair.connection;
            this.pos1 = alignmentPair.pos1;
            this.pos2 = alignmentPair.pos2;
        }

        public AlignmentPair(int rowIndex, int columnIndex, int offsetS1, int offsetS2, CellData.DIRECTION direction) {
            setAlignment(rowIndex,columnIndex,offsetS1, offsetS2,direction);
        }

        private void setAlignment(int rowIndex, int columnIndex, int offsetS1, int offsetS2, CellData.DIRECTION direction) {

            switch (direction) {
                case DIAGONAL:
                    pos1 = columnIndex + offsetS1;
                    pos2 = rowIndex + offsetS2;
                    alignmentS1 = s1.getChar(columnIndex);
                    alignmentS2 = s2.getChar(rowIndex);
                    if(alignmentS1 == alignmentS2) {
                        connection = '|';
                    }
                    else {
                        connection = ' ';
                    }

                    break;
                case LEFT:
                    pos1 = columnIndex + offsetS1;
                    pos2 = -1;
                    alignmentS1 = s1.getChar(columnIndex);
                    alignmentS2 = '-';
                    connection = ' ';
                    break;
                case UP:
                    pos1 = -1;
                    pos2 = rowIndex + offsetS2;
                    alignmentS1 = '-';
                    alignmentS2 = s2.getChar(rowIndex);
                    connection = ' ';
                    break;
                default:
                    LOGGER.error("Invalid direction "  +direction);
                    System.exit(-1);
            }
        }

        public char getAlignmentS1() {
            return alignmentS1;
        }

        public void setAlignmentS1(char alignmentS1) {
            this.alignmentS1 = alignmentS1;
        }

        public char getAlignmentS2() {
            return alignmentS2;
        }

        public void setAlignmentS2(char alignmentS2) {
            this.alignmentS2 = alignmentS2;
        }

        public char getConnection() {
            return connection;
        }

        public void setConnection(char connection) {
            this.connection = connection;
        }

        public int getPos1() {
            return pos1;
        }

        public void setPos1(int pos1) {
            this.pos1 = pos1;
        }

        public int getPos2() {
            return pos2;
        }

        public void setPos2(int pos2) {
            this.pos2 = pos2;
        }

        public boolean aligned() {
            return connection == '|' ? true : false;
        }

        public String toString() {
            return alignmentS1 + " \n" + connection + " \n" + alignmentS2 + " ";
        }

    }

    public class Alignment {
        private List<AlignmentPair> alignmentPairs;
        private StringBuilder firstSequenceLine;
        private StringBuilder markUpLine;
        private StringBuilder secondSequenceLine;
        private int defaultCharsPerLine = 100;
        private long alignedPairsCount;
        private List<ScoredPositionPositionKmerMappingDT> positionMappings;


//        private int offsetPosition1 = 0;
//        private int offsetPosition2 = 0;


        public Alignment(List<AlignmentPair> alignmentPairs) {
            this.alignmentPairs = alignmentPairs;
            firstSequenceLine = new StringBuilder();
            markUpLine = new StringBuilder();
            secondSequenceLine = new StringBuilder();
            positionMappings =
                    alignmentPairs.stream().filter(AlignmentPair::aligned).
                            map(alignmentPair ->
                                    new ScoredPositionPositionKmerMappingDT(
                                            s2.sequenceIdentifier,alignmentPair.pos1, alignmentPair.pos2,
                                            s2.strand,bestScore,eValue,s2.sequence)).
                            sorted().
                            collect(Collectors.toList());
            alignedPairsCount = positionMappings.size();
            //alignmentPairs.stream().filter(AlignmentPair::aligned).count();

        }

//        public Alignment(List<AlignmentPair> alignmentPairs, int offsetS1, int offsetS2) {
//            this(alignmentPairs);
//            this.offsetPosition1 = offsetS1;
//            this.offsetPosition2 = offsetS2;
//        }

//        public void addAlignmentPair(AlignmentPair alignmentPair) {
//            alignmentPairs.push(alignmentPair);
//            firstSequenceLine += alignmentPair.alignmentS1;
//            markUpLine += alignmentPair.connection;
//            secondSequenceLine += alignmentPair.alignmentS2;
//        }

        public List<AlignmentPair> getAlignmentPairs() {
            return alignmentPairs;
        }

        public void parseAlignmentPairs() {

            for(int alignmentCounter = alignmentPairs.size()-1; alignmentCounter >= 0; alignmentCounter--) {
                AlignmentPair alignmentPair = alignmentPairs.get(alignmentCounter);
                firstSequenceLine.append(alignmentPair.alignmentS1);
                markUpLine.append(alignmentPair.connection);
                secondSequenceLine.append(alignmentPair.alignmentS2);
//                firstSequenceLine.append(alignmentPair.alignmentS1 + " ");
//                markUpLine.append(alignmentPair.connection+ " ");
//                secondSequenceLine.append(alignmentPair.alignmentS2+ " ");
            }
            Collections.reverse(alignmentPairs);
        }

        public List<Integer> getPositionsS1() {
            return alignmentPairs.stream().
                    map(alignmentPair -> alignmentPair.getPos1())
                    .filter(pos -> pos > -1).collect(Collectors.toList());
        }

        public List<Integer> getPositionsS2() {
            return alignmentPairs.stream().
                    map(alignmentPair -> alignmentPair.getPos1())
                    .filter(pos -> pos > -1).collect(Collectors.toList());
        }

        private int startPositionFirstSequence(int subSequenceStart, int subSequenceEnd, int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,subSequenceEnd).stream().
                    map(alignmentPair -> alignmentPair.getPos1()).
                    filter(pos -> pos > -1).min(Integer::compare).orElse(lastEnd);

        }

        private int endPositionFirstSequence(int subSequenceStart, int subSequenceEnd, int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,subSequenceEnd).stream().
                    map(alignmentPair -> alignmentPair.getPos1()).
                    filter(pos -> pos > -1).max(Integer::compare).orElse(lastEnd);

        }

        private int startPositionSecondSequence(int subSequenceStart, int subSequenceEnd, int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,subSequenceEnd).stream().
                    map(alignmentPair -> alignmentPair.getPos2()).
                    filter(pos -> pos > -1).min(Integer::compare).orElse(lastEnd);

        }

        private int endPositionSecondSequence(int subSequenceStart, int subSequenceEnd, int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,subSequenceEnd).stream().
                    map(alignmentPair -> alignmentPair.getPos2()).
                    filter(pos -> pos > -1).max(Integer::compare).orElse(lastEnd);

        }

        private int startPositionFirstSequence(int subSequenceStart,  int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,alignmentPairs.size()).stream().
                    map(alignmentPair -> alignmentPair.getPos1()).
                    filter(pos -> pos > -1).min(Integer::compare).orElse(lastEnd);

        }

        private int endPositionFirstSequence(int subSequenceStart, int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,alignmentPairs.size()).stream().
                    map(alignmentPair -> alignmentPair.getPos1()).
                    filter(pos -> pos > -1).max(Integer::compare).orElse(lastEnd);

        }

        private int startPositionSecondSequence(int subSequenceStart,int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,alignmentPairs.size()).stream().
                    map(alignmentPair -> alignmentPair.getPos2()).
                    filter(pos -> pos > -1).min(Integer::compare).orElse(lastEnd);

        }

        private int endPositionSecondSequence(int subSequenceStart,  int lastEnd) {
            return alignmentPairs.subList(subSequenceStart,alignmentPairs.size()).stream().
                    map(alignmentPair -> alignmentPair.getPos2()).
                    filter(pos -> pos > -1).max(Integer::compare).orElse(lastEnd);

        }


        public String getParsedAlignment() {

            if(firstSequenceLine.length() == 0) {
                parseAlignmentPairs();
            }
            String parsedAlignment = StringUtils.repeat("*",defaultCharsPerLine*2) + "\n";//String.format("%"+ 2*defaultCharsPerLine + "s","*");
            //            int wholeLineCount = defaultCharsPerLine*(alignmentPairs.size()/defaultCharsPerLine);
            int lineBreakCounter = 0;
            int lastEndS1 = 0, lastEndS2 = 0, statPosS1, startPosS2;
            final String s1Id = s1.sequenceIdentifier + ":\t";
            final String s2Id = s2.sequenceIdentifier + ":\t";
            int idGap = Math.max(s1Id.length(),s2Id.length());

            while(lineBreakCounter+defaultCharsPerLine <= firstSequenceLine.length()) {
                statPosS1 = startPositionFirstSequence(lineBreakCounter,lineBreakCounter+defaultCharsPerLine,lastEndS1) ;
                startPosS2 = startPositionSecondSequence(lineBreakCounter,lineBreakCounter+defaultCharsPerLine,lastEndS2) ;
                lastEndS1 = endPositionFirstSequence(lineBreakCounter,lineBreakCounter+defaultCharsPerLine,lastEndS1) ;
                lastEndS2 = endPositionSecondSequence(lineBreakCounter,lineBreakCounter+defaultCharsPerLine,lastEndS2) ;

                parsedAlignment +=  s1Id + statPosS1+"\t"+
                        firstSequenceLine.substring(lineBreakCounter,lineBreakCounter+defaultCharsPerLine) +"\t" +
                        lastEndS1 +"\n"
                        + String.format("%" + (Math.max((statPosS1+"").length(), (startPosS2+"").length()) + idGap) + "s","" )+ "\t" +
                        markUpLine.substring(lineBreakCounter,lineBreakCounter+defaultCharsPerLine) + "\n" +
                        s2Id +
                        startPosS2 + "\t" +
                        secondSequenceLine.substring(lineBreakCounter,lineBreakCounter+defaultCharsPerLine) +"\t" +
                        lastEndS2 + "\n\n" ;
                lineBreakCounter += defaultCharsPerLine;
            }
            statPosS1 = startPositionFirstSequence(lineBreakCounter,lastEndS1) ;
            startPosS2 = startPositionSecondSequence(lineBreakCounter,lastEndS2) ;
            lastEndS1 = endPositionFirstSequence(lineBreakCounter,lastEndS1) ;
            lastEndS2 = endPositionSecondSequence(lineBreakCounter,lastEndS2) ;


            parsedAlignment += s1Id+ statPosS1+"\t"+
                    firstSequenceLine.substring(lineBreakCounter) + "\t" +
                    lastEndS1 +"\n"
                    + String.format("%" + (Math.max((statPosS1+"").length(), (startPosS2+"").length()) + idGap) + "s","" )+ "\t" +
                    markUpLine.substring(lineBreakCounter) + "\n" +
                    s2Id +
                    + startPosS2 + "\t" +
                    secondSequenceLine.substring(lineBreakCounter) +"\t" +
                    lastEndS2 + "\n" ;

            return parsedAlignment;
        }

        public String getParsedAlignment(int nucleotidesPerLine) {
            defaultCharsPerLine = nucleotidesPerLine;
            return getParsedAlignment();
        }

        public long getLength() {
            return alignedPairsCount;
        }

        public List<ScoredPositionPositionKmerMappingDT> getPositionMappings() {
            return positionMappings;
        }
    }


    public static void printAlignment(Stack<AlignmentPair> alignmentPairs) {
        int counter = 1;
        int defaultCharsPerLine = 50;
        while(!alignmentPairs.empty()) {
            System.out.print(alignmentPairs.peek());
            alignmentPairs.pop();
            if(counter == defaultCharsPerLine) {
                counter = 0;
                System.out.println();
            }
            counter++;
        }
    }

    protected SequenceAligner(int openGapCost, int extendGapCost, JukesCantorScoreMatrix jukesCantorScoreMatrix) {
        this.scoreMatrix = jukesCantorScoreMatrix;
        this.openGapCost = openGapCost;
        this.extendGapCost = extendGapCost;
        gapSum = -openGapCost-extendGapCost;
        String path = ProjectDirectoryManager.getALIGN_SCORERS_DIR() + "/";
        this.plusScoresFileName = path + "plusScoreParameters_l1000_gapO" + openGapCost + "_gapE" + extendGapCost + ".csv";
        this.minusScoresFileName = path + "minusScoreParameters_l1000_gapO" + openGapCost + "_gapE" + extendGapCost + ".csv";
        setEvalueParameter();
    }


    // shuffled scores based on single sequence from database
    private void getShuffledScores(final Sequence referenceSequence, Sequence shuffleSequence, int shuffleCount,
                                   File resultFile,
                                   String header,
                                   Consumer<BufferedWriter> resultGenerator) {

        long seed = System.currentTimeMillis();
        MersenneTwister mersenneTwister = new MersenneTwister(seed);
        BufferedWriter writer;
        Map<NUCLEOTIDE,Long> nucleotideDistribution = shuffleSequence.getNucleotideCount();
        long randNum;
        long cummulativeSum = 0;
        StringBuilder s = new StringBuilder();
        Long currentCount;
        long nucleotideCount;
        long nucleotideCountReference = nucleotideDistribution.values().stream().mapToLong( n -> n).sum();


        try {
            writer = new BufferedWriter(new FileWriter(resultFile,false));
            writer.write("#seed="+ seed + "\n");
            writer.write(header+ "\n");
            for(int shuffleCounter = 0; shuffleCounter < shuffleCount; shuffleCounter++) {
                Map<NUCLEOTIDE,Long> nucleotideDistributionCopy = Auxiliary.deepCopy(nucleotideDistribution);
                nucleotideCount = nucleotideCountReference;

                while (nucleotideCount > 0) {
                    randNum = (long) (mersenneTwister.nextDouble()*nucleotideCount);
                    cummulativeSum = 0;
                    for(NUCLEOTIDE n: nucleotideDistributionCopy.keySet()) {
                        if(randNum < cummulativeSum+nucleotideDistributionCopy.get(n)) {
                            s.append(n.identifier);
                            nucleotideCount--;
                            currentCount = nucleotideDistributionCopy.get(n);
                            if(currentCount == null) {
                                currentCount = 0L;
                            }
                            nucleotideDistributionCopy.put(n,currentCount-1);
                            break;
                        }
                        cummulativeSum+=nucleotideDistributionCopy.get(n);
                    }
                }

                setSequences(referenceSequence,new Sequence(s.toString(),true,2));
                resultGenerator.accept(writer);
                //reset stringbuilder
                s.setLength(0);
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void getShuffledScores(final Sequence referenceSequence, Sequence shuffleSequence, int shuffleCount,
                                  File resultFile) {
        getShuffledScores(referenceSequence,shuffleSequence,shuffleCount,resultFile,
                "scores,l",
                (writer) -> {
                    computeBestScore();
                    traceBack();
                    printScores();

                    try {
                        writer.write(bestScore+","+ getAlignmentLength()+ "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    public void getShuffledScoresNoTraceBack(final Sequence referenceSequence, Sequence shuffleSequence, int shuffleCount,
                                             File resultFile) {
        getShuffledScores(referenceSequence,shuffleSequence,shuffleCount,resultFile,
                "scores",
                (writer) -> {
                    computeBestScoreNoTraceBack();

                    try {
                        writer.write(bestScore+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    // shuffled scores based on all persisted data
    private void getShuffledScores(final Map<NUCLEOTIDE,Long> nucleotideDistribution, int sequenceLength, File resultFile,
                                   String header,
                                   Consumer<BufferedWriter> resultGenerator) {
        LOGGER.info("Using length: " + sequenceLength);
        long seed = System.currentTimeMillis();
        long randNum;
        long cummulativeSum = 0;
        StringBuilder s = new StringBuilder();
        String [] sequences = new String[2];
        int sequenceIndex = 0;
        Long currentCount;
        int counter = 0;
        String nucleotide;
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(resultFile,false));
            writer.write("#seed="+ seed + "\n");
            writer.write(header+ "\n");

            Map<NUCLEOTIDE,Long> nucleotideDistributionCopy =
                    nucleotideDistribution.entrySet().stream().
                            collect(Collectors.toMap( n -> n.getKey(), n -> n.getValue()));
            MersenneTwister mersenneTwister = new MersenneTwister(seed);
            long nucleotideCount = nucleotideDistributionCopy.values().stream().mapToLong( n -> n).sum();
            while (nucleotideCount > 0) {
                randNum = (long) (mersenneTwister.nextDouble()*nucleotideCount);
                cummulativeSum = 0;
                for(NUCLEOTIDE n: nucleotideDistributionCopy.keySet()) {
                    if(randNum < cummulativeSum+nucleotideDistributionCopy.get(n)) {
                        s.append(n.identifier);
                        nucleotideCount--;
                        currentCount = nucleotideDistributionCopy.get(n);
                        if(currentCount == null) {
                            currentCount = 0L;
                        }
                        nucleotideDistributionCopy.put(n,currentCount-1);
                        break;
                    }
                    cummulativeSum+=nucleotideDistributionCopy.get(n);
                }
                // store sequence if length is achieved
                if(s.length() == sequenceLength) {
                    // get a copy of the current sequence
                    sequences[sequenceIndex] = s.toString();
                    //reset stringbuilder
                    s.setLength(0);
                    if(sequenceIndex == 1){
                        // compute the alignment based on s1 and s2 and write the result into resultFile
                        setSequences(new Sequence(sequences[0],true,1),new Sequence(sequences[1],true,2));
                        resultGenerator.accept(writer);
                        counter++;
                        sequenceIndex = 0;
                    }
                    else {
                        sequenceIndex++;
                    }
                    System.out.println(nucleotideCount);
                }

            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void getShuffledScores(final Map<NUCLEOTIDE,Long> nucleotideDistribution, int sequenceLength, File resultFile) {

        getShuffledScores(nucleotideDistribution,sequenceLength,resultFile,
                "scores,l",
                (writer) -> {
                    computeBestScore();
                    traceBack();
                    if(getAlignmentLength() >= 50) {
                        System.out.println(s1.sequence);
                        System.out.println(s2.sequence);
                        System.out.println(bestScore + " " + getAlignmentLength());
                        printScores();
                        getAlignments().forEach(a -> System.out.println(a.getParsedAlignment()));
                        System.out.println();
                    }

                    try {
                        writer.write(bestScore+","+ getAlignmentLength()+ "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );

    }

    public void getShuffledScores(Map<NUCLEOTIDE,Long> nucleotideCounts, int sequenceLength, int runs, boolean strand, String resultDirectory) {
        if(!resultDirectory.contains("/")) {
            resultDirectory = ProjectDirectoryManager.getALIGN_SCORERS_DIR() + "/" + resultDirectory;
        }
        File resultDir = new File(resultDirectory + (strand ? "plus" : "minus")  +
                "RandomScores_l"+sequenceLength+"_gapO" + openGapCost + "_gapE" + extendGapCost);
        if(!resultDir.exists()) {
            resultDir.mkdirs();
        }
        for(int runCount = 0; runCount < runs; runCount++) {

            File resultFile = new File(resultDir.getAbsolutePath()
                    +"/scores_" + runCount + ".csv");


            getShuffledScores(nucleotideCounts,sequenceLength,resultFile);
        }

    }

    public void getShuffledScoresNoTraceBack(final Map<NUCLEOTIDE,Long> nucleotideDistribution, int sequenceLength, File resultFile) {

        getShuffledScores(nucleotideDistribution,sequenceLength,resultFile,
                "scores",
                (writer) -> {
                    computeBestScoreNoTraceBack();

                    try {
                        writer.write(bestScore+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        );

    }

    public void getShuffledScoresNoTraceBack(Map<NUCLEOTIDE,Long> nucleotideCounts, int sequenceLength, int runs, boolean strand, String resultDirectory) {
        if(!resultDirectory.contains("/")) {
            resultDirectory = ProjectDirectoryManager.getALIGN_SCORERS_DIR() + "/" + resultDirectory;
        }
        File resultDir = new File(resultDirectory + (strand ? "plus" : "minus")  +
                "RandomScores_l"+sequenceLength+"_gapO" + openGapCost + "_gapE" + extendGapCost);
        if(!resultDir.exists()) {
            resultDir.mkdirs();
        }
        for(int runCount = 0; runCount < runs; runCount++) {

            File resultFile = new File(resultDir.getAbsolutePath()
                    +"/scores_" + runCount + ".csv");


            getShuffledScoresNoTraceBack(nucleotideCounts,sequenceLength,resultFile);
        }

    }





    protected CellData setCellData(int eScore, int fScore, int currentScore) {
        CellData cellData = new CellData();

        Set<CellData.DIRECTION> directions = new HashSet<>();
        int maxValue = 0;
        if(eScore > fScore) {
            if(eScore > currentScore) { // eScore is maximum of all three
                if(eScore > maxValue) { // eScore > 0
                    directions.add(CellData.DIRECTION.LEFT);
                    maxValue = eScore;
                }
            }
            else if(eScore < currentScore) { // currentScore is maximum of all three
                if(currentScore > maxValue) { // currentScore > 0
                    directions.add(CellData.DIRECTION.DIAGONAL);
                    maxValue = currentScore;
                }
            }
            else if(eScore > maxValue) { // fScore < eScore = currentScore > 0
                directions.add(CellData.DIRECTION.LEFT);
                directions.add(CellData.DIRECTION.DIAGONAL);
                maxValue = currentScore;
            }

        }
        else if(eScore < fScore) {
            if(fScore > currentScore) { // fScore is maximum of all three
                if(fScore > maxValue) { // fScore > 0
                    directions.add(CellData.DIRECTION.UP);
                    maxValue = fScore;
                }
            }
            else if(fScore < currentScore) {  // currentScore is maximum of all three
                if(currentScore > maxValue) { // currentScore > 0
                    directions.add(CellData.DIRECTION.DIAGONAL);
                    maxValue = currentScore;
                }
            }
            else if(fScore > maxValue) { // eScore < fScore = currentScore > 0
                directions.add(CellData.DIRECTION.UP);
                directions.add(CellData.DIRECTION.DIAGONAL);
                maxValue = currentScore;
            }
        }
        else { // eScore = fScore
            if(fScore > currentScore) { // fScore = eScore > currentScore
                if(fScore > maxValue) {// currentScore < fScore = eScore > 0
                    directions.add(CellData.DIRECTION.UP);
                    directions.add(CellData.DIRECTION.LEFT);
                    maxValue = fScore;
                }
            }
            else if(fScore < currentScore) {  // currentScore is maximum of all three
                if(currentScore > maxValue) { // currentScore > 0
                    directions.add(CellData.DIRECTION.DIAGONAL);
                    maxValue = currentScore;
                }
            }
            else if(fScore > maxValue) { // eScore = fScore = currentScore > 0
                directions.add(CellData.DIRECTION.UP);
                directions.add(CellData.DIRECTION.LEFT);
                directions.add(CellData.DIRECTION.DIAGONAL);
                maxValue = currentScore;
            }
        }

        if(maxValue == 0) {
            directions.add(CellData.DIRECTION.STOP);
        }
        cellData.setDirection(directions);
        cellData.setScore(maxValue);


        return cellData;
    }

    protected CellData setCellDataNoTraceBack(int eScore, int fScore, int currentScore) {
        CellData cellData = new CellData();
        cellData.setScore(Math.max(Math.max(Math.max(eScore,fScore),currentScore),0));

        return cellData;
    }

    protected CellData getBoundaryCell() {
        return new CellData(0, CellData.DIRECTION.STOP);
    }

    protected void updateScores(int score, int rowIndex, int columnIndex) {
        if(score >= bestScore) {
            if(score > bestScore) {
                bestPositions.clear();
                bestScore = score;
            }
            bestPositions.add(new CellPosition(rowIndex,columnIndex));
        }

    }

    protected void updateScores(int score) {
        if(score > bestScore) {
            bestScore = score;
        }
    }

    protected boolean onLeftBoundary(int columnIndex) {
        if(columnIndex == 0) {
            LOGGER.debug("on left boundary");
            return true;
        }
        return false;
    }

    protected boolean onUpperBoundary(int rowIndex) {
        if(rowIndex == 0) {
            LOGGER.debug("on upper boundary");
            return true;
        }
        return false;
    }

    protected int mapPosition(int rowIndex, int columnIndex) {
        if(rowIndex+1 > rowCount || columnIndex+1 > columnCount) {
            LOGGER.error("Invalid cell");
            System.exit(-1);
        }
        return rowIndex* columnCount +columnIndex;
    }

    protected int getAlignScore(int rowIndex, int columnIndex) {
        return scoreMatrix.getScore(s1.getChar(columnIndex),s2.getChar(rowIndex));
    }

    public abstract void computeBestScore();

    public abstract void computeBestScoreNoTraceBack();

    public abstract void traceBack();

    public abstract void traceBackFirst();

    public Alignment getAlignment() {
        computeBestScore();
        LOGGER.debug("done alignment");
        if(eValue <= SequenceMapAnalyzer.Parameter.eValueThreshold) {
            traceBackFirst();
        }
        return alignments != null  && alignments.size() > 0 ? alignments.get(0) : null;
    }

    protected void setEvalueParameter() {
        evaluePlusParameter = SparkComputer.getFirstOrElseNull(Spark.getInstance().read().format("csv") .option("inferSchema", "true")
                .option("header", "true").load(plusScoresFileName).as(EvalueParameterDT.ENCODER));
        evalueMinusParameter = SparkComputer.getFirstOrElseNull(Spark.getInstance().read().format("csv") .option("inferSchema", "true")
                .option("header", "true").load(minusScoresFileName).as(EvalueParameterDT.ENCODER));
    }

    /**** Printing ********************************************************************************/
    public abstract void printScores();

    public abstract void printDirections();


    /**** Getter and Setter ********************************************************************************/


    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getOpenGapCost() {
        return openGapCost;
    }

    public int getExtendGapCost() {
        return extendGapCost;
    }

    public double getAlignmentLength() {
        return alignments.stream().mapToLong(a -> a.getLength()).average().getAsDouble();
    }

    public Sequence getS1() {
        return s1;
    }

    public void setS1(Sequence s1) {
        this.s1 = s1;
    }

    public Sequence getS2() {
        return s2;
    }

    public void setS2(Sequence s2) {
        this.s2 = s2;
    }

    public void setSequences(Sequence s1, Sequence s2) {
        this.s1 = s1;
        this.s2 = s2;
        this.alignments = new ArrayList<>();
        this.rowCount = s2.getSequence().length()+1;
        this.columnCount = s1.getSequence().length()+1;
        this.bestScore = 0;
        bestPositions = new ArrayList<>();
    }

    public List<CellPosition> getBestPositions() {
        return bestPositions;
    }

    public void setBestPositions(List<CellPosition> bestPositions) {
        this.bestPositions = bestPositions;
    }

    public List<Alignment> getAlignments() {
        return alignments;
    }

    public void setAlignments(List<Alignment> alignments) {
        this.alignments = alignments;
    }

    public int getBestScore() {
        return bestScore;
    }

    public double geteValue() {
        return eValue;
    }

    public static void main(String[] args) {

        Sequence s1 = new Sequence("ACT",true,1);//new Sequence("ACTGGACT");
        Sequence s2 = new Sequence("CT",true,2);//new Sequence("ACTGGATC");


        System.exit(0);

    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes;

import java.util.*;

public class Distance implements Comparable<Distance> {
    private int pos1;
    private int pos2;
    private int minDistance;

    /**
     * If distance is the distance from pos1 to pos2, direction = DIRECTION.OUT,
     * else direction = DIRECTION.IN
     */
    private Map<DIRECTION,Integer> distances;
    public enum DIRECTION {
        OUT,
        IN
    }

    public Distance(int p1, int p2, int length, boolean circular) {
        setDistance(p1,p2,length,circular);
    }

    @Override
    public int compareTo(Distance distance) {
        return this.minDistance - distance.minDistance;
    }

    @Override
    public boolean equals(Object object) {
        if( object == this) {
            return true;
        }
        if( object == null || object.getClass() != this.getClass()) {
            return false;
        }

        Distance distance = (Distance) object;
        return this.distances.equals(distance.distances);
    }

    @Override
    public String toString() {
        String distanceString = "p1="+ pos1 + " p2=" + pos2 + "\nmindistance=" + minDistance + "\n";
        distanceString += "connection=" + (distances.size() == 1 ? "linear" : "circular") + "\n";
        Integer distance = distances.get(DIRECTION.IN);
        if(distance != null) {
            distanceString += "p1=<-p2 (" + distance + ")\n";

        }
        distance = distances.get(DIRECTION.OUT);
        if(distance != null) {
            distanceString += "p1->p2 (" + distance +")\n";

        }
        return distanceString;
    }

    public void print() {
        System.out.println(this);
    }

    // getter
    public int getPos1() {
        return pos1;
    }

    public int getPos2() {
        return pos2;
    }

    /**
     * Compute distance between positions p1 and p2
     * @param p1 First position of record - zero based
     * @param p2 Second position of record - zero based
     * @param length Length of record
     * @param circular True if record is cyclic, false otherwise
     */
    private void setDistance(int p1, int p2, int length, boolean circular) {
        distances = new HashMap<>();
        if(!circular) {
            //p1 -> p2
            if(p1 <= p2) {
                distances.put(DIRECTION.OUT,p2-p1);
            }
            else { // p1 <- p2
                distances.put(DIRECTION.IN,p1-p2);
            }
        }
        else {
            // simple route
            int d1 = Math.abs(p1-p2);
            // route over periodic boundary conditions
            int d2 = length-Math.max(p1,p2)+Math.min(p1,p2);
            if(p1 <= p2) {
                distances.put(DIRECTION.OUT,d1);
                distances.put(DIRECTION.IN,d2);
            }
            else {
                distances.put(DIRECTION.IN,d1);
                distances.put(DIRECTION.OUT,d2);
            }
            if(d1 <= d2) {
                minDistance = d1;
            }
            else {
                minDistance = d2;
            }
        }
        this.pos1 = p1;
        this.pos2 = p2;

    }

    /**
     * Compute the minimum distance of all possible pairs of positions from positions1 and positions2.
     * @param positions1 Set of positions.
     * @param positions2 Set of positions.
     * @param length Length of the associated Record
     * @return Minimum Distance object with Distance.pos1 of positions1 and Distance.pos2 of positions2.
     * If one of the position sets is empty or null, null is returned instead.
     */
    public static Distance getMinDistance(Collection<Integer> positions1, Set<Integer> positions2, int length, boolean circular) {
        if(positions1 == null || positions1.size() == 0 || positions2 == null || positions2.size() == 0) {
            return null;
        }
        Distance minDistance = new Distance(0,length,length,false);
        for(int pos1 : positions1) {
            for(int pos2 : positions2) {
                Distance distance = new Distance(pos1,pos2,length,circular);
                if(distance.compareTo(minDistance) < 0) {
                    minDistance = distance;
                }
            }
        }

        return minDistance;
    }

//    /**
//     * Compute the minimum distance of all possible pairs of positions from positions1 and positions2.
//     * @param positions1 Set of positions.
//     * @param positions2 Set of positions.
//     * @param length Length of the associated Record
//     * @return Minimum Distance object with Distance.pos1 of positions1 and Distance.pos2 of positions2.
//     * If one of the position sets is empty or null, null is returned instead.
//     */
//    public static Distance getMinDistance(Set<Integer> positions1, Set<Integer> positions2, int length, boolean circular) {
//        if(positions1 == null || positions1.size() == 0 || positions2 == null || positions2.size() == 0) {
//            return null;
//        }
//        Distance minDistance = new Distance(0,length,length,false);
//        for(int pos1 : positions1) {
//            for(int pos2 : positions2) {
//                Distance distance = new Distance(pos1,pos2,length,circular);
//                if(distance.compareTo(minDistance) < 0) {
//                    minDistance = distance;
//                }
//            }
//        }
//
//        return minDistance;
//    }
//
//    /**
//     * Compute the minimum distance of all possible pairs of positions from positions1 and positions2.
//     * @param positions1 Set of positions.
//     * @param positions2 Set of positions.
//     * @param length Length of the associated Record
//     * @return Minimum Distance object with Distance.pos1 of positions1 and Distance.pos2 of positions2.
//     * If one of the position sets is empty or null, null is returned instead.
//     */
//    public static Distance getMinDistance(List positions1, List positions2, int length, boolean circular) {
//        if(positions1 == null || positions1.size() == 0 || positions2 == null || positions2.size() == 0) {
//            return null;
//        }
//        Distance minDistance = new Distance(0,length,length,false);
//        for(Object pos1 : positions1) {
//            for(Object pos2 : positions2) {
//                Distance distance = new Distance((int)pos1,(int)pos2,length,circular);
//                if(distance.compareTo(minDistance) < 0) {
//                    minDistance = distance;
//                }
//            }
//        }
//
//        return minDistance;
//    }

//    public static Pair<Integer,Distance> getMinDistance(Map<Integer,Distance> distanceMapping) {
//        if(distanceMapping != null && distanceMapping.size() != 0) {
//            Iterator<Map.Entry<Integer,Distance>> it =
//                    distanceMapping.entrySet().iterator();
//            Map.Entry<Integer,Distance> entry0 = it.next();
//            Integer recordId = entry0.getKey();
//            Distance minDistance = entry0.getValue();
//
//            while(it.hasNext()) {
//                Map.Entry<Integer,Distance> entry = it.next();
//                if(entry.getValue().compareTo(minDistance) < 0) {
//                    minDistance = entry.getValue();
//                    recordId = entry.getKey();
//                }
//            }
//            return new Pair<>(recordId,minDistance);
//        }
//        else {
//            return null;
//        }
//    }

//    @Override
//    public String toString() {
//        return "d=" +distance +
//                " (p1=" + pos1 + (direction.equals(DIRECTION.IN) ?
//                "<-" : "->") +"p2="+ pos2 + "), Length=" + length;
//    }



    public int getMinDistance() {
        return minDistance;
    }


    public Map<DIRECTION, Integer> getDistances() {
        return distances;
    }

}

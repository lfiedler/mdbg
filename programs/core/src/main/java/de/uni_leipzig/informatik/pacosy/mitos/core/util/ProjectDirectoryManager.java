package de.uni_leipzig.informatik.pacosy.mitos.core.util;

import com.google.common.io.Resources;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ProjectDirectoryManager {

    private static Map<DIRECTORIES,String> directories;
    private static String controlScript;
    private static Logger LOGGER = LoggerFactory.getLogger(ProjectDirectoryManager.class);


    public enum DIRECTORIES {
        CASSANDRA_HOME_DIR,
        CASSANDRA_BIN_DIR,
        CASSANDRA_CONF_DIR,
        GRAPH_PROPERTIES_DIR,
        RECORD_DATA,
        PROJECT_DIR,
        GRAPH_DATA_DIR,
        STATISTICS_DIR,
        INPUT_FILES_DATA,
        LAYOUT_DATA,
        GRAPH_FILES,
        SEQUENCE_DATA,
        TAXONOMIC_DATA,
        RECORD_SUBGRAPH_DIR,
        ALIGN_SCORERS_DIR,
        EDGE_WEIGHTS_DIR,
        PROPERTY_LENGTH_DIR,
        TAXONOMIC_GROUPS_MAPPING_DIR,
        SCRIPTS_DIR;
    }

    static {
        init(FileIO.getEntryFromPropertyFile(new File(Resources.getResource("mitos.properties").getFile()),"project.dir"));
    }

    private ProjectDirectoryManager() {

    }


    private static void init(String projectDirectory) {
        directories = new HashMap<>();

        directories.put(DIRECTORIES.PROJECT_DIR, projectDirectory);
        directories.put(DIRECTORIES.CASSANDRA_HOME_DIR,projectDirectory + "/cassandra");
        directories.put(DIRECTORIES.CASSANDRA_CONF_DIR,directories.get(DIRECTORIES.CASSANDRA_HOME_DIR) + "/conf/cassandra");
        directories.put(DIRECTORIES.CASSANDRA_BIN_DIR,directories.get(DIRECTORIES.CASSANDRA_HOME_DIR)  + "/bin");
        directories.put(DIRECTORIES.GRAPH_PROPERTIES_DIR,directories.get(DIRECTORIES.CASSANDRA_CONF_DIR)  + "/graphAccess");
        directories.put(DIRECTORIES.GRAPH_DATA_DIR,projectDirectory+ "/graphData");
        directories.put(DIRECTORIES.STATISTICS_DIR,projectDirectory+ "/statistics");
        directories.put(DIRECTORIES.EDGE_WEIGHTS_DIR,directories.get(DIRECTORIES.STATISTICS_DIR)+"/edgeWeights");
        directories.put(DIRECTORIES.ALIGN_SCORERS_DIR,directories.get(DIRECTORIES.STATISTICS_DIR)+ "/alignScores" );
        directories.put(DIRECTORIES.PROPERTY_LENGTH_DIR,directories.get(DIRECTORIES.STATISTICS_DIR)+ "/propertyLengthDistribution" );
        directories.put(DIRECTORIES.LAYOUT_DATA,projectDirectory+ "/layouts");
        directories.put(DIRECTORIES.GRAPH_FILES,projectDirectory+ "/graphFiles");

        directories.put(DIRECTORIES.SEQUENCE_DATA,projectDirectory+ "/sequenceData");
        directories.put(DIRECTORIES.TAXONOMIC_DATA,projectDirectory+ "/taxonomicData");
        directories.put(DIRECTORIES.TAXONOMIC_GROUPS_MAPPING_DIR,directories.get(DIRECTORIES.TAXONOMIC_DATA) + "/taxonomicGroupMapping");
        directories.put(DIRECTORIES.SCRIPTS_DIR,projectDirectory+ "/scripts");
        directories.put(DIRECTORIES.RECORD_SUBGRAPH_DIR,directories.get(DIRECTORIES.GRAPH_FILES) + "/recordSubGraphs");
        directories.put(DIRECTORIES.RECORD_DATA,projectDirectory +"/recordData");
//        directories.put(DIRECTORIES.RECORD_DATA,"/mnt/data/lisa"
//                +"/recordData");
        controlScript=directories.get(DIRECTORIES.CASSANDRA_BIN_DIR)+"/control-cassandra.sh";
        File checkFile = new File(controlScript);

        assert (checkFile.isFile());
        mkdirs();
//        Auxiliary.printMap(directories);

//        printDirectories();
    }

    public static void setProjectDirectory(String projectDirectory) {
        init(projectDirectory);
    }

    public static void printDirectories() {
        for(Map.Entry<DIRECTORIES,String> entry: directories.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static String getControlScript() {
        return controlScript;
    }

    private static void mkdirs() {
        directories.values().stream().forEach(d -> new File(d).mkdirs());
    }


    // get directories
    public static Map<DIRECTORIES, String> getDirectories() {
        return directories;
    }

    public static String getCASSANDRA_HOME_DIR() {
        return directories.get(DIRECTORIES.CASSANDRA_HOME_DIR);
    }

    public static void setCASSANDRA_HOME_DIR(String CASSANDRA_HOME_DIR) {
        directories.put(DIRECTORIES.CASSANDRA_HOME_DIR,CASSANDRA_HOME_DIR);
    }

    public static String getCASSANDRA_BIN_DIR() {
        return directories.get(DIRECTORIES.CASSANDRA_BIN_DIR);
    }

    public static void setCASSANDRA_BIN_DIR(String CASSANDRA_BIN_DIR) {
        directories.put(DIRECTORIES.CASSANDRA_BIN_DIR,CASSANDRA_BIN_DIR);
    }

    public static String getCASSANDRA_CONF_DIR() {
        return directories.get(DIRECTORIES.CASSANDRA_CONF_DIR);
    }

    public static void setCASSANDRA_CONF_DIR(String CASSANDRA_CONF_DIR) {
        directories.put(DIRECTORIES.CASSANDRA_CONF_DIR,CASSANDRA_CONF_DIR);
    }

    public static String getGRAPH_PROPERTIES_DIR() {
        return directories.get(DIRECTORIES.GRAPH_PROPERTIES_DIR);
    }

    public static void setGRAPH_PROPERTIES_DIR(String GRAPH_PROPERTIES_DIR) {
        directories.put(DIRECTORIES.GRAPH_PROPERTIES_DIR,GRAPH_PROPERTIES_DIR);
    }

    public static String getPROJECT_DIR() {
        return directories.get(DIRECTORIES.PROJECT_DIR);
    }

    public static void setPROJECT_DIR(String PROJECT_DIR) {
        directories.put(DIRECTORIES.PROJECT_DIR,PROJECT_DIR);
    }

    public static String getGRAPH_DATA_DIR() {
        return directories.get(DIRECTORIES.GRAPH_DATA_DIR);
    }

    public static void setGRAPH_DATA_DIR(String GRAPH_DATA_DIR) {
        directories.put(DIRECTORIES.GRAPH_DATA_DIR,GRAPH_DATA_DIR);
    }

    public static String getSTATISTICS_DIR() {
        return directories.get(DIRECTORIES.STATISTICS_DIR);
    }

    public static void setSTATISTICS_DIR(String STATISTICS_DIR) {
        directories.put(DIRECTORIES.STATISTICS_DIR,STATISTICS_DIR);
    }

    public static String getALIGN_SCORERS_DIR() {
        return directories.get(DIRECTORIES.ALIGN_SCORERS_DIR);
    }

    public static void setALIGN_SCORERS_DIR(String ALIGN_SCORERS_DIR) {
        directories.put(DIRECTORIES.ALIGN_SCORERS_DIR,ALIGN_SCORERS_DIR);
    }

    public static String getEDGE_WEIGHTS_DIR() {
        return directories.get(DIRECTORIES.EDGE_WEIGHTS_DIR);
    }

    public static void setEDGE_WEIGHTS_DIR(String EDGE_WEIGHTS_DIR) {
        directories.put(DIRECTORIES.EDGE_WEIGHTS_DIR,EDGE_WEIGHTS_DIR);
    }


    public static String getLAYOUT_DATA() {
        return directories.get(DIRECTORIES.LAYOUT_DATA);
    }

    public static void setLAYOUT_DATA(String LAYOUT_DATA) {
        directories.put(DIRECTORIES.LAYOUT_DATA,LAYOUT_DATA);
    }

    public static String getSEQUENCE_DATA() {
        return directories.get(DIRECTORIES.SEQUENCE_DATA);
    }

    public static void setSEQUENCE_DATA(String SEQUENCE_DATA) {
        directories.put(DIRECTORIES.SEQUENCE_DATA,SEQUENCE_DATA);
    }

    public static String getTAXONOMIC_DATA() {
        return directories.get(DIRECTORIES.TAXONOMIC_DATA);
    }

    public static void setTAXONOMIC_DATA(String TAXONOMIC_DATA) {
        directories.put(DIRECTORIES.TAXONOMIC_DATA,TAXONOMIC_DATA);
    }

    public static String getSCRIPTS_DIR() {
        return directories.get(DIRECTORIES.SCRIPTS_DIR);
    }

    public static void setSCRIPTS_DIR(String SCRIPTS_DIR) {
        directories.put(DIRECTORIES.SCRIPTS_DIR,SCRIPTS_DIR);
    }

    public static String getGRAPH_FILES_DIR() {
        return directories.get(DIRECTORIES.GRAPH_FILES);
    }

    public static void setGRAPH_FILES_DIR(String GRAPH_FILES) {
        directories.put(DIRECTORIES.GRAPH_FILES,GRAPH_FILES);
    }

    public static String getPROPERTY_LENGTH_DIR() {
        return directories.get(DIRECTORIES.PROPERTY_LENGTH_DIR);
    }

    public static void setPROPERTY_LENGTH_DIR(String PROPERTY_LENGTH_DIR) {
        directories.put(DIRECTORIES.PROPERTY_LENGTH_DIR,PROPERTY_LENGTH_DIR);
    }

    public static String getRECORD_DATA_DIR() {
        return directories.get(DIRECTORIES.RECORD_DATA);
    }

    public static void setTAXONOMIC_GROUPS_MAPPING_DIR(String TAXONOMIC_GROUPS_MAPPING_DIR) {
        directories.put(DIRECTORIES.TAXONOMIC_GROUPS_MAPPING_DIR,TAXONOMIC_GROUPS_MAPPING_DIR);
    }

    public static String getTAXONOMIC_GROUPS_MAPPING_DIR() {
        return directories.get(DIRECTORIES.TAXONOMIC_GROUPS_MAPPING_DIR);
    }

    public static String getRECORD_DATA_DIR(boolean strand, int recordId) {
        String dir = directories.get(DIRECTORIES.RECORD_DATA)+"/" + recordId +
                (strand ? "/PLUS" : "/MINUS");
        new File(dir).mkdirs();
        return dir;
    }

    public static void setRECORD_DATA_DIR(String RECORD_DATA) {
        directories.put(DIRECTORIES.RECORD_DATA,RECORD_DATA);
    }

    public static String getRECORD_SUBGRAPH_DIR(boolean strand, int recordId) {
        return directories.get(DIRECTORIES.RECORD_SUBGRAPH_DIR) +"/" + recordId +
                (strand ? "/PLUS" : "/MINUS");
    }

    public static void setRECORD_SUBGRAPH_DIR(String GRAPH_FILES) {
        directories.put(DIRECTORIES.RECORD_SUBGRAPH_DIR,GRAPH_FILES);
    }

}

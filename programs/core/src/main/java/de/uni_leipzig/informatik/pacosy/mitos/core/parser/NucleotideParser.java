package de.uni_leipzig.informatik.pacosy.mitos.core.parser;

import java.util.*;

public class NucleotideParser {

    private static Map<Character,List<Character>> nucleotideReplacements = new HashMap<>();

    static {
        nucleotideReplacements.put('A',new ArrayList<>(Arrays.asList('A')));
        nucleotideReplacements.put('C',new ArrayList<>(Arrays.asList('C')));
        nucleotideReplacements.put('G',new ArrayList<>(Arrays.asList('G')));
        nucleotideReplacements.put('T',new ArrayList<>(Arrays.asList('T')));

        nucleotideReplacements.put('N',new ArrayList<>(Arrays.asList('A','C','T','G')));
        nucleotideReplacements.put('K',new ArrayList<>(Arrays.asList('T','G')));
        nucleotideReplacements.put('M',new ArrayList<>(Arrays.asList('A','C')));
        nucleotideReplacements.put('B',new ArrayList<>(Arrays.asList('C','T','G')));
        nucleotideReplacements.put('V',new ArrayList<>(Arrays.asList('A','C','G')));
        nucleotideReplacements.put('S',new ArrayList<>(Arrays.asList('C','G')));
        nucleotideReplacements.put('W',new ArrayList<>(Arrays.asList('A','T')));
        nucleotideReplacements.put('D',new ArrayList<>(Arrays.asList('A','T','G')));
        nucleotideReplacements.put('Y',new ArrayList<>(Arrays.asList('C','T')));
        nucleotideReplacements.put('R',new ArrayList<>(Arrays.asList('A','G')));
        nucleotideReplacements.put('H',new ArrayList<>(Arrays.asList('A','C','T')));
    }

    private NucleotideParser() {

    }

    public static List<Character> getNucleotideReplacements(char ambiguousNucleotide) {
        return nucleotideReplacements.get(ambiguousNucleotide);
    }
}

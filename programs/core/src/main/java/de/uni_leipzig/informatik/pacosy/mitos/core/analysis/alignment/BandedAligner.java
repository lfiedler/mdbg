package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import com.google.common.io.Resources;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.AlignCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ScoredPositionPositionKmerMappingDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;

import java.io.File;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

public class BandedAligner extends SequenceAligner{

    private int bandSizeS1;
    private int bandSizeS2;
    private List<List<CellData>> alignMatrix;

    BandedAligner(int openGapCost, int extendGapCost, JukesCantorScoreMatrix jukesCantorScoreMatrix) {
        super(openGapCost,extendGapCost,jukesCantorScoreMatrix);
    }

    /**
     * Set sequences to be aligned. If provided sequences are Bandedsequences,
     * a banded alignment is set up, with bands provided by the sequences.
     * Otherwise a full alignment is done.
     * @param s1
     * @param s2
     */
    @Override
    public void setSequences(Sequence s1, Sequence s2) {
        super.setSequences(s1,s2);
        if(s1 instanceof BandedSequence) {
            this.bandSizeS1 = ((BandedSequence)s1).getBandSize();
        }
        else {
            this.bandSizeS1 = s1.getSequence().length()-1;
        }
        if(s2 instanceof BandedSequence) {
            this.bandSizeS2 = ((BandedSequence)s2).getBandSize();
        }
        else {
            this.bandSizeS2 = s2.getSequence().length()-1;
        }
        this.alignMatrix = new ArrayList<>();
    }

    private boolean outOfBand(int rowIndex, int columnIndex) {
        if(columnIndex > rowIndex+bandSizeS1 || columnIndex < rowIndex-bandSizeS2) {
            return true;
        }
        return false;
    }

    private boolean onRightBandBoundary(int rowIndex, int columnIndex) {

        if(columnIndex == rowIndex+bandSizeS1) {
            LOGGER.debug("on right band boundary");
            return true;
        }
//        if(rowIndex > columnCount-1-bandSizeS1) {
//            return false;
//        }
//        if(columnIndex == Math.min(columnCount-1,rowIndex+bandSizeS1)) {
//            return true;
//        }
        return false;
    }

    private boolean onLeftBandBoundary(int rowIndex, int columnIndex) {
        if(columnIndex == rowIndex-bandSizeS2) {
            LOGGER.debug("on left band boundary");
            return true;
        }
//        if(columnIndex == Math.max(1,rowIndex-bandSizeS2)) {
//            return true;
//        }
        return false;
    }

    private int getIndexOffset(int columnIndex, int offset) {
        return columnIndex-offset;
    }

    private int getColumnOffset(int rowIndex) {
        return Math.max(1,rowIndex-bandSizeS2);
    }

    private CellData getCell(int rowIndex, int columnIndex) {
        return (onLeftBoundary(columnIndex) || onUpperBoundary(rowIndex)) ?
                getBoundaryCell() :
                alignMatrix.get(rowIndex-1).get(getIndexOffset(columnIndex,getColumnOffset(rowIndex)));
    }

    private int getScore(List<CellData> rowScores, int rowIndex, int columnIndex, int offset) {
        if(onLeftBoundary(columnIndex) || onUpperBoundary(rowIndex)) {
            return 0;
        }

        return rowScores.get(getIndexOffset(columnIndex,offset)).getScore();
    }

    private int getF(List<Integer> fValues, int rowIndex, int columnIndex, int offset) {
        if(onUpperBoundary(rowIndex)) {
            return -Integer.MAX_VALUE;
        }
//        System.out.println("get score f at " + getIndexOffset(columnIndex,offset));
        return fValues.get(getIndexOffset(columnIndex,offset));
    }

    private CellData setCellDataOnBandBoundary(int score) {
        CellData cellData = new CellData();
        cellData.setScore(score);
        cellData.setDirection(Collections.singleton(CellData.DIRECTION.DIAGONAL));
        return cellData;
    }

    private void print(Consumer<CellData> printRoutine) {
        if(alignMatrix.size() == 0) {
            LOGGER.warn("No alignment matrix set");
            return;
        }
        System.out.print("\t-\t");
        s1.print();
        int columnCounter;
        List<CellData> row = null;
        for(int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            if(rowIndex == 0) {
                System.out.print("-\t");
            }
            else {
                System.out.print(s2.getSequence().charAt(rowIndex-1) + "\t");
            }
            columnCounter = 0;
            if(rowIndex >0) {
                row = alignMatrix.get(rowIndex-1);
            }
            for(int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                if(outOfBand(rowIndex,columnIndex) || onUpperBoundary(rowIndex) || onLeftBoundary(columnIndex)) {
                    System.out.print("-\t");
                }
                else {
                    printRoutine.accept(row.get(columnCounter));
                    columnCounter++;
                }

            }
            System.out.println();
        }
    }

    public void printScores() {
        print( t -> System.out.print(t.getScore() + "\t"));
    }

    public void printDirections() {
        print(t -> System.out.print(CellData.DIRECTION.toString(t.getDirection()) +"\t"));

    }

    private void computeBestScore(IntegerTernaryFunction<CellData> cellDataComputer,
                                  Function<List<CellData>,List<CellData>> rowStorageRoutine,
                                  IntegerTernaryConsumer scoreUpdateConsumer) {
        LOGGER.debug("Using "+
                "extedGapCost: " + extendGapCost +
                " openGapCost: " + openGapCost +
                " bandsizeS1: " + bandSizeS1 +
                " bandsizeS2: " + bandSizeS2);

        // for i*j= 0 -> e =  -infinity, f = -infinity, h = 0
        // e(i,j) = max {h(i,j-1)+ gapSum, e(i,j-1)-extendGapCost}
        // f(i,j) = max {h(i-1,j)+ gapSum, f(i-1,j)-extendGapCost}
        // h(i,j) = max {h(i-1,j-1)+alignScore(i,j), e(i,j), f(i,j), 0}
        List<CellData> previousRow = new ArrayList<>();
        List<Integer> previousFValues = new ArrayList<>();
        int e = -Integer.MAX_VALUE, f, currentScore;
        int previousOffset = 1, offset;

        for(int rowIndex = 1; rowIndex < rowCount; rowIndex++) {

            List<CellData> currentRow = new ArrayList<>();
            List<Integer> currentFValues = new ArrayList<>();
            offset = getColumnOffset(rowIndex);
            LOGGER.debug(rowIndex + ": " + offset);

            for(int columnIndex = offset; columnIndex <= Math.min(columnCount-1,rowIndex+bandSizeS1); columnIndex++) {

//                LOGGER.debug(columnIndex+"");
                CellData cellData;

                currentScore = getAlignScore(rowIndex,columnIndex) +
                        getScore(previousRow,rowIndex-1,columnIndex-1,previousOffset);

                if(onLeftBoundary(columnIndex-1) || onLeftBandBoundary(rowIndex,columnIndex)) {

                    // e(i,0) = -infinity
                    // getScore(currentRow, rowIndex, 0, offset) yields 0 for left boundary -> so only gapSum
                    e =  gapSum;
                }
                else {
                    // evaluate e the "standard" way
                    e = Math.max(e-extendGapCost,
                            getScore(currentRow,rowIndex,columnIndex-1,offset)+gapSum);
                }
//                LOGGER.debug("e="+ e);

                if(onUpperBoundary(rowIndex -1) || onRightBandBoundary(rowIndex,columnIndex)) {
                    // f(0,j) = -infinity
                    // getScore(previousRow,0,columnIndex,previousOffset) yields 0 for upper boundary -> so only gapSum
                    f = gapSum;
                }
                else {
                    f = Math.max(getF(previousFValues,rowIndex-1,columnIndex,previousOffset)-extendGapCost,
                            getScore(previousRow,rowIndex-1,columnIndex,previousOffset)+gapSum);
                }
//                LOGGER.debug("f="+ f);
                cellData = cellDataComputer.run(e,f,currentScore);

//                LOGGER.debug("max=" + cellData.getScore());
                scoreUpdateConsumer.consume(cellData.getScore(),rowIndex,columnIndex);
                currentFValues.add(f);
                currentRow.add(cellData);

            }

            previousRow = rowStorageRoutine.apply(currentRow);
            previousFValues = currentFValues;
            previousOffset = offset;
        }

        eValue = evaluePlusParameter.eValue(s1.sequence.length(),s2.sequence.length(),bestScore);
    }

    public void computeBestScoreNoTraceBack() {
        computeBestScore(
                (d1,d2,d3) -> setCellDataNoTraceBack(d1,d2,d3),
                (row) -> row,
                (score,rowIndex,columnIndex) -> updateScores(score)
        );
    }

    public void computeBestScore() {

        computeBestScore(
                (d1,d2,d3) -> setCellData(d1,d2,d3),
                (row) -> {
                    alignMatrix.add(row);
                    return alignMatrix.get(alignMatrix.size()-1);
                },
                (score,rowIndex,columnIndex) -> updateScores(score,rowIndex,columnIndex)

        );

    }


    public void align() {
        traceBack();

    }

    public void traceBack() {
        for(CellPosition cellPosition: bestPositions) {
            traceBack(cellPosition.getRowIndex(),cellPosition.getColumnIndex(),new Stack<>());
//            System.out.println();
        }

    }

    public void traceBackFirst() {
        CellPosition cellPosition = bestPositions.get(0);
        traceBack(cellPosition.getRowIndex(),cellPosition.getColumnIndex(),new Stack<>());
    }

    private boolean traceBack(int rowIndex, int columnIndex, Stack<AlignmentPair> alignment) {

        CellData cellData = getCell(rowIndex,columnIndex);
//        System.out.println(rowIndex + " " + columnIndex + " " + cellData.getDirection());
        if(cellData.getDirection().contains(CellData.DIRECTION.STOP)) {
            return true;
        }


        int rowIdx =0, colIdx = 0;
        for(CellData.DIRECTION direction: cellData.getDirection()) {
            alignment.push(new AlignmentPair(rowIndex,columnIndex,s1.offsetPosition,s2.offsetPosition, direction));

            switch (direction) {
                case DIAGONAL:
                    rowIdx = rowIndex - 1;
                    colIdx = columnIndex - 1;
                    break;
                case LEFT:
                    rowIdx = rowIndex;
                    colIdx = columnIndex - 1;
                    break;
                case UP:
                    rowIdx = rowIndex - 1;
                    colIdx = columnIndex;
                    break;
                default:
                    LOGGER.error("Invalid direction "  +direction);
                    System.exit(-1);
            }

            if (traceBack(rowIdx, colIdx, alignment)) {
                List<AlignmentPair> alignmentPairs = alignment.stream().map(AlignmentPair::new).collect(Collectors.toList());
                alignments.add(new Alignment(alignmentPairs));
            }
            alignment.pop();
        }

        return false;

    }


    public int getBandSizeS1() {
        return bandSizeS1;
    }

    public int getBandSizeS2() {
        return bandSizeS2;
    }

    public List<List<CellData>> getAlignMatrix() {
        return alignMatrix;
    }

    public void printAlignments() {
        alignments.forEach(a -> System.out.println(a.getParsedAlignment() + "\n"));
    }

    public void printAlignments(int nucleotidesPerLine) {
        alignments.forEach(a -> System.out.println(a.getParsedAlignment(nucleotidesPerLine) + "\n"));
    }


}

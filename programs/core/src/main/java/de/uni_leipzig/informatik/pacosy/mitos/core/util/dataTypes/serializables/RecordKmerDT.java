package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.Collections;

public class RecordKmerDT implements RecordIdInterface, Serializable {
    protected String kmer;
    protected int recordId;
    protected int recordPosition;
    protected int recordLength;
    public static final Encoder<RecordKmerDT> ENCODER = Encoders.bean(RecordKmerDT.class);

    public RecordKmerDT() {
    }

    public RecordKmerDT(String kmer, int recordId, int recordPosition, int recordLength) {
        this.kmer = kmer;
        this.recordId = recordId;
        this.recordPosition = recordPosition;
        this.recordLength = recordLength;
    }

    public String getKmer() {
        return kmer;
    }

    public void setKmer(String kmer) {
        this.kmer = kmer;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getRecordPosition() {
        return recordPosition;
    }

    public void setRecordPosition(int recordPosition) {
        this.recordPosition = recordPosition;
    }

    public int getRecordLength() {
        return recordLength;
    }

    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }


}

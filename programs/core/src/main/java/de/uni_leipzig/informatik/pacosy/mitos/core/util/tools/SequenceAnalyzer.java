package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.GenomeDT;
import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

public class SequenceAnalyzer {

    protected static final Logger LOGGER = LoggerFactory.getLogger(SequenceAnalyzer.class);
    private final File fastaFile;
    private final boolean topology;
    private final String id;
    private final File resultDir;
    private final File parquetDir;
    private String plusStrandGenome;
    private String minusStrandGenome;
    private int kSmall;


    public enum PARQUET_NAME{
        GENOME,
        PSQL_SHORT_KMERS_PLUS, //
        PSQL_SHORT_KMERS_MINUS, //
        MAPPING_PSQL_SHORT_KMERS_PLUS, //
        MAPPING_PSQL_SHORT_KMERS_MINUS, //
        MAPPING_KMER_POSITIONS_PLUS,
        MAPPING_KMER_POSITIONS_MIUS;
    }

    public static class Builder {
        private File fastaFile;
        private String id;
        private boolean topology;
        private int ksmall = 0;

        public Builder(File fastaFile) {
            this.fastaFile = fastaFile;
            this.topology = true;
        }

        public Builder setTopology(boolean topology) {
            this.topology = topology;
            return this;
        }

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setKsmall(int ksmall) {
            this.ksmall = ksmall;
            return this;
        }

        public SequenceAnalyzer build() {
            if(id == null) {
                id = fastaFile.getName().substring(0,fastaFile.getName().indexOf("."));
            }
            SequenceAnalyzer sequenceAnalyzer = new SequenceAnalyzer(fastaFile,topology,id);


            return sequenceAnalyzer;
        }
    }

    private SequenceAnalyzer(File fastaFile, boolean topology, String id) {
        this.fastaFile = fastaFile;
        this.topology = topology;
        this.id = id;
        File tempFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()  + "/" + id);
        if(tempFile.exists()) {
            LOGGER.warn("Result directory " + tempFile.getAbsolutePath() + " already exists. Will be overriden");
            try {
                FileUtils.deleteDirectory(tempFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.resultDir = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()  + "/" + id);
        this.parquetDir = new File(resultDir.getAbsolutePath() + "/parquets" );

    }



    protected String fetchGenome(boolean strand) {
        return SparkComputer.read(getParquetPath(PARQUET_NAME.GENOME,strand),Encoders.STRING()).head();
    }

//    public void generateParquets() {
//        // genome
//        String sequence = SequenceParser.parseSequenceFromFasta(fastaFile);
//        SparkComputer.persistDataFrame( // plus strand
//                SparkComputer.createDataFrame(
//                        Collections.singletonList(sequence), String.class, Encoders.STRING()),
//                getParquetPath(PARQUET_NAME.GENOME,true));
//        SparkComputer.persistDataFrame( // plus strand
//                SparkComputer.createDataFrame(
//                        Collections.singletonList(sequence), String.class, Encoders.STRING()),
//                getParquetPath(PARQUET_NAME.GENOME,false));
//        // psql short kmers
//
//    }

    public static String getSubSequenceMinusStrand(String sequence, int startPos, int stopPos) {
        return getSubsequencePlusStrand(sequence,sequence.length()-startPos-1,
                sequence.length()-stopPos-1);
    }

    public static String getSubsequencePlusStrand(String sequence, int startPos, int stopPos) {
        if(stopPos+1 > sequence.length()-1 || startPos+1 > sequence.length()-1 || stopPos < 0 || startPos < 0) {
            LOGGER.warn("Invalid range boundaries. Must be within [" + 0 + ","+ (sequence.length()-1) +"]");
            return null;
        }
        if(startPos > startPos) { // cyclic boundary
            return sequence.substring(stopPos) + sequence.substring(0,startPos+1);
        }
        return sequence.substring(startPos,stopPos+1);
    }


    /**** Auxiliary *************************************************/
    private String getParquetPath(PARQUET_NAME parquetName, boolean strand) {
        return parquetDir.getAbsolutePath() + "/" +
                (strand ? "PLUS" : "MINUS") + "/" +
                parquetName + SparkComputer.PARQUET_EXTENSION;
    }



}

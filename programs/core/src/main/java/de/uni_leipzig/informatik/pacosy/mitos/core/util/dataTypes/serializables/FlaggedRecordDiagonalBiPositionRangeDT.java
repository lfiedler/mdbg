package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class FlaggedRecordDiagonalBiPositionRangeDT extends RecordDiagonalBiPositionRangeDT{
    protected int rearrangeFlag;
    public static final Encoder<FlaggedRecordDiagonalBiPositionRangeDT> ENCODER = Encoders.bean(FlaggedRecordDiagonalBiPositionRangeDT.class);
    public static int CYLIC_FLAG = 1;
    public static int NON_CYCLIC_FLAG = 0;
    public enum COLUMN_NAME implements DBColumnName {
        REARRANGE_FLAG("\"rearrangeFlag\""),
        RECORD_START("\"recordStart\""),
        RECORD_END("\"recordEnd\""),
        RECORDID("\"recordId\""),
        DIAGONAL_IDENTIFIER("\"diagonalValue\""),
        START("start"),
        END("end"),
        LENGTH("length");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public FlaggedRecordDiagonalBiPositionRangeDT() {
    }

    public FlaggedRecordDiagonalBiPositionRangeDT(int start, int end, int recordId, int diagonalValue,int recordLength,  int recordStart, int recordEnd, int rearrangeFlag) {
        super(start, end, recordId, diagonalValue, recordLength,recordStart, recordEnd);
        this.rearrangeFlag = rearrangeFlag;
    }

//    public FlaggedRecordDiagonalBiPositionRangeDT(int start, int end, int recordId, int diagonalValue, int recordStart, int recordEnd, int rearrangeFlag) {
//        super(start, end, recordId, diagonalValue, recordStart, recordEnd);
//        this.rearrangeFlag = rearrangeFlag;
//    }
    public static Dataset<FlaggedRecordDiagonalBiPositionRangeDT> convertToFlaggedRecordDiagonalBiPositionRange(Dataset<?> dataset) {
        return dataset.select(
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.RECORDID.rawIdentifier(),
                ColumnIdentifier.RECORD_LENGTH,
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.DIAGONAL_IDENTIFIER.rawIdentifier(),
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.START.identifier,
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.END.identifier,
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.RECORD_START.rawIdentifier(),
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.RECORD_END.rawIdentifier(),
                RecordDiagonalBiPositionRangeDT.COLUMN_NAME.LENGTH.identifier,
                ColumnIdentifier.STRAND,ColumnIdentifier.REARRANGE_FLAG).
                as(ENCODER);
    }
    public int getRearrangeFlag() {
        return rearrangeFlag;
    }

    public void setRearrangeFlag(int rearrangeFlag) {
        this.rearrangeFlag = rearrangeFlag;
    }
}

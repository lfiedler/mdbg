package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.CountInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.javalang.typed;
import scala.Tuple2;

import java.io.Serializable;

import static org.apache.spark.sql.functions.*;

public class PositionMappingDT implements Serializable, PositionInterface, RecordIdInterface {

    private int recordId;
    private Integer[] positions;
    private int position;
    public static final Encoder<PositionMappingDT> ENCODER = Encoders.bean(PositionMappingDT.class);
    public static MapFunction<PositionMappingDT,Integer> POSITION_KEY = p -> p.getPosition();
    public static MapFunction<PositionMappingDT,Integer> RECORD_KEY = p -> p.getRecordId();
    public PositionMappingDT() {

    }

    public PositionMappingDT(int recordId, Integer[] positions, int position) {
        this.recordId = recordId;
        this.positions = positions;
        this.position = position;
    }

    public enum COLUMN_NAME implements DBColumnName {
        POSITION("position"),
        RECORD_ID("\"recordId\""),
        POSITIONS("positions");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public Integer[] getPositions() {
        return positions;
    }

    public void setPositions(Integer[] positions) {
        this.positions = positions;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    public static <T> Dataset<PositionMappingDT> convert(Dataset<T> dataset) {
        return dataset.
                select(COLUMN_NAME.RECORD_ID.rawIdentifier(),
                        COLUMN_NAME.POSITION.identifier,
                        COLUMN_NAME.POSITIONS.identifier)
                .as(ENCODER);
    }

//    public static Dataset<Row> positionDistribution(Dataset<PositionMappingDT> dataset) {
//        return dataset.groupBy(ColumnIdentifier.POSITION).
//                agg(countDistinct(ColumnIdentifier.RECORD_ID).as(RECORD_COUNT_IDENTIFIER),
//                        count(ColumnIdentifier.RECORD_ID).as(ColumnIdentifier.COUNT));
//
//    }

    public static Dataset<Tuple2<Integer,Long>> positionDistribution(Dataset<PositionMappingDT> dataset) {
        return dataset.groupByKey(POSITION_KEY,Encoders.INT()).
                agg(typed.count(
                        (MapFunction<PositionMappingDT,Object>) s -> s.getRecordId()).
                        name(ColumnIdentifier.COUNT));

    }
}

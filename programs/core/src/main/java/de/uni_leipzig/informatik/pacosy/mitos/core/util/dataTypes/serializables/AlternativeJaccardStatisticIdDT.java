package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class AlternativeJaccardStatisticIdDT extends AlternativeJaccardStatisticDT {
    protected int recordId;
    public static final Encoder<AlternativeJaccardStatisticIdDT> ENCODER = Encoders.bean(AlternativeJaccardStatisticIdDT.class);

    public AlternativeJaccardStatisticIdDT() {
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}

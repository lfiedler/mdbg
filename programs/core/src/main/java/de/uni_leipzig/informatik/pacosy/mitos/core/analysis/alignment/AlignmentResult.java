package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import org.rosuda.REngine.REngine;
import org.rosuda.REngine.REngineStdOutput;

public class AlignmentResult {

    protected double eValue;
    protected int score;
    protected double bitScore;
    protected double K;
    protected double H;
    protected double lambda;
    private REngine rengine_;
    private SequenceAligner sequenceAligner;

    public AlignmentResult(SequenceAligner sequenceAligner) {
        this.sequenceAligner = sequenceAligner;
        setUpR();
    }

    private void setUpR() {
        try {
            this.rengine_ = REngine.engineForClass("org.rosuda.REngine.JRI.JRIEngine",
                    new String[] {"--vanilla"}, new REngineStdOutput(), false);
            rengine_.parseAndEval("require(RCy3);require(RColorBrewer)",null,false);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}

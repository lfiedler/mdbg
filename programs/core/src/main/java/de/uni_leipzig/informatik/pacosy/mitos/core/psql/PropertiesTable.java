package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.PropertyCountDT;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

public class PropertiesTable implements SQLTable {

    private static PropertiesTable propertiesTable;

    private static final String COLUMN_NAMES = "name TEXT NOT NULL, count bigint";

    public static final String PROTEIN = "complete.protein";

    private static final String PROTEIN_FUN = "complete.update_protein";

    public static final String TRNA = "complete.trna";

    private static final String TRNA_FUN = "complete.update_trna";

    public static final String RRNA = "complete.rrna";

    private static final String RRNA_FUN = "complete.update_rrna";

    public static final String REPLICATION_ORIGIN = "complete.\"repOrigin\"";

    private static final String REPLICATION_ORIGIN_FUN = "complete.update_rep_origin";

    public static final String OTHER_TYPE = "complete.\"otherType\"";

    private static final String OTHER_TYPE_FUN = "complete.update_other_type";

    private static final Encoder<PropertyCountDT> PROPERTY_DT_ENCODER = Encoders.bean(PropertyCountDT.class);

    private PropertiesTable() {

    }

    public static synchronized PropertiesTable getInstance() {
        if(propertiesTable == null) {
            propertiesTable = new PropertiesTable();
        }
        return propertiesTable;
    }

    /****CREATION********************************************************************************/

    public void createTable() throws SQLException {

        JDBJInterface.getInstance().createTable(PROTEIN, COLUMN_NAMES + " ,CONSTRAINT protein_primary_key PRIMARY KEY (name)", true);
        JDBJInterface.getInstance().createTable(TRNA, COLUMN_NAMES + " ,CONSTRAINT trna_primary_key PRIMARY KEY (name)", true);
        JDBJInterface.getInstance().createTable(RRNA, COLUMN_NAMES + " ,CONSTRAINT rrna_primary_key PRIMARY KEY (name)", true);
        JDBJInterface.getInstance().createTable(REPLICATION_ORIGIN, COLUMN_NAMES + " ,CONSTRAINT reporigin_primary_key PRIMARY KEY (name)", true);
        JDBJInterface.getInstance().createTable(OTHER_TYPE, COLUMN_NAMES + " ,CONSTRAINT othertype_primary_key PRIMARY KEY (name)", true);

    }

    public void dropTable() throws SQLException {
        JDBJInterface.getInstance().dropTable(PROTEIN, true);
        JDBJInterface.getInstance().dropTable(TRNA, true);
        JDBJInterface.getInstance().dropTable(RRNA, true);
        JDBJInterface.getInstance().dropTable(REPLICATION_ORIGIN, true);
        JDBJInterface.getInstance().dropTable(OTHER_TYPE, true);
    }


    private void generatePropertiesUpdateFunctions(String tablename, String functionName) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("CREATE OR REPLACE FUNCTION " + functionName + "(property TEXT)\n" +
                "RETURNS VOID AS\n" +
                "$$\n" +
                "WITH upsert AS (UPDATE " + tablename + " SET count=count+1 WHERE name=$1 RETURNING *)\n" +
                "    INSERT INTO " + tablename + "  (name, count) SELECT $1, 1 WHERE NOT EXISTS (SELECT * FROM upsert)\n" +
                "$$ LANGUAGE 'sql';");
    }

    private void dropPropertiesUpdateFunctions(String functionname) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("DROP FUNCTION IF EXISTS " + functionname + "(property TEXT)");
    }


    private void generatePropertiesUpdateFunctions() throws SQLException {
        generatePropertiesUpdateFunctions(PROTEIN, PROTEIN_FUN);
        generatePropertiesUpdateFunctions(TRNA, TRNA_FUN);
        generatePropertiesUpdateFunctions(RRNA, RRNA_FUN);
        generatePropertiesUpdateFunctions(REPLICATION_ORIGIN, REPLICATION_ORIGIN_FUN);
        generatePropertiesUpdateFunctions(OTHER_TYPE, OTHER_TYPE_FUN);
    }

    private void dropPropertiesUpdateFunctions() throws SQLException {
        dropPropertiesUpdateFunctions(PROTEIN_FUN);
        dropPropertiesUpdateFunctions(TRNA_FUN);
        dropPropertiesUpdateFunctions(RRNA_FUN);
        dropPropertiesUpdateFunctions(REPLICATION_ORIGIN_FUN);
        dropPropertiesUpdateFunctions(OTHER_TYPE_FUN);
    }

    public void removeAll() throws SQLException {
        dropTable();
        createTable();
    }

    private void remove(String property, String tablename) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("UPDATE " + tablename +
                " SET count=count-1 WHERE name='" + property + "'");
    }

    private void remove(Set<String> properties, String tablename) throws RuntimeException {
        properties.
                forEach(wrapConsumer(t -> remove(t, tablename)));
    }

    public void removeProteins(Set<String> properties) throws RuntimeException {
        remove(properties, PROTEIN);
    }

    public void removeTRNAs(Set<String> properties) throws RuntimeException {
        remove(properties, TRNA);
    }

    public  void removeRRNAs(Set<String> properties) throws RuntimeException {
        remove(properties, RRNA);
    }

    public void removeReplicationOrigins(Set<String> properties) throws RuntimeException {
        remove(properties, REPLICATION_ORIGIN);
    }

    public void removeOtherTypes(Set<String> properties) throws RuntimeException {
        remove(properties, OTHER_TYPE);
    }



    private void persist(Set<String> properties, String functionname) throws SQLException {
        try {
            properties.
                    forEach(wrapConsumer(p ->
                            JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + functionname + "('" + p + "')")));
        } catch (RuntimeException s) {
//            System.out.println(s.getMessage());
            if (s.getMessage().contains("ERROR: function " + functionname + "(unknown) does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                generatePropertiesUpdateFunctions();
                properties.stream().
                        forEach(wrapConsumer(p ->
                                JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + functionname + "('" + p + "')")));
            } else if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                properties.stream().
                        forEach(wrapConsumer(p ->
                                JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + functionname + "('" + p + "')")));
            } else {
                throw s;
            }
        }

    }

    public void persistProteins(Set<String> proteins) throws SQLException {
        persist(proteins, PROTEIN_FUN);
    }

    public void persistTRNAs(Set<String> trnas) throws SQLException {
        persist(trnas, TRNA_FUN);
    }

    public void persistRRNAs(Set<String> rrnas) throws SQLException {
        persist(rrnas, RRNA_FUN);
    }

    public void persistReplicationOrigins(Set<String> repOrigins) throws SQLException {
        persist(repOrigins, REPLICATION_ORIGIN_FUN);
    }

    public void persistOtherTypes(Set<String> otherTypes) throws SQLException {
        persist(otherTypes, OTHER_TYPE_FUN);
    }

    /****QUERIES********************************************************************************/
    private Dataset<PropertyCountDT> getPropertiesDS(String tableName)  {
        return JDBJInterface.getInstance().
                fetchQueryResult("SELECT * FROM " + tableName,PROPERTY_DT_ENCODER);
    }

    private Set<String> getProperties(String tableName)  {
        return new HashSet<>(getPropertyNamesDS(tableName).collectAsList());
    }

    public Dataset<PropertyCountDT> getProteinsDS()  {
        return getPropertiesDS(PROTEIN);
    }

    public Dataset<PropertyCountDT> getTRNAsDS() {
        return getPropertiesDS(TRNA);
    }

    public Dataset<PropertyCountDT> getRRNAsDS() {
        return getPropertiesDS(RRNA);
    }

    public Dataset<PropertyCountDT> getReplicationOriginsDS() {
        return getPropertiesDS(REPLICATION_ORIGIN);
    }

    public Dataset<PropertyCountDT> getOtherTypesDS() {
        return getPropertiesDS(OTHER_TYPE);
    }

    private Dataset<String> getPropertyNamesDS(String tableName)  {
        return JDBJInterface.getInstance().
                fetchQueryResult("SELECT name FROM " + tableName,String.class);
    }

    private Set<String> getPropertyNames(String tableName)  {
        return new HashSet<>(getPropertyNamesDS(tableName).collectAsList());
    }

    public Set<String> getProteinNames()  {
        return getPropertyNames(PROTEIN);
    }

    public Set<String> getTRNANames() {
        return getPropertyNames(TRNA);
    }

    public Set<String> getRRNANames() {
        return getPropertyNames(RRNA);
    }

    public Set<String> getReplicationOriginNames() {
        return getPropertyNames(REPLICATION_ORIGIN);
    }

    public Set<String> getOtherTypeNames() {
        return getPropertyNames(OTHER_TYPE);
    }

    public Dataset<String> getProteinNamesDS()  {
        return getPropertyNamesDS(PROTEIN);
    }

    public Dataset<String> getTRNANamesDS() {
        return getPropertyNamesDS(TRNA);
    }

    public Dataset<String> getRRNANamesDS() {
        return getPropertyNamesDS(RRNA);
    }

    public Dataset<String> getReplicationOriginsNamesDS() {
        return getPropertyNamesDS(REPLICATION_ORIGIN);
    }

    public Dataset<String> getOtherTypesNamesDS() {
        return getPropertyNamesDS(OTHER_TYPE);
    }

    /****Auxiliary********************************************************************************/
    @FunctionalInterface
    public interface CheckedFunction<T, R> {

        R apply(T t) throws Exception;

    }

    @FunctionalInterface
    public interface CheckedConsumer<T> {

        void accept(T t) throws Exception;

    }

    public static <T, R> Function<T, R> wrap(PropertiesTable.CheckedFunction<T, R> checkedFunction) throws RuntimeException {

        return t -> {

            try {

                return checkedFunction.apply(t);

            } catch (Exception e) {

                throw new RuntimeException(e);

            }

        };

    }

    public static <T> Consumer<T> wrapConsumer(PropertiesTable.CheckedConsumer<T> checkedFunction) throws RuntimeException {

        return t -> {

            try {

                checkedFunction.accept(t);

            } catch (Exception e) {

                throw new RuntimeException(e);

            }

        };

    }

}
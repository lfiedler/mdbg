package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.sequenceMap;


import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.CountInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.IdentifierInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import org.apache.spark.sql.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.apache.spark.sql.functions.*;

public class SubsequenceAnalyzer {


    static final Encoder<PositionCountIdentifierDT> POSITION_DATA_ENCODER = Encoders.bean(PositionCountIdentifierDT.class);
    static final Encoder<PositionCountDT> POSITION_DISTRIBUTION_DATA_ENCODER = Encoders.bean(PositionCountDT.class);
    static final Encoder<RangeStatisticsDT> RANGE_STATISTICS_ENCODER = Encoders.bean(RangeStatisticsDT.class);
    static final String countIdentifier = "amount";
    static final String fractionIdentifier = "fraction";

    /*** Filter ******************************************************************************************************************************/


    public static <T extends CountInterface> Dataset<T> filterPositionDistributionDataMinCount(Dataset<T> positionData, int minCount) {
        return positionData.filter(positionData.col(ColumnIdentifier.COUNT).geq(minCount));
    }
    public static <T extends CountInterface> Dataset<T> filterPositionDistributionDataMaxCount(Dataset<T> positionData, int maxCount) {
        return positionData.filter(positionData.col(ColumnIdentifier.COUNT).lt(maxCount));
    }


    public static <T extends PositionInterface> Dataset<T> filterPositionDistributionDataByRange(Dataset<T> positionData, RangeDT range) {
        Column positionColumn = positionData.col(ColumnIdentifier.POSITION);
        return positionData.filter(positionColumn.geq( range.getStart()).and(positionColumn.leq(range.getEnd())));
    }

    public static <T extends PositionInterface & CountInterface> Dataset<PositionCountDT> filterPositionDistributionDataMinLength(
            Dataset<T> positionData, Dataset<RangeDT> rangeData, int minLength) {
        return positionData.
                select(
                        ColumnIdentifier.POSITION,
                        ColumnIdentifier.COUNT).as(POSITION_DISTRIBUTION_DATA_ENCODER).
                except(
                createRangePositionDistributionDataJoin(
                        positionData,
                        // get all ranges with a length smaller than minLength
                        filterRangeDataMaxLength(rangeData,minLength)).
                        select(
                                ColumnIdentifier.POSITION,
                                ColumnIdentifier.COUNT).as(POSITION_DISTRIBUTION_DATA_ENCODER)).
                orderBy(ColumnIdentifier.POSITION);
    }

    public static <T extends IdentifierInterface> Dataset<T> filterPositionDistributionDataByIdentifier(Dataset<T> positionData, String filterProperty) {
        return positionData.filter(col(ColumnIdentifier.IDENTIFIER).equalTo(filterProperty));
    }

    public static <T extends RangeDT> Dataset<T> filterRangeDataMinLength(Dataset<T> rangeData, int minLength) {
        return rangeData.filter(getRangeLength(rangeData).geq(minLength));
    }

    public static <T extends RangeDT> Dataset<T> filterRangeDataMaxLength(Dataset<T> rangeData, int maxLength) {
        return rangeData.filter(getRangeLength(rangeData).lt(maxLength));
    }

    /*** Dataset creation ******************************************************************************************************************************/


//    public static <T extends PositionDistributionDT> Dataset<RangeDT> createRanges(Dataset<T> positionData) {
//
//        return createRangesForPositions(
//                positionData.
//                        select(positionData.col(PositionDistributionDT.COLUMN_NAME.POSITION.identifier)).
//                        as(Encoders.INT()));
//
//    }

    public static <T extends PositionInterface> Dataset<RangeDT> createRanges(Dataset<T> positionData) {

        return createRangesForPositions(
                positionData.
                        select(positionData.col(ColumnIdentifier.POSITION)).
                        as(Encoders.INT()));

    }

    public static Dataset<RangeDT> createRangesForPositions(Dataset<Integer> positions) {

        // get iterator over position Rows
        Iterator<Integer> it = positions.toLocalIterator();

        List<RangeDT> rangeRows = new ArrayList<>();
        Integer prevPos = null;

        // add first element to first range

        Integer pos = it.next();

        RangeDT range = new RangeDT();
        range.setStart(pos);

        while(it.hasNext()) {
            prevPos = pos;
            pos = it.next();


            if( pos != prevPos + 1) {
                range.setEnd(prevPos);
                range.setLength(range.getEnd()-range.getStart()+1);

                rangeRows.add(range);
                range = new RangeDT();
                range.setStart(pos);
            }
        }
        range.setEnd(pos);
        range.setLength(range.getEnd()-range.getStart()+1);
        rangeRows.add(range);
        Dataset<RangeDT> rangeDataset = Spark.getInstance()
                .createDataset(rangeRows,Encoders.bean(RangeDT.class));

//        showAll(rangeDataset);
        return rangeDataset;

    }

    public static <T extends PositionInterface> Dataset<Row> createRangePositionDistributionDataJoin(Dataset<T> positionData, Dataset<RangeDT> rangeData) {
        Column positionColumn = positionData.col(ColumnIdentifier.POSITION);
        Column startColumn = rangeData.col(ColumnIdentifier.START);
        Column endColumn = rangeData.col(ColumnIdentifier.END);

        return rangeData.join(positionData,
                positionColumn.geq(startColumn).and(positionColumn.leq(endColumn)));
    }


    public static Dataset<RangeStatisticsDT> createRangeStatistics(Dataset<PositionCountDT> positionData, Dataset<RangeDT> rangeData, int scale) {

        Column countColumn = positionData.col(ColumnIdentifier.COUNT);
        Column startColumn = rangeData.col(ColumnIdentifier.START);
        Column lengthColumn = rangeData.col(ColumnIdentifier.LENGTH);
        Column endColumn = rangeData.col(ColumnIdentifier.END);


        return createRangePositionDistributionDataJoin(positionData,rangeData).
                groupBy(startColumn,endColumn,lengthColumn).
                agg(//endColumn.minus(startColumn).plus(1).as(RangeStatistics.COLUMN_NAME.LENGTH.identifier()),
                        round(avg(countColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier),
                        max(countColumn).as(RangeStatisticsDT.COLUMN_NAME.MAXCOUNT.identifier),
                        min(countColumn).as(RangeStatisticsDT.COLUMN_NAME.MINCOUNT.identifier),
                        round(kurtosis(countColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.KURTOSIS.identifier),
                        round(skewness(countColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.SKEWNESS.identifier),
                        round(stddev_samp(countColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.STD_DEVIATION.identifier),
                        round(variance(countColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.VARIANCE.identifier)
                ).
                orderBy(startColumn,endColumn).as(RANGE_STATISTICS_ENCODER);
    }

    public static Dataset<Row> createStatisticsForRangeStatistics(Dataset<RangeStatisticsDT> rangeStatistics, String column_name, int scale) {
        Column attributeColumn = rangeStatistics.col(column_name);
        return rangeStatistics.groupBy().
                    agg(
                            round(avg(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier),
                            max(attributeColumn).as(RangeStatisticsDT.COLUMN_NAME.MAXCOUNT.identifier),
                            min(attributeColumn).as(RangeStatisticsDT.COLUMN_NAME.MINCOUNT.identifier),
                            round(kurtosis(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.KURTOSIS.identifier),
                            round(skewness(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.SKEWNESS.identifier),
                            round(stddev_samp(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.STD_DEVIATION.identifier),
                            round(variance(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.VARIANCE.identifier)
                    );
    }

    public static Dataset<Row> createStatisticsForPositionnDistributionData(Dataset<PositionCountDT> positionData, String column_name, int scale) {
        Column attributeColumn = positionData.col(column_name);
        return positionData.groupBy().
                agg(
                        round(avg(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier),
                        max(attributeColumn).as(RangeStatisticsDT.COLUMN_NAME.MAXCOUNT.identifier),
                        min(attributeColumn).as(RangeStatisticsDT.COLUMN_NAME.MINCOUNT.identifier),
                        round(kurtosis(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.KURTOSIS.identifier),
                        round(skewness(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.SKEWNESS.identifier),
                        round(stddev_samp(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.STD_DEVIATION.identifier),
                        round(variance(attributeColumn),scale).as(RangeStatisticsDT.COLUMN_NAME.VARIANCE.identifier)
                );
    }


    public static <T extends RangeDT> Dataset<Row> createLengthCountDistribution(Dataset<T> rangeStatistics) {
        Column lengthColumn = rangeStatistics.col(ColumnIdentifier.LENGTH);

        return rangeStatistics.groupBy(lengthColumn).count().as(countIdentifier).orderBy(lengthColumn);
    }

    public static Dataset<Row> createMeanRecordCountCountDistribution(Dataset<RangeStatisticsDT> rangeStatistics, int scale) {
//        Column MEANCOUNTRecordCount = round(rangeStatistics.col(RANGE_RangeStatisticsDT.COLUMN_NAME.MEANCOUNT_RECORD_COUNT.identifier),scale);

        return rangeStatistics.groupBy(
                round(rangeStatistics.col(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier),scale).
                        as(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier)).
                count().as(countIdentifier).orderBy(RangeStatisticsDT.COLUMN_NAME.MEANCOUNT.identifier);
    }

    public static <T extends PositionCountDT> Dataset<Row> createRecordCountCountDistribution(Dataset<T> positionData) {
        Column recordCount = positionData.col(ColumnIdentifier.COUNT);

        return positionData.groupBy(recordCount).count().as(countIdentifier).orderBy(recordCount);
    }

    public static Dataset<RangeStatisticsDT> orderRangeStatistics(Dataset<RangeStatisticsDT> rangeStatistics,
                                                           String primary_name,
                                                           String [] otherNames) {
        return rangeStatistics.orderBy(primary_name,otherNames).as(RANGE_STATISTICS_ENCODER);
    }


    public static <T extends PositionCountIdentifierDT>  Dataset<PositionCountIdentifierDT> createFullPropertyPositionData(Dataset<T> propertyPositionData, Dataset<T> recordCountPositionData) {
        return  propertyPositionData.withColumnRenamed(ColumnIdentifier.POSITION,"positionJoin").
                join(recordCountPositionData,
                        col(ColumnIdentifier.POSITION).equalTo(col("positionJoin")),
                        "right").
                select(
                        ColumnIdentifier.POSITION,
                        ColumnIdentifier.IDENTIFIER,
                        ColumnIdentifier.COUNT).
                na().fill("",new String [] {ColumnIdentifier.IDENTIFIER}).
                na().fill(0,new String [] {ColumnIdentifier.COUNT}).
                orderBy(ColumnIdentifier.POSITION).as(POSITION_DATA_ENCODER);
    }

    public static <T extends PositionCountDT> Dataset<PositionCountIdentifierDT> createFullPropertyPositionData(Dataset<T> propertyPositionData, Dataset<T> recordCountPositionData, String filterProperty) {
        Column identifierColumn = propertyPositionData.col(ColumnIdentifier.IDENTIFIER);
        Column countColumn = propertyPositionData.col(ColumnIdentifier.COUNT);
        Column positionColumn = propertyPositionData.col(ColumnIdentifier.POSITION);
        return  propertyPositionData.
                filter(identifierColumn.equalTo(filterProperty)).
                join(recordCountPositionData,
                        recordCountPositionData.col(ColumnIdentifier.POSITION).
                                equalTo(positionColumn),
                        "right").
                select(
                        recordCountPositionData.col(ColumnIdentifier.POSITION),
                        identifierColumn,
                        countColumn).
                na().fill("",new String [] {ColumnIdentifier.IDENTIFIER}).
                na().fill(0,new String [] {ColumnIdentifier.COUNT}).as(POSITION_DATA_ENCODER);
    }

    public static Dataset<PositionCountDT> convertToPositionCountDT(Dataset<PositionCountIdentifierDT> positionData) {
        return positionData.select(
                ColumnIdentifier.POSITION,
                ColumnIdentifier.COUNT).
                orderBy(ColumnIdentifier.POSITION).
                as(POSITION_DISTRIBUTION_DATA_ENCODER);
    }


    /*** Dataset extraction/ views ******************************************************************************************************************************/
    public static <T extends PositionCountDT>  Dataset<Integer> getRecordCountToRange(Dataset<T> positionData, RangeDT range) {
        return filterPositionDistributionDataByRange(positionData, range).
                select(positionData.col(ColumnIdentifier.COUNT)).
                as(Encoders.INT());
    }

    public static Dataset<String> getEdgeIdsToRange(Dataset<PositionCountIdentifierDT> positionData, RangeDT range) {
        return filterPositionDistributionDataByRange(
                positionData,range).
                select(positionData.col(ColumnIdentifier.IDENTIFIER)).
                as(Encoders.STRING());
    }

    public static List<String> getEdgeIdsCollectionToRange(Dataset<PositionCountIdentifierDT> positionData, RangeDT range) {
        return getEdgeIdsToRange(positionData,range).collectAsList();
    }

    public static <T extends PositionInterface> Dataset<Integer> getPositionsToRange(Dataset<T> positionData, RangeDT range) {
        return filterPositionDistributionDataByRange(positionData,range).
                select(positionData.col(ColumnIdentifier.POSITION)).
                as(Encoders.INT());
    }

    public static <T extends IdentifierInterface> List<String> getPropertyNames(Dataset<T> propertyPositionData) {
        return propertyPositionData.select(ColumnIdentifier.IDENTIFIER).
                distinct().as(Encoders.STRING()).collectAsList();
    }


    /*** Columns ******************************************************************************************************************************/
    public static <T extends RangeDT> Column getRangeLength(Dataset<T> rangeData) {
        return rangeData.col(ColumnIdentifier.END).
                minus(rangeData.col(ColumnIdentifier.START)).plus(1);
    }

}

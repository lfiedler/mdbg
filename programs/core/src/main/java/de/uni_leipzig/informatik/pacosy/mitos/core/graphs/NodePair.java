package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import java.io.Serializable;
import java.util.Map;

public class NodePair implements Serializable {
    private long v1;
    private long v2;

    public enum NODEPAIR {
        V1("v1"),
        V2("v2");

        public final String identifier;

        NODEPAIR(String identifier) {
            this.identifier = identifier;
        }
    }

    public NodePair(long v1, long v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public NodePair(Map<String,Object> nodeMap) {
        this.v1 = (long)nodeMap.get("v1");
        this.v2 = (long)nodeMap.get("v2");
    }

    public NodePair(String nodePairString, String seperator) {
        String [] nodes = nodePairString.split(seperator);
        this.v1  = Long.parseLong(nodes[0]);
        this.v2  = Long.parseLong(nodes[1]);
    }

    public long getV1() {
        return v1;
    }

    public long getV2() {
        return v2;
    }

    public String toString(){
        return v1 + "->" + v2;
    }


}

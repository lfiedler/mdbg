package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;


import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RangeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.IntBinaryOperator;

public class RecordAlignment {

    private int recordId;
    private int length;
    private boolean topology;

    private Map<Integer, List<Integer>> diagonals;
    private Map<Integer,List<RangeDT>> diagonalRanges;

    private IntBinaryOperator diagonalComputer;
    private final BiPredicate<Integer,Integer> isCyclicSuccessorPredicate;
    private final BiPredicate<Integer,Integer> isLinearSuccessorPredicate;


    public RecordAlignment(int recordId, int length, boolean topology, Map<Integer, List<Integer>> positionMap) {
        this.recordId = recordId;
        this.length = length;
        this.topology = topology;
        this.diagonals = new HashMap<Integer, List<Integer>>();
        this.diagonalRanges = new HashMap<>();
        isCyclicSuccessorPredicate = (p1, p2) -> (p1-p2+1+length)%length == 0;
        isLinearSuccessorPredicate = (p1, p2) -> p1-p2+1 == 0;
        if(topology) {
            diagonalComputer = (s1,s2) -> (((s1-s2)%length)+length)%length;

        }
        else {
            diagonalComputer = (s1,s2) -> s1-s2;

        }
        computeDiagonals(positionMap);
    }

    private void computeDiagonals(Map<Integer, List<Integer>> positionMap) {
        for(Map.Entry<Integer,List<Integer>> positionEntry : positionMap.entrySet()) {
            for(Integer position: positionEntry.getValue()) {
                //(a mod b) == ((a%b)+b)%b -> get diagonal
                int diagonalValue = diagonalComputer.applyAsInt(position,positionEntry.getKey());
                List<Integer> diagonalPositions = diagonals.get(diagonalValue);
                if(diagonalPositions == null) {
                    diagonalPositions = new ArrayList<>();
                }
                // add record position to this diagonal
                diagonalPositions.add(position);
                diagonals.put(diagonalValue,diagonalPositions);

            }
        }

        for(Map.Entry<Integer, List<Integer>> diagonal: diagonals.entrySet()) {
            diagonalRanges.put(diagonal.getKey(),getRanges(diagonal.getValue()));
        }
    }




    public  List<RangeDT> getRanges(List<Integer> positions) {

        if(positions.size() == 0) {
            return new ArrayList<>();
        }

        Collections.sort(positions);
        Iterator<Integer> it = positions.iterator();
        Integer prevPos = null;
        Integer pos = null;

        List<RangeDT> rangeList = new ArrayList<>();

        // add first element to first range
        pos = it.next();
        RangeDT range = new RangeDT();
        range.setStart(pos);

        while(it.hasNext()) {
            prevPos = pos;
            pos = it.next();

            if( !isLinearSuccessorPredicate.test(prevPos,pos)) {
                range.setEnd(prevPos);
                range.setLength(range.getEnd()-range.getStart()+1);
                rangeList.add(range);
                range = new RangeDT();
                range.setStart(pos);
            }
        }
        range.setEnd(pos);
        range.setLength(range.getEnd()-range.getStart()+1);
        rangeList.add(range);

        // if cyclic genome, join start and end range if they are cohesive
        if(topology && isCyclicSuccessorPredicate.test(positions.get(positions.size()-1),positions.get(0))) {
            RangeDT joinedRange = new RangeDT();
            joinedRange.setStart(rangeList.get(rangeList.size()-1).getEnd());
            joinedRange.setEnd(rangeList.get(0).getStart());
            joinedRange.setLength(rangeList.get(0).getLength()+rangeList.get(rangeList.size()-1).getLength());
            rangeList.remove(0);
            rangeList.remove(rangeList.size()-1);
            rangeList.add(joinedRange);
        }
        return rangeList;

    }

    public void getRangeDistribution() {
        for(Map.Entry<Integer,List<RangeDT>> diagonal: diagonalRanges.entrySet()) {

            System.out.println(diagonal.getKey());
            System.out.println(diagonal.getValue().get(0).getStart() + ":" +
                    diagonal.getValue().get(diagonal.getValue().size()-1).getEnd());
            System.out.print(diagonal.getValue().get(0).getLength() + "*");
            for(int r = 1; r < diagonal.getValue().size(); r++) {
                System.out.print(
                        RangeDT.rangeDistance(
                                diagonal.getValue().get(r-1),
                                diagonal.getValue().get(r)) + "-");
                System.out.print(diagonal.getValue().get(r).getLength() + "*");
            }
            System.out.println();
        }
    }

    public void getScores(int alignCost, int gapCost) {

    }


}

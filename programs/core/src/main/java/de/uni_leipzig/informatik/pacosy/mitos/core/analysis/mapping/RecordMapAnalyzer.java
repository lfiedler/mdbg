package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.KmersTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.commons.math3.optim.nonlinear.vector.Weight;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Scope;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Column;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;

import static org.apache.spark.sql.functions.*;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.count;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.values;

public class RecordMapAnalyzer {

    private GraphTraversalSource plusG;
    private GraphTraversalSource minusG;
    private int recordId;
    private static final String GROUP_COLUMN_NAME = "grouped";
    private static final String PLUS_RECORD_EDGE_WEIGHT_TABLE = "plusRecordEdgeWeightTable";
    private static final String MINUS_RECORD_EDGE_WEIGHT_TABLE = "minusRecordEdgeWeightTable";

    protected static final Logger LOGGER = LoggerFactory.getLogger(RecordMapCreator.class);

    public static enum PARQUET_NAME {
        PLUS_RECORD_EDGE_WEIGHT_STATISTIC,
        MINUS_RECORD_EDGE_WEIGHT_STATISTIC
    }

    public RecordMapAnalyzer(int recordId) {
        this.recordId = recordId;
//        initializeGraphTraversalSources();
    }

    public static String getRecordEdgeWeightStatisticsPath(boolean strand) {
        String parquetPath = ProjectDirectoryManager.getRECORD_DATA_DIR() + "/";
        if(strand) {
            parquetPath += PARQUET_NAME.PLUS_RECORD_EDGE_WEIGHT_STATISTIC;
        }
        else {
            parquetPath += PARQUET_NAME.MINUS_RECORD_EDGE_WEIGHT_STATISTIC;
        }
        return parquetPath;
    }

    public static Dataset<Row> fetchRecordEdgeWeightStatistics(boolean strand) {
            return SparkComputer.read(getRecordEdgeWeightStatisticsPath(strand));
    }

    public static void persistRecordEdgeWeightStatistics(boolean strand) {

//        List<StructField> structFields = new ArrayList<>();
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.RECORD_ID,DataTypes.IntegerType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_MEAN,DataTypes.DoubleType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_STD_DEVIATION,DataTypes.DoubleType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_SKEWNESS,DataTypes.DoubleType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_KURTOSIS,DataTypes.DoubleType,false));
//        structFields.add(DataTypes.createStructField(ColumnIdentifier.WEIGHT_MAX,DataTypes.LongType,false));

        String parquetPath = getRecordEdgeWeightStatisticsPath(strand);
        final String totalIdentifier = "total";
        final String hitIdentifier = "hit";
        KmersTable kmersTable = KmersTable.getInstance();
        kmersTable.setK(DbgGraph.K);
        Dataset<RecordWeightCountDT> recordWeights = kmersTable.fetchRecordEdgeWeightDistribution(strand).cache();
        Dataset<Row> recordWeightStatistics = recordWeights.
//                filter(col(ColumnIdentifier.RECORD_ID).equalTo(834)).
                    withColumn(hitIdentifier,
                            explode(array_repeat(col(ColumnIdentifier.WEIGHT),col(ColumnIdentifier.COUNT).
                            cast(DataTypes.IntegerType)))).
                groupBy(ColumnIdentifier.RECORD_ID).
                agg(
                        round(mean(col(hitIdentifier)),2).as(ColumnIdentifier.WEIGHT_MEAN),
                        round(stddev_samp(col(hitIdentifier)),2).as(ColumnIdentifier.WEIGHT_STD_DEVIATION),
                        round(skewness(col(hitIdentifier)),2).as(ColumnIdentifier.WEIGHT_SKEWNESS),
                        round(kurtosis(col(hitIdentifier)),2).as(ColumnIdentifier.WEIGHT_KURTOSIS),
                        min(col(hitIdentifier)).as(ColumnIdentifier.WEIGHT_MIN),
                        max(col(hitIdentifier)).as(ColumnIdentifier.WEIGHT_MAX));
        SparkComputer.persistDataFrame(recordWeightStatistics,parquetPath);

    }

    private void initializeGraphTraversalSources() {
        plusG = Graph.readGraphML(ProjectDirectoryManager.getRECORD_DATA_DIR(true,recordId) + "/" +
                RecordMapCreator.RECORD_SUBGRAPH_NAME);
        minusG = Graph.readGraphML(ProjectDirectoryManager.getRECORD_DATA_DIR(false,recordId) + "/" +
                RecordMapCreator.RECORD_SUBGRAPH_NAME);
    }


    public Map<Long, List<List<RangeDT>>>  getRepeatRegions(GraphTraversalSource g) {
        Map<Long, List<List<RangeDT>>> repeatRegions = new TreeMap<>();
        //        // get distribution of edge weights for all kPlusOneMers to which this record is mapped
        TreeMap<Object, Long> recordCountDistribution = new TreeMap<>((Map<Object, Long>) g.E().
                hasLabel(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).
                groupCount().by(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).next());
        System.out.println("Record count distribution");
        Auxiliary.printMap(recordCountDistribution);

        // get all repeat regions of this record
        Map<Object, Object> repeatRegionMap = g.E().has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).
                has(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier,true).
                group().
                by(__.union(__.outV(),__.inV()).fold()).
                by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier).dedup().order().fold()).
                unfold().
                filter(__.select(Column.values).count(Scope.local).is(P.gt(1))).
                select(Column.values).
                group().
                by(count(Scope.local)).
                by(__.order().by(__.unfold().limit(1)).fold()).next();
         //key is amount of repeats for a repeat region, value is a list of rangelists. Each rangelist contains the ranges that contribute to a repeat region.
         //An entry =3,[ [[1,2], [9,10], [300,301]],[[5,7], [19,21], [305,307]] ]
         //for instance means, that there are two regions which are each repeated three times in the sequence of the record.
         //one region of length 2 with ranges: [1,2], [9,10], [300,301]
         //one region of length 3 with ranges: [5,7], [19,21], [305,307]
        if(repeatRegionMap.size() > 0) {

            for(Map.Entry<Object,Object> entry: repeatRegionMap.entrySet()) {
                List<List<RangeDT>> ranges = RangeDT.createMappedMultiRanges((List<List<Integer>>) entry.getValue());
                repeatRegions.put((Long)entry.getKey(),ranges);
            }

            System.out.println("Repeat regions:");
            Auxiliary.printMap(repeatRegions);
        }
        return repeatRegions;
    }

    public static String[] getUnAnnotatedPropertyColumnNames(Dataset<Row> dataset) {
        return getColumnNames(dataset, s ->
                ! s.equals(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier) &&
                        ! s.equals(ColumnIdentifier.POSITION) &&
                        ! s.endsWith(RecordMapCreator.ANNOTATED_IDENTIFIER));
    }

    public static String[] getAnnotatedPropertyColumnNames(Dataset<Row> dataset) {
        return getColumnNames(dataset, s ->
                 s.endsWith(RecordMapCreator.ANNOTATED_IDENTIFIER));
    }

    public static String[] getAllPropertyColumnNames(Dataset<Row> dataset) {
        return getColumnNames(dataset, s -> ! s.equals(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier) &&
                ! s.equals(ColumnIdentifier.POSITION));
    }

    public static String[] getNonPropertyColumnNames(Dataset<Row> dataset) {
        final List<String> properties = new ArrayList<>(Arrays.asList(getUnAnnotatedPropertyColumnNames(dataset)));
        properties.addAll(Arrays.asList(getAnnotatedPropertyColumnNames(dataset)));
        return getColumnNames(dataset,s -> !properties.contains(s));
    }
    // TODO: refactor arrays to list to remove length
    private static String[] getColumnNames(Dataset<Row> dataset, Function<String,Boolean> filterCriterion) {
        String [] columnNames = dataset.columns();
        List<String> resultNames = new ArrayList<>();

        for(int i = 0; i < columnNames.length; i++) {
            if(filterCriterion.apply(columnNames[i])) {
                resultNames.add(columnNames[i]);
            }
        }

        return resultNames.toArray(new String[resultNames.size()]);
    }



//    private static String[] getColumnNames(Dataset<Row> dataset, Function<String,Boolean> filterCriterion, int resultSize) {
//        String [] columnNames = dataset.columns();
//        String [] resultNames = new String[resultSize];
//        int counter = 0;
//        for(int i = 0; i < columnNames.length; i++) {
//            if(filterCriterion.apply(columnNames[i])) {
//                resultNames[counter] =columnNames[i];
//                counter++;
//            }
//        }
//        System.out.println(counter +" "+ resultSize);
//        return resultNames;
//    }

//    public static Dataset<Row> getPropertyStatistic(Dataset<Row> joinedSet) {
//        joinedSet.withColumn("max_annotated",lit())
//                select(col(ColumnIdentifier.POSITION),
//                col(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier),
//                greatest(gt)).
//
//    }

    public static org.apache.spark.sql.Column getAnnotatedPropertyArrays(Dataset<Row> dataset) {

        return getPropertyArrays(dataset,getAnnotatedPropertyColumnNames(dataset));
    }

    public static org.apache.spark.sql.Column getUnAnnotatedPropertyArrays(Dataset<Row> dataset) {

        return getPropertyArrays(dataset,getUnAnnotatedPropertyColumnNames(dataset));
    }

    public static org.apache.spark.sql.Column getPropertyArrays(Dataset<Row> dataset, String [] propertyColumnNames) {
//        String [] properyColumnNames = getUnAnnotatedPropertyColumnNames(dataset);
        org.apache.spark.sql.Column[] columns = new org.apache.spark.sql.Column[propertyColumnNames.length];
        for(int i = 0; i < columns.length; i++) {
            columns[i] = array(lit(propertyColumnNames[i]),col(propertyColumnNames[i]));

        }
        return array(columns);
    }

    public static Dataset<Row> getJoinedPropertySets(List<Dataset<Row>> datasets) {
        return getJoinedPropertySets(datasets.toArray(new Dataset[datasets.size()]));
    }

    public static Dataset<Row> getJoinedPropertySets(Dataset<Row>... datasets) {
        Dataset<Row> joinedSet = datasets[0];
        for(int i = 1; i < datasets.length; i++) {
            joinedSet = joinedSet.join(
                    datasets[i].select(ColumnIdentifier.POSITION,getAllPropertyColumnNames(datasets[i])).
                            withColumnRenamed(ColumnIdentifier.POSITION,"p_" + i),
                    col(ColumnIdentifier.POSITION).equalTo(col("p_"+i)),
                    "inner"
            ).drop("p_"+i);
        }
        return joinedSet;
    }

    public Dataset<Row> fetchPropertySet(boolean strand, DbgGraph.DBG_EDGE_ATTRIBUTES attribute) {
        String directory = ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/" + attribute;
        if(new File(directory).exists()) {
            return Spark.getInstance().read().parquet(directory).orderBy(ColumnIdentifier.POSITION);
        }
        return null;
    }

    public static org.apache.spark.sql.Column nonZeroColumns(String[] columnNames) {
        if(columnNames.length == 0) {
            return null;
        }
        org.apache.spark.sql.Column column = col(columnNames[0]).gt(0);
        for(int i = 1; i < columnNames.length; i++) {
            column = column.or(col(columnNames[i]).gt(0));
        }
        return column;
    }

    public static Dataset<Row> getRankedPropertySets(Dataset<Row> joinedDataset) {

        WindowSpec windowSpec = Window.partitionBy(ColumnIdentifier.POSITION).orderBy(col(ColumnIdentifier.COUNT).desc());
        return joinedDataset.withColumn(GROUP_COLUMN_NAME,explode(getPropertyArrays(joinedDataset,getAllPropertyColumnNames(joinedDataset)))).
                select(SparkComputer.getJoinedColumns(
                        SparkComputer.getColumns(getNonPropertyColumnNames(joinedDataset)),//col(ColumnIdentifier.POSITION), col(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier),
                        col(GROUP_COLUMN_NAME).getItem(0).as(ColumnIdentifier.NAME),
                        col(GROUP_COLUMN_NAME).getItem(1).as(ColumnIdentifier.COUNT))).
                filter(col(ColumnIdentifier.COUNT).gt(0)).
                withColumn(ColumnIdentifier.RANK,
                        rank().over(windowSpec)).orderBy(ColumnIdentifier.POSITION,ColumnIdentifier.RANK)
                ;
    }

    public Dataset<Row> getTyCount(Dataset<Row> rankedDataset) {
//        rankedDataset.show();
//        rankedDataset.groupBy(ColumnIdentifier.POSITION, DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).
//                agg(functions.count(col(ColumnIdentifier.RANK)).as("tyCount")).
//                filter(col("tyCount").gt(1)).show();
        return rankedDataset.groupBy(ColumnIdentifier.POSITION, DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).
                agg(functions.count(col(ColumnIdentifier.RANK)).as("tyCount"), collect_list(ColumnIdentifier.COUNT)).
                filter(col("tyCount").gt(1));
    }

    public void analyzePropertySets() {
        List<Dataset<Row>> propertyDatasets = new ArrayList<>();
        Dataset<Row> proteins = fetchPropertySet(true, DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN);
        if(proteins != null) {
            propertyDatasets.add(proteins);
        }
        Dataset<Row> rrnas = fetchPropertySet(true, DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA);
        if(rrnas != null) {
            propertyDatasets.add(rrnas);
        }
        Dataset<Row> reps = fetchPropertySet(true, DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN);
        if(reps != null) {
            propertyDatasets.add(reps);
        }
        Dataset<Row> trnas = fetchPropertySet(true, DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA);
        if(trnas != null) {
            propertyDatasets.add(trnas);
        }
        Dataset<Row> others = fetchPropertySet(true, DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER);
        if(others != null) {
            propertyDatasets.add(others);
        }
        // join all propertysets
        Dataset<Row> joined = getJoinedPropertySets(propertyDatasets);

        // extract record and positions for later
        Dataset<Row> recordCounts = proteins.select(ColumnIdentifier.POSITION,
                DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier);
        // rank unAnnotated properties
        Dataset<Row> unAnnotatedRanks = getRankedPropertySets(joined.select(
                ColumnIdentifier.POSITION,getUnAnnotatedPropertyColumnNames(joined)
        ).filter(nonZeroColumns(getUnAnnotatedPropertyColumnNames(joined))));

        unAnnotatedRanks.show(100);

        Dataset<Row> tyCount = getTyCount(recordCounts.join(unAnnotatedRanks.withColumnRenamed(ColumnIdentifier.POSITION,"u_p"),
                col(ColumnIdentifier.POSITION).equalTo(col("u_p")),"inner").
                drop("a_p","u_p").
                na().fill(0));
        SparkComputer.showAll(tyCount);
        // rank annotated properties
//        Dataset<Row> annotatedRanks = getRankedPropertySets(joined.select(ColumnIdentifier.POSITION,getAnnotatedPropertyColumnNames(joined)).
//                filter(nonZeroColumns(getAnnotatedPropertyColumnNames(joined)))).
//                withColumnRenamed(ColumnIdentifier.NAME+ RecordMapCreator.ANNOTATED_IDENTIFIER,ColumnIdentifier.NAME).
//                withColumn(ColumnIdentifier.NAME+ RecordMapCreator.ANNOTATED_IDENTIFIER,
//                        regexp_replace(col(ColumnIdentifier.NAME),RecordMapCreator.ANNOTATED_IDENTIFIER,"")).drop(ColumnIdentifier.NAME);
//
//        annotatedRanks.show(100);
//        Dataset<Row> combined = recordCounts.join(
//                annotatedRanks.
//                        withColumnRenamed(ColumnIdentifier.NAME,ColumnIdentifier.NAME +  RecordMapCreator.ANNOTATED_IDENTIFIER).
//                        withColumnRenamed(ColumnIdentifier.COUNT,ColumnIdentifier.COUNT + RecordMapCreator.ANNOTATED_IDENTIFIER).
//                        withColumnRenamed(ColumnIdentifier.RANK,ColumnIdentifier.RANK + RecordMapCreator.ANNOTATED_IDENTIFIER).
//                        withColumnRenamed(ColumnIdentifier.POSITION,"a_p"),
//                col(ColumnIdentifier.POSITION).equalTo(col("a_p")),"left_outer"
//        ).join(unAnnotatedRanks.withColumnRenamed(ColumnIdentifier.POSITION,"u_p"),
//                col(ColumnIdentifier.POSITION).equalTo(col("u_p")),"left_outer").
//                drop("a_p","u_p").
//                na().fill(0).
//                orderBy(ColumnIdentifier.POSITION);
//
//        combined.show(1000);


    }

}

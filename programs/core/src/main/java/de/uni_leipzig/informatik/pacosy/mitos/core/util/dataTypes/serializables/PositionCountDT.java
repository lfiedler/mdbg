package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.CountInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class PositionCountDT implements Serializable, PositionInterface, CountInterface {
    protected int position;
    protected int count;
    public static final Encoder<PositionCountDT> ENCODER = Encoders.bean(PositionCountDT.class);

    public enum COLUMN_NAME implements DBColumnName {

        COUNT("count"),
        POSITION("position");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public PositionCountDT(int position, int count) {
        this.position = position;
        this.count = count;
    }

    public PositionCountDT() {

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public String toString() {
        return position + ": " + count;
    }


}

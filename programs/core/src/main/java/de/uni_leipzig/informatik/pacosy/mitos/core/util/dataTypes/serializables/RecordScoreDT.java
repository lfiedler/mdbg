package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class RecordScoreDT implements RecordIdInterface {
    protected int recordId;
    protected double eValue;
    public static final Encoder<RecordScoreDT> ENCODER = Encoders.bean(RecordScoreDT.class);
    public RecordScoreDT() {
    }

    public RecordScoreDT(int recordId, double eValue) {
        this.recordId = recordId;
        this.eValue = eValue;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public double geteValue() {
        return eValue;
    }

    public void seteValue(double eValue) {
        this.eValue = eValue;
    }
}

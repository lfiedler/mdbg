package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;
import java.util.Objects;

public class RecordTaxidMappingDT implements Serializable, RecordIdInterface {

    protected int recordId;
    protected int taxid;
    public static final Encoder<RecordTaxidMappingDT> ENCODER = Encoders.bean(RecordTaxidMappingDT.class);

    public RecordTaxidMappingDT() {

    }

    public int getTaxid() {
        return taxid;
    }

    public void setTaxid(int taxid) {
        this.taxid = taxid;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof RecordTaxidMappingDT)) {
            return false;
        }
        RecordTaxidMappingDT that = (RecordTaxidMappingDT) o;
        return getRecordId() == that.getRecordId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRecordId());
    }
}

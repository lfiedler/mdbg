package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import com.univocity.parsers.common.record.Record;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RangeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordTaxidMappingDT;
import org.apache.commons.math3.util.Pair;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.catalyst.parser.SqlBaseBaseListener;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class RecordTable implements SQLTable{


    private static RecordTable recordTable;
    private static final Encoder<RecordDT> RECORD_DT_ENCODER = Encoders.bean(RecordDT.class);
    private static final Encoder<RecordTaxidMappingDT> RECORD_TAXID_MAPPING_DT_ENCODER = Encoders.bean(RecordTaxidMappingDT.class);
    private static final String RECORD_UPDATE_FUN = SCHEMA_NAME+ ".update_record_taxid";
    private static final String RECORD_TABLENAME = SCHEMA_NAME+ ".record";
    private static Dataset<RecordDT> tableEntries;


    private RecordTable() {

    }

    public static synchronized RecordTable getInstance() {
        if(recordTable == null) {
            recordTable = new RecordTable();
        }
        return recordTable;
    }

    /****CREATION********************************************************************************/
    public void createTable()  throws SQLException {
        JDBJInterface.getInstance().createTable(""+RECORD_TABLENAME+"",
                "  "+ RecordDT.COLUMN_NAME.RECORDID.identifier+" integer NOT NULL,\n" +
                        "  "+ RecordDT.COLUMN_NAME.NAME.identifier+" TEXT,\n" +
                        "  "+ RecordDT.COLUMN_NAME.VERSION.identifier+" smallint,\n" +
                        "  "+ RecordDT.COLUMN_NAME.TAXID.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.LENGTH+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.TOPOLOGY.identifier+" boolean,\n" +
                        "  "+ RecordDT.COLUMN_NAME.A_CONTENT.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.C_CONTENT.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.G_CONTENT.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.T_CONTENT.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.AMBIG_COUNT.identifier+" integer,\n" +
                        "  "+ RecordDT.COLUMN_NAME.MITOS_FLAG.identifier+" boolean,\n" +
                        "  "+ RecordDT.COLUMN_NAME.VISITED.identifier+" boolean,\n" +
                        "  CONSTRAINT record_primary_key PRIMARY KEY ("+ RecordDT.COLUMN_NAME.RECORDID.identifier+")" ,true);
    }


    public void dropTable() throws  SQLException {
        JDBJInterface.getInstance().dropTable("compete.record",true);
    }




    public void createForeignKey() throws  SQLException{
        JDBJInterface.getInstance().executeUpdate("ALTER TABLE "+RECORD_TABLENAME+"" +
                " ADD CONSTRAINT record_foreign_key FOREIGN KEY ("+ RecordDT.COLUMN_NAME.TAXID.identifier+") " +
                "REFERENCES complete.taxonomy ("+ RecordDT.COLUMN_NAME.TAXID.identifier+")" +
                " MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION");
    }

    public void removeAll() throws  SQLException {
        dropTable();
        createTable();
    }


    public void generateUpdateRecordTaxidFunction() throws  SQLException {

        JDBJInterface.getInstance().executeUpdate("CREATE OR REPLACE FUNCTION "+RECORD_UPDATE_FUN+"()\n" +
                "RETURNS VOID AS\n" +
                "$$\n" +
                "UPDATE "+RECORD_TABLENAME+"\n" +
                "SET "+ RecordDT.COLUMN_NAME.TAXID.identifier+"=s.m\n" +
                "FROM\n" +
                "(SELECT r."+ RecordDT.COLUMN_NAME.TAXID.identifier+" t, t.\"taxMergeId\" m FROM "+
                RECORD_TABLENAME+" r INNER JOIN complete.taxonomy_merge t ON(r."+
                RecordDT.COLUMN_NAME.TAXID.identifier+" = t."+ RecordDT.COLUMN_NAME.TAXID.identifier+")) AS s\n" +
                "WHERE "+ RecordDT.COLUMN_NAME.TAXID.identifier+" = s.t\n" +
                "$$ LANGUAGE 'sql'");
    }

    public void dropUpdateRecordTaxidFunction() throws  SQLException {
        JDBJInterface.getInstance().executeUpdate("DROP FUNCTION IF EXISTS " +
                RECORD_UPDATE_FUN + "()");
    }

    public void updateTaxids() throws  SQLException {
        try {
            JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + RECORD_UPDATE_FUN + "()");
        } catch (SQLException s) {

            if(s.getMessage().contains("ERROR: function "+RECORD_UPDATE_FUN+"() does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
                generateUpdateRecordTaxidFunction();
      JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + RECORD_UPDATE_FUN + "()");

            }
            else if(s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
      JDBJInterface.getInstance().executeQueryUpdate("SELECT * FROM " + RECORD_UPDATE_FUN + "()");
            }
            else {
                throw s;
            }
        }
    }

    public void remove(int recordId) throws  SQLException {

        JDBJInterface.getInstance().
                removeDataFromTable(""+RECORD_TABLENAME+"",""+ RecordDT.COLUMN_NAME.RECORDID.identifier+" = "+ recordId);

    }

    public RecordDT getRecord(int recordId) {
        return SparkComputer.getFirstOrElseNull(
                JDBJInterface.getInstance().fetchQueryResult("SELECT * FROM " + RECORD_TABLENAME +
                        " WHERE " + RecordDT.COLUMN_NAME.RECORDID.identifier + " = " + recordId,RecordDT.ENCODER));

    }

    public String getValues(RecordDT record) {
        return record.getRecordId() + "," +
                "'"+record.getName() + "'," +
                record.getVersion() + "," +
                record.getTaxid() + "," +
                record.getLength() + "," +
                record.isTopology() + "," +
                record.getaContent() + "," +
                record.getcContent() + "," +
                record.getgContent() + "," +
                record.gettContent() + "," +
                record.getAmbigCount() + "," +
                record.isMitosFlag() + "," +
                record.isVisited();
    }

    public void persistID(RecordDT record) throws SQLException {


        if(getRecord(record.getRecordId()) != null) {
            JDBJInterface.getInstance().removeDataFromTable(RECORD_TABLENAME,
                    RecordDT.COLUMN_NAME.RECORDID.identifier +" = "+ record.getRecordId());
        }
        JDBJInterface.getInstance().insertDataIntoTable(RECORD_TABLENAME,getValues(record));

    }

    public void persist(RecordDT record) throws MissingResourceException, SQLException {

        if(record.getVersion() != 0 && record.getTaxid() != 0 && record.getLength() != 0) {
            persistID(record);
        } else {
            String missingField =
                    (record.getVersion() == 0 ?  "version, ": "")
                            + (record.getTaxid() == 0 ?  "taxid, ": "")
                            + (record.getLength() == 0 ?  "length": "");

            throw new MissingResourceException("Missing fields "+ missingField +
                    ". Must be specified before persistence.","Record","");
        }


    }

    /****QUERIES********************************************************************************/

    public Dataset<RecordDT> getTableEntries() throws SQLException {
        if(tableEntries == null) {

            try {
                tableEntries = JDBJInterface.getInstance().fetchTable(RECORD_TABLENAME,RECORD_DT_ENCODER).cache();
            } catch (Exception e) {
                if (e.getMessage().contains("ERROR: relation \"" + RECORD_TABLENAME + "\" does not exist")) {
                    JDBJInterface.getInstance().rollBack();
                    createTable();
                    tableEntries = Spark.getInstance().emptyDataset(RecordDT.ENCODER);
                } else {
                    throw e;
                }
            }
        }
        return tableEntries;
    }

    public  List<Integer> getPersistedRecordIds() throws SQLException {

        List<Integer> recordIds = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT "+ RecordDT.COLUMN_NAME.RECORDID.identifier+" FROM "+RECORD_TABLENAME+"");
            while(resultSet.next()) {
                recordIds.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }

        return recordIds;
    }

    public  List<String> getPersistedRecordNames(boolean onlyVisited) throws SQLException {
        List<String> recordIds = new ArrayList<>();
        try {
            ResultSet resultSet;
            if(onlyVisited) {
               resultSet  = JDBJInterface.getInstance()
                        .executeQuery("SELECT "+ RecordDT.COLUMN_NAME.NAME.identifier+" FROM "+RECORD_TABLENAME+
                                " WHERE " + RecordDT.COLUMN_NAME.VISITED + " = true");
            } else {
                resultSet  = JDBJInterface.getInstance()
                        .executeQuery("SELECT "+ RecordDT.COLUMN_NAME.NAME.identifier+" FROM "+RECORD_TABLENAME);
            }

            while(resultSet.next()) {
                recordIds.add(resultSet.getString(RecordDT.COLUMN_NAME.NAME.identifier));
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }

        return recordIds;
    }

    public  Dataset<RecordTaxidMappingDT> getRecordTaxIdMappingDS() throws SQLException {
        Dataset<RecordTaxidMappingDT> dataset = null;

        try {
            dataset = JDBJInterface.getInstance().fetchQueryResult("SELECT "+ RecordDT.COLUMN_NAME.RECORDID.identifier+"," +
                    " "+ RecordDT.COLUMN_NAME.TAXID.identifier+" FROM "+RECORD_TABLENAME+"", RECORD_TAXID_MAPPING_DT_ENCODER);
        } catch (Exception e) {
            if (e.getMessage().contains("ERROR: relation \"" + RECORD_TABLENAME + "\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }

        return dataset;
    }

    public  Map<Integer,Integer> getRecordTaxIdMapping() throws SQLException{
        Map<Integer,Integer> mapping = new HashMap<>();

//        try {
//            ResultSet resultSet = psqlInterface.
//                    executeQuery("SELECT "+ RecordTable.RECORD_COLUMN_NAMES.RECORDID.identifier+", "+ RecordTable.RECORD_COLUMN_NAMES.TAXID.identifier+" FROM "+RECORD_TABLENAME+"");
//
//            while(resultSet.next()) {
//                mapping.put(resultSet.getInt("recordId"),resultSet.getInt("taxId"));
//            }
//        } catch (SQLException e) {
//            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
//      JDBJInterface.getInstance().rollBack();
//                createTable(psqlInterface);
//            }
//            else {
//                throw e;
//            }
//        }
        return mapping;
    }

    public  Map<Integer,Integer> getRecordTaxIdMapping(List<Integer> recordIds) throws SQLException{


        Map<Integer,Integer> mapping = getRecordTaxIdMapping();
        mapping.keySet().retainAll(recordIds);

        return mapping;
    }

    public  double getRecordSequenceMeanLength() throws SQLException{

        double meanLength = 0;
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT AVG("+ RecordDT.COLUMN_NAME.LENGTH+") FROM "+RECORD_TABLENAME+"");
            resultSet.next();
            meanLength = resultSet.getDouble(1);

        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return meanLength;
    }

    public  long getRecordSequenceMaxLength() throws SQLException{

        long meanLength = 0;
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT MAX("+ RecordDT.COLUMN_NAME.LENGTH+") FROM "+RECORD_TABLENAME+"");
            resultSet.next();
            meanLength = resultSet.getLong(1);

        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return meanLength;
    }

    public List<Integer> getRecordsWithVisitedStatus(boolean status) throws SQLException {

        List<Integer> visitedList = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT "+ RecordDT.COLUMN_NAME.RECORDID.identifier+
                            " FROM "+RECORD_TABLENAME+" WHERE "+ RecordDT.COLUMN_NAME.VISITED.identifier+" = " + status +
                            " ORDER BY "+ RecordDT.COLUMN_NAME.RECORDID.identifier+" DESC");

            while(resultSet.next()) {
                visitedList.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return visitedList;

    }


    public Map<Integer,Boolean> getVisitedStatus() throws SQLException{


        Map<Integer,Boolean> mapping = new HashMap<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT "+ RecordDT.COLUMN_NAME.RECORDID.identifier+", "+
                            RecordDT.COLUMN_NAME.VISITED.identifier+" FROM "+RECORD_TABLENAME+"");

            while(resultSet.next()) {
                mapping.put(resultSet.getInt("recordId"),
                        resultSet.getBoolean(""+ RecordDT.COLUMN_NAME.VISITED.identifier+""));
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return mapping;

    }

    public  Map<Integer,Boolean> getVisitedStatus(List<Integer> recordIds) throws SQLException{


        Map<Integer,Boolean> mapping =getVisitedStatus();
        mapping.keySet().retainAll(recordIds);
        return mapping;

    }

    public void updateVisitedStatus(int recordId, boolean status) throws SQLException{
        try {
            JDBJInterface.getInstance().
            executeUpdate("UPDATE "+RECORD_TABLENAME+" SET "+
                            RecordDT.COLUMN_NAME.VISITED.identifier+" = "+  status  +" WHERE "+ RecordDT.COLUMN_NAME.RECORDID.identifier+" = " + recordId);


        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
                  JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }

    }


    public  List<Integer> getRecordPairing( int recordId) throws SQLException{
        List<Integer> recordList = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT "+ RecordDT.COLUMN_NAME.RECORDID.identifier+
                            " FROM "+RECORD_TABLENAME+" WHERE "+
                            RecordDT.COLUMN_NAME.RECORDID.identifier+" < " + recordId);

            while(resultSet.next()) {
                recordList.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \""+RECORD_TABLENAME+"\" does not exist")) {
      JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return recordList;
    }

    public Map<Integer,List<Integer>> getNextRecordPairing( ) throws SQLException{
        Map<Integer,List<Integer>> map = new HashMap<>();
        Integer recordId = getRecordsWithVisitedStatus(false).get(0);

        map.put(recordId,getRecordPairing(recordId));
        return map;
    }


    /****Auxiliary********************************************************************************/



}

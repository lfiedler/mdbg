package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;

import java.util.List;
import java.util.stream.Collectors;

public class AlignCost {
    private final ScoreCost scoreCost;
    private final List<GapCost> gapCostOptions;

    public AlignCost(ScoreCost scoreCost, List<GapCost> gapCostOptions) {
        this.scoreCost = scoreCost;
        this.gapCostOptions = gapCostOptions;
    }

    public ScoreCost getScoreCost() {
        return scoreCost;
    }

    public List<GapCost> getGapCostOptions() {
        return gapCostOptions;
    }

    public String toString() {
        return scoreCost + "\t" +
        gapCostOptions.stream().
                        map(gapCost -> gapCost.toString()).
                        collect(Collectors.joining(","));
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LaunchExternal {

    private static Logger LOGGER = LoggerFactory.getLogger(LaunchExternal.class);

    public static enum LAUNCH_OPTION {
        START,
        VOID,
        STOP,
        RESTART;
    }

    public static void stopCassandra() {
        try{

            String cmd[] = {"/bin/sh","-c", ProjectDirectoryManager.getControlScript() + " --stop"};
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void stopElasticsearch() {
        try{

            String cmd[] = {"/bin/sh","-c",ProjectDirectoryManager.getControlScript() + " --stop-es"};
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void startCassandra(boolean restart) {
        try{


            String cmd[] = {"/bin/sh","-c",ProjectDirectoryManager.getControlScript() + " --start"};
            if(restart) {
                stopCassandra();
            }
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine())!= null) {
                System.out.println(line);
                if(line.contains(" OK (returned exit status 0 and printed string \"running\"")) {
                    LOGGER.info("Successfully started cassandra");
                    return;
                }
                else if(!restart && line.contains("Cassandra") && line.contains("is running with pid") ) {
                    LOGGER.info("Cassandra is already running");
                    return;
                }

            }
            LOGGER.error(line);
            System.exit(-1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void startElasticsearch(boolean restart) {
        try{


            String cmd[] = {"/bin/sh","-c",ProjectDirectoryManager.getControlScript() + " --start-es"};
            if(restart) {
                stopElasticsearch();
            }
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine())!= null) {
                System.out.println(line);
                if(line.contains("OK (connected to ")) {
                    LOGGER.info("Successfully started elasticsearch");
                    return;
                }
                else if(!restart && line.contains("Elasticsearch") && line.contains("is running")) {
                    LOGGER.info("Elasticsearch is already running");
                    return;
                }

            }
            LOGGER.error(line);
            System.exit(-1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
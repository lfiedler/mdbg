package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import java.util.HashMap;
import java.util.Map;

public class Sequence {

    protected String sequence;
    protected int offsetPosition = 0;
    protected int sequenceIdentifier;
    protected boolean strand;
    public Sequence(String sequence,boolean strand, int sequenceIdentifier) {
        this.sequence = sequence;
        this.strand = strand;
        this.sequenceIdentifier = sequenceIdentifier;
    }

    public Sequence(String sequence, boolean strand,int sequenceIdentifier, int offsetPosition) {
        this(sequence,strand,sequenceIdentifier);
        this.offsetPosition = offsetPosition;
    }

    public String getSequence() {
        return sequence;
    }

    public char getChar(int index) {
        return sequence.charAt(index-1);
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getCharString() {
        String str = "";
        for(int i = 0; i < sequence.length(); i++) {
            str += sequence.charAt(i) + "\t";
        }
        return str;
    }

    public Map<SequenceAligner.NUCLEOTIDE,Long> getNucleotideCount() {
        Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideCount = new HashMap<>();
        for(int i = 0; i < SequenceAligner.NUCLEOTIDE.values().length; i++) {
            SequenceAligner.NUCLEOTIDE nucleotide = SequenceAligner.NUCLEOTIDE.values()[i];

            nucleotideCount.put(nucleotide,sequence.chars().
                    filter(ch -> ch == nucleotide.identifier).count());
        }
        return nucleotideCount;
    }

    public int getOffsetPosition() {
        return offsetPosition;
    }

    public String toString() {
        return sequence;
    }

    public int getSequenceIdentifier() {
        return sequenceIdentifier;
    }

    public void setSequenceIdentifier(int sequenceIdentifier) {
        this.sequenceIdentifier = sequenceIdentifier;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    public void print() {
        System.out.println(getCharString());
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordPropertyTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class RecordPropertyDT implements Serializable, RecordIdInterface, PositionInterface {

    private int recordId;
    private int position;
    private String category;
    private String property;

    public static final Encoder<RecordPropertyDT> ENCODER = Encoders.bean(RecordPropertyDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        POSITION("position"),
        RECORD_ID("\"recordId\""),
        CATEGORY("category"),
        PROPERTY("property");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RecordPropertyDT() {
    }

    public RecordPropertyDT(int recordId, int position, String category, String property) {
        this.recordId = recordId;
        this.position = position;
        this.category = category;
        this.property = property;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}

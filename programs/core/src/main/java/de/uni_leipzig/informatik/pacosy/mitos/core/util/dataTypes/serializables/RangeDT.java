package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;



import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.ScoredProperty;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.expressions.javalang.typed;
import org.apache.spark.sql.types.DataTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.ranges.Range;
import scala.Tuple2;

import java.io.File;
import java.io.Serializable;
import java.util.*;

import static org.apache.spark.sql.functions.*;

public class RangeDT implements Serializable, Comparable<RangeDT>  {
    protected int start;
    protected int end;
    protected int length;
    protected transient List<Integer> positions;
    public static final MapFunction<RangeDT,Integer> LENGTH_KEY =
            (MapFunction<RangeDT,Integer>) range -> range.getLength();
    public static final MapFunction<RangeDT,Integer> START_KEY =
            (MapFunction<RangeDT,Integer>) range -> range.getStart();
    public static final MapFunction<RangeDT,Integer> END_KEY =
            (MapFunction<RangeDT,Integer>) range -> range.getEnd();
    public static final Encoder<RangeDT> ENCODER = Encoders.bean(RangeDT.class);
    protected static final Logger LOGGER = LoggerFactory.getLogger(RangeDT.class);
    public static final WindowSpec POSITION_FRAME = Window.orderBy(ColumnIdentifier.POSITION);
    public static final WindowSpec START_FRAME = Window.orderBy(ColumnIdentifier.START);
    private static final String DIFF_COLUMN_IDENTIFIER = "diff";


    public enum COLUMN_NAME implements DBColumnName {
        START("start"),
        END("end"),
        LENGTH("length");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RangeDT(RangeDT rangeDT) {
        this.start = rangeDT.start;
        this.end = rangeDT.end;
        this.length = rangeDT.length;
//        this.positions = rangeDT.positions;
    }

    public RangeDT(int start, int end) {
        this.start = start;
        this.end = end;

        this.length = end-start+1;
    }
    public RangeDT(int start, int end, int length) {
        this.start = start;
        this.end = end;
        this.length = length;
    }
    public RangeDT() {

    }

    @Override
    public int compareTo(RangeDT range) {
        return this.start -range.start;
    }

    @Override
    public boolean equals(Object o) {

        if(o == this) {
            return true;
        }
        if(!(o instanceof RangeDT)) {
            return false;
        }
        RangeDT s = (RangeDT) o;
        return Integer.valueOf(start).equals(s.start) && Integer.valueOf(end).equals(s.end);
    }

    @Override
    public String toString() {
        return "[" + start + "," + end + "] " + length;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public static int rangeDistance(RangeDT range1, RangeDT range2) {
        return range2.getStart()-range1.getEnd()-1;
    }

    public List<Integer> positions() {
//        List<Integer> positions = new ArrayList<>();
//        for(int i = start; i <= end; i++) {
//            positions.add(i);
//        }
//        return positions;

        if(positions == null) {
            positions = new ArrayList<>();
            for(int i = start; i <= end; i++) {
                positions.add(i);
            }

        }
        return positions;
    }

    public static boolean cyclic(RangeDT range ) {
        return range.start > range.end ? true : false;
    }

    public boolean cyclic() {
        return cyclic(this);
    }

    /**
     * Get a new range of given length which is not cyclic
     * If this.range is not cyclic return a copy 
     * @param length
     * @return
     */
    public RangeDT getCycleShiftedRange(int length){
        if(!cyclic()) {
            return new RangeDT(this);
        }
        else {
            RangeDT rangeDT = new RangeDT();
            rangeDT.start = 0;
            int diff = length -this.end-2;
            rangeDT.end = this.start+diff;
            return rangeDT;
        }
    }

    public static <T extends RangeDT> Column getRangeLength(Dataset<T> rangeData) {
        return rangeData.col(ColumnIdentifier.END).
                minus(rangeData.col(ColumnIdentifier.START)).plus(1);
    }

    public static Dataset<RangeDT> shrinkRange(Dataset<RangeDT> rangeData, int shrinkSize) {
        return shrinkRange(rangeData,ENCODER,shrinkSize);
    }

    public static <T extends RangeDT> Dataset<T> shrinkRange(Dataset<T> rangeData, Encoder<T> tEncoder, int shrinkSize) {
        if(shrinkSize < 0 ) {
            LOGGER.error("Shrinksize must be positive");
            return null;
        }
        String [] columNames = rangeData.columns();
        Column[] columns = new Column[columNames.length];
        for(int i = 0 ; i < columNames.length; i++) {
            String columnName = columNames[i];
            if(columnName.equals(ColumnIdentifier.START)) {
                columns[i] = rangeData.col(ColumnIdentifier.START).plus(shrinkSize).as(ColumnIdentifier.START);
            }
            else if(columnName.equals(ColumnIdentifier.END)) {
                columns[i] = rangeData.col(ColumnIdentifier.END).minus(shrinkSize).as(ColumnIdentifier.END);
            }
            else if(columnName.equals(ColumnIdentifier.LENGTH)) {
                columns[i] = rangeData.col(ColumnIdentifier.LENGTH).minus(2*shrinkSize).as(ColumnIdentifier.LENGTH);
            }
            else {
                columns[i] = col(columnName);
            }
        }

        // remove all ranges that are to small
        return rangeData.filter(lengthGtFilter(2*shrinkSize)).select(columns).as(tEncoder);
    }

    public static boolean between(RangeDT range, int pos, int boundaryLength) {
        // if range is cyclic
        if(range.cyclic()) {
            return between(new RangeDT(range.getStart(),boundaryLength-1),pos,boundaryLength)
                    || between(new RangeDT(0,range.getEnd()),pos,boundaryLength);
        }

        return pos >= range.getStart() && pos <= range.getEnd();
    }

    public static boolean containedRanges(RangeDT range1, RangeDT range2, int boundaryLength) {
//        System.out.println(range1 + " " + range2);
        if(between(range1,range2.getStart(),boundaryLength) || between(range2,range1.getStart(),boundaryLength)) {
            return true;
        }
        return false;
    }

//    public static <T> Dataset<Row> expandRangeStart(Dataset<T> rangeData, String startColumnName, String endColumnName, int expandSize) {
//        if(expandSize < 0 ) {
//            LOGGER.error("Expandsize must be positive");
//            return null;
//        }
//        String [] columNames = rangeData.columns();
//        Column[] columns = new Column[columNames.length];
//        for(int i = 0 ; i < columNames.length; i++) {
//            String columnName = columNames[i];
//            if(columnName.equals(startColumnName)) {
//                columns[i] = rangeData.col(startColumnName).minus(expandSize).as(ColumnIdentifier.START);
//            }
//            else if(columnName.equals(ColumnIdentifier.END)) {
//                columns[i] = rangeData.col(ColumnIdentifier.END).minus(shrinkSize).as(ColumnIdentifier.END);
//            }
//            else if(columnName.equals(ColumnIdentifier.LENGTH)) {
//                columns[i] = rangeData.col(ColumnIdentifier.LENGTH).minus(2*shrinkSize).as(ColumnIdentifier.LENGTH);
//            }
//            else {
//                columns[i] = col(columnName);
//            }
//        }
//
//        // remove all ranges that are to small
//        return rangeData.filter(lengthGtFilter(2*shrinkSize)).select(columns).as(tEncoder);
//    }

    /*** Filter ****/
//    public static <T extends RangeDT> FilterFunction<T> lengthGtFilter(int length, Class<T> tClass) {
//        return range -> range.getLength() > length;
//    }

    public static <T extends RangeDT> FilterFunction<T> lengthGtFilter(int length) {
        return range -> range.getLength() > length;
    }

    public static <T extends RangeDT> FilterFunction<T> lengthGtEqFilter(int length) {
        return range -> range.getLength() >= length;
    }

    public static <T extends RangeDT> FilterFunction<T> lengthLtFilter(int length) {
        return range -> range.getLength() < length;
    }

    public static <T extends RangeDT> FilterFunction<T> lengtLtEqFilter(int length) {
        return range -> range.getLength() < length;
    }

    public static <T extends RangeDT> Dataset<T> filterRangeDataLtEq(Dataset<T> rangeData, int minLength) {
        return rangeData.filter(lengthGtEqFilter(minLength));
    }

    public static <T extends RangeDT> Dataset<T> filterRangeGtEq(Dataset<T> rangeData, int maxLength) {
        return rangeData.filter(lengtLtEqFilter(maxLength));
    }


    public static <T extends PositionInterface> FilterFunction<T> rangeFilter(RangeDT rangeDT) {
        return (k -> k.getPosition() >= rangeDT.getStart() && k.getPosition() <= rangeDT.getEnd());
    }

    public static <T extends PositionInterface> Dataset<T> filterByRanges(Dataset<T> dataset, Dataset<? extends RangeDT> ranges, Encoder<T> tEncoder) {
        return dataset.join(createPositions(ranges).withColumnRenamed(ColumnIdentifier.POSITION,"p"),
                col(ColumnIdentifier.POSITION).equalTo(col("p")),"inner").
                drop("p").as(tEncoder);
    }

    /*** Creation ****/

    public static <T extends PositionInterface> void checkPositionsAreOrdered(Iterator<T> positions) {
        int prevPos = positions.next().getPosition();
        int pos;
        while(positions.hasNext()) {
            pos = positions.next().getPosition();
            if(pos < prevPos) {
                System.out.println(prevPos + " " + pos);
            }
            prevPos = pos;
        }
    }

    public static  Dataset<RangeDT> createRanges(Dataset<?> positionData) {


        return positionData.withColumn(DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(row_number().over(POSITION_FRAME))).
                groupBy(DIFF_COLUMN_IDENTIFIER).
                agg(
                        min(ColumnIdentifier.POSITION).as(COLUMN_NAME.START.identifier),
                        max(ColumnIdentifier.POSITION).as(COLUMN_NAME.END.identifier),
                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier).
                as(ENCODER);
    }

    public static Dataset<RangeDT> createCyclicRangesFromPositions(Dataset<?> positionData, RangeDT boundaryRange) {
//        positionData.show();
        Dataset<RangeDT> ranges = createRanges(positionData);
//        ranges.show();
        if(ranges.count() == 1) {
//            System.out.println("one range");
//            System.out.println(boundaryRange);
//            System.out.println(ranges.first());
            if(ranges.first().equals(boundaryRange)) {
                return ranges;
            }
        }
//        SparkComputer.showAll(ranges);
        RangeDT range1 = SparkComputer.getFirstOrElseNull(ranges.filter(col(ColumnIdentifier.START).equalTo(boundaryRange.getStart())));
        RangeDT range2 = SparkComputer.getFirstOrElseNull(ranges.filter(col(ColumnIdentifier.END).equalTo(boundaryRange.getEnd())));
//        System.out.println(range1);
//        System.out.println(range2);
        if(range1 != null && range2 != null) {
            RangeDT newRange = new RangeDT(range2.getStart(),range1.getEnd());
            newRange.setLength(range1.getLength()+ range2.getLength());
//            System.out.println(newRange);
            Dataset<RangeDT> newRangeDataset = SparkComputer.createDataFrame(Collections.singletonList(newRange),RangeDT.ENCODER).
                    select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH).as(RangeDT.ENCODER);
//            newRangeDataset.show();
//            ranges.filter(col(ColumnIdentifier.START).notEqual(boundaryRange.getStart()).
//                    and(col(ColumnIdentifier.END).notEqual(boundaryRange.getEnd()))).orderBy(ColumnIdentifier.START).show();
            ranges = ranges.filter(col(ColumnIdentifier.START).notEqual(boundaryRange.getStart()).
                    and(col(ColumnIdentifier.END).notEqual(boundaryRange.getEnd()))).
                    union(newRangeDataset);
        }
        return ranges.orderBy(ColumnIdentifier.START);
    }

    public static Dataset<RangeDT> createLinearizedRanges(Dataset<RangeDT> rangeData, RangeDT boundaryRange) {
        rangeData = convert(rangeData);
        Dataset<RangeDT> cyclicRange = rangeData.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END)));
//        cyclicRange.show();
        rangeData = rangeData.except(cyclicRange);
//        rangeData.show();
        cyclicRange = cyclicRange.drop(ColumnIdentifier.END).withColumn(ColumnIdentifier.END,lit(boundaryRange.end).cast(DataTypes.IntegerType)).
                    select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH).
                union(cyclicRange.drop(ColumnIdentifier.START).withColumn(ColumnIdentifier.START,lit(0).cast(DataTypes.IntegerType)).
                        select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH)).as(ENCODER);
        return rangeData.union(cyclicRange);
    }

    public static Dataset<RangeDT> createCyclicRanges(Dataset<RangeDT> rangeData, int sequenceLength) {
        rangeData = convert(rangeData);
        Dataset<RangeDT> cyclicRange = rangeData.filter(col(ColumnIdentifier.START).equalTo(0).or(col(ColumnIdentifier.END).equalTo(sequenceLength-1))).orderBy(ColumnIdentifier.START);
        if(cyclicRange.count() < 2) {
            return rangeData;
        }
        rangeData = rangeData.except(cyclicRange);
        List<RangeDT> cyclicRangeList = cyclicRange.collectAsList();
        RangeDT newRange = new RangeDT(cyclicRangeList.get(1).getStart(),cyclicRangeList.get(0).getEnd());
        newRange.setLength(cyclicRangeList.get(0).getLength()+ cyclicRangeList.get(1).getLength());
        rangeData = rangeData.union(
            SparkComputer.createDataFrame(Collections.singletonList(newRange),ENCODER)
        );
        return rangeData;
    }

    public static boolean cyclic(Dataset<RangeDT> rangeDTDataset) {
        return !rangeDTDataset.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END))).isEmpty();
    }

    public static boolean cyclic(List<RangeDT> rangeDTDataset) {
        return rangeDTDataset.stream().filter(range -> range.cyclic()).count() > 0;
    }

    public static <T extends PositionInterface> Dataset<Row> createRangesWithData(Dataset<T> positionData,Column aggregateColumn) {


        return positionData.withColumn(DIFF_COLUMN_IDENTIFIER,col(ColumnIdentifier.POSITION).minus(row_number().over(POSITION_FRAME))).
                groupBy(DIFF_COLUMN_IDENTIFIER).
                agg(
                        aggregateColumn,
                        min(ColumnIdentifier.POSITION).as(COLUMN_NAME.START.identifier),
                        max(ColumnIdentifier.POSITION).as(COLUMN_NAME.END.identifier),
                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
                drop(DIFF_COLUMN_IDENTIFIER);
    }

    public static <T> Dataset<Row> createRangesForColumnGroup(String groupByColumnName, String positionColumnName,  Dataset<T> dataset) {

        WindowSpec w = Window.partitionBy(groupByColumnName).orderBy(positionColumnName);

        return dataset.
                withColumn(DIFF_COLUMN_IDENTIFIER,col(positionColumnName).minus(row_number().over(w))).
                groupBy(groupByColumnName,DIFF_COLUMN_IDENTIFIER).
                agg(
                        min(ColumnIdentifier.POSITION).as(COLUMN_NAME.START.identifier),
                        max(ColumnIdentifier.POSITION).as(COLUMN_NAME.END.identifier),
                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
                drop(DIFF_COLUMN_IDENTIFIER);
//                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier);
    }

    public static <T> Dataset<Row> createRangesForColumnGroup(String groupByColumnName, String positionOrderColumnName, String positionColumnName, Dataset<T> dataset) {

        WindowSpec w = Window.partitionBy(groupByColumnName).orderBy(positionOrderColumnName);
        final String diffOrderedIdentifier = "diffOrdered";
        return dataset.
                withColumn(diffOrderedIdentifier,col(positionOrderColumnName).minus(row_number().over(w))).
                withColumn(DIFF_COLUMN_IDENTIFIER,col(positionColumnName).minus(row_number().over(w))).
                groupBy(groupByColumnName,DIFF_COLUMN_IDENTIFIER).
                agg(
                        min(positionOrderColumnName).as(COLUMN_NAME.START.identifier),
                        max(positionOrderColumnName).as(COLUMN_NAME.END.identifier),
                        max(positionOrderColumnName).minus(min(positionOrderColumnName)).plus(1).as(COLUMN_NAME.LENGTH.identifier),
                        min(positionColumnName).as(ColumnIdentifier.RECORD_START),
                        max(positionColumnName).as(ColumnIdentifier.RECORD_END)).
//                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
                drop(DIFF_COLUMN_IDENTIFIER);
//                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier);
    }
//
    public static <T extends PositionInterface> List<RangeDT> createRanges(Iterator<T> positionsIt) {

        if(!positionsIt.hasNext()) {
            System.out.println("No positions provided");
            return null;
        }
//        checkPositionsAreOrdered(positionsIt);
        Integer prevPos = null;
        Integer pos = null;

        List<RangeDT> rangeList = new ArrayList<>();

        // add first element to first range
        pos = positionsIt.next().getPosition();
        RangeDT range = new RangeDT();
        range.setStart(pos);

        while(positionsIt.hasNext()) {
            prevPos = pos;
            pos = positionsIt.next().getPosition();

            if(pos < prevPos) {
                LOGGER.error("Pos > prevpos " + pos + " " + prevPos);

            }
            if( pos != prevPos + 1) {
                range.setEnd(prevPos);
                range.setLength(range.getEnd()-range.getStart()+1);
                rangeList.add(range);
//                System.out.println(range);
                range = new RangeDT();
                range.setStart(pos);
            }
        }
        range.setEnd(pos);
        range.setLength(range.getEnd()-range.getStart()+1);
//        System.out.println(range);
        rangeList.add(range);
//        System.out.println("done");
        return rangeList;

    }

    public static Dataset<RangeDT> createRangesForPositions(Dataset<Integer> positions) {

        String positionColumn = positions.columns()[0];
        return positions.withColumn(DIFF_COLUMN_IDENTIFIER,col(positionColumn).minus(row_number().over(POSITION_FRAME))).
                groupBy(DIFF_COLUMN_IDENTIFIER).
                agg(
                        min(positionColumn).as(COLUMN_NAME.START.identifier),
                        max(positionColumn).as(COLUMN_NAME.END.identifier),
                        max(positionColumn).minus(min(positionColumn)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier).
                as(ENCODER);

    }

    public static List<RangeDT> createRangesForPositions(List<Integer> positions) {
        List<RangeDT> ranges = createRangesForPositions(positions.iterator());
        if(ranges == null) {
            return new ArrayList<>();
        }
        return ranges;
    }

    public static List<RangeDT> createRangesForPositions(Iterator<Integer> positionsIt) {

        if(!positionsIt.hasNext()) {
            System.out.println("No positions provided");
            return null;
        }
        Integer prevPos = null;
        Integer pos = null;

        List<RangeDT> rangeList = new ArrayList<>();

        // add first element to first range
        pos = positionsIt.next();
        RangeDT range = new RangeDT();
        range.setStart(pos);

        while(positionsIt.hasNext()) {
            prevPos = pos;
            pos = positionsIt.next();

            if( pos != prevPos + 1) {
                range.setEnd(prevPos);
                range.setLength(range.getEnd()-range.getStart()+1);
                rangeList.add(range);
//                System.out.println(range);
                range = new RangeDT();
                range.setStart(pos);
            }
        }
        range.setEnd(pos);
        range.setLength(range.getEnd()-range.getStart()+1);
//        System.out.println(range);
        rangeList.add(range);

        return rangeList;

    }

    public static List<List<RangeDT>> createMultiRanges(List<List<Integer>> positionLists) {
        List<List<RangeDT>> rangeList = new ArrayList<>();
        for(int listcounter = 0; listcounter < positionLists.get(0).size(); listcounter++) {
            List<Integer> positions = new ArrayList<>();
            for(int i = 0; i < positionLists.size(); i++) {
                positions.add(positionLists.get(i).get(listcounter));
            }
//            System.out.println(positions);
            rangeList.add(createRangesForPositions(positions));
        }
//        System.out.println(rangeList);
        return rangeList;
    }

    public static List<List<RangeDT>> createMappedMultiRanges(List<List<Integer>> positionLists) {
        List<List<RangeDT>> mappedRangeList = new ArrayList<>();
        List<List<RangeDT>> rangeList = createMultiRanges(positionLists);
        for(int rangeCounter = 0; rangeCounter < rangeList.get(0).size(); rangeCounter++) {
            List<RangeDT> ranges = new ArrayList<>();
            for(int i = 0; i < rangeList.size(); i++) {
                ranges.add(rangeList.get(i).get(rangeCounter));
            }
            mappedRangeList.add(ranges);
        }
        return mappedRangeList;
    }

    public static void adaptCyclicRanges(List<RangeDT> rangeList) {
        if(rangeList.size() == 1) {
            return;
        }
        RangeDT joinedRange = new RangeDT();
        joinedRange.setStart(rangeList.get(rangeList.size()-1).getStart());
        joinedRange.setEnd(rangeList.get(0).getEnd());
        joinedRange.setLength(rangeList.get(0).getLength()+rangeList.get(rangeList.size()-1).getLength());
        rangeList.remove(0);
        rangeList.remove(rangeList.size()-1);
        rangeList.add(joinedRange);
    }


    public static Dataset<Tuple2<Integer,Long>> createRangeLengthDistribution(Dataset<RangeDT> range) {
        return range.groupByKey(LENGTH_KEY,Encoders.INT()).
                agg(typed.count((MapFunction<RangeDT,Object>) r -> r.getStart()).name(ColumnIdentifier.COUNT));
    }

    public static <T extends RangeDT> Dataset<Row> createRangeLengthSummary(Dataset<T> ranges) {
        return ranges.groupBy().agg(
                min(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MIN),
                max(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MAX),
                round(mean(col(ColumnIdentifier.LENGTH))).as(ColumnIdentifier.MEAN),
                sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.SUM));
    }


    /**
     * Create gaps between provided ranges. BoundaryRange defines boundaries in which ranges are embedded.
     * This is necessary to know the minimal possible and maximal possible position.
     * @param ranges
     * @param boundaryRange
     * @return
     */
    public static Dataset<RangeDT> createInverseRange(Dataset<RangeDT> ranges, RangeDT boundaryRange) {

        if(ranges.count() == 1) {
            RangeDT r = ranges.first();
            if(r.getStart() == boundaryRange.getStart() && boundaryRange.getEnd() == r.getEnd()) {
                return SparkComputer.createDataFrame(new ArrayList<>(),RangeDT.ENCODER);
            }
        }
        Dataset<RangeDT> inverseRanges =ranges.withColumn(ColumnIdentifier.INVERSE_START,
                lag(col(ColumnIdentifier.END),1,boundaryRange.getStart()-1).
                        over(START_FRAME).plus(1)).
                withColumn(ColumnIdentifier.INVERSE_END,
                        when(col(ColumnIdentifier.START).gt(boundaryRange.getStart()),
                        col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
                select(
                        col(ColumnIdentifier.INVERSE_START).as(ColumnIdentifier.START),
                        col(ColumnIdentifier.INVERSE_END).as(ColumnIdentifier.END)).
                withColumn(ColumnIdentifier.LENGTH,col(ColumnIdentifier.END).minus(col(ColumnIdentifier.START)).plus(1)).
                as(ENCODER);
        if(inverseRanges.isEmpty()) {
            return inverseRanges;
        }
        final long end = inverseRanges.groupBy().max(ColumnIdentifier.END).as(Encoders.LONG()).first();
//        LOGGER.info("End: " +end);
        if(end < boundaryRange.getEnd()) {
            inverseRanges.union(
                    SparkComputer.createDataFrame(
                            Collections.singletonList(new RangeDT((int)end,boundaryRange.getEnd())),
                            ENCODER));
        }
//        inverseRanges.show();
        return inverseRanges;
    }

    /**
     * Create gaps between provided ranges. BoundaryRange defines boundaries in which ranges are embedded.
     * This is necessary to know the minimal possible and maximal possible position.d
     * @param ranges
     * @param boundaryRange
     * @return
     */
    public static Dataset<RangeDT> createCyclicInverseRange(Dataset<RangeDT> ranges, RangeDT boundaryRange) {
        ranges = convert(createLinearizedRanges(convert(ranges),boundaryRange)).orderBy(ColumnIdentifier.START);
//        System.out.println("linearized");
//        ranges.show();
        Dataset<RangeDT> inverseRanges =convert(ranges.withColumn(ColumnIdentifier.INVERSE_START,
                lag(col(ColumnIdentifier.END),1,boundaryRange.getStart()-1).
                        over(START_FRAME).plus(1)).
                withColumn(ColumnIdentifier.INVERSE_END,
                        when(col(ColumnIdentifier.START).gt(boundaryRange.getStart()),
                                col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
                select(
                        col(ColumnIdentifier.INVERSE_START).as(ColumnIdentifier.START),
                        col(ColumnIdentifier.INVERSE_END).as(ColumnIdentifier.END)).
                withColumn(ColumnIdentifier.LENGTH,col(ColumnIdentifier.END).minus(col(ColumnIdentifier.START)).plus(1)));
        if(inverseRanges.isEmpty()) {
            return inverseRanges;
        }
        final long end = inverseRanges.groupBy().max(ColumnIdentifier.END).as(Encoders.LONG()).first();
//        LOGGER.info("End: " +end);
        if(end < boundaryRange.getEnd()) {
            inverseRanges.union(
                    SparkComputer.createDataFrame(
                            Collections.singletonList(new RangeDT((int)end,boundaryRange.getEnd())),
                            ENCODER));
        }
//        inverseRanges.show();
        inverseRanges = createCyclicRanges(inverseRanges,boundaryRange.end+1);
//        inverseRanges.show();
        return inverseRanges;
    }

    public static Dataset<Integer> createPositions(Dataset<?> ranges) {
        Dataset<RangeDT> rangeDTDataset = convert(ranges);
        return rangeDTDataset.
                withColumn(ColumnIdentifier.POSITION,
                        explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))).
                select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT());
    }

    public static Dataset<Integer> createCyclicPositions(Dataset<?> ranges, RangeDT boundaryRange) {
        Dataset<RangeDT> rangeDTDataset = createLinearizedRanges(convert(ranges),boundaryRange);
        return rangeDTDataset.
                withColumn(ColumnIdentifier.POSITION,
                        explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))).
                select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT());
    }
    public static Dataset<RangeDT> convert(Dataset<?> dataset) {
        return dataset.select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH).as(ENCODER);
    }

}


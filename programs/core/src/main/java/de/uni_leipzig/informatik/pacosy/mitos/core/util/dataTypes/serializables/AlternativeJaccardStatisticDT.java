package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.util.List;

public class AlternativeJaccardStatisticDT extends  JaccardStatisticDT{
    protected String property2;
    protected String category2;

    public static final Encoder<AlternativeJaccardStatisticDT> ENCODER = Encoders.bean(AlternativeJaccardStatisticDT.class);

    public AlternativeJaccardStatisticDT() {
    }

    public AlternativeJaccardStatisticDT(String property, String category, double jaccardIndex,
                                         List<Integer> positions1, List<Integer> positions2,
                                         boolean strand, int matchFlag,String property2, String category2) {
        super(property, category, jaccardIndex, positions1, positions2,strand,matchFlag);
        this.property2 = property2;
        this.category2 = category2;
    }

    public String getProperty2() {
        return property2;
    }

    public void setProperty2(String property2) {
        this.property2 = property2;
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    @Override
    public String toString() {
        return                 " property='" + property + '\'' +
                ", category='" + category + '\'' +
                ", property2='" + property2 + '\'' +
                ", category2='" + category2 + '\'' +

                ", jaccardIndex=" + jaccardIndex +
                ", p1Size=" + p1Size +
                ", p2Size=" + p2Size +
                ", matchFlag=" + matchFlag +
                ", p1NoP2Size=" + p1NoP2Size +
                ", p2NoP1Size=" + p2NoP1Size +
                ", strand=" + strand ;
    }
}

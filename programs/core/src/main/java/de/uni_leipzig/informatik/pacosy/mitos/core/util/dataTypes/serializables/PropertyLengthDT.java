package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class PropertyLengthDT implements Serializable {
    protected String category;
    protected String property;

    protected int meanLength;
    protected int count;

    public static final Encoder<PropertyLengthDT> ENCODER = Encoders.bean(PropertyLengthDT.class);

    public PropertyLengthDT() {

    }

    public PropertyLengthDT(String category, String property, int meanLength, int count) {
        this.category = category;
        this.property = property;
        this.meanLength = meanLength;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }


    public int getMeanLength() {
        return meanLength;
    }

    public void setMeanLength(int meanLength) {
        this.meanLength = meanLength;
    }

    public static Dataset<PropertyLengthDT> convert(Dataset<?> dataset) {
        return dataset.select(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,
                ColumnIdentifier.MEAN_LENGTH,ColumnIdentifier.COUNT).as(ENCODER);
    }

}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class CategoryCountRecordIdDT implements Serializable {
    public static final Encoder<CategoryCountRecordIdDT> ENCODER = Encoders.bean(CategoryCountRecordIdDT.class);
    protected String category;
    protected long count;
    protected int recordId;

    public CategoryCountRecordIdDT(String category, int count, int recordId) {
        this.category = category;
        this.count = count;
        this.recordId = recordId;
    }

    public CategoryCountRecordIdDT() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }
}

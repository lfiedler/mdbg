package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.StrandInterface;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

public class StrandKmerDT extends KmerDT implements StrandInterface {
    private boolean strand;
    public static final Encoder<StrandKmerDT> ENCODER = Encoders.bean(StrandKmerDT.class);
    public static final StructType SCHMEA;
    static {
        List<StructField> structFields = new ArrayList<>();
        structFields.add(DataTypes.createStructField(ColumnIdentifier.KMER,DataTypes.StringType,true));
        structFields.add(DataTypes.createStructField(ColumnIdentifier.RECORD_ID,DataTypes.IntegerType,true));
        structFields.add(DataTypes.createStructField(KmerDT.COLUMN_NAME.POSITIONS.identifier, DataTypes.createArrayType(DataTypes.IntegerType),true));
        structFields.add(DataTypes.createStructField(StrandKmerDT.COLUMN_NAME.STRAND.identifier,DataTypes.BooleanType,true));
        SCHMEA = DataTypes.createStructType(structFields);
    }

    public enum COLUMN_NAME implements DBColumnName {
        STRAND("strand"),
        KMER("kmer"),
        RECORD_ID("\"recordId\""),
        POSITIONS("positions");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public StrandKmerDT(String kmer, Integer recordId, Integer[] positions, boolean strand) {
        super(kmer, recordId, positions);
        this.strand = strand;
    }

    public StrandKmerDT() {

    }


    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    public static Dataset<KmerDT> filterStrand(Dataset<StrandKmerDT> kmers, boolean strand) {
        return kmers.filter(StrandInterface.strandFilter(strand,StrandKmerDT.class)).
                select(ColumnIdentifier.KMER,
                        ColumnIdentifier.RECORD_ID,
                        KmerDT.COLUMN_NAME.POSITIONS.identifier).
                as(KmerDT.ENCODER);
    }
}

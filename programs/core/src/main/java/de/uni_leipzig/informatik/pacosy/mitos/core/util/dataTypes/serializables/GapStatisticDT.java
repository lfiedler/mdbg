package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class GapStatisticDT implements Serializable {
    protected long gapCount;
    protected long unbridgedCount;
    public static final Encoder<GapStatisticDT> ENCODER = Encoders.bean(GapStatisticDT.class);

    public GapStatisticDT() {
    }

    public GapStatisticDT(long gapCount, long unbridgedCount) {
        this.gapCount = gapCount;
        this.unbridgedCount = unbridgedCount;
    }

    public long getGapCount() {
        return gapCount;
    }

    public void setGapCount(long gapCount) {
        this.gapCount = gapCount;
    }

    public long getUnbridgedCount() {
        return unbridgedCount;
    }

    public void setUnbridgedCount(long unbridgedCount) {
        this.unbridgedCount = unbridgedCount;
    }

    public double bridgedRatio() {
        return ((double) unbridgedCount)/gapCount;
    }


}

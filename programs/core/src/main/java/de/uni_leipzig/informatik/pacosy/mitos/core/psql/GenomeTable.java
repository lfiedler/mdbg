package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.BandedAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.Sequence;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAlignerFactory;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.AlignCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.GenomeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Encoders;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenomeTable implements SQLTable {

    private static GenomeTable genomeTable;
    private static final String GENOME_PLUS_TABLENAME = "complete.genome_plus";
    private static final String GENOME_MINUS_TABLENAME = "complete.genome_minus";
    private static final String GENOME_PLUS_PKEY = "genome_plus_pkey";
    private static final String GENOME_MINUS_PKEY = "genome_minus_pkey";
    private static final String EXTRACT_PLUS_SUBSTRING_FUN = "complete.extract_plus_substring";
    private static final String EXTRACT_SUBSTRING_FUN = "complete.extract_substring";
    private static final String EXTRACT_MINUS_SUBSTRING_FUN = "complete.extract_minus_substring_direct";
//    private static final String EXTRACT_MINUS_SUBSTRING_FUN = "complete.extract_minus_substring";


    private GenomeTable() {

    }

    public static synchronized GenomeTable getInstance() {
        if(genomeTable == null) {
            genomeTable = new GenomeTable();
        }
        return genomeTable;
    }

    /****CREATION********************************************************************************/
    public void createTable(String tableName) throws SQLException {
        JDBJInterface.getInstance().createTable(tableName,
                GenomeDT.COLUMN_NAME.RECORD_ID.identifier + " Integer,\n" +
                        GenomeDT.COLUMN_NAME.SEQUENCE.identifier + " TEXT" ,true);
    }

    public void createTable() throws SQLException {
        createTable(GENOME_PLUS_TABLENAME);
        createTable(GENOME_MINUS_TABLENAME);
        createExtractSubstringFunction();
    }

    public void dropTable() throws SQLException {
        JDBJInterface.getInstance().dropTable(GENOME_PLUS_TABLENAME,true);
        JDBJInterface.getInstance().dropTable(GENOME_MINUS_TABLENAME,true);
    }

    public void createPrimaryKey() throws SQLException {
        JDBJInterface.getInstance().createPrimaryKey(GENOME_PLUS_TABLENAME, GENOME_PLUS_PKEY, GenomeDT.COLUMN_NAME.RECORD_ID.identifier);
        JDBJInterface.getInstance().createPrimaryKey(GENOME_MINUS_TABLENAME, GENOME_MINUS_PKEY, GenomeDT.COLUMN_NAME.RECORD_ID.identifier);
    }

    public void dropPrimaryKey() throws SQLException {
        JDBJInterface.getInstance().dropPrimaryKey(GENOME_PLUS_TABLENAME, GENOME_PLUS_PKEY);
        JDBJInterface.getInstance().dropPrimaryKey(GENOME_MINUS_TABLENAME, GENOME_MINUS_PKEY);
    }

    private void createExtractSubstringFunction() throws SQLException {
        JDBJInterface.getInstance().executeUpdate(
            "CREATE OR REPLACE FUNCTION "+ EXTRACT_PLUS_SUBSTRING_FUN +"( record integer, startpos integer,  stoppos integer)\n" +
                    "RETURNS TEXT\n" +
                    "AS $$\n" +
                    "DECLARE\n" +
                    "\tr ALIAS FOR $1;\n" +
                    "\tsp integer;\n" +
                    "\tse integer;\n" +
                    "\ts text;\n" +
                    "BEGIN\n" +
                    "\tsp := $2 +1;\n" +
                    "\n" +
                    "\tIF ($2 > $3) THEN\n" +
                    "\t\tse := $3+1;\n" +
                    "\t\tSELECT substring(sequence,sp) || substring(sequence,1,se) FROM "+ GENOME_PLUS_TABLENAME+" WHERE \"recordId\" = r INTO s;\n" +
                    "\tELSE\n" +
                    "\t\tse := $3-$2+1;\n" +
                    "\t\tSELECT substring(sequence,sp,se) FROM "+ GENOME_PLUS_TABLENAME+" WHERE \"recordId\" = r INTO s;\n" +
                    "\tEND IF;\n" +
                    "\n" +
                    "\tRETURN s;\n" +
                    "END\n" +
                    "\n" +
                    "$$ LANGUAGE plpgsql;"
        );


        JDBJInterface.getInstance().executeUpdate(
                "CREATE OR REPLACE FUNCTION "+ EXTRACT_MINUS_SUBSTRING_FUN +"( record integer, startpos integer,  stoppos integer)\n" +
                        "RETURNS TEXT\n" +
                        "AS $$\n" +
                        "DECLARE\n" +
                        "\tr ALIAS FOR $1;\n" +
                        "\tsp integer;\n" +
                        "\tse integer;\n" +
                        "\ts text;\n" +
                        "BEGIN\n" +
                        "\tsp := $2 +1;\n" +
                        "\n" +
                        "\tIF ($2 > $3) THEN\n" +
                        "\t\tse := $3+1;\n" +
                        "\t\tSELECT substring(sequence,sp) || substring(sequence,1,se) FROM "+ GENOME_MINUS_TABLENAME+" WHERE \"recordId\" = r INTO s;\n" +
                        "\tELSE\n" +
                        "\t\tse := $3-$2+1;\n" +
                        "\t\tSELECT substring(sequence,sp,se) FROM "+ GENOME_MINUS_TABLENAME+" WHERE \"recordId\" = r INTO s;\n" +
                        "\tEND IF;\n" +
                        "\n" +
                        "\tRETURN s;\n" +
                        "END\n" +
                        "\n" +
                        "$$ LANGUAGE plpgsql;"
        );
    }

    private void dropExtractSubstringFunction() throws SQLException {
        JDBJInterface.getInstance().dropFunction(EXTRACT_PLUS_SUBSTRING_FUN);
        JDBJInterface.getInstance().dropFunction(EXTRACT_MINUS_SUBSTRING_FUN);
    }

    public void addGenome(int recordId, String genome) throws SQLException {
        try {
            JDBJInterface.getInstance().insertDataIntoTable(GENOME_PLUS_TABLENAME,"("+recordId+",'" + genome + "')");
            JDBJInterface.getInstance().insertDataIntoTable(GENOME_MINUS_TABLENAME,"("+recordId+",'" +
                    SequenceParser.reverseComplement(genome) + "')");

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                JDBJInterface.getInstance().insertDataIntoTable(GENOME_PLUS_TABLENAME,"("+recordId+",'" + genome + "')");
                JDBJInterface.getInstance().insertDataIntoTable(GENOME_MINUS_TABLENAME,"("+recordId+",'" +
                        SequenceParser.reverseComplement(genome) + "')");
            } else {
                throw s;
            }
        }
    }

    /****QUERIES********************************************************************************/

    private String getSequence( int recordId, String tableName) throws SQLException {
        String sequence = null;
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT " + GenomeDT.COLUMN_NAME.SEQUENCE.identifier +
                            " FROM " +tableName +" WHERE " + GenomeDT.COLUMN_NAME.RECORD_ID.identifier + "=" + recordId);
            resultSet.next();
            sequence = resultSet.getString(GenomeDT.COLUMN_NAME.SEQUENCE.identifier);
        } catch (SQLException e) {
            if (e.getMessage().contains("ERROR: relation "+tableName+" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }
        return sequence;
    }

    public String getPlusSequence( int recordId) throws SQLException {
        return getSequence(recordId,GENOME_PLUS_TABLENAME);
    }

    public String getMinusSequence( int recordId) throws SQLException {
        return getSequence(recordId,GENOME_MINUS_TABLENAME);
    }

    private String getSubSequence( String functionName, int recordId, int startPos, int endPos) throws SQLException {
        String result = null;
        try {
            result = SparkComputer.getFirstOrElseNull(JDBJInterface.getInstance().
                    fetchQueryResult("SELECT * FROM " + functionName +
                            " (" + recordId + "," + startPos + "," + endPos + ")",Encoders.STRING()));

        } catch (Exception s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
        return result;
    }

    /**
     * Fetch the substring [startPos,endPos] from the genome,
     * where startPos and endPos are positions on the positive strand.
     * If startPos > endPos apply cyclic boundary conditions
     * @param recordId
     * @param startPos
     * @param endPos
     * @return
     * @throws SQLException
     */
    public String getPlusSubSequence( int recordId, int startPos, int endPos) throws SQLException {
        return getSubSequence(EXTRACT_PLUS_SUBSTRING_FUN, recordId,startPos,endPos);
    }

    /**
     * Fetch the substring [startPos,endPos] from the genome,
     * where startPos and endPos are positions on the POSITIVE strand,
     * that is usually (except for the boundaries startPos > endPos)
     * If startPos < endPos apply cyclic boundary conditions
     * @param recordId
     * @param startPos
     * @param endPos
     * @return
     * @throws SQLException
     */
    public String getMinusSubSequence( int recordId, int startPos, int endPos) throws SQLException {
        return getSubSequence(EXTRACT_MINUS_SUBSTRING_FUN, recordId,startPos,endPos);
    }

    public String getSubSequence( int recordId, int startPos, int endPos, boolean strand) throws SQLException {
        if(strand) {
            return getPlusSubSequence(recordId,startPos,endPos);
        }

        return getMinusSubSequence(recordId,startPos,endPos);
    }
    public List<Integer> getPersistedRecordIds() throws SQLException {

        List<Integer> recordIds = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT DISTINCT(" + RecordDT.COLUMN_NAME.RECORDID.identifier +
                            ") FROM " +GENOME_PLUS_TABLENAME +"");
            while (resultSet.next()) {
                recordIds.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("ERROR: relation \"" + GENOME_PLUS_TABLENAME+ "\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }

        return recordIds;
    }

    public void removeDuplicates(boolean strand) throws SQLException {
        try {
            JDBJInterface.getInstance().executeUpdate("DELETE FROM\n" +
                    getTableName(strand) + " t1\n" +
                    "USING "+getTableName(strand) +" t2\n" +
                    "WHERE t1.ctid < t2.ctid\n" +
                    "AND t1.\"recordId\" = t2.\"recordId\"\n" +
                    "AND t1.sequenc = t2.sequenc\n");

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
    }
    private static String getTableName(boolean strand) {
        if(strand) {
            return GENOME_PLUS_TABLENAME;
        }
        return GENOME_MINUS_TABLENAME;
    }


}

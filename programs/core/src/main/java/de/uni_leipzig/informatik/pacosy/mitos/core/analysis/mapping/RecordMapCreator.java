package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.IntegerType;
import org.apache.spark.sql.types.StructField;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph.writeToGraphML;
import static org.apache.tinkerpop.gremlin.process.traversal.Operator.sum;

public class RecordMapCreator {
    protected static final Logger LOGGER = LoggerFactory.getLogger(RecordMapCreator.class);
    private DbgGraph dbgGraph;
    private int recordId;
    public static final String RECORD_SUBGRAPH_NAME = "recordSubGraph.xml";
    public static final String ANNOTATED_IDENTIFIER = "_annotated";


    public RecordMapCreator() {
        this.dbgGraph = (DbgGraph)
                new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                        startOption(LaunchExternal.LAUNCH_OPTION.VOID).build();
    }

    public RecordMapCreator(int recordId) {
        this();
        this.recordId = recordId;
    }

    private static List<StructField> createPropertyStructFields(Map<Object,Object> properties, String identifier) {
        List<StructField> structFields = new ArrayList<>();
        List<String> attributes = ((List<Object>)properties.get(identifier)).stream().
                map(p -> p.toString()).collect(Collectors.toList());
        structFields.add(DataTypes.createStructField(ColumnIdentifier.POSITION,
                DataTypes.IntegerType,false));
        structFields.add(DataTypes.createStructField(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,
                DataTypes.LongType,false));
        for(String attribute: attributes) {
            structFields.add(DataTypes.createStructField(attribute,
                    DataTypes.LongType,true));
            structFields.add(DataTypes.createStructField(attribute +
                            ANNOTATED_IDENTIFIER,
                    DataTypes.LongType,true));
        }
        return structFields;
    }

    private static Object[] getRowObjects(int position, Map<String,Map<Object,Long>> valueMap, Map<String,Map<Object,Long>> valueMapAnnotated,
                                          List<StructField> structFields, String identifier) {

        Object [] objects = new Object[structFields.size()];
        for(int i = 0; i < structFields.size(); i++) {
            StructField structField = structFields.get(i);
            if(structField.name().equals(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier)) {
                objects[i] =Long.valueOf((int)valueMap.get(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).keySet().iterator().next());
            }
            else if(structField.name().equals(ColumnIdentifier.POSITION)) {
                objects[i] = position;
            }
            else {
                if(structField.name().endsWith(ANNOTATED_IDENTIFIER)) {
                    Map<Object,Long> map = valueMapAnnotated.get(identifier);
                    if(map != null) {
                        objects[i] = valueMapAnnotated.get(identifier).get(structField.name().replaceFirst(ANNOTATED_IDENTIFIER,""));

                    }
                    else {
                        objects[i] = 0L;
                    }

                }
                else {
                    Map<Object,Long> map = valueMap.get(identifier);
                    if(map != null) {
                        objects[i] = valueMap.get(identifier).get(structField.name());
                    }
                    else {
                        objects[i] = 0L;
                    }
                }
                if(objects[i] == null) {
                    objects[i] = 0L;
                }
            }
        }
        return objects;
    }

    public static void createPropertyPostitionDataSets(GraphTraversalSource g, boolean strand, int recordId) {
        final String recordEdgeIdentifier = "e", nodeIdentifier = "o";

        // get all involved properties
        Map<Object,Object> properties = g.E().properties(
                DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier,
                DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier,
                DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier,
                DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier
        ).group().by(__.key()).by(__.value().dedup().fold()).next();
        // rename replication origin and rrna
        List<Boolean> repOriginsList = (List<Boolean>)properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier);
        if(repOriginsList != null) {
            properties.put(DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                    repOriginsList.stream().map(rep -> DbgGraph.ReplicationOrigin.getFeatureAsString(rep)).collect(Collectors.toList()));
        }
        List<Boolean> rrnasList = (List<Boolean>)properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier);
        if(rrnasList != null) {
            properties.put(DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier,
                    rrnasList.stream().map(rep -> DbgGraph.RRNA.getFeatureAsString(rep)).collect(Collectors.toList()));
        }

        List<Row> proteins = null;
        List<Row> trnas= null;
        List<Row> repOrigins = null;
        List<Row> rrnas = null;
        List<Row> others = null;
        List<StructField> proteinStructFields = null;
        List<StructField> trnasStructFields = null;
        List<StructField> rrnasStructFields = null;
        List<StructField> othersStructFields = null;
        List<StructField> repOriginStructFields = null;
        if(properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier) != null) {
            proteinStructFields = createPropertyStructFields(properties, DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier);
            proteins = new ArrayList<>();
//            SparkComputer.showStructFields(proteinStructFields);
//            System.out.println();
        }
        if(properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier) != null) {
            trnasStructFields = createPropertyStructFields(properties, DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier);
            trnas = new ArrayList<>();
//            SparkComputer.showStructFields(trnasStructFields);
//            System.out.println();
        }
        if(properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier) != null) {
            rrnasStructFields = createPropertyStructFields(properties, DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier);
            rrnas = new ArrayList<>();
//            SparkComputer.showStructFields(rrnasStructFields);
//            System.out.println();
        }
        if(properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier) != null) {
            repOriginStructFields = createPropertyStructFields(properties, DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier);
            repOrigins = new ArrayList<>();
//            SparkComputer.showStructFields(repOriginStructFields);
//            System.out.println();
        }
        if(properties.get(DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier) != null) {
            othersStructFields = createPropertyStructFields(properties, DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier);
            others = new ArrayList<>();
//            SparkComputer.showStructFields(othersStructFields);
//            System.out.println();
        }


        Map<Object,Object> otherPropertyMap = g.E().hasLabel(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier)
                .has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).as(recordEdgeIdentifier).
                group().
                by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier)).
                by(__.outV().outE().hasLabel(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier,
                        DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).
                        not(__.has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId)).
                        filter(__.otherV().as(nodeIdentifier).select(recordEdgeIdentifier).inV().where(P.eq(nodeIdentifier))).
                        properties(
                                DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier
                        ).group().by(__.key()).by(__.value().groupCount())).next();

        //System.out.println();
        //Auxiliary.printMap(otherPropertyMap);
        Map<Object,Object> recordPropertyMap = g.E().hasLabel(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).as(recordEdgeIdentifier).
                group().
                by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier)).
                by(__.
                        properties(
                                DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                                DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier
                        ).group().by(__.key()).by(__.value().groupCount())).next();
        //Auxiliary.printMap(recordPropertyMap);

        for(Map.Entry<Object,Object> entry: otherPropertyMap.entrySet()) {
            int position = (int) entry.getKey();
            Map<String,Map<Object,Long>> valueMap = (Map<String,Map<Object,Long>>)  entry.getValue();
            Map<String,Map<Object,Long>> annotatedValueMap = ((Map<String,Map<Object,Long>>)recordPropertyMap.get(position));
            if(proteins != null) {
                Row row = RowFactory.create(getRowObjects(position,valueMap,annotatedValueMap,proteinStructFields, DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN.identifier));
//                for(int rowIndex = 0; rowIndex < row.length(); rowIndex++) {
//                    System.out.print(row.get(rowIndex) +",");
//                }
//                System.out.println();
                proteins.add(row);
            }
            if(trnas != null) {
                trnas.add(RowFactory.create(getRowObjects(position,valueMap,annotatedValueMap,trnasStructFields, DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA.identifier)));
            }
            if(rrnas != null) {
                rrnas.add(RowFactory.create(getRowObjects(position,valueMap,annotatedValueMap,rrnasStructFields, DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA.identifier)));
            }
            if(repOrigins != null) {
                repOrigins.add(RowFactory.create(getRowObjects(position,valueMap,annotatedValueMap,repOriginStructFields, DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier)));
            }
            if(others != null) {
                others.add(RowFactory.create(getRowObjects(position,valueMap,annotatedValueMap,othersStructFields, DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER.identifier)));
            }
        }
        Dataset<Row> proteinSet = null;
        Dataset<Row> trnaSet = null;
        Dataset<Row> rrnaSet = null;
        Dataset<Row> repOrignSet = null;
        Dataset<Row> otherTypeSet = null;

        if(proteins != null) {
            proteinSet = Spark.getInstance().createDataFrame(proteins, DataTypes.createStructType(proteinStructFields));
//            proteinSet.show(1000);
            SparkComputer.persistDataFrame(proteinSet,ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/"
                    + DbgGraph.DBG_EDGE_ATTRIBUTES.PROTEIN);
            LOGGER.info("Persisted Proteins");
        }
        if(trnas != null) {
            trnaSet = Spark.getInstance().createDataFrame(trnas,DataTypes.createStructType(trnasStructFields));
//            trnaSet.show();
            SparkComputer.persistDataFrame(trnaSet,ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/"
                    + DbgGraph.DBG_EDGE_ATTRIBUTES.TRNA);
            LOGGER.info("Persisted Trnas");
        }
        if(rrnas != null) {
            rrnaSet = Spark.getInstance().createDataFrame(rrnas,DataTypes.createStructType(rrnasStructFields));
//            rrnaSet.show();
            SparkComputer.persistDataFrame(rrnaSet,ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/"
                    + DbgGraph.DBG_EDGE_ATTRIBUTES.RRNA);
            LOGGER.info("Persisted Rrnas");
        }
        if(repOrigins != null) {
            repOrignSet = Spark.getInstance().createDataFrame(repOrigins,DataTypes.createStructType(repOriginStructFields));
//            repOrignSet.show();
            SparkComputer.persistDataFrame(repOrignSet,ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/"
                    + DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN);
            LOGGER.info("Persisted Reporigins");
        }
        if(others != null) {
            otherTypeSet = Spark.getInstance().createDataFrame(others,DataTypes.createStructType(othersStructFields));
//            otherTypeSet.show();
            SparkComputer.persistDataFrame(otherTypeSet,ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +"/"
                    + DbgGraph.DBG_EDGE_ATTRIBUTES.OTHER);
            LOGGER.info("Persisted otherTypes");
        }


    }

    public void addMergeableRecordBranches(GraphTraversalSource g, boolean strand, List<Object> recordList) {
        final String startVertexIdentifier = "v";
        LOGGER.info("Adding mergeable branches for " + recordList.size() + " records.");
        for(Object recordId: recordList) {
            g.withSack(0).V().
                    filter(__.outE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId)).
                    as(startVertexIdentifier).
                    //no exactly one incoming vertex and outcoming vertex connected with an edge with recordId
                            filter(__.or(__.inE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).outV().dedup().count().is(P.neq(1)),
                            __.select(startVertexIdentifier).outE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                    has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().count().is(P.neq(1) )
                    )).
                    repeat(__.outE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().simplePath().
                            sack(sum).by(__.constant(1))).
                    until(__.or(__.outE().has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().count().is(P.neq(1)),
                            __.inE().has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).outV().dedup().count().is(P.neq(1)))).
                    addE(DbgGraph.DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).from(startVertexIdentifier).
                    property(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).
                    property(DbgGraph.DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier,__.sack()).iterate();

            LOGGER.info("Done with adding record " + recordId);
        }
        writeSubGraph(g,strand);
    }

    public void setDbgGraph(DbgGraph dbgGraph) {
        this.dbgGraph = dbgGraph;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public void addMergeableRecordBranches(GraphTraversalSource g, boolean strand) {


        List<Object> recordList = g.E().values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().toList();
        addMergeableRecordBranches(g,strand,recordList);
    }

    private void writeSubGraph(GraphTraversalSource g, boolean strand) {
        writeToGraphML(ProjectDirectoryManager.getRECORD_DATA_DIR(strand,recordId) +
                "/" + RECORD_SUBGRAPH_NAME, g);
    }

    public void createMinusRecordSubGraph(List<?> recordEdges) {
        final String recordEdgeIdentifier = "recordEdges";

        List<?> minusStrandRecordEdges = dbgGraph.getG().E(recordEdges).has(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier,false).id().toList();
        dbgGraph.commit();
        GraphTraversalSource minusRecordSubGraph =dbgGraph.
                createSubGraph(dbgGraph.getAllEdgesToSuffixEdgesTraversal(minusStrandRecordEdges,recordEdgeIdentifier).id().toList());
        addMergeableRecordBranches(minusRecordSubGraph,false, Collections.singletonList(recordId));
        dbgGraph.commit();
        LOGGER.info("Constructed minus strand subgraph");
        writeSubGraph(minusRecordSubGraph,false);
        LOGGER.info("Created record subgraphs");
        createPropertyPostitionDataSets(minusRecordSubGraph,false,recordId);

    }

    public void createPlusRecordSubGraph(List<?> recordEdges) {
        final String recordEdgeIdentifier = "recordEdges";
        List<?> plusStrandRecordEdges = dbgGraph.getG().E(recordEdges).has(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier,true).id().toList();
        dbgGraph.commit();
        GraphTraversalSource plusRecordSubGraph =
                dbgGraph.createSubGraph(dbgGraph.getAllEdgesToSuffixEdgesTraversal(plusStrandRecordEdges,recordEdgeIdentifier).id().toList());
        addMergeableRecordBranches(plusRecordSubGraph,true, Collections.singletonList(recordId));
        LOGGER.info("Constructed plus strand subgraph");
        writeSubGraph(plusRecordSubGraph,true);
        createPropertyPostitionDataSets(plusRecordSubGraph,true,recordId);
    }

    public void createRecordSubGraph() {
        List<?> recordEdges = dbgGraph.filterRecords(Collections.singleton(recordId));
        dbgGraph.commit();
        createPlusRecordSubGraph(recordEdges);
        createMinusRecordSubGraph(recordEdges);
    }


    public DbgGraph getDbgGraph() {
        if(dbgGraph == null) {
            dbgGraph = (DbgGraph)
                    new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                            startOption(LaunchExternal.LAUNCH_OPTION.START).build();
        }
        return dbgGraph;
    }


}

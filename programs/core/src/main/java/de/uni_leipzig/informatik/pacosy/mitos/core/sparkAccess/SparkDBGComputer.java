package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordSimilarityDT;

import java.util.List;

public class SparkDBGComputer extends SparkComputer {

    public enum DBG_PARQUET_NAMES implements PARQUET_NAMES {
        SIMILARITY_DISTRIBUTION( "recordSimilarityDistribution" + PARQUET_EXTENSION );

        public final String identifier;


        DBG_PARQUET_NAMES(String identifier) {
            this.identifier = identifier;
        }
    }

    private SparkDBGComputer() {
        super(ProjectDirectoryManager.getSTATISTICS_DIR());
    }

    public static void persistRecordSimilarities(List<RecordSimilarityDT> recordSimilarities) {
        Spark.getInstance().
                createDataFrame(recordSimilarities,RecordSimilarityDT.class).write().
                parquet(ProjectDirectoryManager.getSTATISTICS_DIR()
                        + "/" + DBG_PARQUET_NAMES.SIMILARITY_DISTRIBUTION.identifier);
    }


}

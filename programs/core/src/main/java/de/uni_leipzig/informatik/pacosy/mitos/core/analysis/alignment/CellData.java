package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import java.util.*;

public class CellData implements Comparable<CellData> {

    public enum DIRECTION {
        INVALID,
        LEFT,
        UP,
        STOP,
        DIAGONAL;

        public static String toString(DIRECTION direction) {
            switch (direction) {
                case INVALID:
                 return "x";
                case UP:
                    return "^";
                case LEFT:
                    return "<";
                case DIAGONAL:
                    return "\\";
                case STOP:
                    return "!";
            }
            return null;
        }

        public static String toString(Set<DIRECTION> directions) {
            String str = "";
            for(DIRECTION direction: directions) {
                 str += toString(direction) ;// + " ";
            }
            return str;
        }
    }


    private int score;
    private Set<DIRECTION> directions;


    public CellData(int score, DIRECTION direction) {
        this();
        this.score = score;
        addDirection(direction);
    }

    public CellData() {
        this.directions = new HashSet<>();
    }

    @Override
    public int compareTo(CellData cellData) {
        return ((Integer)this.score).compareTo(cellData.score);
    }

    public boolean discard(CellData cellData, double Xg) {
        if(this.score < cellData.score-Xg) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }
        if(object == null || object.getClass() != this.getClass()) {
            return false;
        }
        CellData cellData = (CellData) object;
        return this.score == cellData.score;
    }


    @Override
    public String toString() {
        return this.score + "(" + DIRECTION.toString(directions) + ")";
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public Set<DIRECTION> getDirection() {
        return directions;
    }

    public void setDirection(Set<DIRECTION> directions) {
        this.directions = directions;
    }

    public void addDirection(DIRECTION direction) {
        directions.add(direction);
    }

}
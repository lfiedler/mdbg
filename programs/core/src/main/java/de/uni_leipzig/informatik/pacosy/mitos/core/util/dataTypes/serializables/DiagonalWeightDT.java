package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class DiagonalWeightDT implements Serializable {
    protected int recordId;
    protected int diagonalValue;
    protected long weight;
    protected double rangeLengthMean;
    protected double inverseRangeLengthMean;
    protected int hitCount;
    protected int missCount;

    public static final Encoder<DiagonalWeightDT> ENCODER = Encoders.bean(DiagonalWeightDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        RECORDID("\"recordId\""),
        DIAGONAL_IDENTIFIER("\"diagonalValue\""),
        WEIGHT("weight");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public DiagonalWeightDT() {

    }

    public DiagonalWeightDT(int recordId, int diagonalValue, long weight) {
        this.recordId = recordId;
        this.diagonalValue = diagonalValue;
        this.weight = weight;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getDiagonalValue() {
        return diagonalValue;
    }

    public void setDiagonalValue(int diagonalValue) {
        this.diagonalValue = diagonalValue;
    }

    public long getWeight() {
        return weight;
    }

    public void setWeight(long weight) {
        this.weight = weight;
    }

    public double getRangeLengthMean() {
        return rangeLengthMean;
    }

    public void setRangeLengthMean(double rangeLengthMean) {
        this.rangeLengthMean = rangeLengthMean;
    }

    public double getInverseRangeLengthMean() {
        return inverseRangeLengthMean;
    }

    public void setInverseRangeLengthMean(double inverseRangeLengthMean) {
        this.inverseRangeLengthMean = inverseRangeLengthMean;
    }

    public int getHitCount() {
        return hitCount;
    }

    public void setHitCount(int hitCount) {
        this.hitCount = hitCount;
    }

    public int getMissCount() {
        return missCount;
    }

    public void setMissCount(int missCount) {
        this.missCount = missCount;
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class StringStringDT implements Serializable {
    protected String s1;
    protected String s2;
    public static final Encoder<StringStringDT> ENCODER = Encoders.bean(StringStringDT.class);

    public StringStringDT(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public StringStringDT() {
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        }
        if(!(o instanceof StringStringDT)) {
            return false;
        }

        StringStringDT s = (StringStringDT) o;
        return s.s1.equals(s1) && s.s2.equals(s2);
    }
}

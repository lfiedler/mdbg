package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class KmerDT implements Serializable, RecordIdInterface {

    private String kmer;
    private int recordId;
    private Integer[] positions;
    public static final Encoder<KmerDT> ENCODER = Encoders.bean(KmerDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        KMER("kmer"),
        RECORD_ID("\"recordId\""),
        POSITIONS("positions");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public KmerDT(String kmer, Integer recordId, Integer[] positions) {
        this.kmer = kmer;
        this.recordId = recordId;
        this.positions = positions;
    }

    public KmerDT() {

    }

    @Override
    public String toString() {
        String result =  kmer + " " + recordId + " [";
        for(int i = 0; i < positions.length; i++) {
            result += positions[i] + " ";
        }
        result = result.substring(0,result.length()-2) +  "]";
        return result;
    }

    public String getKmer() {
        return kmer;
    }

    public void setKmer(String kmer) {
        this.kmer = kmer;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public Integer[] getPositions() {
        return positions;
    }

    public void setPositions(Integer[] positions) {
        this.positions = positions;
    }

    public static String getKmerName() {
        return "kmer";
    }

    public static String getRecordIdName() {
        return "recordId";
    }

    public static String getPositionsName() {
        return "positions";
    }

    public static <T> Dataset<KmerDT> convert(Dataset<T> kmers) {
        return kmers.
                select(COLUMN_NAME.KMER.identifier, COLUMN_NAME.RECORD_ID.rawIdentifier(), COLUMN_NAME.POSITIONS.identifier)
                .as(ENCODER);
    }

    public static String getColumnNames() {
        return COLUMN_NAME.KMER.identifier + ","+
                COLUMN_NAME.RECORD_ID.identifier +","+
                COLUMN_NAME.POSITIONS.identifier;
    }

}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DecimalType;

import java.math.BigDecimal;
import java.math.MathContext;

public class PropertyRangeProbDT extends PropertyRangeDT {
    protected BigDecimal probability;

    protected transient BigDecimal probabilityCounter = BigDecimal.ZERO;

    public PropertyRangeProbDT() {
        this.probability = BigDecimal.ZERO;
    }

    public PropertyRangeProbDT(PropertyRangeProbDT propertyRange){
        this.property = propertyRange.property;
        this.category = propertyRange.category;
        this.start = propertyRange.start;
        this.end = propertyRange.end;
        this.length = propertyRange.length;
        this.probability = propertyRange.probability;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + probability + ")";
    }

    public static final Encoder<PropertyRangeProbDT> ENCODER = Encoders.bean(PropertyRangeProbDT.class);

    public PropertyRangeProbDT(String category, String property, int start, int end, int length, BigDecimal probability) {
        super(category, property, start, end, length);
        this.probability = probability;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;

    }

    public static Dataset<PropertyRangeProbDT> convert(Dataset<PropertyRangeDT> dataset) {
        return dataset.withColumn(ColumnIdentifier.PROBABILITY, functions.lit(1).cast(new DecimalType(38,0))).as(ENCODER);
    }

    public void addProbability(BigDecimal probability) {
        this.probability = this.probability.add(probability);
        this.probabilityCounter = this.probabilityCounter.add(BigDecimal.ONE);
    }

    public void normalizeProbability() {
        probability = probability.divide(probabilityCounter, MathContext.DECIMAL128);
    }

}

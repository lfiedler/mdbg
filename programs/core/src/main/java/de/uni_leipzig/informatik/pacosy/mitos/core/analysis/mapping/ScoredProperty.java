package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;


import org.apache.commons.collections.list.TreeList;


import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class ScoredProperty implements Comparable<ScoredProperty>, Serializable {
    private String property;
    private String category;
    private long count;
    private BigDecimal score;
    private BigDecimal normScore;
    private static double ROUNDING_THRESHOLD = 1e-5;

    public ScoredProperty() {
    }

    public ScoredProperty(ScoredProperty scoredProperty) {
        this.property = scoredProperty.property;
        this.category = scoredProperty.category;
        this.count = scoredProperty.count;
        this.score = scoredProperty.score;
        this.normScore = scoredProperty.normScore;
    }

    public ScoredProperty(String property, String category, BigDecimal score, long count) {
        this.property = property;
        this.category = category;
        this.score = score;
        this.count = count;
    }

    public static List<ScoredProperty> merge(List<ScoredProperty> properties1, List<ScoredProperty> properties2) {
//        List<ScoredProperty> properties = new TreeList();
//        for(ScoredProperty scoredProperty: properties1) {
//            properties.add(new ScoredProperty(scoredProperty));
//        }
//        for(ScoredProperty scoredProperty: properties2) {
//            int index = properties.indexOf(scoredProperty);
//            if(index > -1) {
//                ScoredProperty s = properties.get(index);
//                s.score += scoredProperty.score;
//                s.count += scoredProperty.count;
////                properties.set(index,s);
//            }
//            else {
//                properties.add(new ScoredProperty(scoredProperty));
//            }
//        }
//        setNormWeights(properties);
//        return properties;
        List<ScoredProperty> properties = new TreeList();
        for(ScoredProperty scoredProperty: properties1) {
            int index = properties2.indexOf(scoredProperty);
            if(index > -1) {
                ScoredProperty s = new ScoredProperty(properties2.get(index));
                s.score.add(scoredProperty.score);
                s.count += scoredProperty.count;
                properties.add(s);
            }

        }
        setNormWeights(properties);
        return properties;
    }

    public static void setNormWeights(List<ScoredProperty> properties) {
        Collections.sort(properties);
        BigDecimal weight = properties.stream().map(scoredProperty -> scoredProperty.score).reduce(BigDecimal.ZERO, (a, b) -> a.add(b));
        for(int i = 0; i < properties.size(); i++) {
            ScoredProperty scoredProperty = properties.get(i);
            scoredProperty.computeNormScore(weight);
            properties.set(i,scoredProperty);
        }
    }

    @Override
    public boolean equals(Object o) {

        if(o == this) {
            return true;
        }
        if(!(o instanceof ScoredProperty)) {
            return false;
        }
        ScoredProperty s = (ScoredProperty)o;
        return s.property.equals(property);
    }

    @Override
    public int compareTo(ScoredProperty scoredProperty) {
        return scoredProperty.score.compareTo(score);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String toString() {

//        return property + "(" + category + ":" + score+  "," + normScore+ "," + count+")";
        return property + "("  + score+  "," + normScore+ "," + count+")";
    }

    public BigDecimal getNormScore() {
        return normScore;
    }

    private void computeNormScore(BigDecimal totalWeight) {
        this.normScore = score.divide(totalWeight,MathContext.DECIMAL128);
    }

    public void setNormScore(BigDecimal normScore) {

        this.normScore = normScore;
//        this.normScore = score/normScore;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }


    public static boolean normedCorrectly(List<ScoredProperty> scoredProperties) {
        if(scoredProperties.size() > 1) {
            System.out.println(scoredProperties.stream().map(s -> s.normScore).reduce(BigDecimal.ZERO, (a, b) -> a.add(b,new MathContext(5))));
//            System.out.println(scoredProperties.stream().map(s -> s.normScore).reduce(BigDecimal.ZERO, (a, b) -> a.add(b)));
        }
        return scoredProperties.stream().map(s -> s.normScore).reduce(BigDecimal.ZERO, (a, b) -> a.add(b,new MathContext(5)))
                .compareTo(BigDecimal.ONE) ==0;
//        return scoredProperties.stream().map(s -> s.normScore).reduce(BigDecimal.ZERO, (a, b) -> a.add(b)).abs().subtract(BigDecimal.ONE,new MathContext(5))
//                .compareTo(BigDecimal.ZERO) ==0;
    }

    public static BigDecimal normSum(List<ScoredProperty> scoredProperties) {
        return scoredProperties.stream().map(s -> s.normScore).reduce(new BigDecimal(0), (a, b) -> a.add(b));
    }


}

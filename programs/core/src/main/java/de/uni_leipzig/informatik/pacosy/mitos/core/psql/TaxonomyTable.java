package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import com.google.common.io.Resources;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaxonomyTable implements SQLTable {

    private static TaxonomyTable taxonomyTable;
    private static final String RANK_TABELNAME = "complete.taxonomy_rank";
    private static final String DIVISION_TABELNAME = "complete.taxonomy_division";
    private static final String MERGE_TABELNAME = "complete.taxonomy_merge";
    private static final String TAXID_TABELNAME = "complete.taxonomy";

    public enum MERGE_COLUMN_NAMES {

        TAXID( "\"taxId\""),
        MERGE_TAXID("\"taxMergeId\"");

        public final String identifier;
        public final String rawIdentifier;

        MERGE_COLUMN_NAMES(String identifier) {
            this.identifier = identifier;
            this.rawIdentifier = identifier.replaceAll("\"", "");
        }
    }

    private TaxonomyTable(){

    }

    public static synchronized TaxonomyTable getInstance() {
        if(taxonomyTable == null) {
            taxonomyTable = new TaxonomyTable();
        }
        return taxonomyTable;
    }

    /****CREATION********************************************************************************/
    public void createTable() throws SQLException {
        JDBJInterface.getInstance().createTable(MERGE_TABELNAME,"  \"taxId\" bigint,\n" +
                "  \"taxMergeId\" bigint,\n" +
                "  PRIMARY KEY(\"taxId\",\"taxMergeId\")",true);
        JDBJInterface.getInstance().createTable(TAXID_TABELNAME,"  \"taxId\" bigint",true);
        JDBJInterface.getInstance().createTable(RANK_TABELNAME,"rank TEXT, PRIMARY KEY(rank)",true);
        JDBJInterface.getInstance().createTable(DIVISION_TABELNAME,"  id smallint, name TEXT, PRIMARY KEY(id)",true);
    }

    public void dropTable() throws  SQLException {
        JDBJInterface.getInstance().dropTable(MERGE_TABELNAME,true);
        JDBJInterface.getInstance().dropTable(TAXID_TABELNAME,true);
        JDBJInterface.getInstance().dropTable(RANK_TABELNAME,true);
        JDBJInterface.getInstance().dropTable(DIVISION_TABELNAME,true);
    }

    private void createTaxonomyPrimarykey() throws SQLException {
        JDBJInterface.getInstance().executeUpdate("ALTER TABLE " + TAXID_TABELNAME + " ADD PRIMARY KEY (\"taxId\")");
    }

    /****QUERIES********************************************************************************/

    public void persistMergedTaxids(List<String []> mergeTaxids) throws  SQLException {

        String values = "";
        for(int i = 0; i < mergeTaxids.size(); i++) {
            values += JDBJInterface.getInstance().parseToColumnFormat(mergeTaxids.get(i)) + ",";
        }
        values = values.substring(0,values.length()-1);

        try {
            JDBJInterface.getInstance().insertDataIntoTable(MERGE_TABELNAME,values);
        } catch (SQLException s) {
            if(s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                JDBJInterface.getInstance().insertDataIntoTable(MERGE_TABELNAME,values);
            }
            else {
                throw s;
            }
        }
    }


    public void persistTaxids(List<Integer> taxids) throws  SQLException {
        String values = "";
        try {
            for(int i = 0; i < taxids.size(); i++) {
                values += JDBJInterface.getInstance().parseToColumnFormat(taxids.get(i).toString()) + ",";
                if(i % CHUNK_SIZE == 0) {
                    values = values.substring(0,values.length()-1);
                    JDBJInterface.getInstance().insertDataIntoTable(TAXID_TABELNAME,values);
                    System.out.println(i);
                    values = "";
                }
            }
            if(values.length() != 0) {
                values = values.substring(0,values.length()-1);
                JDBJInterface.getInstance().insertDataIntoTable(TAXID_TABELNAME,values);
            }

            createTaxonomyPrimarykey();

        } catch (SQLException s) {
            if(s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                values = "";
                for(int i = 0; i < taxids.size(); i++) {
                    values += JDBJInterface.getInstance().parseToColumnFormat(taxids.get(i).toString()) + ",";
                    if(i % CHUNK_SIZE == 0) {
                        values = values.substring(0,values.length()-1);
                        JDBJInterface.getInstance().insertDataIntoTable(TAXID_TABELNAME,values);
                        System.out.println(i);
                        values = "";
                    }
                }
                if(values.length() != 0) {
                    values = values.substring(0,values.length()-1);
                    JDBJInterface.getInstance().insertDataIntoTable(TAXID_TABELNAME,values);
                }
                createTaxonomyPrimarykey();
            }
            else {
                throw s;
            }
        }

    }

    public void persistRanks(List<String> ranks) throws  SQLException {
        String values = "";
        try {
            for(int i = 0; i < ranks.size(); i++) {
                values += JDBJInterface.getInstance().parseToColumnFormat(ranks.get(i)) + ",";
            }
            values = values.substring(0,values.length()-1);
            JDBJInterface.getInstance().insertDataIntoTable(RANK_TABELNAME,values);

        } catch (SQLException s) {
            if(s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                values = "";
                for(int i = 0; i < ranks.size(); i++) {
                    values += JDBJInterface.getInstance().parseToColumnFormat(ranks.get(i)) + ",";
                }
                values = values.substring(0,values.length()-1);
                JDBJInterface.getInstance().insertDataIntoTable(RANK_TABELNAME,values);
            }
            else {
                throw s;
            }
        }
    }

    public void persistDivisions(List<String []> divisions) throws  SQLException {

        String values = "";
        for(int i = 0; i < divisions.size(); i++) {
            values += JDBJInterface.getInstance().parseToColumnFormat(divisions.get(i)) + ",";
        }
        values = values.substring(0,values.length()-1);

        try {
            JDBJInterface.getInstance().insertDataIntoTable(DIVISION_TABELNAME,values);
        } catch (SQLException s) {
            if(s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
                JDBJInterface.getInstance().insertDataIntoTable(DIVISION_TABELNAME,values);
            }
            else {
                throw s;
            }
        }
    }

    public List<Integer> getTaxIds() throws SQLException  {
        return getTaxIdsDS().collectAsList();
    }

    public Dataset<Integer> getTaxIdsDS() throws SQLException {
        Dataset<Integer> dataset = null;

        try {
            dataset = JDBJInterface.getInstance().
                    fetchQueryResult("SELECT \"taxId\" FROM " + TAXID_TABELNAME).
                    as(Encoders.INT());
        } catch (Exception e) {
            if (e.getMessage().contains("ERROR: relation \"" + TAXID_TABELNAME + "\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }
        return dataset;
    }

    public Integer getMergeTaxids(Integer taxid) throws  SQLException {


        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT "+ TaxonomyTable.MERGE_COLUMN_NAMES.MERGE_TAXID.identifier +
                            " FROM " + MERGE_TABELNAME +
                            " WHERE " + TaxonomyTable.MERGE_COLUMN_NAMES.TAXID.identifier + "=" + taxid);


            if(resultSet.next()) {
                return

                        resultSet.getInt(TaxonomyTable.MERGE_COLUMN_NAMES.MERGE_TAXID.rawIdentifier);
            }


        } catch (SQLException e) {
            if(e.getMessage().contains("ERROR: relation \"complete.record\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            }
            else {
                throw e;
            }
        }
        return null;
    }


}


package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

//TODO:
public class AlignMatrix {
//    protected static final Logger LOGGER = LoggerFactory.getLogger(AlignMatrix.class);
//
//    // default values
//    private static final int defaultXg = 100;
//    private static final double defaultOpenGapCost = 3;
//    private static final double defaultExtendGapCost = 2;
//
//    /**
//     * Alignment matrix. The ith element corresponds to the ith row.
//     * The row data is stored in a map. The key is the start column index and the List contains succesive elements.
//     * This way multiple column ranges within the row can be stored.
//     */
//    private List<Map<Integer, List<CellData>>> matrix;
//
//    private int bestPosition;
//    private CellData bestCell;
//    private final int rowCount;             // number of rows
//    private final int columnCount;             // number of columns
//
//    private double openGapCost;
//    private double extendGapCost;
//    private double gapSum;
//    private int Xg;
//
//    private ScoreMatrix scoreMatrix;
//    private Sequence s1;
//    private Sequence s2;
//
//
//
//    public AlignMatrix(Sequence s1, Sequence s2, ScoreMatrix scoreMatrix) {
//        this.s1 = s1;
//        this.s2 = s2;
//        this.scoreMatrix = scoreMatrix;
//        this.matrix = new ArrayList<>();
//        this.rowCount = s2.getSequence().length()+1;
//        this.columnCount = s1.getSequence().length()+1;
//        this.bestCell = new CellData();
//
//        openGapCost = defaultOpenGapCost;
//        extendGapCost = defaultExtendGapCost;
//        Xg = defaultXg;
//        gapSum = -openGapCost-extendGapCost;
//        bestCell = new CellData();
//        bestCell.setScore(0);
//    }
//
//    public double getOpenGapCost() {
//        return openGapCost;
//    }
//
//    public void setOpenGapCost(double openGapCost) {
//        this.openGapCost = openGapCost;
//    }
//
//    public double getExtendGapCost() {
//        return extendGapCost;
//    }
//
//    public void setExtendGapCost(double extendGapCost) {
//        this.extendGapCost = extendGapCost;
//    }
//
//    public int getXg() {
//        return Xg;
//    }
//
//    public void setXg(int xg) {
//        Xg = xg;
//    }
//
//
//    private boolean checkUpperBoundNotReached(int rowIndex) {
//        return rowIndex== 0 ? false : true;
//    }
//
//    private boolean checkLeftBoundNotReached(int columnIndex) {
//        return columnIndex== 0 ? false : true;
//    }
//
//
//    private double getAlignScore(int rowIndex, int columnIndex) {
//        return scoreMatrix.getScore(s1.getChar(columnIndex),s2.getChar(rowIndex));
//    }
//
//    private CellData setCellData(double eScore, double fScore, double currentScore) {
//        CellData cellData = new CellData();
//
//        Set<CellData.DIRECTION> directions = new HashSet<>();
//        double maxValue = 0;
//        if(eScore > fScore) {
//            if(eScore > currentScore) { // eScore is maximum of all three
//                if(eScore > maxValue) { // eScore > 0
//                    directions.add(CellData.DIRECTION.LEFT);
//                    maxValue = eScore;
//                }
//            }
//            else if(eScore < currentScore) { // currentScore is maximum of all three
//                if(currentScore > maxValue) { // currentScore > 0
//                    directions.add(CellData.DIRECTION.DIAGONAL);
//                    maxValue = currentScore;
//                }
//            }
//            else if(eScore > maxValue) { // fScore < eScore = currentScore > 0
//                    directions.add(CellData.DIRECTION.LEFT);
//                    directions.add(CellData.DIRECTION.DIAGONAL);
//                    maxValue = currentScore;
//            }
//
//        }
//        else if(eScore < fScore) {
//            if(fScore > currentScore) { // fScore is maximum of all three
//                if(fScore > maxValue) { // fScore > 0
//                    directions.add(CellData.DIRECTION.UP);
//                    maxValue = fScore;
//                }
//            }
//            else if(fScore < currentScore) {  // currentScore is maximum of all three
//                if(currentScore > maxValue) { // currentScore > 0
//                    directions.add(CellData.DIRECTION.DIAGONAL);
//                    maxValue = currentScore;
//                }
//            }
//            else if(fScore > maxValue) { // eScore < fScore = currentScore > 0
//                    directions.add(CellData.DIRECTION.UP);
//                    directions.add(CellData.DIRECTION.DIAGONAL);
//                    maxValue = currentScore;
//            }
//        }
//        else { // eScore = fScore
//            if(fScore > currentScore) { // fScore = eScore > currentScore
//                if(fScore > maxValue) {// currentScore < fScore = eScore > 0
//                    directions.add(CellData.DIRECTION.UP);
//                    directions.add(CellData.DIRECTION.LEFT);
//                    maxValue = fScore;
//                }
//            }
//            else if(fScore < currentScore) {  // currentScore is maximum of all three
//                if(currentScore > maxValue) { // currentScore > 0
//                    directions.add(CellData.DIRECTION.DIAGONAL);
//                    maxValue = currentScore;
//                }
//            }
//            else if(fScore > maxValue) { // eScore = fScore = currentScore > 0
//                directions.add(CellData.DIRECTION.UP);
//                directions.add(CellData.DIRECTION.LEFT);
//                directions.add(CellData.DIRECTION.DIAGONAL);
//                maxValue = currentScore;
//            }
//        }
//
//        if(maxValue == 0) {
//            directions.add(CellData.DIRECTION.STOP);
//        }
//        cellData.setDirection(directions);
//        cellData.setScore(maxValue);
//
//        return cellData;
//    }
//
//
//    private List<CellData> getNewRowScore() {
//        List<CellData> cellDataArrayList = new ArrayList<>();
//        CellData c = new CellData();
//        c.setDirection(Collections.singleton(CellData.DIRECTION.STOP));
//        c.setScore(0);
//        cellDataArrayList.add(c);
//        return cellDataArrayList;
//    }
//
//    private void upDateMatrix(int startValue, List<CellData> rowData ) {
//        Map<Integer, List<CellData>> rowScores = new HashMap<>();
//        rowScores.put(startValue,rowData);
//        matrix.add(rowScores);
//    }
//
//
//    public void printScores(List<Map<Integer, List<CellData>>> matrix) {
//        System.out.print("\t-\t");
//        s1.print();
//        for(int rowCounter = 0; rowCounter < matrix.size(); rowCounter++) {
//            if(rowCounter == 0) {
//                System.out.print("-\t");
//            }
//            else {
//                System.out.print(s2.getSequence().charAt(rowCounter-1) + "\t");
//            }
//
//            for(Map.Entry<Integer,List<CellData>> entry: matrix.get(rowCounter).entrySet()) {
//                for(int fillRows = 1; fillRows < entry.getKey(); fillRows++) {
//                    System.out.print("-\t");
//                }
//                for(int k = 0; k < entry.getValue().size(); k++) {
//                    CellData cellData = entry.getValue().get(k);
//                    System.out.print(cellData.getScore() +"\t");
//                }
//                for(int fillRows = entry.getKey()-1+entry.getValue().size(); fillRows < columnCount; fillRows++) {
//                    System.out.print("-\t");
//                }
//                System.out.println();
//            }
//
//        }
//    }
//
//    public void printDirections(List<Map<Integer, List<CellData>>> matrix) {
//        System.out.print("\t-\t");
//        s1.print();
//        for(int rowCounter = 0; rowCounter < matrix.size(); rowCounter++) {
//            if(rowCounter == 0) {
//                System.out.print("-\t");
//            }
//            else {
//                System.out.print(s2.getSequence().charAt(rowCounter-1) + "\t");
//            }
//
//            for(Map.Entry<Integer,List<CellData>> entry: matrix.get(rowCounter).entrySet()) {
//                for(int fillRows = 1; fillRows < entry.getKey(); fillRows++) {
//                    System.out.print("-\t");
//                }
//                for(int k = 0; k < entry.getValue().size(); k++) {
//                    CellData cellData = entry.getValue().get(k);
//                    System.out.print(CellData.DIRECTION.toString(cellData.getDirection()) +"\t");
//                }
//                for(int fillRows = entry.getKey()-1+entry.getValue().size(); fillRows < columnCount; fillRows++) {
//                    System.out.print("-\t");
//                }
//                System.out.println();
//            }
//
//        }
//    }
//
////    public void printScores(List<Map<Integer, List<CellData>>> matrix) {
////        int rowCounter = 0, columnCounter = 0;
////        for(Map<Integer,List<CellData>> m: matrix) {
////            for(Map.Entry<Integer,List<CellData>> entry: m.entrySet()) {
////                System.out.println(s2.g);
////                System.out.print(entry.getKey() + "\t");
////                for(CellData cellData: entry.getValue()) {
////                    System.out.print(cellData.getScore() +" ");
////                }
////                System.out.println();
////            }
////
////        }
////    }
//
//
//    public List<Map<Integer, List<CellData>>> align() {
//
//
//
//        // set range boundaries
//        int currentRowStart = 1; // first row starts at zero -> not possible to fall below Xg at this point
//        int currentRowStop = columnCount; //column at the end of a range where Xg constraint is violated
//
//        List<Double> fValues =
//                new ArrayList<>();
//        // get new row of scores list -> first element is cell with score=0 -> size(firstRow) = size(fValues)+1
//        List<CellData> firstRow = getNewRowScore();
//        // for i*j= 0 -> e =  -infinity, f = -infinity, h = 0
//        // e(i,j) = max {h(i,j-1)+ gapSum, e(i,j-1)-extendedGapCost}
//        // f(i,j) = max {h(i-1,j)+ gapSum, f(i-1,j)-extendedGapCost}
//        // h(i,j) = max {h(i-1,j-1)+alignScore(i,j), e(i,j), f(i,j), 0}
//        double currentScore = 0;
//        double e = Double.NEGATIVE_INFINITY;
//        double f;
//
//        // compute first row
//        for(int columnIndex = 1; columnIndex < columnCount; columnIndex++) {
//            e = Math.max(e-extendGapCost,currentScore+gapSum);
//            // f(0,j) = -infinity, h(0,j) = 0 -> f(1,j) = max{0+gapSum,-infinity} = gapSum
//            fValues.add(gapSum);
//
//            // compute cell with score h(i,j)
//            CellData cellData = setCellData(e,fValues.get(fValues.size()-1),getAlignScore(1,columnIndex));
//            currentScore = cellData.getScore();
//
//            // store results
//            firstRow.add(cellData);
//            if(cellData.discard(bestCell,Xg)) { // stop if below Xg
//                // remember where to stop
//                currentRowStop = columnIndex;
//                cellData.setDirection(Collections.singleton(CellData.DIRECTION.STOP));
//                cellData.setScore(Double.NEGATIVE_INFINITY);
//                LOGGER.info("Xg violated " +1 + ","+  columnIndex);
//                break;
//            }
//            // update best score
//            if(cellData.compareTo(bestCell) > 0) {
//                bestCell = cellData;
//                bestPosition = mapPosition(0,columnIndex);
//            }
//        }
//
//
//
//
//        List<CellData> dummyRow = new ArrayList<>();
//        CellData dummyCell = new CellData();
//        dummyCell.setDirection(Collections.singleton(CellData.DIRECTION.STOP));
//        for(int i = 0; i < firstRow.size(); i++) {
//            dummyRow.add(dummyCell);
//        }
//        // store the first row scores in the align matrix
//        upDateMatrix(currentRowStart,dummyRow);
//        upDateMatrix(currentRowStart,firstRow);
//
//        boolean belowXgBoundary = false;
//        for(int rowIndex = 2; rowIndex < rowCount; rowIndex++) {
//            // get new row of scores list -> first element is cell with score=0
//            List<CellData> rowData = getNewRowScore();
//            // row elements computed in the previous iteration
//            List<CellData> previousRowData = matrix.get(matrix.size()-1).get(currentRowStart);
//            // list to store f values of this row
//            List<Double> tempFValues = new ArrayList<>();
//
//            // remember positions where score violates Xg constraint
//            List<Integer> invalidPositions = new ArrayList<>();
//
//            e = Double.NEGATIVE_INFINITY;
//            int columnIndex = currentRowStart;
//            int indexCounter = 0;
//            belowXgBoundary = false;
//
//            // iterate through columns, starting at the currentRowStart of the previous iteration
//            // and stopping at the column before currentRowStop of last iteration
//            for(;
//                columnIndex < currentRowStop;
//                columnIndex++, indexCounter++) {
//
//                //size(previousRowData) = size(fValues)+1
//                currentScore = getAlignScore(rowIndex,columnIndex) +
//                        previousRowData.get(indexCounter).getScore();
//                e = Math.max(e-extendGapCost,rowData.get(rowData.size()-1).getScore()+gapSum);
//                f = Math.max(fValues.get(indexCounter)-extendGapCost,
//                        previousRowData.get(indexCounter+1).getScore()+gapSum);
//                CellData cellData = setCellData(e,f,currentScore);
//
//
//                if(cellData.discard(bestCell,Xg)) { // cell score violates Xg constraint -> invalidate it
//
//                    // set all values to -infinity, so this cell will never be selected in backtracking
//                    e = Double.NEGATIVE_INFINITY;
//                    f = Double.NEGATIVE_INFINITY;
//                    cellData.setScore(Double.NEGATIVE_INFINITY);
//                    cellData.setInvalid(true);
//                    cellData.setDirection(Collections.singleton(CellData.DIRECTION.INVALID));
//                    // save the index where this happened
//                    invalidPositions.add(indexCounter);
//                }
//                else if(cellData.compareTo(bestCell) > 0) { // update bestcell
//                    bestCell = cellData;
//                    bestPosition = mapPosition(rowIndex,columnIndex);
//                }
//                // store results
//                tempFValues.add(f);
//                rowData.add(cellData);
//            }
//
//
//            // if no invalid elements at the end and still columns to process
//            // -> continue expansion past currentRowStop of the last iteration
//            if(currentRowStop < columnCount && e != Double.NEGATIVE_INFINITY) {
////                indexCounter++;
////                columnIndex++;
//                // compute values at currentRowStop -> here Xg was violated in the previous iteration
//                currentScore = getAlignScore(rowIndex,columnIndex) + previousRowData.get(indexCounter).getScore();
//                e = Math.max(e-extendGapCost,rowData.get(rowData.size()-1).getScore()+gapSum);
//                f = Double.NEGATIVE_INFINITY;
//                CellData c = setCellData(e,f,currentScore);
//
//                if(c.discard(bestCell,Xg)) { // Xg constraint is violated at same position as in previous iteraion
//                    belowXgBoundary = true;
//                    LOGGER.info("Xg violated " +rowIndex + ","+  columnIndex);
//                }
//                else if(c.compareTo(bestCell) > 0) {
//                    bestCell = c;
//                    bestPosition = mapPosition(rowIndex,columnIndex);
//                }
//                // continue further if Xg constraint is not violated so far
//                if (!belowXgBoundary) {
//                    tempFValues.add(f);
//                    rowData.add(c);
//                    for(;columnIndex < columnCount; columnIndex++, indexCounter++) {
//                        // now only e is a valid option
//                        e = Math.max(e-extendGapCost,rowData.get(rowData.size()-1).getScore()+gapSum);
//                        f = Double.NEGATIVE_INFINITY;
//                        CellData cellData = new CellData();
//                        cellData.setScore(e);
//                        cellData.setDirection(Collections.singleton(CellData.DIRECTION.LEFT));
//
//                        if(cellData.discard(bestCell,Xg)) {
//                            LOGGER.info("Xg violated " +rowIndex + ","+  columnIndex);
//                            currentRowStop = columnIndex;
//                            break;
//                        }
//                        else if(cellData.compareTo(bestCell) > 0) {
//                            bestCell = cellData;
//                            bestPosition = mapPosition(rowIndex,columnIndex);
//                        }
//                        tempFValues.add(f);
//                        rowData.add(cellData);
//                    }
//                }
//
//            }
//
//            // if invalid elements at the start
//            if(invalidPositions.size() > 0 && invalidPositions.get(0) == 0) {
//
//                    // get the last of the invalid elements at the start
//                    int i;
//                    for(i = 0; i < invalidPositions.size(); i++) {
//                        if(invalidPositions.get(i) != i) {
//                            break;
//                        }
//                    }
//                    if(i == invalidPositions.size()+1) {
//                        i++;
//                    }
//                    // update rowstart to this value
//                    currentRowStart =  currentRowStart + i;
//                    // remove these elements
//                    fValues = tempFValues.subList(i,tempFValues.size());
//                    rowData = rowData.subList(i+1,rowData.size());
//            }
//            upDateMatrix(currentRowStart,rowData);
//
//        }
//        return matrix;
//
//    }
//
//
//
//    private int mapPosition(int rowIndex, int columnIndex) {
//        if(rowIndex+1 > rowCount || columnIndex+1 > columnCount) {
//            LOGGER.error("Invalid cell");
//            System.exit(-1);
//        }
//        return rowIndex* columnCount +columnIndex;
//    }
//
//    public int getRowCount() {
//        return rowCount;
//    }
//
//    public int getColumnCount() {
//        return columnCount;
//    }
//
//
//
//    public static void main(String[] args) {
//
//        Sequence s1 = new Sequence("TATCTTAACG");//new Sequence("ACTGGACT");
//        Sequence s2 = new Sequence("GATCAATTCGCA");//new Sequence("ACTGGATC");
//        AlignMatrix alignMatrix = new AlignMatrix(s1,s2,new JukesCantor());
////        alignMatrix.alignBanded(3,2);
////        List<Map<Integer, List<CellData>>> m = alignMatrix.align();
////        alignMatrix.printDirections(m);
////        System.out.println();
////        alignMatrix.printScores(m);
////        System.out.print(alignMatrix.bestCell);
//        System.exit(0);
//
//    }

}

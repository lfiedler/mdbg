package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.io.Serializable;

public class TaxonomicGroupMappingDT implements Serializable, RecordIdInterface {
    private int recordId;
    private String name;
    private String taxonomicGroupName;
    private int taxId;
    public static final Encoder<TaxonomicGroupMappingDT> ENCODER = Encoders.bean(TaxonomicGroupMappingDT.class);

    public TaxonomicGroupMappingDT() {
    }

    public TaxonomicGroupMappingDT(int recordId, String name, String taxonomicGroupName, int taxId) {
        this.recordId = recordId;
        this.name = name;
        this.taxonomicGroupName = taxonomicGroupName;
        this.taxId = taxId;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxonomicGroupName() {
        return taxonomicGroupName;
    }

    public void setTaxonomicGroupName(String taxonomicGroupName) {
        this.taxonomicGroupName = taxonomicGroupName;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public static Dataset<Row> joinWithTaxonomicGroups(Dataset<?> dataset) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);
        return dataset.join(taxonomicGroupDataset,
                taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID).
                        equalTo(dataset.col(ColumnIdentifier.RECORD_ID))
        ).drop(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID));

    }
}

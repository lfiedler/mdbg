package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class PositionPositionMappingDT implements Serializable, PositionInterface, RecordIdInterface, Comparable<PositionPositionMappingDT> {

    protected int recordId;
    protected int position;
    protected int recordPosition;
    protected boolean strand;
    public static final Encoder<PositionPositionMappingDT> ENCODER = Encoders.bean(PositionPositionMappingDT.class);

    public PositionPositionMappingDT(int recordId, int position, int recordPosition, boolean strand) {
        this.recordId = recordId;
        this.position = position;
        this.recordPosition = recordPosition;
        this.strand = strand;
    }

    public PositionPositionMappingDT() {
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setPosition(int position) {
        this.position = position;
    }

    public int getRecordPosition() {
        return recordPosition;
    }

    public void setRecordPosition(int recordPosition) {
        this.recordPosition = recordPosition;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    @Override
    public String toString() {
        return recordId + "(" + position + "," + recordPosition + ")";
    }

    @Override
    public int compareTo(PositionPositionMappingDT positionPositionMapping) {
        return  Integer.valueOf(position).compareTo(positionPositionMapping.position);
    }
}

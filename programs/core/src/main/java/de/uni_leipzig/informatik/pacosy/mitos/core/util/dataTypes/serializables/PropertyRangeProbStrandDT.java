package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;

import java.util.Objects;

public class PropertyRangeProbStrandDT extends PropertyRangeProbDT  {
    protected boolean strand;

    public static final Encoder<PropertyRangeProbStrandDT> ENCODER = Encoders.bean(PropertyRangeProbStrandDT.class);

    public PropertyRangeProbStrandDT() {
    }

    public PropertyRangeProbStrandDT(PropertyRangeProbStrandDT propertyRange){
        this.property = propertyRange.property;
        this.category = propertyRange.category;
        this.start = propertyRange.start;
        this.end = propertyRange.end;
        this.length = propertyRange.length;
        this.probability = propertyRange.probability;
        this.strand = propertyRange.strand;
    }

    public static Dataset<PropertyRangeProbStrandDT> convert(Dataset<PropertyRangeProbDT> dataset, boolean strand) {
        return dataset.withColumn(ColumnIdentifier.STRAND, functions.lit(strand).cast(DataTypes.BooleanType)).as(ENCODER);
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    @Override
    public String toString() {
        return super.toString() + " " + strand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyRangeProbStrandDT)) return false;
        if (!super.equals(o)) return false;
        PropertyRangeProbStrandDT that = (PropertyRangeProbStrandDT) o;
        return isStrand() == that.isStrand();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isStrand());
    }


    public int compareTo(PropertyRangeProbStrandDT propertyRangeDT) {
        int v = super.compareTo(propertyRangeDT);
        if(v == 0) {
            return Boolean.valueOf(this.strand).compareTo(propertyRangeDT.strand);
        }
        return v;
    }
}

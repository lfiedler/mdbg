package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.Distance;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkDBGComputer;

import de.uni_leipzig.informatik.pacosy.mitos.core.psql.PropertiesTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;


import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Pop;
import org.apache.tinkerpop.gremlin.process.traversal.Scope;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Column;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.janusgraph.core.EdgeLabel;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.core.schema.SchemaAction;
import org.janusgraph.graphdb.database.management.ManagementSystem;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.apache.tinkerpop.gremlin.process.traversal.Operator.*;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.*;

/**
 * Contains dbg graphs of multiple genomes. Each genome is associated with an integer "record".
 * For a genome with a certain "record"=r, all its edges are labeled with this value.
 * For each genome both strands are persisted. That is the genome as it is stored in a fasta file (plus strand) and its reverse compliment (minus strand).
 * Corresponding edges are labeled with a boolean "strand", where "strand"=true means the plus strand and "strand"=false means the minus strand.
 * Each edge is further labeled with an integer "pos".
 * This is the position on the plus strand of the genome, which corresponds to the nucleotide associated with this edge.
 * Positions of a genome r may range from 0 to l-1 if the genome is cyclic, or from k+1 to l-1 if it is linear and on the plus strand, or from
 * 0 to l-k if it is linear and on the minus strand.
 * Since the reverse complement is read from right to left, but positions refer to the plus strand,
 * the "pos" identifier of succeeding edges associated with the minus strand decreases.
 * For succeeding plus strand edges, the "pos" identifier increases correspondigly.
 * Each position in a genome r may be associated with one or multiple properties.
 * There is one edge labeled with the correspondig strand, type and value of each of these properties.
 * Such edges are labeled "prefix_edges". Hence there may be multiple "prefix_edges" for a given strand and position.
 * Furthermore, there is precisely one edge labeled "topology_edge" between each pair of nodes, which is connected by at least one "prefix_edge".
 * This edge is labeled with an attribute "record_count", denoting how many records have at least one "prefix_edge" between the correspondig nodes.
 * That is, even if there are multiple "prefix_edges" for a certain record, it will only contribute one to the "record_count".
 */
public class DbgGraph extends Graph {

    public enum DBG_NODE_ATTRIBUTES implements NODE_ATTRIBUTES {
        PARTITION("partition"),
        KMER("prefix");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_NODE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }

    }

    public enum DBG_EDGE_ATTRIBUTES implements EDGE_ATTRIBUTES {
        RECORD_COUNT("record_count"),
        RECORD_COUNT_PLUS("record_count_plus"),
        RECORD_COUNT_MINUS("record_count_minus"),
        RECORD("record"),
        POSITION("pos"),
        STRAND("strand"),
        PROTEIN("protein"),
        TRNA("trna"),
        REPORIGIN("repOrigin"),
        RRNA("rrna"),
        OTHER("otherType");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_EDGE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_SUBGRAPH_NODE_ATTRIBUTES implements NODE_ATTRIBUTES {
        HOP_COUNT("hop_count");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_SUBGRAPH_NODE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_SUBGRAPH_EDGE_ATTRIBUTES implements EDGE_ATTRIBUTES {

        HOP_COUNT("hop_count");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_SUBGRAPH_EDGE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_EDGE_LABEL{
        SUFFIX_EDGE("suffix_edge"),
        TOPOLOGY_EDGE("topology_edge");

        public final String identifier;


        DBG_EDGE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_SUBGRAPH_EDGE_LABEL {
        SEQUENCE_EDGE("sequence_edge"),
        MERGE_EDGE("merge_edge");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_SUBGRAPH_EDGE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_SUBGRAPH_NODE_LABEL {
        UNMATCHED_NODE("unmatched_node");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        DBG_SUBGRAPH_NODE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum DBG_NODE_LABEL{
        PREFIX_NODE("prefix_node");

        public final String identifier;


        DBG_NODE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum PROPERTIES {
        PROTEIN,
        TRNA,
        RRNA,
        REPLICATION_ORIGIN,
        OTHER;

        public static boolean contained(String identifier) {
            for(PROPERTIES names: PROPERTIES.values()) {
                if(identifier.equals(names.name())) {
                    return true;
                }

            }
            return false;
        }
    }


    private static final String MAX_PARTITION_IDENTIFIER = "maxPartitionCount";

    private static final String CURRENT_PARTITION_IDENTIFIER = "currentPartitionCount";

    private static final String PARTITION_BOUNDARY_IDENTIFIER = "partitionBoundary";

    private static final int partitionBoundary = 10000;

    private int currentPartitionCount;

    private int maxPartitionCount;

    private Map<PROPERTIES,Dataset<PropertyCountDT>> properties;

    public static String KEYSPACE = "debruijn";
    public static int K = 16;


//    private TaxonomyGraph taxonomyGraph;

    public <T> Feature<T> getFeatureType(DBG_EDGE_ATTRIBUTES attributes, T featureName) {
        Feature feature = null;
        switch (attributes) {
            case RRNA:
                feature =  new RRNA((Boolean)featureName);
                break;
            case TRNA:
                feature =  new TRNA("trn"+featureName);
                break;
            case OTHER:
                feature =  new OtherType((String)featureName);
                break;
            case PROTEIN:
                feature =  new Protein((String)featureName);
                break;
            case REPORIGIN:
                feature =  new ReplicationOrigin((Boolean)featureName);
                break;
            default:
                LOGGER.error("Invalid feature " + attributes.identifier);
        }
        return feature;
    }

    public static abstract class Feature<T> implements Comparable<Feature> {
        public T feature;
        public String category;
        private RangeDT range;
        private String strand;

        public Feature() {

        }

        public Feature(T feature) {
            this.feature = feature;
        }

        public void print() {
//            System.out.println("Category: " + category);
//            System.out.println("Feature: " + feature);
            System.out.println(this);
        }

        public String toString() {
            String s = "";
            if(range != null) {
                s += "[" +range.getStart() + "," + range.getEnd()+ "] ";
            }

            s += getFeatureAsString() + "(" + category + ")";
            if(strand != null) {
              s += " " + getStrand();
            }
            return s;
        }

        public abstract String getFeatureAsString();

        public String getAttributes() {
            return "|Category: " + category + "|Feature: " + feature;
        }

        public RangeDT getRange() {
            return range;
        }

        public void setRange(RangeDT range) {
            this.range = range;
        }

        public void setStrand(boolean strand) {
            if(strand == true) {
                this.strand = "+";
            }
            else {
                this.strand = "-";
            }
        }

        public String getStrand() {
            return strand;
        }

        public int compareTo(Feature feature) {
            return this.range.compareTo(feature.range);
        }


    }

    public static class Protein extends Feature<String> {

        public Protein() {

        }

        public Protein(String feature) {
            super(feature);
            category = "protein";
        }

        public String getFeatureAsString() {
            return feature;
        }

    }

    public static class TRNA extends Feature<String> {

        public TRNA() {

        }
        public TRNA(String feature) {
            super(feature);
            category = "trna";
        }

        public String getFeatureAsString() {
            return feature;
        }
    }

    public static class ReplicationOrigin extends Feature<Boolean> {

        public ReplicationOrigin() {

        }

        public ReplicationOrigin(Boolean feature) {
            super(feature);
            category = "repOrigin";
        }

        public String getFeatureAsString() {
            return (feature ? "OL" : "OH");
        }

        public static String getFeatureAsString(boolean feature) {
            return (feature ? "OL" : "OH");
        }
    }

    public static class RRNA extends Feature<Boolean> {

        public RRNA() {

        }

        public RRNA(Boolean feature) {
            super(feature);
            category = "rrna";
        }

        public String getFeatureAsString() {
            return (feature ? "rrnL" : "rrnS");
        }

        public static String getFeatureAsString(boolean feature) {
            return (feature ? "rrnL" : "rrnS");
        }
    }

    public static class OtherType extends Feature<String> {

        public OtherType() {

        }

        public OtherType(String feature) {
            super(feature);
            category = "otherType";
        }

        public String getFeatureAsString() {
            return feature;
        }
    }

    DbgGraph() {
        super();
    }

    @Override
    protected void initialize() {
        super.initialize();
        setLocalPartitionAttributes();
    }


    /*** Schema creation ******************************************************************************************************************************/
    public void createSchema() {


        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // vertex attributes
        final PropertyKey prefix = management.makePropertyKey(DBG_NODE_ATTRIBUTES.KMER.identifier).dataType(String.class).make();
        final PropertyKey partition = management.makePropertyKey(DBG_NODE_ATTRIBUTES.PARTITION.identifier).dataType(Integer.class).make();

        management.buildIndex(DBG_NODE_ATTRIBUTES.PARTITION.identifier, Vertex.class).addKey(partition).buildCompositeIndex();


        // edge attributes
        final PropertyKey record = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dataType(Integer.class).make();
        management.makePropertyKey(DBG_EDGE_ATTRIBUTES.POSITION.identifier).dataType(Integer.class).make();
        final PropertyKey strand = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.STRAND.identifier).dataType(Boolean.class).make();

        final PropertyKey protein = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.PROTEIN.identifier).dataType(String.class).make();
        final PropertyKey trna = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.TRNA.identifier).dataType(String.class).make();
        final PropertyKey replicationOrigin = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier).dataType(Boolean.class).make();
        final PropertyKey rrna = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RRNA.identifier).dataType(Boolean.class).make();
        final PropertyKey otherType = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.OTHER.identifier).dataType(String.class).make();
        final PropertyKey recordCount = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).dataType(Integer.class).make();

        EdgeLabel topologyLabel = management.makeEdgeLabel(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();

        String topology_index = "topology_index";
        management.buildIndex(topology_index,Edge.class).addKey(recordCount).buildMixedIndex("search");


        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
        management.buildIndex("record_index", Edge.class).addKey(record).buildCompositeIndex();
        management.buildIndex("strand_index", Edge.class).addKey(strand).buildCompositeIndex();
        management.buildIndex("protein_index", Edge.class).addKey(protein).buildCompositeIndex();
        management.buildIndex("trna_index", Edge.class).addKey(trna).buildCompositeIndex();
        management.buildIndex("repOrigin_index", Edge.class).addKey(replicationOrigin).buildCompositeIndex();
        management.buildIndex("rrna_index", Edge.class).addKey(rrna).buildCompositeIndex();
        management.buildIndex("otherType_index", Edge.class).addKey(otherType).buildCompositeIndex();


        management.makeEdgeLabel(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeVertexLabel(DBG_NODE_LABEL.PREFIX_NODE.identifier).make();

        management.commit();

        try {

            ManagementSystem.awaitGraphIndexStatus(graph,topology_index).call();
            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "record_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "strand_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "protein_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "trna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "repOrigin_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "rrna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "otherType_index").call();


        } catch (InterruptedException  i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

        openGraph();
        initPartitionBoundary();

    }

    public void createSchemaMinimal() {


        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // vertex attributes
        final PropertyKey prefix = management.makePropertyKey(DBG_NODE_ATTRIBUTES.KMER.identifier).dataType(String.class).make();
        final PropertyKey partition = management.makePropertyKey(DBG_NODE_ATTRIBUTES.PARTITION.identifier).dataType(Integer.class).make();

        management.buildIndex(DBG_NODE_ATTRIBUTES.PARTITION.identifier, Vertex.class).addKey(partition).buildCompositeIndex();


        // edge attributes
        final PropertyKey record = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dataType(Integer.class).make();
        management.makePropertyKey(DBG_EDGE_ATTRIBUTES.POSITION.identifier).dataType(Integer.class).make();
        final PropertyKey strand = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.STRAND.identifier).dataType(Boolean.class).make();

        final PropertyKey recordCountPlus = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RECORD_COUNT_PLUS.identifier).dataType(Integer.class).make();
        final PropertyKey recordCountMinus = management.makePropertyKey(DBG_EDGE_ATTRIBUTES.RECORD_COUNT_MINUS.identifier).dataType(Integer.class).make();
        EdgeLabel topologyLabel = management.makeEdgeLabel(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();

        String topology_index = "topology_index";
        management.buildIndex(topology_index + "plus",Edge.class).addKey(recordCountPlus).buildMixedIndex("search");
        management.buildIndex(topology_index + "minus",Edge.class).addKey(recordCountMinus).buildMixedIndex("search");

        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
        management.buildIndex("record_index", Edge.class).addKey(record).buildCompositeIndex();
        management.buildIndex("strand_index", Edge.class).addKey(strand).buildCompositeIndex();

        management.makeEdgeLabel(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeVertexLabel(DBG_NODE_LABEL.PREFIX_NODE.identifier).make();

        management.commit();

        try {

            ManagementSystem.awaitGraphIndexStatus(graph,topology_index+ "plus").call();
            ManagementSystem.awaitGraphIndexStatus(graph,topology_index+ "minus").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "record_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "strand_index").call();



        } catch (InterruptedException  i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

        openGraph();
        initPartitionBoundary();

    }

    public void createSchemaIndecees() {

        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // vertex attributes
        final PropertyKey prefix =
                management.getPropertyKey(DBG_NODE_ATTRIBUTES.KMER.identifier);
        final PropertyKey partition = management.getPropertyKey(DBG_NODE_ATTRIBUTES.PARTITION.identifier);

        management.buildIndex(DBG_NODE_ATTRIBUTES.PARTITION.identifier, Vertex.class).addKey(partition).buildCompositeIndex();


        // edge attributes
        final PropertyKey record = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.RECORD.identifier);
        management.getPropertyKey(DBG_EDGE_ATTRIBUTES.POSITION.identifier);
        final PropertyKey strand = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.STRAND.identifier);

        final PropertyKey protein = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.PROTEIN.identifier);
        final PropertyKey trna = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.TRNA.identifier);
        final PropertyKey replicationOrigin = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier);
        final PropertyKey rrna = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.RRNA.identifier);
        final PropertyKey otherType = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.OTHER.identifier);
        final PropertyKey recordCount = management.getPropertyKey(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier);

        EdgeLabel topologyLabel = management.getEdgeLabel(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);

        String topology_index = "topology_index";
        management.buildIndex(topology_index,Edge.class).addKey(recordCount).buildMixedIndex("search");


        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
        management.buildIndex("record_index", Edge.class).addKey(record).buildCompositeIndex();
        management.buildIndex("strand_index", Edge.class).addKey(strand).buildCompositeIndex();
        management.buildIndex("protein", Edge.class).addKey(protein).buildCompositeIndex();
        management.buildIndex("trna_index", Edge.class).addKey(trna).buildCompositeIndex();
        management.buildIndex("repOrigin_index", Edge.class).addKey(replicationOrigin).buildCompositeIndex();
        management.buildIndex("rrna", Edge.class).addKey(rrna).buildCompositeIndex();
        management.buildIndex("otherType_index", Edge.class).addKey(otherType).buildCompositeIndex();


        management.makeEdgeLabel(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeVertexLabel(DBG_NODE_LABEL.PREFIX_NODE.identifier).make();

        management.commit();

        try {

            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "record_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "strand_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "protein_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "trna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "repOrigin_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "rrna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "otherType_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph,topology_index).call();

            management = graph.openManagement();
            management.updateIndex(management.getGraphIndex("prefix_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("record_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("protein_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("trna_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("repOrigin_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("rrna_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("otherType_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex("strand_index"), SchemaAction.REINDEX).get();
            management.updateIndex(management.getGraphIndex(topology_index),SchemaAction.REINDEX).get();

            management.commit();

        } catch (InterruptedException | ExecutionException i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

        openGraph();
        initPartitionBoundary();
    }

    protected boolean schemaGenerationSuccessful() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        List<JanusGraphIndex> vertexIndecees = (List)management.getGraphIndexes(Vertex.class);
        for(JanusGraphIndex i : vertexIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        List<JanusGraphIndex> edgeIndecees = (List)management.getGraphIndexes(Edge.class);
        for(JanusGraphIndex i : edgeIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

//        RelationTypeIndex relationTypeIndex = management.getRelationIndex(management.getRelationType(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier),DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);
//        if(relationTypeIndex != null) {
//            LOGGER.info("Discovered relation index" + relationTypeIndex.name());
//            LOGGER.info("Index status: " + relationTypeIndex.getIndexStatus());
//            if(!relationTypeIndex.getIndexStatus().toString().equals("ENABLED")) {
//                LOGGER.error("Index " + relationTypeIndex.name() +" was not enabled!");
//                return false;
//            }
//        }

        LOGGER.info("All indecees were successfully generated!");
        return true;
    }

    protected boolean showIndecees() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        List<JanusGraphIndex> vertexIndecees = (List)management.getGraphIndexes(Vertex.class);
        for(JanusGraphIndex i : vertexIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        List<JanusGraphIndex> edgeIndecees = (List)management.getGraphIndexes(Edge.class);
        for(JanusGraphIndex i : edgeIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

//        RelationTypeIndex relationTypeIndex = management.getRelationIndex(management.getRelationType(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier),DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);
//        if(relationTypeIndex != null) {
//            LOGGER.info("Discovered relation index" + relationTypeIndex.name());
//            LOGGER.info("Index status: " + relationTypeIndex.getIndexStatus());
//            if(!relationTypeIndex.getIndexStatus().toString().equals("ENABLED")) {
//                LOGGER.error("Index " + relationTypeIndex.name() +" was not enabled!");
//                return false;
//            }
//        }

        LOGGER.info("All indecees were successfully generated!");
        return true;
    }

    /*** Initializing ******************************************************************************************************************************/


    // propertiesNames
    private void initPropertiesNames() {
        this.properties = new HashMap<>();
        properties.put(PROPERTIES.PROTEIN,PropertiesTable.getInstance().getProteinsDS());
        properties.put(PROPERTIES.TRNA,PropertiesTable.getInstance().getTRNAsDS());
        properties.put(PROPERTIES.RRNA,PropertiesTable.getInstance().getRRNAsDS());
        properties.put(PROPERTIES.REPLICATION_ORIGIN,PropertiesTable.getInstance().getReplicationOriginsDS());
        properties.put(PROPERTIES.OTHER,PropertiesTable.getInstance().getOtherTypesDS());
    }

    public void initPartitionBoundary() {
        graph.variables().set(PARTITION_BOUNDARY_IDENTIFIER,partitionBoundary);
        commit();
    }

    public void setPartitionAttributes() {
        graph.variables().set(MAX_PARTITION_IDENTIFIER,maxPartitionCount);
        graph.variables().set(CURRENT_PARTITION_IDENTIFIER,currentPartitionCount);

        commit();
    }

    private void setLocalPartitionAttributes() {
        if(graph.variables().get(MAX_PARTITION_IDENTIFIER).isPresent()) {
            maxPartitionCount = (int)graph.variables().get(MAX_PARTITION_IDENTIFIER).get();
            currentPartitionCount = (int)graph.variables().get(CURRENT_PARTITION_IDENTIFIER).get();
        }
        else {
            maxPartitionCount = 0;
            currentPartitionCount = 0;
            setPartitionAttributes();
        }
        LOGGER.info("Partition attributes: \nmaxPartion: " + maxPartitionCount + "\ncurrentPartitionCount: " + currentPartitionCount);
    }

    /*** persisting ******************************************************************************************************************************/
    public void updateEdges(String kmer, int pos, boolean strand, int record, List<Feature> features){

        if(features == null) {
            features = Arrays.asList();
        }

        try {

            final String featuresIdentifier = "features";
            final String v1Identifier = "v1";
            final String v2Identifier = "v2";
            final String newVertexIdentifier = "newVertex";

            currentPartitionCount += g.withSideEffect(featuresIdentifier,features)
                    .V().has(DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(0,kmer.length()-1)).fold().
                            coalesce(
                                    __.unfold(),
                                    __.addV(DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                            property(DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(0,kmer.length()-1)).
                                            property(DBG_NODE_ATTRIBUTES.PARTITION.identifier,maxPartitionCount).
                                            sideEffect(__.constant(0).store(newVertexIdentifier))).
                            as(v1Identifier).
                            coalesce(
                                    __.V().has(DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(1,kmer.length())),
                                    __.addV(DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                            property(DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(1,kmer.length())).
                                            property(DBG_NODE_ATTRIBUTES.PARTITION.identifier,maxPartitionCount).
                                            sideEffect(__.constant(0).store(newVertexIdentifier))).
                            as(v2Identifier).
                            sideEffect(
                                    __.choose(
                                            __.select(featuresIdentifier).unfold().count().is(P.eq(0)),
                                            __.addE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).
                                                    property(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                                                    property(DBG_EDGE_ATTRIBUTES.POSITION.identifier,pos).from(v1Identifier).to(v2Identifier)).
                                            select(featuresIdentifier).unfold().
                                            addE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                            property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).
                                            property(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                                            property(DBG_EDGE_ATTRIBUTES.POSITION.identifier,pos).
                                            property(__.map(t -> ((Feature)t.get()).category),
                                                    __.map(t -> ((Feature)t.get()).feature)).from(v1Identifier).to(v2Identifier)).
                            sideEffect(
                                    __.coalesce(
                                    __.inE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).filter(__.otherV().where(P.eq(v1Identifier))),
                                    __.addE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).from(v1Identifier).to(v2Identifier)).
                            property(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,__.select(v2Identifier).inE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                    filter(__.otherV().where(P.eq(v1Identifier))).
                                 values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count())).
                            cap(newVertexIdentifier).unfold().count().next();

            if(currentPartitionCount >= partitionBoundary) {
                maxPartitionCount++;
                currentPartitionCount=0;
            }
//            System.out.println(currentPartitionCount + " " + maxPartitionCount);
        } catch (Exception e) {

            e.printStackTrace();
            LOGGER.error(e.getMessage());
            System.exit(-1);
        }

    }

    public void updateEdges(List<PositionCountIdentifierDT> kmers, boolean strand, int record){



        try {

            final String recordCountIdentifier = (strand ? DBG_EDGE_ATTRIBUTES.RECORD_COUNT_PLUS.identifier :
                    DBG_EDGE_ATTRIBUTES.RECORD_COUNT_MINUS.identifier);
            final String v1Identifier = "v1";
            final String v2Identifier = "v2";
            final String newVertexIdentifier = "newVertex";

            for(PositionCountIdentifierDT kmerEntry : kmers) {
                String kmer = kmerEntry.getIdentifier();
                int pos = kmerEntry.getPosition();
                currentPartitionCount += g
                        .V().has(DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(0,kmer.length()-1)).fold().
                                coalesce(
                                        __.unfold(),
                                        __.addV(DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                                property(DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(0,kmer.length()-1)).
                                                property(DBG_NODE_ATTRIBUTES.PARTITION.identifier,maxPartitionCount).
                                                sideEffect(__.constant(0).store(newVertexIdentifier))).
                                as(v1Identifier).
                                coalesce(
                                        __.V().has(DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(1,kmer.length())),
                                        __.addV(DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                                property(DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(1,kmer.length())).
                                                property(DBG_NODE_ATTRIBUTES.PARTITION.identifier,maxPartitionCount).
                                                sideEffect(__.constant(0).store(newVertexIdentifier))).
                                as(v2Identifier).

                                sideEffect(
                                        __.addE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).
                                                        property(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                                                        property(DBG_EDGE_ATTRIBUTES.POSITION.identifier,pos).from(v1Identifier).to(v2Identifier)).
                                sideEffect(
                                        __.coalesce(
                                                __.inE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).has(recordCountIdentifier).filter(__.otherV().where(P.eq(v1Identifier))),
                                                __.addE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).from(v1Identifier).to(v2Identifier)).
                                                property(recordCountIdentifier,__.select(v2Identifier).inE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                                        has(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                                                        filter(__.otherV().where(P.eq(v1Identifier))).
                                                        values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count())).
                                cap(newVertexIdentifier).unfold().count().next();

                if(currentPartitionCount >= partitionBoundary) {
                    maxPartitionCount++;
                    currentPartitionCount=0;
                }
            }

//            System.out.println(currentPartitionCount + " " + maxPartitionCount);
        } catch (Exception e) {

            e.printStackTrace();
            LOGGER.error(e.getMessage());
            System.exit(-1);
        }

    }

    public void persistRecordFromMap( Map<Map<String,String>,List<Map<String,Object>>> kmerMap) {
        System.out.println("Starting persistence");
        Set<String> kmers = new HashSet<>();
        for(Map<String,String> pair : kmerMap.keySet()) {
            kmers.add(pair.get("from"));
            kmers.add(pair.get("to"));
        }



        //System.out.println(g.inject(kmerMap).unfold().as("m").project("from","to").by(__.select(Column.keys).select("from")).by(__.select(Column.keys).select("to")).toList());

        Map<String,Vertex> vertexList = (Map) g.V().has(DBG_NODE_ATTRIBUTES.KMER.identifier,P.within(kmers)).group().by(__.values(DBG_NODE_ATTRIBUTES.KMER.identifier)).by().next();
        kmers.removeAll(vertexList.keySet());

        vertexList.putAll( (Map) g.inject(kmers).unfold().as("x").addV(DBG_NODE_LABEL.PREFIX_NODE.identifier).
                property(DBG_NODE_ATTRIBUTES.KMER.identifier,__.select("x")).group().by(__.values(DBG_NODE_ATTRIBUTES.KMER.identifier)).by().next());

        System.out.println("Done with vertices");
        //g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m").sideEffect(__.select("v").unfold().select(Column.keys).//.select("v").select(select("m").select(Column.keys).select("to")).toList();
        //System.out.println(g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m").select("v").select("CTGT").toList());

        g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m").
                sideEffect(__.select(Column.values).unfold().as("p").
                        addE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                        property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,__.select("p").select(DBG_EDGE_ATTRIBUTES.RECORD.identifier).unfold()).
                        property(DBG_EDGE_ATTRIBUTES.POSITION.identifier,__.select("p").select(DBG_EDGE_ATTRIBUTES.POSITION.identifier).unfold()).
                        property(DBG_EDGE_ATTRIBUTES.STRAND.identifier,__.select("p").select(DBG_EDGE_ATTRIBUTES.STRAND.identifier).unfold()).
                        from(select("v").select(select("m").select(Column.keys).select("from").unfold()).unfold()).
                        to(select("v").select(select("m").select(Column.keys).select("to").unfold()).unfold()).as("e").
                        coalesce(__.select("p").select("protein").unfold().as("optional").select("e").property("protein",__.select("optional")),
                                __.select("p").select("trna").unfold().as("optional").select("e").property("trna",__.select("optional")),
                                __.select("p").select("repOrigin").unfold().as("optional").select("e").property("repOrigin",__.select("optional")),
                                __.select("p").select("rrna").unfold().as("optional").select("e").property("rrna",__.select("optional")),
                                __.select("p").select("otherType").unfold().as("optional").select("e").property("otherType",__.select("optional")),
                                __.inject(0)
                        )
                ).

                iterate();

        System.out.println("Done with edges");
//        g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m").
//                sideEffect(__.addE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
//                        from(vertexList.get())
//                from(select("v").select(select("m").select(Column.keys).select("from")))
//                        .to(select("v").select(select("m").select(Column.keys).select("to")))).iterate();
//        g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m").local(
//                __.unfold().addE("suffix_edge").property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,__.select("record")).property(DBG_EDGE_ATTRIBUTES.POSITION.identifier,__.select("pos")).property("strand",__.select("strand")).from(__.select("v").select(""))).iterate();

//        g.withSideEffect("v",vertexList).inject(kmerMap).unfold().as("m")
//                .sideEffect(
//                __.select("v").select(__.select("m").select(Column.keys).limit(Scope.local,1)).as("v1").
//                        select("v").select(__.select("m").select(Column.keys).tail(Scope.local,1)).as("v2").
//                        select("m").select(Column.values).unfold().addE("suffix_edge").from("v1").to("v2")).iterate();

//       g.inject(kmerMap).unfold().as("m").sideEffect(__.select(Column.keys).unfold().limit(Scope.local,1).as("v1")
//                        .select("m").select(Column.keys).unfold().tail(Scope.local,2).as("v2").select("v1").sideEffect(u -> System.out.println(u )));
        //l.forEach(t -> System.out.println(l));
//                .sideEffect(
//                        __.select("v").select(__.select("m").select(Column.keys).limit(Scope.local,1)).as("v1").
//                                select("v").select(__.select("m").select(Column.keys).tail(Scope.local,1)).as("v2").
//                                select("m").select(Column.values).unfold().addE("suffix_edge").from("v1").to("v2")).iterate();

//        Map m = (Map) g.withSideEffect("kmerMap",kmerMap).
//                inject(kmerMap.keySet()).
//                sideEffect(__.unfold().
//                       sideEffect(__.coalesce(__.V().has("prefix", __.map(t -> ((List<String>)t.get()).get(0))) ,
//                        __.addV("prefix_node").property("prefix", __.map(t -> ((List<String>)t.get()).get(0)))).
//                               group("m").by(__.values("prefix")).by(__.unfold())).
//                        sideEffect(__.coalesce(__.V().has("prefix", __.map(t -> ((List<String>)t.get()).get(1))) ,
//                                __.addV("prefix_node").property("prefix", __.map(t -> ((List<String>)t.get()).get(1)))).
//                                group("m").by(__.values("prefix")).by(__.unfold()))).select("m").next();
        //kmerMap.keySet().stream().forEach(t -> System.out.println( ((List<String>)t).get(0)));
//        Map m = (Map) g.inject(kmerMap.keySet()).
//                sideEffect(__.unfold().coalesce(__.V().has("prefix", __.map(t -> ((List<String>)t.get()).get(0))) ,
//                                __.addV("prefix_node").property("prefix", __.map(t -> ((List<String>)t.get()).get(0)))).
//                                group("m").by(__.values("prefix")).by(__.unfold())).select("m").next();

//        g.inject(kmerMap.keySet()).
//                unfold().coalesce(__.V().has("prefix", __.map(t -> ((List<String>)t.get()).get(0))) ,
//                        __.addV("prefix_node").property("prefix", __.map(t -> ((List<String>)t.get()).get(0)))).iterate();

//        List l = g.inject(kmerMap.keySet()).
//                unfold().map(t -> ((List<String>)t.get()).get(0)).toList();
//        l.forEach(u -> System.out.println(u));

//                List l = g.inject(kmerMap.keySet()).
//                unfold().local(__.unfold().limit(1).map(t -> t + "J")).toList();
//        l.forEach(u -> System.out.println(u));

//        g.inject(kmerMap.keySet()).
//                unfold().addV("prefix_node").property("prefix",__.map(t -> ((List<String>)t.get()).get(0))).iterate();
        //g.inject(kmerMap.keySet()).sideEffect(t -> System.out.println( ((List<String>)t.get()).get(0)) ).iterate();

        //g.inject(kmerMap.keySet()).unfold().sideEffect(t -> System.out.println( ((List<String>)t.get()).get(0) )).iterate();
        //g.withSideEffect("kmerMap",kmerMap).select("kmerMap").sideEffect(t -> System.out.println("hello")).iterate();

        g.tx().commit();
        System.out.println("Done with persistence");
        //printMap(m);

    }



    /*** deleting ******************************************************************************************************************************/
    public void removeRecords(List<Integer> records) {

        List suffixEdges = filterRecords(records);
        List nodesToCheck = g.E(suffixEdges).as("e").outV().aggregate("nodes").select("e").drop().
                select("nodes").unfold().
                toList();
        getTopologyEdgesToSuffixEdgesTraversal(suffixEdges,DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).property(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,__.select(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count()).iterate();
        g.tx().commit();
        g.V(nodesToCheck).filter(__.bothE().count().is(0)).drop().iterate();
        g.tx().commit();

        if(g.E().has("record",P.within(records)).count().next() != 0) {
            LOGGER.error("Deletion not successful due to edges!");
            System.exit(-1);
        }
    }

    public void removeRecordAttributes(int record) {
        graph.variables().remove(record+"");
    }


    /*** traversals ******************************************************************************************************************************/

    private GraphTraversal<?,?> getPropertyRecordCountMapByPositionTraversal(GraphTraversal<Edge,Edge> graphTraversal,
                                                                             DBG_EDGE_ATTRIBUTES attribute, boolean strand) {
        return graphTraversal.has(attribute.identifier).has(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                group().
                by(values(attribute.identifier)).
                by(group().
                        by(values(DBG_EDGE_ATTRIBUTES.POSITION.identifier)).
                        by(values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count()).
                        order(Scope.local).by(Column.keys));
    }

    private GraphTraversal<?,?> getPropertySetUnWrappedTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes) {
        return graphTraversal.properties(attributes).group().by(__.key()).by(__.value().dedup().fold());
    }

    private GraphTraversal<?,?> getPropertySetUnWrappedTraversal(GraphTraversal<?,?> graphTraversal) {
        return graphTraversal.properties().group().by(__.key()).by(__.value().dedup().fold());
    }

    private GraphTraversal<?,?> getPropertySetTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes) {
        return graphTraversal.by(__.properties(attributes).group().by(__.key()).by(__.value().dedup().fold()));
    }

    private GraphTraversal<?,?> getPropertySetTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes, String identifier) {
        return graphTraversal.by(__.select(identifier).properties(attributes).group().by(__.key()).by(__.value().dedup().fold()));
    }
    private GraphTraversal<?,?> getPropertySetTraversal(GraphTraversal<?,?> graphTraversal) {
        return graphTraversal.by(__.properties().group().by(__.key()).by(__.value().dedup().fold()));
    }

    private GraphTraversal<?,?> getPropertySetTraversal(GraphTraversal<?,?> graphTraversal, String identifier) {
        return graphTraversal.by(__.select(identifier).properties().group().by(__.key()).by(__.value().dedup().fold()));
    }

    private GraphTraversal<?,?> getPropertyDistributionUnWrappedTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes) {
        return graphTraversal.properties(attributes).group().by(__.key()).by(__.value().groupCount());
    }

    private GraphTraversal<?,?> getPropertyDistributionUnWrappedTraversal(GraphTraversal<?,?> graphTraversal) {
        return graphTraversal.properties().group().by(__.key()).by(__.value().groupCount());
    }

    private GraphTraversal<?,?> getPropertyDistributionTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes) {
        return graphTraversal.by(__.properties(attributes).group().by(__.key()).by(__.value().groupCount()));
    }

    private GraphTraversal<?,?> getPropertyDistributionTraversal(GraphTraversal<?,?> graphTraversal, String[] attributes, String identifier) {
        return graphTraversal.by(__.select(identifier).properties(attributes).group().by(__.key()).by(__.value().groupCount()));
    }

    private GraphTraversal<?,?> getPropertyDistributionTraversal(GraphTraversal<?,?> graphTraversal) {
        return graphTraversal.by(__.properties().group().by(__.key()).by(__.value().groupCount().fold()));
    }

    private GraphTraversal<?,?> getPropertyDistributionTraversal(GraphTraversal<?,?> graphTraversal, String identifier) {
        return graphTraversal.by(__.select(identifier).properties().group().by(__.key()).by(__.value().groupCount()));
    }


    /**
     *
     * @param edgeIds Ids of suffix edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @param labels List of labels to consider for result edges
     * @return All edges with label within labels which are part of the same nodepair as provided suffix edges
     */
    private GraphTraversal<?,?> getLabeledEdgesToSuffixEdgesTraversal(List edgeIds, String edgeIdentifier, String [] labels) {
        return g.E(edgeIds).as(edgeIdentifier).outV().outE(labels).
                filter(__.otherV().as("o").select(edgeIdentifier).inV().where(P.eq("o")));
    }

    /**
     *
     * @param edgeIds Ids of suffix edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return All edges part of the same nodepair as provided suffix edges
     */
    public GraphTraversal<?,?> getAllEdgesToSuffixEdgesTraversal(List edgeIds, String edgeIdentifier) {
        return getLabeledEdgesToSuffixEdgesTraversal(edgeIds,edgeIdentifier,
                new String[] {DBG_EDGE_LABEL.SUFFIX_EDGE.identifier,DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier});
    }

    /**
     *
     * @param edgeIds Ids of suffix edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return All topology edges which are part of the same nodepair as provided suffix edges
     */
    private GraphTraversal<?,?> getTopologyEdgesToSuffixEdgesTraversal(List edgeIds, String edgeIdentifier) {
        return getLabeledEdgesToSuffixEdgesTraversal(edgeIds,edgeIdentifier,
                new String[]{DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier});
    }

    /**
     *
     * @param edgeIds Ids of topology edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return
     */
    private GraphTraversal<?,?> getSuffixEdgesToTopologyEdgesTraversal(List edgeIds, String edgeIdentifier) {
        return g.E(edgeIds).as(edgeIdentifier).outV().outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                filter(__.otherV().as("o").select(edgeIdentifier).inV().where(P.eq("o")));
    }

    /**
     *
     * @param edgeIds Ids of topology edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return
     */
    private GraphTraversal<?,?> getNodePairsToEdgesTraversal(List edgeIds, String edgeIdentifier) {
        return g.E(edgeIds).as(edgeIdentifier).local(__.union(__.outV().id(),__.inV().id()).fold()).dedup();
    }

    /**
     *
     * @param graphTraversal Edge Traversal start
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return
     */
    private GraphTraversal<?,?> getNodePairsToEdgesTraversal(GraphTraversal<Edge,Edge> graphTraversal, String edgeIdentifier) {
        return graphTraversal.as(edgeIdentifier).local(__.union(__.outV().id(),__.inV().id()).fold()).dedup();
    }



    /**
     * First part of group traversal: Fetch Suffix edges and group by provided topology edges
     * @param edgeIds Ids of topology edges
     * @return
     */
    private GraphTraversal<?,?> getSuffixEdgesToTopologyEdgesGroupTraversal(List edgeIds) {
        final String edgeIdentifier = "topology_Edge";
        return getSuffixEdgesToTopologyEdgesTraversal(edgeIds,edgeIdentifier).
                group().by(__.select(edgeIdentifier).id());
    }

    /**
     * First part of group traversal: Fetch associated topology edges and group by these
     * @param edgeIds Ids of suffix edges
     * @return
     */
    private GraphTraversal<?,?> getTopologyEdgesToSuffixEdgesGroupTraversal(List edgeIds) {
        final String edgeIdentifier = DBG_EDGE_LABEL.SUFFIX_EDGE.identifier;
        return  getTopologyEdgesToSuffixEdgesTraversal(edgeIds,edgeIdentifier).group().by(__.id());

    }

    /**
     * First part of group traversal: Group by associated nodepairs
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds Ids of edges
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return
     */
    private GraphTraversal<?,?> getNodePairsToEdgesGroupTraversal(List edgeIds, String edgeIdentifier) {
        return g.E(edgeIds).as(edgeIdentifier).
                sack(assign).by(__.outV().id().fold()).
                sack(addAll).by(__.inV().id().fold()).
                group().by(__.sack());
    }


    /**
     * First part of group traversal: Group by associated nodepairs
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param graphTraversal Edge Traversal start
     * @param edgeIdentifier Identifier for edgeIds in traversal
     * @return
     */
    private GraphTraversal<?,?> getNodePairsToEdgesGroupTraversal(GraphTraversal<Edge,Edge> graphTraversal, String edgeIdentifier) {
        return graphTraversal.as(edgeIdentifier).
                sack(assign).by(__.outV().id().fold()).
                sack(addAll).by(__.inV().id().fold()).
                group().by(__.sack());
    }

    private GraphTraversal<?,?> getCommonNodePairsForRecordsTraversal(List<Integer> recordIds, int minMatch) {
        return  getNodePairsToEdgesGroupTraversal(filterRecords(recordIds),DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                by(__.values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().fold()).
                unfold().
                filter(__.select(Column.values).count(Scope.local).is(P.gte(minMatch)));
    }


    private GraphTraversal<?,?> getRecordCountDistributionTraversal(GraphTraversal<Edge,Edge> graphTraversal) {
        return graphTraversal.groupCount().by(DBG_EDGE_ATTRIBUTES.RECORD.identifier);
    }

    public static void getMergeableRecordBranchesTraversal(GraphTraversalSource graphTraversalSource,
                                                             int recordId) {
//        graphTraversalSource.withSack(0).
//                V().
//                filter(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId)).
//                filter(__.inE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).count().is(P.eq(0))).
//                id().toList().forEach(i -> System.out.println(i));
        List<Vertex> startVerticees = graphTraversalSource.
                V().
//                // at least one outgoing edge with recordId
                filter(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId)).
                        as("v").
                        //no exactly one incoming vertex and outcoming vertex connected with an edge with recordId
                filter(__.or(__.inE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).outV().dedup().count().is(P.neq(1)),
                                __.select("v").outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().count().is(P.neq(1) )
                        )).
                toList();

        LOGGER.info("Startverticees: " + startVerticees.size() + " for " + recordId);
        int counter = startVerticees.size();
//        Collection<List<Vertex>> partitions = Auxiliary.partion(startVerticees,5);
        for(Vertex v: startVerticees) {
//            System.out.println(v.id());
//            graphTraversalSource.withSack(0).V(v).
//                repeat(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().simplePath().as(v2Identifier).sack(sum).by(__.constant(1))).
//                emit().
//                    until(__.inE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).outV().dedup().count().is(P.gt(1))).
////                filter(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
////                        has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).count().is(P.eq(0))).
//                    select(v2Identifier).
//                                addE(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).from(v).
//                    property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).
//                    property(DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier,__.sack()).iterate();
            graphTraversalSource.withSack(0).V(v).
                    repeat(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().simplePath().
                            sack(sum).by(__.constant(1))).
                    until(__.or(__.outE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).inV().dedup().count().is(P.neq(1)),
                            __.inE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).outV().dedup().count().is(P.neq(1)))).
                    addE(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).from(v).
                    property(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).
                    property(DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier,__.sack()).iterate();
            System.out.println(counter+"");
            counter--;
        }



    }

    public static GraphTraversal<?,?> getMergeableBranchesTraversal(GraphTraversalSource graphTraversalSource,
                                                             String v1Identifier, String v2Identifier) {
        return graphTraversalSource.withSack(0).V().
                filter(__.or(__.inE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.neq(1)),
                        __.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.neq(1)))).
                as(v1Identifier).
                repeat(__.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).inV().simplePath().sack(sum).by(__.constant(1))).
                until(__.or(__.inE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.neq(1)),
                        __.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.neq(1)))).
                as(v2Identifier);

//        return graphTraversalSource.withSack(0).
//                V().filter(__.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.gt(1))).as(v1Identifier).
//                repeat(__.out().as(v2Identifier).sack(sum).by(__.constant(1))).
//                until(__.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.gt(1)));
    }

    public GraphTraversal<?,?> getMergeableBranchesTraversal(GraphTraversalSource graphTraversalSource, String v1Identifier, String v2Identifier, int minHopCount) {

//        return graphTraversalSource.withSack(0).
//                V().filter(__.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.gt(1))).as(v1Identifier).
//                repeat(__.out().as(v2Identifier).sack(sum).by(__.constant(1))).
//                until(__.outE(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).count().is(P.gt(1))).
//                filter(__.sack().is(P.gte(minHopCount)));
        return getMergeableBranchesTraversal(graphTraversalSource,v1Identifier,v2Identifier).
                filter(__.sack().is(P.gte(minHopCount)));
    }

    public GraphTraversal<?,?> getMergedBranchesDistributionTraversal(GraphTraversalSource graphTraversalSource) {
        return graphTraversalSource.E().hasLabel(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).
                groupCount().by(__.values(DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier));
    }

    /*** Maps for edges ******************************************************************************************************************************/

    /**
     * Get a duplicate free list of nodepairs for provided edgeIds.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds
     * @return
     */

    /**
     * @param nodePairs Set of nodePairs
     * @return All topology edges which are part of the same nodepair as provided via kPlusOneMers
     * Removes all kPlusOneMers that matched
     */
    public <T extends NodePairDT> List<?> getLabelEdgesToNodePairs(Collection<T> nodePairs, String... labels) {
        List<Object> topologyEdgeIdList = new ArrayList<>();

        Iterator<T> kmerIterator = nodePairs.iterator();
        while (kmerIterator.hasNext()) {
            T nodePair = kmerIterator.next();
            Optional<Object> topologyEdgeId =
                    g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, nodePair.getV1()).
                            outE(labels).
                            filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, nodePair.getV2())).id().
                            tryNext();
            if (topologyEdgeId.isPresent()) {
                System.out.println("remove");
                topologyEdgeIdList.add(topologyEdgeId.get());
                kmerIterator.remove();
            }
        }
        System.out.println("in db " + nodePairs.size());
        return topologyEdgeIdList;
    }

    /**
     *
     * @param kPlusOneMers Set of kplusOneMers
     * @return All topology edges which are part of the same nodepair as provided via kPlusOneMers
     * Removes all kPlusOneMers that matched
     */
    public List<?> getLabelEdgesToKplusOneMers(Collection<String> kPlusOneMers, String... labels) {
        List<Object> topologyEdgeIdList = new ArrayList<>();
        Iterator<String> kPlusOneMerIterator = kPlusOneMers.iterator();
        while (kPlusOneMerIterator.hasNext()) {
            String kPlusOneMer = kPlusOneMerIterator.next();
            String prefix = kPlusOneMer.substring(0, kPlusOneMer.length() - 1);
            String suffix = kPlusOneMer.substring(1);
            Optional<Object> topologyEdgeId =
                    g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, prefix).
                            outE(labels).
                            filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, suffix)).id().
                            tryNext();
            if (topologyEdgeId.isPresent()) {

                topologyEdgeIdList.add(topologyEdgeId.get());
                kPlusOneMerIterator.remove();
            }
        }

        return topologyEdgeIdList;
    }

    /**
     *
     * @param kPlusOneMers Set of kplusOneMers
     * @return All topology edges which are part of the same nodepair as provided via kPlusOneMers
     * Removes all kPlusOneMers that matched
     */
    public List<?> getTopologyEdgesToKplusOneMers(Collection<String> kPlusOneMers) {

        return getLabelEdgesToKplusOneMers(kPlusOneMers,DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);
    }

    /**
     * @param nodePairs Set of nodePairs
     * @return All topology edges which are part of the same nodepair as provided via kPlusOneMers
     * Removes all kPlusOneMers that matched
     */
    public <T extends NodePairDT> List<?> getTopologyEdgesToNodePairs(Collection<T> nodePairs) {
        return getLabelEdgesToNodePairs(nodePairs, DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);
    }

    /**
     *
     * @param kPlusOneMers Set of kplusOneMers
     * @return All edges which are part of the same nodepair as provided via kPlusOneMers
     */
    public List<?> getAllEdgesToKplusOneMers(Collection<String> kPlusOneMers) {
        return getLabelEdgesToKplusOneMers(kPlusOneMers,
                DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier,
                DBG_EDGE_LABEL.SUFFIX_EDGE.identifier);
    }

    public List getNodePairsToEdges(List edgeIds) {
        return getNodePairsToEdgesTraversal(edgeIds,"edges").toList();
    }

    /**
     * Get Map where provided topology edge ids are key and list of corresponding suffix edge ids are values
     * @param edgeIds Ids of topology edges to involve
     * @return
     */
    public Map<Object,Object> getSuffixEdgeMapToTopologyEdges(List edgeIds) {
        return (Map<Object,Object>) getSuffixEdgesToTopologyEdgesGroupTraversal(edgeIds).
                by(__.id().fold()).next();
    }

    /**
     * Get Map where associated topology edge ids are key and list of provided corresponding suffix edge ids are values
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object,Object> getSuffixEdgeMapToSuffixEdges(List edgeIds) {
        return (Map<Object,Object>) getTopologyEdgesToSuffixEdgesGroupTraversal(edgeIds).
                by(__.select(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).id().fold()).next();
    }

    /**
     * Get Map where nodepairs are key and list of provided corresponding suffix edge ids are values
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object,Object> getNodePairMapToSuffixEdges(List edgeIds) {
        return (Map<Object,Object>) getNodePairsToEdgesGroupTraversal(edgeIds,DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                by(__.id().fold()).next();
    }

    /**
     * Get list of suffix edgeIds corresponding to the provided topology edgeIds
     * @param edgeIds Ids of topology edges to involve
     * @return
     */
    public List getSuffixEdgesToTopologyEdges(List edgeIds) {
        return getSuffixEdgesToTopologyEdgesTraversal(edgeIds,DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                dedup().id().toList();
    }

    /**
     * Get list of topology edgeIds corresponding to the provided suffix edgeIds
     * @param edgeIds Ids of suffix edges
     * @return
     */
    public List getTopologyEdgesToSuffixEdges(List edgeIds) {
        return getTopologyEdgesToSuffixEdgesTraversal(edgeIds,"topology_Edge").id().toList();
    }

    /**
     * Get Map where provided topology edge ids are key and property distribution of the corresponding suffix edges are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * @param edgeIds Ids of topology edges to involve
     * @return
     */
    public Map<Object,Object> getTopologyEdgesPropertyDistributionsMapForTopologyEdges(List edgeIds) {
        return (Map<Object,Object>)
                getPropertyDistributionTraversal(
                getSuffixEdgesToTopologyEdgesGroupTraversal(edgeIds)).
                        next();
    }

    /**
     * Get Map where provided topology edge ids are key and property distribution of the corresponding suffix edges are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * @param edgeIds Ids of topology edges to involve
     * @return
     */
    public Map<Object,Object> getTopologyEdgesPropertyDistributionsMapForTopologyEdges(List edgeIds, String[] attributes) {
        return (Map<Object,Object>)
                getPropertyDistributionTraversal(
                        getSuffixEdgesToTopologyEdgesGroupTraversal(edgeIds),attributes).
                        next();
    }

    /**
     * Get Map where provided topology edge ids are key and property sets of the corresponding suffix edges are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * @param edgeIds Ids of topology edges to involve
     * @return
     */
    public Map<Object,Object> getTopologyEdgesPropertySetsMapForTopologyEdges(List edgeIds) {
        return (Map<Object,Object>)
                getPropertySetTraversal(
                getSuffixEdgesToTopologyEdgesGroupTraversal(edgeIds)).
                        next();
    }

    /**
     * Get Map where provided topology edge ids are key and property sets of the corresponding suffix edges are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * @param edgeIds Ids of topology edges to involve
     * @param attributes Properties to include
     * @return
     */
    public Map<Object,Object> getTopologyEdgesPropertySetsMapForTopologyEdges(List edgeIds, String[] attributes) {
        return (Map<Object,Object>)
                getPropertySetTraversal(
                        getSuffixEdgesToTopologyEdgesGroupTraversal(edgeIds),attributes).
                        next();
    }

    /**
     * Get Map where associated topology edge ids are key and property distribution of the provided suffix edges contributing to this topology edge are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object, Object> getTopologyPropertyDistributionsMapForSuffixEdges(List edgeIds) {

        return (Map<Object,Object>)
                getPropertyDistributionTraversal(
                        getTopologyEdgesToSuffixEdgesGroupTraversal(edgeIds),DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                next();
                //getPropertyDistributionTraversal (getNodePairsToEdgesTraversal(edgeIds)).next();
    }

    /**
     * Get Map where associated topology edge ids are key and property distribution of the provided suffix edges contributing to this topology edge are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * @param edgeIds Ids of suffix edges to involve
     * @param attributes Properties to include
     * @return
     */
    public Map<Object, Object> getTopologyPropertyDistributionsMapForSuffixEdges(List edgeIds, String[] attributes) {

        return (Map<Object,Object>)
                getPropertyDistributionTraversal(
                        getTopologyEdgesToSuffixEdgesGroupTraversal(edgeIds),attributes, DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                        next();
        //getPropertyDistributionTraversal (getNodePairsToEdgesTraversal(edgeIds)).next();
    }

    /**
     * Get Map where associated topology edge ids are key and property sets of the provided suffix edges contributing to this topology edge are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object, Object> getTopologyPropertySetsMapForSuffixEdges(List edgeIds) {
        return (Map<Object,Object>)
                getPropertySetTraversal(
                        getTopologyEdgesToSuffixEdgesGroupTraversal(edgeIds),DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                        next();
    }

    /**
     * Get Map where associated topology edge ids are key and property sets of the provided suffix edges contributing to this topology edge are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * @param edgeIds Ids of suffix edges to involve
     * @param attributes Properties to include
     * @return
     */
    public Map<Object, Object> getTopologyPropertySetsMapForSuffixEdges(List edgeIds, String[] attributes) {
        return (Map<Object,Object>)
                getPropertySetTraversal(
                        getTopologyEdgesToSuffixEdgesGroupTraversal(edgeIds),attributes,DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                        next();
    }

    /**
     * Get Map where associated nodepairs are key and property distribution of the provided suffix edges contributing to this topology edge are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object, Object> getNodepairPropertyDistributionsMapForSuffixEdges(List edgeIds) {
        final String edgeIdentifier = DBG_EDGE_LABEL.SUFFIX_EDGE.identifier;
        return (Map<Object,Object>)
                getPropertyDistributionTraversal (
                        getNodePairsToEdgesGroupTraversal(edgeIds,edgeIdentifier),edgeIdentifier).
                        next();
    }

    /**
     * Get Map where associated nodepairs are key and property distribution of the provided suffix edges contributing to this topology edge are values.
     * A property distribution is a map where property idenfiers are keys and values are a map with property values as key and count as value.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds Ids of suffix edges to involve
     * @param attributes Properties to include
     * @return
     */
    public Map<Object, Object> getNodepairPropertyDistributionsMapForSuffixEdges(List edgeIds, String[] attributes) {
        final String edgeIdentifier = DBG_EDGE_LABEL.SUFFIX_EDGE.identifier;
        return (Map<Object,Object>)
                getPropertyDistributionTraversal (
                        getNodePairsToEdgesGroupTraversal(edgeIds,edgeIdentifier),attributes,edgeIdentifier).
                        next();
    }

    /**
     * Get Map where associated nodepairs are key and property sets of the provided suffix edges contributing to this topology edge are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds Ids of suffix edges to involve
     * @return
     */
    public Map<Object, Object> getNodepairPropertySetsMapForSuffixEdges(List edgeIds) {
        final String edgeIdentifier = DBG_EDGE_LABEL.SUFFIX_EDGE.identifier;
        return (Map<Object,Object>)
                getPropertySetTraversal (
                        getNodePairsToEdgesGroupTraversal(edgeIds,edgeIdentifier),edgeIdentifier).
                        next();
    }

    /**
     * Get Map where associated nodepairs are key and property sets of the provided suffix edges contributing to this topology edge are values.
     * A property set is a map where property idenfiers are keys and values are the set of associated property values.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param edgeIds Ids of suffix edges to involve
     * @param attributes Properties to include
     * @return
     */
    public Map<Object, Object> getNodepairPropertySetsMapForSuffixEdges(List edgeIds, String[] attributes) {
        final String edgeIdentifier = DBG_EDGE_LABEL.SUFFIX_EDGE.identifier;
        return (Map<Object,Object>)
                getPropertySetTraversal (
                        getNodePairsToEdgesGroupTraversal(edgeIds,edgeIdentifier),attributes,edgeIdentifier).
                        next();
    }

    /**
     * Fetch all suffix edges that correspond to topology edges of minRecordCount record_count and compute
     * a map for these where attribute identifiers (rrna, protein, trna, reporgin, othertype and recordId) are key
     * and associated countdistributions are values.
     * @param minRecordCount Minimum value record_count has to have for topology edges to be considered
     * @return
     */
    public Map<String, Map<Object,Long>> getRecordMinCountFilteredAttributesDistribution(int minRecordCount) {

        String [] attributes = new String[] {
                DBG_EDGE_ATTRIBUTES.RECORD.identifier,
                DBG_EDGE_ATTRIBUTES.PROTEIN.identifier,
                DBG_EDGE_ATTRIBUTES.TRNA.identifier,
                DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                DBG_EDGE_ATTRIBUTES.RRNA.identifier        };
        return getRecordMinCountFilteredAttributesDistribution(minRecordCount,attributes);
    }

    /**
     * Fetch all suffix edges that correspond to topology edges of minRecordCount record_count and compute
     * a map for these where provided attribute identifiers are key
     * and associated countdistributions are values.
     * @param minRecordCount Minimum value record_count has to have for topology edges to be considered
     * @param attributes Attribute identifiers to consider
     * @return
     */
    public Map<String, Map<Object,Long>> getRecordMinCountFilteredAttributesDistribution(int minRecordCount, String [] attributes) {
        final String topologyEdgeIdentifier = "topology_edge";

        return (Map<String, Map<Object,Long>>) getPropertyDistributionUnWrappedTraversal(
                getSuffixEdgesToTopologyEdgesTraversal(
                        g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gte(minRecordCount)).toList(),
                        topologyEdgeIdentifier),
                attributes
        ).next();
    }

    /**
     * Fetch all suffix edges that correspond to topology edges of minRecordCount record_count and compute
     * a map for these where provided attribute identifiers are key
     * and associated countdistributions are values.
     * @param minRecordCount Minimum value record_count has to have for topology edges to be considered
     * @param splitBoundary Conduct query only for splitBoundary many topology edges at a time, merge maps in the end
     * @param attributes Attribute identifiers to consider
     * @return
     */
    public Map<String, Map<Object,Long>> getRecordMinCountFilteredAttributesDistribution(int minRecordCount, int splitBoundary, String [] attributes) {
        final String topologyEdgeIdentifier = "topology_edge";
        int startIndex = 0;
        List edges = g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gte(minRecordCount)).toList();
        Map<String, Map<Object,Long>> map = new HashMap<>();

        while(startIndex < edges.size()) {
            Map<String, Map<Object,Long>> tempMap;
            List subList = null;
            if(startIndex+splitBoundary < edges.size()) {
                subList = edges.subList(startIndex,startIndex+splitBoundary);
            }
            else {
                subList = edges.subList(startIndex,edges.size());
            }
//            System.out.println("sublist: "+ subList.size());
            tempMap = (Map<String, Map<Object,Long>>) getPropertyDistributionUnWrappedTraversal(
                    getSuffixEdgesToTopologyEdgesTraversal(
                            subList,
                            topologyEdgeIdentifier),
                    attributes
            ).next();
//            System.out.println(tempMap);
            for(String attribute: attributes) {
                if(!tempMap.containsKey(attribute)) {
                    continue;
                }
                if(!map.containsKey(attribute)) {
                    map.put(attribute,tempMap.get(attribute));
                    continue;
                }
                Auxiliary.mergeDistributions(
                        tempMap.get(attribute),
                        map.get(attribute)
                        );
            }
//            System.out.println(map);

            startIndex += splitBoundary;
        }

        return map;

    }

    public Map<String, Map<Object,Long>> getRecordCountFilteredAttributesDistribution(int recordCount, String [] attributes) {
        final String topologyEdgeIdentifier = "topology_edge";

        return (Map<String, Map<Object,Long>>) getPropertyDistributionUnWrappedTraversal(
                getSuffixEdgesToTopologyEdgesTraversal(
                        g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.eq(recordCount)).toList(),
                        topologyEdgeIdentifier),
                attributes
        ).next();
    }

    /**
     * Get a Map where attribute values are keys and a sorted Map are values.
     * The sorted map has position as key and corresponding record counts as value.
     * @param g SubGraph to conduct query on
     * @param attribute
     * @param strand
     * @return
     */
    public Map<Object,Object> getPropertyRecordCountMapByPosition(GraphTraversalSource g, DBG_EDGE_ATTRIBUTES attribute, boolean strand) {
        return (Map<Object,Object>) getPropertyRecordCountMapByPositionTraversal(g.E(),attribute,strand).next();
    }

    /**
     * Get a Map where attribute values are keys and a sorted Map are values.
     * The sorted map has position as key and corresponding record counts as value.
     * @param g SubGraph to conduct query on
     * @param attribute
     * @param strand
     * @param recordIds Only consider edges with records within recordIds
     * @return
     */
    public Map<Object,Object> getPropertyRecordCountMapByPosition(GraphTraversalSource g, DBG_EDGE_ATTRIBUTES attribute, boolean strand, List<Integer> recordIds) {
        return (Map<Object,Object>) getPropertyRecordCountMapByPositionTraversal(
                g.E().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,P.within(recordIds)),
                attribute,
                strand).
                next();
    }




    public List getMergeableBranches(GraphTraversalSource graphTraversalSource) {

        final String v1Identifier = "v1";
        final String v2Identifier = "v2";
        return getMergeableBranchesTraversal(graphTraversalSource,v1Identifier,v2Identifier).
                local(__.union(__.select(v1Identifier,v2Identifier),__.sack()).fold()).toList();
    }

    public static void addMergeableBranches(GraphTraversalSource graphTraversalSource) {

        final String v1Identifier = "v1";
        final String v2Identifier = "v2";

        getMergeableBranchesTraversal(graphTraversalSource,v1Identifier,v2Identifier).
                addE(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).from("v1").to("v2").
                property(DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier,__.sack()).iterate();
    }



    public static Map<Object,Object> getMergedRecordEdgeProperties(GraphTraversalSource g, int recordId, String[] propertyKeys) {
        final String edgeIdentifier = "e", recordEdgeIdentier = "r";
        return g.E().hasLabel(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).as(edgeIdentifier).
                outV().
                repeat(__.outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,recordId).as(recordEdgeIdentier).
                        inV().dedup()).
                until(__.inE(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).where(P.eq("e"))).
                group().
                by(__.select(edgeIdentifier).id()).
                by(__.select(Pop.all,recordEdgeIdentier).unfold().values(propertyKeys).fold()).next();
    }

    public static  Map<Object,Object> getMergedRecordEdgeHopCountDistribution(GraphTraversalSource g) {
        return g.E().hasLabel(DBG_SUBGRAPH_EDGE_LABEL.MERGE_EDGE.identifier).
                group().
                by(__.values(DBG_EDGE_ATTRIBUTES.RECORD.identifier)).
                by(__.values(DBG_SUBGRAPH_EDGE_ATTRIBUTES.HOP_COUNT.identifier).groupCount()).next();
    }

    public GraphTraversalSource createExploreSubGraph(GraphTraversal<Edge,Edge> graphTraversal, int exploreDepth) {
        final String subGraph = "subgraph";
        final String edgeLabel = "e";
        TinkerGraph tinkerGraph = (TinkerGraph)graphTraversal.as(edgeLabel).subgraph(subGraph).
                repeat(__.inV().outE().subgraph(subGraph)).times(exploreDepth).
                select(edgeLabel).outV().inE().subgraph(subGraph).repeat(__.outV().inE().subgraph(subGraph)).times(exploreDepth-1).cap("subgraph").next();
        return tinkerGraph.traversal();

    }


    public void exploreTopologyEdges(Object edgeId, int exploreDepth) {

        GraphTraversalSource subG = createExploreSubGraph(g.E(edgeId),exploreDepth);

        String [] attributes = new String[] {
                DBG_EDGE_ATTRIBUTES.RECORD.identifier,
                DBG_EDGE_ATTRIBUTES.PROTEIN.identifier,
                DBG_EDGE_ATTRIBUTES.TRNA.identifier,
                DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                DBG_EDGE_ATTRIBUTES.RRNA.identifier
        };
        Map<Object,Object> edgeProperties = getTopologyEdgesPropertyDistributionsMapForTopologyEdges(Collections.singletonList(edgeId),attributes);
        Map<Object,Object> propertyDistribution = (Map<Object,Object>) getPropertyDistributionUnWrappedTraversal(
                subG.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gt(0)), attributes).
                next();

        // get distribution of record counts
        Map<Object,Object> recordCountDistribution = (Map<Object,Object>) getRecordCountDistributionTraversal(
                subG.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gt(0))).
                next();

        addMergeableBranches(subG);

        Map<Object,Object> mergedBranchesDistribution = (Map<Object,Object>)getMergedBranchesDistributionTraversal(subG).next();

        //TODO: routine to map properties of suffix_edges to mergeEdges and delete them
        //TODO: routine to persist pair wise kmer common count
    }



    /*** queries ******************************************************************************************************************************/

//    public String getSubSequenceForRecord(int recordId, boolean strand, SubsequenceAnalyzer.Range range) {
//        String subSequence = g.E(g.E().has(DBG_EDGE_ATTRIBUTES.Rhas(DBG_EDGE_ATTRIBUTES.POSITION.identifier,range.getStart()).toList()).has()
//    }

    public int getKmerLength() {
        return g.V().limit(1).values(DBG_NODE_ATTRIBUTES.KMER.identifier).map(k -> ((String)k.get()).length()).next();
    }

    public List<KmerDT> getMappingKmers(Collection<String> kPlusOneMers, boolean strand) {
        List<KmerDT> kmerList = new ArrayList<>();
        for(String kPlusOneMer: kPlusOneMers) {
            String prefix = kPlusOneMer.substring(0,kPlusOneMer.length()-1);
            String suffix = kPlusOneMer.substring(1);
            kmerList.addAll(
            ((Map<Object,Object>)g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,prefix).
                    outE().hasLabel(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                    has(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                    filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,suffix)).
                    group().
                    by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier)).
                    by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier).fold()).next()).
                    entrySet().stream().map(entry -> new KmerDT(
                    kPlusOneMer,
                    (int)entry.getKey(),
                    ((List<Integer>)entry.getValue() ).
                            toArray(new Integer[((List<Integer>) entry.getValue()).size()]))).
                    collect(Collectors.toList()));
        }
        return kmerList;
    }

    public Dataset<PositionCountIdentifierDT> getRecordCountPositionKmerDistribution(Map<String,Integer []> kmerMap, boolean strand) {
        final List<PositionCountIdentifierDT> positionDTS = new ArrayList<>();
        for(Map.Entry<String,Integer[]> kPlusOneMer: kmerMap.entrySet()) {
            String prefix = kPlusOneMer.getKey().substring(0,kPlusOneMer.getKey().length()-1);
            String suffix = kPlusOneMer.getKey().substring(1);
//            System.out.println(prefix + " " + suffix);
            int recordCount =
                    g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,prefix).
                            outE().hasLabel(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                            has(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                            filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,suffix)).
                            values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count().map(r -> r.get().intValue()).
                            next();
            for(int position: kPlusOneMer.getValue()) {
                positionDTS.add(new PositionCountIdentifierDT(position,kPlusOneMer.getKey(),recordCount));
            }
        }
        return SparkComputer.createDataFrame(positionDTS, PositionCountIdentifierDT.ENCODER);
    }

    public Dataset<PositionCountDT> getRecordCountPositionDistribution(Map<String,Integer []> kmerMap, boolean strand) {
        final List<PositionCountDT> positionDTS = new ArrayList<>();
        for(Map.Entry<String,Integer[]> kPlusOneMer: kmerMap.entrySet()) {
            String prefix = kPlusOneMer.getKey().substring(0,kPlusOneMer.getKey().length()-1);
            String suffix = kPlusOneMer.getKey().substring(1);
//            System.out.println(prefix + " " + suffix);
            List<Integer> recordCount =
                    g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,prefix).
                        outE().hasLabel(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).
                        filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,suffix)).
                        values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).map(r -> (int)r.get()).toList();
            if(recordCount.size() == 0) {
                recordCount.add(0);
            }
            for(int position: kPlusOneMer.getValue()) {
                positionDTS.add(new PositionCountDT(position,recordCount.get(0)));
            }
        }
        return SparkComputer.createDataFrame(positionDTS, PositionCountDT.class);
    }

    public Dataset<PositionCountRecordsDT> getRecordDistribution(Map<String,Integer []> kmerMap) {
        final List<PositionCountRecordsDT> positionDTS = new ArrayList<>();
        for(Map.Entry<String,Integer[]> kPlusOneMer: kmerMap.entrySet()) {
            String prefix = kPlusOneMer.getKey().substring(0,kPlusOneMer.getKey().length()-1);
            String suffix = kPlusOneMer.getKey().substring(1);
//            System.out.println(prefix + " " + suffix);
            List<Integer> records =
                    g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,prefix).
                                            outE().
                                            filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,suffix)).
                                            values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().
                                            map(r -> (int)r.get()).toList();

            for(int position: kPlusOneMer.getValue()) {
                positionDTS.add(new PositionCountRecordsDT(position,records.size(),records));
            }
        }
        return SparkComputer.createDataFrame(positionDTS, PositionCountRecordsDT.class);
    }

    /**
     * Get a list of common nodepairs for provided recordIds.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param recordIds
     * @param requireAllMatch If true only nodepairs that exist for all provided recordids are considered.
     *                        Otherwise, at least two recordIds have to exist for a nodepair to be considered.
     * @return
     */
    public List getCommonNodePairsForRecords(List<Integer> recordIds, boolean requireAllMatch) {
        if(requireAllMatch) {
            return  getCommonNodePairsForRecordsTraversal(recordIds,recordIds.size()).
                    select(Column.keys).toList();
        }
        return  getCommonNodePairsForRecordsTraversal(recordIds,2).
                select(Column.keys).toList();
    }

    /**
     * Get the amount of common nodepairs for provided recordIds.
     * A nodepair is a list of size two. First element is startvertex id. Second element is stopvertex id.
     * @param recordIds
     * @param requireAllMatch If true only nodepairs that exist for all provided recordids are considered.
     *                        Otherwise, at least two recordIds have to exist for a nodepair to be considered.
     * @return
     */
    public long getCommonNodePairsForRecordsCount(List<Integer> recordIds, boolean requireAllMatch) {
        if(requireAllMatch) {
            return  getCommonNodePairsForRecordsTraversal(recordIds,recordIds.size()).count().next();
        }
        return  getCommonNodePairsForRecordsTraversal(recordIds,2).count().next();
    }

    public List getOutEdgeRecordsForNode(long v) {
        return  g.V(v).outE().values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().toList();
    }

    public List getInEdgeRecordsForNode(long v) {
        return  g.V(v).inE().values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().toList();
    }

    /**
     * Get List of edges connecting node v1 (starting node) with node v2 (end node)
     * @param v1 node id of startnode
     * @param v2 node id of endnode
     * @return
     */
    public List getCommonEdgesToNodes(long v1, long v2) {
        return g.V(v2).as("v2").
                V(v1).outE().
                filter(__.otherV().where(P.eq("v2"))).toList();
    }

    /**
     * Get List of edge ids connecting node v1 (starting node) with node v2 (end node)
     * @param v1 node id of startnode
     * @param v2 node id of endnode
     * @return
     */
    public List getCommonEdgeIdsToNodes(long v1, long v2) {
        return g.V(v2).as("v2").
                V(v1).outE().
                filter(__.otherV().where(P.eq("v2"))).id().toList();
    }

    /**
     * Get list of common records for nodes v1 and v2.
     * Records are considered for outEdges of v1 and inEdges of v2
     * @param v1
     * @param v2
     * @return
     */
    public List getCommonRecordsForNodes(long v1, long v2) {
        return g.V(v2).inE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier
                ,P.within(getOutEdgeRecordsForNode(v1))).values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().toList();
    }

    /*** New sequence analysis ******************************************************************************************************************************/


    /*** Whole graph analysis ******************************************************************************************************************************/


    /*** Record pair analysis ******************************************************************************************************************************/

    /**
     *
     */
    public void computeNextRecordSimilarityDistribution() {

        try {
            Map<Integer,List<Integer>> recordMap = RecordTable.getInstance().getNextRecordPairing();
            for( Map.Entry<Integer,List<Integer>> entry: recordMap.entrySet()) {
                List<RecordSimilarityDT> recordSimilarities = new ArrayList<>();
                LOGGER.info("Conducting pairing for record " + entry.getKey());
                for(Integer record: entry.getValue()) {
                    LOGGER.info("At record: " + record);
                    RecordDT r1 = getRecordAttributes(record);
                    RecordDT r2 = getRecordAttributes(entry.getKey());
                    RecordSimilarityDT recordSimilarity = new RecordSimilarityDT(r1.getRecordId(),r2.getRecordId(),
                            Math.min(r1.getLength(),r2.getLength()),
                            (int)getCommonNodePairsForRecordsCount(new ArrayList<>(Arrays.asList(r1.getRecordId(),r2.getRecordId())),true)
                    );

                }
                SparkDBGComputer.persistRecordSimilarities(recordSimilarities);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*** Single record analysis ******************************************************************************************************************************/



    /**
     * For specified record get map where kmer pair is key and property maps of associated edgeds are values.
     * @param record
     * @return
     */
    public Map getKmerPropertyMapForRecord(int record) {
        return g.E().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).sack(assign).by(__.outV().values(DBG_NODE_ATTRIBUTES.KMER.identifier).fold()).sack(addAll).
                by(__.inV().values(DBG_NODE_ATTRIBUTES.KMER.identifier).fold()).
                group().by(__.sack()).
                by(__.local(__.valueMap()).fold()).next();
    }

    /*** Distances and routes ******************************************************************************************************************************/

    /**
     * For starting node v1, get map with recordId as key and list of associated positions as value for all recordIds within List records
     * @param v Node id of starting node, i.e. only out edegs are consdiered
     * @param records List of recordids to consider
     * @return map with recordId as key and list of associated positions as value or null if no matching records were found
     */
    private Object getRecordPositionMappingForOutEdges(long v, List records) {
        try {
            return g.V(v).outE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,P.within(records)).
                    group().by(DBG_EDGE_ATTRIBUTES.RECORD.identifier).
                    by(DBG_EDGE_ATTRIBUTES.POSITION.identifier).next();
        } catch (NoSuchElementException n) {
            LOGGER.warn("No matching records found");
            return null;
        }
    }

    /**
     * For stop node v1, get map with recordId as key and list of associated positions as value for all recordIds within List records
     * @param v Node id of stop node, i.e. only in edegs are consdiered
     * @param records List of recordids to consider
     * @return map
     */
    private Object getRecordPositionMappingForInEdges(long v, List records) {
        try {
            return g.V(v).inE().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,P.within(records)).
                    group().by(DBG_EDGE_ATTRIBUTES.RECORD.identifier).
                    by(DBG_EDGE_ATTRIBUTES.POSITION.identifier).next();
        } catch (NoSuchElementException n) {
            LOGGER.warn("No matching records found");
            return null;
        }
    }

    /**
     * Return true if there is a consistent connection between vertices with ids v1, v2, else return false.
     * A connection is consistent if at least one of the records labels on the outgoing edges of v1 is
     * contained in the set of records labled for the ingoing edges of v2.
     * @param v1
     * @param v2
     * @return
     */
    public boolean isConsistentlyConnected(long v1, long v2) {
        if(getCommonRecordsForNodes(v1,v2).size() == 0 && getCommonRecordsForNodes(v2,v1).size() == 0) {
            return false;
        }
        return true;
    }

    /**
     * Find all consistent routes from startnode v1 to endnode v2 (v1 -> ... -> v2)
     * A connection is consistent if at least one of the records labels on the outgoing edges of v1 is
     * contained in the set of records labled for the ingoing edges of v2.
     * @param v1
     * @param v2
     * @return Map, with recordId as key and list of distances as value
     */
    public Map<Integer,List<Distance>> getConsistentOutRouteForNodes(long v1, long v2) {
        Map<Integer,List<Distance>> routes = new HashMap<>();
        List records = getCommonRecordsForNodes(v1,v2);
        if(records.size() > 0) {
            Map<Object,Object> posMap1 = (Map)getRecordPositionMappingForOutEdges(v1,records);
            Map<Object,Object> posMap2 = (Map)getRecordPositionMappingForInEdges(v2,records);
            for(Map.Entry<Object,Object> recordPosPair: posMap1.entrySet()) {
                int record = (int)recordPosPair.getKey();
                List recordRoute = new ArrayList();
                for(Object pos1: (List)recordPosPair.getValue()) {
                    for(Object pos2: (List)posMap2.get(record)) {
                        RecordDT r = getRecordAttributes(record);
                        recordRoute.add(new Distance((int)pos1,(int)pos2,
                                r.getLength(), r.isTopology()));
                    }
                }
                routes.put(record,recordRoute);

            }
        }

        return routes;

    }

    /**
     * Find consistent routes from startnode v1 to endnode v2 (v1 -> ... -> v2) (first entry of returned list)
     * Find consistent routes from startnode v2 to endnode v1 (v2 -> ... -> v1) (second entry of returned list)
     * A connection  (v1 -> ... -> v2) is consistent if at least one of the records labels on the outgoing edges of v1 is
     * contained in the set of records labled for the ingoing edges of v2.
     * @param v1
     * @param v2
     * @return List of routes
     */
    public List<Map<Integer,List<Distance>>> getConsistentRouteForNodes(long v1, long v2) {
        List<Map<Integer,List<Distance>>> routeList = new ArrayList<>();
        // find routes v1 -> ... -> v2
        routeList.add(getConsistentOutRouteForNodes(v1,v2));

        // find routes v2 -> ... -> v1
        routeList.add(getConsistentOutRouteForNodes(v2,v1));
        return routeList;
    }


    /*** check routines ******************************************************************************************************************************/

    public boolean checkForRecord(int record) {
        return g.E().has("record",record).hasNext();
    }

    public void checkForRecords(List records) {
        for(Object r: records) {
            if(!checkForRecord((int)r)) {
                LOGGER.info("Missing record " + r) ;
            }
        }
    }

    public void checkForRecords() {

        List records = null;
        try {
            records = RecordTable.getInstance().getPersistedRecordIds();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(Object r: records) {
            if(!checkForRecord((int)r)) {
                LOGGER.info("Missing record " + r) ;
            }
        }
    }

    public void checkTopologyEdges() {

        String identifier = "topology_Edge";


        System.out.println(g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gt(0)).as(identifier).
                outV().outE(DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                filter(__.otherV().as("o").select(identifier).inV().where(P.eq("o"))).
                group().
                by(__.select(identifier)).
                by(__.values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count()).
                unfold().as("p").
                select(Column.keys).
                filter(__.values(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).as("count").select("p").select(Column.values).where(P.neq("count"))).toList());
    }

    // TODO : think about how to remember previos connectiontype
    public void checkPartions() {
        PropertiesConfiguration oldConfiguration =
                changeGraphConfiguration(GraphConfiguration.CONNECTION_TYPE.LOW_CACHE_OLTP);
        long vertexCount = 0;
        long partitionCount;
        for(int i = 0; i < maxPartitionCount; i++) {
            partitionCount = g.V().has(DBG_NODE_ATTRIBUTES.PARTITION.identifier,i).count().next();
            commit();
            LOGGER.info("Partion count: "+ partitionCount + " at partion " + i );
            if(partitionCount < partitionBoundary) {
                LOGGER.error("Wrong partion count " + partitionCount + " At partion " + i );
                System.exit(-1);
            }
            vertexCount += partitionCount;
        }
        partitionCount = g.V().has(DBG_NODE_ATTRIBUTES.PARTITION.identifier,maxPartitionCount).count().next();
        commit();

        LOGGER.info("Partion count " + partitionCount + " at partion " + maxPartitionCount);
        if(partitionCount != currentPartitionCount) {
            LOGGER.error("Wrong partion count " + partitionCount + " At partion " + maxPartitionCount);
            System.exit(-1);
        }
        vertexCount += partitionCount;
        System.out.println("VertexCount: " + vertexCount);
        changeGraphConfiguration(oldConfiguration);
    }

    public void addPropertiesToTopologyEdgesInSubGraph(GraphTraversalSource subGraphg, Map<Object,Object> topologyEdgePropertyMap ) {
        for(Map.Entry<Object,Object> mapEntry: topologyEdgePropertyMap.entrySet()) {
            subGraphg.E(mapEntry.getKey()).property("map",mapEntry.getValue()).iterate();
            commit();
        }
        List res = subGraphg.E().hasLabel(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).values("map").toList();
        System.out.println(((Map)res.get(0)).get(DBG_EDGE_ATTRIBUTES.RECORD.identifier));
//        System.out.println(subGraphg.E(topologyEdgePropertyMap.keySet()).toList());
//        subGraphg.E(topologyEdgePropertyMap.keySet()).as("e").property("map",topologyEdgePropertyMap.get())
    }


    public void generateTopologySubgraph() {
        GraphTraversalSource subG = createSubGraph(g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,P.gt(0)).id().toList());
        System.out.println(subG.E().count());
        System.out.println(subG.E().groupCount().by(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).next());
    }

    public Map getMapForRecord(int record) {

        return g.E().has(DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).sack(assign).by(__.outV().values().fold()).sack(addAll).
                by(__.inV().values().fold()).
                group().by(__.sack()).
                by(__.local(__.valueMap()).fold()).next();
    }

    Map getEdgePropertyMap(String propertyKey) {
        return g.E().values(propertyKey).groupCount().next();
    }

    long getMapTotalCount(Map<?,Long> m) {
        return m.values().stream().reduce(Long.valueOf(0),Long::sum);
    }

    public List getPersistedRecords() {
        return g.E().values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().toList();
    }



    void getCommonEdges() {

        Iterator it = g.E().sack(assign).by(__.outV().values().fold()).sack(addAll).
                by(__.inV().values().fold()).
                group().by(__.sack()).
                by(__.values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().fold()).unfold()
                .filter(__.select(Column.values).count(Scope.local).is(P.gt(1)));

        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }


    /*** topology edge filter -> Filter based on topology edge property******************************************************************************************************************************/
    public GraphTraversal<?,?> filterRecordCountsTraversal(Collection<Integer> values) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RECORD_COUNT,values);
    }


    /*** suffix edge filter -> Filter based on suffix edge property******************************************************************************************************************************/
    public GraphTraversal<?, ?> filterRecordsTraversal(Collection<Integer> values) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RECORD,values);
    }

    public Iterator filterRecordsTraversal(Collection<Integer> values, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RECORD,values, ids);
    }

    public Iterator filterStrandTraversal(Boolean value) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.STRAND,Collections.singleton(value));
    }

    public Iterator filterStrandTraversal(Boolean value, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.STRAND,Collections.singleton(value),ids);
    }

    public Iterator filterProteinsTraversal(Collection<String> values) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.PROTEIN,values);
    }

    public Iterator filterProteinsTraversal() {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.PROTEIN,getProteinNames());
    }

    public Iterator filterProteinsTraversal(Collection<String> values, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.PROTEIN,values,ids);
    }

    public Iterator filterProteinsTraversal(List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.PROTEIN,getProteinNames(),ids);
    }

    public Iterator filterTRNAsTraversal(Collection<String> values) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.TRNA,values);
    }

    public Iterator filterTRNAsTraversal() {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.TRNA,getTrnaNames());
    }

    public Iterator filterTRNAsTraversal(Collection<String> values, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.TRNA,values,ids);
    }

    public Iterator filterTRNAsTraversal(List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.TRNA,getTrnaNames(),ids);
    }

    public Iterator filterOtherTypesTraversal(Set<String> values) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.OTHER,values);
    }

    public Iterator filterOtherTypesTraversal() {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.OTHER,getOtherNames());
    }

    public Iterator filterOtherTypesTraversal(Collection<String> values, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.OTHER,values,ids);
    }

    public Iterator filterOtherTypesTraversal(List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.OTHER,getOtherNames(),ids);
    }

    public Iterator filterRRNATraversal(Boolean value) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RRNA,Collections.singleton(value));
    }

    public Iterator filterRRNATraversal() {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RRNA,getPRRNANames());
    }

    public Iterator filterRRNATraversal(Boolean value, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RRNA,Collections.singleton(value),ids);
    }

    public Iterator filterRRNATraversal(List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.RRNA,getPRRNANames(),ids);
    }

    public Iterator filterReplicationOriginTraversal(Boolean value) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.REPORIGIN,Collections.singleton(value));
    }

    public Iterator filterReplicationOriginTraversal() {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.REPORIGIN,getReplicationOriginNames());
    }

    public Iterator filterReplicationOriginTraversal(Boolean value, List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.REPORIGIN,Collections.singleton(value),ids);
    }

    public Iterator filterReplicationOriginTraversal(List ids) {
        return filterEdgePropertyTraversal(DBG_EDGE_ATTRIBUTES.REPORIGIN,getReplicationOriginNames(),ids);
    }


    public List<?> filterRecords(Collection<Integer> values) {
//        List records = filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RECORD,values);
//        System.out.println(records);
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RECORD,values);
    }

    public List filterRecords(Collection<Integer> values, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RECORD,values, ids);
    }

    public List filterStrand(Boolean value) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.STRAND,Collections.singleton(value));
    }

    public List filterStrand(Boolean value, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.STRAND,Collections.singleton(value),ids);
    }

    public List filterProteins(Collection<String> values) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.PROTEIN,values);
    }

    public List filterProteins() {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.PROTEIN,getProteinNames());
    }

    public List filterProteins(Collection<String> values, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.PROTEIN,values,ids);
    }

    public List filterProteins(List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.PROTEIN,getProteinNames(),ids);
    }

    public List filterTRNAs(Collection<String> values) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.TRNA,values);
    }

    public List filterTRNAs() {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.TRNA,getTrnaNames());
    }

    public List filterTRNAs(Collection<String> values, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.TRNA,values,ids);
    }

    public List filterTRNAs(List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.TRNA,getTrnaNames(),ids);
    }

    public List filterOtherTypes(Collection<String> values) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.OTHER,values);
    }

    public List filterOtherTypes() {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.OTHER,getOtherNames());
    }

    public List filterOtherTypes(Collection<String> values, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.OTHER,values,ids);
    }

    public List filterOtherTypes(List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.OTHER,getOtherNames(),ids);
    }

    public List filterRRNA(Boolean value) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RRNA,Collections.singleton(value));
    }

    public List filterRRNA() {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RRNA,getPRRNANames());
    }

    public List filterRRNA(Boolean value, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RRNA,Collections.singleton(value),ids);
    }

    public List filterRRNA(List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.RRNA,getPRRNANames(),ids);
    }

    public List filterReplicationOrigin(Boolean value) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.REPORIGIN,Collections.singleton(value));
    }

    public List filterReplicationOrigin() {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.REPORIGIN,getReplicationOriginNames());
    }

    public List filterReplicationOrigin(Boolean value, List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.REPORIGIN,Collections.singleton(value),ids);
    }

    public List filterReplicationOrigin(List ids) {
        return filterEdgeProperty(DBG_EDGE_ATTRIBUTES.REPORIGIN,getReplicationOriginNames(),ids);
    }

//
//    public void persistToSQL() {
//
//        GraphConfiguration.CONNECTION_TYPE connectionType = graphConfiguration.getConnectionType();
//        List<LocalProperty> localProperties = new ArrayList<>();
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES,1));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.ENABLE_CACHING,false));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.DATABASE_LEVEL_CACHE_PERCENTAGE,0.1));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.ENABLE_PRE_FETCH_ALL_PROPERTIES,false));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.STORAGE_CONNECTION_TIMEOUT, Duration.ofMillis(1000000)));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.STORAGE_READ_TIME, Duration.ofMillis(1000000)));
//        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.ENABLE_PRE_FETCH_ALL_PROPERTIES,false));
//        graphConfiguration.addLocalProperties(localProperties);
//        graphConfiguration.printConfiguration();
//        changeCacheTime(5,graph);
//        reopenGraph();
//
//        checkPSQLInterfaceIsSet();
//        int counter = 0;
//        int partitionCounter = 0;
//
//
//
//        try {
//            List<Map<String,Object>> results = new ArrayList<>();
//            Iterator<Object> idsIt = g.V().id();
//
//            while(idsIt.hasNext()) {
//                Object id = idsIt.next();
//
//                results.addAll(g.V(id).
//                        sideEffect(__.property(DBG_NODE_ATTRIBUTES.PARTITION.identifier,partitionCounter)).
//                        as("v1").out().dedup().
//                        project(Dbg.DBG_TABLE_COLUMNNAMES.V1.identifier,
//                                Dbg.DBG_TABLE_COLUMNNAMES.V2.identifier,
//                                Dbg.DBG_TABLE_COLUMNNAMES.RECORD_COUNT.identifier).
//                        by(__.select("v1").id()).
//                        by(__.id()).
//                        by(__.inE().filter(__.otherV().where(P.eq("v1"))).
//                                values(DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count()).toList());
//
//
//                if((counter > 0) && ((counter % 10000) == 0)) {
//                    System.out.println(counter);
//                    de.uni_leipzig.informatik.pacosy.mitos.psql.Dbg.persistDBG(psqlInterface,results);
//                    results = new ArrayList<>();
//                    if(counter == 100000) {
//                        partitionCounter++;
//                        counter = 0;
//                        System.out.println("PartitionCounter: "+ partitionCounter);
//                    }
//                    commit();
//
//                }
//
//                counter++;
//            }
//
//            if(results.size() > 0) {
//                de.uni_leipzig.informatik.pacosy.mitos.psql.Dbg.persistDBG(psqlInterface,results);
//            }
//
//
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        changeCacheTime(0,graph);
//        resetGraph(connectionType);
//        reopenGraph();
//    }

    /*** Auxiliary ******************************************************************************************************************************/

    public void addFeatures(DBG_EDGE_ATTRIBUTES attribute, boolean strand, GraphTraversalSource subGraph, List<Feature> features) {
//        System.out.println(attribute.identifier + " " + strand);
        Map<Object,Object> propertyMap = subGraph.E().
                has(attribute.identifier).
                has(DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand).
                as("e").
                group().by(__.properties(attribute.identifier).value()).
                by(__.values(DBG_EDGE_ATTRIBUTES.POSITION.identifier).dedup().order().fold()).next();

        for(Map.Entry<Object,Object> propertyEntry: propertyMap.entrySet()) {
            List<Integer> positions = (List<Integer>) propertyEntry.getValue();
            List<RangeDT> rangeList = RangeDT.createRangesForPositions(positions);
            for(RangeDT range: rangeList) {
                Feature feature = getFeatureType(attribute,propertyEntry.getKey());
                feature.setRange(range);
                feature.setStrand(strand);
//                feature.print();

                features.add(feature);
            }
        }
    }



    /*** Printing ******************************************************************************************************************************/

    public void printPropertiesNames() {
        if(properties == null) {
            LOGGER.error("Property names are not yet initialized.");
        }
        else if(properties.size() == 0) {
            LOGGER.error("Property names are not set yet");
        }

        System.out.println("Protein names: " + getProteinNames());
        System.out.println("Trna names: " + getTrnaNames());
        System.out.println("Othertype names: " + getOtherNames());
        System.out.println("Replication Origin names: " + getReplicationOriginNames());
        System.out.println("Rrna names: " + getPRRNANames());
    }

    public void printRoute(Map<Integer,List<Distance>> routes) {
        for(Map.Entry<Integer,List<Distance>> route: routes.entrySet()) {
            System.out.println(route.getKey());
            for(Distance distance: route.getValue()) {
                System.out.println(distance);
            }
            System.out.println();
        }
    }

    public void printRoutes(List<Map<Integer,List<Distance>>> routeList) {
        System.out.println("v1 -> ... -> v2");
        printRoute(routeList.get(0));
        System.out.println("v2 -> ... -> v1");
        printRoute(routeList.get(1));
    }

    public void printPatitionAttributes() {
        setLocalPartitionAttributes();
    }

    /*** Getter and setter ******************************************************************************************************************************/

    public String getKeyspace() {
        return KEYSPACE;
    }

    public List<String> getPropertyNames(PROPERTIES property) {
        return  getProperties().get(property).select(PropertyCountDT.COLUMN_NAME.NAME.identifier).as(Encoders.STRING()).collectAsList();
    }

    public List<String> getProteinNames() {
        return  getPropertyNames(PROPERTIES.PROTEIN);
    }

    public List<String> getTrnaNames() {
        return  getPropertyNames(PROPERTIES.TRNA);
    }

    public List<String> getPRRNANames() {
        return  getPropertyNames(PROPERTIES.RRNA);
    }

    public List<String> getReplicationOriginNames() {
        return  getPropertyNames(PROPERTIES.REPLICATION_ORIGIN);
    }

    public List getOtherNames() {
        return  getPropertyNames(PROPERTIES.OTHER);
    }

    public Map<PROPERTIES, Dataset<PropertyCountDT>> getProperties() {
        if(properties == null) {
            initPropertiesNames();
        }
        return properties;
    }


    class RecordThread extends Thread {
        List<String> results;
        int recordId;
        int maxPosition;

        public RecordThread(int recordId) {
            this.recordId = recordId;
        }

        public void run() {
//            results =
//                    (List<String>) filterRecordsTraversal(Collections.singleton(recordId)).
//                            values(DBG_EDGE_ATTRIBUTES.PROTEIN.identifier).map(p-> p.get().toString()).
//                            dedup().toList();
            maxPosition = (int)filterRecordsTraversal(Collections.singleton(recordId)).values(DBG_EDGE_ATTRIBUTES.POSITION.identifier).max().next();
        }

        public List<String> getResults() {
            return results;
        }

        public int getMaxPosition() {
            return maxPosition;
        }
    }

    public void testThread() {
        RecordThread r1 = new RecordThread(834);
        RecordThread r2 = new RecordThread(844);
        r1.start();
        r2.start();
        try {
            r1.join();
            r2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(r1.getMaxPosition());
        System.out.println(r2.getMaxPosition());
    }

    public void test() {
        System.out.println(g.E().has(DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,300).limit(10).toList());
    }



}

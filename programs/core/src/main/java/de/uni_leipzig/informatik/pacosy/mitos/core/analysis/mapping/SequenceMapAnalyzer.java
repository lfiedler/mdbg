package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.BandedSequence;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.Sequence;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAlignerFactory;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.Graph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.SequenceMapGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.TaxonomyGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.AlignCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import jdk.nashorn.internal.scripts.JD;
import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.*;

import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class SequenceMapAnalyzer {
    protected static final Logger LOGGER = LoggerFactory.getLogger(SequenceMapAnalyzer.class);
    private RangeDT boundaryRange;
    private static final String scoreTableName = "extracted.scores";
    private String plusStrandGenome;
    private String minusStrandGenome;
    private ResultDirectoryTree resultDirectoryTree;
    private SequenceInformationDT sequenceInformation;
    private final String id;
    private DbgGraph dbgGraph;
    private Parameter parameter;
    private SequenceAligner sequenceAligner;
    private AlignCost alignCost;
    private CheckedFunction recordSubstringComputer;
    private int sequenceId;
    private Dataset<PositionStrandCountIdentifierDT> sequenceKmers;
    private Dataset<StrandedRecordKmerDT> recordKmers;
    private static Dataset<PropertyLengthDT> propertyLengths;
    private double normRangeLength;

    private List<Integer> matchPositions;
    private List<Dataset<RecordDiagonalBiPositionRangeDT>> diagonalRanges;
    private List<RangeDT> ranges;
    private List<RangeDT> inverseRanges;
    private List<Dataset<RecordDiagonalBiPositionRangeDT>> bridgedRanges;

    private static final String DIFF_IDENTIFIER = "diff";

    private static final WindowSpec RECORD_PROPERTY_POSITION_FRAME = Window.partitionBy(ColumnIdentifier.POSITION).orderBy(col(ColumnIdentifier.WEIGHT).desc());
    private static final WindowSpec RECORD_DIAGONAL_ORDERED_FRAME =
            Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                    orderBy(ColumnIdentifier.RECORD_POSITION);
    private static final WindowSpec RECORD_DIAGONAL_INVERSE_ORDERED_FRAME =
            Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                    orderBy(desc(ColumnIdentifier.RECORD_POSITION));
    private static final WindowSpec RECORD_DIAGONAL_FRAME =
            Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER);
    private static final WindowSpec RECORD_DIAGONAL_ORDERED_BY_POSITION_START_FRAME =
            Window.partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                    orderBy(ColumnIdentifier.START);
    private static Dataset<RecordDT> recordDTDataset = null;





    private BiFunction<Integer,Integer,String> sequenceSubstringComputer;

    static {
        try {
            recordDTDataset = RecordTable.getInstance().getTableEntries().cache();
            propertyLengths = RecordPropertyTable.getInstance().fetchPropertyLengthDistribution().cache();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public interface CheckedFunction {
        String apply(int v1, int v2, int v3, boolean v4) throws Exception;
    }

    private static CheckedFunction checkFunctionWrapper(CheckedFunction function) throws RuntimeException {
        return (v1,v2,v3,v4) -> {
            try {
                return function.apply(v1,v2,v3,v4);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        };
    }

    public static class Parameter {

        private int permissableGapSize = 200;
        private int maxgapSizeFactor = 7;
        private double bandSizeFactor = 0.2;
        private int toggleSequenceLength = 30;
        private int minSequenceLength = 2;
        private int maxSequenceLength = 1000;
        private int defaultGapSizeFactor = 2;
        private double gapSizeFactor;
        public static double eValueThreshold;
        private double diagonalDiffPercentage;
        static int minRangeLength;
        private int minSmallRangeLength;
        private double rangeCoverFactor;
        private static String seperator = "\n";

        public Parameter(int defaultGapSizeFactor) {

            this.defaultGapSizeFactor = defaultGapSizeFactor;
            resetParameters();
        }

        public Parameter() {
            resetParameters();
        }
        private boolean gapTooLong(RangeDT range1, RangeDT range2, int gapSize) {
//            LOGGER.info("Cover limit " + (range1.getLength()+range2.getLength())*rangeCoverFactor + ", gapsize: " + gapSize);
//            return gapSize > getMaxGapSize(range1,range2);
            return gapTooLong(range1,gapSize) || gapTooLong(range2,gapSize);
        }

        private boolean gapTooLong(RangeDT range1,  int gapSize) {

            return gapSize > getMaxGapSize(range1);
        }

        private boolean parameterRelativeTooHigh(RangeDT range1, RangeDT range2, int gapsize) {
            return parameterRelativeTooHigh(range1,gapsize) || parameterRelativeTooHigh(range2,gapsize);
        }

        private boolean parameterRelativeTooHigh(RangeDT range1, int gapSize) {

            return ((gapSizeFactor)*gapSize)-gapSize > range1.getLength();
//            return 0.5*(gapSizeFactor*gapSize) > getMaxGapSize(range1);
        }

        private boolean bridgingIsValid(int gapSize) {
            return gapSizeFactor <= maxgapSizeFactor && gapSize <= permissableGapSize;
        }

        private boolean nextParameterRelativeTooHigh(RangeDT range1, RangeDT range2, int gapsize) {
            return nextParameterRelativeTooHigh(range1,gapsize) || parameterRelativeTooHigh(range2,gapsize);
        }

        private boolean nextParameterRelativeTooHigh(RangeDT range1, int gapSize) {

            return ((gapSizeFactor+1)*gapSize)-gapSize > range1.getLength();
//            return 0.5*(gapSizeFactor*gapSize) > getMaxGapSize(range1);
        }

        private int getMaxGapSize(RangeDT range) {
            return (int)((range.getLength())*rangeCoverFactor);
        }

        private int getMaxGapSize(RangeDT range1, RangeDT range2) {
            return (int)((range1.getLength()+range2.getLength())*rangeCoverFactor);
        }

        protected void setDefaultGapSizeFactor(int g) {
            this.defaultGapSizeFactor = g;
        }
        private void resetParameters() {
            gapSizeFactor = defaultGapSizeFactor;
            eValueThreshold = 1e-3;
            diagonalDiffPercentage = 0.2;
            minRangeLength = 5;
            minSmallRangeLength = 1;
            rangeCoverFactor = 0.5;

        }

        public String toString() {
            return "gapsizeFactor: " + gapSizeFactor +
                    seperator + "evalueThreshold: " + eValueThreshold +
                    seperator + "diagonalDiffPercentage: " + diagonalDiffPercentage +
                    seperator + "minRangeLength: " + minRangeLength +
                    seperator+ "rangeCoverFactor:" + rangeCoverFactor;
        }
    }

    /*** Initialization ***************************************************************************************************************************************************************************/

//    public SequenceMapAnalyzer()
    public SequenceMapAnalyzer(String id) {
        this.id = id;
        this.parameter = new Parameter();
        this.resultDirectoryTree = new ResultDirectoryTree(id,false);
        this.sequenceInformation = getSequenceInformation();
        this.sequenceId = RecordDT.mapToRecordId(getSequenceInformation().getId());
        alignCost = SequenceAlignerFactory.LEVEL_6;
        sequenceAligner = SequenceAlignerFactory.createBandedSequenceAligner(
                alignCost.getGapCostOptions().get(0),
                alignCost.getScoreCost());
        this.sequenceSubstringComputer = (v1,v2) -> this.getPlusSubSequence(v1,v2);


        this.sequenceKmers = fetchSequenceKmers().cache();
        this.recordKmers = getShiftedKmers().cache();
        this.normRangeLength = sequenceInformation.getLength();
        this.matchPositions = new ArrayList<>();
        this.diagonalRanges = new ArrayList<>();


        setRecordSubstringComputer();

        LOGGER.debug("Done initializing");
    }



    public void clean() {

        this.sequenceKmers.unpersist();
        this.recordKmers.unpersist();
        if(diagonalRanges != null) {
            for(Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange: diagonalRanges) {
                diagonalRange.unpersist();
            }
            diagonalRanges = null;
        }

    }


    /*** Analysis *********************************************************/


    public void computeMappingCoverage(List<RangeDT> rangeList, List<Dataset<ScoredPositionPositionKmerMappingDT>> scoreList) {

    }

    /**** Filter *************************************************************************************************************************************************************************************************/

    private static Dataset<Row> filterByRange(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, Dataset<StrandedRecordKmerDT> recordKmers, RangeDT range) {
        return sequenceKmers.filter(RangeDT.rangeFilter(range)). //filter for positions within range
                filter(col(ColumnIdentifier.COUNT_PLUS).gt(0).or(col(ColumnIdentifier.COUNT_MINUS).gt(0))). //filter for kmers that have a count > 0
                join(recordKmers,
                recordKmers.col(ColumnIdentifier.KMER).
                        equalTo(sequenceKmers.col(ColumnIdentifier.IDENTIFIER)));
    }

    /**
     * Creates PositionMappingDT dataset for all sequenceKmers with position in range, that match with entries in recordKmers.
     * If range is cyclic (start > end), all positions in [0,end] are shifted by sequence length to simulate a non cyclic range
     * @param sequenceKmers
     * @param recordKmers
     * @param range
     * @return
     */
    private Dataset<Row> filterByRangeToPositionMappingDT(
            Dataset<PositionStrandCountIdentifierDT> sequenceKmers, Dataset<StrandedRecordKmerDT> recordKmers, RangeDT range) {
        if(range.cyclic()) { // cyclic range -> split in 2
            final int length = getSequenceInformation().getLength()-1;
            final RangeDT r1 = new RangeDT(0,range.getEnd());
            final RangeDT r2 = new RangeDT(range.getStart(),length);

            LOGGER.debug("Cyclic range detected - Splitting into "+ r1  +" and " + r2);

            return
                    filterByRange(sequenceKmers,recordKmers,r2).
                            select(
                                    col(ColumnIdentifier.POSITION),
                                    col(ColumnIdentifier.RECORD_ID),
                                    col(ColumnIdentifier.RECORD_POSITION),
                                    col(ColumnIdentifier.STRAND),
                                    col(ColumnIdentifier.RECORD_LENGTH)).
                            union(filterByRange(sequenceKmers,recordKmers,r1).
                                    select(
                                            col(ColumnIdentifier.POSITION).plus(length+1).as(ColumnIdentifier.POSITION),
                                            col(ColumnIdentifier.RECORD_ID),
                                            col(ColumnIdentifier.RECORD_POSITION),
                                            col(ColumnIdentifier.STRAND),
                                            col(ColumnIdentifier.RECORD_LENGTH))
                            );
        }
        return filterByRange(sequenceKmers,recordKmers,range).select(
                col(ColumnIdentifier.POSITION),
                col(ColumnIdentifier.RECORD_ID),
                col(ColumnIdentifier.RECORD_POSITION),
                col(ColumnIdentifier.STRAND),
                col(ColumnIdentifier.RECORD_LENGTH));
    }
//    private static Dataset<Row> filterByRange(Dataset<PositionCountIdentifierDT> kmers, Dataset<KmerDT> mappingKmers, RangeDT range) {
//        Class<PositionCountIdentifierDT> tClass = PositionCountIdentifierDT.class;
//        return kmers.filter(RangeDT.rangeFilter(range)). //filter for positions within range
//            filter(CountInterface.countGtZeroFilter(tClass)). //filter for kmers that have a count > 0
//            join(mappingKmers,
//                mappingKmers.col(ColumnIdentifier.KMER).
//                        equalTo(kmers.col(ColumnIdentifier.IDENTIFIER)));
//    }

//    public static Dataset<KmerDT> filterByRangeToKmerDT(Dataset<PositionCountIdentifierDT> kmers, Dataset<KmerDT> mappingKmers, RangeDT range) {
//        return KmerDT.convert(filterByRange(kmers,mappingKmers,range));
//    }

//    /**
//     * Creates PositionMappingDT dataset for all sequenceKmers with position in range, that match with entries in recordKmers.
//     * If range is cyclic (start > end), all positions in [0,end] are shifted by sequence length to simulate a non cyclic range
//     * @param sequenceKmers
//     * @param recordKmers
//     * @param range
//     * @return
//     */
//    private Dataset<PositionMappingDT> filterByRangeToPositionMappingDT(
//            Dataset<PositionCountIdentifierDT> sequenceKmers, Dataset<KmerDT> recordKmers, RangeDT range) {
//        if(range.cyclic()) { // cyclic range -> split in 2
//            final int length = getSequenceInformation().getLength()-1;
//            final RangeDT r1 = new RangeDT(0,range.getEnd());
//            final RangeDT r2 = new RangeDT(range.getStart(),length);
//
//            LOGGER.debug("Cyclic range detected - Splitting into "+ r1  +" and " + r2);
//
//            return PositionMappingDT.convert(
//                            filterByRange(sequenceKmers,recordKmers,r2).
//                            select(
//                                    col(ColumnIdentifier.POSITION),
//                                    col(ColumnIdentifier.RECORD_ID),
//                                    col(ColumnIdentifier.POSITIONS)).
//                            union(filterByRange(sequenceKmers,recordKmers,r1).
//                                    select(
//                                            col(ColumnIdentifier.POSITION).plus(length+1).as(ColumnIdentifier.POSITION),
//                                            col(ColumnIdentifier.RECORD_ID),
//                                            col(ColumnIdentifier.POSITIONS))
//                            ));
//        }
//        return PositionMappingDT.convert(filterByRange(sequenceKmers,recordKmers,range));
//    }

    /**** Cluster computation *************************************************************************************************************************************************************************************************/


//    private void fillPropertyCluster(Dataset<Row> properties) {
//        properties = properties.orderBy(ColumnIdentifier.POSITION);
//        Iterator<Row> propertyIterator = properties.toLocalIterator();
//        int oldPosition = properties.select(ColumnIdentifier.POSITION).as(Encoders.INT()).first();
//        List<ScoredProperty> propertyDistribution = new TreeList();
//        double weightsum = 0;
//        int pos = oldPosition;
////        for(int p = 0; p < oldPosition; p++) {
////            LOGGER.debug("Filling missing position " + p);
////            TreeNode treeNode2 = new TreeNode(new RangeDT(p,p));
////            propertyCluster.add(treeNode2);
////        }
//        while(propertyIterator.hasNext()) {
//            Row recordProperty = propertyIterator.next();
//            pos = recordProperty.getInt(recordProperty.fieldIndex(ColumnIdentifier.POSITION));
//            if(pos > oldPosition) {
////                for(int p = oldPosition+1; p < pos; p++) {
////                    LOGGER.debug("Filling missing position " + p);
////                    TreeNode treeNode = new TreeNode(new RangeDT(p,p));
////                    propertyCluster.add(treeNode);
////                }
//                LOGGER.debug("New cluster " + oldPosition);
//                TreeNode treeNode = new TreeNode(new RangeDT(oldPosition,oldPosition));
//                treeNode.setProperties(propertyDistribution,weightsum);
//                propertyCluster.add(treeNode);
//                propertyDistribution = new TreeList();
//                weightsum = 0;
//                oldPosition = pos;
//            }
//            String property = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.PROPERTY));
//            String category = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.CATEGORY));
//            double weight = recordProperty.getDouble(recordProperty.fieldIndex(ColumnIdentifier.WEIGHT));
//            long count = recordProperty.getLong(recordProperty.fieldIndex(ColumnIdentifier.COUNT));
//            propertyDistribution.add(new ScoredProperty(property,category,weight,count));
//            weightsum += weight;
//        }
//        LOGGER.debug("New cluster " + pos);
//        TreeNode treeNode = new TreeNode(new RangeDT(pos,pos));
//        treeNode.setProperties(propertyDistribution,weightsum);
//        propertyCluster.add(treeNode);
////        for(int p = pos+1; p < sequenceInformation.getLength(); p++) {
////            LOGGER.debug("Filling missing position " + p);
////            TreeNode treeNode2 = new TreeNode(new RangeDT(p,p));
////            propertyCluster.add(treeNode2);
////        }
//
//    }

    private void fillPropertyCluster(Dataset<Row> properties, List<TreeNode> propertyCluster) {

//        properties.show();
        Iterator<Row> propertyIterator = properties.toLocalIterator();

        while(propertyIterator.hasNext()) {
            Row recordProperty = propertyIterator.next();
            int start = recordProperty.getInt(recordProperty.fieldIndex(ColumnIdentifier.START));
            int end = recordProperty.getInt(recordProperty.fieldIndex(ColumnIdentifier.END));
            String property = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.PROPERTY));
            String category = recordProperty.getString(recordProperty.fieldIndex(ColumnIdentifier.CATEGORY));
            BigDecimal weight = recordProperty.getDecimal(recordProperty.fieldIndex(ColumnIdentifier.WEIGHT));

            for (int i = start; i <= end; i++) {
                propertyCluster.get(i).addProperty(property, category, weight);
            }
        }
    }

//    public void createCluster() {
//
//
//        diagonalRanges = new ArrayList<>(Arrays.asList(fetchJoinedRanges()));
//        for(boolean strand : Arrays.asList(true,false)){
//            List<TreeNode> propertyCluster = createCluster(strand);
//            FileIO.serializeJson(resultDirectoryTree.getPropertyClusterFile(strand),propertyCluster);
//        }
//    }

    public void createCluster() {


        Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange = fetchJoinedRanges();
//        System.out.println("DiagonalRanges"diagonalRanges.size());



        List<Dataset<Row>> results = fetchAggregatedProperties(diagonalRange);
        LOGGER.debug("Fetched aggregated properties");
        for(int resultIndex = 0; resultIndex < results.size(); resultIndex++) {
            List<TreeNode> propertyCluster = new ArrayList<>();
            boolean sequenceStrand = resultIndex == 0 ? true : false;
            LOGGER.debug("SequenceStrand: " + sequenceStrand);
            for(int pos = 0; pos < sequenceInformation.getLength(); pos++) {
                propertyCluster.add(new TreeNode(new RangeDT(pos,pos)));
            }
            fillPropertyCluster(results.get(resultIndex),propertyCluster);
            Iterator<TreeNode> clusterIterator = propertyCluster.iterator();
            while (clusterIterator.hasNext()) {
                TreeNode treeNode = clusterIterator.next();
                if(treeNode.getProperties().size() == 0) {
                    clusterIterator.remove();
                }
                else {
                    treeNode.normalizeProperties();
//                if(!ScoredProperty.normedCorrectly(treeNode.getProperties())) {
//                    LOGGER.error("Normalization Error " + treeNode);
//                }
                }
            }
            LOGGER.debug("Filled propertycluster");
            FileIO.serializeJson(resultDirectoryTree.getPropertyClusterFile(sequenceStrand),propertyCluster);
            LOGGER.debug("Serialized propertycluster");
            computeCluster(propertyCluster,sequenceStrand);
            LOGGER.debug("Persisted propertycluster");
//            System.out.println("Plus annotation");
//            results.get(0).orderBy("start","end","property","recordId").show(150);
//            System.out.println("Minus annotation");
//            results.get(1).orderBy("start","end","property","recordId").show(150);
//            properties.orderBy(ColumnIdentifier.START).show(50);


        }
    }





    public int getClusterSuccessorIndex(int currentIndex,int clusterSize) {
        if(clusterSize == 1) {
            return -1;
        }
        if(currentIndex < clusterSize-1) {
            return currentIndex+1;
        }
        if(sequenceInformation.isTopology()) {
            return 0;
        }
        return -1;
    }

    public int getClusterPredecessorIndex(int currentIndex, int clusterSize) {
        if(clusterSize == 1) {
            return -1;
        }
        if(currentIndex > 0) {
            return currentIndex-1;
        }
        if(sequenceInformation.isTopology()) {
            return clusterSize-1;
        }
        return -1;
    }

    public TreeNode getClusterSuccessor(TreeNode currentCluster, List<TreeNode> propertyCluster) {
        int index = propertyCluster.indexOf(currentCluster);
        int successorIndex = getClusterSuccessorIndex(index,propertyCluster.size());
        if(successorIndex == -1) {
            return null;
        }
        return propertyCluster.get(successorIndex);
    }

    public TreeNode getClusterPredecessor(TreeNode currentCluster, List<TreeNode> propertyCluster) {
        int index = propertyCluster.indexOf(currentCluster);
        int predecessorIndex = getClusterPredecessorIndex(index,propertyCluster.size());
        if(predecessorIndex == -1) {
            return null;
        }
        return propertyCluster.get(predecessorIndex);
    }


    public void computeCluster() {
        for(boolean strand : Arrays.asList(true,false)) {
            LOGGER.debug("Strand " + strand);
            computeCluster(fetchPropertyCluster(strand),strand);
        }
    }

    public void computeCluster(List<TreeNode> propertyCluster,boolean sequenceStrand) {

//        LOGGER.debug("Cluster start "+ propertyCluster.get(0).getRange());
//        LOGGER.debug("Cluster end "+ propertyCluster.get(propertyCluster.size()-1).getRange());
//        int topCandidateIndex = 0;
        // compute pairwise cluster candidates -> set their merged properties
        for(int clusterIndex = 0; clusterIndex < propertyCluster.size(); clusterIndex++) {

//            if(clusterIndex == 15264) {
//                System.out.println(clusterIndex);
//            }
            TreeNode cluster = propertyCluster.get(clusterIndex);
            int neighborIndex = getClusterSuccessorIndex(clusterIndex,propertyCluster.size());
            if(neighborIndex == -1) {
                break;
            }
            TreeNode neighbourCluster = propertyCluster.get(neighborIndex);
            cluster.updateNeighbourMerge(neighbourCluster,sequenceInformation);
        }

        int counter = 0;
        TreeNode winner = propertyCluster.get(0);
//        if(!checkConsistentRanges(propertyCluster)) {
//            LOGGER.error("Start");
//        }
        do {
            // find winner amongst cluster candidates
            for(int clusterIndex = 0; clusterIndex < propertyCluster.size(); clusterIndex++) {
                TreeNode cluster = propertyCluster.get(clusterIndex);
                if(cluster.beats(winner)){
                    winner = cluster;
                }
            }
            if(winner.getMergeProperties().size() == 0) {
                break;
            }

            TreeNode successorCluster = getClusterSuccessor(winner,propertyCluster);
            TreeNode mergedCluster = winner.merge(successorCluster);
//            System.out.println("Merged: " + mergedCluster);
            propertyCluster.set(propertyCluster.indexOf(winner),mergedCluster);
            propertyCluster.remove(successorCluster);
            if(propertyCluster.size() == 1) {
                break;
            }
//            System.out.println("in list "+ propertyCluster.get(topCandidateIndex));
//            System.out.println("in list "+ propertyCluster.get(successorIndex));
            TreeNode predecessorCluster = getClusterPredecessor(mergedCluster,propertyCluster);
            if(predecessorCluster != null) {
                predecessorCluster.updateNeighbourMerge(mergedCluster,sequenceInformation);
            }
            successorCluster = getClusterSuccessor(mergedCluster,propertyCluster);
            if(successorCluster != null) {
                mergedCluster.updateNeighbourMerge(successorCluster,sequenceInformation);
            }
            else{
                mergedCluster.reset();
            }
//            if(!checkConsistentRanges(propertyCluster)) {
//                LOGGER.error("At " + counter);
//            }
            counter++;
//            if(propertyCluster.size() <= 14) {
//                printPropertyCluster(propertyCluster);
//                propertyCluster.forEach(treeNode -> System.out.println(treeNode.getRange() + " " + TreeNode.treeHeight(treeNode)));
//                break;
//            }
//            if(propertyCluster.size() <= 1600) {
//                break;
//            }
//            if(topCandidateIndex >= propertyCluster.size()) {
//                System.out.println(topCandidateIndex);
//            }
            winner = mergedCluster;


//            LOGGER.debug("cluster size: " + propertyCluster.size());
////            LOGGER.debug("Winnder index: " + topCandidateIndex);
//            LOGGER.debug("Winner: "+ winner.getRange() );
//            LOGGER.debug(winner.qualityCandidateAsString());
//            printPropertyCluster(propertyCluster);


        } while (true);

//        LOGGER.debug("root amount " + propertyCluster.size());
        Cluster.persistTree(propertyCluster,resultDirectoryTree.getPropertyClusterTreeFile(sequenceStrand));


    }

    public boolean checkConsistentRanges(List<TreeNode> propertyCluster) {
        for(int i = 0; i < propertyCluster.size(); i++) {
            if(Auxiliary.mod(propertyCluster.get(Auxiliary.mod(i+1,propertyCluster.size())).getRange().getStart()-propertyCluster.get(i).getRange().getEnd(),
                    sequenceInformation.getLength()) != 1){
                LOGGER.error(propertyCluster.get(i).getRange() + " " + propertyCluster.get(Auxiliary.mod(i+1,propertyCluster.size())));
                return false;
            }
        }
        return true;
    }

    public void checkNormScore(List<TreeNode> propertyCluster) {
        propertyCluster.stream().filter(treeNode -> !ScoredProperty.normedCorrectly(treeNode.getProperties())).forEach(treeNode -> System.out.println(treeNode));
    }



    /**** Diagonal computation *************************************************************************************************************************************************************************************************/

    /**
     * Compute RecordDiagonalBiPositionRanges for provided range.
     * @param range Range for which diagonals are to be computed.
     * @return
     */
    public  Dataset<RecordDiagonalBiPositionRangeDT> computeDiagonalRanges(RangeDT range) {

//        sequenceKmers.orderBy(desc(ColumnIdentifier.POSITION)).show(500);
        LOGGER.debug("Compute diagonal ranges for " + range);
        int sequenceLength = sequenceInformation.getLength();
        Dataset<Row> rangeFilteredDataset = filterByRangeToPositionMappingDT(sequenceKmers,recordKmers,range);

//        rangeFilteredDataset.orderBy(desc(ColumnIdentifier.POSITION)).show(500)
        Dataset<FlaggedRecordDiagonalBiPositionRangeDT> diagonalDataset = computeDiagonalRanges(rangeFilteredDataset);
        Dataset<RecordDiagonalBiPositionRangeDT> cyclicDiagonalsDataset = computeCyclicDiagonalRanges(diagonalDataset);
//        System.out.println("1");
//        diagonalDataset.show();
//        System.out.println("2");
//        cyclicDiagonalsDataset.show();

        Dataset<RecordDiagonalBiPositionRangeDT> result=
                RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                diagonalDataset.
                        withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
                        withColumnRenamed(ColumnIdentifier.STRAND,"s").
                        withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,"d").
                        // remove all rows with recordId and diagonalvalue contained in cyclicDiagonalDataset
                                join(
                                cyclicDiagonalsDataset,col("r").equalTo(cyclicDiagonalsDataset.col(ColumnIdentifier.RECORD_ID)).
                                        and(col("s").equalTo(cyclicDiagonalsDataset.col(ColumnIdentifier.STRAND))).
                                        and(col("d").equalTo(cyclicDiagonalsDataset.col(ColumnIdentifier.DIAGONAL_IDENTIFIER))),
                                "leftanti").
                        withColumnRenamed("r",ColumnIdentifier.RECORD_ID).
                        withColumnRenamed("s",ColumnIdentifier.STRAND).
                        withColumnRenamed("d",ColumnIdentifier.DIAGONAL_IDENTIFIER)).
                // add cyclic diagonals
                        union(cyclicDiagonalsDataset);
//        System.out.println(3);
//        result.show();
        if(range.cyclic()) {
            LOGGER.debug("Reshifting range");
            result = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                    result.withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.START_2).
                    withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.END_2).
                    withColumn(ColumnIdentifier.START,pmod(col(ColumnIdentifier.START_2),lit(sequenceLength).cast(DataTypes.IntegerType))).
                    withColumn(ColumnIdentifier.END,pmod(col(ColumnIdentifier.END_2),lit(sequenceLength).cast(DataTypes.IntegerType))).
                    drop(ColumnIdentifier.START_2,ColumnIdentifier.END_2));
        }
        return result;
    }

//    /**
//     * Compute RecordDiagonalBiPositionRanges for provided range.
//     * @param range Range for which diagonals are to be computed.
//     * @return
//     */
//    public  Dataset<RecordDiagonalBiPositionRangeDT> computeDiagonalRanges(RangeDT range) {
//
////        sequenceKmers.orderBy(desc(ColumnIdentifier.POSITION)).show(500);
//        LOGGER.debug("Compute diagonal ranges for " + range);
//        int sequenceLength = sequenceInformation.getLength();
//        Dataset<PositionMappingDT> rangeFilteredDataset = filterByRangeToPositionMappingDT(sequenceKmers,recordKmers,range);
////        rangeFilteredDataset.orderBy(desc(ColumnIdentifier.POSITION)).show(500);
//        Dataset<FlaggedRecordDiagonalBiPositionRangeDT> diagonalDataset = computeDiagonalRanges(rangeFilteredDataset,!strand).cache();
//        Dataset<RecordDiagonalBiPositionRangeDT> cyclicDiagonalsDataset = computeCyclicDiagonalRanges(diagonalDataset).cache();
//
//        Dataset<RecordDiagonalBiPositionRangeDT> result= RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                diagonalDataset.
//                withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
//                withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,"d").
//                        // remove all rows with recordId and diagonalvalue contained in cyclicDiagonalDataset
//                join(
//                cyclicDiagonalsDataset,col("r").equalTo(cyclicDiagonalsDataset.col(ColumnIdentifier.RECORD_ID)).
//                        and(col("d").equalTo(cyclicDiagonalsDataset.col(ColumnIdentifier.DIAGONAL_IDENTIFIER))),
//                        "leftanti").
//                withColumnRenamed("r",ColumnIdentifier.RECORD_ID).
//                withColumnRenamed("d",ColumnIdentifier.DIAGONAL_IDENTIFIER)).
//                // add cyclic diagonals
//                union(cyclicDiagonalsDataset);
//        if(range.cyclic()) {
//            LOGGER.debug("Reshifting range");
//            // TODO: reshift Ranges !
//            result = result.withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.START_2).
//                    withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.END_2).
//                    withColumn(ColumnIdentifier.START,pmod(col(ColumnIdentifier.START_2),lit(sequenceLength).cast(DataTypes.IntegerType))).
//                    withColumn(ColumnIdentifier.END,pmod(col(ColumnIdentifier.END_2),lit(sequenceLength).cast(DataTypes.IntegerType))).
//                    drop(ColumnIdentifier.START_2,ColumnIdentifier.END_2).as(RecordDiagonalBiPositionRangeDT.ENCODER);
//        }
//        return result;
//    }

    /**
     * Based on RecordDiagonalBiPositionRange results (dataset),
     * compute associated inverse RecordDiagonalBiPositionRanges for the provided range.
     * @param dataset
     * @param range
     * @return
     */
    public static Dataset<RecordDiagonalPositionRangeDT> computeInverseDiagonalRanges(
            Dataset<? extends RecordDiagonalBiPositionRangeDT> dataset, RangeDT range) {

        return dataset.
                withColumn(ColumnIdentifier.INVERSE_START,

                        lag(col(ColumnIdentifier.END),1,range.getStart()-1).
                                over(RECORD_DIAGONAL_ORDERED_BY_POSITION_START_FRAME).plus(1)).
                withColumn(ColumnIdentifier.INVERSE_END,
                        // If "start" of the first range within a diagonal is equal to range.getStart() set -1 for the associated inverse range -> needs to be filtered
                        when(col(ColumnIdentifier.START).gt(range.getStart()),
                                col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER,ColumnIdentifier.INVERSE_START,ColumnIdentifier.INVERSE_END).
                union(
                        dataset.
                                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                                agg(
                                        when(max(ColumnIdentifier.END).notEqual(range.getEnd()),
                                                max(ColumnIdentifier.END).plus(1)).otherwise(-1).as(ColumnIdentifier.INVERSE_START),
                                        lit(range.getEnd()).cast(DataTypes.IntegerType).as(ColumnIdentifier.INVERSE_END)).
                                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
                                select(
                                        ColumnIdentifier.RECORD_ID,
                                        ColumnIdentifier.DIAGONAL_IDENTIFIER,
                                        ColumnIdentifier.INVERSE_START,ColumnIdentifier.INVERSE_END)
                ).
                select(
                        col(ColumnIdentifier.RECORD_ID),
                        col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
                        col(ColumnIdentifier.INVERSE_START).as(ColumnIdentifier.START),
                        col(ColumnIdentifier.INVERSE_END).as(ColumnIdentifier.END)).
                withColumn(ColumnIdentifier.LENGTH,
                        col(ColumnIdentifier.END).
                                minus(col(ColumnIdentifier.START)).plus(1)).
                as(RecordDiagonalPositionRangeDT.ENCODER);
    }

    /**
     * Compute FlaggedRecordDiagonalBiPositionRanges.
     * Each diagonal group which may potentially correspond to a cyclic record range is marked
     * with FlaggedRecordDiagonalBiPositionRangeDT.CYLIC_FLAG and can be preprocessed later.
     * @param positionMapping
     * @return
     */
    private static Dataset<FlaggedRecordDiagonalBiPositionRangeDT> computeDiagonalRanges(Dataset<Row> positionMapping) {

        return  FlaggedRecordDiagonalBiPositionRangeDT.
                convertToFlaggedRecordDiagonalBiPositionRange(
                positionMapping.
                withColumn(ColumnIdentifier.DIAGONAL_IDENTIFIER,
                        when(col(ColumnIdentifier.RECORD_LENGTH).notEqual(-1),
                                pmod(col(ColumnIdentifier.RECORD_POSITION).minus(col(ColumnIdentifier.POSITION)),
                                        col(ColumnIdentifier.RECORD_LENGTH))).
                                otherwise(
                                        col(ColumnIdentifier.RECORD_POSITION).minus(col(ColumnIdentifier.POSITION)))).
                withColumn(DIFF_IDENTIFIER,col(ColumnIdentifier.RECORD_POSITION).minus(row_number().over(RECORD_DIAGONAL_ORDERED_FRAME))).
                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,
                        DIFF_IDENTIFIER,ColumnIdentifier.RECORD_LENGTH,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                agg(
                        when(min(col(ColumnIdentifier.RECORD_POSITION)).equalTo(0).
                                or(max(col(ColumnIdentifier.RECORD_POSITION)).
                                        equalTo(col(ColumnIdentifier.RECORD_LENGTH).minus(1))),lit(FlaggedRecordDiagonalBiPositionRangeDT.CYLIC_FLAG)).
                                otherwise(lit(FlaggedRecordDiagonalBiPositionRangeDT.NON_CYCLIC_FLAG)).
                                as(ColumnIdentifier.REARRANGE_FLAG),
                        min(ColumnIdentifier.POSITION).as(ColumnIdentifier.START),
                        max(ColumnIdentifier.POSITION).as(ColumnIdentifier.END),
                        min(ColumnIdentifier.RECORD_POSITION).as(ColumnIdentifier.RECORD_START),
                        max(ColumnIdentifier.RECORD_POSITION).as(ColumnIdentifier.RECORD_END)).
                withColumn(ColumnIdentifier.LENGTH,
                        col(ColumnIdentifier.END).
                                minus(col(ColumnIdentifier.START)).plus(1)));
    }

    /**
     * Extract all diagonal groups that correspond to actual record cyclic ranges and group them accordingly,
     * i.e. merging start and end ranges beyond periodic boundaries
     * @param dataset RecordDiagonalBiPositionRanges
     * @return
     */
    private static Dataset<RecordDiagonalBiPositionRangeDT> computeCyclicDiagonalRanges(Dataset<FlaggedRecordDiagonalBiPositionRangeDT> dataset) {
        String cyclicCountIdentifier = "cyclicCount";
        final String rank = "rank", recordIdentifier = "r",
                diagonalIdentifier = "d", lengthIdentifier = "l";
        final WindowSpec w = Window.
                partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.DIAGONAL_IDENTIFIER).
                orderBy(col(ColumnIdentifier.RECORD_START).desc());


        Dataset<Row> cyclicDiagonals =
                dataset.withColumn(cyclicCountIdentifier,
                        sum(col(ColumnIdentifier.REARRANGE_FLAG)).
                                over(RECORD_DIAGONAL_FRAME).as(cyclicCountIdentifier)).
                        filter(col(cyclicCountIdentifier).equalTo(2)).cache();



        Dataset<Row> ranks = cyclicDiagonals.filter(
                col(ColumnIdentifier.REARRANGE_FLAG).equalTo(FlaggedRecordDiagonalBiPositionRangeDT.CYLIC_FLAG)).
                withColumn(rank,row_number().over(w));



        Dataset<Row> d1 = ranks.filter(col(rank).equalTo(2)).
                select(col(ColumnIdentifier.RECORD_ID).as(recordIdentifier),
                        col(ColumnIdentifier.DIAGONAL_IDENTIFIER).as(diagonalIdentifier),
                        col(ColumnIdentifier.END),col(ColumnIdentifier.RECORD_END),
                        col(ColumnIdentifier.LENGTH).as(lengthIdentifier),
                        col(ColumnIdentifier.STRAND)
                        );

        Dataset<RecordDiagonalBiPositionRangeDT> result =  RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                ranks.filter(col(rank).equalTo(1)).
                        select(ColumnIdentifier.RECORD_ID, ColumnIdentifier.RECORD_LENGTH,ColumnIdentifier.DIAGONAL_IDENTIFIER,
                                ColumnIdentifier.START,ColumnIdentifier.RECORD_START,ColumnIdentifier.LENGTH).
                        join(d1,
                                col(ColumnIdentifier.RECORD_ID).equalTo(d1.col(recordIdentifier)).
                                        and(col(ColumnIdentifier.DIAGONAL_IDENTIFIER).equalTo(d1.col(diagonalIdentifier)))
                        ).
                        select(col(ColumnIdentifier.RECORD_ID),
                                col(ColumnIdentifier.RECORD_LENGTH),
                                col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
                                col(ColumnIdentifier.START), col(ColumnIdentifier.END),
                                col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_END),
                                col(ColumnIdentifier.LENGTH).plus(col(lengthIdentifier)).as(ColumnIdentifier.LENGTH),
                                col(ColumnIdentifier.STRAND)
                        ).
                        union(cyclicDiagonals.filter(col(ColumnIdentifier.REARRANGE_FLAG).
                                equalTo(FlaggedRecordDiagonalBiPositionRangeDT.NON_CYCLIC_FLAG)).
                                select(col(ColumnIdentifier.RECORD_ID),
                                        col(ColumnIdentifier.RECORD_LENGTH),
                                        col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
                                        col(ColumnIdentifier.START), col(ColumnIdentifier.END),
                                        col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_END),
                                        col(ColumnIdentifier.LENGTH),
                                        col(ColumnIdentifier.STRAND)
                                )
                        ));
        cyclicDiagonals.unpersist();
        return result;

    }
//    /**
//     * Extract all diagonal groups that correspond to actual record cyclic ranges and group them accordingly,
//     * i.e. merging start and end ranges beyond periodic boundaries
//     * @param dataset RecordDiagonalBiPositionRanges
//     * @return
//     */
//    private static Dataset<RecordDiagonalBiPositionRangeDT> computeCyclicDiagonalRanges(Dataset<FlaggedRecordDiagonalBiPositionRangeDT> dataset) {
//        String cyclicCountIdentifier = "cyclicCount";
//        final String rank = "rank", recordIdentifier = "r",
//                diagonalIdentifier = "d", lengthIdentifier = "l";
//        final WindowSpec w = Window.
//                partitionBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).
//                orderBy(col(ColumnIdentifier.RECORD_START).desc());
//
//
//        Dataset<Row> cyclicDiagonals =
//                dataset.withColumn(cyclicCountIdentifier,
//                        sum(col(ColumnIdentifier.REARRANGE_FLAG)).
//                                over(RECORD_DIAGONAL_FRAME).as(cyclicCountIdentifier)).
//                        filter(col(cyclicCountIdentifier).equalTo(2)).cache();
//
//
//
//        Dataset<Row> ranks = cyclicDiagonals.filter(
//                col(ColumnIdentifier.REARRANGE_FLAG).equalTo(FlaggedRecordDiagonalBiPositionRangeDT.CYLIC_FLAG)).
//                withColumn(rank,row_number().over(w));
//
//
//
//        Dataset<Row> d1 = ranks.filter(col(rank).equalTo(2)).
//                select(col(ColumnIdentifier.RECORD_ID).as(recordIdentifier),
//                        col(ColumnIdentifier.DIAGONAL_IDENTIFIER).as(diagonalIdentifier),
//                        col(ColumnIdentifier.END),col(ColumnIdentifier.RECORD_END),
//                        col(ColumnIdentifier.LENGTH).as(lengthIdentifier));
//
//        return RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                ranks.filter(col(rank).equalTo(1)).
//                        select(ColumnIdentifier.RECORD_ID, ColumnIdentifier.RECORD_LENGTH,ColumnIdentifier.DIAGONAL_IDENTIFIER,
//                                ColumnIdentifier.START,ColumnIdentifier.RECORD_START,ColumnIdentifier.LENGTH).
//                        join(d1,
//                                col(ColumnIdentifier.RECORD_ID).equalTo(d1.col(recordIdentifier)).
//                                        and(col(ColumnIdentifier.DIAGONAL_IDENTIFIER).equalTo(d1.col(diagonalIdentifier)))
//                        ).
//                        select(col(ColumnIdentifier.RECORD_ID),
//                                col(ColumnIdentifier.RECORD_LENGTH),
//                                col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
//                                col(ColumnIdentifier.START), col(ColumnIdentifier.END),
//                                col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_END),
//                                col(ColumnIdentifier.LENGTH).plus(col(lengthIdentifier)).as(ColumnIdentifier.LENGTH)
//                        ).
//                        union(cyclicDiagonals.filter(col(ColumnIdentifier.REARRANGE_FLAG).
//                                equalTo(FlaggedRecordDiagonalBiPositionRangeDT.NON_CYCLIC_FLAG)).
//                                select(col(ColumnIdentifier.RECORD_ID),
//                                        col(ColumnIdentifier.RECORD_LENGTH),
//                                        col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
//                                        col(ColumnIdentifier.START), col(ColumnIdentifier.END),
//                                        col(ColumnIdentifier.RECORD_START), col(ColumnIdentifier.RECORD_END),
//                                        col(ColumnIdentifier.LENGTH)
//                                )
//                        ));
//
//    }

    /**** Weights *************************************************************************************************************************************************************************************************/

    private static Dataset<Row> computeLengthDependentWeights(Dataset<? extends RecordDiagonalPositionRangeDT> dataset, double weight) {
             return dataset.withColumn("normalized",
                col(ColumnIdentifier.LENGTH).cast(DataTypes.LongType).multiply(weight).multiply(col(ColumnIdentifier.LENGTH))).
                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).
                agg(
                        sum(col("normalized")).as(ColumnIdentifier.WEIGHT),
                        sum(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.SUM),
                        avg(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MEAN));
    }

    private Dataset<Row> computeLengthDependentWeights(Dataset<RecordDiagonalBiPositionRangeDT> dataset) {
        final String normWeightIdentifier = "normalizedWeight";

        Dataset<RecordDiagonalBiPositionRangeDT> dataset2 = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                dataset.filter(col(ColumnIdentifier.RECORD_LENGTH).equalTo(-1)).
                        drop(ColumnIdentifier.RECORD_LENGTH).
                        join(recordDTDataset.withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.RECORD_LENGTH).
                                        withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                                dataset.col(ColumnIdentifier.RECORD_ID).
                                        equalTo(col("r")))).
                union(
                        RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                                dataset.filter(col(ColumnIdentifier.RECORD_LENGTH).notEqual(-1))));
//        dataset2.
//                withColumn(normfactorIdentifier,least(col(ColumnIdentifier.RECORD_LENGTH),lit(sequenceInformation.getLength())).cast(DataTypes.DoubleType)).
//                withColumn(normWeightIdentifier,
//                        col(ColumnIdentifier.LENGTH).divide(col(normfactorIdentifier)).multiply(col(ColumnIdentifier.LENGTH))).
//                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND).
//                agg(
//                        sum(col(normWeightIdentifier)).as(ColumnIdentifier.WEIGHT)).
//                show();

//        dataset2.
//                withColumn(ColumnIdentifier.MIN_RECORD_LENGTH,least(col(ColumnIdentifier.RECORD_LENGTH),lit(sequenceInformation.getLength())).cast(DataTypes.DoubleType)).
//                withColumn(normWeightIdentifier,
//                        col(ColumnIdentifier.LENGTH).cast(DataTypes.LongType).multiply(col(ColumnIdentifier.LENGTH))).
//                show(500);
//        return dataset2.
//                withColumn(ColumnIdentifier.MIN_RECORD_LENGTH,least(col(ColumnIdentifier.RECORD_LENGTH),lit(sequenceInformation.getLength())).cast(DataTypes.DoubleType)).
//                withColumn(normWeightIdentifier,
//                col(ColumnIdentifier.LENGTH).divide(col(ColumnIdentifier.MIN_RECORD_LENGTH)).multiply(col(ColumnIdentifier.LENGTH))).
//                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.MIN_RECORD_LENGTH).
//                agg(
//                        (sum(col(normWeightIdentifier)).multiply(100)).divide(col(ColumnIdentifier.MIN_RECORD_LENGTH)).as(ColumnIdentifier.WEIGHT));
        return dataset2.
                withColumn(ColumnIdentifier.MIN_RECORD_LENGTH,least(col(ColumnIdentifier.RECORD_LENGTH),lit(sequenceInformation.getLength())).cast(DataTypes.DoubleType)).
                withColumn(normWeightIdentifier,
                        col(ColumnIdentifier.LENGTH).divide(col(ColumnIdentifier.MIN_RECORD_LENGTH)).
                                multiply(col(ColumnIdentifier.LENGTH)).divide(col(ColumnIdentifier.MIN_RECORD_LENGTH))).
                groupBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.MIN_RECORD_LENGTH).
                agg(
                        round((sum(col(normWeightIdentifier)).multiply(100)),6).as(ColumnIdentifier.WEIGHT),
                        round(mean(col(ColumnIdentifier.LENGTH)),2).as(ColumnIdentifier.MEAN_LENGTH),
                        max(col(ColumnIdentifier.LENGTH)).as(ColumnIdentifier.MAX),
                        round(sum(col(ColumnIdentifier.LENGTH)).divide(col(ColumnIdentifier.MIN_RECORD_LENGTH)).multiply(100),6).
                                as(ColumnIdentifier.COUNT)
                        );
    }

    public static Dataset<Row> computeDiagonalStatistic(Dataset<DiagonalWeightDT> diagonalWeights) {
        return diagonalWeights.groupBy(ColumnIdentifier.RECORD_ID).
                agg(    count(col(ColumnIdentifier.DIAGONAL_IDENTIFIER)).as(ColumnIdentifier.COUNT),
                        round(avg(col(ColumnIdentifier.DIAGONAL_IDENTIFIER)),2).as(ColumnIdentifier.DIAGONAL_MEAN),
                        round(stddev_samp(col(ColumnIdentifier.DIAGONAL_IDENTIFIER)),2).as(ColumnIdentifier.DIAGONAL_STD_DEVIATION),
//                        round(kurtosis(col(ColumnIdentifier.DIAGONAL_IDENTIFIER)),2).as(ColumnIdentifier.DIAGONAL_KURTOSIS),
//                        round(skewness(col(ColumnIdentifier.DIAGONAL_IDENTIFIER)),2).as(ColumnIdentifier.DIAGONAL_SKEWNESS),
                        round(avg(col(ColumnIdentifier.WEIGHT)),2).as(ColumnIdentifier.WEIGHT_MEAN),
                        round(stddev_samp(col(ColumnIdentifier.WEIGHT)),2).as(ColumnIdentifier.WEIGHT_STD_DEVIATION));
//                        round(kurtosis(col(ColumnIdentifier.WEIGHT)),2).as(ColumnIdentifier.WEIGHT_KURTOSIS),
//                        round(skewness(col(ColumnIdentifier.WEIGHT)),2).as(ColumnIdentifier.WEIGHT_SKEWNESS));
    }

    public static Dataset<DiagonalWeightDT> computeDiagonalWeights(
            Dataset<RecordDiagonalBiPositionRangeDT> diagonals,
            Dataset<RecordDiagonalPositionRangeDT> inverseDiagonals,
            double diagonalWeight, double inverseDiagonalWeight,
            BiFunction<Dataset<? extends RecordDiagonalPositionRangeDT>, Double,Dataset<Row>> weightComputer) {
        Dataset<Row> diagonalWeights = weightComputer.apply(diagonals,diagonalWeight).
                withColumnRenamed(ColumnIdentifier.SUM,ColumnIdentifier.HIT_COUNT).
                withColumnRenamed(ColumnIdentifier.MEAN,ColumnIdentifier.RANGE_LENGTH_MEAN);
//        diagonalWeights.show();

        Dataset<Row> inverseWeights = weightComputer.apply(inverseDiagonals,inverseDiagonalWeight).
                withColumnRenamed(ColumnIdentifier.RECORD_ID,ColumnIdentifier.INVERSE_RECORD_ID).
                withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,ColumnIdentifier.INVERSE_DIAGONAL_VALUE).
                withColumnRenamed(ColumnIdentifier.WEIGHT,ColumnIdentifier.INVERSE_WEIGHT).
                withColumnRenamed(ColumnIdentifier.SUM,ColumnIdentifier.MISS_COUNT).
                withColumnRenamed(ColumnIdentifier.MEAN,ColumnIdentifier.INVERSE_RANGE_LENGTH_MEAN);
//        inverseWeights.show();

        Dataset<Row> joined = diagonalWeights.join(inverseWeights,
                col(ColumnIdentifier.RECORD_ID).equalTo(col(ColumnIdentifier.INVERSE_RECORD_ID)).
                and(col(ColumnIdentifier.DIAGONAL_IDENTIFIER).equalTo(col(ColumnIdentifier.INVERSE_DIAGONAL_VALUE))),"left").
                orderBy(ColumnIdentifier.DIAGONAL_IDENTIFIER);


        return joined.select(
                col(ColumnIdentifier.RECORD_ID),
                col(ColumnIdentifier.DIAGONAL_IDENTIFIER),
                round(col(ColumnIdentifier.WEIGHT).
                        plus(col(ColumnIdentifier.INVERSE_WEIGHT))).
                        as(ColumnIdentifier.WEIGHT),
                col(ColumnIdentifier.HIT_COUNT),
                col(ColumnIdentifier.MISS_COUNT),
                col(ColumnIdentifier.RANGE_LENGTH_MEAN),
                col(ColumnIdentifier.INVERSE_RANGE_LENGTH_MEAN)).
                as(DiagonalWeightDT.ENCODER);
    }

    /**** Gap bridging *************************************************************************************************************************************************************************************************/
    private void updateScores(int startPos,int endPos, int startPos2, int endPos2,
                              int recordId, boolean recordStrand,
                              Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap) {
//        LOGGER.debug("RecordId " + recordId + " [" + startPos + "," + endPos + "]" + " [" + startPos2 + "," + endPos2 + "] " + recordStrand);
        Sequence s1 = null;
        Sequence s2 = null;
        try {
            String s1Str = sequenceSubstringComputer.apply(startPos,endPos);
            String s2Str = recordSubstringComputer.apply(recordId,startPos2,endPos2,recordStrand);

            if(s1Str.length() > parameter.maxSequenceLength || s2Str.length() > parameter.maxSequenceLength
                    || s1Str.length() < parameter.minSequenceLength || s2Str.length() < parameter.minSequenceLength) {
                LOGGER.debug("sequence size " + s1Str.length() + " " + s2Str.length());
                return;
            }
            if(s1Str.length() <= parameter.toggleSequenceLength && s2Str.length() <= parameter.toggleSequenceLength) {
                s1 = new Sequence(s1Str,true,sequenceId,startPos);
                s2 = new Sequence(s2Str,recordStrand,recordId,startPos2);
            }
            else {
//                LOGGER.debug("Banded "+ (int)Math.ceil(s1Str.length()*parameter.bandSizeFactor) + " " +(int)Math.ceil(s2Str.length()*parameter.bandSizeFactor));
                s1 = new BandedSequence(s1Str,true,sequenceId,startPos,(int)Math.ceil(s1Str.length()*parameter.bandSizeFactor));
                s2 = new BandedSequence(s2Str,recordStrand,recordId,startPos2,(int)Math.ceil(s2Str.length()*parameter.bandSizeFactor));
            }

        } catch (Exception throwables) {
            throwables.printStackTrace();
            throw new RuntimeException(throwables);
        }

//        LOGGER.debug(s1.getSequence() +" " + s1.getSequenceIdentifier());
//        LOGGER.debug(s2.getSequence()+ " "+ s2.getSequenceIdentifier());



        List<ScoredPositionPositionKmerMappingDT> oldScores = scoreMap.get(recordId);
        sequenceAligner.setSequences(s1,s2);
        List<ScoredPositionPositionKmerMappingDT> newScores = null;
        SequenceAligner.Alignment alignment = sequenceAligner.getAlignment();
        if(alignment != null) {
            newScores = sequenceAligner.getAlignment().getPositionMappings();
        }


        // if alignment is sufficiently good
        if(newScores != null && sequenceAligner.geteValue() <= parameter.eValueThreshold &&
                (oldScores == null ||(oldScores != null && oldScores.get(0).getScore() > sequenceAligner.geteValue())) )  {
            scoreMap.put(recordId,newScores);
//            System.out.println(newScores.get(0).isStrand());
//            LOGGER.debug(sequenceAligner.getBestScore() + " " + String.format("%e", + sequenceAligner.geteValue()));
        }
//        else {
//            LOGGER.debug("Skipping too bad value " + sequenceAligner.geteValue());
//        }
    }

    private List<ScoredPositionPositionKmerMappingDT> getPositionMappings(Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap) {
        return scoreMap.values().stream().flatMap(l -> l.stream()).collect(Collectors.toList());
    }

    private Dataset<ScoredPositionPositionKmerMappingDT> getPositionMappingsDataset(Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap) {
        return SparkComputer.createDataFrame(getPositionMappings(scoreMap), ScoredPositionPositionKmerMappingDT.ENCODER);
    }

    public Dataset<ScoredPositionPositionKmerMappingDT> expandToRight(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range, int endPos) {

        int sequenceLength = getSequenceInformation().getLength();
        int gapSize = Auxiliary.mod(endPos-range.getEnd(),sequenceLength);
        LOGGER.debug("Gapsize " + gapSize + " endPos: " + endPos);

//        SparkComputer.showAll(recordDiagonalRange.
//                filter(lit(range.getEnd()).cast(DataTypes.IntegerType).minus(col(ColumnIdentifier.END)).leq(((double)parameter.gapSizeFactor*gapSize)/2).
//                        and(col(ColumnIdentifier.LENGTH).gt(lit(parameter.minRangeLength).cast(DataTypes.IntegerType)))
//                ));
        // if record range suffieciently close to range end
        Iterator<RecordDiagonalBiPositionRangeDT> bridgeCandidatesIt = recordDiagonalRange.
                filter(pmod(lit(range.getEnd()).cast(DataTypes.IntegerType).minus(col(ColumnIdentifier.END)),lit(sequenceLength).cast(DataTypes.IntegerType)).
                        leq(((double)(parameter.gapSizeFactor*gapSize-gapSize))/2).
                        and(col(ColumnIdentifier.LENGTH).gt(lit(parameter.minSmallRangeLength).cast(DataTypes.IntegerType)))
                ).toLocalIterator();
        Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap = new HashMap<>();


        while (bridgeCandidatesIt.hasNext()) {
            RecordDiagonalBiPositionRangeDT bridgeCandidate = bridgeCandidatesIt.next();

            boolean strand = bridgeCandidate.isStrand();
            int recordId = bridgeCandidate.getRecordId();
            int startPos = Auxiliary.mod(bridgeCandidate.getEnd()+1,sequenceLength);
            int posDiff = endPos-startPos;
            int diff = Auxiliary.mod(posDiff,sequenceLength);
            int recordLength = bridgeCandidate.getRecordLength();
            int startPos2;
            int endPos2;
                    //(int)(Math.ceil (((posDiff % sequenceLength)%sequenceLength)*(1+parameter.diagonalDiffPercentage)));
            if(recordLength > -1) {
                startPos2 = Auxiliary.mod(bridgeCandidate.getRecordStart()+1,recordLength);
                endPos2 = Auxiliary.mod(startPos2+diff,recordLength);
            }
            else {
                startPos2 = bridgeCandidate.getRecordStart()+1;
                endPos2 = startPos2+diff;
            }


            updateScores(startPos,endPos,startPos2,endPos2,recordId,strand,scoreMap);

        }
        return getPositionMappingsDataset(scoreMap);
    }

    public Dataset<ScoredPositionPositionKmerMappingDT> expandToRight(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range) {
            return expandToRight(recordDiagonalRange,range,getSequenceInformation().getLength()-1);
    }



    public Dataset<ScoredPositionPositionKmerMappingDT> expandToLeft(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range, int startPos) {


        int sequenceLength = getSequenceInformation().getLength();
        int gapSize = (((range.getStart()-startPos) %sequenceLength) +sequenceLength) %sequenceLength;
        LOGGER.debug("Gapsize " + gapSize);

//        SparkComputer.showAll( recordDiagonalRange.
//                filter(col(ColumnIdentifier.START).minus(lit(range.getStart()).cast(DataTypes.IntegerType)).
//                        leq(((double)parameter.gapSizeFactor*gapSize)/2).
//                        and(col(ColumnIdentifier.LENGTH).gt(lit(parameter.minRangeLength).cast(DataTypes.IntegerType)))
//                ));

        // if record range sufficiently close to range start
        Iterator<RecordDiagonalBiPositionRangeDT> bridgeCandidatesIt = recordDiagonalRange.
                filter(pmod(col(ColumnIdentifier.START).minus(lit(range.getStart()).cast(DataTypes.IntegerType)),lit(sequenceLength).cast(DataTypes.IntegerType)).
                        leq(((double)parameter.gapSizeFactor*gapSize)/2).
                        and(col(ColumnIdentifier.LENGTH).gt(lit(parameter.minSmallRangeLength).cast(DataTypes.IntegerType)))
                        ).toLocalIterator();


        Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap = new HashMap<>();
        while (bridgeCandidatesIt.hasNext()) {
            RecordDiagonalBiPositionRangeDT bridgeCandidate = bridgeCandidatesIt.next();

            boolean strand = bridgeCandidate.isStrand();
            int recordId = bridgeCandidate.getRecordId();
            int endPos = Auxiliary.mod(bridgeCandidate.getStart()-1,sequenceLength);
            int diff = Auxiliary.mod(endPos-startPos,sequenceLength);
            int recordLength = bridgeCandidate.getRecordLength();
                    //(int)(Math.ceil ((((posDiff % sequenceLength)+sequenceLength)%sequenceLength)*(1+parameter.diagonalDiffPercentage)));
            int endPos2; //= bridgeCandidate.getRecordStart()-1;
            int startPos2;
            if(recordLength > -1) { // cyclic
                endPos2 = Auxiliary.mod(bridgeCandidate.getRecordStart()-1,recordLength);
                startPos2 = Auxiliary.mod(endPos2-diff,recordLength);
            }
            else { // linear
                endPos2 = Math.max(0,bridgeCandidate.getRecordStart()-1);
                startPos2 = Math.max(0,endPos2-diff);
            }
            updateScores(startPos,endPos,startPos2,endPos2,recordId,strand,scoreMap);
        }
        return getPositionMappingsDataset(scoreMap);
    }

    public Dataset<ScoredPositionPositionKmerMappingDT> expandToLeft(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range) {

        return expandToLeft(recordDiagonalRange,range,0);
    }



    public Dataset<ScoredPositionPositionKmerMappingDT> expandToBothSides(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range
    ) {
        Dataset<ScoredPositionPositionKmerMappingDT> scores = null;
        if(!parameter.gapTooLong(range,getLeftBoundaryGapSize(range))) {
            LOGGER.debug("Gap is too long to bridge");
            scores = expandToLeft(recordDiagonalRange,range);
        }

        if(scores != null && !scores.isEmpty() && !parameter.gapTooLong(range,getRightBoundaryGapSize(range))) {
            return expandToRight(recordDiagonalRange,range).union(scores);
        }
        return expandToRight(recordDiagonalRange,range);

    }

    public Dataset<ScoredPositionPositionKmerMappingDT> expandToBothSides(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range,
                                                                          int startPos, int endPos
    ) {

        return expandToRight(recordDiagonalRange,range,endPos).
                union(expandToLeft(recordDiagonalRange,range,startPos));
    }

    public Dataset<Row> computeBridgeCandidates(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange1,
                                                Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange2
    ) {


        final String recordRangeDiff = "recordRangeDiff";
        int sequenceLength = sequenceInformation.getLength();
        return recordDiagonalRange1.join(
                recordDiagonalRange2.
                        withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
                        withColumnRenamed(ColumnIdentifier.RECORD_LENGTH,"l").
                        withColumnRenamed(ColumnIdentifier.STRAND,"s").
                        withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,ColumnIdentifier.DIAGONAL_IDENTIFIER_2).
                        withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.START_2).
                        withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.END_2).
                        withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH_2).
                        withColumnRenamed(ColumnIdentifier.RECORD_START,ColumnIdentifier.RECORD_START_2).
                        withColumnRenamed(ColumnIdentifier.RECORD_END,ColumnIdentifier.RECORD_END_2)
                ,
                col("r").equalTo(col(ColumnIdentifier.RECORD_ID)).
                        and(col("s").equalTo(col(ColumnIdentifier.STRAND))),
                SparkComputer.JOIN_TYPES.INNER
        ).
                withColumn(recordRangeDiff,
                        when(col(ColumnIdentifier.RECORD_LENGTH).notEqual(-1),
                                pmod(col(ColumnIdentifier.RECORD_START_2).minus(col(ColumnIdentifier.RECORD_END)),col(ColumnIdentifier.RECORD_LENGTH))).
                                otherwise(col(ColumnIdentifier.RECORD_START_2).minus(col(ColumnIdentifier.RECORD_END_2)))).
                withColumn(ColumnIdentifier.RANGE_DIFF,pmod(col(ColumnIdentifier.START_2).minus(col(ColumnIdentifier.END)),
                        lit(sequenceLength).cast(DataTypes.IntegerType))).
                withColumn(ColumnIdentifier.DIAGONAL_DIFF,
                        abs(col(recordRangeDiff).minus(col(ColumnIdentifier.RANGE_DIFF)))
                ).
                withColumn(ColumnIdentifier.RANGE_LENGTH_MEAN,col(ColumnIdentifier.LENGTH_2).plus(col(ColumnIdentifier.LENGTH)).divide(2)).

                drop("r","l","s",recordRangeDiff).orderBy(ColumnIdentifier.RANGE_DIFF);
    }


    public Dataset<ScoredPositionPositionKmerMappingDT> bridgeGapSelfJoin(Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange, RangeDT range, RangeDT gap
    ) {
        if(parameter.gapTooLong(range,gap.getLength())) {
            LOGGER.debug("Gap is too long to bridge");
            return null;
        }
        final String recordRangeDiff = "recordRangeDiff";
        int sequenceLength = sequenceInformation.getLength();
        int gapSize = gap.getLength();
        //sequenceInformation.getLength()-range.getLength();
        LOGGER.debug("gap " + gap);
        LOGGER.debug("Gap size " + gapSize);
        LOGGER.debug("Paramerter: "+ parameter.gapSizeFactor*gapSize);
//        recordDiagonalRange.show();
        Dataset<RecordDiagonalBiPositionRangeDT> recordDiagonalRange2 = recordDiagonalRange.
                filter(pmod(lit(gap.getEnd()).cast(DataTypes.IntegerType).
                                minus(col(ColumnIdentifier.END)),
                        lit(sequenceLength).cast(DataTypes.IntegerType)).
                        leq(parameter.gapSizeFactor*gapSize));
//        recordDiagonalRange2.show();
//                withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
//                withColumnRenamed(ColumnIdentifier.RECORD_LENGTH,"l").
//                withColumnRenamed(ColumnIdentifier.STRAND,"s").
//                withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,ColumnIdentifier.DIAGONAL_IDENTIFIER_2).
//                withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.START_2).
//                withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.END_2).
//                withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.LENGTH_2).
//                withColumnRenamed(ColumnIdentifier.RECORD_START,ColumnIdentifier.RECORD_START_2).
//                withColumnRenamed(ColumnIdentifier.RECORD_END,ColumnIdentifier.RECORD_END_2);
//        recordDiagonalRange2.show();

        recordDiagonalRange = recordDiagonalRange.filter(pmod(
                        col(ColumnIdentifier.START).minus(lit(gap.getStart()).cast(DataTypes.IntegerType)),
                lit(sequenceLength).cast(DataTypes.IntegerType)).
                leq(parameter.gapSizeFactor*gapSize));

//        recordDiagonalRange.show();
//        Dataset<Row> bridgeCandidates = recordDiagonalRange.join(recordDiagonalRange2,
//                col("r").equalTo(col(ColumnIdentifier.RECORD_ID)).
//                        and(col("s").equalTo(col(ColumnIdentifier.STRAND))).
//                        and(col(ColumnIdentifier.START).notEqual(col(ColumnIdentifier.START_2))).
//                        //range diff
//                        and(pmod(col(ColumnIdentifier.START_2).minus(col(ColumnIdentifier.END)),
//                                lit(sequenceLength).cast(DataTypes.IntegerType)).
//                        leq(parameter.gapSizeFactor*gapSize))
//
//                ,
//                SparkComputer.JOIN_TYPES.INNER).
//                withColumn(recordRangeDiff,
//                        when(col(ColumnIdentifier.RECORD_LENGTH).notEqual(-1),
//                                pmod(col(ColumnIdentifier.RECORD_START_2).minus(col(ColumnIdentifier.RECORD_END)),col(ColumnIdentifier.RECORD_LENGTH))).
//                                otherwise(col(ColumnIdentifier.RECORD_START_2).minus(col(ColumnIdentifier.RECORD_END_2)))).
//                withColumn(ColumnIdentifier.RANGE_DIFF,pmod(col(ColumnIdentifier.START_2).minus(col(ColumnIdentifier.END)),
//                        lit(sequenceLength).cast(DataTypes.IntegerType))).
//                withColumn(ColumnIdentifier.DIAGONAL_DIFF,
//                        abs(col(recordRangeDiff).minus(col(ColumnIdentifier.RANGE_DIFF)))
//                ).
//                withColumn(ColumnIdentifier.RANGE_LENGTH_MEAN,col(ColumnIdentifier.LENGTH_2).plus(col(ColumnIdentifier.LENGTH)).divide(2)).
//
//                drop("r","l","s",recordRangeDiff);

//                .filter(
//                // distance between sequence ranges is sufficiently small
//                col(ColumnIdentifier.RANGE_DIFF).leq(parameter.gapSizeFactor*gapSize)
//                        // mean range sizes over range lengths on either side of the gap are sufficiently large
//                        .and(col(ColumnIdentifier.RANGE_LENGTH_MEAN).gt(lit(parameter.minRangeLength).cast(DataTypes.IntegerType))).
//                        // difference of the distance between record ranges and sequence ranges is only a small fraction of the distance of the sequence ranges
//                                and(col(ColumnIdentifier.DIAGONAL_DIFF).leq(col(ColumnIdentifier.RANGE_DIFF).
//                                multiply(lit(parameter.diagonalDiffPercentage)).cast(DataTypes.DoubleType))));

//        bridgeCandidates.show();
//        System.out.println(bridgeCandidates.count());

//        return expandToRight(recordDiagonalRange,range).
//                union(expandToLeft(recordDiagonalRange,range));


//        return bridgeGap(bridgeCandidates,gapSize);
        return bridgeGap(computeBridgeCandidates(recordDiagonalRange2,recordDiagonalRange),gapSize);
    }

    public Dataset<ScoredPositionPositionKmerMappingDT> bridgeGap(Dataset<Row> bridgeCandidates, int gapSize) throws RuntimeException {

        LOGGER.debug("Parameters used: \n" + parameter);

//        System.out.println("previous filter");
//        bridgeCandidates.show();
        LOGGER.debug("Bridging gap of size " + gapSize);
        if(parameter.gapSizeFactor == parameter.defaultGapSizeFactor) {
            // only retain candidates that correspond to ranges not far from the respective ends
            bridgeCandidates = bridgeCandidates.filter(
                    // distance between sequence ranges is sufficiently small
                    col(ColumnIdentifier.RANGE_DIFF).leq(parameter.gapSizeFactor*gapSize).
                    // mean range sizes over range lengths on either side of the gap are sufficiently large
                    and(col(ColumnIdentifier.RANGE_LENGTH_MEAN).gt(lit(parameter.minSmallRangeLength).cast(DataTypes.IntegerType))).
                            // difference of the distance between record ranges and sequence ranges is only a small fraction of the distance of the sequence ranges
                                    and(col(ColumnIdentifier.DIAGONAL_DIFF).leq(col(ColumnIdentifier.RANGE_DIFF).
                                    multiply(lit(parameter.diagonalDiffPercentage)).cast(DataTypes.DoubleType))));
        }
        else {
            // only retain candidates that correspond to ranges not far from the respective ends
            bridgeCandidates = bridgeCandidates.filter(
                    // distance between sequence ranges is sufficiently small
                    col(ColumnIdentifier.RANGE_DIFF).leq(parameter.gapSizeFactor*gapSize).
                            and(col(ColumnIdentifier.RANGE_DIFF).gt((parameter.gapSizeFactor-1)*gapSize)).
                    // mean range sizes over range lengths on either side of the gap are sufficiently large
                    and(col(ColumnIdentifier.RANGE_LENGTH_MEAN).gt(lit(parameter.minSmallRangeLength).cast(DataTypes.IntegerType))).
                    // difference of the distance between record ranges and sequence ranges is only a small fraction of the distance of the sequence ranges
                            and(col(ColumnIdentifier.DIAGONAL_DIFF).leq(col(ColumnIdentifier.RANGE_DIFF).
                            multiply(lit(parameter.diagonalDiffPercentage)).cast(DataTypes.DoubleType))));
        }


//        bridgeCandidates.filter(col(ColumnIdentifier.RECORD_ID).equalTo(23531).and(col(ColumnIdentifier.START).equalTo(16480).or(col(ColumnIdentifier.START).equalTo(16478)))).show();
//        SparkComputer.showAll(bridgeCandidates.filter(col(ColumnIdentifier.RECORD_ID).equalTo(23531)));
//        SparkComputer.showAll(bridgeCandidates);

//        LOGGER.debug("Filtered:" +bridgeCandidates.count());
//        Iterator<Row> bridgeCandidatesIt = bridgeCandidates.toLocalIterator();
        Iterator<Row> bridgeCandidatesIt = bridgeCandidates.limit(50000).toLocalIterator();
        Map<Integer,List<ScoredPositionPositionKmerMappingDT>> scoreMap = new HashMap<>();
//        System.out.println("created map " + bridgeCandidates.count() );
        while (bridgeCandidatesIt.hasNext()) {

            Row bridgeCandidate = bridgeCandidatesIt.next();
            int recordLength = bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_LENGTH));
            RangeDT range1 = new RangeDT(
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.START)),
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.END)));
            RangeDT range2 = new RangeDT(
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.START_2)),
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.END_2)));
//            LOGGER.debug("Sequence ranges to bridge: " + range1 + " " + range2);
            if(containedRanges(range1, range2)) {
                LOGGER.debug("Filter contained sequence range");
                continue;
            }
            RangeDT recRange1 = new RangeDT(
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_START)),
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_END)));
            RangeDT recRange2 = new RangeDT(
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_START_2)),
                    bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_END_2)));
//            LOGGER.debug("Record ranges to bridge: " + recRange1 + " " + recRange2);
            if(containedRanges(recRange1,recRange2,recordLength)) {
                LOGGER.debug("Filter contained record range");
                continue;
            }
//            if(Auxiliary.mod(oldStart2-oldEnd,sequenceInformation.getLength()))
            boolean strand = bridgeCandidate.getBoolean(bridgeCandidate.fieldIndex(ColumnIdentifier.STRAND));
            int recordId = bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_ID));
            int startPos = Auxiliary.mod(bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.END))+1,sequenceInformation.getLength());
            int endPos = Auxiliary.mod(bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.START_2))-1,sequenceInformation.getLength());

            int startPos2 = bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_END))+1;
            int endPos2 = bridgeCandidate.getInt(bridgeCandidate.fieldIndex(ColumnIdentifier.RECORD_START_2))-1;
            if(recordLength != -1) {
                startPos2 = Auxiliary.mod(startPos2,recordLength);
                endPos2 = Auxiliary.mod(endPos2,recordLength);
            }


            updateScores(startPos,endPos,startPos2,endPos2,recordId,strand,scoreMap);

        }

        return getPositionMappingsDataset(scoreMap);
    }



    public <T> Dataset<T> linearizeRecordRanges(Dataset<T> cyclicRanges, Encoder<T> tEncoder) {
        final Column[] diagonalColumns = SparkComputer.getColumns(cyclicRanges.columns());
        return cyclicRanges.
                drop(ColumnIdentifier.RECORD_END).
                withColumn(ColumnIdentifier.RECORD_END,col(ColumnIdentifier.RECORD_LENGTH).minus(1)).
                select(diagonalColumns).
                union(
                        cyclicRanges.drop(ColumnIdentifier.RECORD_START).
                                withColumn(ColumnIdentifier.RECORD_START, lit(0).cast(DataTypes.IntegerType)).
                                select(diagonalColumns)
                ).as(tEncoder);
    }

//    public <T> Dataset<T> linearizeSequenceRanges(Dataset<T> cyclicRanges, Encoder<T> tEncoder) {
//        final Column[] diagonalColumns = SparkComputer.getColumns(cyclicRanges.columns());
//        return cyclicRanges.
//                drop(ColumnIdentifier.RECORD_END).
//                withColumn(ColumnIdentifier.RECORD_END,col(ColumnIdentifier.RECORD_LENGTH).minus(1)).
//                select(diagonalColumns).
//                union(
//                        cyclicRanges.drop(ColumnIdentifier.RECORD_START).
//                                withColumn(ColumnIdentifier.RECORD_START, lit(0).cast(DataTypes.IntegerType))
//                ).as(tEncoder);
//    }

    public List<Dataset<Row>> fetchAggregatedProperties(Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange) {
//38703
//        diagonalRange = diagonalRange.limit(10);
        RecordPropertyTable recordPropertyTable = RecordPropertyTable.getInstance();
//        System.out.println(diagonalRange.groupBy(ColumnIdentifier.RECORD_ID).count().count());
//        List<Integer> fiterRecords = new ArrayList<>(diagonalRange.groupBy(ColumnIdentifier.RECORD_ID).count().
//                select(col(ColumnIdentifier.RECORD_ID)).as(Encoders.INT()).takeAsList(100));
//        fiterRecords.add(844);
//        diagonalRange = diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).isInCollection(fiterRecords)).
//                orderBy(ColumnIdentifier.RECORD_START);
//        SparkComputer.showAll(diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).equalTo(844)));
//        diagonalRange.show();
//        SparkComputer.showAll(diagonalRange);
//        if(shift) {
//            diagonalRange = decreaseRangeStart(diagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER);
//        }
        //.orderBy(ColumnIdentifier.RECORD_ID).limit(10);

        // make sure any cyclic

        // reshift record ranges with negative strand
        diagonalRange = shiftRangesToStrand(diagonalRange);
//        System.out.println("After shift");
//        SparkComputer.showAll(diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).equalTo(844)));
        Dataset<RecordDiagonalBiPositionRangeDT> cyclicRanges = diagonalRange.filter(col(ColumnIdentifier.RECORD_START).
                gt(ColumnIdentifier.RECORD_END));
        if(!cyclicRanges.isEmpty()) {
            LOGGER.debug("Found cyclic record input ranges");
            diagonalRange = diagonalRange.except(cyclicRanges);
            cyclicRanges = linearizeRecordRanges(cyclicRanges,RecordDiagonalBiPositionRangeDT.ENCODER);
//            diagonalRange = diagonalRange.union(cyclicRanges);
        }

        // in first list position are properties for final plus annotations (based on db plus matches) -> look up in plus property table,
        // in second list position are properties for final minus annotations (based on db plus matches) -> look up in minus property table
        List<Dataset<Row>> properties = recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
                diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(true)),sequenceInformation.getLength(),"tab1");
        // in first list position are properties for final minus annotations (based on db minus matches) -> look up in plus property table,
        // in second list position are properties for final plus annotations (based on db minus matches) -> look up in minus property table
        List<Dataset<Row>> propertiesReversed = recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
                diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(false)),sequenceInformation.getLength(),"tab2");
        // now first list position are properties for final plus annotations, second list position are properties for final minus annotations
        properties.set(0, properties.get(0).union(propertiesReversed.get(1)));
        properties.set(1, properties.get(1).union(propertiesReversed.get(0)));

        if(!cyclicRanges.isEmpty()) {
            // in first list position are properties for final plus annotations (based on db plus matches) -> look up in plus property table,
            // in second list position are properties for final minus annotations (based on db plus matches) -> look up in minus property table
            List<Dataset<Row>> propertiesCyclic = recordPropertyTable.getAggregatedPropertiesFromCyclicRecordRanges(
                    diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(true)),sequenceInformation.getLength(),"tab3");
            // in first list position are properties for final minus annotations (based on db minus matches) -> look up in plus property table,
            // in second list position are properties for final plus annotations (based on db minus matches) -> look up in minus property table
            List<Dataset<Row>> propertiesReversedCyclic = recordPropertyTable.getAggregatedPropertiesFromCyclicRecordRanges(
                    diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(false)),sequenceInformation.getLength(),"tab4");


            // now first list position are properties for final plus annotations, second list position are properties for final minus annotations
            propertiesCyclic.set(0, propertiesCyclic.get(0).union(propertiesReversedCyclic.get(1)));
            propertiesCyclic.set(1, propertiesCyclic.get(1).union(propertiesReversedCyclic.get(0)));

            // now first list position are properties for final plus annotations, second list position are properties for final minus annotations
            properties.set(0, properties.get(0).union(propertiesCyclic.get(0)));
            properties.set(1, properties.get(1).union(propertiesCyclic.get(1)));
        }

//        properties.get(0).orderBy("start","end","property","recordId").show(150);
//        properties.get(1).orderBy("start","end","property","recordId").show(150);
//        for(boolean sequenceStrand: Arrays.asList(true,false)) {
//
//        }
//        Dataset<Row> properties = recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
//                sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)),sequenceInformation.getLength(),"t1");
//        properties = properties.union(recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
//                !sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(!sequenceStrand)),sequenceInformation.getLength(),"t2"));
//        if(!cyclicRanges.isEmpty()) {
//            Dataset<Row> cyclicAggregatedRanges =
//                    recordPropertyTable.
//                            getAggregatedPropertiesFromCyclicRecordRanges(sequenceStrand,
//                                    cyclicRanges.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)),sequenceInformation.getLength(),"t3");
//            cyclicAggregatedRanges.union(recordPropertyTable.
//                    getAggregatedPropertiesFromCyclicRecordRanges(!sequenceStrand,
//                            cyclicRanges.filter(col(ColumnIdentifier.STRAND).equalTo(!sequenceStrand)),
//                            sequenceInformation.getLength(),"t4") );
//            LOGGER.debug("Cyclic aggregated input ranges");
////            cyclicAggregatedRanges.show();
//            properties = properties.union(cyclicAggregatedRanges);
//        }
//
        for(int i = 0; i < 2; i++) {
            Dataset<Row> cyclicProperties = properties.get(i).filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END))).
                    select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                            ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY);

            if(!cyclicProperties.isEmpty()) {
                LOGGER.debug("Found cyclic input sequence ranges for " + i );
                properties.set(i,properties.get(i).select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).except(cyclicProperties));
                cyclicProperties = cyclicProperties.drop(ColumnIdentifier.END).
                        withColumn(ColumnIdentifier.END,lit(sequenceInformation.getLength()-1).cast(DataTypes.IntegerType)).
                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).
                        union(cyclicProperties.drop(ColumnIdentifier.START).
                                withColumn(ColumnIdentifier.START, lit(0)).
                                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY)
                        );
                properties.set(i, properties.get(i).
                        select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).union(cyclicProperties));
            }



            properties.set(i,properties.get(i).
                    join(propertyLengths.
                                    select(col(ColumnIdentifier.PROPERTY).as("p"),col(ColumnIdentifier.MEAN_LENGTH))
                            ,col(ColumnIdentifier.PROPERTY).equalTo(col("p")),SparkComputer.JOIN_TYPES.INNER).
                    withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.LENGTH). //cast(DataTypes.DoubleType).
                            divide(col(ColumnIdentifier.MEAN_LENGTH))).drop(ColumnIdentifier.LENGTH,ColumnIdentifier.MEAN_LENGTH));

        }

//        System.out.println("Before return");
//        properties.get(0).orderBy("start","end","property","recordId").show(150);
//        properties.get(1).orderBy("start","end","property","recordId").show(150);
        return properties;

    }

//    public Dataset<Row> fetchAggregatedProperties(Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange, boolean sequenceStrand) {
////38703
////        diagonalRange = diagonalRange.limit(10);
//        RecordPropertyTable recordPropertyTable = RecordPropertyTable.getInstance();
////        System.out.println(diagonalRange.groupBy(ColumnIdentifier.RECORD_ID).count().count());
////        List<Integer> fiterRecords = new ArrayList<>(diagonalRange.groupBy(ColumnIdentifier.RECORD_ID).count().
////                select(col(ColumnIdentifier.RECORD_ID)).as(Encoders.INT()).takeAsList(100));
////        fiterRecords.add(844);
////        diagonalRange = diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).isInCollection(fiterRecords)).
////                orderBy(ColumnIdentifier.RECORD_START);
////        SparkComputer.showAll(diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).equalTo(844)));
////        diagonalRange.show();
////        SparkComputer.showAll(diagonalRange);
////        if(shift) {
////            diagonalRange = decreaseRangeStart(diagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER);
////        }
//         //.orderBy(ColumnIdentifier.RECORD_ID).limit(10);
//
//        // make sure any cyclic
//
//        // reshift record ranges with negative strand
//        diagonalRange = shiftRangesToStrand(diagonalRange);
////        System.out.println("After shift");
////        SparkComputer.showAll(diagonalRange.filter(col(ColumnIdentifier.RECORD_ID).equalTo(844)));
//        Dataset<RecordDiagonalBiPositionRangeDT> cyclicRanges = diagonalRange.filter(col(ColumnIdentifier.RECORD_START).
//                gt(ColumnIdentifier.RECORD_END));
//        if(!cyclicRanges.isEmpty()) {
//            LOGGER.debug("Found cyclic record input ranges");
//            diagonalRange = diagonalRange.except(cyclicRanges);
//            cyclicRanges = linearizeRecordRanges(cyclicRanges,RecordDiagonalBiPositionRangeDT.ENCODER);
////            diagonalRange = diagonalRange.union(cyclicRanges);
//        }
//
//
////        Dataset<Row> properties = recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
////                sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)).collectAsList(),sequenceInformation.getLength());
////        properties = properties.union(recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
////                !sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(!sequenceStrand)).collectAsList(),sequenceInformation.getLength()));
//        Dataset<Row> properties = recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
//                sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)),sequenceInformation.getLength(),"t1");
//        properties = properties.union(recordPropertyTable.getAggregatedPropertiesFromRecordRanges(
//                !sequenceStrand,diagonalRange.filter(col(ColumnIdentifier.STRAND).equalTo(!sequenceStrand)),sequenceInformation.getLength(),"t2"));
//        if(!cyclicRanges.isEmpty()) {
//            Dataset<Row> cyclicAggregatedRanges =
//                    recordPropertyTable.
//                            getAggregatedPropertiesFromCyclicRecordRanges(sequenceStrand,
//                                    cyclicRanges.filter(col(ColumnIdentifier.STRAND).equalTo(sequenceStrand)),sequenceInformation.getLength(),"t3");
//            cyclicAggregatedRanges.union(recordPropertyTable.
//                    getAggregatedPropertiesFromCyclicRecordRanges(!sequenceStrand,
//                            cyclicRanges.filter(col(ColumnIdentifier.STRAND).equalTo(!sequenceStrand)),
//                            sequenceInformation.getLength(),"t4") );
//            LOGGER.debug("Cyclic aggregated input ranges");
////            cyclicAggregatedRanges.show();
//            properties = properties.union(cyclicAggregatedRanges);
//        }
//
//        Dataset<Row> cyclicProperties = properties.filter(col(ColumnIdentifier.START).gt(col(ColumnIdentifier.END))).
//                select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
//                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY);
//        if(!cyclicProperties.isEmpty()) {
//            LOGGER.debug("Found cyclic input sequence ranges");
//            properties = properties. select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
//                    ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).except(cyclicProperties);
//            cyclicProperties = cyclicProperties.drop(ColumnIdentifier.END).
//                    withColumn(ColumnIdentifier.END,lit(sequenceInformation.getLength()-1).cast(DataTypes.IntegerType)).
//                    select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
//                            ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).
//                    union(cyclicProperties.drop(ColumnIdentifier.START).
//                            withColumn(ColumnIdentifier.START, lit(0)).
//                            select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
//                                    ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY)
//                    );
//            properties = properties.
//                    select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
//                            ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY).union(cyclicProperties);
//        }
////        LOGGER.debug("properties before return");
////        properties.show();
////        SparkComputer.showAll(properties.filter(col(ColumnIdentifier.RECORD_ID).equalTo(844)).orderBy(ColumnIdentifier.START));
//        return properties.
//                join(propertyLengths.
//                                select(col(ColumnIdentifier.PROPERTY).as("p"),col(ColumnIdentifier.MEAN_LENGTH))
//                        ,col(ColumnIdentifier.PROPERTY).equalTo(col("p")),SparkComputer.JOIN_TYPES.INNER).
//                withColumn(ColumnIdentifier.WEIGHT,col(ColumnIdentifier.LENGTH). //cast(DataTypes.DoubleType).
//                divide(col(ColumnIdentifier.MEAN_LENGTH))).drop(ColumnIdentifier.LENGTH,ColumnIdentifier.MEAN_LENGTH);
////        return properties.withColumn(ColumnIdentifier.POSITION,explode(sequence(col(ColumnIdentifier.START),col(ColumnIdentifier.END)))).
////        groupBy(ColumnIdentifier.POSITION,ColumnIdentifier.PROPERTY,ColumnIdentifier.CATEGORY).
////                        agg(sum(col(ColumnIdentifier.LENGTH).divide(normRangeLength)).as(ColumnIdentifier.WEIGHT),
////                                count(ColumnIdentifier.PROPERTY).as(ColumnIdentifier.COUNT)).
////                        withColumn(ColumnIdentifier.RANK,rank().over(RECORD_PROPERTY_POSITION_FRAME)).
////                        orderBy(ColumnIdentifier.POSITION,ColumnIdentifier.RANK);
//
//    }


    public GapStatisticDT evaluateBridging(boolean bridged) {
        if(! bridged) {
            Dataset<Integer> gapPositions = RangeDT.createPositions(SparkComputer.createDataFrame(inverseRanges,RangeDT.ENCODER));
            GapStatisticDT gapStatistic = new GapStatisticDT();
            long count = gapPositions.count();
            gapStatistic.setGapCount(count);
            gapStatistic.setUnbridgedCount(count);
            return gapStatistic;
        }
        Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges = fetchMappedRanges();
        Dataset<Integer> gapPositions = RangeDT.createPositions(SparkComputer.createDataFrame(inverseRanges,RangeDT.ENCODER));
        Dataset<Integer> bridgedGapPositions = createPositions(mappedRanges);
        GapStatisticDT gapStatistic = new GapStatisticDT();
        gapStatistic.setGapCount(gapPositions.count());
        gapStatistic.setUnbridgedCount(gapPositions.except(bridgedGapPositions).count());
        return gapStatistic;
    }

    public GapStatisticDT evaluateBridging(Dataset<ScoredPositionPositionMappingDT> scores) {
        Dataset<Integer> gapPositions = RangeDT.createPositions(SparkComputer.createDataFrame(inverseRanges,RangeDT.ENCODER));
        GapStatisticDT gapStatistic = new GapStatisticDT();
        gapStatistic.setGapCount(gapPositions.count());
        gapStatistic.setUnbridgedCount(gapPositions.except(scores.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT())).count());
        return gapStatistic;
    }

//    public void attemptPropertyDetection(boolean sequenceStrand) {
//
//        Dataset<Row> properties = null;
//        for(int i = 0; i < diagonalRanges.size(); i++) {
//            Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange = diagonalRanges.get(i);
////            RangeDT range = ranges.get(i);
//            if(properties != null) {
//                properties = properties.union(getProperties(diagonalRange,sequenceStrand,true));
//            }
//            else {
//                properties = getProperties(diagonalRange,sequenceStrand,true);
//            }
//        }
//        properties = properties.union(getProperties(scoreRanges,sequenceStrand,false)).cache();
//
////        Iterator<Row> propertyIt = properties.toLocalIterator();
////        while(propertyIt.hasNext()) {
////            Row r
////            System.out.println(p);
////        }
////        getPropertySummary(properties);
////        LOGGER.debug("computing cluster");
////        fillPropertyCluster(properties);
////        FileIO.serializeJson(resultDirectoryTree.plusPropertyCluster,propertyCluster);
//    }
//    public void attemptPropertyDetection() {
//        initializePropertyCluster();
//        List<RangeDT> ranges = sequenceInformation.isTopology() ?
//                SequenceMapAnalyzer.createCyclicRanges(sequenceKmers,0,getBoundaryRange()).
//                filter(col(ColumnIdentifier.LENGTH).geq(parameter.minRangeLength)).
//                        collectAsList() :
//                SequenceMapAnalyzer.createRanges(sequenceKmers,0).
//                        filter(col(ColumnIdentifier.LENGTH).geq(parameter.minRangeLength)).
//                        collectAsList();
//        LOGGER.debug("generated ranges");
//        for(RangeDT rangeDT : ranges) {
//            LOGGER.debug(rangeDT+"");
//        }
//        List<Dataset<RecordDiagonalBiPositionRangeDT>> diagonalRanges = new ArrayList<>();
//        Dataset<Row> properties = null;
//        properties = getProperties(computeDiagonalRanges(ranges.get(0)),ranges.get(0),true);
////        properties.show(500,(int)properties.count()-500);
//
//
//        for(RangeDT range: ranges) {
//            LOGGER.debug("Computing diagonal range " +range );
//            diagonalRanges.add(computeDiagonalRanges(range).cache());
//            LOGGER.debug("done");
//            if(properties != null) {
//                properties.union(getProperties(diagonalRanges.get(diagonalRanges.size()-1),range,true));
//            }
//            else {
//                properties = getProperties(diagonalRanges.get(diagonalRanges.size()-1),range,true);
//            }
//        }
//        getPropertySummary(properties);
//        LOGGER.debug("computing cluster");
//        fillPropertyCluster(properties);
//
//    }

//    public void persistDiagonalRanges() {
//        setRanges();
//
//        List<RangeDT> newRanges = new ArrayList<>();
//        int counter = 0, counter1 = 0;
//        Dataset<RecordDiagonalBiPositionRangeDT> diagonalRangeFull = null;
//        for(RangeDT range: ranges) {
//
//
//            Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange =
//                    computeDiagonalRanges(range);
//            diagonalRange = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                    decreaseRangeStart(diagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER).coalesce(2));
//
//            if(range.getLength() >= parameter.minRangeLength) {
//
//                diagonalRange.cache();
//                diagonalRanges.add(diagonalRange);
//                newRanges.add(new RangeDT(range));
//                if(diagonalRangeFull == null) {
//                    diagonalRangeFull = diagonalRange;
//                }
//                else {
//                    diagonalRangeFull = diagonalRangeFull.union(diagonalRange);
//                }
//                if(counter1 %10 == 0) {
//                    if(counter == 0 && counter1 == 0) {
//                        SparkComputer.persistDataFrame(diagonalRangeFull,
//                                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                    }
//                    else {
//                        SparkComputer.appendToPersistedDataFrame(diagonalRangeFull,
//                                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                    }
//                    diagonalRangeFull = null;
//                }
//
//                counter1++;
//
//            }
//            else {
//                if(counter == 0 || counter1 == 0) {
//                    SparkComputer.persistDataFrame(diagonalRange,
//                            resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                }
//                else {
//                    SparkComputer.appendToPersistedDataFrame(diagonalRange,
//                            resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                }
//                counter++;
//            }
//
//        }
//        ranges = newRanges;
//
//        SparkComputer.appendToPersistedDataFrame(diagonalRangeFull,
//                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//        LOGGER.debug("Done computing diagonal ranges");
//        Auxiliary.printSeq(ranges);
//    }

    public void persistMappedRanges() {

        setRanges(0);
        Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange = null;
        if(sequenceInformation.isTopology() && inverseRanges.size() == 0) {
            diagonalRange = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                    decreaseRangeStart(computeDiagonalRanges(ranges.get(0)),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER));
            RangeDT range2 = new RangeDT(sequenceInformation.getLength()-1,sequenceInformation.getLength()-2);
            range2.setLength(sequenceInformation.getLength());
            Dataset<RecordDiagonalBiPositionRangeDT> tempRanges =
                    RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                    RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                    decreaseRangeStart(computeDiagonalRanges(range2),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER)).
                    join(diagonalRange.withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
                                    withColumnRenamed(ColumnIdentifier.RECORD_LENGTH,"rl").
                                    withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,"d").
                                    withColumnRenamed(ColumnIdentifier.START,"s").
                                    withColumnRenamed(ColumnIdentifier.END,"e").
                                    withColumnRenamed(ColumnIdentifier.RECORD_START,"rs").
                                    withColumnRenamed(ColumnIdentifier.RECORD_END,"re").
                                    withColumnRenamed(ColumnIdentifier.LENGTH,"l").
                                    withColumnRenamed(ColumnIdentifier.STRAND,"st")
                            ,col(ColumnIdentifier.RECORD_ID).equalTo(col("r")).
                            and(col(ColumnIdentifier.RECORD_ID).equalTo(col("r"))).
                                    and(col(ColumnIdentifier.START).equalTo(col("s"))).
                                    and(col(ColumnIdentifier.END).equalTo(col("e"))).
                                    and(col(ColumnIdentifier.RECORD_START).equalTo(col("rs"))).
                                    and(col(ColumnIdentifier.RECORD_END).equalTo(col("re"))).
                                    and(col(ColumnIdentifier.STRAND).equalTo(col("st"))),
                            SparkComputer.JOIN_TYPES.LEFT_ANTI
                    ));
//            tempRanges.show();
//                    select(ColumnIdentifier.RECORD_ID,
//                            ColumnIdentifier.RECORD_LENGTH,
//                            ColumnIdentifier.DIAGONAL_IDENTIFIER,
//                            ColumnIdentifier.START,
//                            ColumnIdentifier.END,
//                            ColumnIdentifier.RECORD_START,
//                            Column
//                            )
//                    distinct()
            diagonalRange = diagonalRange.union(tempRanges);
//            diagonalRange = diagonalRange.union(RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                    decreaseRangeStart(computeDiagonalRanges(range2),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER))).
//                    distinct();
        }
        else {
            int start = ranges.stream().filter(r -> r.getStart() <= r.getEnd()).mapToInt(r -> r.getStart()).min().orElse(-1);
            int end = ranges.stream().filter(r -> r.getStart() <= r.getEnd()).mapToInt(r -> r.getEnd()).max().orElse(-1);

            if(start > -1 && end > -1) {
                RangeDT range1 = new RangeDT(start,end);
                LOGGER.debug("Linear part " + range1);
                diagonalRange = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                        decreaseRangeStart(computeDiagonalRanges(range1),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER));
            }


            if(RangeDT.cyclic(ranges)) {

                RangeDT range2 = ranges.stream().filter(r -> r.getStart() > r.getEnd()).findFirst().get();
                LOGGER.debug("Cyclic part " + range2);
                if(diagonalRange != null) {
                    diagonalRange = diagonalRange.
                            union(RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                                    decreaseRangeStart(computeDiagonalRanges(range2),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER)));
                }
                else {
                    diagonalRange = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                            decreaseRangeStart(computeDiagonalRanges(range2),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER));
                }

            }
        }

        SparkComputer.persistDataFrame(diagonalRange,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//        SparkComputer.appendToPersistedDataFrame(diagonalRange,
//                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//        if(append) {
//
//        }
//        else {
//            SparkComputer.persistDataFrame(diagonalRange,
//                    resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//        }


    }


    public void checkDiagonalRanges(int minCount) {
        setRanges(minCount);

        for(RangeDT range: ranges) {
            Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange =
                    computeDiagonalRanges(range).filter(col(ColumnIdentifier.START).lt(range.getStart()).or(col(ColumnIdentifier.END).gt(range.getEnd())));
            Dataset<RecordDiagonalBiPositionRangeDT> wrongRanges1 = diagonalRange.filter(col(ColumnIdentifier.START).lt(range.getStart()).or(col(ColumnIdentifier.END).gt(range.getEnd())));
            if(!wrongRanges1.isEmpty()) {
                LOGGER.debug("Computing diagonal range " +range );
                SparkComputer.showAll(wrongRanges1);
            }
            System.out.println("Checking shifted ranges");
            Dataset<RecordDiagonalBiPositionRangeDT> wrongRanges = decreaseRangeStart(diagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER).
                    filter(col(ColumnIdentifier.START).lt(range.getStart()-DbgGraph.K).or(col(ColumnIdentifier.END).gt(range.getEnd())));

            if(!wrongRanges.isEmpty()) {
                LOGGER.debug("Computing diagonal range " +range );
                SparkComputer.showAll(wrongRanges);
            }


        }

    }

    public void bridgeGap(
                          Map<Integer,Dataset<RecordDiagonalBiPositionRangeDT>> mappedRangeMap, int rangeIndex) {
        RangeDT range1 = ranges.get(rangeIndex);
        Dataset<ScoredPositionPositionKmerMappingDT> scores = null;
        int rangeIndex2 = Auxiliary.mod(rangeIndex+1,ranges.size());
        RangeDT range2 = ranges.get(rangeIndex2);
        Dataset<RecordDiagonalBiPositionRangeDT> leftMappedRange = mappedRangeMap.get(rangeIndex);
        mappedRangeMap.remove(rangeIndex);
        boolean enlargeRange;
        do {

            int gapSize = getGapSize(range1,range2);
            Dataset<RecordDiagonalBiPositionRangeDT> rightMappedRange = mappedRangeMap.get(rangeIndex2);
            if(rightMappedRange == null) {
                LOGGER.debug("computing mapped range");
                rightMappedRange = computeMappedRange(ranges.get(rangeIndex2)).cache();
                mappedRangeMap.put(rangeIndex2,rightMappedRange);
            }
            LOGGER.debug("Ranges " + range1 + " " + range2 + " RangeIndex " + rangeIndex + " " + rangeIndex2 + " gapsize: " + gapSize);

            // stop bridging if gap gets too long
            if(!parameter.bridgingIsValid(gapSize)) {
                LOGGER.debug("Safety guard active. Stopped briding attempt.");
                break;
            }
            if(!(parameter.gapTooLong(range1,range2,gapSize) ||
                    parameter.parameterRelativeTooHigh(range1,range2,gapSize))) {
                LOGGER.debug("Attempt bridging");
                // compute scores for current gap and parameter settings
                scores = bridgeGap(
                        computeBridgeCandidates(
                                leftMappedRange, rightMappedRange), gapSize);

            }
            if(scores == null || scores.isEmpty()) { // if no results are returned for current settings
                LOGGER.debug("No score results returned");
                int tempRangeIndex2 = Auxiliary.mod(rangeIndex2+1,ranges.size());
                RangeDT tempRange = ranges.get(tempRangeIndex2);
                int tempGapSize = getGapSize(range1,tempRange);
                if(range1.equals(tempRange) || parameter.gapTooLong(range1,tempRange,gapSize)) {// gap is too long or both ranges are the same
                    // but parameter tuning is not
                    if(!parameter.nextParameterRelativeTooHigh(range1,range2,gapSize )) {
                        enlargeRange = false;
                    }
                    else { // also no parameter tuning is possible
                        LOGGER.debug("Could not bridge. Both parameter settings cannot be tuned and gap is too long.");
                        break;
                    }
                } // gap is NOT too long but parameter tuning is NOT possible
                else if(parameter.nextParameterRelativeTooHigh(range1,range2,gapSize )) {
                    enlargeRange = true;
                } // both is an option
                else {
                    if(tempGapSize < gapSize*(parameter.gapSizeFactor+1)) {
                        LOGGER.debug("both an option chose gap enlarging");
                        enlargeRange = true;
                    }
                    else {
                        LOGGER.debug("both an option chose increasing parameter");
                        enlargeRange = false;
                    }
                }

                if(enlargeRange) {
                    LOGGER.debug("Enlarging range");
                    range2 = tempRange;
                    rangeIndex2 = tempRangeIndex2;
                    parameter.resetParameters();
                }
                else { // it is better to increase the gapsizefactor and retain the current ranges
                    LOGGER.debug("Increasing gapSizeFactor");
                    parameter.gapSizeFactor++;
                }
            }
        }while(scores == null || scores.isEmpty());

        parameter.resetParameters();
        if(scores != null && !scores.isEmpty()) {
            LOGGER.debug("Appending scores");
            handleScores(scores);
            matchPositions.addAll(scores.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT()).collectAsList());
        }
        leftMappedRange.unpersist();
    }

    public void bridgeGaps(int minCount,int minLength) {

        LOGGER.debug("Bridging gaps with minCount " + minCount + " and minLength " + minLength);
        setRanges(minCount,minLength);
        LOGGER.debug("Computed " + ranges.size() + " ranges");

        try {
            JDBJInterface.getInstance().dropTable(scoreTableName, true);
            JDBJInterface.getInstance().createTable(scoreTableName, ScoredPositionPositionMappingDT.tableValues(),true);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return;
        }

        if(inverseRanges.size() != 0) {
            Dataset<RecordDiagonalBiPositionRangeDT> leftMappedRange = computeMappedRange(ranges.get(0)).cache();
            if(ranges.size() == 1) {
                LOGGER.debug("Only one range detected " );
                RangeDT range = ranges.get(0);
                Dataset<ScoredPositionPositionKmerMappingDT> scores = null;
                if(sequenceInformation.isTopology()) { //only one cyclic
                    //                int startPos = Auxiliary.mod(range.getEnd()+1,sequenceInformation.getLength());
                    //                int endPos = Auxiliary.mod(range.getStart()-1,sequenceInformation.getLength());
                    LOGGER.debug("Bridging gap selfjoin");
                    do{
                        if(parameter.parameterRelativeTooHigh(ranges.get(0),inverseRanges.get(0).getLength())) {
                            LOGGER.debug("Parameters too high");
                            break;
                        }

                        scores = bridgeGapSelfJoin(leftMappedRange, ranges.get(0),inverseRanges.get(0));
                        if(scores.isEmpty()) {
                            LOGGER.debug("No results returned increasing gapsizefacor");
                            parameter.gapSizeFactor++;
                        }
                    } while(scores.isEmpty());
                    parameter.resetParameters();
                    //                scores.show();
                    // if gap too long try to expand to both sides as long as possible
                    if(scores == null || scores.isEmpty()) {
                        LOGGER.debug("Expanding too both sides");
                        int maxGapsize = parameter.getMaxGapSize(range);
                        scores =expandToLeft(leftMappedRange,
                                range,Auxiliary.mod(range.getStart()-maxGapsize,sequenceInformation.getLength()));
                        if(scores != null) {
                            scores = scores.union(
                                    expandToRight(leftMappedRange,
                                            range,Auxiliary.mod(range.getEnd()+maxGapsize,sequenceInformation.getLength()))
                            );
                        }
                        else {
                            scores = expandToRight(leftMappedRange,
                                    range,Auxiliary.mod(range.getEnd()+maxGapsize,sequenceInformation.getLength()));
                        }
                    }

                }
                else {
                    LOGGER.debug("Bridging gap to both sides");
                    scores = expandToBothSides(leftMappedRange, range);
                    // if gap too long try to expand to both sides as long as possible
                    if(scores == null || scores.isEmpty()) {
                        int maxGapsize = parameter.getMaxGapSize(range);
                        scores = expandToLeft(leftMappedRange,
                                range,Math.max(range.getStart()-maxGapsize,0));
                        if(scores != null) {
                            scores = scores.union(
                                    expandToRight(leftMappedRange,
                                            range,Math.max(range.getEnd()+maxGapsize,sequenceInformation.getLength()-1))
                            );
                        }
                        else {
                            scores = expandToRight(leftMappedRange,
                                    range,Math.max(range.getEnd()+maxGapsize,sequenceInformation.getLength()-1));
                        }
                    }
                }
                if(scores != null && !scores.isEmpty()) {
                    LOGGER.debug("Appending scores");
                    handleScores(scores);
                    matchPositions.addAll(scores.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT()).collectAsList());
                }
            }
            else {


                Map<Integer,Dataset<RecordDiagonalBiPositionRangeDT>> mappedRangeMap = new TreeMap<>();

                mappedRangeMap.put(0,leftMappedRange);
                if(!sequenceInformation.isTopology()) {
                    if(ranges.get(0).getStart() != 0) {
                        if(!parameter.gapTooLong(ranges.get(0),getLeftBoundaryGapSize(ranges.get(0)))) {
                            LOGGER.debug("Expanding to left");
                            Dataset<ScoredPositionPositionKmerMappingDT> scores = expandToLeft(leftMappedRange,ranges.get(0));
                            if(scores == null || scores.isEmpty()) {
                                int maxGapsize = parameter.getMaxGapSize(ranges.get(0));
                                scores = expandToLeft(leftMappedRange,
                                        ranges.get(0),Math.max(ranges.get(0).getStart()-maxGapsize,0));
                            }
                            if(scores != null && !scores.isEmpty()) {
                                LOGGER.debug("Appending scores");
                                handleScores(scores);


                                matchPositions.addAll(scores.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT()).collectAsList());
                            }
                        }

                    }
                }
                for(int rangeIndex = 0; rangeIndex < ranges.size(); rangeIndex++) {
                    if(!sequenceInformation.isTopology() && rangeIndex == ranges.size()-1) {
                        LOGGER.debug("stop expanding beyond linear boundaries");
                        break;
                    }
                    LOGGER.debug("Left Rangeindex " + rangeIndex);
                    bridgeGap(mappedRangeMap,rangeIndex);
                }

                if(!sequenceInformation.isTopology()) {
                    if(ranges.get(ranges.size()-1).getEnd() != sequenceInformation.getLength()-1 &&
                            !parameter.gapTooLong(ranges.get(ranges.size()-1),getRightBoundaryGapSize(ranges.get(ranges.size()-1)))) {
                        LOGGER.debug("Expanding to right");
                        leftMappedRange  = mappedRangeMap.get(ranges.size()-1);
                        Dataset<ScoredPositionPositionKmerMappingDT> scores =
                                expandToRight(leftMappedRange,ranges.get(ranges.size()-1));
                        if(scores == null || scores.isEmpty()) {
                            int maxGapsize = parameter.getMaxGapSize(ranges.get(0));
                            scores = expandToRight(leftMappedRange,ranges.get(ranges.size()-1),
                                    Math.max(ranges.get(0).getStart()-maxGapsize,0));
                        }
                        if(scores != null && !scores.isEmpty()) {
                            LOGGER.debug("Appending scores");
                            handleScores(scores);


                            matchPositions.addAll(scores.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT()).collectAsList());
                        }
                    }
                }

                for(Dataset<RecordDiagonalBiPositionRangeDT> dataset: mappedRangeMap.values()) {
                    dataset.unpersist();
                }
            }

        }
        computeMappedRangesFromScores();
        persistMappedRanges();

    }



    private void handleScores(Dataset<ScoredPositionPositionKmerMappingDT> scores) {
        JDBJInterface.getInstance().insertDataIntoTable(scoreTableName,scores.drop(col(ColumnIdentifier.KMER)));

    }

    private void computeMappedRangesFromScores() {
        try {
            // remove record duplicates
            JDBJInterface.getInstance().executeUpdate("DELETE FROM\n" +
                    scoreTableName + " t1\n" +
                    "USING "+scoreTableName +" t2\n" +
                    "WHERE t1.ctid < t2.ctid\n" +
                    "AND t1.\"recordId\" = t2.\"recordId\"\n" +
                    "AND t1.position = t2.position\n" +
                    "AND t1.\"recordPosition\" = t2.\"recordPosition\"\n" +
                    "AND t1.strand = t2.strand");

        } catch (SQLException s) {
            s.printStackTrace();
            System.exit(-1);
        }
        Dataset<ScoredPositionPositionMappingDT> scores =
                JDBJInterface.getInstance().fetchTable(scoreTableName,ScoredPositionPositionMappingDT.ENCODER);


        Dataset<RecordDiagonalBiPositionRangeDT> newScoreRanges =
        RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                createRangesForRecords(scores.join(recordDTDataset,
                        scores.col(ColumnIdentifier.RECORD_ID).
                                equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
                                select(
//                                            lit(0).cast(DataTypes.IntegerType).as(ColumnIdentifier.DIAGONAL_IDENTIFIER),
                                        scores.col(ColumnIdentifier.POSITION),
                                        scores.col(ColumnIdentifier.RECORD_POSITION),
                                        scores.col(ColumnIdentifier.STRAND),
                                        recordDTDataset.col(ColumnIdentifier.RECORD_ID),
                                        when(col(ColumnIdentifier.TOPOLOGY).equalTo(true),
                                                recordDTDataset.col(ColumnIdentifier.LENGTH)).
                                                otherwise(lit(-1)).as(ColumnIdentifier.RECORD_LENGTH)),
                        col(ColumnIdentifier.RECORD_LENGTH),
                        lit(0).cast(DataTypes.IntegerType).as(ColumnIdentifier.DIAGONAL_IDENTIFIER)));

        SparkComputer.persistDataFrame(newScoreRanges,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.BRIDGED_RANGES));
    }
//    public void bridgeGaps() {
//
//
//        setRanges();
//
//        for(RangeDT range: ranges) {
////            LOGGER.debug("Computing diagonal range " +range );
//            diagonalRanges.add(computeDiagonalRanges(range).cache());
//
////            diagonalRanges.get(diagonalRanges.size()-1).
////                    orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.STRAND,ColumnIdentifier.START).show();
//        }
//        LOGGER.debug("Done computing diagonal ranges");
//        if(inverseRanges.size() == 0) {
//            return;
//        }
//        boolean terminateFlag = false;
//        boolean enlargeRange = true;
//
//        if(ranges.size() == 1) { // treat weird boundary ranges here -> only one cyclic or linear
//            LOGGER.debug("Only one range detected " );
//            RangeDT range = ranges.get(0);
//            Dataset<ScoredPositionPositionMappingDT> scores = null;
//            if(sequenceInformation.isTopology()) { //only one cyclic
////                int startPos = Auxiliary.mod(range.getEnd()+1,sequenceInformation.getLength());
////                int endPos = Auxiliary.mod(range.getStart()-1,sequenceInformation.getLength());
//                LOGGER.debug("Bridging gap selfjoin");
//                do{
//                    if(parameter.parameterTooHigh(ranges.get(0),inverseRanges.get(0).getLength())) {
//                        LOGGER.debug("Parameters too high");
//                        break;
//                    }
//
//                    scores = bridgeGapSelfJoin(diagonalRanges.get(0), ranges.get(0),inverseRanges.get(0));
//                    if(scores.isEmpty()) {
//                        LOGGER.debug("No results returned increasing gapsizefacor");
//                        parameter.gapSizeFactor++;
//                    }
//                } while(scores.isEmpty());
//                parameter.resetParameters();
////                scores.show();
//                // if gap too long try to expand to both sides as long as possible
//                if(scores == null || scores.isEmpty()) {
//                    LOGGER.debug("Expanding too both sides");
//                    int maxGapsize = parameter.getMaxGapSize(range);
//                    scores =expandToLeft(diagonalRanges.get(0),
//                            range,Auxiliary.mod(range.getStart()-maxGapsize,sequenceInformation.getLength()));
//                    if(scores != null) {
//                        scores = scores.union(
//                                expandToRight(diagonalRanges.get(0),
//                                        range,Auxiliary.mod(range.getEnd()+maxGapsize,sequenceInformation.getLength()))
//                        );
//                    }
//                    else {
//                        scores = expandToRight(diagonalRanges.get(0),
//                                range,Auxiliary.mod(range.getEnd()+maxGapsize,sequenceInformation.getLength()));
//                    }
//                }
//
//            }
//            else {
//                LOGGER.debug("Bridging gap to both sides");
//                scores = expandToBothSides(diagonalRanges.get(0), range);
//                // if gap too long try to expand to both sides as long as possible
//                if(scores == null || scores.isEmpty()) {
//                    int maxGapsize = parameter.getMaxGapSize(range);
//                    scores = expandToLeft(diagonalRanges.get(0),
//                            range,Math.max(range.getStart()-maxGapsize,0));
//                    if(scores != null) {
//                        scores = scores.union(
//                                expandToRight(diagonalRanges.get(0),
//                                        range,Math.max(range.getEnd()+maxGapsize,sequenceInformation.getLength()-1))
//                        );
//                    }
//                    else {
//                        scores = expandToRight(diagonalRanges.get(0),
//                                range,Math.max(range.getEnd()+maxGapsize,sequenceInformation.getLength()-1));
//                    }
//                }
//            }
//            if(scores != null && !scores.isEmpty()) {
//                scoreList.add(scores);
//            }
//        }
//        else {
//            List<RangeDT> unbridgedRanges = new ArrayList<>();
//            List<List<Integer>> rangeTuples = createRangeIndexTuples(ranges);
//            LOGGER.debug("Computed " + rangeTuples.size() + " rangeTuples");
//            int rangeTupleCounter = 0;
//            if(!sequenceInformation.isTopology()) {
//                if(ranges.get(0).getStart() != 0) {
//                    if(!parameter.gapTooLong(ranges.get(0),getLeftBoundaryGapSize(ranges.get(0)))) {
//                        LOGGER.debug("Expanding to left");
//                        Dataset<ScoredPositionPositionMappingDT> scores = expandToLeft(diagonalRanges.get(0),ranges.get(0));
//                        if(scores == null || scores.isEmpty()) {
//                            int maxGapsize = parameter.getMaxGapSize(ranges.get(0));
//                            scores = expandToLeft(diagonalRanges.get(0),
//                                    ranges.get(0),Math.max(ranges.get(0).getStart()-maxGapsize,0));
//                        }
//                        if(!scores.isEmpty()) {
//                            scoreList.add(scores);
//                        }
//                    }
//
//                }
//            }
//
//            List<Integer> rangeTuple;
//
//            rangeTuple = rangeTuples.get(rangeTupleCounter);
//            do {
//                Dataset<ScoredPositionPositionMappingDT> scores = null;
//                do {
//
//                    RangeDT range1 = ranges.get(rangeTuple.get(0));
//                    RangeDT range2 = ranges.get(rangeTuple.get(1));
//                    int gapSize = getGapSize(range1,range2);
//
//                    LOGGER.debug("RangeTupleCounter " + rangeTupleCounter);
//                    LOGGER.debug("Ranges " + range1 + " " + range2);
//                    LOGGER.debug("Range tuple indecees: " + rangeTuple.get(0) + " " + rangeTuple.get(1) + " gapsize: " + gapSize);
//
//
//                    // stop bridging if gap gets too long
//                    if(!(parameter.gapTooLong(range1,range2,gapSize) || parameter.parameterTooHigh(range1,range2,gapSize))) {
//                        LOGGER.debug("Attempt bridging");
//                        // compute scores for current gap and parameter settings
//                        scores = bridgeGap(
//                                computeBridgeCandidates(
//                                        diagonalRanges.get(rangeTuple.get(0)),
//                                        diagonalRanges.get(rangeTuple.get(1))),
//                                gapSize);
//                    }
//                    if(scores == null || scores.isEmpty()) { // if no results are returned for current settings
//
//                        if(! sequenceInformation.isTopology() && rangeTupleCounter == rangeTuple.size()-1 ) {
//
//                            if(!parameter.nextParameterTooHigh(ranges.get(rangeTuple.get(0)),ranges.get(rangeTuple.get(1)),gapSize )) {
//                                LOGGER.debug("Linear boundary case gapSizeFactor");
//                                parameter.gapSizeFactor++;
//                            }
//                            else {
//                                LOGGER.debug("Linear boundary case break");
//                                break;
//                            }
//                        }
//                        else {
//                            LOGGER.debug("No score results returned");
//
//                            List<Integer> tempRangeTuple = new ArrayList<>(
//                                    Arrays.asList(rangeTuple.get(0),rangeTuples.get(Auxiliary.mod(rangeTupleCounter+1,rangeTuples.size())).get(1)));
//                            range1 = ranges.get(tempRangeTuple.get(0));
//                            range2 = ranges.get(tempRangeTuple.get(1));
//                            int tempGapSize = getGapSize(range1,range2);
//
//
//                            // gap is too long
//                            if(parameter.gapTooLong(range1,range2,gapSize)) {
//                                // but parameter tuning is not
//                                if(!parameter.nextParameterTooHigh(ranges.get(rangeTuple.get(0)),ranges.get(rangeTuple.get(1)),gapSize )) {
//                                    enlargeRange = false;
//                                }
//                                else { // also no parameter tuning is possible
//                                    unbridgedRanges.add(ranges.get(rangeTuple.get(0)));
//                                    LOGGER.debug("Could not bridge. Both parameter settings cannot be tuned and gap is too long.");
//                                    break;
//                                }
//                            } // gap is NOT too long but parameter tuning is NOT possible
//                            else if(parameter.nextParameterTooHigh(ranges.get(rangeTuple.get(0)),ranges.get(rangeTuple.get(1)),gapSize )) {
//                                enlargeRange = true;
//                            } // both is an option
//                            else {
//                                if(tempGapSize < gapSize*(parameter.gapSizeFactor+1)) {
//                                    LOGGER.debug("both an option chose gap enlarging");
//                                    enlargeRange = true;
//                                }
//                                else {
//                                    LOGGER.debug("both an option chose increasing parameter");
//                                    enlargeRange = false;
//                                }
//                            }
//
//                            if(enlargeRange) {
//                                LOGGER.debug("Enlarging range");
//                                LOGGER.debug("New Ranges " +range1 + " " + range2);
//                                LOGGER.debug("Gapsize range skipping: " + tempGapSize);
////                                LOGGER.debug("RangeTupleCounter " + rangeTupleCounter + "(" + rangeTuples.size() + ")");
//
//                                rangeTuple = tempRangeTuple;
//                                if(rangeTupleCounter == rangeTuples.size()-1) {
//                                    LOGGER.debug("Setting terminate flag");
//                                    terminateFlag = true;
//                                    // make sure no infinite loop starts here!!!
//                                }
//                                rangeTupleCounter = Auxiliary.mod(rangeTupleCounter+1,rangeTuples.size());
//                                parameter.resetParameters();
//                            }
//                            else { // it is better to increase the gapsizefactor and retain the current ranges
//                                LOGGER.debug("Increasing gapSizeFactor");
//                                parameter.gapSizeFactor++;
//                            }
//
//                        }
//
//
//                    }
//                    else { // the current ranges could be bridged -> see if unbridged ones are now covered
//                        // remove unbridged ranges if covered by currently bridged range
//                        Iterator<RangeDT> rangeDTIterator = unbridgedRanges.iterator();
//                        RangeDT coverRange = new RangeDT(range1.getStart(),range2.getEnd());
//                        while (rangeDTIterator.hasNext()) {
//                            RangeDT unbridgedRange = rangeDTIterator.next();
//                            if(containedRanges(unbridgedRange,coverRange)) {
//                                LOGGER.debug("Removed unbridged range " + unbridgedRange);
//                                rangeDTIterator.remove();
//                            }
//                        }
//                    }
//                } while(scores == null || scores.isEmpty());
//                LOGGER.debug("Completed");
//                parameter.resetParameters();
//                if(scores != null && !scores.isEmpty()) {
//
//
////                    analyzeScoreResults(scores);
////                    scores.show();
////                    scores.filter(col(ColumnIdentifier.STRAND).equalTo(true)).show();
//                    scoreList.add(scores);
//                }
//                if(terminateFlag) {
//                    break;
//                }
//                rangeTupleCounter++;
//                if(rangeTupleCounter < rangeTuples.size()) {
//                    LOGGER.debug("in here");
//                    rangeTuple = rangeTuples.get(rangeTupleCounter);
//                }
//
//            } while (rangeTupleCounter < rangeTuples.size() || terminateFlag);
//            for(RangeDT range: unbridgedRanges) {
//                int index = ranges.indexOf(range);
//                int maxGapsize = parameter.getMaxGapSize(range);
//                LOGGER.debug("Trying to enlargen unbridged ranges index " + index + " maxGapsize " + maxGapsize );
////                diagonalRanges.get(index).show();
//                Dataset<ScoredPositionPositionMappingDT> scores = expandToRight(diagonalRanges.get(index),
//                        range,Auxiliary.mod(range.getEnd()+maxGapsize,sequenceInformation.getLength()));
//                if(scores != null && !scores.isEmpty()) {
//                    LOGGER.debug("Successfully enlarged unbridged range");
//                    scoreList.add(scores);
//                }
//            }
//            if(!sequenceInformation.isTopology()) {
//                if(ranges.get(ranges.size()-1).getLength() != sequenceInformation.getLength() &&
//                        !parameter.gapTooLong(ranges.get(ranges.size()-1),getRightBoundaryGapSize(ranges.get(ranges.size()-1)))) {
//                    LOGGER.debug("Expanding to right");
//                    Dataset<ScoredPositionPositionMappingDT> scores = expandToRight(diagonalRanges.get(ranges.size()-1),ranges.get(ranges.size()-1));
//                    if(scores == null || scores.isEmpty()) {
//                        int maxGapsize = parameter.getMaxGapSize(ranges.get(0));
//                        scores = expandToRight(diagonalRanges.get(ranges.size()-1),ranges.get(ranges.size()-1),
//                                Math.max(ranges.get(0).getStart()-maxGapsize,0));
//                    }
//                    if(!scores.isEmpty()) {
//                        scoreList.add(scores);
//                    }
//                }
//            }
//        }
//        for(int i = 0; i < diagonalRanges.size(); i++) {
//            // shift range
//            Dataset<RecordDiagonalBiPositionRangeDT> oldDiagonalRange =
//                    diagonalRanges.get(i);
//            oldDiagonalRange.unpersist();
//            diagonalRanges.set(i,RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                    decreaseRangeStart(oldDiagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER)).cache());
//
//        }
//        LOGGER.debug("Done shifting ranges");
//
//
//        for(Dataset<ScoredPositionPositionMappingDT> score : scoreList) {
////            LOGGER.debug("Record Ranges");
//
//            Dataset<RecordDiagonalBiPositionRangeDT> newScoreRanges =
//                    RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
//                            createRangesForRecords(score.join(recordDTDataset,
//                                    score.col(ColumnIdentifier.RECORD_ID).
//                                            equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
//                                    select(
////                                            lit(0).cast(DataTypes.IntegerType).as(ColumnIdentifier.DIAGONAL_IDENTIFIER),
//                                            score.col(ColumnIdentifier.POSITION),
//                                            score.col(ColumnIdentifier.RECORD_POSITION),
//                                            score.col(ColumnIdentifier.STRAND),
//                                            recordDTDataset.col(ColumnIdentifier.RECORD_ID),
//                                            when(col(ColumnIdentifier.TOPOLOGY).equalTo(true),
//                                                    recordDTDataset.col(ColumnIdentifier.LENGTH)).
//                                                    otherwise(lit(-1)).as(ColumnIdentifier.RECORD_LENGTH)),
//                                    col(ColumnIdentifier.RECORD_LENGTH),
//                                    lit(0).cast(DataTypes.IntegerType).as(ColumnIdentifier.DIAGONAL_IDENTIFIER)));
//            if(scoreRanges == null) {
//                scoreRanges = newScoreRanges;
//            }
//            else {
//                scoreRanges = scoreRanges.union(newScoreRanges);
//            }
//            matchPositions.addAll(score.select(ColumnIdentifier.POSITION).distinct().as(Encoders.INT()).collectAsList());
//        }
//        if(scoreRanges != null && !scoreRanges.isEmpty()){
//            scoreRanges.cache();
//        }
//
////        scoreRanges.show();
//
//        GapStatisticDT gapStatistic = new GapStatisticDT();
//        gapStatistic.setGapCount(gapPositions.size());
////        LOGGER.debug("Gap Positions");
////        System.out.println(gapPositions);
////        LOGGER.debug("Gap Positions unmapped");
//        gapPositions.removeAll(matchPositions);
//        gapStatistic.setUnbridgedCount(gapPositions.size());
//        LOGGER.debug("Gaps: " + gapStatistic.getGapCount() + " unbridged: " + gapStatistic.getUnbridgedCount() + "(" + gapStatistic.bridgedRatio() + ")");
//        FileIO.serializeJson(resultDirectoryTree.gapStatistic,gapStatistic);
////        System.out.println(gapPositions);
////
////        scoreRanges.orderBy(ColumnIdentifier.RECORD_ID,ColumnIdentifier.START).show(500);
//
//    }

//    public void persistMappedRanges() {
//        Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges = null;
//
//        for(int i = 0; i < diagonalRanges.size(); i++) {
//            if(mappedRanges == null) {
//                mappedRanges = diagonalRanges.get(i);
//            }
//            else {
//                mappedRanges = mappedRanges.union(diagonalRanges.get(i));
//            }
//            diagonalRanges.get(i).unpersist();
//        }
//
//        mappedRanges.union(scoreRanges);
//        scoreRanges.unpersist();
//        SparkComputer.persistDataFrame(mappedRanges,
//                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//        LOGGER.debug("Persisted mapped Ranges");
//    }

//    public void persistMappedRanges() {
//
//
//        for(int i = 0; i < diagonalRanges.size(); i++) {
//            if(i == 0) {
//                SparkComputer.persistDataFrame(diagonalRanges.get(i),
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//            }
//            else {
//                SparkComputer.appendToPersistedDataFrame(diagonalRanges.get(i),
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//            }
//            diagonalRanges.get(i).unpersist();
//        }
//        if(scoreRanges != null && !scoreRanges.isEmpty()) {
//            SparkComputer.appendToPersistedDataFrame(scoreRanges,
//                    resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//            scoreRanges.unpersist();
//
//            LOGGER.debug("Persisted mapped Ranges");
//        }
//
//    }

//    public void run() {
//
//        bridgeGaps();
//        LOGGER.debug("Bridged Gaps");
////        createCluster();
////        LOGGER.debug("Created cluster");
////        persistMappedRanges();
////        LOGGER.debug("Persisted Mapped Ranges");
//    }

    /**** Range manipulation *************************************************************************************************************************************************************************************************/

    public void setRanges(int minCount, int minLength) {
        Dataset<RangeDT> rangesDataset = sequenceInformation.isTopology() ?
                SequenceMapAnalyzer.createCyclicRanges(sequenceKmers,minCount,getBoundaryRange()).
                        filter(col(ColumnIdentifier.LENGTH).geq(minLength)) :
                SequenceMapAnalyzer.createRanges(sequenceKmers,minCount).
                        filter(col(ColumnIdentifier.LENGTH).geq(minLength));

//        LOGGER.info("generated ranges");
//        SparkComputer.showAll(rangesDataset);


        Dataset<RangeDT> inverseRangesDataset =
                sequenceInformation.isTopology() ?
                        RangeDT.createCyclicInverseRange(rangesDataset,getBoundaryRange()) :
                        RangeDT.createInverseRange(rangesDataset,getBoundaryRange());
//        LOGGER.info("generated gaps");
//        SparkComputer.showAll(inverseRangesDataset);

        ranges = new ArrayList<>(rangesDataset.collectAsList());
        inverseRanges = new ArrayList<>(inverseRangesDataset.collectAsList());
//        LOGGER.info("generated ranges");
//        for(RangeDT rangeDT : ranges) {
//            LOGGER.info(rangeDT+"");
//        }
    }

    public void setRanges(int minCount) {
//        Dataset<RangeDT> rangesDataset = sequenceInformation.isTopology() ?
//                SequenceMapAnalyzer.createCyclicRanges(sequenceKmers,0,getBoundaryRange()).
//                        filter(col(ColumnIdentifier.LENGTH).geq(parameter.minRangeLength)) :
//                SequenceMapAnalyzer.createRanges(sequenceKmers,0).
//                        filter(col(ColumnIdentifier.LENGTH).geq(parameter.minRangeLength));

        Dataset<RangeDT> rangesDataset = sequenceInformation.isTopology() ?
                SequenceMapAnalyzer.createCyclicRanges(sequenceKmers,minCount,getBoundaryRange()) :
                SequenceMapAnalyzer.createRanges(sequenceKmers,minCount);
//        LOGGER.info("generated ranges");
//        SparkComputer.showAll(rangesDataset);


        Dataset<RangeDT> inverseRangesDataset =
                sequenceInformation.isTopology() ?
                        RangeDT.createCyclicInverseRange(rangesDataset,getBoundaryRange()) :
                        RangeDT.createInverseRange(rangesDataset,getBoundaryRange());
//        LOGGER.info("generated gaps");
//        SparkComputer.showAll(inverseRangesDataset);

        ranges = new ArrayList<>(rangesDataset.collectAsList());
        inverseRanges = new ArrayList<>(inverseRangesDataset.collectAsList());
//        LOGGER.info("generated ranges");
//        for(RangeDT rangeDT : ranges) {
//            LOGGER.info(rangeDT+"");
//        }
    }
    /**
     * Expand each range by expandsize to left
     * @param rangeData
     * @param expandSize
     * @param <T>
     * @return
     */
    public  <T> Dataset<T> decreaseRangeStart(Dataset<T> rangeData, int expandSize, Encoder<T> tEncoder) {
        return decreaseRangeStart(rangeData,expandSize).
                as(tEncoder);

    }

    public Dataset<Row> decreaseRangeStart(Dataset<?> rangeData, int expandSize) {
//        rangeData.show();
        if(expandSize < 0 ) {
            LOGGER.error("Expandsize must be positive");
            return null;
        }
        final String startTemp = "startTemp",lengthTemp = "lengthTemp", recordStartTemp = "recordstartTemp";
        Dataset<Row> results = rangeData.
                withColumnRenamed(ColumnIdentifier.START,startTemp).
                withColumnRenamed(ColumnIdentifier.RECORD_START,recordStartTemp).
                withColumnRenamed(ColumnIdentifier.LENGTH,lengthTemp).
                withColumn(ColumnIdentifier.RECORD_START,
                        when(
                                col(ColumnIdentifier.RECORD_LENGTH).gt(-1),
                                pmod(
                                        col(recordStartTemp).minus(expandSize),col(ColumnIdentifier.RECORD_LENGTH))).
                                otherwise(greatest(lit(0),col(recordStartTemp).minus(expandSize))));
        if(sequenceInformation.isTopology()) {
            results = results.withColumn(ColumnIdentifier.START,
                            pmod(
                                    col(startTemp).minus(expandSize),
                                    lit(sequenceInformation.getLength()).cast(DataTypes.IntegerType)));
        }
        else {
            results = results.withColumn(ColumnIdentifier.START, greatest(lit(0),col(startTemp).minus(expandSize)));
        }

        return
                results.
                withColumn(ColumnIdentifier.LENGTH,col(lengthTemp).plus(expandSize)).
                        drop(startTemp,recordStartTemp,lengthTemp);

    }

    /**
     * Computes ranges for each recordId in the provided dataset.
     * If further columns of dataset are to be kept in the result,
     * these must be specified via columnGroups
     * @param dataset
     * @param columnGroups
     * @return
     */
    public static Dataset<Row> createRangesForRecords(Dataset<?> dataset, Column... columnGroups) {

        final String diffOrderedIdentifier = "diffOrdered", DIFF_COLUMN_IDENTIFIER = "diff",
                positionOrderColumnName = ColumnIdentifier.POSITION, positionColumnName = ColumnIdentifier.RECORD_POSITION;
        WindowSpec w = Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy(positionOrderColumnName);
//        LOGGER.debug("create record range");
//        dataset.show();

//        dataset.filter(col(ColumnIdentifier.STRAND).equalTo(true)).show();
        Column [] newColumnGroups = new Column[columnGroups.length+3];
        newColumnGroups[0] = col(ColumnIdentifier.RECORD_ID);
        newColumnGroups[1] = col(ColumnIdentifier.STRAND);
        for(int i = 0; i < columnGroups.length; i++) {
            newColumnGroups[i+2] = columnGroups[i];
        }
        newColumnGroups[columnGroups.length+2] = col(DIFF_COLUMN_IDENTIFIER);

        return dataset.
                withColumn(diffOrderedIdentifier,col(positionOrderColumnName).minus(row_number().over(w))).
                withColumn(DIFF_COLUMN_IDENTIFIER,col(positionColumnName).minus(row_number().over(w))).
                groupBy(newColumnGroups).
                agg(
                        min(positionOrderColumnName).as(RangeDT.COLUMN_NAME.START.identifier),
                        max(positionOrderColumnName).as(RangeDT.COLUMN_NAME.END.identifier),
                        max(positionOrderColumnName).minus(min(positionOrderColumnName)).plus(1).as(RangeDT.COLUMN_NAME.LENGTH.identifier),
                        min(positionColumnName).as(ColumnIdentifier.RECORD_START),
                        max(positionColumnName).as(ColumnIdentifier.RECORD_END)).
//                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
        drop(DIFF_COLUMN_IDENTIFIER);
//                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier);
    }


    public static <T> Dataset<Row> createRangesLengthsForRecords(Dataset<T> dataset) {


        final String diffOrderedIdentifier = "diffOrdered", DIFF_COLUMN_IDENTIFIER = "diff",
                positionOrderColumnName = ColumnIdentifier.POSITION, positionColumnName = ColumnIdentifier.RECORD_POSITION;
        WindowSpec w = Window.partitionBy(ColumnIdentifier.RECORD_ID).orderBy(positionOrderColumnName);

        return dataset.
                withColumn(diffOrderedIdentifier,col(positionOrderColumnName).minus(row_number().over(w))).
                withColumn(DIFF_COLUMN_IDENTIFIER,col(positionColumnName).minus(row_number().over(w))).
                groupBy(ColumnIdentifier.RECORD_ID,
                        DIFF_COLUMN_IDENTIFIER).
                agg(
                        max(positionOrderColumnName).minus(min(positionOrderColumnName)).plus(1).as(RangeDT.COLUMN_NAME.LENGTH.identifier)).
//                        max(ColumnIdentifier.POSITION).minus(min(ColumnIdentifier.POSITION)).plus(1).as(COLUMN_NAME.LENGTH.identifier)).
        drop(DIFF_COLUMN_IDENTIFIER);
//                select(COLUMN_NAME.START.identifier,COLUMN_NAME.END.identifier,COLUMN_NAME.LENGTH.identifier);
    }

    public static Dataset<Row> createStrandPositionMapping(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, Dataset<KmerDT> recordKmers, Dataset<RecordDT> recordDTDataset) {
        return recordKmers.
                join(recordDTDataset,
                        recordKmers.col(ColumnIdentifier.RECORD_ID).
                                equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
                select(recordKmers.col(ColumnIdentifier.POSITIONS),
                        recordKmers.col(ColumnIdentifier.KMER),
                        recordDTDataset.col(ColumnIdentifier.RECORD_ID),
                        when(col(ColumnIdentifier.TOPOLOGY).equalTo(true),
                                recordDTDataset.col(ColumnIdentifier.LENGTH)).otherwise(lit(-1)).as(ColumnIdentifier.RECORD_LENGTH),
                        recordDTDataset.col(ColumnIdentifier.TOPOLOGY)).
                join(sequenceKmers,
                        sequenceKmers.col(ColumnIdentifier.IDENTIFIER).
                                equalTo(col(ColumnIdentifier.KMER)),SparkComputer.JOIN_TYPES.INNER).
                drop(ColumnIdentifier.IDENTIFIER,ColumnIdentifier.COUNT_MINUS,
                        ColumnIdentifier.COUNT_PLUS).
                withColumn(ColumnIdentifier.RECORD_POSITION,explode(col(ColumnIdentifier.POSITIONS))).
                drop(ColumnIdentifier.POSITIONS,ColumnIdentifier.KMER);
    }

    public static Dataset<Row> createPositionMapping(Dataset<PositionCountIdentifierDT> sequenceKmers, Dataset<KmerDT> recordKmers, Dataset<RecordDT> recordDTDataset) {
        return recordKmers.
                join(recordDTDataset,
                        recordKmers.col(ColumnIdentifier.RECORD_ID).
                                equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
                select(recordKmers.col(ColumnIdentifier.POSITIONS),
                        recordKmers.col(ColumnIdentifier.KMER),
                        recordDTDataset.col(ColumnIdentifier.RECORD_ID),
                        when(col(ColumnIdentifier.TOPOLOGY).equalTo(true),
                                recordDTDataset.col(ColumnIdentifier.LENGTH)).otherwise(lit(-1)).as(ColumnIdentifier.RECORD_LENGTH),
                        recordDTDataset.col(ColumnIdentifier.TOPOLOGY)).
                join(sequenceKmers,
                        sequenceKmers.col(ColumnIdentifier.IDENTIFIER).
                                equalTo(col(ColumnIdentifier.KMER)),SparkComputer.JOIN_TYPES.INNER).
                drop(ColumnIdentifier.IDENTIFIER,ColumnIdentifier.COUNT).
                withColumn(ColumnIdentifier.RECORD_POSITION,explode(col(ColumnIdentifier.POSITIONS))).
                drop(ColumnIdentifier.POSITIONS,ColumnIdentifier.KMER);
    }

    public static Dataset<Row> createPositionMapping(Dataset<PositionCountIdentifierDT> sequenceKmers, Dataset<KmerDT> recordKmers) {
        return recordKmers.
                join(sequenceKmers,
                        sequenceKmers.col(ColumnIdentifier.IDENTIFIER).
                                equalTo(col(ColumnIdentifier.KMER)),SparkComputer.JOIN_TYPES.INNER).
                drop(ColumnIdentifier.IDENTIFIER,ColumnIdentifier.COUNT).
                withColumn(ColumnIdentifier.RECORD_POSITION,explode(col(ColumnIdentifier.POSITIONS))).
                drop(ColumnIdentifier.POSITIONS,ColumnIdentifier.KMER);
    }

    public  Dataset<Row> createRecordStrandRanges(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, Dataset<KmerDT> recordKmers) {
        Dataset<RecordDT> recordDTDataset = null;
        try {
            recordDTDataset = RecordTable.getInstance().getTableEntries();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        // join
        Dataset<Row> positionMapping = createStrandPositionMapping(sequenceKmers,recordKmers,recordDTDataset);
        positionMapping.show();
        Dataset<Row> recordRanges = createRangesForRecords(positionMapping);
        recordRanges.show();
        return decreaseRangeStart(recordRanges,DbgGraph.K);
    }

    public Dataset<Row> createRecordRanges(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, Dataset<StrandKmerDT> recordKmers) {
        Dataset<Row> plusRecordRanges = createRecordStrandRanges(sequenceKmers,StrandKmerDT.filterStrand(recordKmers,true)).
                withColumn(ColumnIdentifier.STRAND, functions.lit(true).cast(DataTypes.BooleanType));
        Dataset<Row> minusRecordRanges = createRecordStrandRanges(sequenceKmers,StrandKmerDT.filterStrand(recordKmers,false)).
                withColumn(ColumnIdentifier.STRAND, functions.lit(false).cast(DataTypes.BooleanType));
        return plusRecordRanges.union(minusRecordRanges);
    }

    public static Dataset<Row> createRecordLengthStatistics(Dataset<Row> recordRanges) {
        return recordRanges.select(ColumnIdentifier.LENGTH).
                groupBy().
                agg(max(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MAX),
                        mean(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MEAN));
    }

    public static Dataset<Row> createRecordLengthDistribution(Dataset<Row> recordRanges) {
        return recordRanges.
                groupBy(ColumnIdentifier.LENGTH).agg(count(ColumnIdentifier.LENGTH).as(ColumnIdentifier.COUNT)).
                orderBy(desc(ColumnIdentifier.COUNT));
    }

    public static Dataset<Row> createRecordLengthRanking(Dataset<Row> recordRanges) {
        return recordRanges.groupBy(ColumnIdentifier.RECORD_ID).
                agg(max(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MAX),
                        mean(ColumnIdentifier.LENGTH).as(ColumnIdentifier.MEAN),
                        sum(ColumnIdentifier.LENGTH).as(ColumnIdentifier.SUM)
                        ).orderBy(desc(ColumnIdentifier.MEAN));
    }

    public static Dataset<RangeDT> createRangesForStrand(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, boolean strand, int minCount) {
        org.apache.spark.sql.Column countColumn = (strand ? col(ColumnIdentifier.COUNT_PLUS) : col(ColumnIdentifier.COUNT_MINUS));
        return RangeDT.createRanges(
                sequenceKmers.filter(countColumn.gt(minCount)).
                        orderBy(ColumnIdentifier.POSITION));

    }

    public static Dataset<RangeDT> createCyclicRanges(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, int minCount, RangeDT boundaryRange) {
        return   RangeDT.createCyclicRangesFromPositions(
                sequenceKmers.filter(col(ColumnIdentifier.COUNT_PLUS).gt(minCount).or(col(ColumnIdentifier.COUNT_MINUS).gt(minCount))).
                        orderBy(ColumnIdentifier.POSITION),boundaryRange);
    }

    public static Dataset<RangeDT> createRanges(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, int minCount) {
        return RangeDT.createRanges(
                sequenceKmers.filter(col(ColumnIdentifier.COUNT_PLUS).gt(minCount).or(col(ColumnIdentifier.COUNT_MINUS).gt(minCount))).
                        orderBy(ColumnIdentifier.POSITION));
    }


    public static Dataset<Integer> getKmersMappingOnStrandOnly(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, boolean strand) {
        org.apache.spark.sql.Column countColumnStrand = (strand ? col(ColumnIdentifier.COUNT_PLUS) : col(ColumnIdentifier.COUNT_MINUS));
        org.apache.spark.sql.Column countColumnOtherStrand = (strand ? col(ColumnIdentifier.COUNT_MINUS) : col(ColumnIdentifier.COUNT_PLUS));
        return sequenceKmers.filter(countColumnStrand.gt(0).and(countColumnOtherStrand.equalTo(0))).select(ColumnIdentifier.POSITION).as(Encoders.INT());
    }

    public  List<List<Integer>> createRangeIndexTuples(List<RangeDT> ranges) {
        List<List<Integer>> rangeTuples = new ArrayList<>();
        for(int i = 0; i < ranges.size()-1; i++) {
            LOGGER.debug("Adding range tuple indecees " + i + " " + (i+1));
            rangeTuples.add(new ArrayList<>(Arrays.asList(i,i+1)));
        }
        if(sequenceInformation.isTopology()) {
            LOGGER.debug("Adding range tuple indecees " + (ranges.size()-1)+ " " + 0);
            rangeTuples.add(new ArrayList<>(Arrays.asList(ranges.size()-1,0)));
        }
        return rangeTuples;
    }

    public Dataset<Integer> createPositions(Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges) {
        return RangeDT.createCyclicPositions(mappedRanges.select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH).distinct(),getBoundaryRange()).
                orderBy(ColumnIdentifier.POSITION);
    }

    public Dataset<RangeDT> createRanges(Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges) {
        Dataset<RangeDT>  dataset = RangeDT.createCyclicRangesFromPositions(createPositions(mappedRanges),getBoundaryRange()).orderBy(ColumnIdentifier.START);
        System.out.println(dataset);
        SparkComputer.showAll(dataset);
        return dataset;
    }

    public Dataset<RangeDT> createRemainingGaps(Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges,boolean cyclic) {
//        Dataset<Row> cyclic = mappedRanges.filter(col(ColumnIdentifier.END))
//        Dataset<Row> dataset = mappedRanges.groupBy(ColumnIdentifier.START).agg(max(ColumnIdentifier.END).as(ColumnIdentifier.END)).
//                orderBy(ColumnIdentifier.START);
//        List<RangeDT> ranges = new ArrayList<>();
//        RangeDT range = new RangeDT(0,0);
//        for(Row rangeData : dataset.collectAsList()) {
//            int start  = rangeData.getInt(rangeData.fieldIndex(ColumnIdentifier.START));
//            int stop  = rangeData.getInt(rangeData.fieldIndex(ColumnIdentifier.END));
//            if(start <= range.getEnd()+1 ) {
//                if(stop > range.getEnd()) {
//                    range.setEnd(stop);
//                }
//            }
//            else {
//                ranges.add(range);
//                range = new RangeDT(start,stop);
//            }
//        }
//        ranges.add(range);
//        for(int i = 0; i < ranges.size(); i++) {
//            RangeDT r = ranges.get(i);
//            if(r.cyclic()) {
//                r.setLength(sequenceInformation.getLength()-r.getStart()+r.getEnd()+1);
//            }
//            else {
//                r.setLength(r.getEnd()-r.getStart()+1);
//            }
//        }
//        System.out.println(ranges);
//        Dataset<RangeDT> gaps = RangeDT.createCyclicInverseRange(SparkComputer.createDataFrame(ranges,RangeDT.ENCODER),getBoundaryRange());

        Dataset<RangeDT> gaps = cyclic ? RangeDT.createCyclicInverseRange(createRanges(mappedRanges),getBoundaryRange()) :
                RangeDT.createInverseRange(createRanges(mappedRanges),getBoundaryRange());
//        System.out.println("gaps");
//        SparkComputer.showAll(gaps);
        return gaps;
    }
//    public static Dataset<KmerDT> transformRecordPositions(Dataset<KmerDT> kmerDTDataset) {
//        kmerDTDataset.join(recordDTDataset,
//                kmerDTDataset.col(ColumnIdentifier.RECORD_ID).
//                        equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
//                select(col(ColumnIdentifier.KMER),col(ColumnIdentifier.POSITIONS),kmerDTDataset.col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.LENGTH)).
//
//    }

    /**** Statistics *************************************************************************************************************************************************************************************************/
    public static void analyzeScoreResults(Dataset<ScoredPositionPositionKmerMappingDT> scoreResults) {
        Dataset<Row> recordScores = scoreResults.groupBy(ColumnIdentifier.RECORD_ID).agg(
                first(ColumnIdentifier.KMER).as(ColumnIdentifier.KMER),
                first(ColumnIdentifier.SCORE).as(ColumnIdentifier.SCORE),
                first(ColumnIdentifier.EVALUE).as(ColumnIdentifier.EVALUE)
        );
        Dataset<Row> kmerCounts = recordScores.groupBy(ColumnIdentifier.KMER).count();

        Dataset<Row> meanScores = recordScores.select(ColumnIdentifier.EVALUE).groupBy().mean(ColumnIdentifier.EVALUE);
//                recordScores.groupBy().
//                agg(round(mean(ColumnIdentifier.EVALUE),2).as(ColumnIdentifier.MEAN));
        SparkComputer.showAll(kmerCounts);
        SparkComputer.showAll(meanScores);
    }

    public static void analyzeBridgedRanges(Dataset<RecordDiagonalBiPositionRangeDT> scoreRanges) {
        LOGGER.info("Plus strand");
        Dataset<RecordDiagonalBiPositionRangeDT> plusFiltered = scoreRanges.filter(col(ColumnIdentifier.STRAND).equalTo(true));

        LOGGER.info("Count: " + plusFiltered.count());
        SparkComputer.showAll(plusFiltered.groupBy(ColumnIdentifier.LENGTH).count());

        LOGGER.info("Minus strand");
        Dataset<RecordDiagonalBiPositionRangeDT> minusFiltered = scoreRanges.filter(col(ColumnIdentifier.STRAND).equalTo(false));
        LOGGER.info("Count: " + minusFiltered.count());
        SparkComputer.showAll(minusFiltered.groupBy(ColumnIdentifier.LENGTH).count());
    }

    public static void evaluateCluster(List<TreeNode> propertyCluster) {
        List<TreeNode> tieList = propertyCluster.stream().filter(treeNode -> treeNode.getProperties().size() > 1).collect(Collectors.toList());
        LOGGER.info("Unique properties: " + (propertyCluster.size()-tieList.size()));
        LOGGER.info("Tie properties: " + tieList.size() + "(" + (((double)tieList.size())/propertyCluster.size()) + ")");
        LOGGER.info("Tie properties amount: " + tieList.stream().mapToInt(treeNode -> treeNode.getProperties().size()).summaryStatistics());
        LOGGER.info("Tie property norm scores: " + tieList.stream().mapToDouble(treeNode -> treeNode.getProperties().get(0).getNormScore().doubleValue()).summaryStatistics());
    }

    public long getUnbridgedCount(Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges) {
        Long result = SparkComputer.getFirstOrElseNull(createRemainingGaps(mappedRanges,true).groupBy().
                agg(sum(ColumnIdentifier.LENGTH)).as(Encoders.LONG()));
//        Long result = SparkComputer.getFirstOrElseNull(createRemainingGaps(mappedRanges,sequenceInformation.isTopology()).groupBy().
//                agg(sum(ColumnIdentifier.LENGTH)).as(Encoders.LONG()));
        return result == null ? 0 : result;
    }

    public long getUnMappedCount() {
        int minCount  = 3, minLength = 0;
        Dataset<RangeDT> rangesDataset =
                sequenceInformation.isTopology() ?
                        SequenceMapAnalyzer.createCyclicRanges(sequenceKmers,minCount,getBoundaryRange()).
                                filter(col(ColumnIdentifier.LENGTH).geq(minLength)) :
                        SequenceMapAnalyzer.createRanges(sequenceKmers,minCount).
                                filter(col(ColumnIdentifier.LENGTH).geq(minLength));
//        SparkComputer.showAll(rangesDataset);

        Dataset<RangeDT> inverseRangesDataset =
                sequenceInformation.isTopology() ?
                        RangeDT.createCyclicInverseRange(rangesDataset,getBoundaryRange()) :
                        RangeDT.createInverseRange(rangesDataset,getBoundaryRange());
        Long result = SparkComputer.getFirstOrElseNull(inverseRangesDataset.groupBy().agg(sum(ColumnIdentifier.LENGTH)).as(Encoders.LONG()));
//        SparkComputer.showAll(inverseRangesDataset);
        return result == null ? 0 : result;
    }

    public  void evaluateMappedRanges(Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges) {
        Dataset<RangeDT> gaps = createRemainingGaps(mappedRanges,sequenceInformation.isTopology());
        SparkComputer.showAll(gaps);
        System.out.println(gaps.groupBy().agg(sum(ColumnIdentifier.LENGTH)).first());

        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);

        taxonomicGroupDataset.filter(col(ColumnIdentifier.NAME).equalTo(getSequenceInformation().getId())).show();

        Dataset<Row> recordWeights = computeLengthDependentWeights(mappedRanges);
//        recordWeights.show();
        Dataset<Row> results = recordWeights.join(taxonomicGroupDataset,
                taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID).
                        equalTo(recordWeights.col(ColumnIdentifier.RECORD_ID))
                ).drop(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID));
        Dataset<Row> aggregatedResults = results.groupBy(col(ColumnIdentifier.TAXONOMIC_GROUP_NAME)).
                agg(max(col(ColumnIdentifier.WEIGHT)).as(ColumnIdentifier.WEIGHT_MAX),
                        mean(col(ColumnIdentifier.WEIGHT)).as(ColumnIdentifier.WEIGHT_MEAN),
                        max(col(ColumnIdentifier.COUNT)).as(ColumnIdentifier.COUNT_MAX),
                        mean(col(ColumnIdentifier.COUNT)).as(ColumnIdentifier.COUNT_MEAN),
                        max(col(ColumnIdentifier.MEAN_LENGTH)).as(ColumnIdentifier.MAX_LENGTH),
                        mean(col(ColumnIdentifier.MEAN_LENGTH)).as(ColumnIdentifier.MEAN_LENGTH)
                );
        SparkComputer.showAll(aggregatedResults);
//        recordWeights.withColumn("test",col(ColumnIdentifier.WEIGHT).divide(ColumnIdentifier.MIN_RECORD_LENGTH).cast(DataTypes.DoubleType)).show();
//        recordWeights.orderBy(ColumnIdentifier.WEIGHT).show(500);
    }

    /**** Auxiliary *************************************************************************************************************************************************/

    private static Dataset<Row> renameInverseRanges(Dataset<RecordDiagonalPositionRangeDT> dataset) {
        return dataset.withColumnRenamed(ColumnIdentifier.START,ColumnIdentifier.INVERSE_START).
                withColumnRenamed(ColumnIdentifier.END,ColumnIdentifier.INVERSE_END).
                withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.INVERSE_LENGTH).
                withColumnRenamed(ColumnIdentifier.RECORD_ID,ColumnIdentifier.INVERSE_RECORD_ID).
                withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,ColumnIdentifier.DIAGONAL_IDENTIFIER);
    }

    public void setRecordSubstringComputer() {
        GenomeTable genomeTable = GenomeTable.getInstance();
        recordSubstringComputer = checkFunctionWrapper((v1, v2, v3,v4) -> genomeTable.getSubSequence(v1,v2,v3,v4));
//        recordSubstringComputer = (recordStrand ?
//                checkFunctionWrapper((v1, v2, v3) -> genomeTable.getPlusSubSequence(v1,v2,v3)) :
//                checkFunctionWrapper((v1, v2, v3) -> genomeTable.getMinusSubSequence(v1,v2+DbgGraph.K,v3+DbgGraph.K)));
    }

    public static boolean identifyStrand(Dataset<PositionStrandCountIdentifierDT> sequenceKmers) {
        long plusSum = sequenceKmers.select(ColumnIdentifier.COUNT_PLUS).groupBy().sum().as(Encoders.LONG()).first();
        long minusSum = sequenceKmers.select(ColumnIdentifier.COUNT_MINUS).groupBy().sum().as(Encoders.LONG()).first();
        LOGGER.debug("Plus sum: " + plusSum + ",minus sum: " + minusSum);
        if(plusSum > minusSum) {
            LOGGER.debug("Identified plus strand");
            return true;
        }
        LOGGER.debug("Identified minus strand");
        return false;
    }

    private int getGapSize(RangeDT range1, RangeDT range2) {
        return Auxiliary.mod(range2.getStart()-
                range1.getEnd(),sequenceInformation.getLength());
    }


    private RangeDT getGap(RangeDT range1, RangeDT range2) {
        return new RangeDT(Auxiliary.mod(range1.getEnd()+1,sequenceInformation.getLength()),
                Auxiliary.mod(range2.getStart()-1,sequenceInformation.getLength()));
    }

    private boolean containedRanges(RangeDT range1, RangeDT range2) {
        return RangeDT.containedRanges(range1,range2,sequenceInformation.getLength());
    }

    private boolean containedRanges(RangeDT range1, RangeDT range2, int recordLength) {
        return RangeDT.containedRanges(range1,range2,recordLength);
    }

    private int getCyclicGapSize(RangeDT range) {
        return sequenceInformation.getLength()-range.getLength();
    }

    private int getRightBoundaryGapSize(RangeDT range) {
        return Auxiliary.mod(sequenceInformation.getLength()-range.getEnd(),sequenceInformation.getLength());
    }

    private int getLeftBoundaryGapSize(RangeDT range) {
        return Auxiliary.mod(range.getStart(),sequenceInformation.getLength());
    }

    private static String getMinusSubsequence(String sequence, int startPos, int endPos) {
        return new StringBuilder(getPlusSubSequence(sequence,sequence.length()-endPos-1,
                sequence.length()-startPos-1)).reverse().toString();
    }

    private static String getPlusSubSequence(String sequence, int startPos, int endPos) {

        if(startPos > endPos) {
            return sequence.substring(startPos)+
                    sequence.substring(0,endPos+1) ;
        }
        else {
            return sequence.substring(startPos,endPos+1);
        }
//        if(stopPos+1 > sequence.length()-1 || startPos+1 > sequence.length()-1 || stopPos < 0 || startPos < 0) {
//            LOGGER.debug("Invalid range boundaries. Must be within [" + 0 + ","+ (sequence.length()-1) +"]");
//            return null;
//        }
//        if(startPos > startPos) { // cyclic boundary
//            return sequence.substring(stopPos) + sequence.substring(0,startPos+1);
//        }
//        return sequence.substring(startPos,stopPos+1);
    }


    public static void printPropertyCluster(List<TreeNode> propertyCluster) {
        propertyCluster.forEach(treeNode -> System.out.println(treeNode));
    }

    public static void printPropertyClusterProperties(List<TreeNode> propertyCluster) {
        propertyCluster.forEach(treeNode -> System.out.println(treeNode.getRange() + ": " + treeNode.propertiesAsString()));
    }

    public static void printPropertyClusterDetails(List<TreeNode> propertyCluster) {
        propertyCluster.forEach(treeNode -> System.out.println(treeNode.detailsAsString()));
    }

    public static void printPropertyClusterNormWeights(List<TreeNode> propertyCluster) {
        propertyCluster.forEach(treeNode -> System.out.println( treeNode + " -> " +(BigDecimal.ONE.subtract(ScoredProperty.normSum(treeNode.getProperties())))));
    }

    /**** Getter and Setter *************************************************************************************************************************************************/


    public Dataset<RecordDiagonalBiPositionRangeDT> computeMappedRange(RangeDT range) {
        return
        RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                decreaseRangeStart(computeDiagonalRanges(range),DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER));
    }

    public void setDiagonalRanges(int minCount) {
        setRanges(minCount);

        List<RangeDT> newRanges = new ArrayList<>();
        try{
            for(RangeDT range: ranges) {


                if(range.getLength() >= parameter.minRangeLength) {
                    Dataset<RecordDiagonalBiPositionRangeDT> diagonalRange =
                            computeDiagonalRanges(range);
                    diagonalRange = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                            decreaseRangeStart(diagonalRange,DbgGraph.K,RecordDiagonalBiPositionRangeDT.ENCODER));
                    diagonalRange.cache();
                    diagonalRanges.add(diagonalRange);
                    newRanges.add(new RangeDT(range));
                }

            }
            ranges = newRanges;

            LOGGER.debug("Done computing diagonal ranges");
            Auxiliary.printSeq(ranges);
        } catch (RuntimeException e) {
            System.out.println("error");
            e.printStackTrace();
        }

    }

    public GraphTraversalSource fetchSubGraph(boolean strand){
        return Graph.readGraphML(resultDirectoryTree.getSubGraphName(strand));
    }

    /**
     * Fetch Record kmers that could be mapped to the sequence kmers of the provided strand.
     * The column strand of the result set identifies whether record kmers belong to plus or minus strand.
     * @return
     */

    public void getPropertySummary(Dataset<Row> properties) {
        SparkComputer.showAll(properties.groupBy(ColumnIdentifier.RANK).count());
//        SparkComputer.showAll();
    }

    public Dataset<StrandKmerDT> fetchRecordKmers() {
        return SparkComputer.read(resultDirectoryTree.getParquetPath(
                ResultDirectoryTree.PARQUET_NAME.RECORD_KMERS),
                StrandKmerDT.ENCODER,StrandKmerDT.SCHMEA);
    }

    /**
     * Shift records on negative strand back to how they are annotated in the sequence files
     * @param dataset
     * @return
     */
    public Dataset<RecordDiagonalBiPositionRangeDT> shiftRangesToStrand(Dataset<RecordDiagonalBiPositionRangeDT> dataset) {

        //
        Dataset<Row> dataset2 = RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                dataset.filter(col(ColumnIdentifier.RECORD_LENGTH).equalTo(-1)).
                drop(ColumnIdentifier.RECORD_LENGTH).
                join(recordDTDataset.withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.RECORD_LENGTH).
                        withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),
                        dataset.col(ColumnIdentifier.RECORD_ID).
                                equalTo(col("r")))).
                union(
                        RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                        dataset.filter(col(ColumnIdentifier.RECORD_LENGTH).notEqual(-1)))).
                withColumnRenamed(ColumnIdentifier.RECORD_END,ColumnIdentifier.RECORD_END_2).
                withColumnRenamed(ColumnIdentifier.RECORD_START,ColumnIdentifier.RECORD_START_2);


        return RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                dataset2.
                filter(col(ColumnIdentifier.STRAND).equalTo(false)).
                withColumn(ColumnIdentifier.RECORD_START,
                        col(ColumnIdentifier.RECORD_LENGTH).minus(col(ColumnIdentifier.RECORD_END_2)).minus(1)).
                withColumn(ColumnIdentifier.RECORD_END,
                        col(ColumnIdentifier.RECORD_LENGTH).minus(col(ColumnIdentifier.RECORD_START_2)).minus(1))).
                union(
                        RecordDiagonalBiPositionRangeDT.convertToRecordDiagonalBiPositionRange(
                                dataset2.filter(col(ColumnIdentifier.STRAND).equalTo(true)).
                                        withColumnRenamed(ColumnIdentifier.RECORD_END_2,ColumnIdentifier.RECORD_END).
                                        withColumnRenamed(ColumnIdentifier.RECORD_START_2,ColumnIdentifier.RECORD_START)));
    }

    public Dataset<StrandedRecordKmerDT> getShiftedKmers() {
        final String explodeIdentifier = "eT";
        Column recordLengthColumn = when(col(ColumnIdentifier.TOPOLOGY).equalTo(true),
                recordDTDataset.col(ColumnIdentifier.LENGTH)).otherwise(lit(-1)).as(ColumnIdentifier.LENGTH);

        Dataset<StrandKmerDT> strandedRecordKmers = fetchRecordKmers();
        Dataset<Row> newKmers = strandedRecordKmers.
                join(recordDTDataset,
                        strandedRecordKmers.col(ColumnIdentifier.RECORD_ID).
                                equalTo(recordDTDataset.col(ColumnIdentifier.RECORD_ID))).
                withColumn(explodeIdentifier,explode(col(ColumnIdentifier.POSITIONS)));


        return newKmers.filter(col(ColumnIdentifier.STRAND).equalTo(false)).
                select(recordDTDataset.col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.KMER),
                        col(ColumnIdentifier.LENGTH).minus(col(explodeIdentifier)).minus(1).as(ColumnIdentifier.RECORD_POSITION),
                        recordLengthColumn,col(ColumnIdentifier.STRAND)).
                union(
                        newKmers.filter(col(ColumnIdentifier.STRAND).equalTo(true)).
                                select(recordDTDataset.col(ColumnIdentifier.RECORD_ID),col(ColumnIdentifier.KMER),
                                        col(explodeIdentifier).as(ColumnIdentifier.RECORD_POSITION),
                                        recordLengthColumn,col(ColumnIdentifier.STRAND))
                ).withColumnRenamed(ColumnIdentifier.LENGTH,ColumnIdentifier.RECORD_LENGTH).as(StrandedRecordKmerDT.ENCODER);

    }

    public Dataset<PositionStrandCountIdentifierDT> fetchSequenceKmers() {
        return SparkComputer.read(resultDirectoryTree.getParquetPath(
                ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS),
                PositionStrandCountIdentifierDT.ENCODER, PositionStrandCountIdentifierDT.SCHMEA);
    }

    protected String fetchGenome(boolean strand) {
        return SparkComputer.read(resultDirectoryTree.getParquetPath(
                ResultDirectoryTree.PARQUET_NAME.GENOME,strand), Encoders.STRING()).head();
    }

    protected SequenceInformationDT fetchSequenceInformation() {
        SequenceInformationDT sequenceInformationDT =  SparkComputer.getFirstOrElseNull(SparkComputer.read(resultDirectoryTree.getParquetPath(
                ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA),SequenceInformationDT.ENCODER));
        LOGGER.info(sequenceInformationDT+ "");
        return sequenceInformationDT;
    }

    public SequenceInformationDT getSequenceInformation() {
        if(sequenceInformation == null) {
            sequenceInformation = fetchSequenceInformation();
        }
        return sequenceInformation;
    }

    public String getPlusStrandGenome() {
        if(plusStrandGenome == null) {
            plusStrandGenome = fetchGenome(true);
        }
        return plusStrandGenome;
    }

    public String getMinusStrandGenome() {
        if(minusStrandGenome == null) {
            minusStrandGenome = fetchGenome(false);
        }
        return minusStrandGenome;
    }

    public String getPlusSubSequence(int startPos, int endPos) {
        return getPlusSubSequence(getPlusStrandGenome(),startPos,endPos);
//        if(startPos > endPos) {
//            return getPlusStrandGenome().substring(startPos)+
//                    getPlusStrandGenome().substring(0,endPos+1) ;
//        }
//        else {
//            return getPlusStrandGenome().substring(startPos,endPos+1);
//        }
    }

    public String getMinusSubSequence(int startPos, int endPos) {
//        int start = sequenceInformation.getLength()-endPos-1;
//        int end = sequenceInformation.getLength()-startPos-1;
//        System.out.println(start + " " + end);
//        if(start > end) {
//            System.out.println(getMinusStrandGenome().substring(start));
//            System.out.println(getMinusStrandGenome().substring(0,end+1));
//            System.out.println(getMinusStrandGenome().substring(0,end+1) + getPlusStrandGenome().substring(start));
//            return new StringBuilder(getMinusStrandGenome().substring(0,end+1) + getPlusStrandGenome().substring(start)).reverse().toString();
//        }
//        else {
//            return new StringBuilder(getMinusStrandGenome().substring(start,end+1)).reverse().toString();
//        }
        return getMinusSubsequence(getMinusStrandGenome(),startPos,endPos);
    }

    public RangeDT getBoundaryRange() {
        if(boundaryRange == null) {
            boundaryRange = new RangeDT(0,getSequenceInformation().getLength()-1);
        }
        return boundaryRange;
    }

    public static void test(Dataset<Integer> positions) {
        WindowSpec w = Window.partitionBy("length").orderBy("start");
        Column pos = positions.col("position");
        Dataset<RangeDT> ranges = RangeDT.createRangesForPositions(positions);
        ranges.show();
        ranges.withColumn("inverseStart",col("start").minus(lag(col("end"),1,0).over(w)))
                .select("length","start","end","inverseStart")
                .show();

//        WindowSpec w = Window.orderBy("position");
////        positions.withColumn("row",currentRow()).show();
//        positions.withColumn("diff",positions.col("position").minus(row_number().over(w))).
//        groupBy("diff").agg(min(pos).as("start"),max(pos).as("end"),max(pos).minus(min(pos)).plus(1).as("length")).
//    show();
//        Window.orderBy(col("position"))
    }



//    public void persistLongKmerCounts(boolean strand) {
//
//
//        String strandIdentifier= (strand ? "_plus" : "_minus");
//        LOGGER.info("persising " + strandIdentifier);
//        final String countTable = "countkmers" + strandIdentifier,
//                rangeTablePlus = "rangePlus" + strandIdentifier ,
//                rangeTableMinus = "rangeMinus" + strandIdentifier;
//        Map<String,Integer []> sequenceMap = SequenceParser.getKmerMap(
//                strand ? getPlusStrandGenome() : getMinusStrandGenome(),
//                DbgGraph.K,
//                getSequenceInformation().isTopology(),
//                strand,
//                SequenceParser.AmbigFilter.NO_FILTER);
//        System.out.println(sequenceMap);
//
//        Dataset<PositionCountIdentifierDT> plusKmerCounts = getDbgGraph().getRecordCountPositionKmerDistribution(sequenceMap,true).cache();
//        Dataset<RangeDT> plusRanges = RangeDT.
//                createRanges(plusKmerCounts.filter(col(ColumnIdentifier.COUNT).gt(0)).orderBy(ColumnIdentifier.POSITION));
//        plusRanges.write().bucketBy(4,ColumnIdentifier.LENGTH).sortBy(ColumnIdentifier.START).
//                option(Spark.PATH_OPTION_KEY,
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.RANGE,strand,true)).
//                saveAsTable(rangeTablePlus);
//
//
//        Dataset<PositionCountIdentifierDT> minusKmerCounts = getDbgGraph().getRecordCountPositionKmerDistribution(sequenceMap,false).cache();
//        Dataset<RangeDT> minusRanges = RangeDT.
//                createRanges(minusKmerCounts.filter(col(ColumnIdentifier.COUNT).gt(0)).orderBy(ColumnIdentifier.POSITION));
//        minusRanges.write().bucketBy(4,ColumnIdentifier.LENGTH).sortBy(ColumnIdentifier.START).
//                option(Spark.PATH_OPTION_KEY,
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.RANGE,strand,false)).
//                saveAsTable(rangeTableMinus);
//
//
//        final String identifier = "i", positionIdentifier = "p";
//        Dataset<PositionStrandCountIdentifierDT> kmerCounts = PositionStrandCountIdentifierDT.convert(plusKmerCounts.
//                withColumn(ColumnIdentifier.STRAND, functions.lit(true).cast(DataTypes.BooleanType)).
//                withColumnRenamed(ColumnIdentifier.IDENTIFIER,identifier).
//                withColumnRenamed(ColumnIdentifier.POSITION,positionIdentifier).
//                withColumnRenamed(ColumnIdentifier.COUNT,ColumnIdentifier.COUNT_PLUS).
//                join(
//                        minusKmerCounts.
//                                withColumn(ColumnIdentifier.STRAND, functions.lit(false).cast(DataTypes.BooleanType)).
//                                withColumnRenamed(ColumnIdentifier.COUNT,ColumnIdentifier.COUNT_MINUS),
//                        col(positionIdentifier).equalTo(col(ColumnIdentifier.POSITION)), "left"
//                ));
////        LOGGER.info("KmerCounts " + kmerCounts.count());
////        kmerCounts.show();
//        Dataset<RangeDT> joinedRanges = RangeDT.createRanges(kmerCounts.
//                filter(col(ColumnIdentifier.COUNT_PLUS).plus(col(ColumnIdentifier.COUNT_MINUS)).gt(0)).
//                orderBy(ColumnIdentifier.POSITION));
//        joinedRanges.write().bucketBy(4,ColumnIdentifier.LENGTH).sortBy(ColumnIdentifier.START).
//                option(Spark.PATH_OPTION_KEY,
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.RANGE,strand)).
//                saveAsTable("joined");
//
//        kmerCounts.write().
//                bucketBy(4,ColumnIdentifier.COUNT_MINUS).
//                bucketBy(4,ColumnIdentifier.COUNT_PLUS).
//                sortBy(ColumnIdentifier.POSITION).
//                option(
//                        Spark.PATH_OPTION_KEY,
//                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS,strand)).
//                saveAsTable(countTable);
//
//    }

    public DbgGraph getDbgGraph() {
        if(dbgGraph == null) {
            dbgGraph = (DbgGraph)
                    new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                            startOption(LaunchExternal.LAUNCH_OPTION.START).build();
        }
        return dbgGraph;
    }

    public void setDbgGraph(DbgGraph dbgGraph) {
        this.dbgGraph = dbgGraph;
    }

    public void persistGenomePosterior() {
        try {
            SequenceMapCreator.persistGenome(GenomeTable.getInstance().
                    getPlusSequence(RecordDT.mapToRecordId(getSequenceInformation().getId())),resultDirectoryTree);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<TreeNode> fetchPropertyCluster(boolean strand) {
        if(strand) {
            return FileIO.deSerializeJsonList(resultDirectoryTree.plusPropertyCluster,TreeNode.class);
        }
        return FileIO.deSerializeJsonList(resultDirectoryTree.minusPropertyCluster,TreeNode.class);
    }

    public Dataset<RecordDiagonalBiPositionRangeDT> fetchMappedRanges() {
        return SparkComputer.read(resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES),RecordDiagonalBiPositionRangeDT.ENCODER);
    }

    public Dataset<RecordDiagonalBiPositionRangeDT> fetchBridgedRanges() {
        return SparkComputer.read(resultDirectoryTree.
                getParquetPath(ResultDirectoryTree.PARQUET_NAME.BRIDGED_RANGES),
                RecordDiagonalBiPositionRangeDT.ENCODER);
    }

    public Dataset<RecordDiagonalBiPositionRangeDT> fetchJoinedRanges() {
        Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges = fetchMappedRanges();
        return mappedRanges.union(fetchBridgedRanges()).orderBy(col(ColumnIdentifier.RECORD_ID));
    }

    public ResultDirectoryTree getResultDirectoryTree() {
        return resultDirectoryTree;
    }

    /**** Stuff I will probably never need *************************************************************************************************************************************************/
    private <T extends PositionInterface & IdentifierInterface> void createMatchGraph(Dataset<T> sequenceKmerDataset, boolean strand) {
        Dataset<NodePairPositionDT> nodePairs = NodePairPositionDT.convert(sequenceKmerDataset);
        List<NodePairPositionDT> misMatchedNodePairs = nodePairs.collectAsList();

        LOGGER.info("Creating matching subgraph");
        // create subgraph of matched nodes and topology edges
        DbgGraph dbgGraph = getDbgGraph();
        GraphTraversalSource g = dbgGraph.createSubGraph(dbgGraph.getTopologyEdgesToNodePairs(misMatchedNodePairs));
        LOGGER.info("Adding all nodes");
        // get all mismatched nodes and add to graph
        List<String> misMatchedNodes = NodePairDT.getDistinctNodes(misMatchedNodePairs);
        g.inject(misMatchedNodes).unfold().addV(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier).
                property(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,__.identity()).
                property(SequenceMapGraph.SEQUENCE_NODE_ATTRIBUTES.FILTER_FLAG.identifier,
                        SequenceMapGraph.FILTER.NON_MAPPING).iterate();







        List<Map<String,Object>> nodePairList = new ArrayList<>();
        nodePairs.collectAsList().forEach(n -> {
            HashMap<String,Object> map = new HashMap<>();
            map.put(ColumnIdentifier.V1, n.getV1());
            map.put(ColumnIdentifier.V2, n.getV2());
            map.put(ColumnIdentifier.POSITION, n.getPosition());
            nodePairList.add(map);
        } );

        LOGGER.info("Adding sequence edges");
//        List l = g.withSideEffect("nodePairs",nodePairList).
//                V().
//                group("m").
//                by(__.values(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier)).
//                by(__.unfold()).toList();
//        System.out.println(l.get(0));
//        System.out.println(g.withSideEffect("nodePairs",nodePairList).
//                V().
//                group("m").
//                by(__.values(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier)).
//                by(__.unfold()).
//                select("m").select("TCCTAACCTTACCGTT").toList());

//                select("nodePairs").unfold().as("nodePair").
//                addE(DbgGraph.DBG_SUBGRAPH_EDGE_LABEL.SEQUENCE_EDGE.identifier).
//                from(__.select("m").select(__.select("nodePair").select(ColumnIdentifier.V1))).
//                to(__.select("m").select(__.select("nodePair").select(ColumnIdentifier.V2))).
//                property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,__.select("nodePair").unfold().select(ColumnIdentifier.POSITION)).
//                iterate();
//
//        g.withSideEffect("nodePairs",nodePairList).
//                V().
//                sideEffect(__.
//                group("m").
//                by(__.values(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier)).
//                by(__.unfold())).
//                select("nodePairs").unfold().as("nodePair").
//                addE(DbgGraph.DBG_SUBGRAPH_EDGE_LABEL.SEQUENCE_EDGE.identifier).
//                from(__.select("m").select(org.apache.tinkerpop.gremlin.structure.Column.keys).select(__.select("nodePair").select(org.apache.tinkerpop.gremlin.structure.Column.keys).select(ColumnIdentifier.V1))).
//                to(__.select("m").select(org.apache.tinkerpop.gremlin.structure.Column.keys).select(__.select("nodePair").select(org.apache.tinkerpop.gremlin.structure.Column.keys).select(ColumnIdentifier.V2))).
//                property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,__.select("nodePair").select(org.apache.tinkerpop.gremlin.structure.Column.keys).select(ColumnIdentifier.POSITION)).
//                iterate();
//        g.
//                V().
//                        group("m").
//                        by(__.values(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier)).
//                        by(__.unfold()).
//                select("m").select(org.apache.tinkerpop.gremlin.structure.Column.keys).toList().forEach(k -> System.out.println(k));

//
////        Auxiliary.printSeq(allDistinctNodes);
//
//        // add all sequence edges to graph
//        GraphTraversal<?,?> graphTraversal = g.inject(0);
        Iterator<NodePairPositionDT> nodPairIterator = nodePairs.toLocalIterator();
        while (nodPairIterator.hasNext()){
            NodePairPositionDT nodePairPosition = nodPairIterator.next();
            g.addE(DbgGraph.DBG_SUBGRAPH_EDGE_LABEL.SEQUENCE_EDGE.identifier).
                    from(__.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,nodePairPosition.getV1())).
                    to(__.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,nodePairPosition.getV2())).
                    property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,nodePairPosition.getPosition()).iterate();
        }
//        graphTraversal.iterate();

        LOGGER.info("Writing graph to file");
        Graph.writeToGraphML(resultDirectoryTree.getSubGraphName(strand),g);

    }


}

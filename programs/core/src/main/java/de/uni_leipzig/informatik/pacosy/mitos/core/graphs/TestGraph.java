package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.KmersTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.KmerDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.PositionCountIdentifierDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.StrandKmerDT;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Traverser;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.EdgeLabel;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.graphdb.database.management.ManagementSystem;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class TestGraph extends Graph {
    static final String KEYSPACE = "test";

    public void createSchema() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // vertex attributes
        final PropertyKey prefix = management.makePropertyKey(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier).dataType(String.class).make();

        // edge attributes
        final PropertyKey record = management.makePropertyKey(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier).dataType(Integer.class).make();
        management.makePropertyKey(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier).dataType(Integer.class).make();
        final PropertyKey strand = management.makePropertyKey(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier).dataType(Boolean.class).make();

        final PropertyKey recordCount = management.makePropertyKey(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).dataType(Integer.class).make();

        EdgeLabel topologyLabel = management.makeEdgeLabel(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();

        String topology_index = "topology_index";
        management.buildIndex(topology_index, Edge.class).addKey(recordCount).buildMixedIndex("search");


        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
        management.buildIndex("record_index", Edge.class).addKey(record).buildCompositeIndex();
        management.buildIndex("strand_index", Edge.class).addKey(strand).buildCompositeIndex();


        management.makeEdgeLabel(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeVertexLabel(DbgGraph.DBG_NODE_LABEL.PREFIX_NODE.identifier).make();

        management.commit();

        try {

            ManagementSystem.awaitGraphIndexStatus(graph,topology_index).call();
            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "record_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "strand_index").call();

        } catch (InterruptedException  i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

        openGraph();
        management.commit();
    }

    public TestGraph() {
        super();
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    protected boolean schemaGenerationSuccessful() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        List<JanusGraphIndex> vertexIndecees = (List)management.getGraphIndexes(Vertex.class);
        for(JanusGraphIndex i : vertexIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        List<JanusGraphIndex> edgeIndecees = (List)management.getGraphIndexes(Edge.class);
        for(JanusGraphIndex i : edgeIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

//        RelationTypeIndex relationTypeIndex = management.getRelationIndex(management.getRelationType(DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier),DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier);
//        if(relationTypeIndex != null) {
//            LOGGER.info("Discovered relation index" + relationTypeIndex.name());
//            LOGGER.info("Index status: " + relationTypeIndex.getIndexStatus());
//            if(!relationTypeIndex.getIndexStatus().toString().equals("ENABLED")) {
//                LOGGER.error("Index " + relationTypeIndex.name() +" was not enabled!");
//                return false;
//            }
//        }

        LOGGER.info("All indecees were successfully generated!");
        return true;
    }

    public String getKeyspace() {
        return KEYSPACE;
    }

    public void updateEdges(String kmer, int pos, boolean recordStrand, int record){

        try {

            final String v1Identifier = "v1";
            final String v2Identifier = "v2";

            g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(0,kmer.length()-1)).fold().
                            coalesce(
                                    __.unfold(),
                                    __.addV(DbgGraph.DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                            property(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(0,kmer.length()-1))).
                            as(v1Identifier).
                            coalesce(
                                    __.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier, kmer.substring(1,kmer.length())),
                                    __.addV(DbgGraph.DBG_NODE_LABEL.PREFIX_NODE.identifier).
                                            property(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,kmer.substring(1,kmer.length()))).
                            as(v2Identifier).
                                    sideEffect(__.addE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                            property(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier,record).
                                            property(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier,recordStrand).
                                            property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,pos).
                            from(v1Identifier).to(v2Identifier)).
                            sideEffect(__.coalesce(
                                            __.inE(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).filter(__.otherV().where(P.eq(v1Identifier))),
                                            __.addE(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).from(v1Identifier).to(v2Identifier)).
                                            property(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,__.select(v2Identifier).inE(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).
                                                    filter(__.otherV().where(P.eq(v1Identifier))).
                                                    values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier).dedup().count())).
                            iterate();

        } catch (Exception e) {

            e.printStackTrace();
            LOGGER.error(e.getMessage());
            System.exit(-1);
        }

    }

    public void populate(File fastaFile, boolean topology) {

        KmersTable kmersTable = KmersTable.getInstance();
        for(boolean strand : new Boolean[] {true}) {
            Dataset<PositionCountIdentifierDT> sequencekmers =
                    SparkComputer.createDataFrame(SequenceParser.getMappedKmers(fastaFile,KmersTable.DEFAULT_K,
                    topology,strand, SequenceParser.AmbigFilter.NO_FILTER),
                    PositionCountIdentifierDT.ENCODER).cache();
            sequencekmers.show();
            // extract all occuring kmers of the sequence
            List<String> sequenceKmers = sequencekmers.
                    select(ColumnIdentifier.IDENTIFIER).
                    distinct().as(Encoders.STRING()).collectAsList();
            System.out.println(sequenceKmers.size());

            // get psql kmers on both strands
            Dataset<StrandKmerDT> recordKmers =
                    kmersTable.getKmerMatches(true,sequenceKmers).
                            withColumn(ColumnIdentifier.STRAND, functions.lit(true).cast(DataTypes.BooleanType)).
                            union( kmersTable.getKmerMatches(false,sequenceKmers).
                                    withColumn(ColumnIdentifier.STRAND, functions.lit(false).cast(DataTypes.BooleanType))).
                            as(StrandKmerDT.ENCODER).cache();
            recordKmers.show();
            long counter = recordKmers.count();
            System.out.println(counter);
//            System.out.println(recordKmers.collectAsList().size());
            Iterator<StrandKmerDT> recordKmerIterator =recordKmers.toLocalIterator();
            while (recordKmerIterator.hasNext()) {
                StrandKmerDT strandKmerDT = recordKmerIterator.next();
                System.out.println("Poscount: " + strandKmerDT.getPositions().length);
                for(int pos: strandKmerDT.getPositions()) {
                    updateEdges(strandKmerDT.getKmer(),pos,strandKmerDT.isStrand(),strandKmerDT.getRecordId());
                }
                System.out.println(counter);
                counter--;
                commit();

            }

        }

    }

    public void print() {
        System.out.println(g.V().count().next());
        System.out.println(g.V().valueMap().toList());
    }

    public void test() {
        String testKmer = "ACTGA";
//        Function<Traverser<Edge>,KmerDT> function = edgeTraverser -> {
//            Edge edge = edgeTraverser.get();
//            KmerDT kmer = new KmerDT(testKmer,(int)edge.property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier), )
//
//        }
        Object id = g.E().has(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier,3).outV().id().limit(1).next();
        Map<Object,Object> map = g.V(id).outE().hasLabel(DbgGraph.DBG_EDGE_LABEL.SUFFIX_EDGE.identifier).group().
                by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD.identifier)).
                by(__.values(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier).fold()).next();

        map.entrySet().stream().map(entry -> new KmerDT(
                testKmer,
                (int)entry.getKey(),
                ((List<Integer>)entry.getValue() ).toArray(new Integer[((List<Integer>) entry.getValue()).size()]))).forEach(kmerDT -> System.out.println(kmerDT));
//        System.out.println();
//                by(__.map((Traverser<Edge>) t -> t))
//                );
    }

}

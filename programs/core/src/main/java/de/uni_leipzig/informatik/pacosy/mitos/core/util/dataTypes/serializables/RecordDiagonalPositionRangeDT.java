package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.DiagonalInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.StrandInterface;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.col;

public class RecordDiagonalPositionRangeDT extends RangeDT implements RecordIdInterface, DiagonalInterface, StrandInterface {

    protected int recordId;
    protected int diagonalValue;
    protected int recordLength;
    protected boolean strand;
    public static final Encoder<RecordDiagonalPositionRangeDT> ENCODER = Encoders.bean(RecordDiagonalPositionRangeDT.class);
    public enum COLUMN_NAME implements DBColumnName {
        RECORD_LENGTH("\"recordLength\""),
        RECORDID("\"recordId\""),
        DIAGONAL_IDENTIFIER("\"diagonalValue\""),
        START("start"),
        END("end"),
        LENGTH("length");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public RecordDiagonalPositionRangeDT() {

    }


    public RecordDiagonalPositionRangeDT(int start, int end, int recordId, int diagonalValue, int recordLength) {
        super(start, end);
        this.recordId = recordId;
        this.recordLength = recordLength;
        this.diagonalValue = diagonalValue;
    }

    @Override
    public boolean isStrand() {
        return strand;
    }

    @Override
    public void setStrand(boolean strand) {
        this.strand = strand;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getDiagonalValue() {
        return diagonalValue;
    }

    public void setDiagonalValue(int diagonalValue) {
        this.diagonalValue = diagonalValue;
    }

    public int getRecordLength() {
        return recordLength;
    }

    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }

    public static Dataset<RecordDiagonalPositionRangeDT> convertToRecordDiagonalPositionRange(Dataset<?> dataset) {
        return dataset.select(
                ColumnIdentifier.RECORD_ID,
                ColumnIdentifier.DIAGONAL_IDENTIFIER,
                ColumnIdentifier.START,
                ColumnIdentifier.END,
                ColumnIdentifier.LENGTH,
                ColumnIdentifier.STRAND).
                as(ENCODER);
    }

    public static Dataset<Row> getRecordDiagonalPairs(Dataset<? extends RecordDiagonalPositionRangeDT> diagonals) {
        return diagonals.select(ColumnIdentifier.RECORD_ID,ColumnIdentifier.DIAGONAL_IDENTIFIER).distinct();
    }

    public static <T extends RecordDiagonalPositionRangeDT> Dataset<T> filterRecordDiagonalPairs(
            Dataset<T> dataset,
            Dataset<?> recordDiagonalPairs, Encoder<T> tEncoder) {
        recordDiagonalPairs = recordDiagonalPairs.
                withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").
                withColumnRenamed(ColumnIdentifier.DIAGONAL_IDENTIFIER,"d");
//        dataset.join(recordDiagonalPairs,
//                col(ColumnIdentifier.RECORD_ID).equalTo(col("r")).
//                        and(col(ColumnIdentifier.DIAGONAL_IDENTIFIER).equalTo(col("d")))).show();
        return SparkComputer.convert(dataset.join(recordDiagonalPairs,
                col(ColumnIdentifier.RECORD_ID).equalTo(col("r")).
                        and(col(ColumnIdentifier.DIAGONAL_IDENTIFIER).equalTo(col("d")))),
                dataset.columns(),tEncoder);
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.RecordAlignment;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LocalProperty;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.PositionCountDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RangeDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.spark.sql.Dataset;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.Scope;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Column;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Property;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.Cardinality;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.PropertyKey;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.graphdb.database.management.ManagementSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SequenceMapGraph extends Graph {

    private DbgGraph dbgGraph;
    private GraphTraversalSource taxonomySubGraph;
    private String taxIdIdentifier = "taxId";
    private String idIdentifier = "id";
    public static String KEYSPACE = "sequence_map";

    protected static final Logger LOGGER = LoggerFactory.getLogger(SequenceMapGraph.class);

    public enum FILTER {
        NON_MAPPING(0),
        MAPPING(1),
        SELECTED(2);

        public int identifier;

        FILTER(int identifier) {
            this.identifier = identifier;
        }

    }

    public enum SEQUENCE_NODE_ATTRIBUTES implements NODE_ATTRIBUTES {

        PLUS_STRAND_SEQUENCE_POSITIONS("plusSequencePositions"),
        MINUS_STRAND_SEQUENCE_POSITIONS("minusSequencePositions"),
        RECORD_COUNT("record_count"),
        FILTER_FLAG("nodeFilterFlag"),
        KMER("prefix");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        SEQUENCE_NODE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }

    }

    public enum SEQUENCE_EDGE_ATTRIBUTES implements EDGE_ATTRIBUTES {
        FILTER_FLAG("edgeFilterFlag"),

        RECORD("record"),
        POSITION("pos"),
        RRNA("rrna"),
        REPORIGIN("repOrigin"),
        PROTEIN("protein"),
        TRNA("trna"),
        OTHER("otherType");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        SEQUENCE_EDGE_ATTRIBUTES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum SEQUENCE_EDGE_LABEL{
        SUFFIX_EDGE("suffix_edge"),
        CONNECTION_EDGE("connection_edge"),
        TOPOLOGY_EDGE("topology_edge");

        public final String identifier;


        SEQUENCE_EDGE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum SEQUENCE_NODE_LABEL{
        DATA_NODE("data_node"),
        PREFIX_NODE("prefix_node");

        public final String identifier;


        SEQUENCE_NODE_LABEL(String identifier) {
            this.identifier = identifier;
        }
    }

    /*** Initialization ******************************************************************************************************************************/
    public SequenceMapGraph() {
        super();
        // TODO: some routine to get a free keyspaceExtension
    }

    protected void initialize() {
        super.initialize();
        this.dbgGraph = (DbgGraph) new Graph.Builder(DbgGraph.KEYSPACE,
                GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP)
                .build();
    }

    private void initTaxId(Integer taxId) {
        graph.variables().set(taxIdIdentifier,taxId);
        commit();
    }

    private void initId(String id) {
        graph.variables().set(idIdentifier,id);
        commit();
    }

    /*** Schema creation ******************************************************************************************************************************/

    protected void reset() {
        changeGraphConfiguration(GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP);
        entireDeletion();
        PropertiesConfiguration propertiesConfiguration = changeGraphConfiguration(GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP);
        createSchema();
        changeGraphConfiguration(propertiesConfiguration);
    }

    protected void createSchema() {


        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        // edge attributes
        management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.POSITION.identifier).dataType(Integer.class).make();
        final PropertyKey edgeFilter = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.FILTER_FLAG.identifier).dataType(Integer.class).make();
        final PropertyKey record = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.RECORD.identifier).dataType(Integer.class).make();

        final PropertyKey protein = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier).dataType(String.class).make();
        final PropertyKey trna = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier).dataType(String.class).make();
        final PropertyKey replicationOrigin = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier).dataType(String.class).make();
        final PropertyKey rrna = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier).dataType(String.class).make();
        final PropertyKey otherType = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier).dataType(String.class).make();


        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.TOPOLOGY_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.CONNECTION_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();

        String topology_index = "topology_index";

        management.buildIndex("edge_filter_index", Edge.class).addKey(edgeFilter).buildCompositeIndex();
        management.buildIndex("record_index", Edge.class).addKey(record).buildCompositeIndex();
        management.buildIndex("protein_index", Edge.class).addKey(protein).buildCompositeIndex();
        management.buildIndex("trna_index", Edge.class).addKey(trna).buildCompositeIndex();
        management.buildIndex("repOrigin_index", Edge.class).addKey(replicationOrigin).buildCompositeIndex();
        management.buildIndex("rrna_index", Edge.class).addKey(rrna).buildCompositeIndex();
        management.buildIndex("otherType_index", Edge.class).addKey(otherType).buildCompositeIndex();

        // vertex attributes
        final PropertyKey prefix = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).dataType(String.class).make();
        final PropertyKey plusStrandPos = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.PLUS_STRAND_SEQUENCE_POSITIONS.identifier).
                dataType(Integer.class).cardinality(Cardinality.SET).make();
        final PropertyKey minusStrandPos = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.MINUS_STRAND_SEQUENCE_POSITIONS.identifier).
                dataType(Integer.class).cardinality(Cardinality.SET).make();
        final PropertyKey recordCount = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.RECORD_COUNT.identifier).dataType(Integer.class).make();
        final PropertyKey nodeFilter = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.FILTER_FLAG.identifier).dataType(Integer.class).make();

        management.buildIndex("node_filter_index", Vertex.class).addKey(nodeFilter).buildCompositeIndex();
        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
//        management.buildIndex("prefix_index", Vertex.class).addKey(prefix, Mapping.STRING.asParameter()).buildMixedIndex("search");
//        management.buildIndex("plusStrand_index", Vertex.class).addKey(plusStrandPos).buildCompositeIndex();
//        management.buildIndex("minusStrand_index", Vertex.class).addKey(minusStrandPos).buildCompositeIndex();
        management.buildIndex("plusStrand_index", Vertex.class).addKey(plusStrandPos).buildMixedIndex("search");
        management.buildIndex("minusStrand_index", Vertex.class).addKey(minusStrandPos).buildMixedIndex("search");
        management.buildIndex(topology_index, Vertex.class).addKey(recordCount).buildMixedIndex("search");

        management.makeVertexLabel(SEQUENCE_NODE_LABEL.PREFIX_NODE.identifier).make();
        management.makeVertexLabel(SEQUENCE_NODE_LABEL.DATA_NODE.identifier).make();


        management.commit();

        try {

            ManagementSystem.awaitGraphIndexStatus(graph, "node_filter_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "plusStrand_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "minusStrand_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph,topology_index).call();

            ManagementSystem.awaitGraphIndexStatus(graph,"edge_filter_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "record_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "protein_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "trna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "repOrigin_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "rrna_index").call();
            ManagementSystem.awaitGraphIndexStatus(graph, "otherType_index").call();


        } catch (InterruptedException  i) {
            i.printStackTrace();
        }

        if(!schemaGenerationSuccessful()) {
            System.exit(-1);
        }

//        reopenGraph();

    }

//    public void createSchemaProperties() {
//
//
//        graph.tx().rollback();
//        JanusGraphManagement management = graph.openManagement();
//
//        // edge attributes
//        management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.PLUS_STRAND_POS.identifier).dataType(int[].class).make();
//        management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.MINUS_STRAND_POS.identifier).dataType(int[].class).make();
//
//        final PropertyKey record = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.RECORD.identifier).dataType(Integer.class).make();
//        final PropertyKey recordCount = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.RECORD_COUNT.identifier).dataType(Integer.class).make();
//
//
//        final PropertyKey protein = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier).dataType(String.class).make();
//        final PropertyKey trna = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier).dataType(String.class).make();
//        final PropertyKey replicationOrigin = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier).dataType(String.class).make();
//        final PropertyKey rrna = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier).dataType(String.class).make();
//        final PropertyKey otherType = management.makePropertyKey(SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier).dataType(String.class).make();
//
//
//        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.TOPOLOGY_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
//        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.SUFFIX_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
//        management.makeEdgeLabel(SEQUENCE_EDGE_LABEL.SEQUENCE_EDGE.identifier).multiplicity(Multiplicity.MULTI).make();
//
//
//
//        // vertex attributes
//        final PropertyKey prefix = management.makePropertyKey(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).dataType(String.class).make();
//        management.buildIndex("prefix_index", Vertex.class).addKey(prefix).buildCompositeIndex();
//
//        management.makeVertexLabel(SEQUENCE_NODE_LABEL.PREFIX_NODE.identifier).make();
//        management.makeVertexLabel(SEQUENCE_NODE_LABEL.UNMATCHED_NODE.identifier).make();
//
//
//        management.commit();
//
//        try {
//
//
//            ManagementSystem.awaitGraphIndexStatus(graph, "prefix_index").call();
//
//
//
//        } catch (InterruptedException  i) {
//            i.printStackTrace();
//        }
//
//        if(!schemaGenerationSuccessful()) {
//            System.exit(-1);
//        }
//
//        reopenGraph();
//
//    }

    protected boolean schemaGenerationSuccessful() {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();

        List<JanusGraphIndex> vertexIndecees = (List)management.getGraphIndexes(Vertex.class);
        for(JanusGraphIndex i : vertexIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        List<JanusGraphIndex> edgeIndecees = (List)management.getGraphIndexes(Edge.class);
        for(JanusGraphIndex i : edgeIndecees) {
            LOGGER.info("Discovered index " + i.name());
            for(PropertyKey p : i.getFieldKeys()) {
                LOGGER.info("Index status: " + i.getIndexStatus(p));
                if(!i.getIndexStatus(p).toString().equals("ENABLED")) {
                    LOGGER.error("Index " + i.name() +" was not enabled!");
                    return false;
                }

            }
        }

        LOGGER.info("All indecees were successfully generated!");
        return true;
    }

    protected static void createKeyspaces(int amount) {
        SequenceMapGraph sequenceMapGraph = (SequenceMapGraph) new Graph.Builder(
                KEYSPACE + "_0",
                GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                startOption(LaunchExternal.LAUNCH_OPTION.START).build();
        sequenceMapGraph.createSchema();
        LOGGER.info("Done with schemacreation 0");

        for(int i = 1; i < amount-1; i++) {
            sequenceMapGraph = (SequenceMapGraph) new Graph.Builder(
                    KEYSPACE + "_" + i,
                    GraphConfiguration.CONNECTION_TYPE.BULK_LOAD_OLTP).
                    startOption(LaunchExternal.LAUNCH_OPTION.START).build();
            sequenceMapGraph.createSchema();
            LOGGER.info("Done with schemacreation " + i);
        }

    }

    /*** Persistence ******************************************************************************************************************************/


    /**
     *
     * @param id Sequence id
     * @param kmerMap A map, where kmers are keys and a list of strand-position maps.
     * Each of the strand-position maps contains an entry with key "pos" and value of the associated position
     * and an entry with key "strand" and value true if on positive strand and false otherwise.
     */
    protected void createSequenceMatchGraph(String id,Map<String,List<Map<String,Object>>> kmerMap) {

        // change connection properties
        List<LocalProperty> localProperties = new ArrayList<>();
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.DATABASE_LEVEL_CACHE_PERCENTAGE,0.3));
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES,1000));
        PropertiesConfiguration dbgPropertiesConfiguration = dbgGraph.changeGraphConfiguration(localProperties);
        localProperties.clear();
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.DATABASE_LEVEL_CACHE_PERCENTAGE,0.5));
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES,10000));
        PropertiesConfiguration sequenceMatchGraphConfiguration = changeGraphConfiguration(localProperties);

        initId(id);
        boolean mapped;
        int counter = 0;
        // if there was no match in the dbg graph -> store associated position
        List<Integer> plusStrandMismatchPositions = new ArrayList<>();
        List<Integer> minusStrandMismatchPositions = new ArrayList<>();


        for(Map.Entry<String,List<Map<String,Object>>> mapEntry: kmerMap.entrySet()) {


            Vertex v1 = null;
            Vertex v2 = null;
            String prefix = mapEntry.getKey().substring(0,mapEntry.getKey().length()-1);
            String suffix = mapEntry.getKey().substring(1);
            LOGGER.info("Investigating : " + prefix +"->"+ suffix);


            // Persist vertices if they match
            List<Vertex> startVertex = dbgGraph.g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,
                    prefix).toList();
            if(startVertex.size() != 0) {
                v1 = addKmerVertex(prefix);
            }
            List<Vertex> endVertex = dbgGraph.g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,
                    suffix).toList();
            if(endVertex.size() != 0) {
                v2 = addKmerVertex(suffix);
            }


            List<Edge> edgeList = new ArrayList<>();
            if(startVertex.size() != 0 && endVertex.size() != 0) { // if there could be a connecting edge
                edgeList = dbgGraph.g.V(startVertex.get(0)).
                        outE().
                        filter(__.otherV().is(endVertex.get(0))).toList();
            }
            dbgGraph.commit();

            // if there is an edge between both kmers
            if(edgeList.size() > 0) {
                LOGGER.info("Persisting edges");
                // add a data vertex storing all positions, add "connection" edges v1 -> dataVertex -> v2
                Vertex dataVertex = addDataVertex(mapEntry.getValue());
                g.addE(SEQUENCE_EDGE_LABEL.CONNECTION_EDGE.identifier).from(dataVertex).to(v2).
                        addE(SEQUENCE_EDGE_LABEL.CONNECTION_EDGE.identifier).from(v1).to(dataVertex).
                        next();

                GraphTraversal<?,?> graphTraversal = g.inject(0);
                for(int i = 0; i< edgeList.size(); i++) {
                    Edge edge = edgeList.get(i);
                    String edgeLabel = edge.label();
                    graphTraversal = graphTraversal.addE(edgeLabel).
                            from(v1);


                    if(edgeLabel.equals(SEQUENCE_EDGE_LABEL.TOPOLOGY_EDGE.identifier)) {
                        // add a topology edge between v1 and v2
                        graphTraversal = graphTraversal.to(v2).property(SEQUENCE_EDGE_ATTRIBUTES.FILTER_FLAG.identifier,FILTER.MAPPING.identifier);
                        // store the associated record count in the dataVertex
                        String attributeIdentifier = SEQUENCE_NODE_ATTRIBUTES.RECORD_COUNT.identifier;
                        g.V(dataVertex).
                                property(attributeIdentifier,edge.property(attributeIdentifier).value()).iterate();
                    }
                    else {
                        // store all prefix edges as incoming edges to the dataVertex
                        Iterator<Property<Object>> propertyIterator = edge.properties(
                                new String[]{
                                        SEQUENCE_EDGE_ATTRIBUTES.POSITION.identifier,
                                        SEQUENCE_EDGE_ATTRIBUTES.RECORD.identifier,
                                        SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier,
                                        SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier,
                                        SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier});
                        graphTraversal = graphTraversal.to(dataVertex);
                        while (propertyIterator.hasNext()) {
                            Property<Object> property = propertyIterator.next();
                            graphTraversal = graphTraversal.property(property.key(),property.value());
                        }

                        Property<Object> property = edge.property(SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier);
                        if(property.isPresent()){
                            if((boolean)property.value()) {
                                graphTraversal = graphTraversal.property(SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier,"rrnS");
                            }
                            else {
                                graphTraversal = graphTraversal.property(SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier,"rrnL");
                            }
                        }
                        property = edge.property(DbgGraph.DBG_EDGE_ATTRIBUTES.REPORIGIN.identifier);
                        if(property.isPresent()){
                            if((boolean)property.value()) {
                                graphTraversal = graphTraversal.property(SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier,"OL");
                            }
                            else {
                                graphTraversal = graphTraversal.property(SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier,"OH");
                            }
                        }
                    }

                    if(((i > 0) && (i% 100 == 0)) || i == edgeList.size()-1) {

                        graphTraversal.iterate();
                        commit();
                        graphTraversal = g.inject(0);

                    }


                }
            }
            else {
                // store positions that did not match
                updateMismatchPositions(mapEntry.getValue(),plusStrandMismatchPositions,minusStrandMismatchPositions);

            }

            commit();
        }

        dbgGraph.changeGraphConfiguration(dbgPropertiesConfiguration);
        changeGraphConfiguration(sequenceMatchGraphConfiguration);
    }

    private Vertex addKmerVertex(String kMer) {

        return g.V().has(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier,kMer).
                fold().
                coalesce(__.unfold(),__.addV(SEQUENCE_NODE_LABEL.PREFIX_NODE.identifier).
                        property(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier,
                                kMer)).
                property(SEQUENCE_NODE_ATTRIBUTES.FILTER_FLAG.identifier,FILTER.MAPPING.identifier)
                .next();

    }

    private Vertex addKmerVertex(String kMer, boolean mapped) {

        Integer filterValue = mapped ? FILTER.MAPPING.identifier : FILTER.NON_MAPPING.identifier;

        return g.V().has(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier,kMer).
                fold().
                coalesce(__.unfold(),__.addV(SEQUENCE_NODE_LABEL.PREFIX_NODE.identifier).
                        property(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier,
                                kMer)).
                property(SEQUENCE_NODE_ATTRIBUTES.FILTER_FLAG.identifier,filterValue)
                .next();

    }

    private Vertex addDataVertex(List<Map<String,Object>> strandPositions) {
        GraphTraversal<Vertex, Vertex> vertexVertexGraphTraversal = g.addV(SEQUENCE_NODE_LABEL.DATA_NODE.identifier);

        for(Map<String,Object> entry: strandPositions) {
            if((boolean)entry.get(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier)) {
                vertexVertexGraphTraversal = vertexVertexGraphTraversal.
                        property(
                                SEQUENCE_NODE_ATTRIBUTES.PLUS_STRAND_SEQUENCE_POSITIONS.identifier,
                                (Integer) entry.get(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier));
            }
            else {
                vertexVertexGraphTraversal = vertexVertexGraphTraversal.
                        property(
                                SEQUENCE_NODE_ATTRIBUTES.MINUS_STRAND_SEQUENCE_POSITIONS.identifier,
                                (Integer) entry.get(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier));
            }

        }
        return vertexVertexGraphTraversal.next();
    }


    private void updateMismatchPositions(List<Map<String,Object>> strandPositions,
                                         List<Integer> plusStrandMismatchPositions,
                                         List<Integer> minusStrandMismatchPositions) {

        for(Map<String,Object> map: strandPositions) {
            if((boolean)map.get(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier)) {
                plusStrandMismatchPositions.add((Integer)map.get(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier));
            }
            else {
                minusStrandMismatchPositions.add((Integer)map.get(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier));
            }
        }
    }

    private void addMisMatchEdges( List<Integer> mismatchPositions) {
        List<RangeDT> ranges = RangeDT.createRangesForPositions(mismatchPositions);

    }

    protected void createTaxonomy(Integer taxId) {
        initTaxId(taxId);
    }



    /*** Traversals ******************************************************************************************************************************/

    private GraphTraversal getStrandPositionTraversal(String nodeIdentifier, String positionIdentifier, boolean strand) {
        String strandIdentifier= getStrandIdentifier(strand);

//        System.out.println("in here " + nodeIdentifier + " " + ColumnIdentifier.STRAND + " "+ positionIdentifier);
        return g.V().has(strandIdentifier).as(nodeIdentifier).
                values(strandIdentifier).as(positionIdentifier);
    }

    private GraphTraversal getPositionGroupTraversal(GraphTraversal graphTraversal, String nodeIdentifier,
                                                     String idIdentifier, String attributeIdentifier, SEQUENCE_NODE_ATTRIBUTES attribute ) {
        return graphTraversal.group().
                by().
                by(__.select(nodeIdentifier).
                        project(idIdentifier,attributeIdentifier).
                        by(__.id()).
                        by(__.values(attribute.identifier)));
    }

    private GraphTraversal getPositionRecordCountGroupTraversal(GraphTraversal graphTraversal, String nodeIdentifier,
                                                                String idIdentifier, String countIdentifier) {
        return getPositionGroupTraversal(graphTraversal,nodeIdentifier,idIdentifier,countIdentifier,SEQUENCE_NODE_ATTRIBUTES.RECORD_COUNT);
    }

////    private GraphTraversal getPositionPropertyGroupTraversal(GraphTraversal graphTraversal, String edgeIdentifier, String idIdentifier, String countIdentifier) {
////        return graphTraversal.group().
////                by().
////                by(__.select(edgeIdentifier).
////                        project(idIdentifier,idIdentifier).
////                        by(__.id()).
////                        by(__.coalesce(__.values(attribute.identifier),__.constant(0))));
////    }
//
    public Map<Object,Object> getPropertyMap() {
        return g.E().properties(
                SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier,
                SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier,
                SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier,
                SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier
        ).
                group().by(__.key()).by(__.value().dedup().fold()).next();
    }

//    private GraphTraversal getDataNodesForRange(Range range, boolean strand) {
//        return __.V().has(getColumnIdentifier.STRAND(strand),P.between(range.getStart(),range.getEnd())).
//                asAdmin().clone();
//    }

    private GraphTraversal getDataNodesForRangeTraversal(RangeDT range, boolean strand) {
        return g.V().has(getStrandIdentifier(strand),P.between(range.getStart(),range.getEnd()));
    }

    private GraphTraversal getRangeGroupTraversal(RangeDT range, boolean strand, String dataNodeIdentifier) {
        return getDataNodesForRangeTraversal(range,strand).as(dataNodeIdentifier).values(getStrandIdentifier(strand)).
                where(__.is(P.between(range.getStart(),range.getEnd()))).
                group().by();
    }

    private GraphTraversal<?,Map<Integer,List<Integer>>> getRecordPositionsForRange(RangeDT range, boolean strand, int recordId, String dataNodeIdentifier){
        return getRangeGroupTraversal(range,strand,dataNodeIdentifier).
                by(__.select(dataNodeIdentifier).inE().has(SEQUENCE_EDGE_ATTRIBUTES.RECORD.identifier,recordId).
                values(SEQUENCE_EDGE_ATTRIBUTES.POSITION.identifier).fold());
    }

    public Dataset<PositionCountDT> getRecordCountPositionDistribution(Map<String,Integer []> kmerMap) {
        final List<PositionCountDT> positionDTS = new ArrayList<>();
        for(Map.Entry<String,Integer[]> kPlusOneMer: kmerMap.entrySet()) {
            String prefix = kPlusOneMer.getKey().substring(0,kPlusOneMer.getKey().length()-1);
            String suffix = kPlusOneMer.getKey().substring(1);
            final int recordCount =
                    dbgGraph.g.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,prefix).
                    coalesce(__.
                    outE().hasLabel(DbgGraph.DBG_EDGE_LABEL.TOPOLOGY_EDGE.identifier).
                    filter(__.otherV().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,suffix)).
                    values(DbgGraph.DBG_EDGE_ATTRIBUTES.RECORD_COUNT.identifier),
                            __.constant(0)).next();
            for(int position: kPlusOneMer.getValue()) {
                positionDTS.add(new PositionCountDT(position,recordCount));
            }
        }
        return SparkComputer.createDataFrame(positionDTS, PositionCountDT.class);
    }

    /*** Analysis ******************************************************************************************************************************/

    protected Map<Object,Object> getStrandRecordCountPositionMap(boolean strand, String nodeIdentifier,
                                                                 String positionIdentifier, String idIdentifier, String countIdentifier) {

        return (Map<Object,Object>) getPositionRecordCountGroupTraversal(
                getStrandPositionTraversal(nodeIdentifier,positionIdentifier,strand),
                nodeIdentifier,idIdentifier,countIdentifier).
                next();
    }

    // TODO : think about how to do this now!
    protected Map<Object,Object> getStrandPropertyCountPositionMap(boolean strand) {

        final String strandIdentifier = getStrandIdentifier(strand);
        final String nodeIdentifier = "v";
        return g.V().has(strandIdentifier).as(nodeIdentifier).
                values(strandIdentifier).
                group().
                by().
                by(__.select(nodeIdentifier).inE(). //hasLabel(SEQUENCE_EDGE_LABEL.SUFFIX_EDGE.identifier).
                        properties(SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier,
                                SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier,
                                SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier,
                                SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier,
                                SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier).
                        group().
                        by(__.key()).
                        by(__.value().groupCount())
                ).next();

    }

    private String getKmer(String strandIdentifier, int position) {
        return (String) g.V().has(strandIdentifier,position).in().values(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).next();
    }

    private String getOutEdgeLabel(String strandIdentifier, int position) {
        return g.V().has(strandIdentifier,position).out().values(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).
                map(kmer -> ((String)kmer.get()).substring(15)).next();
    }

    private String getEdgeLabel(String strandIdentifier, int position) {
        Object id = g.V().has(strandIdentifier,position).id().next();
        return g.V(id).in().values(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).
                map(kmer -> ((String)kmer.get())).next() +
                g.V(id).out().values(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier).
                        map(kmer -> ((String)kmer.get()).substring(15)).next();
    }

    public void getKmersForRange(RangeDT range, boolean strand) {
        Map<Integer,String> kmers = (Map<Integer,String>) getDataNodesForRangeTraversal(range,strand).group().
                by(__.values(getStrandIdentifier(strand)).where(__.is(P.between(range.getStart(),range.getEnd())))).
                by(__.in().values(SEQUENCE_NODE_ATTRIBUTES.KMER.identifier)).order(Scope.local).by(Column.keys).next();
        Auxiliary.printMap(kmers);
    }

//    public String getSubSequence(Range range, boolean strand) {
//        int position = range.getStart();
//        String strandIdentifier = getStrandIdentifier(strand);
//        StringBuilder subSequence = new StringBuilder(getInEdgeLabel(strandIdentifier,position));
//        position++;
//        while (position <= range.getEnd()) {
//            subSequence.append(getOutEdgeLabel(strandIdentifier,position));
//            position++;
//        }
//    }

    public void test() {
        // get record groupCount for range -> only count a record once per datanode
        g.V().has("plusSequencePositions",P.inside(457,546)).local(__.inE().values("record").dedup().fold()).unfold().groupCount().
                order(Scope.local).by(Column.values);

        // filter records for minimum occurance
        g.V().has("plusSequencePositions",P.inside(457,546)).local(__.inE().values("record").dedup().fold()).unfold().groupCount().
                unfold().filter(__.select(Column.values).is(P.gt(10))).order().by(Column.values).fold();

        // get positions for specific record within the range
        g.V().has("plusSequencePositions",P.inside(457,546)).as("v").values("plusSequencePositions").
                where(__.is(P.inside(457,546))).group().by().by(__.select("v").inE().has("record",5).
                values(SEQUENCE_EDGE_ATTRIBUTES.POSITION.identifier).fold());

        g.V().has("plusSequencePositions",P.inside(457,546)).as("v").values("plusSequencePositions").
                where(__.is(P.inside(457,546))).group().by().by(__.select("v").inE().values("record").dedup().fold());

    }



    public void analyzeRecordsForRange(RangeDT range, boolean strand) {
        List<?> dataNodes = getDataNodesForRangeTraversal(range,strand).id().toList();

        GraphTraversal<Object, Vertex> has = __.V().has(getStrandIdentifier(strand), P.between(range.getStart(), range.getEnd()));
    }

    // todo check if provided range is a cyclic one -> if so shift in resulst
    public void analyzeRecordForRange(RangeDT range, boolean strand, int recordId) {
        RecordDT r = dbgGraph.getRecordAttributes(recordId);
        String dataNodeIdentifier = "dataNode";


        Map<Integer,List<Integer>> recordPositionMap =
                getRecordPositionsForRange(range,strand,recordId,dataNodeIdentifier).next();

        RecordAlignment recordAlignment = new RecordAlignment(recordId,
                r.getLength(),
                r.isTopology(),
                recordPositionMap);

        recordAlignment.getRangeDistribution();
    }

    public void analyzeRecordForRange(List<?> dataNodes, int recordId) {


//        g.V(dataNodes).inE().has(SEQUENCE_EDGE_ATTRIBUTES.RECORD.identifier,recordId)

    }

//    protected Map<Object,Object> getStrandPropertyPositionMap(boolean strand) {
//
//        final String edgeIdentifier = "e", nodeIdentifier = "v";
//        final String strandIdentifier = getStrandIdentifier(strand);
//        return g.E().has(strandIdentifier).as(edgeIdentifier).
//                group().
//                by(__.values(strandIdentifier)).
//                by(__.outV().outE().hasLabel(SEQUENCE_EDGE_LABEL.SUFFIX_EDGE.identifier).
//                        filter(__.inV().as(nodeIdentifier).select(edgeIdentifier).inV().where(P.eq(nodeIdentifier))).
//                        properties(SEQUENCE_EDGE_ATTRIBUTES.PROTEIN.identifier,
//                                SEQUENCE_EDGE_ATTRIBUTES.TRNA.identifier,
//                                SEQUENCE_EDGE_ATTRIBUTES.RRNA.identifier,
//                                SEQUENCE_EDGE_ATTRIBUTES.REPORIGIN.identifier,
//                                SEQUENCE_EDGE_ATTRIBUTES.OTHER.identifier).
//                        group().
//                        by(__.key()).
//                        by(__.value().dedup().fold())
//                ).next();
//
//    }
//
////    protected Map<Object,Object> getStrandPropertyPositionMap(boolean strand, String edgeIdentifier, String positionIdentifier, String idIdentifier, String propertyIdentifier, String property) {
////
////        return (Map<Object,Object>) getPositionGroupTraversal(
////                getStrandPositionTraversal(edgeIdentifier,positionIdentifier,strand),edgeIdentifier,idIdentifier,propertyIdentifier,property).
////                next();
////    }
//
//    protected void getMappingBranches() {
////g.E().has("plusPos").as("e").values("plusPos").unfold().as("p").group().by().by(select("e").project("id","count").by(__.id()).by(__.coalesce(__.values("record_count"),__.constant(0)))).order(local).by(Column.keys).unfold().filter(__.select(Column.values).is(gt(500))).fold()
//    }
//
    /*** Auxiliary ******************************************************************************************************************************/


    private String getStrandIdentifier(boolean strand) {
        return  (strand ?
                SEQUENCE_NODE_ATTRIBUTES.PLUS_STRAND_SEQUENCE_POSITIONS.identifier :
                SEQUENCE_NODE_ATTRIBUTES.MINUS_STRAND_SEQUENCE_POSITIONS.identifier);
    }

    /*** Getter and Setter ******************************************************************************************************************************/
    public DbgGraph getDbgGraph() {
        return dbgGraph;
    }

    public void setDbgGraph(DbgGraph dbgGraph) {
        this.dbgGraph = dbgGraph;
    }

    public String getKeyspace() {
        return KEYSPACE;
    }






}

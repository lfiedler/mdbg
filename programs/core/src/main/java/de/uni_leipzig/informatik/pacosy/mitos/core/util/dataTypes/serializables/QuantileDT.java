package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class QuantileDT implements Serializable {

    protected String attribute;
    protected double quantile25;
    protected double quantile50;
    protected double quantile75;

    protected double min;
    protected double max;
    protected double lowerWhisker;
    protected double upperWhisker;

    public static final Encoder<QuantileDT> ENCODER = Encoders.bean(QuantileDT.class);

    public QuantileDT() {
    }

    public QuantileDT(String attribute, double[] quantiles, double min, double max, double lowerWhisker, double upperWhisker) {
        this.attribute = attribute;
        quantile25 = quantiles[0];
        quantile50 = quantiles[1];
        quantile75 = quantiles[2];

        this.lowerWhisker = lowerWhisker;
        this.upperWhisker = upperWhisker;

        this.min = min;
        this.max = max;
    }

    @Override
    public String toString() {
        return "Quantiles: [" + quantile25 + "," + quantile50 + "," + quantile75 + "]" + " Whiskers: [" + lowerWhisker + "," + upperWhisker + "] MinMax: [" + min + "," + max + "}";
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public double getQuantile25() {
        return quantile25;
    }

    public void setQuantile25(double quantile25) {
        this.quantile25 = quantile25;
    }

    public double getQuantile50() {
        return quantile50;
    }

    public void setQuantile50(double quantile50) {
        this.quantile50 = quantile50;
    }

    public double getQuantile75() {
        return quantile75;
    }

    public void setQuantile75(double quantile75) {
        this.quantile75 = quantile75;
    }


    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getLowerWhisker() {
        return lowerWhisker;
    }

    public void setLowerWhisker(double lowerWhisker) {
        this.lowerWhisker = lowerWhisker;
    }

    public double getUpperWhisker() {
        return upperWhisker;
    }

    public void setUpperWhisker(double upperWhisker) {
        this.upperWhisker = upperWhisker;
    }
}

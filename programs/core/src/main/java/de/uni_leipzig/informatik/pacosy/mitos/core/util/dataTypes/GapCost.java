package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes;

public class GapCost {

    private final int openGapCost;
    private final int extendGapCost;

    public GapCost(GapCost gapCost) {
        this.openGapCost = gapCost.openGapCost;
        this.extendGapCost = gapCost.extendGapCost;
    }

    public GapCost(int openGapCost, int extendGapCost) {
        this.openGapCost = openGapCost;
        this.extendGapCost = extendGapCost;
    }

    public int getOpenGapCost() {
        return openGapCost;
    }

    public int getExtendGapCost() {
        return extendGapCost;
    }

    public String toString() {
        return openGapCost+"/"+extendGapCost;
    }
}

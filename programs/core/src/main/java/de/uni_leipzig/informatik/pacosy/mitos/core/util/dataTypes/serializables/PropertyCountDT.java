package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class PropertyCountDT implements Serializable {
    private String name;
    private long count;
    public static final Encoder<PropertyCountDT> ENCODER = Encoders.bean(PropertyCountDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        NAME("name"),
        COUNT("count");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public PropertyCountDT(String name, long count) {
        this.name = name;
        this.count = count;
    }

    public PropertyCountDT() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.parser;

import java.io.*;
import java.util.*;

public class TaxonomyParser {

    private File nodesFile;
    private File namesFile;
    private File divisionFile;

    private File mergeFile;
    Map<Integer, Map<String,Object>> nodes;
    Map<Integer,Integer> edges;
    Map<Integer,Map<String,String>> names;


    List<String[]> mergeTaxids;
    List<String[]> divisions;
    Set<String> ranks;

    public TaxonomyParser(File nodesFile, File namesFile, File mergeFile, File divisonFile) {
        this.namesFile = namesFile;
        this.nodesFile = nodesFile;
        this.mergeFile = mergeFile;
        this.divisionFile = divisonFile;
        this.nodes = new HashMap<>();
        this.edges = new HashMap<>();
        this.names = new HashMap<>();
        this.ranks = new HashSet<>();
        this.mergeTaxids = new ArrayList<>();
        this.divisions = new ArrayList<>();
    }

    public void parseNodes() {
        try {

            BufferedReader br = new BufferedReader(new FileReader(nodesFile));
            String line;
            while((line = br.readLine()) != null) {
                Map<String,Object> nodeData = new HashMap<>();
                String[] split = line.split("\t\\|\t");
                Integer taxId = Integer.parseInt(split[0]);
                nodeData.put("rank",split[2]);
                nodeData.put("divId",Integer.parseInt(split[4]));
                nodeData.put("genId",Integer.parseInt(split[10]));
                ranks.add("'" + split[2] + "'");
                nodes.put(taxId,nodeData);
                edges.put(taxId,Integer.parseInt(split[1]));

            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void parseNames() {
        try {


            BufferedReader br = new BufferedReader(new FileReader(namesFile));
            String line;

            while((line = br.readLine()) != null) {

                String[] split = line.split("\t\\|\t");
                Map<String,String> nameData = names.get(Integer.parseInt(split[0]));
                if(nameData == null) {
                    nameData = new HashMap<>();
                }
                nameData.put(split[3].replace("\t|","").
                        replace(" ","_").
                        replace("-","_"),split[1]);
                names.put(Integer.parseInt(split[0]),nameData);
            }
            br.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseMergedTaxids() {

        try {
            BufferedReader br = new BufferedReader(new FileReader(mergeFile));
            String line;

            while((line = br.readLine()) != null) {
                String[] split = line.split("\t\\|\t");
                split[1] = split[1].replace("\t|","");
                mergeTaxids.add(split);
            }
            br.close();



        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void parseDivisions() {
        try {


            BufferedReader br = new BufferedReader(new FileReader(divisionFile));
            String line;

            while((line = br.readLine()) != null) {
                String[] split = line.split("\t\\|\t");
                String[] division = new String[2];
                division[0] = split[0];
                division[1] = "'" + split[2] + "'";
                divisions.add(division);

            }
            br.close();



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // getter and setter

    public List<String[]> getMergeTaxids() {
        return mergeTaxids;
    }

    public Map<Integer, Map<String, Object>> getNodes() {
        return nodes;
    }

    public Map<Integer, Integer> getEdges() {
        return edges;
    }

    public Map<Integer, Map<String, String>> getNames() {
        return names;
    }


    public List<String[]> getDivisions() {
        return divisions;
    }

    public Set<String> getRanks() {
        return ranks;
    }


}


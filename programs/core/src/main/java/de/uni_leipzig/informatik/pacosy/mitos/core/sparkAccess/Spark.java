package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import java.io.Serializable;
import java.util.List;

public class Spark {

    private static Spark spark;
    private static SparkSession sparkSession;
    private static final String SPARK_APP_NAME = "MITOS_DBG";
    private static final String MASTER = "local[*]";
    public static final String PATH_OPTION_KEY = "path";
    public static JavaSparkContext javaSparkContext;

    private Spark() {

    }

    public static SparkSession getInstance() {
        if(sparkSession == null) {
//            spark = new Spark();
            setSparkSession(MASTER);

            javaSparkContext = new JavaSparkContext(sparkSession.sparkContext());
        }

        return sparkSession;
    }

    public static void reset() {
        sparkSession.close();
        setSparkSession(MASTER);
        javaSparkContext = new JavaSparkContext(sparkSession.sparkContext());
    }


    public static JavaSparkContext getJavaSparkContext() {
        return javaSparkContext;
    }

    public static void setSparkSession(String master) {
        if(sparkSession != null) {
            sparkSession.close();
        }
        sparkSession =  SparkSession.builder().
                config("spark.driver.maxResultSize", "5g").
                config("spark.scheduler.listenerbus.eventqueue.size", "1000000").
                config("spark.rpc.askTimeout", "10000s").
                config("spark.driver.memory","1G").
                config("spark.executor.memory","2G").
                config("spark.sql.broadcastTimeout",8000).
//                config("spark.cleaner.periodicGC.interval","2min").
                config("spark.local.dir","/tmp").
                        master(master).
                appName(SPARK_APP_NAME).getOrCreate();
//        sparkSession.conf().set("spark.driver.maxResultSize", "2g");
//        sparkSession.conf().set("spark.scheduler.listenerbus.eventqueue.size", "1000000");
//        sparkSession.conf().set("spark.rpc.askTimeout", "10000s");
//        sparkSession.conf().set("spark.sql.autoBroadcastJoinThreshold","-1");
//        sparkSession.conf().set("spark.driver.memory","1G");
//        sparkSession.conf().set("spark.executor.memory","2G");
//        sparkSession.conf().set("spark.sql.broadcastTimeout",8000);
//        sparkSession.conf().set("spark.cleaner.periodicGC.interval","2min");

//        System.out.println(sparkSession.conf().get("spark.sql.broadcastTimeout"));
    }






}

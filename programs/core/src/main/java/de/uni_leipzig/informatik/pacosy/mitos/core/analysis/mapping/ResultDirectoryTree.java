package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.tools.SequenceAnalyzer;
import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.Encoders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

class ResultDirectoryTree {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ResultDirectoryTree.class);
    static final String GENOME_IDENTIFIER = "genome";
    final File resultDir;
    final File parquetDir;
    final File propertyCluster;
    final File plusPropertyCluster;
    final File minusPropertyCluster;
    final File plusPropertyClusterGraph;
    final File minusPropertyClusterGraph;
    File plusAnnotation;
    File minusAnnotation;
    File agreementFile;
    File plusAnnotaedPlusRefseqPropertyAgreement;
    File plusAnnotaedMinusRefseqPropertyAgreement;
    File minusAnnotaedPlusRefseqPropertyAgreement;
    File minusAnnotaedMinusRefseqPropertyAgreement;
    File plusAnnotaedPlusRefseqAlternativePropertyAgreement;
    File plusAnnotaedMinusRefseqAlternativePropertyAgreement;
    File minusAnnotaedPlusRefseqAlternativePropertyAgreement;
    File minusAnnotaedMinusRefseqAlternativePropertyAgreement;
    final File statisticDir;
    final File gapStatistic;
    public enum PARQUET_NAME{
        BRIDGED_RANGES,
        MAPPED_RANGES,
        SEQUENCE_DATA,
        GENOME,
        RECORD_KMERS, //
        SEQUENCE_KMERS,
        RANGE;
    }

    protected final String plusStrandSubGraphName;
    protected final String minusStrandSubGraphName;

    ResultDirectoryTree(String id, boolean clearDirectoryIfExists) {
        if(clearDirectoryIfExists) {
            File tempFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()  + "/" + id);
            if(tempFile.exists()) {
                LOGGER.warn("Result directory " + tempFile.getAbsolutePath() + " already exists. Will be overriden");
                try {
                    FileUtils.deleteDirectory(tempFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        this.resultDir = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()  + "/" + id);
        resultDir.mkdirs();
        this.propertyCluster = new File(resultDir.getAbsolutePath()+"/propertyCluster");
        propertyCluster.mkdirs();
        this.plusPropertyCluster = new File(propertyCluster.getAbsolutePath() +"/plusCluster.json");
        this.minusPropertyCluster = new File(propertyCluster.getAbsolutePath() +"/minusCluster.json");
        this.parquetDir = new File(resultDir.getAbsolutePath() + "/parquets" );
        parquetDir.mkdirs();
        this.plusPropertyClusterGraph = new File(propertyCluster.getAbsolutePath() +"/plusClusterTree.gryo");
        this.minusPropertyClusterGraph = new File(propertyCluster.getAbsolutePath() +"/minusClusterTree.gryo");

        if(clearDirectoryIfExists) {
            LOGGER.info("Created new result directory: " + resultDir.getAbsolutePath());
        }
        String graphPath = resultDir.getAbsolutePath() + "/graphs";
        new File(graphPath).mkdirs();
        plusStrandSubGraphName = graphPath + "/plusStrandSubGraph.xml";
        minusStrandSubGraphName = graphPath + "/minusStrandSubGraph.xml";
        this.statisticDir = new File(resultDir.getAbsolutePath()+"/statistics");
        statisticDir.mkdirs();
        this.gapStatistic = new File(statisticDir.getAbsolutePath()+"/gapStatistic.json");
    }

    /**** Auxiliary *************************************************/

    void setAnnotationFiles(double alphaValue) {
        File annotationFile = new File(propertyCluster.getAbsolutePath() +"/" +((int)(alphaValue*100)));
        annotationFile.mkdirs();
        this.plusAnnotation = new File(annotationFile.getAbsolutePath() + "/plusAnnotation");
        this.minusAnnotation = new File(annotationFile.getAbsolutePath() + "/minusAnnotation");
    }

    void setAgreementFiles(double alphaValue) {
        agreementFile = new File(propertyCluster.getAbsolutePath() +"/" +((int)(alphaValue*100)));
        agreementFile.mkdirs();

//        this.plusAnnotaedPlusRefseqPropertyAgreement = new File(agreementFile.getAbsolutePath() + "/plusAnnotaedPlusRefseqPropertyAgreement");
//        this.plusAnnotaedPlusRefseqPropertyAgreement.mkdirs();
//        this.plusAnnotaedMinusRefseqPropertyAgreement = new File(agreementFile.getAbsolutePath() + "/plusAnnotaedMinusRefseqPropertyAgreement");
//        this.plusAnnotaedMinusRefseqPropertyAgreement.mkdirs();
//        this.minusAnnotaedPlusRefseqPropertyAgreement = new File(agreementFile.getAbsolutePath() + "/minusAnnotaedPlusRefseqPropertyAgreement");
//        this.minusAnnotaedPlusRefseqPropertyAgreement.mkdirs();
//        this.minusAnnotaedMinusRefseqPropertyAgreement = new File(agreementFile.getAbsolutePath() + "/minusAnnotaedMinusRefseqPropertyAgreement");
//        this.minusAnnotaedMinusRefseqPropertyAgreement.mkdirs();
        this.agreementFile = new File(agreementFile.getAbsolutePath() + "/propertyAgreement");
    }

    void setAlternativeAgreementFiles(double alphaValue) {
        File agreementFile = new File(propertyCluster.getAbsolutePath() +"/" +((int)(alphaValue*100)));
        agreementFile.mkdirs();
        this.plusAnnotaedPlusRefseqAlternativePropertyAgreement = new File(agreementFile.getAbsolutePath() + "/plusAnnotaedPlusRefseqAlternativePropertyAgreement");
        this.plusAnnotaedPlusRefseqAlternativePropertyAgreement.mkdirs();
        this.plusAnnotaedMinusRefseqAlternativePropertyAgreement = new File(agreementFile.getAbsolutePath() + "/plusAnnotaedMinusRefseqAlternativePropertyAgreement");
        this.plusAnnotaedMinusRefseqAlternativePropertyAgreement.mkdirs();
        this.minusAnnotaedPlusRefseqAlternativePropertyAgreement = new File(agreementFile.getAbsolutePath() + "/minusAnnotaedPlusRefseqAlternativePropertyAgreement");
        this.minusAnnotaedPlusRefseqAlternativePropertyAgreement.mkdirs();
        this.minusAnnotaedMinusRefseqAlternativePropertyAgreement = new File(agreementFile.getAbsolutePath() + "/minusAnnotaedMinusRefseqAlternativePropertyAgreement");
        this.minusAnnotaedMinusRefseqAlternativePropertyAgreement.mkdirs();
    }

    String getSubGraphName(boolean strand) {
        return strand ? plusStrandSubGraphName : minusStrandSubGraphName;
    }

    String getParquetPath(PARQUET_NAME parquetName) {
        return parquetDir.getAbsolutePath() + "/" +
                parquetName ;
    }

    String getParquetPath(PARQUET_NAME parquetName, boolean strand) {
        return parquetDir.getAbsolutePath() + "/" +
                (strand ? "PLUS" : "MINUS") + "_" +
                parquetName; //+ SparkComputer.PARQUET_EXTENSION; //+ SparkComputer.PARQUET_EXTENSION;
    }

    File getPropertyClusterFile(boolean strand) {
        return strand ? plusPropertyCluster: minusPropertyCluster;
    }

    File getPropertyClusterTreeFile(boolean strand) {
        return strand ? plusPropertyClusterGraph: minusPropertyClusterGraph;
    }

    public void mkDir(PARQUET_NAME parquetName) {
        File mappedRangeFile = new File(getParquetPath(parquetName));
        mappedRangeFile.mkdirs();
    }

    File getAnnotationFile(boolean strand) {
        return strand ? plusAnnotation : minusAnnotation;
    }

    File getAggreementFile() {
        return agreementFile;
    }

    File getAggreementFile(boolean annotatedStrand, boolean refSeqStrand) {
        if(annotatedStrand) {
            if(refSeqStrand) {
                return plusAnnotaedPlusRefseqPropertyAgreement;
            }
            return plusAnnotaedMinusRefseqPropertyAgreement;
        }
        if(refSeqStrand) {
            return minusAnnotaedPlusRefseqPropertyAgreement;
        }
        return minusAnnotaedMinusRefseqPropertyAgreement;
    }

    File getAlternativeAgreementFile(boolean annotatedStrand, boolean refSeqStrand) {
        if(annotatedStrand) {
            if(refSeqStrand) {
                return plusAnnotaedPlusRefseqAlternativePropertyAgreement;
            }
            return plusAnnotaedMinusRefseqAlternativePropertyAgreement;
        }
        if(refSeqStrand) {
            return minusAnnotaedPlusRefseqAlternativePropertyAgreement;
        }
        return minusAnnotaedMinusRefseqAlternativePropertyAgreement;
    }

    public File getResultDir() {
        return resultDir;
    }



}

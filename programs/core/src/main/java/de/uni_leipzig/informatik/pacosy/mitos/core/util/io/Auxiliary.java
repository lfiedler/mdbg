package de.uni_leipzig.informatik.pacosy.mitos.core.util.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.TreeNode;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordPropertyTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import org.apache.commons.lang.time.StopWatch;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.zookeeper.data.Id;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.count;

public class Auxiliary {

    public static class A {
        private B b;

        public B getB() {
            return b;
        }

        public void setB(B b) {
            this.b = b;
        }
    }

    public static class B {
        private int value;

        public B(int value) {
            this.value = value;
        }
    }

    private Auxiliary() {

    }

    public static <K,V>void printMap(Map<K,V> m) {
        printMap(m," ");
    }

    public static <K,V>void printMap(Map<K,V> m, String separator) {
        m.forEach((u,v) -> System.out.println(u + separator  + v));
    }

    public static <K,V>void printMap(Map<K,V> m, Function<K,String> keyMapper, Function<V,String> valueMapper, String separator) {
        m.forEach((u,v) -> System.out.println( keyMapper.apply(u) + separator  +  valueMapper.apply(v)));
    }

    public static <K,V>void printMap(Map<K,V> m, Function<K,String> keyMapper,  Function<V,String> valueMapper) {
        printMap(m,keyMapper,valueMapper," ");
    }

    public static <K> void printSeq(Collection<K> m) {
        m.forEach((u) -> System.out.println(u));
    }

    public static <K> void printSeq(Collection<K> m, Function<K,String> mapper) {
        m.forEach((u) -> System.out.println(mapper.apply(u)));
    }

    public static <K,V extends Number> void updateDistribution(Class<V> valueClass, Map<K,V> map, K key) {
        if(valueClass.equals(Integer.class)) {
            Integer val = (Integer)map.get(key);
            if(val != null) {
                val++;
            }
            else {
                val = 1;
            }
            map.put(key,(V)val);
        }
        else if(valueClass.equals(Long.class)) {
            Long val = (Long)map.get(key);
            if(val != null) {
                val++;
            }
            else {
                val = 1L;
            }
            map.put(key,(V)val);
        }
    }

    public static <T> Collection<List<T>> partition(List<T> list, int chunkSize) {
        final AtomicInteger counter = new AtomicInteger();
        return list.stream()
                .collect(Collectors.groupingBy(it -> counter.getAndIncrement() / chunkSize))
                .values();
    }

    /**
     * Merges data form first map into second map
     * @param m1
     * @param m2
     */
    public static void mergeDistributions(Map<Object,Long> m1, Map<Object,Long> m2) {
        if(m1 == null) {
            return;
        }
        else if(m2 == null) {
            m2 = new HashMap<>(m1);
            return;
        }
        for(Map.Entry<Object,Long> entry1 : m1.entrySet()) {
            Long count2 = m2.get(entry1.getKey());
            if(count2 == null) {
                m2.put(entry1.getKey(),entry1.getValue());
            }
            else {
                m2.put(entry1.getKey(),entry1.getValue()+count2);
            }
        }
    }

    private static void updateRecordPositionMap(Map<Integer,String> records, Integer record, Integer position) {
        String posString = records.get(record);
        int p;
        String [] posSplit;
        if(posString != null) {
            String [] positions = posString.split(",");
            for(int i = 0; i < positions.length; i++) {
                p = Integer.parseInt(positions[i]);
                if(position == p) {
                    break;
                }
                else if(position < p) {
                    posSplit = posString.split(positions[i]);
                    records.put(record,posSplit[0] + position + "," + positions[i] + posSplit[1]);
                    break;
                }
            }
        }
        else {
            records.put(record,position + ",");
        }
    }

    public static String getCaseInsensitiveRegex(String match) {
        StringBuilder regexString = new StringBuilder();
        for(int i = 0; i < match.length(); i++) {
            String c = match.substring(i,i+1);
            if(!c.equals(" ")) {
                regexString.append("["+ c.toLowerCase() + c.toUpperCase() + "]");
            }
            else {
                regexString.append(" ");
            }
        }
        return regexString.toString();
    }

    private static String getCaseInsensitiveCapital(String match) {
        return "["+ match.substring(0,1).toUpperCase() + match.substring(0,1).toLowerCase()+"]"+ match.substring(1);
    }

    public static String getCaseInsensitiveCapitalRegex(String match) {
        StringBuilder regexString = new StringBuilder();
        String [] words = match.split(" ");

        for(String word: words) {
            regexString.append(getCaseInsensitiveCapital(word) + " ");
        }
        regexString.deleteCharAt(regexString.length()-1);
        return regexString.toString();
    }

    public static void test(StringBuilder str) {

        str.delete(0,str.length());
        str.append("a");
    }

    public static <K,V> Map<K,V> deepCopy(Map<K,V> map) {
        return map.entrySet().stream().
                collect(Collectors.toMap(n -> n.getKey(), n -> n.getValue()));
    }

    public static <T> List<T> deepCopy(List<T> list) {
        return list.stream().collect(Collectors.toList());
    }

    public static <T> Stream<T> getStream(Iterator<T> it) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(it,0),false);
    }

    public static String trimLastCharacter(String string) {
        return string.substring(0,string.length()-1);
    }

    public static int mod(int a, int b) {
        return ((a%b)+b)%b;
    }

    public static void test() {

        List<List<Integer>> lists = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            list.add(i);
            if(i == 5) {
                lists.add(list);
                list = new ArrayList<>();
            }
        }
        lists.add(list);
        System.out.println(lists);
    }

    public static double withMathRound(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    public static List<Integer> getSequence(int start, int end) {
        return IntStream.rangeClosed(start,end).boxed().collect(Collectors.toList());
    }


}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.IdentifierInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class PositionCountIdentifierDT extends PositionCountDT implements IdentifierInterface {

    private String identifier;
    public static final Encoder<PositionCountIdentifierDT> ENCODER = Encoders.bean(PositionCountIdentifierDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        IDENTIFIER("identifier"),
        COUNT("count"),
        POSITION("position");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public PositionCountIdentifierDT(String identifier, int position) {
        this.identifier = identifier;
        this.position = position;
    }


    public PositionCountIdentifierDT(int position, String identifier, int count) {
        super(position,count);
        this.identifier = identifier;
    }

    public PositionCountIdentifierDT() {

    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }


    @Override
    public String toString() {
        return position + ": " + identifier + " " + count;
    }


}

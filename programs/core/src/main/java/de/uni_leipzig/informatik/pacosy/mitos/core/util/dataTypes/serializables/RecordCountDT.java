package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.RecordIdInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class RecordCountDT implements RecordIdInterface, Serializable {
    private int recordId;
    private long count;
    public static final Encoder<RecordCountDT> ENCODER = Encoders.bean(RecordCountDT.class);

    public RecordCountDT() {
    }

    public RecordCountDT(int recordId, long count) {
        this.recordId = recordId;
        this.count = count;
    }

    @Override
    public int getRecordId() {
        return recordId;
    }

    @Override
    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}

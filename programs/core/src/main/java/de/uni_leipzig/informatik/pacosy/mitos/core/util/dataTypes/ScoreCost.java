package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes;

public class ScoreCost {

    private final int rewardCost;
    private final int penaltyCost;

    public ScoreCost(ScoreCost scoreCost) {
        this.rewardCost = scoreCost.rewardCost;
        this.penaltyCost = scoreCost.penaltyCost;
    }
    public ScoreCost(int rewardCost, int penaltyCost) {
        this.rewardCost = rewardCost;
        this.penaltyCost = penaltyCost;
    }

    public int getRewardCost() {
        return rewardCost;
    }

    public int getPenaltyCost() {
        return penaltyCost;
    }

    public String toString() {
        return rewardCost +"/" + penaltyCost;
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import de.uni_leipzig.informatik.pacosy.mitos.core.parser.NucleotideParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.ScoreCost;

import java.util.List;

public class JukesCantorScoreMatrix {

    private ScoreCost scoreCost;

    public JukesCantorScoreMatrix(ScoreCost scoreCost) {
        this.scoreCost = scoreCost;
    }

    public int getScore(char n1, char n2) {
        if(match(n1,n2)) {
            return scoreCost.getRewardCost();
        }
        return scoreCost.getPenaltyCost();
    }

    private static boolean match(char n1, char n2) {
            if(n1 == n2) {
                return true;
            }
            else {
                List<Character> commonNucleorides = NucleotideParser.getNucleotideReplacements(n1);
                commonNucleorides.
                        retainAll(NucleotideParser.getNucleotideReplacements(n2));
                if(commonNucleorides.size() > 0) {
                    return true;
                }
            }
            return false;
    }




}

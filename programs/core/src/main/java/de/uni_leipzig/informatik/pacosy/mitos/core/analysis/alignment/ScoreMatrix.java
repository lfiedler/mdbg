package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScoreMatrix {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ScoreMatrix.class);
    protected int[][] scores;

    protected static final char A = 'A';
    protected static final char C = 'C';
    protected static final char T = 'T';
    protected static final char G = 'G';


    public ScoreMatrix() {
        this.scores = new int[4][4];
    }

    public ScoreMatrix(int[][] scores) {
        this.scores = scores;
    }

    /**
     *
     * @param n1
     * @param n2
     * @return score
     */
    public int getScore(char n1, char n2) {
        return this.scores[getIndex(n1)][getIndex(n2)];
    }

    public void show() {
        for(int i = 0; i < scores.length; i++) {
            for(int j = 0; j < scores.length; j++) {
                System.out.print(scores[i][j] + " ");
            }
            System.out.println();
        }
    }


    protected static int getIndex(char c) {
        switch (c) {
            case A :
                return 0;
            case C :
                return 1;
            case T :
                return 2;
            case G :
                return 3;
            default:
                LOGGER.error("Invalid nucleotide " + c);
                System.exit(-1);
        }
        return -1;
    }

    public int[][] getScores() {
        System.out.print("hello");
        return scores;
    }
}

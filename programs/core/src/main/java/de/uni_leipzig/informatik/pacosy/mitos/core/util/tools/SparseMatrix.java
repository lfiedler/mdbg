package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SparseMatrix {

    protected static final Logger LOGGER = LoggerFactory.getLogger(SparseMatrix.class);
    private Map<Integer, FullCellData> matrices;
    private final int rowCount;             // number of rows
    private final int columnCount;             // number of columns
    private double openGapCost;
    private double extendGapCost;
    private double bestScore;
    private int bestPosition;
    private FullCellData bestCell;
    private double Xg;

    public SparseMatrix(int columnCount, int rowCount, double openGapCost, double extendGapCost, double Xg) {
        this.Xg = Xg;
        this.matrices = new HashMap<>();
        this.rowCount = rowCount+1;
        this.columnCount = columnCount+1;
        this.openGapCost = openGapCost;
        this.extendGapCost = extendGapCost;
        this.bestCell = new FullCellData();
    }

    public FullCellData getCell(int rowIndex, int columnIndex) {
//        CellData cellData = matrices.get(mapPosition(rowIndex,columnIndex));
//        if(cellData == null) {
//            if(rowIndex == 0 || columnIndex == 0) {
//                cellData = new CellData(0,0,0, CellData.DIRECTION.STOP);
//                setCell(rowIndex,columnIndex,cellData);
//            }
//
//        }

        FullCellData cellData = matrices.get(mapPosition(rowIndex,columnIndex));
        if(cellData == null) {
            // within padding regions -> set if necessary
            if(rowIndex == 0 || columnIndex == 0) {
                cellData = new FullCellData(0,0,0, FullCellData.DIRECTION.INVALID);
                setCell(rowIndex,columnIndex,cellData);
            }
            else {
                LOGGER.error("Cell not set yet: " + rowIndex + " " + columnIndex);
                System.exit(-1);
            }

        }

        return cellData;
    }


    public void setCell(int rowIndex, int columnIndex, FullCellData cellData) {
        matrices.put(mapPosition(rowIndex,columnIndex),cellData);
    }

    protected boolean cellIsVisited(int rowIndex, int columnIndex) {
        return cellIsVisited(mapPosition(rowIndex,columnIndex));
    }

    protected boolean cellIsVisited(int position) {
        return matrices.containsKey(position);
    }

    public boolean updateCell(int rowIndex, int columnIndex, double alignScore) {
        System.out.println("Update with row: " + rowIndex + " column: " + columnIndex + " score: " + alignScore);
        if(cellIsVisited(rowIndex,columnIndex)) {
            LOGGER.info("Cell was visited already");
            return false;
        }
        FullCellData previousCell;
        FullCellData cellData = new FullCellData();
        double v1,v2,maxValue;

        // update e
        previousCell = getCell(rowIndex,columnIndex-1);
        v1 = previousCell.getScore()-(openGapCost+extendGapCost);
        v2 = previousCell.getE()-extendGapCost;
        maxValue = Math.max(Math.max(v1,v2),0);
        cellData.setE(maxValue);

        // update f
        previousCell = getCell(rowIndex-1,columnIndex);
        v1 = previousCell.getScore()-(openGapCost+extendGapCost);
        v2 = previousCell.getF()-extendGapCost;
        maxValue = Math.max(Math.max(v1,v2),0);
        cellData.setF(maxValue);

        // update h
        previousCell = getCell(rowIndex-1,columnIndex-1);
        v1 = previousCell.getScore()+ alignScore;
//        v2 = Math.max(v1,Math.max(cellData.geteGap(),cellData.getfGap()));

        if(cellData.getE() > cellData.getF()) {

            if(cellData.getE() >= v1) { // e is maximum
                maxValue = cellData.getE();

                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.UP);
                    if (cellData.getE() == v1) { // e and v score equally well
                        cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                    }
                }
            }
            else { // v1 is maximum
                maxValue = v1;
                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                }
            }
        }
        else if(cellData.getE() < cellData.getF()){
            if(cellData.getF() >= v1) { // f is maximum
                maxValue = cellData.getF();
                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.LEFT);
                    if(cellData.getF() == v1) { // f and v score equally well
                        cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                    }
                }
            }
            else { // v1 is maximum
                maxValue = v1;
                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                }
            }
        }
        else { // e and f score equally well
            if(cellData.getF() >= v1) { // f/e is maximum
                maxValue = cellData.getF();
                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.LEFT);
                    cellData.addDirection(FullCellData.DIRECTION.UP);
                    if(cellData.getF() == v1) { // f, e and v score equally well
                        cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                    }
                }
            }
            else { // v1 is maximum
                maxValue = v1;
                if(maxValue > 0) {
                    cellData.addDirection(FullCellData.DIRECTION.DIAGONAL);
                }
            }
        }
        if(maxValue == 0) {
            cellData.addDirection(FullCellData.DIRECTION.STOP);
        }

//
//        maxValue = Math.max(Math.max(v1,v2),0);
        cellData.setScore(maxValue);
        if(cellData.discard(bestCell,Xg)) {
            return false;
        }
        if(cellData.compareTo(bestCell) > 0) {
            bestCell = cellData;
            bestPosition = mapPosition(rowIndex,columnIndex);
        }
        setCell(rowIndex,columnIndex,cellData);
        return true;
    }

    private int mapPosition(int rowIndex, int columnIndex) {
        if(rowIndex+1 > rowCount || columnIndex+1 > columnCount) {
            LOGGER.error("Invalid cell");
            System.exit(-1);
        }
        return rowIndex* columnCount +columnIndex;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }
}

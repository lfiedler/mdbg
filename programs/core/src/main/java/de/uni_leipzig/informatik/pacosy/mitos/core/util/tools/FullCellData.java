package de.uni_leipzig.informatik.pacosy.mitos.core.util.tools;

import java.util.ArrayList;
import java.util.List;

public class FullCellData implements Comparable<FullCellData> {

    public enum DIRECTION {
        LEFT,
        UP,
        DIAGONAL,
        INVALID,
        STOP;
    }

    private double e;
    private double f;
    private double score;
    private List<DIRECTION> directions;


    public FullCellData(double e, double f, double score, DIRECTION direction) {
        this();
        this.e = e;
        this.f = f;
        this.score = score;
        addDirection(direction);
    }

    public FullCellData() {
        this.directions = new ArrayList<>();
    }

    @Override
    public int compareTo(FullCellData cellData) {
        return ((Double)this.score).compareTo(cellData.score);
    }

    public boolean discard(FullCellData cellData, double Xg) {
        if(this.score < cellData.score-Xg) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object object) {
        if(object == this) {
            return true;
        }
        if(object == null || object.getClass() != this.getClass()) {
            return false;
        }
        FullCellData cellData = (FullCellData) object;
        return this.score == cellData.score;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public List<DIRECTION> getDirection() {
        return directions;
    }

    public void setDirection(List<DIRECTION> directions) {
        this.directions = directions;
    }

    public void addDirection(DIRECTION direction) {
        directions.add(direction);
    }


}

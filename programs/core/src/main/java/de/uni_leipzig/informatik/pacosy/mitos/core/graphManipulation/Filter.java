package de.uni_leipzig.informatik.pacosy.mitos.core.graphManipulation;

import org.apache.tinkerpop.gremlin.process.computer.Computer;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.process.traversal.strategy.decoration.SubgraphStrategy;
//import org.apache.tinkerpop.gremlin.spark.process.computer.SparkGraphComputer;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.janusgraph.core.JanusGraph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Filter {

    JanusGraph graph;
    private static final Logger LOGGER = LoggerFactory.getLogger(Filter.class);

    public Filter(JanusGraph graph) {
        this.graph = graph;
    }

    public GraphTraversalSource filterCollection(String identifier, Collection c) {
        GraphTraversalSource g = null;
        try {
            g = graph.traversal();
            List ids = g.E().has(identifier,P.within(c)).outV().dedup().id().toList();
            System.out.println("Done with id retrieval");
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).edges(__.has(identifier, P.within(c))).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(0);
        }
        return g;
    }

    public <S> GraphTraversalSource filterSingle(String identifier, S property) {
        GraphTraversalSource g = null;
        try {
            g = graph.traversal();
            List ids = g.E().has(identifier,property).outV().dedup().id().toList();
            System.out.println("Done with id retrieval");
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).edges(__.has(identifier, property)).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(0);
        }
        return g;
    }

    public static GraphTraversalSource filterIds(List<Long> ids, JanusGraph graph){
        GraphTraversalSource g = null;
        try {
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(0);
        }
        return g;
    }

    public static GraphTraversalSource filterForProperties(List propertylist, JanusGraph graph){
        GraphTraversalSource g = null;
        try {
            g = graph.traversal();
            List ids = g.E().has("propertyId",P.within(propertylist)).outV().dedup().id().toList();
            System.out.println("Done with id retrieval");
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).edges(__.has("propertyId", P.within(propertylist))).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(0);
        }
        return g;
    }

    public static GraphTraversalSource filterStrand(boolean strand, JanusGraph graph){
        GraphTraversalSource g = null;
        try {
            g = graph.traversal();
            List ids = g.E().has("strand",strand).outV().dedup().id().toList();
            System.out.println("Done with id retrieval");
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).edges(__.has("strand", strand)).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(0);
        }
        return g;

    }

    public static GraphTraversalSource filterForRecords(List<Integer> records, JanusGraph graph){
        GraphTraversalSource g = null;
        try {
            g = graph.traversal();
            List ids = g.E().has("record",P.within(records)).outV().id().dedup().toList();
            System.out.println("Done with id retrieval");
            g = graph.traversal().withStrategies(SubgraphStrategy.build().
                    vertices(__.hasId(P.within(ids))).edges(__.has("record", P.within(records))).create());
        }
        catch (Exception e) {
            System.out.println("Could not filter for access");
            e.printStackTrace();
            System.exit(-1);
        }
        return g;

    }

}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class JaccardStatisticTaxonomyDT extends JaccardStatisticIdDT{
    protected String name;
    protected String taxonomicGroupName;
    protected int taxId;
    public static final Encoder<JaccardStatisticTaxonomyDT> ENCODER = Encoders.bean(JaccardStatisticTaxonomyDT.class);


    public JaccardStatisticTaxonomyDT() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxonomicGroupName() {
        return taxonomicGroupName;
    }

    public void setTaxonomicGroupName(String taxonomicGroupName) {
        this.taxonomicGroupName = taxonomicGroupName;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.psql;

import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapAnalyzer;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping.SequenceMapCreator;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.sys.Prop;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

public class RecordPropertyTable implements SQLTable {

    private static RecordPropertyTable recordPropertyTable;
    private static final String BASE_TABLENAME = "complete.record_property_";
    protected static final String INDEX_BASE_NAME = "RECORD_PROPERTY_INDEX";
    protected static final String VOID_PROPERTIES_PLUS_FUNCTION = "complete.get_void_properties_plus";
    protected static final String VOID_PROPERTIES_MINUS_FUNCTION = "complete.get_void_properties_minus";
    protected static String REFSEQ ="";
    protected static final Logger LOGGER = LoggerFactory.getLogger(RecordPropertyTable.class);

    private RecordPropertyTable(
    ) {
        setREFSEQ("refseq");
    }

    public static synchronized RecordPropertyTable getInstance() {
        if(recordPropertyTable == null) {
            recordPropertyTable = new RecordPropertyTable();
        }
        return recordPropertyTable;
    }

    /****CREATION********************************************************************************/
    public void createTable() throws SQLException {
        createTable(true);
        createTable(false);
        createFunctions();
    }

    public void createTable(boolean strand) throws SQLException {
        String tableName = getTableName(strand);
        JDBJInterface.getInstance().createTable(tableName,
                RecordPropertyDT.COLUMN_NAME.RECORD_ID.identifier + " integer,\n" +
                        RecordPropertyDT.COLUMN_NAME.POSITION.identifier + " integer,\n" +
                        RecordPropertyDT.COLUMN_NAME.CATEGORY.identifier + " TEXT,\n" +
                        RecordPropertyDT.COLUMN_NAME.PROPERTY.identifier + " TEXT\n"
                , true);
    }


    public void dropTable() throws SQLException {
        dropTable(true);
        dropTable(false);
    }

    public void dropTable(boolean strand) throws SQLException {
        JDBJInterface.getInstance().dropTable(getTableName(strand), true);
    }

    public void dropPrimaryKey(boolean strand) throws SQLException {
        try {
            JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS " + getTableName(strand) +
                    " DROP CONSTRAINT IF EXISTS " + getPrimaryKeyConstraintIdentifier(strand));

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable(strand);
            } else {
                throw s;
            }
        }

    }

    public void dropPrimaryKey() throws SQLException {
        dropPrimaryKey(true);
        dropPrimaryKey(false);
    }

    public void createIndex(boolean strand) throws SQLException {
        JDBJInterface.getInstance().executeUpdate(
                "CREATE INDEX " + getIndexName(strand) +
                        " ON " + getTableName(strand) + " ()"
        );
    }

    public void createIndex() throws SQLException {
        createIndex(true);
        createIndex(false);
    }

    public void createPrimaryKey(boolean strand) throws SQLException {
        JDBJInterface.getInstance().executeUpdate("ALTER TABLE IF EXISTS " + getTableName(strand) +
                " ADD CONSTRAINT " + getPrimaryKeyConstraintIdentifier(strand) +
                " PRIMARY KEY (" + RecordPropertyDT.COLUMN_NAME.RECORD_ID.identifier + ","
                + RecordPropertyDT.COLUMN_NAME.POSITION.identifier + ","
                + RecordPropertyDT.COLUMN_NAME.PROPERTY.identifier +
                ")");
    }

    public void createPrimaryKey() throws SQLException {
        createPrimaryKey(true);
        createPrimaryKey(false);
    }

    public void createFunctions() throws SQLException {
        JDBJInterface.getInstance().executeUpdate(
                "CREATE OR REPLACE FUNCTION "+getVoidFunctionName(true)+"(rec INTEGER)\n" +
                        "RETURNS TABLE ( \n" +
                        "  \"recordId\" integer,\n" +
                        "  \"position\" integer,\n" +
                        "  category text,\n" +
                        "  property text)\n" +
                        "AS $$\n" +
                        "DECLARE\n" +
                        "\tr ALIAS FOR $1;\n" +
                        "\tl integer;\n" +
                        "BEGIN\n" +
                        "\tSELECT rTable.length FROM complete.record rTable WHERE rTable.\"recordId\" = r INTO l;\n" +
                        "\n" +
                        "\n" +
                        "\tRAISE NOTICE 'USING l %', l;\n" +
                        "\tRETURN QUERY\n" +
                        "\tSELECT r, *, 'void', 'void'\n" +
                        "\tFROM\n" +
                        "\t(SELECT *\n" +
                        "\t FROM\n" +
                        "\tgenerate_series(0,l-1) EXCEPT(\n" +
                        "\tSELECT t.position FROM\n" +
                        "\t"+getTableName(true)+" t\n" +
                        "\tWHERE t.\"recordId\" = r)) AS t1;\n" +
                        "END;\n" +
                        "$$ LANGUAGE plpgsql;"
        );
        JDBJInterface.getInstance().executeUpdate(
                "CREATE OR REPLACE FUNCTION "+getVoidFunctionName(false)+"(rec INTEGER)\n" +
                        "RETURNS TABLE ( \n" +
                        "  \"recordId\" integer,\n" +
                        "  \"position\" integer,\n" +
                        "  category text,\n" +
                        "  property text)\n" +
                        "AS $$\n" +
                        "DECLARE\n" +
                        "\tr ALIAS FOR $1;\n" +
                        "\tl integer;\n" +
                        "BEGIN\n" +
                        "\tSELECT rTable.length FROM complete.record rTable WHERE rTable.\"recordId\" = r INTO l;\n" +
                        "\n" +
                        "\n" +
                        "\tRAISE NOTICE 'USING l %', l;\n" +
                        "\tRETURN QUERY\n" +
                        "\tSELECT r, *, 'void', 'void'\n" +
                        "\tFROM\n" +
                        "\t(SELECT *\n" +
                        "\t FROM\n" +
                        "\tgenerate_series(0,l-1) EXCEPT(\n" +
                        "\tSELECT t.position FROM\n" +
                        "\t"+getTableName(false)+" t\n" +
                        "\tWHERE t.\"recordId\" = r)) AS t1;\n" +
                        "END;\n" +
                        "$$ LANGUAGE plpgsql;"
        );
    }

    private void renameOrfs(boolean strand) throws SQLException {
        try {
//            System.out.println("UPDATE \n" +
//                    getTableName(strand) + " \n" +
//                    "set category = 'otherType'  WHERE category='protein' AND (property = 'orf' OR property = 'orf168')");
            JDBJInterface.getInstance().executeUpdate(
                    "UPDATE \n" +
                    getTableName(strand) + " \n" +
                    "set category = 'otherType'  WHERE category='protein' AND (property = 'orf' OR property = 'orf168')");

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
    }

    public void renameOrfs() throws SQLException {
        renameOrfs(true);
        renameOrfs(false);
    }

    public void removeDuplicates(boolean strand) throws SQLException {
        try {
            JDBJInterface.getInstance().executeUpdate("DELETE FROM\n" +
                    getTableName(strand) + " t1\n" +
                    "USING "+getTableName(strand) +" t2\n" +
                    "WHERE t1.ctid < t2.ctid\n" +
                    "AND t1.\"recordId\" = t2.\"recordId\"\n" +
                    "AND t1.position = t2.position\n" +
                    "AND t1.property = t2.property\n" +
                    "AND t1.category = t2.category");

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
    }

    public void removeDuplicates() throws SQLException {
        removeDuplicates(true);
        removeDuplicates(false);
    }

    public void addProperties(boolean strand, int recordId, Map<Integer,List<DbgGraph.Feature>> properties) throws SQLException {
        String values = "";
        try {
            for (Map.Entry<Integer,List<DbgGraph.Feature>> entry : properties.entrySet()) {
                for(DbgGraph.Feature feature: entry.getValue()) {
//                    System.out.println("( " + recordId + "," + entry.getKey() + ",'" + feature.category + "','" + feature.getFeatureAsString() +"'),");
                    values += "( " + recordId + "," + entry.getKey() + ",'" + feature.category + "','" + feature.getFeatureAsString() +"'),";
                }
            }
            values = values.substring(0, values.length() - 1);
            JDBJInterface.getInstance().insertDataIntoTable(getTableName(strand), values);

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable(strand);
                values = "";
                for (Map.Entry<Integer,List<DbgGraph.Feature>> entry : properties.entrySet()) {
                    for(DbgGraph.Feature feature: entry.getValue()) {
                        values += "( " + recordId + "," + entry.getKey() + ",'" + feature.category + "','" + feature.getFeatureAsString() +"'),";
                    }
                }
                values = values.substring(0, values.length() - 1);
                JDBJInterface.getInstance().insertDataIntoTable(getTableName(strand), values);
            } else {
                throw s;
            }
        }
    }

    public void addVoidProperties(int recordId) throws SQLException {
        try {

            for(boolean strand: Arrays.asList(true,false)) {
                JDBJInterface.getInstance().executeUpdate( "INSERT INTO " + getTableName(strand) +
                        " SELECT * FROM " + getVoidFunctionName(strand) + "(" + recordId + ")");
            }

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: function ")) {
                createFunctions();
                for(boolean strand: Arrays.asList(true,false)) {
                    JDBJInterface.getInstance().executeUpdate( "INSERT INTO " + getTableName(strand) +
                            " SELECT * FROM " + getVoidFunctionName(strand));
                }
            }
            else if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();

            } else {
                throw s;
            }
        }
    }

    public void deleteProperties(boolean strand, int recordId) throws SQLException {
        try {
            JDBJInterface.getInstance().
                    removeDataFromTable(getTableName(strand),
                            RecordDT.COLUMN_NAME.RECORDID.identifier + " = " +recordId);

        } catch (SQLException s) {
            if (s.getMessage().contains("ERROR: relation ") && s.getMessage().contains(" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw s;
            }
        }
    }

    public void addVoidProperties() throws SQLException {
        int counter = 0;
        List<RecordDT> records = RecordTable.getInstance().getTableEntries().collectAsList();
        for(RecordDT record: records) {
            addVoidProperties(record.getRecordId());
//            System.out.println("Record " + record.getRecordId() + " done " + counter);
            counter++;
        }
    }

    /****QUERIES********************************************************************************/

    public List<String> getAllPersistedProperties() {
        String query =
                "SELECT DISTINCT(property) FROM " + getTableName(true) +
                        " UNION SELECT DISTINCT(property) FROM " + getTableName(false);
        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(Encoders.STRING()).distinct().collectAsList();
    }

    public List<String> getAllPersistedCategories() {
        String query =
                "SELECT DISTINCT(category) FROM " + getTableName(true) +
                        " UNION SELECT DISTINCT(category) FROM " + getTableName(false);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(Encoders.STRING()).distinct().collectAsList();
    }

    public Dataset<PropertyLengthDetailedDT> getPropertyLengthDistribution(boolean strand) {
        String query =
                "SELECT category, property, MIN(length) as \"minLength\", MAX(length) as \"maxLength\", AVG(length) AS \"meanLength\" , count(length) AS count\n" +
                        "FROM(\n" +
                        "SELECT \"recordId\",category, property, max(position)-min(position)+ 1 AS length\n" +
                        "FROM\n" +
                        "(SELECT *,position- ROW_NUMBER() OVER(PARTITION BY \"recordId\",property ORDER BY position) AS r\n" +
                        "FROM "+ getTableName(strand)+ ")  AS t\n" +
                        "GROUP BY \"recordId\", \"r\", category, property) AS t2\n" +
                        "GROUP BY category, property";
        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(PropertyLengthDetailedDT.ENCODER);
    }

    public String getPropertyCountQuery(boolean strand, int recordId, boolean removeSL) {
        if(removeSL) {
            System.out.println("in here");
            return  "SELECT category , COUNT(*) FROM\n"+
                    "(SELECT  DISTINCT(property),category\n" +
                    "FROM "+getTableName(strand)+ " WHERE \"recordId\" = "+recordId+" AND category IN('trna','rrna','protein') AND property NOT IN('S1','S2','S', 'L1','L2','L')) AS t\n"+
                    "GROUP BY category";
        }
        return  "SELECT category , COUNT(*) FROM\n"+
                "(SELECT  DISTINCT(property),category\n" +
                "FROM "+getTableName(strand)+ " WHERE \"recordId\" = "+recordId+" AND category IN('trna','rrna','protein')) AS t\n"+
                "GROUP BY category";
    }
    public List<CategoryCountRecordIdDT> getPropertyCounts(List<Integer> ids, boolean removeSL) {

        List<CategoryCountRecordIdDT> results = new ArrayList<>();
        for(int recordId: ids) {


            List<CategoryCountRecordIdDT> dataset = JDBJInterface.getInstance().fetchQueryResult(getPropertyCountQuery(true,recordId,removeSL)).
                    union(JDBJInterface.getInstance().fetchQueryResult(getPropertyCountQuery(false,recordId,removeSL))).
                    groupBy(ColumnIdentifier.CATEGORY).agg(sum(ColumnIdentifier.COUNT).as(ColumnIdentifier.COUNT)).withColumn(ColumnIdentifier.RECORD_ID,lit(recordId)).
                    as(CategoryCountRecordIdDT.ENCODER).
                    collectAsList();
            results.addAll(dataset);

        }
        return results;


    }



    public Dataset<PropertyRangeDT> getPropertyRanges(boolean strand, int recordId) {
//        String query =
//                "SELECT t1.\"recordId\", t1.position, t1.category, t1.property FROM " +
//                "(SELECT * "+
//                        "FROM " + getTableName(strand) +
//                        " WHERE " + RecordPropertyDT.COLUMN_NAME.RECORD_ID.identifier + " = " + recordId +") AS t1" +
//                        " INNER JOIN "+
//                        " (VALUES " + positions.stream().map(k -> "("+k+")").collect(Collectors.joining(",")) + ") AS t2 (s) " +
//                        " ON (t1.position = t2.position) ";
        String query =
               "SELECT  category, property, min(position) AS start, max(position) As end, max(position)-min(position)+ 1 AS length\n" +
                       "FROM\n" +
                       "(SELECT *,position- ROW_NUMBER() OVER(PARTITION BY property ORDER BY position) AS r\n" +
                       "FROM "+getTableName(strand)+ " WHERE \"recordId\" = "+recordId+") AS t\n" +
                       "GROUP BY \"r\", category, property\n" +
                       "ORDER BY start";
//        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(PropertyRangeDT.ENCODER);
    }

    public Dataset<RecordPropertyDT> getProperties(boolean strand, int recordId, Collection<Integer> positions) {
//        String query =
//                "SELECT t1.\"recordId\", t1.position, t1.category, t1.property FROM " +
//                "(SELECT * "+
//                        "FROM " + getTableName(strand) +
//                        " WHERE " + RecordPropertyDT.COLUMN_NAME.RECORD_ID.identifier + " = " + recordId +") AS t1" +
//                        " INNER JOIN "+
//                        " (VALUES " + positions.stream().map(k -> "("+k+")").collect(Collectors.joining(",")) + ") AS t2 (s) " +
//                        " ON (t1.position = t2.position) ";
        String query =
                "SELECT *  " +
                        "FROM " + getTableName(strand) +
                        " WHERE " + RecordPropertyDT.COLUMN_NAME.RECORD_ID.identifier + " = " + recordId +"\n" +
                        "AND position IN "+ JDBJInterface.parseToColumnFormat(positions,Integer.class);
        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(RecordPropertyDT.ENCODER);
    }

    public Dataset<RecordPropertyDT> getProperties(boolean strand, List<Row> recordPositions) {
//        String values = recordPositions.stream().map(k -> "("+k.getInt(0) +","+ k.getInt(1)+")").collect(Collectors.joining(","));

//        String query =
//                "SELECT \"recordId\", position, category, property " +
//                        "FROM " + getTableName(strand) +
//                        " INNER JOIN "+
//                        " (VALUES " + values+ ") AS t2 (s) " +
//                        " ON (t1.position = t2.position) ";
        String query =
                "SELECT *  " +
                        "FROM " + getTableName(strand) +
                        " WHERE (\"recordId\", position) "+
                        "IN "+ JDBJInterface.parseRowListToColumnFormat(recordPositions,Integer.class);
        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query).as(RecordPropertyDT.ENCODER);
    }

    public Dataset<Row> getPropertiesFromRecordRanges(boolean strand, List<RecordDiagonalBiPositionRangeDT> recordPositions) {
        String values = recordPositions.stream().
                map(k -> "("+k.getRecordId() +","+ k.getRecordStart()+","+k.getRecordEnd()+","+ k.getStart() + "," + k.getEnd() +")").
                collect(Collectors.joining(","));

        String query =
                "SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                        + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\"\n" +
                        "FROM " + getTableName(strand) +
                        " INNER JOIN "+
                        " (VALUES " + values+ ")\n AS t1 (rec,\""+ ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                        + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\")" +
                        " ON (\"recordId\"=t1.rec AND position >= t1.\""+ColumnIdentifier.RECORD_START+"\" AND position <= t1.\""+ColumnIdentifier.RECORD_END+"\") ";

        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public Dataset<Row> getAggregatedPropertiesFromRecordRanges(boolean strand, List<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength) {
        String values = recordPositions.stream().
                map(k -> "("+k.getRecordId() +","+ k.getRecordStart()+","+k.getRecordEnd()+","+ k.getStart() + "," + k.getEnd() +")").
                collect(Collectors.joining(","));
        try {
//            JDBJInterface.getInstance().dropTable("t1", true);
            System.out.println("here");
            JDBJInterface.getInstance().execute("CREATE TEMP TABLE t1  AS WITH v (rec,\""+ ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                    + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\") AS (VALUES " + values+ ") SELECT * FROM v");

//            JDBJInterface.getInstance().executeUpdate("WITH v (rec,\""+ ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
//                    + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\") AS (VALUES " + values+ ") SELECT * INTO temporary table t1 FROM v");
//            JDBJInterface.getInstance().execute("CREATE INDEX pINDEX ON t1 USING btree(rec)");
            System.out.println(JDBJInterface.getInstance().exists("t1"));
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT rec FROM t1");
            while(resultSet.next()) {
                System.out.println(resultSet.getInt(1));
            }
            LOGGER.info("Done with temp table creation");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                "FROM\n" +
                "(SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.START + "\",t.\""+ColumnIdentifier.END + "\", \"recordId\" ORDER BY t.property, t.position) AS diff\n" +
                "FROM (SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                "FROM " + getTableName(strand) +
                " INNER JOIN t1 "+
                " ON (\"recordId\"=t1.rec AND position >= t1.\""+ColumnIdentifier.RECORD_START+"\" AND position <= t1.\""+ColumnIdentifier.RECORD_END+"\")) AS t) AS t2\n" +
                "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";


        System.out.println(query);
        Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);

        try {
            JDBJInterface.getInstance().commit();
            JDBJInterface.getInstance().dropTable("t1", true);
            LOGGER.info("Dropped temp table");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        return result;
    }

    public List<Dataset<Row>> getAggregatedPropertiesFromRecordRanges( Dataset<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength, String tableIdentifier) {

        final String tableName = "extracted." + tableIdentifier;
        List<Dataset<Row>> results = new ArrayList<>();
        try {

            JDBJInterface.getInstance().dropTable(tableName, true);
//            System.out.println("Exists " + JDBJInterface.getInstance().exists(tableName));
            JDBJInterface.getInstance().
                    createTable(tableName,"rec integer, \"recordStart\" integer, \"recordEnd\" integer,start integer ,\"end\" integer",true);

            JDBJInterface.getInstance().insertDataIntoTable(tableName,recordPositions.select(
                    col(ColumnIdentifier.RECORD_ID).as("rec"),col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.START),col(ColumnIdentifier.END)));
            JDBJInterface.getInstance().executeUpdate("CREATE INDEX "+tableIdentifier +"pINDEX ON "+ tableName+" USING btree(rec)");
//            JDBJInterface.getInstance().executeUpdate("ALTER TABLE " + tableName + " RENAME COLUMN \"rec\" TO rec " );
//            System.out.println("Exists " + JDBJInterface.getInstance().exists(tableName));
//            ResultSet resultSet = JDBJInterface.getInstance().
//                    executeQuery("SELECT rec FROM t1");
//            while(resultSet.next()) {
//                System.out.println(resultSet.getInt(1));
//            }
//            LOGGER.info("Done with temp table creation");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        for(boolean strand : Arrays.asList(true,false)) {
//            System.out.println(strand);
            String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                    "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                    ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                    "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                    ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
                    "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                    "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                    "FROM\n" +
                    "(SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                    + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                    "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.START + "\",t.\""+ColumnIdentifier.END + "\", \"recordId\" ORDER BY t.property, t.position) AS diff\n" +
                    "FROM (SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                    + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                    "FROM " + getTableName(strand) +
                    " INNER JOIN "+ tableName + " t1 "+
                    " ON (\"recordId\"=t1.rec AND position >= t1.\""+ColumnIdentifier.RECORD_START+"\" AND position <= t1.\""+ColumnIdentifier.RECORD_END+"\")) AS t) AS t2\n" +
                    "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                    "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";

//            System.out.println(query);
            Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);
            results.add(result);
        }

//        result.show();

//        try {
//
//            JDBJInterface.getInstance().dropTable(tableName, true);
//            LOGGER.info("Dropped temp table");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return null;
//        }
        return results;
    }

    public Dataset<Row> getAggregatedPropertiesFromRecordRanges(boolean strand, Dataset<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength, String tableIdentifier) {

        final String tableName = "extracted." + tableIdentifier + "_" + strand;
        try {

            JDBJInterface.getInstance().dropTable(tableName, true);
            System.out.println("Exists " + JDBJInterface.getInstance().exists(tableName));
            JDBJInterface.getInstance().
                    createTable(tableName,"rec integer, \"recordStart\" integer, \"recordEnd\" integer,start integer ,\"end\" integer",true);

            JDBJInterface.getInstance().insertDataIntoTable(tableName,recordPositions.select(
                    col(ColumnIdentifier.RECORD_ID).as("rec"),col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.START),col(ColumnIdentifier.END)));
            JDBJInterface.getInstance().executeUpdate("CREATE INDEX "+tableIdentifier +"_" + strand+"pINDEX ON "+ tableName+" USING btree(rec)");
//            JDBJInterface.getInstance().executeUpdate("ALTER TABLE " + tableName + " RENAME COLUMN \"rec\" TO rec " );
            System.out.println(JDBJInterface.getInstance().exists(tableName));
//            ResultSet resultSet = JDBJInterface.getInstance().
//                    executeQuery("SELECT rec FROM t1");
//            while(resultSet.next()) {
//                System.out.println(resultSet.getInt(1));
//            }
            LOGGER.info("Done with temp table creation");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                "FROM\n" +
                "(SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.START + "\",t.\""+ColumnIdentifier.END + "\", \"recordId\" ORDER BY t.property, t.position) AS diff\n" +
                "FROM (SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                "FROM " + getTableName(strand) +
                " INNER JOIN "+ tableName + " t1 "+
                 " ON (\"recordId\"=t1.rec AND position >= t1.\""+ColumnIdentifier.RECORD_START+"\" AND position <= t1.\""+ColumnIdentifier.RECORD_END+"\")) AS t) AS t2\n" +
                "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";



        System.out.println(query);
        Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);
//        result.show();

//        try {
//
//            JDBJInterface.getInstance().dropTable(tableName, true);
//            LOGGER.info("Dropped temp table");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return null;
//        }
        return result;
    }

    public List<Dataset<Row>> getAggregatedPropertiesFromCyclicRecordRanges(Dataset<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength, String tableIdentifier) {

        final String tableName = "extracted." + tableIdentifier ;
        List<Dataset<Row>> results = new ArrayList<>();
        try {

            JDBJInterface.getInstance().dropTable(tableName, true);
            System.out.println("Exists " + JDBJInterface.getInstance().exists(tableName));
            JDBJInterface.getInstance().
                    createTable(tableName,"rec integer, \"recordStart\" integer, \"recordEnd\" integer,start integer ,\"end\" integer",true);

            JDBJInterface.getInstance().insertDataIntoTable(tableName,recordPositions.select(
                    col(ColumnIdentifier.RECORD_ID).as("rec"),col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.START),col(ColumnIdentifier.END)));
            JDBJInterface.getInstance().executeUpdate("CREATE INDEX "+tableIdentifier+"pINDEX ON "+ tableName+" USING btree(rec)");
//            JDBJInterface.getInstance().executeUpdate("ALTER TABLE " + tableName + " RENAME COLUMN \"rec\" TO rec " );
            System.out.println("Exists " + JDBJInterface.getInstance().exists(tableName));
//            ResultSet resultSet = JDBJInterface.getInstance().
//                    executeQuery("SELECT rec FROM t1");
//            while(resultSet.next()) {
//                System.out.println(resultSet.getInt(1));
//            }
            LOGGER.info("Done with temp table creation");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        for(boolean strand : Arrays.asList(true,false)) {
            System.out.println(strand);
            String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                    "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                    ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                    "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                    ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
//                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                    "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                    "FROM\n" +
                    "(SELECT \"recordId\", " +
                    " position,  \"" +ColumnIdentifier.RECORD_LENGTH+"\" " +
                    ", category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                    + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                    "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.RECORD_START + "\",t.\""+ColumnIdentifier.RECORD_END + "\", \"recordId\"  ORDER BY t.property, t.position) AS diff\n" +
                    "FROM (SELECT \"recordId\", " +
                    "CASE WHEN NOT(position <= \"recordEnd\" + \"recordLength\"  AND position >=\"recordStart\") THEN  position + \"recordLength\" ELSE position END AS position, " +
                    "category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" +
                    ColumnIdentifier.RECORD_END + "\" + \""+ColumnIdentifier.RECORD_LENGTH+"\" AS  \""+ColumnIdentifier.RECORD_END+"\" ," +
                    " \""+ColumnIdentifier.RECORD_LENGTH+"\" ,\""+
                    ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                    "FROM " + getTableName(strand) +
                    " INNER JOIN "+ tableName + " t1 "+
                    " ON (\"recordId\"=t1.rec AND (position >= t1.\""+ColumnIdentifier.RECORD_START+"\" OR position <= t1.\""+ColumnIdentifier.RECORD_END+"\"))) AS t) AS t2\n" +
                    "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                    "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";


            System.out.println(query);
            Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);
            results.add(result);
        }

//        result.show();

//        try {
//
//            JDBJInterface.getInstance().dropTable(tableName, true);
//            LOGGER.info("Dropped temp table");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return null;
//        }
        return results;
    }

    public Dataset<Row> getAggregatedPropertiesFromCyclicRecordRanges(boolean strand, Dataset<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength, String tableIdentifier) {

        final String tableName = "extracted." + tableIdentifier + "_" + strand;
        try {

            JDBJInterface.getInstance().dropTable(tableName, true);
            JDBJInterface.getInstance().
                    createTable(tableName,"rec integer, \"recordStart\" integer, \"recordEnd\" integer,start integer ,\"end\" integer",true);

            JDBJInterface.getInstance().insertDataIntoTable(tableName,recordPositions.select(
                    col(ColumnIdentifier.RECORD_ID).as("rec"),col(ColumnIdentifier.RECORD_START),col(ColumnIdentifier.RECORD_END),col(ColumnIdentifier.START),col(ColumnIdentifier.END)));
            JDBJInterface.getInstance().executeUpdate("CREATE INDEX "+tableIdentifier+"pINDEX ON "+ tableName+" USING btree(rec)");
//            JDBJInterface.getInstance().executeUpdate("ALTER TABLE " + tableName + " RENAME COLUMN \"rec\" TO rec " );
            System.out.println(JDBJInterface.getInstance().exists(tableName));
//            ResultSet resultSet = JDBJInterface.getInstance().
//                    executeQuery("SELECT rec FROM t1");
//            while(resultSet.next()) {
//                System.out.println(resultSet.getInt(1));
//            }
            LOGGER.info("Done with temp table creation");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
//                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                "FROM\n" +
                "(SELECT \"recordId\", " +
                " position,  \"" +ColumnIdentifier.RECORD_LENGTH+"\" " +
                ", category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.RECORD_START + "\",t.\""+ColumnIdentifier.RECORD_END + "\", \"recordId\"  ORDER BY t.property, t.position) AS diff\n" +
                "FROM (SELECT \"recordId\", " +
                "CASE WHEN NOT(position <= \"recordEnd\" + \"recordLength\"  AND position >=\"recordStart\") THEN  position + \"recordLength\" ELSE position END AS position, " +
                "category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" +
                ColumnIdentifier.RECORD_END + "\" + \""+ColumnIdentifier.RECORD_LENGTH+"\" AS  \""+ColumnIdentifier.RECORD_END+"\" ," +
                " \""+ColumnIdentifier.RECORD_LENGTH+"\" ,\""+
                ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                "FROM " + getTableName(strand) +
                " INNER JOIN "+ tableName + " t1 "+
                " ON (\"recordId\"=t1.rec AND (position >= t1.\""+ColumnIdentifier.RECORD_START+"\" OR position <= t1.\""+ColumnIdentifier.RECORD_END+"\"))) AS t) AS t2\n" +
                "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";

//        String query ="SELECT \"recordId\"," +
////                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
//                "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
//                ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
//                "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
//                ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
//                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
////                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
//                "MAX(position)-MIN(position)+1 AS length, property,category\n" +
//                "FROM\n" +
//                "(SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
//                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
//                "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.START + "\",t.\""+ColumnIdentifier.END + "\", \"recordId\" ORDER BY t.property, t.position) AS diff\n" +
//                "FROM (SELECT \"recordId\", position, category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
//                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
//                "FROM " + getTableName(strand) +
//                " INNER JOIN "+
//                " (VALUES " + values+ ")\n AS t1 (rec,\""+ ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
//                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\")" +
//                " ON (\"recordId\"=t1.rec AND position >= t1.\""+ColumnIdentifier.RECORD_START+"\" AND position <= t1.\""+ColumnIdentifier.RECORD_END+"\")) AS t) AS t2\n" +
//                "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
//                "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";


//        System.out.println(query);
        Dataset<Row> result = JDBJInterface.getInstance().fetchQueryResult(query);
//        result.show();

//        try {
//
//            JDBJInterface.getInstance().dropTable(tableName, true);
//            LOGGER.info("Dropped temp table");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return null;
//        }
        return result;
    }

    public Dataset<Row> getAggregatedPropertiesFromCyclicRecordRanges(boolean strand, List<RecordDiagonalBiPositionRangeDT> recordPositions, int sequenceLength) {
        String values = recordPositions.stream().
                map(k -> "("+k.getRecordId() +","+ k.getRecordStart()+","+k.getRecordEnd()+","+ k.getStart() + "," + k.getEnd() + "," + k.getRecordLength()+")").
                collect(Collectors.joining(","));

        String query ="SELECT \"recordId\"," +
//                "\""+ColumnIdentifier.START+"\", \""+ColumnIdentifier.END+"\"," +
                "(((\""+ColumnIdentifier.START+"\"+ (MIN(position)-\""+ColumnIdentifier.RECORD_START+"\")) % "+sequenceLength+") + "+sequenceLength+
                ") %"+ sequenceLength+" AS \""+ColumnIdentifier.START+"\", " +
                "(((\""+ColumnIdentifier.END+"\"- (\"" + ColumnIdentifier.RECORD_END + "\"-MAX(position)))% "+sequenceLength+") + " + sequenceLength +
                ") %" +sequenceLength+ " AS \""+ColumnIdentifier.END+"\", " +
//                "\""+ColumnIdentifier.RECORD_START + "\", \"" + ColumnIdentifier.RECORD_END + "\", " +
//                "MIN(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_START + "\", MAX(position) AS \"" + ColumnIdentifier.RECORD_SHIFT_END + "\", " +
                "MAX(position)-MIN(position)+1 AS length, property,category\n" +
                "FROM\n" +
                "(SELECT \"recordId\", " +
                " position,  \"" +ColumnIdentifier.RECORD_LENGTH+"\" " +
                ", category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +
                "\",position - ROW_NUMBER() OVER( PARTITION BY t.\""+ColumnIdentifier.RECORD_START + "\",t.\""+ColumnIdentifier.RECORD_END + "\", \"recordId\"  ORDER BY t.property, t.position) AS diff\n" +
                "FROM (SELECT \"recordId\", " +
                "CASE WHEN NOT(position <= \"recordEnd\" + \"recordLength\"  AND position >=\"recordStart\") THEN  position + \"recordLength\" ELSE position END AS position, " +
                "category, property,\""+ColumnIdentifier.RECORD_START + "\",\"" +
                ColumnIdentifier.RECORD_END + "\" + \""+ColumnIdentifier.RECORD_LENGTH+"\" AS  \""+ColumnIdentifier.RECORD_END+"\" ," +
                " \""+ColumnIdentifier.RECORD_LENGTH+"\" ,\""+
                ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END +"\"\n"+
                "FROM " + getTableName(strand) +
                " INNER JOIN "+
                " (VALUES " + values+ ")\n AS t1 (rec,\""+ ColumnIdentifier.RECORD_START + "\",\"" + ColumnIdentifier.RECORD_END + "\",\""
                + ColumnIdentifier.START + "\",\"" + ColumnIdentifier.END + "\",\"" + ColumnIdentifier.RECORD_LENGTH + "\")" +
                " ON (\"recordId\"=t1.rec AND (position >= t1.\""+ColumnIdentifier.RECORD_START+"\" OR position <= t1.\""+ColumnIdentifier.RECORD_END+"\"))) AS t) AS t2\n" +
                "GROUP BY \"recordId\",t2.diff, t2.\""+ColumnIdentifier.START+"\", t2.\""+ColumnIdentifier.END+
                "\", t2.\""+ColumnIdentifier.RECORD_START+"\", t2.\""+ColumnIdentifier.RECORD_END+"\", property,category";

        System.out.println(query);
        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public Dataset<RecordPropertyDT> getProperties(boolean strand, Dataset<Row> recordPositions) {

        return  JDBJInterface.getInstance().fetchTable(getTableName(strand)).join(recordPositions.
                        withColumnRenamed(ColumnIdentifier.RECORD_ID,"r").withColumnRenamed(ColumnIdentifier.POSITION,"p"),
                col("p").equalTo(col(ColumnIdentifier.POSITION)).and(col("r").equalTo(col(ColumnIdentifier.RECORD_ID))), SparkComputer.JOIN_TYPES.INNER).
                drop("p","r").as(RecordPropertyDT.ENCODER);
    }

    public List<Integer> getPersistedRecordIds(boolean strand) throws SQLException {

        List<Integer> recordIds = new ArrayList<>();
        try {
            ResultSet resultSet = JDBJInterface.getInstance().
                    executeQuery("SELECT DISTINCT(" + RecordDT.COLUMN_NAME.RECORDID.identifier +
                            ") FROM " +getTableName(strand) +"");
            while (resultSet.next()) {
                recordIds.add(resultSet.getInt("recordId"));
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("ERROR: relation \"" + getTableName(strand)+ "\" does not exist")) {
                JDBJInterface.getInstance().rollBack();
                createTable();
            } else {
                throw e;
            }
        }

        return recordIds;
    }

    public List<Integer> getPersistedRecordIds() throws SQLException {

        List<Integer> recordIds = getPersistedRecordIds(true);
        recordIds.addAll(getPersistedRecordIds(false));
        return recordIds.stream().distinct().collect(Collectors.toList());

    }

    public Dataset<Row> checkForDuplicates(boolean strand) {
        String query = "SELECT \"recordId\", POSITION, property\n" +
                "FROM "+getTableName(strand)+ "\n" +
                "GROUP BY \"recordId\", POSITION, property\n" +
                "HAVING COUNT(*) > 1";


        return JDBJInterface.getInstance().fetchQueryResult(query);
    }

    public void persistPropertyLengthDistribution(boolean strand) {
        SparkComputer.persistDataFrame(getPropertyLengthDistribution(strand), ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/" +strand);
    }

    public void persistPropertyLengthDistribution() {
        persistPropertyLengthDistribution(true);
        persistPropertyLengthDistribution(false);

        Dataset<PropertyLengthDetailedDT> propertyLengthDTDataset = recordPropertyTable.fetchPropertyLengthDistribution(true);
        SparkComputer.showAll(propertyLengthDTDataset.orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY));

        Dataset<PropertyLengthDetailedDT> propertyLengthDTDataset2 = recordPropertyTable.fetchPropertyLengthDistribution(false);
        SparkComputer.showAll(propertyLengthDTDataset2.orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY));

        Dataset<PropertyLengthDT> p1 = PropertyLengthDT.convert(propertyLengthDTDataset.join(
                propertyLengthDTDataset2.withColumnRenamed(ColumnIdentifier.PROPERTY,"p"),col(ColumnIdentifier.PROPERTY).equalTo(col("p")),SparkComputer.JOIN_TYPES.LEFT_ANTI));
        Dataset<PropertyLengthDT> p2 = PropertyLengthDT.convert(propertyLengthDTDataset2.join(
                propertyLengthDTDataset.withColumnRenamed(ColumnIdentifier.PROPERTY,"p"),col(ColumnIdentifier.PROPERTY).equalTo(col("p")),SparkComputer.JOIN_TYPES.LEFT_ANTI));

        Dataset<PropertyLengthDT> joinedSet = PropertyLengthDT.convert(propertyLengthDTDataset.join(
                propertyLengthDTDataset2.withColumnRenamed(ColumnIdentifier.PROPERTY,"p"),
                col("p").equalTo(col(ColumnIdentifier.PROPERTY))
        ).
                select(propertyLengthDTDataset.col(ColumnIdentifier.CATEGORY),propertyLengthDTDataset.col(ColumnIdentifier.PROPERTY),
                        (propertyLengthDTDataset.col(ColumnIdentifier.MEAN_LENGTH).plus(propertyLengthDTDataset2.col(ColumnIdentifier.MEAN_LENGTH))).divide(2).as(ColumnIdentifier.MEAN_LENGTH),
                        propertyLengthDTDataset.col(ColumnIdentifier.COUNT).plus(propertyLengthDTDataset2.col(ColumnIdentifier.COUNT)).as(ColumnIdentifier.COUNT))).
                union(p1).union(p2);
        SparkComputer.persistDataFrame(joinedSet, ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/joined");
    }

    public Dataset<PropertyLengthDetailedDT> fetchPropertyLengthDistribution(boolean strand) {
        if(!new File(ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/" +strand).exists()) {
            LOGGER.warn("Property Length distribution is not persisted yet. Will be persisted now.");
            persistPropertyLengthDistribution(strand);
        }
        return SparkComputer.read(ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/" +strand,PropertyLengthDetailedDT.ENCODER);
    }

    public Dataset<PropertyLengthDT> fetchPropertyLengthDistribution() {
        if(!new File(ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/joined").exists()) {
            LOGGER.warn("Property Length distribution is not persisted yet. Will be persisted now.");
            persistPropertyLengthDistribution();
        }
        return SparkComputer.read(ProjectDirectoryManager.getPROPERTY_LENGTH_DIR() + "/joined", PropertyLengthDT.ENCODER);
    }

    /****Auxiliary********************************************************************************/

    private static String getStrandIdentifier(boolean strand) {
        return (strand? "plus" : "minus");
    }

    private static String getTableName(boolean strand) {
        return BASE_TABLENAME + getStrandIdentifier(strand) + REFSEQ;
    }

    private static String getIndexName(boolean strand) {
        return INDEX_BASE_NAME + getStrandIdentifier(strand) + REFSEQ;
    }

    private static String getPrimaryKeyConstraintIdentifier(boolean strand) {
        return "RECORD_PROPERTY_" +getStrandIdentifier(strand) +REFSEQ+ "_pkey";
    }

    private static String getVoidFunctionName(boolean strand) {
        return (strand ? VOID_PROPERTIES_PLUS_FUNCTION +REFSEQ: VOID_PROPERTIES_MINUS_FUNCTION+ REFSEQ);
    }

    public static String getREFSEQ() {
        return REFSEQ;
    }

    public static void setREFSEQ(String REFSEQ) {
        RecordPropertyTable.REFSEQ = "_"+REFSEQ;
    }


}

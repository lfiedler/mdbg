package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class StrandedRecordKmerDT extends RecordKmerDT {
    protected boolean strand;
    public static final Encoder<StrandedRecordKmerDT> ENCODER = Encoders.bean(StrandedRecordKmerDT.class);

    public StrandedRecordKmerDT() {
    }

    public StrandedRecordKmerDT(String kmer, int recordId, int recordPosition, int recordLength, boolean strand) {
        super(kmer, recordId, recordPosition, recordLength);
        this.strand = strand;
    }

    public boolean isStrand() {
        return strand;
    }

    public void setStrand(boolean strand) {
        this.strand = strand;
    }
}

package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.mapping;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.GraphConfiguration;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.LaunchExternal;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.parser.SequenceParser;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.GenomeTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.KmersTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordPropertyTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.ColumnIdentifier;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.types.DataTypes;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.spark.sql.functions.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SequenceMapCreator {
    protected static final Logger LOGGER = LoggerFactory.getLogger(SequenceMapCreator.class);
    private final String sequence;
    private final SequenceInformationDT sequenceInformation;
    private static final int RECORD_BUCKET_SIZE = 4;//4000;
    private static final int RECORD_COUNT_SIZE = 4;//4000;
    private static final String K_PLUS_IDENTIFIER = "kPlus";
    private static final String K_MINUS_IDENTIFIER = "kMinus";
    private static final File RECORDS_FILE = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/records.txt");
    private static final int RANGE_BUCKET_SIZE = 4;
    private DbgGraph dbgGraph;
    private boolean persistGenomes;
    private int recordId = -1;
    private ResultDirectoryTree resultDirectoryTree;

    public static class Builder {

        private File fastaFile;
        private int recordId = -1;
        private SequenceInformationDT sequenceInformation;
        private boolean persistGenomes = true;

        public Builder(int recordId) {
            this.recordId = recordId;
            this.sequenceInformation = new SequenceInformationDT();
            RecordDT recordDT = RecordTable.getInstance().getRecord(recordId);
            if(recordDT == null) {
                LOGGER.error("Record " + recordId + " does not exist");
                System.exit(-1);
            }
            setTopology(recordDT.isTopology());
            setTaxId(recordDT.getTaxid());
            sequenceInformation.setId(recordDT.getName());

        }
        public Builder(File fastaFile) {
            this.fastaFile = fastaFile;
            this.sequenceInformation = new SequenceInformationDT();
        }

        public Builder setTopology(boolean topology) {
            sequenceInformation.setTopology(topology);
            return this;
        }

        public Builder setId(String id) {
            sequenceInformation.setId(id);
            return this;
        }

        public Builder setTaxName(String taxName) {
            sequenceInformation.setTaxName(taxName);
            return this;
        }

        public Builder setTaxId(int taxId) {
            sequenceInformation.setTaxId(taxId);
            return this;
        }

        public Builder persistGenomes(boolean persist) {
            this.persistGenomes = persist;
            return this;
        }

        private void inferTaxonomicData() {
            if(sequenceInformation.getTaxId() == -1 || sequenceInformation.getTaxName() == null) {
                TaxonomyGraph taxonomyGraph =
                        (TaxonomyGraph) new Graph.Builder(TaxonomyGraph.KEYSPACE,
                                GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                                startOption(LaunchExternal.LAUNCH_OPTION.START).build();
                if(sequenceInformation.getTaxName() == null) {
                    if(sequenceInformation.getTaxId() == -1) {
                        sequenceInformation.setTaxName(SequenceParser.getScientificName(fastaFile,taxonomyGraph));
                    }
                    else {
                        sequenceInformation.setTaxName(taxonomyGraph.getTaxonomicName(sequenceInformation.getTaxId()));
                    }
                }
                if(sequenceInformation.getTaxId() == -1 && sequenceInformation.getTaxName() != null) {
                    sequenceInformation.setTaxId(
                            taxonomyGraph.getTaxIdForTaxonomicName(sequenceInformation.getTaxName()));
                }
            }
            LOGGER.info("Using sequence Information:\n" + sequenceInformation);
        }

        public SequenceMapCreator build() {
            String sequence = null;
            if(fastaFile != null) {
                if(sequenceInformation.getId() == null) {
                    String id= SequenceParser.getIdFromFastaFile(fastaFile);
                    if(id.contains(".")) {
                        id = id.substring(0,id.indexOf("."));
                    }
                    sequenceInformation.setId(id); }
                if(sequenceInformation.getId() == null) {
                    sequenceInformation.setId(fastaFile.getName().substring(0,fastaFile.getName().indexOf(".")));
                }
                if(sequenceInformation.getTaxId() != -1) {
                    inferTaxonomicData();
                }
                sequence = SequenceParser.parseSequenceFromFasta(fastaFile);
            }
            else {
                try {
                    sequence = GenomeTable.getInstance().getPlusSequence(recordId);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    System.exit(-1);
                }
            }

            LOGGER.info("Creating new sequence mapping with id " + sequenceInformation.getId());

            SequenceMapCreator sequenceAnalyzer = new SequenceMapCreator(sequence, persistGenomes,sequenceInformation);
            if(recordId != -1) {
                sequenceAnalyzer.setRecordId(recordId);
                LOGGER.info("setting recordId to " + recordId);
            }


            return sequenceAnalyzer;
        }
    }

    private SequenceMapCreator(String sequence, boolean persistGenomes, SequenceInformationDT sequenceInformation) {
        this.sequence = sequence;

        this.persistGenomes = persistGenomes;
        this.sequenceInformation = sequenceInformation;
        this.resultDirectoryTree = new ResultDirectoryTree(sequenceInformation.getId(),true);

    }

    private Dataset<Row> getPsqlRecordCounts(Dataset<StrandKmerDT> psqlKmerDataset, boolean psqlStrand) {
        String strandIdentifier = psqlStrand ? K_PLUS_IDENTIFIER : K_MINUS_IDENTIFIER;
        String countIdentifier = psqlStrand ?
                ColumnIdentifier.COUNT_PLUS :
                ColumnIdentifier.COUNT_MINUS;
        return psqlKmerDataset.
                filter(StrandInterface.strandFilter(psqlStrand,StrandKmerDT.class)).
                groupBy(ColumnIdentifier.KMER).
                agg(countDistinct(ColumnIdentifier.RECORD_ID).cast(DataTypes.IntegerType).
                        as(countIdentifier)).
                withColumnRenamed(ColumnIdentifier.KMER,strandIdentifier);
    }

    private Dataset<PositionStrandCountIdentifierDT> createKmerDatasets(
            Dataset<StrandKmerDT> psqlKmerDataset,
            Dataset<PositionCountIdentifierDT> sequenceKmerDataset) {
        return sequenceKmerDataset.
                join(
                    getPsqlRecordCounts(psqlKmerDataset,true),
                    col(ColumnIdentifier.IDENTIFIER).equalTo(col(K_PLUS_IDENTIFIER)),
                    "left").
                join(
                        getPsqlRecordCounts(psqlKmerDataset,false),
                        col(ColumnIdentifier.IDENTIFIER).equalTo(col(K_MINUS_IDENTIFIER)),
                        "left"
                ).
                select(
                        ColumnIdentifier.IDENTIFIER,
                        ColumnIdentifier.POSITION,
                        ColumnIdentifier.COUNT_PLUS,
                        ColumnIdentifier.COUNT_MINUS
                        ).
                as(PositionStrandCountIdentifierDT.ENCODER).
                na().fill(0).as(PositionStrandCountIdentifierDT.ENCODER);

    }



    public static Dataset<Row> createRangesGroupCount(Dataset<PositionStrandCountIdentifierDT> sequenceKmers, boolean strand, int minCount) {
        org.apache.spark.sql.Column countColumn = (strand ? col(ColumnIdentifier.COUNT_PLUS) : col(ColumnIdentifier.COUNT_MINUS));
        return RangeDT.createRangesWithData(
                sequenceKmers.filter(countColumn.gt(minCount)).orderBy(ColumnIdentifier.POSITION),
                max(strand ? ColumnIdentifier.COUNT_PLUS : ColumnIdentifier.COUNT_MINUS));
    }



    private void createKmerDatasets() {

        LOGGER.info("Persisting psql kmers: ");
        KmersTable kmersTable = KmersTable.getInstance();


        final int k =  DbgGraph.K;
        kmersTable.setK(k);
        LOGGER.info("Using k: " + kmersTable.getK());
        final String psqlTableIdentifier = "psql"  + "Kmers" +  sequenceInformation.getId(),
                kmersTableIdentifier = "sequence"  +"Kmers" + sequenceInformation.getId();


        LOGGER.info("Getting sequence kmers");
        // get all kmers of the sequence on the plus strand
        Dataset<PositionCountIdentifierDT> sequenceKmerDataset =
                SparkComputer.createDataFrame(SequenceParser.getMappedKmers(sequence,k,
                        sequenceInformation.isTopology(),true, SequenceParser.AmbigFilter.NO_FILTER),
                        PositionCountIdentifierDT.ENCODER).cache();


        LOGGER.info("Getting record kmers");
        // extract all occuring (dropping duplicates) kmers of the sequence
        List<String> sequenceKmersList = sequenceKmerDataset.
                select(ColumnIdentifier.IDENTIFIER).
                distinct().as(Encoders.STRING()).collectAsList();

        // get psql kmers on both strands
        Dataset<KmerDT> k1 = kmersTable.getKmerMatches(true,sequenceKmersList);
        LOGGER.info("First done");
        Dataset<KmerDT> k2 = kmersTable.getKmerMatches(false,sequenceKmersList);
        LOGGER.info("Second done");
        if(recordId != -1) {
            LOGGER.info("Removing record");
            k1 = k1.filter(col(ColumnIdentifier.RECORD_ID).notEqual(recordId));
            k2 = k2.filter(col(ColumnIdentifier.RECORD_ID).notEqual(recordId));
        }

        Dataset<StrandKmerDT> psqlKmerDataset =
                k1.
                        withColumn(ColumnIdentifier.STRAND, functions.lit(true).cast(DataTypes.BooleanType)).
                        union( k2.
                                withColumn(ColumnIdentifier.STRAND, functions.lit(false).cast(DataTypes.BooleanType))).
                        as(StrandKmerDT.ENCODER);//.cache();

        LOGGER.info("Done generating");
        psqlKmerDataset.write().
                partitionBy(ColumnIdentifier.STRAND).
                bucketBy(RECORD_BUCKET_SIZE,ColumnIdentifier.RECORD_ID).
                option(
                        Spark.PATH_OPTION_KEY,
                        resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.RECORD_KMERS)
                        ).
                saveAsTable(psqlTableIdentifier);

        LOGGER.info("Done");
        LOGGER.info("Persisting Sequence kmers: ");
        // fill counts on both strands appropriately
        Dataset<PositionStrandCountIdentifierDT> sequenceKmers = createKmerDatasets(psqlKmerDataset,sequenceKmerDataset);



        sequenceKmers.write().
        bucketBy(RECORD_COUNT_SIZE,
                        ColumnIdentifier.COUNT_PLUS).
        bucketBy(RECORD_COUNT_SIZE,
                        ColumnIdentifier.COUNT_MINUS).
        sortBy(ColumnIdentifier.POSITION).
        option(
                Spark.PATH_OPTION_KEY,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS)).
        saveAsTable(kmersTableIdentifier);

        LOGGER.info("Done");
        // empty cache
        sequenceKmerDataset.unpersist();
    }

    public static void persistGenome(String sequence, ResultDirectoryTree resultDirectoryTree) {
        LOGGER.info("Persisting genome");


        Dataset<String> genomeDT =
                SparkComputer.createDataFrame(
                        Collections.singletonList(sequence), Encoders.STRING()).
                        withColumnRenamed("value",ResultDirectoryTree.GENOME_IDENTIFIER).
                        as(Encoders.STRING());

        SparkComputer.persistDataFrame( // plus strand
                genomeDT,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.GENOME,true));
        genomeDT = SparkComputer.createDataFrame(
                Collections.singletonList(SequenceParser.reverseComplement(sequence)),  Encoders.STRING()).
                withColumnRenamed("value",ResultDirectoryTree.GENOME_IDENTIFIER).
                as(Encoders.STRING());

        SparkComputer.persistDataFrame( // minus strand
                genomeDT,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.GENOME,false));
        LOGGER.info("Done");
    }

    public void generateParquets() {


        if(persistGenomes) {
            // genome
            LOGGER.info("Persisting genome");


            Dataset<String> genomeDT =
                    SparkComputer.createDataFrame(
                            Collections.singletonList(sequence), Encoders.STRING()).
                            withColumnRenamed("value",ResultDirectoryTree.GENOME_IDENTIFIER).
                            as(Encoders.STRING());

            SparkComputer.persistDataFrame( // plus strand
                    genomeDT,
                    resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.GENOME,true));
            genomeDT = SparkComputer.createDataFrame(
                    Collections.singletonList(SequenceParser.reverseComplement(sequence)),  Encoders.STRING()).
                    withColumnRenamed("value",ResultDirectoryTree.GENOME_IDENTIFIER).
                    as(Encoders.STRING());

            SparkComputer.persistDataFrame( // minus strand
                    genomeDT,
                    resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.GENOME,false));
            LOGGER.info("Done");
        }

        // sequence information
        LOGGER.info("Persisting Sequence information");
        sequenceInformation.setLength(sequence.length());
        Dataset<SequenceInformationDT> sequenceInformationDTDataset =
                SparkComputer.createDataFrame(
                        Collections.singletonList(sequenceInformation),
                        SequenceInformationDT.ENCODER);

        SparkComputer.persistDataFrame(
                sequenceInformationDTDataset,
                resultDirectoryTree.getParquetPath(ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA)
        );
        LOGGER.info("Done");

        LOGGER.info("Creating kmer sets");
        createKmerDatasets();
        LOGGER.info("Done");
    }

    public static List<File> getFiles() {
        List<String> records = getFileNames();
        List<File> files = records.stream().map(s -> new File(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/" + s)).collect(Collectors.toList());
        return files;
    }

    public static List<String> getFileNames() {
        List<String> records = new ArrayList<>();
        FileIO.readSeqFromFile(RECORDS_FILE.getAbsolutePath(),records,false,0,Long.MAX_VALUE,String.class);
        return records;
    }

    public static List<String> getNCFileNames() {
        try (Stream<Path> walk = Files.walk(Paths.get(ProjectDirectoryManager.getGRAPH_FILES_DIR())).
                filter(p -> p.toFile().getName().startsWith("NC"))) {
            return walk.map(path -> path.toFile().getName()).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<File> getNCFiles() {
        try (Stream<Path> walk = Files.walk(Paths.get(ProjectDirectoryManager.getGRAPH_FILES_DIR())).filter(Files::isDirectory).
                filter(p -> p.toFile().getName().startsWith("NC"))) {
            return walk.map(path -> path.toFile()).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void measureRunTime(List<String> ids) {
        List<TimeDT> times = new ArrayList<>();

        StopWatch stopwatch = new StopWatch();
        long counter = ids.size();
        for(String id: ids) {
            LOGGER.info(id);
            int recordId = RecordDT.mapToRecordId(id);
            stopwatch.start();
            SequenceMapCreator sequenceMapCreator = new SequenceMapCreator.Builder(recordId).build();
            sequenceMapCreator.generateParquets();
            LOGGER.info("Done generating");
            stopwatch.stop();
            long dataPersistenceTime = stopwatch.getTime();
            stopwatch.reset();
            stopwatch.start();
            SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);
            sequenceMapAnalyzer.bridgeGaps(0,SequenceMapAnalyzer.Parameter.minRangeLength);
            LOGGER.info("Done bridging");
            stopwatch.stop();
            long gapBridgingTime =stopwatch.getTime();
            stopwatch.reset();
            stopwatch.start();
            sequenceMapAnalyzer.createCluster();
            LOGGER.info("Done creating cluster");
            stopwatch.stop();
            long clusteringTime = stopwatch.getTime();
            stopwatch.reset();
            TimeDT time = new TimeDT(sequenceMapAnalyzer.getSequenceInformation().getId(),
                    sequenceMapAnalyzer.getSequenceInformation().getTaxId(),
                    dataPersistenceTime,gapBridgingTime,clusteringTime);
            times.add(time);
            LOGGER.info(time +"");
            counter--;
            LOGGER.info("Still " + counter);

        }
        SparkComputer.appendToPersistedDataFrame(SparkComputer.createDataFrame(times,TimeDT.ENCODER),
                TimeDT.TIMES_DIR);
    }



    public static void fullRun(List<String> ids) {

        long counter = ids.size();
        for(String id: ids) {
            LOGGER.info(id);
            int recordId = RecordDT.mapToRecordId(id);
            SequenceMapCreator sequenceMapCreator = new SequenceMapCreator.Builder(recordId).build();
            sequenceMapCreator.generateParquets();

            SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);
            sequenceMapAnalyzer.bridgeGaps(3,SequenceMapAnalyzer.Parameter.minRangeLength);
            LOGGER.info("Done bridging");

            sequenceMapAnalyzer.createCluster();
            LOGGER.info("Done creating cluster");

            counter--;
            LOGGER.info("Still " + counter);

        }

    }

    public static void fullRunFromFile(List<File> fastaFiles) {

        long counter = fastaFiles.size();
        for(File fastaFile: fastaFiles) {
            LOGGER.info(fastaFile.getName());


            SequenceMapCreator sequenceMapCreator = new SequenceMapCreator.Builder(fastaFile).build();
            sequenceMapCreator.generateParquets();

            SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(sequenceMapCreator.sequenceInformation.getId());
            sequenceMapAnalyzer.bridgeGaps(3,SequenceMapAnalyzer.Parameter.minRangeLength);
            LOGGER.info("Done bridging");

            sequenceMapAnalyzer.createCluster();
            LOGGER.info("Done creating cluster");

            counter--;
            LOGGER.info("Still " + counter);

        }

    }

    public static void renameFiles() {
        try (Stream<Path> walk = Files.walk(Paths.get(ProjectDirectoryManager.getGRAPH_FILES_DIR())).filter(Files::isDirectory).
                filter(p -> p.toFile().getName().startsWith("NC"))) {
            List<File> files = walk.map(path -> path.toFile()).collect(Collectors.toList());
            for(File file: files) {


//                ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(file.getName(),false);
//                try {
//                    persistGenome(GenomeTable.getInstance().
//                            getPlusSequence(RecordDT.mapToRecordId(file.getName())),resultDirectoryTree);
//                } catch (SQLException throwables) {
//                    throwables.printStackTrace();
//                }
                File f2 = new File(file.getAbsolutePath() + "/parquets/SEQUENCE_DATA.parquet");
                f2.renameTo(new File(file.getAbsolutePath() + "/parquets/SEQUENCE_DATA"));
//                File f3 = new File(file.getAbsolutePath() + "/parquets/PLUS/LONG_SEQUENCE_KMERS");
//                f2.renameTo(new File(file.getAbsolutePath() + "/parquets/RECORD_KMERS"));
//                f3.renameTo(new File(file.getAbsolutePath() + "/parquets/SEQUENCE_KMERS"));
//                new File(file.getAbsolutePath() + "/parquets/PLUS").delete();
//                FileUtils.deleteDirectory(f);
//                FileUtils.deleteDirectory(new File(file.getAbsolutePath() + "/parquets/PLUS"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DbgGraph getDbgGraph() {
        if(dbgGraph == null) {
            dbgGraph = (DbgGraph)
                    new Graph.Builder(DbgGraph.KEYSPACE, GraphConfiguration.CONNECTION_TYPE.STANDARD_OLTP).
                            startOption(LaunchExternal.LAUNCH_OPTION.START).build();
        }
        return dbgGraph;
    }

    private void createMatchGraph(Dataset<PositionCountIdentifierDT> sequenceKmerDataset, boolean strand) {
        Dataset<NodePairPositionDT> nodePairs = NodePairPositionDT.convert(sequenceKmerDataset);
        List<NodePairPositionDT> misMatchedNodePairs = nodePairs.collectAsList();
        // create subgraph of matched nodes and topology edges
        DbgGraph dbgGraph = getDbgGraph();
        GraphTraversalSource g = dbgGraph.createSubGraph(dbgGraph.getTopologyEdgesToNodePairs(misMatchedNodePairs));
        // get all mismatched nodes and add to graph
        List<String> misMatchedNodes = NodePairDT.getDistinctNodes(misMatchedNodePairs);
        g.inject(misMatchedNodes).addV(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier).
                property(SequenceMapGraph.SEQUENCE_NODE_ATTRIBUTES.FILTER_FLAG,SequenceMapGraph.FILTER.NON_MAPPING).iterate();
        // add all sequence edges to graph
        GraphTraversal<?,?> graphTraversal = g.inject(0);
        Iterator<NodePairPositionDT> nodPairIterator = nodePairs.toLocalIterator();
        while (nodPairIterator.hasNext()){
            NodePairPositionDT nodePairPosition = nodPairIterator.next();
            graphTraversal = graphTraversal.addE(DbgGraph.DBG_SUBGRAPH_EDGE_LABEL.SEQUENCE_EDGE.identifier).
                    from(__.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,nodePairPosition.getV1())).
                    to(__.V().has(DbgGraph.DBG_NODE_ATTRIBUTES.KMER.identifier,nodePairPosition.getV2())).
                    property(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,nodePairPosition.getPosition());
        }
        graphTraversal.iterate();

        Graph.writeToGraphML(resultDirectoryTree.getSubGraphName(strand),g);


//        Dataset<String> kPlusOneMersMatchedDataset = SparkComputer.createDataFrame(kPlusOneMersMatched,Encoders.STRING());
//        Dataset<PositionCountIdentifierDT> matchFilteredSequenceKmers = sequenceKmerDataset.join(
//                kPlusOneMersMatchedDataset,
//                col(ColumnIdentifier.VALUE).equalTo(col(ColumnIdentifier.IDENTIFIER)),
//                "inner").drop(ColumnIdentifier.VALUE).as(PositionCountIdentifierDT.ENCODER);
//        Dataset<PositionCountIdentifierDT> misMatchFilteredSequenceKmers =
//                sequenceKmerDataset.except(matchFilteredSequenceKmers);


    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public static void analyzeStrand(File file) {

        String id =file.getName();

        ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
        boolean strand = SequenceMapAnalyzer.identifyStrand(SparkComputer.read(resultDirectoryTree.getParquetPath(
                ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS),
                PositionStrandCountIdentifierDT.ENCODER, PositionStrandCountIdentifierDT.SCHMEA));
        if(! strand) {
            LOGGER.info(id);
        }
    }

    public static void analyzeStrand(List<String> ids) {

        for(String id : ids){
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
//            Dataset<PositionStrandCountIdentifierDT> sequenceKmers =
//                    SparkComputer.read(resultDirectoryTree.getParquetPath(
//                            ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS),
//                            PositionStrandCountIdentifierDT.ENCODER, PositionStrandCountIdentifierDT.SCHMEA);
//            if(sequenceKmers == null || sequenceKmers.isEmpty()) {
//                KmersTable kmersTable = KmersTable.getInstance();
//                sequenceKmerDataset =
//                        SparkComputer.createDataFrame(SequenceParser.getMappedKmers(sequence,k,
//                                sequenceInformation.isTopology(),true, SequenceParser.AmbigFilter.NO_FILTER),
//                                PositionCountIdentifierDT.ENCODER).cache();
//            }
            boolean strand = SequenceMapAnalyzer.identifyStrand(SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS),
                    PositionStrandCountIdentifierDT.ENCODER, PositionStrandCountIdentifierDT.SCHMEA));
            if(! strand) {
                LOGGER.info(id);
            }
        }
    }

    public static void createIfEmpty(List<String> ids) {
        for(String id: ids) {
            LOGGER.info(id);
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            Dataset<PositionStrandCountIdentifierDT> sequenceKmers = SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_KMERS),
                    PositionStrandCountIdentifierDT.ENCODER, PositionStrandCountIdentifierDT.SCHMEA);
            if(sequenceKmers == null || sequenceKmers.isEmpty()) {
                LOGGER.info("Generating");
                int recordId = RecordDT.mapToRecordId(id);
//                System.out.println(id);
                SequenceMapCreator sequenceMapCreator = new SequenceMapCreator.Builder(recordId).build();
                sequenceMapCreator.generateParquets();
            }
        }
    }


    public static void compute(int minCount, int counter, File doneFile, List<String> records) {
        BufferedWriter writer = null;

        System.out.println(records.size());
        System.out.println(doneFile.getAbsoluteFile());

        List<String> recordsDone = new ArrayList<>();
        try{
            writer = new BufferedWriter( new FileWriter(doneFile,true));
            for(String id: records) {
                LOGGER.info("At record " + id + " (" + counter +")") ;
                counter++;
                SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);
                File mappedRangeFile = new File(
                        sequenceMapAnalyzer.getResultDirectoryTree().getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
                if(mappedRangeFile.exists()) {
                    FileUtils.deleteDirectory(mappedRangeFile);
                    System.out.println("deleted mapped ranges");
                }
                File scoredRangeFile = new File(
                        sequenceMapAnalyzer.getResultDirectoryTree().getParquetPath(ResultDirectoryTree.PARQUET_NAME.BRIDGED_RANGES));
                if(scoredRangeFile.exists()) {
                    FileUtils.deleteDirectory(scoredRangeFile);
                    System.out.println("deleted scored ranges");
                }
                sequenceMapAnalyzer.bridgeGaps(minCount,SequenceMapAnalyzer.Parameter.minRangeLength);
                sequenceMapAnalyzer.clean();
                writer.write(id+"\n");
                System.out.println("written");
                recordsDone.add(id);
                writer.flush();
            }
            writer.close();


        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
//            LOGGER.error(e.getMessage());
//            if(!(e instanceof IOException)) {
//                try {
//                    writer.close();
//                    JDBJInterface.reset();
//                    Spark.reset();
//                    records.removeAll(recordsDone);
//                    compute(minCount,counter,doneFile,records);
//                } catch (IOException ioException) {
//                    ioException.printStackTrace();
//                }
//
//            }



        }
    }

    public static void run(int counter, File doneFile, List<String> records, Consumer<SequenceMapAnalyzer> computeRoutine) {
        BufferedWriter writer = null;

        System.out.println(records.size());
        System.out.println(doneFile.getAbsoluteFile());

        List<String> recordsDone = new ArrayList<>();
        try{
            writer = new BufferedWriter( new FileWriter(doneFile,true));
            for(String id: records) {
                LOGGER.info("At record " + id + " (" + counter +")") ;
                counter++;
                SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);

                computeRoutine.accept(sequenceMapAnalyzer);
                sequenceMapAnalyzer.clean();
                writer.write(id+"\n");
                System.out.println("written");
                recordsDone.add(id);
                writer.flush();
            }
            writer.close();


        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
//            LOGGER.error(e.getMessage());
//            if(!(e instanceof IOException)) {
//                try {
//                    writer.close();
//                    JDBJInterface.reset();
//                    Spark.reset();
//                    records.removeAll(recordsDone);
//                    compute(minCount,counter,doneFile,records);
//                } catch (IOException ioException) {
//                    ioException.printStackTrace();
//                }
//
//            }



        }
    }

    public static void createCluster(int counter, File doneFile, List<String> records) {
        run(counter,doneFile,records,sequenceMapAnalyzer -> sequenceMapAnalyzer.createCluster());
    }

    public static void computeCluster(int counter, File doneFile, List<String> records) {
        run(counter,doneFile,records,sequenceMapAnalyzer -> sequenceMapAnalyzer.computeCluster());
    }

    public static void compute(int amount,int minCount) {
        Dataset<Row> dataset = RecordMapAnalyzer.fetchRecordEdgeWeightStatistics(true);
        List<String> records = null;
        try {
            records = new ArrayList<>(dataset.join(RecordTable.getInstance().getTableEntries().
                    withColumnRenamed(ColumnIdentifier.RECORD_ID,"r"),col("r").equalTo(col(ColumnIdentifier.RECORD_ID))).
                    orderBy(desc(ColumnIdentifier.WEIGHT_MEAN)).
                    select(ColumnIdentifier.NAME).as(Encoders.STRING()).collectAsList());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        List<String> doneRecords = new ArrayList<>();
        File doneFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/done.txt");
        FileIO.readSeqFromFile(doneFile.getAbsolutePath(),doneRecords,false,0,Long.MAX_VALUE,String.class);
        System.out.println(doneRecords);
        records.removeAll(doneRecords);

        System.out.println(records.size());
        int counter = 0;

        records = records.subList(0,Math.min(records.size(),amount));
        compute(minCount,0,doneFile,records);
//        BufferedWriter writer = null;
//        System.out.println(records.size());
//        try{
//            writer = new BufferedWriter( new FileWriter(doneFile,true));
//            for(String id: records) {
//                LOGGER.info("At record " + id + " (" + counter +")") ;
//                counter++;
//                SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);
//                File mappedRangeFile = new File(sequenceMapAnalyzer.getResultDirectoryTree().getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                if(mappedRangeFile.exists()) {
//                    FileUtils.deleteDirectory(mappedRangeFile);
//                    System.out.println("deleted mapped ranges");
//                }
//                sequenceMapAnalyzer.bridgeGaps();
//                writer.write(id+"\n");
//                writer.flush();
//            }
//            writer.close();
//
//
//        } catch (Exception e) {
//            if(!(e instanceof IOException)) {
//                try {
//                    writer.close();
//                } catch (IOException ioException) {
//                    ioException.printStackTrace();
//                }
//
//            }
//
//            LOGGER.warn(e.getMessage());
//
//        }
    }

    public static void computeTaxonomicGroup(int amount,int minCount, File file) {
        List<String> records = new ArrayList<>();
        FileIO.readSeqFromFile(file.getAbsolutePath(),records,false,0,Long.MAX_VALUE,String.class);
        List<String> doneRecords = new ArrayList<>();
        File doneFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/done.txt");
        FileIO.readSeqFromFile(doneFile.getAbsolutePath(),doneRecords,false,0,Long.MAX_VALUE,String.class);
//        System.out.println(doneRecords);
        records.removeAll(doneRecords);

        System.out.println(records.size());
        int counter = 0;

        records = records.subList(0,Math.min(records.size(),amount));
        LOGGER.info("Computing: " + records);
        compute(minCount,0,doneFile,records);
//        BufferedWriter writer = null;
//        System.out.println(records.size());
//        try{
//            writer = new BufferedWriter( new FileWriter(doneFile,true));
//            for(String id: records) {
//                LOGGER.info("At record " + id + " (" + counter +")") ;
//                counter++;
//                SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(id);
//                File mappedRangeFile = new File(sequenceMapAnalyzer.getResultDirectoryTree().getParquetPath(ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES));
//                if(mappedRangeFile.exists()) {
//                    FileUtils.deleteDirectory(mappedRangeFile);
//                    System.out.println("deleted mapped ranges");
//                }
//                sequenceMapAnalyzer.bridgeGaps();
//                writer.write(id+"\n");
//                writer.flush();
//            }
//            writer.close();
//
//
//        } catch (Exception e) {
//            if(!(e instanceof IOException)) {
//                try {
//                    writer.close();
//                } catch (IOException ioException) {
//                    ioException.printStackTrace();
//                }
//
//            }
//
//            LOGGER.warn(e.getMessage());
//
//        }
    }

    public static void run(int amount, File inFile, File outFile, Consumer<SequenceMapAnalyzer> computeRoutine) {
        List<String> records = new ArrayList<>();
        FileIO.readSeqFromFile(inFile.getAbsolutePath(),records,false,0,Long.MAX_VALUE,String.class);
        List<String> doneRecords = new ArrayList<>();
        try {
            outFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        FileIO.readSeqFromFile(outFile.getAbsolutePath(),doneRecords,false,0,Long.MAX_VALUE,String.class);
//        System.out.println(doneRecords);
        records.removeAll(doneRecords);

        System.out.println(records.size());

        records = records.subList(0,Math.min(records.size(),amount));
        LOGGER.info("Computing: " + records);

        run(0,outFile,records,computeRoutine);
    }

    public static void getClustersBelowAlphaStat() {
        List<String> ids = getFileNames();

        Cluster cluster = new Cluster();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            System.out.println(id);
            for(boolean strand : Arrays.asList(true,false)) {
                System.out.println("Strand: " + strand);
                cluster.setG(resultDirectoryTree.getPropertyClusterTreeFile(strand));
                Cluster.getRootBelowAlphaStatistic(cluster.g,TreeNode.alphaValue.doubleValue());
            }
            System.out.println();


        }
    }


    public static void checkProperties() {
        RecordPropertyTable recordPropertyTable = RecordPropertyTable.getInstance();
        List<String> ids = getFileNames();
        for(String id: ids) {
            for(boolean refSeqStrand : Arrays.asList(true,false)) {
                Dataset<PropertyRangeDT> propertyRangeDTDataset =
                        recordPropertyTable.getPropertyRanges(refSeqStrand,RecordDT.mapToRecordId(id)).
                                filter(col(ColumnIdentifier.PROPERTY).isInCollection(Arrays.asList("S","S1","S2","L","L1","L2")));
                if(!propertyRangeDTDataset.isEmpty()) {
                    System.out.println(id);
                    SparkComputer.showAll(propertyRangeDTDataset);
                }
            }

        }
    }

    public static void showPropertyAnnotation(double alphaValue, String id) {
        System.out.println(id);
        System.out.println("alpha="+ alphaValue);
        System.out.println("Positive strand");
        showPropertyAnnotation(alphaValue,id,true);
        System.out.println("Negative strand");
        showPropertyAnnotation(alphaValue,id,false);
    }
    public static void showPropertyAnnotation(double alphaValue, String id, boolean strand) {

        ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
        resultDirectoryTree.setAnnotationFiles(alphaValue);


        Dataset<PropertyRangeProbDT> propertyRangesAnnotated =
                SparkComputer.read(resultDirectoryTree.getAnnotationFile(strand).getAbsolutePath(),PropertyRangeProbDT.ENCODER);


//        System.out.println("Annotation Inverse: ");
//        SparkComputer.showAll(propertyRangesAnnotated.withColumn(ColumnIdentifier.INVERSE_START,
//                lag(col(ColumnIdentifier.END),1,-1).
//                        over(START_FRAME).plus(1)).
//                withColumn(ColumnIdentifier.INVERSE_END,
//                        when(col(ColumnIdentifier.START).gt(0),
//                                col(ColumnIdentifier.START).minus(1)).otherwise(-1)).
//                filter(col(ColumnIdentifier.INVERSE_END).gt(-1)).
//                select(
//                        col(ColumnIdentifier.INVERSE_START).as(ColumnIdentifier.START),
//                        col(ColumnIdentifier.INVERSE_END).as(ColumnIdentifier.END),
//                        col(ColumnIdentifier.CATEGORY),
//                        col(ColumnIdentifier.PROPERTY),col(ColumnIdentifier.PROBABILITY)).
//                withColumn(ColumnIdentifier.LENGTH,col(ColumnIdentifier.END).minus(col(ColumnIdentifier.START)).plus(1)).
//                orderBy(ColumnIdentifier.PROPERTY,ColumnIdentifier.START,ColumnIdentifier.END));

        SparkComputer.showAll(propertyRangesAnnotated.
//                filter(col(ColumnIdentifier.PROPERTY).equalTo("nad6")).
                filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("trna","rrna","protein"))).

        select(col(ColumnIdentifier.START),col(ColumnIdentifier.END),col(ColumnIdentifier.LENGTH),
        col(ColumnIdentifier.CATEGORY),substring_index( col(ColumnIdentifier.PROPERTY), "_",1).as("gene"),col(ColumnIdentifier.PROBABILITY)).
//                filter(col(ColumnIdentifier.PROBABILITY).equalTo(1)).
        orderBy(ColumnIdentifier.PROPERTY,ColumnIdentifier.START,ColumnIdentifier.END));




    }
    public static void showPropertyAnnotationBoth(double alphaValue, String id, boolean strand) {

        RecordPropertyTable recordPropertyTable = RecordPropertyTable.getInstance();
        Dataset<PropertyRangeProbDT> propertyRangesRefseq = PropertyRangeProbDT.convert(
                recordPropertyTable.getPropertyRanges(strand,RecordDT.mapToRecordId(id)));

        System.out.println("Annotation: ");
        showPropertyAnnotation(alphaValue,id,strand);

        System.out.println("Refseq");

        SparkComputer.showAll(propertyRangesRefseq.
                filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("trna","rrna","protein"))).
                select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).
                orderBy(ColumnIdentifier.PROPERTY,ColumnIdentifier.START,ColumnIdentifier.END));


    }



    public static Dataset<JaccardStatisticIdDT> getMergedStrandSwitchJaccardStatistic(double alphaValue, boolean strand) {
        List<String> ids = getFileNames();
        List<Dataset<JaccardStatisticIdDT>> agreements = new ArrayList<>();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAgreementFiles(alphaValue);
            Dataset<JaccardStatisticDT> j1 =
                    SparkComputer.read(resultDirectoryTree.getAggreementFile(strand,!strand).getAbsolutePath(),
                        JaccardStatisticDT.ENCODER).filter(col(ColumnIdentifier.JACCARD_INDEX).geq(JaccardStatisticIdDT.THRESHOLD));
            if(!j1.isEmpty()) {
                agreements.add(j1.
                        withColumn(ColumnIdentifier.RECORD_ID,lit(RecordDT.mapToRecordId(id)).cast(DataTypes.IntegerType)).as(JaccardStatisticIdDT.ENCODER));
            }


        }
        return SparkComputer.union(agreements,JaccardStatisticIdDT.ENCODER);
    }

    public static Dataset<JaccardStatisticTaxonomyDT> getMergedStrandSwitchJaccardStatisticTaxonomicGroup(double alphaValue, boolean strand) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);
        Dataset<JaccardStatisticIdDT> merged = getMergedStrandSwitchJaccardStatistic(alphaValue,strand);
        if(merged == null) {
            return null;
        }

        return merged.join(taxonomicGroupDataset,merged.col(ColumnIdentifier.RECORD_ID).
                equalTo(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.INNER).
                drop(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)).as(JaccardStatisticTaxonomyDT.ENCODER);
    }


    public static void analyzeOtherPropertyCandidates(double alphaValue, List<String> ids,List<String> properties) {
        Cluster cluster = new Cluster();
        for(String id: ids) {
            System.out.println(id);
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAnnotationFiles(alphaValue);

            for(boolean strand: Arrays.asList(true,false)) {

                System.out.println(strand);
                cluster.setG(resultDirectoryTree.getPropertyClusterTreeFile(strand));
                Cluster.analyzeOtherCandidates(cluster.g,alphaValue,properties);

//                Map<String, List<PropertyRangeDT>> m = propertyRangesAnnotated.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
//                if(m.containsKey("S") && (m.containsKey("S1") || m.containsKey("S2"))) {
//                    System.out.println(id  + " " + strand);
//                    Auxiliary.printSeq(m.get("S"));
//                    if(m.containsKey("S1")) {
//                        Auxiliary.printSeq(m.get("S1"));
//                    }
//                    if(m.containsKey("S2")) {
//                        Auxiliary.printSeq(m.get("S2"));
//                    }
//                }
//                if(m.containsKey("L") && (m.containsKey("L1") || m.containsKey("L2"))) {
//                    Auxiliary.printSeq(m.get("L"));
//                    if(m.containsKey("L1")) {
//                        Auxiliary.printSeq(m.get("L1"));
//                    }
//                    if(m.containsKey("L2")) {
//                        Auxiliary.printSeq(m.get("L2"));
//                    }
//                }


            }
            System.out.println();
        }
    }

    public static void showBesPropertyAnnotation(List<String> ids) {
        Cluster cluster = new Cluster();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);

            SequenceInformationDT sequenceInformationDT =  SparkComputer.getFirstOrElseNull(SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA),SequenceInformationDT.ENCODER));
            for(boolean strand: Arrays.asList(true,false)) {

                System.out.println(strand);
                cluster.setG(resultDirectoryTree.getPropertyClusterTreeFile(strand));
                Dataset<PropertyRangeProbDT> propertyRangesAnnotated =
                        SparkComputer.createDataFrame(Cluster.extractBestPropertiesProperties(cluster.g, sequenceInformationDT.getLength()),PropertyRangeProbDT.ENCODER).
                                filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("trna","rrna","protein"))).
                                select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).
                                as(PropertyRangeProbDT.ENCODER);
                SparkComputer.showAll(propertyRangesAnnotated.orderBy(ColumnIdentifier.PROPERTY,ColumnIdentifier.START,ColumnIdentifier.END));
//                SparkComputer.persistOverwriteDataFrame(
//                        propertyRangesAnnotated,
//                        resultDirectoryTree.getAnnotationFile(strand).getAbsolutePath());

//                Map<String, List<PropertyRangeDT>> m = propertyRangesAnnotated.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
//                if(m.containsKey("S") && (m.containsKey("S1") || m.containsKey("S2"))) {
//                    System.out.println(id  + " " + strand);
//                    Auxiliary.printSeq(m.get("S"));
//                    if(m.containsKey("S1")) {
//                        Auxiliary.printSeq(m.get("S1"));
//                    }
//                    if(m.containsKey("S2")) {
//                        Auxiliary.printSeq(m.get("S2"));
//                    }
//                }
//                if(m.containsKey("L") && (m.containsKey("L1") || m.containsKey("L2"))) {
//                    Auxiliary.printSeq(m.get("L"));
//                    if(m.containsKey("L1")) {
//                        Auxiliary.printSeq(m.get("L1"));
//                    }
//                    if(m.containsKey("L2")) {
//                        Auxiliary.printSeq(m.get("L2"));
//                    }
//                }


            }
        }
    }

    public static void persistPropertyAnnotations(double alphaValue, List<String> ids) {
        Cluster cluster = new Cluster();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAnnotationFiles(alphaValue);
            SequenceInformationDT sequenceInformationDT =  SparkComputer.getFirstOrElseNull(SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA),SequenceInformationDT.ENCODER));
            for(boolean strand: Arrays.asList(true,false)) {

                cluster.setG(resultDirectoryTree.getPropertyClusterTreeFile(strand));
                Dataset<PropertyRangeProbDT> propertyRangesAnnotated =
                        SparkComputer.createDataFrame(Cluster.createAnnotatedProperties(cluster.g, alphaValue,sequenceInformationDT.getLength()),PropertyRangeProbDT.ENCODER).
                                filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("trna","rrna","protein"))).
                                select(ColumnIdentifier.START,ColumnIdentifier.END,ColumnIdentifier.LENGTH,
                                        ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROBABILITY).
                                as(PropertyRangeProbDT.ENCODER);
                SparkComputer.persistOverwriteDataFrame(
                        propertyRangesAnnotated,
                        resultDirectoryTree.getAnnotationFile(strand).getAbsolutePath());

//                Map<String, List<PropertyRangeDT>> m = propertyRangesAnnotated.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty));
//                if(m.containsKey("S") && (m.containsKey("S1") || m.containsKey("S2"))) {
//                    System.out.println(id  + " " + strand);
//                    Auxiliary.printSeq(m.get("S"));
//                    if(m.containsKey("S1")) {
//                        Auxiliary.printSeq(m.get("S1"));
//                    }
//                    if(m.containsKey("S2")) {
//                        Auxiliary.printSeq(m.get("S2"));
//                    }
//                }
//                if(m.containsKey("L") && (m.containsKey("L1") || m.containsKey("L2"))) {
//                    Auxiliary.printSeq(m.get("L"));
//                    if(m.containsKey("L1")) {
//                        Auxiliary.printSeq(m.get("L1"));
//                    }
//                    if(m.containsKey("L2")) {
//                        Auxiliary.printSeq(m.get("L2"));
//                    }
//                }


            }
        }


    }

    public static void computePropertyAgreement(double alphaValue, List<String> ids) {

        RecordPropertyTable recordPropertyTable = RecordPropertyTable.getInstance();

        for(String id: ids) {

//            System.out.println(id);
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAgreementFiles(alphaValue);
            resultDirectoryTree.setAnnotationFiles(alphaValue);
            SequenceInformationDT sequenceInformationDT =  SparkComputer.getFirstOrElseNull(SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA),SequenceInformationDT.ENCODER));
//            System.out.println("Length: " + sequenceInformationDT.getLength());

            Dataset<PropertyRangeProbDT> propertyRangesAnnotatedPlus =
                    SparkComputer.read(resultDirectoryTree.getAnnotationFile(true).getAbsolutePath(),
                            PropertyRangeProbDT.ENCODER);

            Dataset<PropertyRangeProbDT> propertyRangesAnnotatedMinus =
                    SparkComputer.read(resultDirectoryTree.getAnnotationFile(false).getAbsolutePath(),
                            PropertyRangeProbDT.ENCODER);

            List<PropertyRangeProbStrandDT> propertyRangesAnnotated =
                    PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedPlus,true).
                    union(PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedMinus,false)).collectAsList();
            // filter too small attribute annotations
//            List<String> filterAttributes = propertyRangesAnnotated.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty,Collectors.summingInt(PropertyRangeDT::getLength))).
//                    entrySet().stream().filter(e -> e.getValue() <= minSize).map(e -> e.getKey()).collect(Collectors.toList());
//            propertyRangesAnnotated = propertyRangesAnnotated.stream().filter(p -> !filterAttributes.contains(p.getProperty())).collect(Collectors.toList());
//            System.out.println("Annotated");
//            Collections.sort(propertyRangesAnnotated);
//            Auxiliary.printSeq(propertyRangesAnnotated);

            List<PropertyRangeProbDT> propertyRangesRefseqPlus =PropertyRangeProbDT.convert(
                    recordPropertyTable.getPropertyRanges(true,RecordDT.mapToRecordId(id))).
                    collectAsList();
            PropertyRangeDT.groupProperties(propertyRangesRefseqPlus,sequenceInformationDT.getLength());
            List<PropertyRangeProbDT> propertyRangesRefseqMinus = PropertyRangeProbDT.convert(
                    recordPropertyTable.getPropertyRanges(false,RecordDT.mapToRecordId(id))).
                    collectAsList();
            PropertyRangeDT.groupProperties(propertyRangesRefseqMinus,sequenceInformationDT.getLength());

            List<PropertyRangeProbStrandDT> propertyRangesRefSeq =
                    PropertyRangeProbStrandDT.convert(SparkComputer.createDataFrame(propertyRangesRefseqPlus,PropertyRangeProbDT.ENCODER), true).
                            union(PropertyRangeProbStrandDT.convert(SparkComputer.createDataFrame(propertyRangesRefseqMinus,PropertyRangeProbDT.ENCODER), false)).collectAsList();
//            System.out.println("Refseq");
//            Auxiliary.printSeq(propertyRangesRefSeq);
            Dataset<AlternativeJaccardStatisticDT> jaccardStatisticDTDataset =
                    SparkComputer.createDataFrame(
                            PropertyRangeDT.compareAgreementJaccard(propertyRangesAnnotated,propertyRangesRefSeq),
                            AlternativeJaccardStatisticDT.ENCODER);
            SparkComputer.persistOverwriteDataFrame(
                    jaccardStatisticDTDataset,
                    resultDirectoryTree.getAggreementFile().getAbsolutePath());

        }


    }

    public static void computePropertyAgreementBed(double alphaValue, List<String> ids) {


        final int minSize = 20;


        final double associationThreshold = 0.75;

        for(String id: ids) {

//            System.out.println(id);
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAgreementFiles(alphaValue);
            resultDirectoryTree.setAnnotationFiles(alphaValue);
            SequenceInformationDT sequenceInformationDT =  SparkComputer.getFirstOrElseNull(SparkComputer.read(resultDirectoryTree.getParquetPath(
                    ResultDirectoryTree.PARQUET_NAME.SEQUENCE_DATA),SequenceInformationDT.ENCODER));
//            System.out.println("Length: " + sequenceInformationDT.getLength());

            Dataset<PropertyRangeProbDT> propertyRangesAnnotatedPlus =
                    SparkComputer.read(resultDirectoryTree.getAnnotationFile(true).getAbsolutePath(),
                            PropertyRangeProbDT.ENCODER);

            Dataset<PropertyRangeProbDT> propertyRangesAnnotatedMinus =
                    SparkComputer.read(resultDirectoryTree.getAnnotationFile(false).getAbsolutePath(),
                            PropertyRangeProbDT.ENCODER);

            List<PropertyRangeProbStrandDT> propertyRangesAnnotated =
                    PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedPlus,true).
                            union(PropertyRangeProbStrandDT.convert(propertyRangesAnnotatedMinus,false)).collectAsList();
            // filter too small attribute annotations
//            List<String> filterAttributes = propertyRangesAnnotated.stream().collect(Collectors.groupingBy(PropertyRangeDT::getProperty,Collectors.summingInt(PropertyRangeDT::getLength))).
//                    entrySet().stream().filter(e -> e.getValue() <= minSize).map(e -> e.getKey()).collect(Collectors.toList());
//            propertyRangesAnnotated = propertyRangesAnnotated.stream().filter(p -> !filterAttributes.contains(p.getProperty())).collect(Collectors.toList());
//            System.out.println("Annotated");
//            Collections.sort(propertyRangesAnnotated);
//            Auxiliary.printSeq(propertyRangesAnnotated);


            List<PropertyRangeProbStrandDT> propertyRangesRefSeq =
                SequenceParser.parseFeaturesFromBEDToPropertyRanges(new File(ProjectDirectoryManager.getSEQUENCE_DATA() + "/refseq204/bed/" +
                        FileIO.getFileNamesStartWith(ProjectDirectoryManager.getSEQUENCE_DATA() + "/refseq204/bed",id).get(0)),sequenceInformationDT.getLength());
//            System.out.println("Refseq");
//            Auxiliary.printSeq(propertyRangesRefSeq);
            Dataset<AlternativeJaccardStatisticDT> jaccardStatisticDTDataset =
                    SparkComputer.createDataFrame(
                            PropertyRangeDT.compareAgreementJaccard(propertyRangesAnnotated,propertyRangesRefSeq),
                            AlternativeJaccardStatisticDT.ENCODER);
            SparkComputer.persistOverwriteDataFrame(
                    jaccardStatisticDTDataset,
                    resultDirectoryTree.getAggreementFile().getAbsolutePath());

        }


    }


    public static void showAgreement(double alphaValue, String id) {
        ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
        resultDirectoryTree.setAgreementFiles(alphaValue);

        SparkComputer.showAll(SparkComputer.read(
                resultDirectoryTree.getAggreementFile().getAbsolutePath(),
                JaccardStatisticDT.ENCODER).orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.JACCARD_INDEX));
    }

    public static void compareAgreement(double alpha1, double alpha2, String id) {
        final String alphaIdentifier = "alphaIndentifier";
        ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
        resultDirectoryTree.setAgreementFiles(alpha1);
        Dataset<JaccardStatisticDT> j1 = SparkComputer.read(
                resultDirectoryTree.getAggreementFile().getAbsolutePath(),
                JaccardStatisticDT.ENCODER).filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("protein","trna","rrna"))).
//                filter(not( col(ColumnIdentifier.PROPERTY).isInCollection(new ArrayList<>(Arrays.asList("S1","S2","S","L","L1","L2"))) )).
                orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.JACCARD_INDEX);
        resultDirectoryTree.setAgreementFiles(alpha2);
//        SparkComputer.showAll(j1.orderBy(ColumnIdentifier.CATEGORY));
        Dataset<JaccardStatisticDT> j2 = SparkComputer.read(
                resultDirectoryTree.getAggreementFile().getAbsolutePath(),
                JaccardStatisticDT.ENCODER).filter(col(ColumnIdentifier.CATEGORY).isInCollection(Arrays.asList("protein","trna","rrna"))).
//                filter(not( col(ColumnIdentifier.PROPERTY).isInCollection(new ArrayList<>(Arrays.asList("S1","S2","S","L","L1","L2"))) )).
                orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.JACCARD_INDEX);
//        SparkComputer.showAll(j2.orderBy(ColumnIdentifier.CATEGORY));
        Dataset<Row> comparedAgreement = j1.withColumn(alphaIdentifier,lit(alpha1)).
                join(j2.withColumn(alphaIdentifier,lit(alpha2)).
                                withColumnRenamed(ColumnIdentifier.JACCARD_INDEX,ColumnIdentifier.JACCARD_INDEX + "2").
                                withColumnRenamed(ColumnIdentifier.CATEGORY,ColumnIdentifier.CATEGORY+"2").
                                withColumnRenamed(ColumnIdentifier.PROPERTY,ColumnIdentifier.PROPERTY+"_2").
                                withColumnRenamed(ColumnIdentifier.PROPERTY2,ColumnIdentifier.PROPERTY2+"_2")
                        ,col(ColumnIdentifier.PROPERTY).equalTo(col(ColumnIdentifier.PROPERTY+"_2")).
                        and(j1.col(ColumnIdentifier.STRAND).equalTo(j2.col(ColumnIdentifier.STRAND)))
                        ,"full").orderBy(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY);
//        SparkComputer.showAll(comparedAgreement.filter(col(ColumnIdentifier.CATEGORY).equalTo("trna")).orderBy(ColumnIdentifier.JACCARD_INDEX));
        Dataset<Row> result = comparedAgreement.
                withColumn("abovePrev",col(ColumnIdentifier.JACCARD_INDEX ).geq(0.7)).
                withColumn("above",col(ColumnIdentifier.JACCARD_INDEX + "2").geq(0.7)).
                withColumn("changed",col("abovePrev").equalTo(false).and(col("above").equalTo(true))).
                withColumn("diff", col(ColumnIdentifier.JACCARD_INDEX + "2").minus(greatest(col(ColumnIdentifier.JACCARD_INDEX),lit(0)))).
                withColumn("improved",col(ColumnIdentifier.JACCARD_INDEX + "2").geq(col(ColumnIdentifier.JACCARD_INDEX))).orderBy(j1.col(ColumnIdentifier.CATEGORY)).
                filter(col("improved").equalTo(true)).select(ColumnIdentifier.CATEGORY,ColumnIdentifier.PROPERTY,ColumnIdentifier.PROPERTY2, ColumnIdentifier.PROPERTY+"_2",ColumnIdentifier.PROPERTY2+"_2",
                ColumnIdentifier.JACCARD_INDEX,ColumnIdentifier.JACCARD_INDEX+"2","abovePrev","above","changed","diff");

        SparkComputer.showAll(
            result
        );
//        SparkComputer.writeToCSV(result,ProjectDirectoryManager.getSTATISTICS_DIR() + "/alpha60To45_" + id );

        SparkComputer.showAll(result.filter(col("abovePrev").equalTo(false)).groupBy(ColumnIdentifier.CATEGORY,"changed").agg(count("changed")).orderBy(ColumnIdentifier.CATEGORY));
    }

    public static Dataset<JaccardStatisticIdDT> getMergedJaccardStatistic(double alphaValue, boolean strand) {
        List<String> ids = getFileNames();
        List<Dataset<JaccardStatisticIdDT>> agreements = new ArrayList<>();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAgreementFiles(alphaValue);
            agreements.add(
                    SparkComputer.read(
                                    resultDirectoryTree.getAggreementFile(strand,strand).getAbsolutePath(),
                            JaccardStatisticDT.ENCODER).
                            withColumn(ColumnIdentifier.RECORD_ID,lit(RecordDT.mapToRecordId(id)).cast(DataTypes.IntegerType)).as(JaccardStatisticIdDT.ENCODER)
            );

        }
        return SparkComputer.union(agreements,JaccardStatisticIdDT.ENCODER);
    }

    public static Dataset<AlternativeJaccardStatisticIdDT> getJaccardStatistic(double alphaValue,String id) {
        ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
        resultDirectoryTree.setAgreementFiles(alphaValue);
        return SparkComputer.read(
                resultDirectoryTree.getAggreementFile().getAbsolutePath(),
                AlternativeJaccardStatisticDT.ENCODER).
                withColumn(ColumnIdentifier.RECORD_ID,lit(RecordDT.mapToRecordId(id)).cast(DataTypes.IntegerType)).
                as(AlternativeJaccardStatisticIdDT.ENCODER);
    }
    public static Dataset<AlternativeJaccardStatisticIdDT> getMergedJaccardStatistic(double alphaValue, List<String> ids ) {

        List<Dataset<AlternativeJaccardStatisticIdDT>> agreements = new ArrayList<>();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id,false);
            resultDirectoryTree.setAgreementFiles(alphaValue);
            agreements.add(
                    getJaccardStatistic(alphaValue,id)
            );

        }
        return SparkComputer.union(agreements,AlternativeJaccardStatisticIdDT.ENCODER);
    }
    public static Dataset<AlternativeJaccardStatisticIdDT> getMergedJaccardStatistic(double alphaValue) {
        List<String> ids = getFileNames();

        return getMergedJaccardStatistic(alphaValue,ids);
    }

    public static Dataset<JaccardStatisticTaxonomyDT> getMergedJaccardStatisticTaxonomicGroup(double alphaValue, boolean strand) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);
        Dataset<JaccardStatisticIdDT> merged = getMergedJaccardStatistic(alphaValue,strand);

        return merged.join(taxonomicGroupDataset,merged.col(ColumnIdentifier.RECORD_ID).
                equalTo(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.INNER).
                drop(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)).as(JaccardStatisticTaxonomyDT.ENCODER);
    }

    public static Dataset<AlternativeJaccardStatisticTaxonomyDT> getMergedJaccardStatisticTaxonomicGroup(double alphaValue) {

        return getMergedJaccardStatisticTaxonomicGroup(alphaValue,getFileNames());
    }

    public static Dataset<AlternativeJaccardStatisticTaxonomyDT> getMergedJaccardStatisticTaxonomicGroup(double alphaValue, List<String> ids) {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);
        Dataset<AlternativeJaccardStatisticIdDT> merged = getMergedJaccardStatistic(alphaValue,ids);

        return merged.join(taxonomicGroupDataset,merged.col(ColumnIdentifier.RECORD_ID).
                equalTo(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)),SparkComputer.JOIN_TYPES.INNER).
                drop(taxonomicGroupDataset.col(ColumnIdentifier.RECORD_ID)).as(AlternativeJaccardStatisticTaxonomyDT.ENCODER);
    }

    public static String getMergedJaccardStatisticPath(double alphaValue) {

        return ProjectDirectoryManager.getGRAPH_DATA_DIR()+ "/MERGED_JACCARD_STATISTIC/" +
                ((int)(alphaValue*100)) ;
    }


    public static String getMergedJaccardStatisticPath(double alphaValue, boolean strand, boolean alternative) {
        if(alternative) {
            return ProjectDirectoryManager.getGRAPH_DATA_DIR()+ "/MERGED_ALTERNATIVE_JACCARD_STATISTIC/" +
                    ((int)(alphaValue*100)) + "/" + (strand ? "plusStrand" : "minusStrand" );
        }
        return ProjectDirectoryManager.getGRAPH_DATA_DIR()+ "/MERGED_JACCARD_STATISTIC/" +
                ((int)(alphaValue*100)) + "/" + (strand ? "plusStrand" : "minusStrand" );
    }


    public static void persistMergedJaccardStatisticTaxonomicGroup(double alphaValue) {

        Dataset<AlternativeJaccardStatisticTaxonomyDT> dataset = getMergedJaccardStatisticTaxonomicGroup(alphaValue);
        SparkComputer.
                persistOverwriteDataFrame(dataset,
                        getMergedJaccardStatisticPath(alphaValue) + SparkComputer.PARQUET_PATH);
        SparkComputer.
                writeToCSV(dataset.drop(ColumnIdentifier.POSITIONS1,ColumnIdentifier.POSITIONS2),
                        getMergedJaccardStatisticPath(alphaValue) + "/CSV");

    }

    public static void persistMergedJaccardStatisticTaxonomicGroup(double alphaValue, List<String> ids) {

        Dataset<AlternativeJaccardStatisticTaxonomyDT> dataset = getMergedJaccardStatisticTaxonomicGroup(alphaValue,ids);
        SparkComputer.
                persistOverwriteDataFrame(dataset,
                        getMergedJaccardStatisticPath(alphaValue) + SparkComputer.PARQUET_PATH);
        SparkComputer.
                writeToCSV(dataset.drop(ColumnIdentifier.POSITIONS1,ColumnIdentifier.POSITIONS2),
                        getMergedJaccardStatisticPath(alphaValue) + "/CSV");

    }



    public static void evaluateCategoryAgreement(double alphaValue) {

        for(boolean strand : Arrays.asList(true,false)) {
            Dataset<JaccardStatisticIdDT> agreement = getMergedJaccardStatistic(alphaValue,strand);
//            agreement.show();
            JaccardStatisticDT.categoryQuantiles(agreement,
                    ((int)(alphaValue*100)) +"/all/" + ((strand == true)? "plusStrand" : "minusStrand"));
        }

    }

    public static Dataset<Row> evaluateGapStatistic() {
        Dataset<TaxonomicGroupMappingDT> taxonomicGroupDataset = SparkComputer.read(
                ProjectDirectoryManager.getTAXONOMIC_GROUPS_MAPPING_DIR() + SparkComputer.PARQUET_PATH,
                TaxonomicGroupMappingDT.ENCODER);
        List<String> ids = getFileNames();
        List<GapStatisticIdDT> gapStatistics = new ArrayList<>();
        for(String id: ids) {
            ResultDirectoryTree resultDirectoryTree = new ResultDirectoryTree(id, false);
            GapStatisticDT gapStatisticDT = FileIO.deSerializeJson(resultDirectoryTree.gapStatistic,GapStatisticDT.class);
            gapStatistics.add(new GapStatisticIdDT(id,gapStatisticDT));

        }
        Dataset<GapStatisticIdDT> gapStatisticIdDTDataset = SparkComputer.createDataFrame(gapStatistics,GapStatisticIdDT.ENCODER);
        return gapStatisticIdDTDataset.join(taxonomicGroupDataset,gapStatisticIdDTDataset.col(ColumnIdentifier.NAME).
                equalTo(taxonomicGroupDataset.col(ColumnIdentifier.NAME)),SparkComputer.JOIN_TYPES.INNER).
                drop(taxonomicGroupDataset.col(ColumnIdentifier.NAME));

    }

    public static void persistGapStatistic() {
        SparkComputer.writeToCSV(evaluateGapStatistic(),ProjectDirectoryManager.getGRAPH_DATA_DIR()+ "/MERGED_GAP_STATISTIC");
    }


    public static void computePropertyAgreement(double alphaValue) {
        computePropertyAgreement(alphaValue,getFileNames());
    }

    public static void computeTaxonomicGroupCluster(int amount,File file) {
        List<String> records = new ArrayList<>();
        FileIO.readSeqFromFile(file.getAbsolutePath(),records,false,0,Long.MAX_VALUE,String.class);
        List<String> doneRecords = new ArrayList<>();
        File doneFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/doneCluster.txt");
        FileIO.readSeqFromFile(doneFile.getAbsolutePath(),doneRecords,false,0,Long.MAX_VALUE,String.class);
//        System.out.println(doneRecords);
        records.removeAll(doneRecords);

        System.out.println(records.size());

        records = records.subList(0,Math.min(records.size(),amount));
        LOGGER.info("Computing: " + records);
        createCluster(0,doneFile,records);

    }

    public static void deleteFiles(File joinedFile) {
        List<String> joinedRecords = new ArrayList<>();
        FileIO.readSeqFromFile(joinedFile.getAbsolutePath(),joinedRecords,false,0,Long.MAX_VALUE,String.class);
        System.out.println("done");
//        List<File> files = getFiles();
//        System.out.println("done");
//        files = files.stream().filter(file -> !joinedRecords.contains(file.getName())).collect(Collectors.toList());
        List<String> names = getFileNames();
        System.out.println("done");
        names.removeAll(joinedRecords);
        for(String name: names) {
            File f = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()+"/" +name);

            try {
                FileUtils.deleteDirectory(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        for(String id: doneRecords) {
//            System.out.println(id);
//            File mappedRangeFile = new File(ProjectDirectoryManager.getGRAPH_FILES_DIR()+"/" + id + "/parquets/" + ResultDirectoryTree.PARQUET_NAME.MAPPED_RANGES );
//            if(mappedRangeFile.exists()) {
//                try {
//                    FileUtils.deleteDirectory(mappedRangeFile);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("deleted mapped ranges");
//            }
//        }
    }

    public static Dataset<RecordGapStatisticDT> getGapStatistic() {
        List<RecordGapStatisticDT> gapStatistics = new ArrayList<>();
        Dataset<RecordDT> recordData = null;
        try {
            recordData = RecordTable.getInstance().getTableEntries();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.exit(-1);
        }
        List<File> files = getFiles();
        int counter = 0;
        for(File file: files) {
            LOGGER.info(counter+"");
            int sequenceLength = recordData.filter(col(ColumnIdentifier.NAME).equalTo(file.getName())).select(col(ColumnIdentifier.LENGTH)).as(Encoders.INT()).first();
            gapStatistics.
                    add(new RecordGapStatisticDT(FileIO.deSerializeJson(new File(file.getAbsolutePath() + "/statistics/gapStatistic.json"),GapStatisticDT.class),
                            RecordDT.mapToRecordId(file.getName()),sequenceLength));
            counter++;
        }

        return Spark.getInstance().createDataset(gapStatistics,RecordGapStatisticDT.ENCODER);
    }

    public static Dataset<RecordGapStatisticDT> getGapStatisticCount() {
        List<RecordGapStatisticDT> gapStatistics = new ArrayList<>();
        Dataset<RecordDT> recordData = null;
        try {
            recordData = RecordTable.getInstance().getTableEntries();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.exit(-1);
        }
        List<File> files = getFiles();
        int counter = 0;
        for(File file: files) {
            LOGGER.info(counter+"");
            SequenceMapAnalyzer sequenceMapAnalyzer = new SequenceMapAnalyzer(file.getName());

            Dataset<RecordDiagonalBiPositionRangeDT> mappedRanges = sequenceMapAnalyzer.fetchJoinedRanges();

            int sequenceLength = recordData.filter(col(ColumnIdentifier.NAME).equalTo(file.getName())).select(col(ColumnIdentifier.LENGTH)).as(Encoders.INT()).first();
            RecordGapStatisticDT gapStatistic =
                    new RecordGapStatisticDT(sequenceMapAnalyzer.getUnMappedCount(),sequenceMapAnalyzer.getUnbridgedCount(mappedRanges),
                    RecordDT.mapToRecordId(file.getName()),sequenceLength);
            System.out.println(gapStatistic);
            gapStatistics.
                    add(gapStatistic);
            counter++;
        }

        return Spark.getInstance().createDataset(gapStatistics,RecordGapStatisticDT.ENCODER);
    }

    public static void persistAllAnnotations(List<String> ids) {
        for(double alpha: Arrays.asList(0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0)) {
            persistPropertyAnnotations(alpha,ids);
        }
    }

    public static void compare(List<String> ids) {
        for(double alpha: Arrays.asList(0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0)) {
            computePropertyAgreement(alpha,ids);
            persistMergedJaccardStatisticTaxonomicGroup(alpha,ids);
        }
    }

    public static void main(String[] args) {
        

        if(args.length == 0 ) {
            System.out.println("Option must be provided");
            System.exit(-1);
        }

        if(args[0].equals("create_annotations")) {
            List<String> ids = getFileNames();
            System.out.println("Creating annotations");
            fullRun(ids);
            persistAllAnnotations(ids);
        }
        else if(args[0].startsWith("create_annotations_fasta")) {
            if(args.length < 2) {
                System.out.println("File must be provided as argument");
                System.exit(-1);
            }
            File file = new File(args[1]);
            if(!file.exists()) {
                System.out.println("File " + file.getAbsolutePath() + " does not exist");
                System.exit(-1);
            }
            System.out.println("Creating annotations for file " + file.getAbsolutePath());
            fullRunFromFile(Collections.singletonList(file));
            persistAllAnnotations(Collections.singletonList(file.getName().substring(0,file.getName().indexOf("."))));
        }
        else if(args[0].equals("evaluate_annotations")) {
            List<String> ids = getFileNames();
            System.out.println("Evaluating annotations");
            compare(ids);
        }
        else if(args[0].equals("show_annotations")) {
            if(args.length < 2) {
                System.out.println("Accession id must be provided");
                System.exit(-1);
            }
            for(double alpha: Arrays.asList(0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0)) {
                showPropertyAnnotation(alpha,args[1]);
            }
        }



        System.exit(0);
    }


}

package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.CountInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.IdentifierInterface;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class IdentifierCountDT implements IdentifierInterface, CountInterface, Serializable {

    private String identifier;
    private int count;
    public static final Encoder<IdentifierCountDT> ENCODER = Encoders.bean(IdentifierCountDT.class);

    public enum COLUMN_NAME implements DBColumnName {
        IDENTIFIER("identifier"),
        COUNT("count");

        public final String identifier;

        public String getIdentifier() {
            return identifier;
        }

        COLUMN_NAME(String identifier) {
            this.identifier = identifier;
        }
    }

    public IdentifierCountDT(String identifier, int count) {
        this.identifier = identifier;
        this.count = count;
    }

    public IdentifierCountDT() {

    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void setCount(int count) {
        this.count = count;
    }
}

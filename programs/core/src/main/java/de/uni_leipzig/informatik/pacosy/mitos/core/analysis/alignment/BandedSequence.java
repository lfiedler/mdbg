package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

public class BandedSequence extends Sequence {

    private final int bandSize;
    public BandedSequence(String sequence, boolean strand, int sequenceIdentifier, int offsetPosition, int bandSize) {
        super(sequence,strand,sequenceIdentifier,offsetPosition);
        this.bandSize = bandSize;
    }

    public BandedSequence(String sequence, boolean strand,int sequenceIdentifier, int bandSize) {
        super(sequence,strand,sequenceIdentifier);
        this.bandSize = bandSize;
    }

    public int getBandSize() {
        return bandSize;
    }
}

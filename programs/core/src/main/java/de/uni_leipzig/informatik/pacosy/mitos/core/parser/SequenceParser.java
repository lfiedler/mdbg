package de.uni_leipzig.informatik.pacosy.mitos.core.parser;


import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.DbgGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.graphs.TaxonomyGraph;
import de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment.SequenceAligner;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordPropertyTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;

import de.uni_leipzig.informatik.pacosy.mitos.core.psql.TaxonomyTable;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.Auxiliary;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.io.FileIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.*;


public class SequenceParser {

    static public enum AmbigFilter {
        REPLACE_FILTER,
        NO_FILTER,
        SKIP_FILTER,
        CUT_FILTER
    }


    private RecordDT record;

    private File fastaFile;

    private File gbFile;

    private File bedFile;

    private int k;

    private AmbigFilter ambigFilter;

    private Map<Integer,List<DbgGraph.Feature>> propertiesPos;

    private Map<Integer,List<DbgGraph.Feature>> propertiesNeg;

    private Set<String> proteins;
    private Set<String> trnas;
    private Set<String> rrnas;
    private Set<String> repOrigins;
    private Set<String> others;

    private DbgGraph dbgGraph;

    private static Logger LOGGER = LoggerFactory.getLogger(SequenceParser.class);

    // regex patterns
    private static Pattern trnaPattern = Pattern.
            compile("^(trn)(\\p{Alpha}+)([1-9]\\d*\\p{Alpha}*)?(_\\d+)?(-\\p{Alpha})?\\((\\p{Alpha}{3}|-{3})\\)$");

    private static Pattern repPattern = Pattern.compile("^O([LH])(_\\d+)?(-\\p{Alpha}+)?$");

    private static Pattern rrnaPattern = Pattern.compile("^rrn([LS])(_\\d+)?(-\\p{Alpha}+)?$");

    private static Pattern proteinPattern = Pattern.compile("^(\\p{Alpha}+)([1-9]\\d*\\p{Alpha}*)?(_\\d+)?(-\\p{Alpha})?$");

    private static Pattern orfPattern = Pattern.compile("^([hfm])?orf(\\d*)?");

    private static Pattern intronPattern = Pattern.compile("^Intron_gp(I{1,3})(-)?.*");

    private static Pattern gbPattern = Pattern.compile("^LOCUS\\p{Blank}*\\p{Upper}{2}_\\p{Digit}+\\p{Blank}*([1-9]\\p{Digit}*)" +
            "\\p{Blank}*bp\\p{Blank}*DNA\\p{Blank}*(circular|linear)\\p{Blank}*\\p{Upper}{3}\\p{Blank}" +
            "\\p{Digit}{2}-(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)-(1|2)\\p{Digit}{3}$");

    private static Pattern versionPattern = Pattern.compile("^VERSION\\p{Blank}*\\p{Upper}{2}_\\p{Digit}+\\.([1-9]\\p{Digit}*)$");

    private static Pattern taxidPattern = Pattern.compile("\\p{Blank}*/db_xref=\"taxon:([1-9]\\p{Digit}*)\"");

    private static Pattern ambigPattern = Pattern.compile("([^ACGT])");

    private static Pattern ambigPatternGlobal = Pattern.compile("(.*[^ACGT].*)");

    private static Pattern fastaHeaderPattern = Pattern.compile("^>([\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+)[\\p{Blank}](.*)(([\\p{Blank}]\\bmitochond.*\\b[\\p{Punct}])|[\\p{Blank}]genome[\\p{Punct}]).*");

    private static Pattern fastaShorthHeaderPattern = Pattern.compile("^>([\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+)[\\p{Blank}].*");

    private static Pattern fastaHeaderLiberalPattern = Pattern.compile("^>[\\p{Upper}]+_[\\p{Digit}]+\\.[1-9]+[\\p{Blank}](.*)");

    private static Pattern fastaLinePattern = Pattern.compile("\\p{Upper}+\\p{Blank}?+");

    public SequenceParser(File bedFile) {
        this();
        this.bedFile = bedFile;
    }

    public SequenceParser(int k, RecordDT record, File bedFile, File fastaFile, File gbFile, AmbigFilter ambigFilter) {
        this.k = k;
        this.record = record;
        this.bedFile = bedFile;
        this.fastaFile = fastaFile;
        this.gbFile = gbFile;
        this.ambigFilter = ambigFilter;
        this.proteins = new HashSet<>();
        this.trnas = new HashSet<>();
        this.rrnas = new HashSet<>();
        this.repOrigins = new HashSet<>();
        this.others = new HashSet<>();
        this.propertiesPos = new HashMap<>();
        this.propertiesNeg = new HashMap<>();
    }

    public SequenceParser(RecordDT record, File fastaFile, File gbFile, AmbigFilter ambigFilter) {
        this.k = k;
        this.record = record;
        this.bedFile = bedFile;
        this.fastaFile = fastaFile;
        this.gbFile = gbFile;
        this.ambigFilter = ambigFilter;
        this.proteins = new HashSet<>();
        this.trnas = new HashSet<>();
        this.rrnas = new HashSet<>();
        this.repOrigins = new HashSet<>();
        this.others = new HashSet<>();
        this.propertiesPos = new HashMap<>();
        this.propertiesNeg = new HashMap<>();
    }

    public SequenceParser(int k, RecordDT record, File bedFile, File fastaFile, File gbFile, AmbigFilter ambigFilter, DbgGraph dbgGraph) {
        this(k,record,bedFile,fastaFile,gbFile,ambigFilter);
        this.dbgGraph = dbgGraph;
    }

    public SequenceParser( File fastaFile, DbgGraph dbgGraph) {
        this.fastaFile = fastaFile;
        this.dbgGraph = dbgGraph;
        this.k = dbgGraph.getKmerLength();
    }


    public SequenceParser() {
        this.proteins = new HashSet<>();
        this.trnas = new HashSet<>();
        this.rrnas = new HashSet<>();
        this.repOrigins = new HashSet<>();
        this.others = new HashSet<>();
        this.propertiesPos = new HashMap<>();
        this.propertiesNeg = new HashMap<>();
    }


    // Auxiliary functions
    public void printCompressedMap(Map<Integer,List<DbgGraph.Feature>> properties) {
        int start = 0, stop = 0;
        Iterator it = properties.entrySet().iterator();
        Map.Entry<Integer,List<DbgGraph.Feature>> startMap = (Map.Entry<Integer, List<DbgGraph.Feature>>) it.next();
        start = startMap.getKey();
        stop = start;
        while(it.hasNext()) {
            Map.Entry<Integer,List<DbgGraph.Feature>> currentMap = (Map.Entry<Integer, List<DbgGraph.Feature>>) it.next();

            if(!currentMap.getValue().equals(startMap.getValue())) {
                System.out.println(start + "," + stop);
                for(DbgGraph.Feature f : startMap.getValue()) {
                    System.out.println("("+ f.category + ")" + f.feature + " ");
                }
                startMap = currentMap;
                start = currentMap.getKey();
            }
            stop = currentMap.getKey();
        }

        System.out.println(start + "," + stop);
        for(DbgGraph.Feature f : startMap.getValue()) {
            System.out.println("("+ f.category + ")" + f.feature + " ");
        }

    }

    public static void printFeatures(Map<Integer,List<DbgGraph.Feature>> properties) {
        for(Map.Entry<Integer,List<DbgGraph.Feature>> e : properties.entrySet()) {
            System.out.println("Pos: " + e.getKey());
            e.getValue().forEach( v -> v.print());
        }
    }

    public void printKmer(String kmer, String kmerInv, int kmerPos, int kmerInvPos, int length) {
        LOGGER.debug(kmer + "(" + kmerPos + "," + getKmerPartner(kmerPos,true,length,k) + ")"+
                (propertiesPos.get(kmerPos) != null ? propertiesPos.get(kmerPos).stream().map(DbgGraph.Feature::getAttributes).reduce("",(a, b)-> a + b) : "-")
                +", "
                + kmerInv + "(" + kmerInvPos + ","+ getKmerPartner(kmerInvPos,false,length,k) + ")" +
                (propertiesNeg.get(kmerInvPos) != null ? propertiesNeg.get(kmerInvPos).stream().map(DbgGraph.Feature::getAttributes).reduce("",(a, b)-> a + b) : "-"));
    }

    public void printPropertySets() {
        System.out.println("Proteins:");
        proteins.forEach(p -> System.out.println(p));

        System.out.println("Trnas:");
        trnas.forEach(p -> System.out.println(p));

        System.out.println("Rrnas:");
        rrnas.forEach(p -> System.out.println(p));

        System.out.println("Replication Origins:");
        repOrigins.forEach(p -> System.out.println(p));

        System.out.println("Other types::");
        others.forEach(p -> System.out.println(p));
    }

    public static char complement(char n){
        char r = 'X';
        switch (n){
            case 'A':
                r= 'T';
                break;
            case 'C':
                r= 'G';
                break;
            case 'G':
                r= 'C';
                break;
            case 'T':
                r= 'A';
                break;
            case 'U':
                r= 'A';
                break;
            case 'Y':
                r= 'R';
                break;
            case 'R':
                r= 'Y';
                break;
            case 'S':
                r= 'S';
                break;
            case 'W':
                r= 'W';
                break;
            case 'K':
                r= 'M';
                break;
            case 'M':
                r= 'K';
                break;
            case 'B':
                r= 'V';
                break;
            case 'V':
                r= 'B';
                break;
            case 'D':
                r= 'H';
                break;
            case 'H':
                r= 'D';
                break;
            case 'N':
                r= 'N';
                break;
            default:
                System.out.println("Invalid nucleotide " + n);
                //System.exit(-1);
        }
        return r;
    }

    public static String reverseComplement(String kmer){
        String revComp = "";
        CharacterIterator it = new StringCharacterIterator(kmer);
        while (it.current() != CharacterIterator.DONE) {
            revComp += complement(it.current());
            it.next();
        }
        StringBuilder in = new StringBuilder();
        in.append(revComp);
        return in.reverse().toString();
    }



    public static int getKmerPartner(int pos, boolean strand, int length, int k) {
        if(strand) {
            return ( pos < k ? (length-k+pos) : (pos-k));
        }
        else {
            return (pos >= (length-k) ? pos-(length-k) : pos+k );
        }
    }

    private boolean compareMaps(Map<String,Object> m1, Map<String,Object> m2) {
        if(!m1.keySet().equals(m2.keySet())) {
            return false;
        }
        for(Map.Entry<String,Object> e: m1.entrySet()) {
            if(!e.getValue().toString().equals(m2.get(e.getKey()).toString())) {
                return false;
            }
        }
        return true;
    }

    private static void updateKmerMap(Map<String,Integer []> kmerMap, String kmer, int position) {
        Integer [] positions = kmerMap.get(kmer);
        if(positions == null) {
            positions = new Integer[1];
            positions[0] = position;
        }
        else {
            positions = ArrayUtils.add(positions,position);
        }
        kmerMap.put(kmer,positions);

    }

    private void updateFeatureMap(List<Map<String,Object>> featureMap, List<DbgGraph.Feature> features, boolean strand, int pos, int record) {

        Map<String,Object> m = new HashMap<>();
        m.put("strand",strand);
        m.put("pos",pos);
        m.put("record",record);
        if(features != null) {

            for(DbgGraph.Feature f : features) {
                Map<String,Object> m2 = new HashMap<>();
                m2.put("strand",strand);
                m2.put("pos",pos);
                m2.put("record",record);
                m2.put(f.category,f.feature);

                featureMap.add(m2);
            }
        }
        else {
            featureMap.add(m);
        }

    }

    private void updateKmerMap1(Map<List<String>,List<Map<String,Object>>> kmerMap, String kmer, int pos, boolean strand, List<DbgGraph.Feature> features) {

        List kmers = Arrays.asList(kmer.substring(0,kmer.length()-1), kmer.substring(1,kmer.length()));
        List<Map<String,Object>> featureMap = kmerMap.get(kmers);
        if(featureMap == null) {
            featureMap = new ArrayList<>();
        }
        updateFeatureMap(featureMap,features,strand,pos,record.getRecordId());
        kmerMap.put(kmers,featureMap) ;
    }

    private void updateKmerMap(Map<Map<String,String>,List<Map<String,Object>>> kmerMap, String kmer, int pos, boolean strand, List<DbgGraph.Feature> features) {

        Map<String,String> kmers = new HashMap<>();
        kmers.put("from",kmer.substring(0,kmer.length()-1));
        kmers.put("to",kmer.substring(1,kmer.length()));

        List<Map<String,Object>> featureMap = kmerMap.get(kmers);
        if(featureMap == null) {
            featureMap = new ArrayList<>();
        }
        updateFeatureMap(featureMap,features,strand,pos,record.getRecordId());
        kmerMap.put(kmers,featureMap) ;
    }

    private static void updateKmerMap(Map<String,List<Map<String,Object>>> kmerMap, String kmer, int pos, boolean strand) {
        List<Map<String,Object>> properties = kmerMap.get(kmer);
        if(properties == null) {
            properties = new ArrayList<>();
        }
        Map<String,Object> propMap = new HashMap<>();
        propMap.put(DbgGraph.DBG_EDGE_ATTRIBUTES.POSITION.identifier,pos);
        propMap.put(DbgGraph.DBG_EDGE_ATTRIBUTES.STRAND.identifier,strand);
        properties.add(propMap);
        kmerMap.put(kmer,properties);
    }

    // getter and setter

    public AmbigFilter getAmbigFilter() {
        return ambigFilter;
    }

    public void setAmbigFilter(AmbigFilter ambigFilter) {
        this.ambigFilter = ambigFilter;
    }

    public File getFastaFile() {
        return fastaFile;
    }

    public File getGbFile() {
        return gbFile;
    }

    public File getBedFile() {
        return bedFile;
    }

    public Set<String> getProteins() {
        return proteins;
    }

    public Set<String> getTrnas() {
        return trnas;
    }

    public Set<String> getRrnas() {
        return rrnas;
    }

    public Set<String> getRepOrigins() {
        return repOrigins;
    }

    public Set<String> getOthers() {
        return others;
    }

    // feature related functions
    public  DbgGraph.Feature parseFeature(String feature) throws IllegalArgumentException{

        DbgGraph.Feature f;

        Matcher m  = trnaPattern.matcher(feature);
        if(m.matches()) {
            f = new DbgGraph.TRNA((m.group(2) == null ? "" : m.group(2))  + (m.group(3) == null ? "" : m.group(3)) );
            trnas.add(f.getFeatureAsString());
        }
        else{
            m = repPattern.matcher(feature);
            if(m.matches()) {
                if(m.group(1).equals("L")) {
                    f = new DbgGraph.ReplicationOrigin(true);
                }
                else {
                    f = new DbgGraph.ReplicationOrigin(false);
                }
                repOrigins.add(f.getFeatureAsString());
            }
            else {
                m = rrnaPattern.matcher(feature);
                if(m.matches()) {
                    if(m.group(1).equals("L")) {
                        f = new DbgGraph.RRNA(true);
                    }
                    else {
                        f = new DbgGraph.RRNA(false);
                    }
                    rrnas.add(f.getFeatureAsString());
                }
                else {
                    m = intronPattern.matcher(feature);
                    if(m.matches()) {
                        f = new DbgGraph.OtherType("Intron_gp" + m.group(1));
                        others.add(f.getFeatureAsString());
                    }
                    else {
                        m = orfPattern.matcher(feature);
                        if(m.matches()) {
                            f = new DbgGraph.OtherType((m.group(1) == null ? "" : m.group(1))+ "orf");
                            others.add(f.getFeatureAsString());
                        }
                        else {
                            m = proteinPattern.matcher(feature);
                            if(m.matches()) {
                                f = new DbgGraph.Protein(m.group(1) + (m.group(2) == null ? "" : m.group(2)));
                                proteins.add(f.getFeatureAsString());
                            }
                            else {
                                LOGGER.error("No matching category for feature " + feature);
                                throw new IllegalArgumentException("No matching category for feature " + feature);
                            }
                        }
                    }
                }
            }
        }

        return f;
    }

    public static DbgGraph.Feature parseFeatureStatic(String feature) throws IllegalArgumentException{

        DbgGraph.Feature f;

        Matcher m  = trnaPattern.matcher(feature);
        if(m.matches()) {
            f = new DbgGraph.TRNA((m.group(2) == null ? "" : m.group(2))  + (m.group(3) == null ? "" : m.group(3)) );

        }
        else{
            m = repPattern.matcher(feature);
            if(m.matches()) {
                if(m.group(1).equals("L")) {
                    f = new DbgGraph.ReplicationOrigin(true);
                }
                else {
                    f = new DbgGraph.ReplicationOrigin(false);
                }

            }
            else {
                m = rrnaPattern.matcher(feature);
                if(m.matches()) {
                    if(m.group(1).equals("L")) {
                        f = new DbgGraph.RRNA(true);
                    }
                    else {
                        f = new DbgGraph.RRNA(false);
                    }

                }
                else {
                    m = intronPattern.matcher(feature);
                    if(m.matches()) {
                        f = new DbgGraph.OtherType("Intron_gp" + m.group(1));

                    }
                    else {
                        m = orfPattern.matcher(feature);
                        if(m.matches()) {
                            f = new DbgGraph.OtherType((m.group(1) == null ? "" : m.group(1))+ "orf");

                        }
                        else {
                            m = proteinPattern.matcher(feature);
                            if(m.matches()) {
                                f = new DbgGraph.Protein(m.group(1) + (m.group(2) == null ? "" : m.group(2)));

                            }
                            else {
                                LOGGER.error("No matching category for feature " + feature);
                                throw new IllegalArgumentException("No matching category for feature " + feature);
                            }
                        }
                    }
                }
            }
        }

        return f;
    }

    private void fillProps(Map<Integer,List<DbgGraph.Feature>> properties, String feature, int start, int stop) {
        DbgGraph.Feature f = parseFeature(feature);
        for(Integer i = start; i <= stop; i++) {
            List<DbgGraph.Feature> features = properties.get(i);
            if(features == null) {
                features = new ArrayList<>();
            }
            features.add(f);
            properties.put(i,features);
        }
    }

    private static void fillProps(List<PropertyRangeProbDT> properties, String feature, int start, int stop) {
        DbgGraph.Feature f = parseFeatureStatic(feature);

        properties.add(new PropertyRangeProbDT(f.category,f.getFeatureAsString(),start,stop,stop-start+1, BigDecimal.ONE));

    }

    public static void parseMinimal(DbgGraph dbgGraph, int recordId, boolean topology, File fastaFile) {
        for(boolean strand : Arrays.asList(true,false)) {
            List<PositionCountIdentifierDT> mappedPlusKmers = getMappedKmers(fastaFile,DbgGraph.K,topology,strand,AmbigFilter.NO_FILTER);
            dbgGraph.updateEdges(mappedPlusKmers,strand,recordId);
            dbgGraph.commit();
        }
    }

    // parsing
    public boolean parse() {
        parseRecordFromGB();
        parseFeaturesFromBED();
        return parseSequenceFromFasta();

    }

    public boolean parseFromFile(File file) {

        parseRecordFromFile(file);
        parseFeaturesFromBED();
        return parseSequenceFromFasta();

    }

    public boolean parseForRecordOnly() {

        parseRecordFromGB();
//        parseFeaturesFromBED();
        return parseSequenceFromFastaRecordUpdateOnly();

    }

    public void parseCheck() {

        parseFeaturesFromBED();
        parseRecordFromGB();

        Map<List<String>,List<Map<String,Object>>> kmerMap = parseSequenceFromFastaToMap();
        Map graphMap = dbgGraph.getMapForRecord(record.getRecordId());
        if(!graphMap.keySet().equals(kmerMap.keySet())) {
            LOGGER.error("Keys do not match");
            System.exit(-1);
        }

        boolean found = false;
        for(Map.Entry<List<String>,List<Map<String,Object>>> e : kmerMap.entrySet()) {
            List<Map<String,Object>> graphEntry = (List)graphMap.get(e.getKey());
            if(graphEntry.size() == e.getValue().size()) {
                for(Map<String,Object> m1: graphEntry) {
                    found = false;
                    for(Map<String,Object> m2 : e.getValue()) {
                        if(compareMaps(m1,m2)) {
                            found = true;
                            break;
                        }
                    }
                    if(!found) {
                        LOGGER.error("Feature lists do not match");
                        System.exit(-1);
                    }
                }
            }
            else {
                LOGGER.error("Feature lists do not match");
                System.exit(-1);
            }

        }
        LOGGER.info("Feature maps match!");

    }

    /**
     * @param fastaFile
     * @param topology
     * @param k
     * @return A map, where kmers are keys and a list of strand-position maps.
     *  Each of the strand-position maps contains an entry with key "pos" and value of the associated position
     *  and an entry with key "strand" and value true if on positive strand and false otherwise.
     */
    public static Map<String,List<Map<String,Object>>> parseSequenceForMapping(File fastaFile, boolean topology, int k) {
        System.out.println("start sequence mapping");
        Map<String,List<Map<String,Object>>> kmerMap = new HashMap<>();

        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();

            Matcher fastaLineMatcher;
            Matcher fastaHeaderMatcher;
            int lineCounter = 0;

            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {


                String kmer;
                Matcher m;
                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    m = ambigPatternGlobal.matcher(kmer);
                    if(!m.matches()) {
                        updateKmerMap(kmerMap,kmer,pos+k,true);
                        updateKmerMap(kmerMap,reverseComplement(kmer),pos,false);
                    } else {
                        LOGGER.info("Skipping ambiguous kmer " + kmer);
                    }


                }
                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        m = ambigPattern.matcher(kmer);
                        if(!m.matches()) {
                            updateKmerMap(kmerMap,kmer,-pos+k,true);
                            updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos,false);
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }

                    }
                }



            }
//            System.out.println(kmerMap.entrySet().stream().filter(s -> s.getValue().size() > 1).collect(Collectors.toList()));
            Auxiliary.printMap(kmerMap);

        } catch(Exception e) {
            e.printStackTrace();
        }

        return kmerMap;
    }

    public static void getNucleotideCount(File fastaFile, Map<SequenceAligner.NUCLEOTIDE,Long> nucleotideCount) {
        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();

            Matcher fastaLineMatcher;
            Matcher fastaHeaderMatcher;
            int lineCounter = 0;

            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaShorthHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {


                for(int i = 0; i < SequenceAligner.NUCLEOTIDE.values().length; i++) {
                    SequenceAligner.NUCLEOTIDE nucleotide = SequenceAligner.NUCLEOTIDE.values()[i];
                    Long currentCount = nucleotideCount.get(nucleotide);
                    if(currentCount == null) {
                        currentCount = 0L;
                    }

                    nucleotideCount.put(nucleotide,currentCount +sequence.chars().
                            filter(ch -> ch == nucleotide.identifier).count());
                }
            }


        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static List<String> getKmers(String sequence, int k, boolean strand, boolean topology) {

        List<String> kmerList = new ArrayList<>();
        String kmer;
        for(int pos = 0; pos < sequence.length()-k; pos++) {
            kmer = sequence.substring(pos,pos+k+1);
            if(strand) {
                kmerList.add(kmer);
            }
            else {
                kmerList.add(reverseComplement(kmer));
            }

        }
        if(topology) {
            for(int pos = k; pos > 0; pos--) {
                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                if(strand) {
                    kmerList.add(kmer);
                }
                else {
                    kmerList.add(reverseComplement(kmer));
                }

            }
        }
        return kmerList;
    }

    public static Map<String,Integer []> getKmerMap(File fastaFile, int k, boolean topology, boolean strand, AmbigFilter ambigFilter) {

        return getKmerMap(parseSequenceFromFasta(fastaFile,fastaHeaderLiberalPattern,fastaLinePattern),k,topology,strand,ambigFilter);
    }

    public static Map<String,Integer []> getKmerMap(String sequence, int k, boolean topology, boolean strand, AmbigFilter ambigFilter) {
        Map<String,Integer []> kmerMap = new HashMap<>();

        if(ambigFilter == AmbigFilter.NO_FILTER) {
            String kmer;
            for(int pos = 0; pos < sequence.length()-k; pos++) {
                kmer = sequence.substring(pos,pos+k+1);

                if(!strand) {
                    updateKmerMap(kmerMap,reverseComplement(kmer),pos);
                }
                else {
                    updateKmerMap(kmerMap,kmer,pos+k);
                }



            }
//            System.out.println("end:");
            if(topology) {
                for(int pos = k; pos > 0; pos--) {
                    kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    if(!strand) {
                        updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,-pos+k);
                    }

                }
            }
        }
        else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
            Matcher matcher = ambigPattern.matcher(sequence);
            int ambigCounter = 0;
            Map<String,Integer> ambigNucleotides = new HashMap<>();
            while(matcher.find()) {
                Integer count = ambigNucleotides.get(matcher.group(1));
                ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                ambigCounter++;
            }
            if(ambigCounter > 0) {
                LOGGER.info("Skipping ambiguous sequence ");
                LOGGER.info(ambigNucleotides.keySet().stream().
                        map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                return kmerMap;
            }
            else {
                String kmer;

                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    if(!strand) {
                        updateKmerMap(kmerMap,reverseComplement(kmer),pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,pos+k);
                    }
                }

                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        if(!strand) {
                            updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos);
                        }
                        else {
                            updateKmerMap(kmerMap,kmer,-pos+k);
                        }

                    }
                }
            }

        } else {

            String kmer;
            Matcher m;
            for(int pos = 0; pos < sequence.length()-k; pos++) {
                kmer = sequence.substring(pos,pos+k+1);
                m = ambigPatternGlobal.matcher(kmer);
                if(!m.matches()) {
                    if(!strand) {
                        updateKmerMap(kmerMap,reverseComplement(kmer),pos);
                    }
                    else {
                        updateKmerMap(kmerMap,kmer,pos+k);
                    }
                } else {
                    LOGGER.info("Skipping ambiguous kmer " + kmer);
                }


            }
//            System.out.println("end:");
            if(topology) {
                for(int pos = k; pos > 0; pos--) {
                    kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                    m = ambigPattern.matcher(kmer);
                    if(!m.matches()) {
                        if(!strand) {
                            updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos);
                        }
                        else {
                            updateKmerMap(kmerMap,kmer,-pos+k);
                        }
                    } else {
                        LOGGER.info("Skipping ambiguous kmer " + kmer);
                    }

                }
            }
        }


        return kmerMap;
    }

    public static List<PositionCountIdentifierDT> getMappedKmers(String sequence, int k, boolean topology, boolean strand, AmbigFilter ambigFilter) {
        List<PositionCountIdentifierDT> mappedKmerDTS = new ArrayList<>();
        try {
            if(ambigFilter == AmbigFilter.NO_FILTER) {

                String kmer;

                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    if(!strand) {
                        mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                    }
                    else {
                        mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                    }
                }

                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                        }

                    }
                }

            }
            else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                Matcher matcher = ambigPattern.matcher(sequence);
                int ambigCounter = 0;
                Map<String,Integer> ambigNucleotides = new HashMap<>();
                while(matcher.find()) {
                    Integer count = ambigNucleotides.get(matcher.group(1));
                    ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                    ambigCounter++;
                }
                if(ambigCounter > 0) {
                    LOGGER.info("Skipping ambiguous sequence ");
                    LOGGER.info(ambigNucleotides.keySet().stream().
                            map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                    return mappedKmerDTS;
                }
                else {
                    String kmer;

                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                        }
                    }

                    if(topology) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            if(!strand) {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                            }
                            else {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                            }

                        }
                    }
                }

            } else {

                String kmer;
                Matcher m;
                for(int pos = 0; pos < sequence.length()-k; pos++) {
                    kmer = sequence.substring(pos,pos+k+1);
                    m = ambigPatternGlobal.matcher(kmer);
                    if(!m.matches()) {
                        if(!strand) {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),pos));
                        }
                        else {
                            mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,pos+k));

                        }
                    } else {
                        LOGGER.info("Skipping ambiguous kmer " + kmer);
                    }


                }
//                System.out.println("end:");
                if(topology) {
                    for(int pos = k; pos > 0; pos--) {
                        kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                        m = ambigPattern.matcher(kmer);
                        if(!m.matches()) {
                            if(!strand) {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(reverseComplement(kmer),sequence.length()-pos));
                            }
                            else {
                                mappedKmerDTS.add(new PositionCountIdentifierDT(kmer,-pos+k));
                            }
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }

                    }
                }
            }



        } catch(Exception e) {
            e.printStackTrace();
        }

        return mappedKmerDTS;
    }

    public static List<PositionCountIdentifierDT> getMappedKmers(File fastaFile, int k, boolean topology, boolean strand, AmbigFilter ambigFilter) {
        String sequence = parseSequenceFromFasta(fastaFile,fastaHeaderLiberalPattern,fastaLinePattern);
        return getMappedKmers(sequence,k,topology,strand,ambigFilter);
    }

    private Map<List<String>,List<Map<String,Object>>> parseSequence() {
        Map<List<String>,List<Map<String,Object>>> kmerMap = new HashMap<>();
        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {

                if(ambigFilter == AmbigFilter.NO_FILTER) {

                }
                else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                    Matcher matcher = ambigPattern.matcher(sequence);
                    int ambigCounter = 0;
                    Map<String,Integer> ambigNucleotides = new HashMap<>();
                    while(matcher.find()) {
                        Integer count = ambigNucleotides.get(matcher.group(1));
                        ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                        ambigCounter++;
                    }
                    if(ambigCounter > 0) {
                        LOGGER.info("Skipping ambiguous sequence " + record.getName()
                                +". Discovered " + ambigCounter + " ambiguous nucleotides.");
                        LOGGER.info(ambigNucleotides.keySet().stream().
                                map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                        return kmerMap;
                    }
                    else {
                        String kmer;

                        for(int pos = 0; pos < sequence.length()-k; pos++) {
                            kmer = sequence.substring(pos,pos+k+1);
                            updateKmerMap1(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap1(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));


                        }

                        if(record.isTopology()) {
                            for(int pos = k; pos > 0; pos--) {
                                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                                updateKmerMap1(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap1(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            }
                        }
                    }

                } else {

                    String kmer;
                    Matcher m;
                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        m = ambigPatternGlobal.matcher(kmer);
                        if(!m.matches()) {
                            updateKmerMap1(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap1(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }


                    }
//                    System.out.println("end:");
                    if(record.isTopology()) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            m = ambigPattern.matcher(kmer);
                            if(!m.matches()) {
                                updateKmerMap1(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap1(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            } else {
                                LOGGER.info("Skipping ambiguous kmer " + kmer);
                            }

                        }
                    }
                }


            }


        } catch(Exception e) {
            e.printStackTrace();
        }

        return kmerMap;
    }

    private Map<List<String>,List<Map<String,Object>>> parseSequenceFromFastaToMap() {
        Map<List<String>,List<Map<String,Object>>> kmerMap = new HashMap<>();
        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {

                if(ambigFilter == AmbigFilter.NO_FILTER) {

                }
                else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                    Matcher matcher = ambigPattern.matcher(sequence);
                    int ambigCounter = 0;
                    Map<String,Integer> ambigNucleotides = new HashMap<>();
                    while(matcher.find()) {
                        Integer count = ambigNucleotides.get(matcher.group(1));
                        ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                        ambigCounter++;
                    }
                    if(ambigCounter > 0) {
                        LOGGER.info("Skipping ambiguous sequence " + record.getName()
                                +". Discovered " + ambigCounter + " ambiguous nucleotides.");
                        LOGGER.info(ambigNucleotides.keySet().stream().
                                map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                        return kmerMap;
                    }
                    else {
                        String kmer;

                        for(int pos = 0; pos < sequence.length()-k; pos++) {
                            kmer = sequence.substring(pos,pos+k+1);
                            updateKmerMap1(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap1(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));


                        }

                        if(record.isTopology()) {
                            for(int pos = k; pos > 0; pos--) {
                                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                                updateKmerMap1(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap1(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            }
                        }
                    }

                } else {

                    String kmer;
                    Matcher m;
                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        m = ambigPatternGlobal.matcher(kmer);
                        if(!m.matches()) {
                            updateKmerMap1(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap1(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }


                    }
//                    System.out.println("end:");
                    if(record.isTopology()) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            m = ambigPattern.matcher(kmer);
                            if(!m.matches()) {
                                updateKmerMap1(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap1(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            } else {
                                LOGGER.info("Skipping ambiguous kmer " + kmer);
                            }

                        }
                    }
                }


            }


        } catch(Exception e) {
            e.printStackTrace();
        }

        return kmerMap;
    }

    public boolean parseSequenceFromFasta(Map<Map<String,String>,List<Map<String,Object>>> kmerMap) {

        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {

                if(ambigFilter == AmbigFilter.REPLACE_FILTER) {

                }
                else if(ambigFilter == AmbigFilter.NO_FILTER) {

                }
                else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                    Matcher matcher = ambigPattern.matcher(sequence);
                    int ambigCounter = 0;
                    Map<String,Integer> ambigNucleotides = new HashMap<>();
                    while(matcher.find()) {
                        Integer count = ambigNucleotides.get(matcher.group(1));
                        ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                        ambigCounter++;
                    }
                    if(ambigCounter > 0) {
                        LOGGER.info("Skipping ambiguous sequence " + record.getName()
                                +". Discovered " + ambigCounter + " ambiguous nucleotides.");
                        LOGGER.info(ambigNucleotides.keySet().stream().
                                map(u -> u + ":" + ambigNucleotides.get(u) + "\n").toString());
                        return false;
                    }
                    else {
                        String kmer;

                        for(int pos = 0; pos < sequence.length()-k; pos++) {
                            kmer = sequence.substring(pos,pos+k+1);
                            updateKmerMap(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));


                        }

                        if(record.isTopology()) {
                            for(int pos = k; pos > 0; pos--) {
                                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                                updateKmerMap(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            }
                        }
                    }

                } else {

                    String kmer;
                    Matcher m;
                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        m = ambigPatternGlobal.matcher(kmer);
                        if(!m.matches()) {
                            updateKmerMap(kmerMap,kmer,pos+k,true,propertiesPos.get(pos+k));
                            updateKmerMap(kmerMap,reverseComplement(kmer),pos,false,propertiesNeg.get(pos));
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }


                    }
//                    System.out.println("end:");
                    if(record.isTopology()) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            m = ambigPattern.matcher(kmer);
                            if(!m.matches()) {
                                updateKmerMap(kmerMap,kmer,-pos+k,true,propertiesPos.get(-pos+k));
                                updateKmerMap(kmerMap,reverseComplement(kmer),sequence.length()-pos,false,
                                        propertiesNeg.get(sequence.length()-pos));
                            } else {
                                LOGGER.info("Skipping ambiguous kmer " + kmer);
                            }

                        }
                    }
                }


            }


        } catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * Checks provided sequence (String of nucleotides) for ambiguous nucleotides
     * @param sequence
     * @return amount of ambiguous nucleoticdes
     */
    public static int getSequenceAmbiguousNucleotides(String sequence) {
        int ambigCounter = 0;

        try {

            Matcher matcher = ambigPattern.matcher(sequence);
            Map<String,Integer> ambigNucleotides = new HashMap<>();
            while(matcher.find()) {
                Integer count = ambigNucleotides.get(matcher.group(1));
                ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                ambigCounter++;
            }

            if(ambigCounter > 0) {
                ambigNucleotides.entrySet().stream().forEach(n -> System.out.println(n.getKey() + ":" + n.getValue()));
                LOGGER.info("Disconverd Ambiguous nucleotides:" +ambigNucleotides.keySet().stream().reduce("",String::concat) +").");
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return ambigCounter;
    }

    /**
     * Checks provided FastaFile for ambiguous nucleotides
     * @param fastaFile
     * @return true if no ambiguous nucleortides were found, else false
     */
    public static int getSequenceAmbiguousNucleotides(File fastaFile) {
        int ambigCounter = 0;

        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaShorthHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {
                Matcher matcher = ambigPattern.matcher(sequence);

                Map<String,Integer> ambigNucleotides = new HashMap<>();
                while(matcher.find()) {
                    Integer count = ambigNucleotides.get(matcher.group(1));
                    ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                    ambigCounter++;
                }
                if(ambigCounter > 0) {
                    LOGGER.info("Ambig sequence " + fastaFile.getName());
//                    if(ambigCounter >= 100) {
//                        System.out.println(ambigCounter + "/" + sequence.length() + "(" + 100*(ambigCounter/((double)sequence.length())) + ")");
//                    }


//                    LOGGER.info("Ambiguous sequence " + fastaFile.getName()
//                            +". Discovered " + ambigCounter + " ambiguous nucleotides ("+ambigNucleotides.keySet().stream().reduce("",String::concat) +").");
                }


            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return ambigCounter;
    }

    /**
     * Checks provided FastaFile for ambiguous nucleotides
     * @param fastaFile
     * @return true if no ambiguous nucleortides were found, else false
     */
    public static boolean checkSequenceForAmbiguousNucleotides(File fastaFile) {
        int ambigCounter = getSequenceAmbiguousNucleotides(fastaFile);
        if(ambigCounter == 0) {
            return true;
        }

        return false;
    }

    private boolean parseSequenceFromFasta() {

        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }
            else {

                if(ambigFilter == AmbigFilter.NO_FILTER) {
                    int ambigCounter = getSequenceAmbiguousNucleotides(sequence.toString());
//                    if((100 * (ambigCounter / ((double) sequence.length()))) > 1) {
//                        LOGGER.info("Skipping ambiguous sequence " + ambigCounter + "/" +
//                                sequence.length() + "(" + 100*(ambigCounter/((double)sequence.length())) + ")");
//                        return false;
//                    }
                    record.setAmbigCount(ambigCounter);
                    String kmer;
                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        dbgGraph.updateEdges(kmer,pos+k,true,record.getRecordId(),propertiesPos.get(pos+k));
                        dbgGraph.updateEdges(reverseComplement(kmer),pos,false,record.getRecordId(),propertiesNeg.get(pos));
                    }

                    if(record.isTopology()) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            dbgGraph.updateEdges(kmer,-pos+k,true,record.getRecordId(),propertiesPos.get(-pos+k));
                            dbgGraph.updateEdges(reverseComplement(kmer),sequence.length()-pos,false,
                                    record.getRecordId(),propertiesNeg.get(sequence.length()-pos));

                        }
                    }
                }
                else if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                    Matcher matcher = ambigPattern.matcher(sequence);
                    int ambigCounter = 0;
                    Map<String,Integer> ambigNucleotides = new HashMap<>();
                    while(matcher.find()) {
                        Integer count = ambigNucleotides.get(matcher.group(1));
                        ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                        ambigCounter++;
                    }
                    if(ambigCounter > 0) {

                        LOGGER.info("Skipping ambiguous sequence " + record.getName()
                                +". Discovered " + ambigCounter + " ambiguous nucleotides ("+ambigNucleotides.keySet().stream().reduce("",String::concat) +").");
                        return false;
                    }
                    else {
                        String kmer;

                        for(int pos = 0; pos < sequence.length()-k; pos++) {
                            kmer = sequence.substring(pos,pos+k+1);
                            dbgGraph.updateEdges(kmer,pos+k,true,record.getRecordId(),propertiesPos.get(pos+k));
                            dbgGraph.updateEdges(reverseComplement(kmer),pos,false,record.getRecordId(),propertiesNeg.get(pos));

                        }

                        if(record.isTopology()) {
                            for(int pos = k; pos > 0; pos--) {
                                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                                dbgGraph.updateEdges(kmer,-pos+k,true,record.getRecordId(),propertiesPos.get(-pos+k));
                                dbgGraph.updateEdges(reverseComplement(kmer),sequence.length()-pos,false,
                                        record.getRecordId(),propertiesNeg.get(sequence.length()-pos));

                            }
                        }
                    }

                } else {

                    String kmer;
                    Matcher m;
                    for(int pos = 0; pos < sequence.length()-k; pos++) {
                        kmer = sequence.substring(pos,pos+k+1);
                        m = ambigPatternGlobal.matcher(kmer);
                        if(!m.matches()) {
                            dbgGraph.updateEdges(kmer,pos+k,true,record.getRecordId(),propertiesPos.get(pos+k));
                            dbgGraph.updateEdges(reverseComplement(kmer),pos,false,record.getRecordId(),propertiesNeg.get(pos));
                        } else {
                            LOGGER.info("Skipping ambiguous kmer " + kmer);
                        }
                    }

                    if(record.isTopology()) {
                        for(int pos = k; pos > 0; pos--) {
                            kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                            m = ambigPattern.matcher(kmer);
                            if(!m.matches()) {
                                dbgGraph.updateEdges(kmer,-pos+k,true,record.getRecordId(),propertiesPos.get(-pos+k));
                                dbgGraph.updateEdges(reverseComplement(kmer),sequence.length()-pos,false,
                                        record.getRecordId(),propertiesNeg.get(sequence.length()-pos));
                            } else {
                                LOGGER.info("Skipping ambiguous kmer " + kmer);
                            }

                        }
                    }
                }

                record.setaContent(StringUtils.countMatches(sequence,"A"));
                record.setcContent(StringUtils.countMatches(sequence,"C"));
                record.setgContent(StringUtils.countMatches(sequence,"G"));
                record.settContent(StringUtils.countMatches(sequence,"T"));
                assert (sequence.length() == record.getLength());
//                record.setLength(sequence.length());

            }


        } catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean parseSequenceFromFastaRecordUpdateOnly() {

        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;
            StringBuilder sequence = new StringBuilder();
            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHeaderLiberalPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }

            if(ambigFilter == AmbigFilter.SKIP_FILTER) {
                Matcher matcher = ambigPattern.matcher(sequence);
                int ambigCounter = 0;
                Map<String,Integer> ambigNucleotides = new HashMap<>();
                while(matcher.find()) {
                    Integer count = ambigNucleotides.get(matcher.group(1));
                    ambigNucleotides.put(matcher.group(1), (count == null) ? 1 : count+1);
                    ambigCounter++;
                }
                if(ambigCounter > 0) {
                    LOGGER.info("Skipping ambiguous sequence " + record.getName()
                            +". Discovered " + ambigCounter + " ambiguous nucleotides ("+ambigNucleotides.keySet().stream().reduce("",String::concat) +").");
                    return false;
                }

            }

            record.setaContent(StringUtils.countMatches(sequence,"A"));
            record.setcContent(StringUtils.countMatches(sequence,"C"));
            record.setgContent(StringUtils.countMatches(sequence,"G"));
            record.settContent(StringUtils.countMatches(sequence,"T"));
            record.setLength(sequence.length());


        } catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public void parseFeaturesFromBED() {
        try{
            if(bedFile == null) {
                return;
            }

            String [] values;

            BufferedReader br = new BufferedReader(new FileReader(bedFile));
            String line;
            while((line = br.readLine()) != null )  {
                values = line.split("\t");
                if(values.length == 1) {
                     break;
                }
                int pos1 = Integer.parseInt(values[1]);
                int pos2 = Integer.parseInt(values[2]);

                LOGGER.debug("Positions: " + pos1 + " to " + pos2);
                if(pos1 > pos2) {
                    LOGGER.debug("Need to rearrange bed file");
                    if (values[5].equals("+")) {
                        LOGGER.debug("Plus strand");
                        fillProps(propertiesPos,values[3],pos1,record.getLength()-1);
                        fillProps(propertiesPos,values[3],0,pos2);
                    } else if (values[5].equals("-")) {
                        LOGGER.debug("Minus strand");
                        fillProps(propertiesNeg,values[3],pos1,record.getLength()-1);
                        fillProps(propertiesNeg,values[3],0,pos2);
                    } else {
                        LOGGER.error("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                        throw new IllegalArgumentException("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                    }
                }
                else {
                    if (values[5].equals("+")) {
                        LOGGER.debug("Plus strand");
                        fillProps(propertiesPos,values[3],pos1,pos2);
                    } else if (values[5].equals("-")) {
                        LOGGER.debug("Minus strand");
                        fillProps(propertiesNeg,values[3],pos1,pos2);
                    } else {
                        LOGGER.error("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                        throw new IllegalArgumentException("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                    }
                }

            }
            br.close();

//            System.out.println("Positive");
//            printCompressedMap(propertiesPos);
//            System.out.println();
//            System.out.println("Negative");
//            printCompressedMap(propertiesNeg);

        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<PropertyRangeProbStrandDT> parseFeaturesFromBEDToPropertyRanges(File bedFile, int length) {
        List<PropertyRangeProbStrandDT> propertyRangesRefSeq = null;
        try{
            if(bedFile == null) {
                return null;
            }

            String [] values;

            BufferedReader br = new BufferedReader(new FileReader(bedFile));
            String line;
            List<PropertyRangeProbDT> propertiesPos = new ArrayList<>();
            List<PropertyRangeProbDT> propertiesNeg = new ArrayList<>();
            while((line = br.readLine()) != null )  {
                values = line.split("\t");
                if(values.length == 1) {
                    break;
                }
                int pos1 = Integer.parseInt(values[1]);
                int pos2 = Integer.parseInt(values[2]);

                LOGGER.debug("Positions: " + pos1 + " to " + pos2);
                if(pos1 > pos2) {
                    LOGGER.debug("Need to rearrange bed file");
                    if (values[5].equals("+")) {
                        LOGGER.debug("Plus strand");
                        fillProps(propertiesPos,values[3],pos1,length-1);
                        fillProps(propertiesPos,values[3],0,pos2);
                    } else if (values[5].equals("-")) {
                        LOGGER.debug("Minus strand");
                        fillProps(propertiesNeg,values[3],pos1,length-1);
                        fillProps(propertiesNeg,values[3],0,pos2);
                    } else {
                        LOGGER.error("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                        throw new IllegalArgumentException("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                    }
                }
                else {
                    if (values[5].equals("+")) {
                        LOGGER.debug("Plus strand");
                        fillProps(propertiesPos,values[3],pos1,pos2);
                    } else if (values[5].equals("-")) {
                        LOGGER.debug("Minus strand");
                        fillProps(propertiesNeg,values[3],pos1,pos2);
                    } else {
                        LOGGER.error("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                        throw new IllegalArgumentException("Illegal strandedness " + values[5] + " in bed file " + bedFile.getName());
                    }
                }

            }
            br.close();
            PropertyRangeDT.groupProperties(propertiesPos,length);
            PropertyRangeDT.groupProperties(propertiesNeg,length);
            propertyRangesRefSeq =
                    PropertyRangeProbStrandDT.convert(SparkComputer.createDataFrame(propertiesPos,PropertyRangeProbDT.ENCODER), true).
                            union(PropertyRangeProbStrandDT.convert(SparkComputer.createDataFrame(propertiesNeg,PropertyRangeProbDT.ENCODER), false)).collectAsList();


//            System.out.println("Positive");
//            printCompressedMap(propertiesPos);
//            System.out.println();
//            System.out.println("Negative");
//            printCompressedMap(propertiesNeg);

        }  catch (Exception e) {
            e.printStackTrace();
        }
        return propertyRangesRefSeq;
    }

    public static  void parseToBED(String id, String fileName, List<DbgGraph.Feature> features) {
        List<String> bedLines = new ArrayList<>();
        for(DbgGraph.Feature feature: features) {
            String s = id + "\t";
            s += feature.getRange().getStart() + "\t" + feature.getRange().getEnd() + "\t";
            s += feature.getFeatureAsString() + "\t.\t" + feature.getStrand();
            bedLines.add(s);
        }
        FileIO.writeSeqToFile(bedLines,fileName,false);
    }

    private void parseRecordFromGB() {

        Matcher m;
        try {


            BufferedReader br = new BufferedReader(new FileReader(gbFile));

            // parse length and topology
            String line = br.readLine();

            m = gbPattern.matcher(line);
            if(m.matches()) {
                record.setLength(Integer.parseInt(m.group(1)));

                record.setTopology(m.group(2).equals("circular") ? true : false);
            }
            else {
                LOGGER.error("LOCUS field " + line + " does not match");
                throw new IllegalArgumentException("LOCUS field " + line + " does not match");
            }


            while((line = br.readLine()) != null)  {
                m = versionPattern.matcher(line);
                if(m.matches()) {
                    record.setVersion(Integer.parseInt(m.group(1)));
                    break;
                }

            }

            while((line = br.readLine()) != null)  {
                m = taxidPattern.matcher(line);
                if(m.matches()) {
                    record.setTaxid(Integer.parseInt(m.group(1)));
                    break;
                }

            }
            if(line == null) {
                LOGGER.error("Taxid not specified in file " + gbFile.getName());
                throw new MissingResourceException("Taxid not specified in file " + gbFile.getName(),"Record","taxid");
            }
            br.close();

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void parseRecordFromFile(File file) {
        try {
            Map<String,Object> recordMap = new HashMap<>();
            recordMap.put("topology", Boolean.TRUE);
            recordMap.put("taxid", 0);
            recordMap.put("mitosFlag", Boolean.TRUE);
            recordMap.put("version", 1);

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while((line = br.readLine()) != null) {
                String[] split = line.split("=");
                Object value = recordMap.get(split[0]);
                if(value != null) {
                    if(value instanceof Integer) {
                        recordMap.put(split[0],Integer.parseInt(split[1]));
                    }
                    else if(value instanceof Boolean) {
                        recordMap.put(split[0],Boolean.parseBoolean(split[1]));
                    }
                    else {
                        LOGGER.error("Wrong instance!");
                        System.exit(-1);
                    }

                }
            }

            record.setMitosFlag((Boolean)recordMap.get("mitosFlag"));
            record.setTopology((Boolean)recordMap.get("topology"));
            record.setTaxid((Integer)recordMap.get("taxid"));
            record.setVersion((Integer)recordMap.get("version"));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String parseSequenceFromFasta(File fastaFile) {

        return parseSequenceFromFasta(fastaFile,fastaHeaderLiberalPattern,fastaLinePattern);
    }

    public static String parseSequenceFromFasta(File fastaFile, Pattern fastaHaderPattern, Pattern fastaLinePattern) {

        StringBuilder sequence = new StringBuilder();
        try {

            BufferedReader br = new BufferedReader(new FileReader(fastaFile));
            String line;

            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {

                fastaLineMatcher = fastaLinePattern.matcher(line);
                if(fastaLineMatcher.matches()) {
                    sequence.append(line);
                }
                else {
                    fastaHeaderMatcher = fastaHaderPattern.matcher(line);
                    if(fastaHeaderMatcher.matches()) {
                        lineCounter++;
                    }
                }

            }
            br.close();
            LOGGER.info("Sequence length: " + sequence.length());
            if(lineCounter < 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter > 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return sequence.toString();
    }

    private static String getParsedEdgeLabel(String kPlusOneMer) {
        return kPlusOneMer.substring(0,kPlusOneMer.length()-1) + "->" + kPlusOneMer.substring(kPlusOneMer.length()-1);
    }

    public static void parseSequence(String sequence, int k, boolean topology) {
        String kmer;
        System.out.println("+Strand");
        for(int pos = 0; pos < sequence.length()-k; pos++) {
            kmer = sequence.substring(pos,pos+k+1);
            System.out.println((pos+k) + ": " + getParsedEdgeLabel(kmer));
        }

        if(topology) {
            for(int pos = k; pos > 0; pos--) {
                kmer = sequence.substring(sequence.length()-pos) + sequence.substring(0,-pos+k+1);
                System.out.println((-pos+k) + ": " + getParsedEdgeLabel(kmer));

            }
        }
        System.out.println("-Strand");
        for(int pos = 0; pos < sequence.length()-k; pos++) {
            kmer = sequence.substring(pos,pos+k+1);
            System.out.println((pos) + ": " + getParsedEdgeLabel(reverseComplement(kmer)));
        }

        if(topology) {
            for(int pos = k; pos > 0; pos--) {
                kmer = sequence.substring(sequence.length()-pos,sequence.length()) + sequence.substring(0,-pos+k+1);
                System.out.println((sequence.length()-pos) + ": " + getParsedEdgeLabel(reverseComplement(kmer)));


            }
        }
    }


    public static String getFastaHeaderLine(File fastaFile, Pattern fastaHaderPattern) {

        String header = null;
        try (BufferedReader br = new BufferedReader(new FileReader(fastaFile))){

            String line;

            Matcher fastaHeaderMatcher;
            Matcher fastaLineMatcher;
            int lineCounter = 0;
            while((line = br.readLine()) != null)  {
                fastaHeaderMatcher = fastaHaderPattern.matcher(line);
                if(fastaHeaderMatcher.matches()) {
                    lineCounter++;
                    header = line;
                }
            }

            if(lineCounter > 1) {
                LOGGER.error("More than one fasta record. Amount of records: " + lineCounter);
                System.exit(-1);
            }
            else if(lineCounter < 1) {
                LOGGER.error("No fasta record included");
                System.exit(-1);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            return header;
        }

    }

    public static String getScientificName(File fastaFile, TaxonomyGraph taxonomyGraph) {
        BufferedReader br = null;
        String scientificName = null;
        try {
            br = new BufferedReader(new FileReader(fastaFile));
            Matcher fastaHeaderMatcher = null;
            try {
                String line = br.readLine();
//                System.out.println(line);
                fastaHeaderMatcher = fastaHeaderPattern.matcher(line);
                if(fastaHeaderMatcher.matches()) {
                    scientificName = fastaHeaderMatcher.group(2);
//                    System.out.println(fastaHeaderMatcher.group(0));
//                    System.out.println(fastaHeaderMatcher.group(1));
//                    System.out.println(fastaHeaderMatcher.group(2));
                }
                else {
                    fastaHeaderMatcher = fastaHeaderLiberalPattern.matcher(line);
                    LOGGER.warn("Could only extract liberal match: " + line);
                    if(fastaHeaderMatcher.matches()) {
                        Pattern punctuationPattern = Pattern.compile("(.*)[\\p{Punct}](.*)");
                        Matcher punctuationMatcher = punctuationPattern.matcher(fastaHeaderMatcher.group(1));
                        if(punctuationMatcher.matches()) {
                            LOGGER.warn("Found puntuation symbol. Trying to strip it");
                            scientificName = punctuationMatcher.group(1);
                        }
                        else {
                            scientificName = fastaHeaderMatcher.group(1);
                        }

                    }
                    else {
                        LOGGER.error("Could not extract scientific name for " + fastaFile.getName());
                        LOGGER.error(line);
                        System.exit(-1);
                    }

//                    System.exit(-1);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        TaxonomyGraph.TaxonomicGroup taxonomicGroup =  taxonomyGraph.getTaxonomicGroupName(scientificName);
        if(taxonomicGroup == null) {
            LOGGER.warn("Could not identify taxonomic group");
            return null;
        }
        return taxonomicGroup.getScientificName();

    }

    public static Integer getTaxId(File gbFile) {

        Matcher m;
        try {


            BufferedReader br = new BufferedReader(new FileReader(gbFile));

            // parse length and topology
            String line = br.readLine();

            m = gbPattern.matcher(line);
            if(!m.matches()) {
                LOGGER.error("LOCUS field " + line + " does not match");
                br.close();
                throw new IllegalArgumentException("LOCUS field " + line + " does not match");
            }

            while((line = br.readLine()) != null)  {
                m = taxidPattern.matcher(line);
                if(m.matches()) {
                    br.close();
                    Integer taxid = Integer.parseInt(m.group(1));
                    Integer mergeTaxid = TaxonomyTable.getInstance().getMergeTaxids(taxid);
                    return (mergeTaxid == null ? taxid : mergeTaxid);

                }
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    public static String getIdFromFastaFile(File fastaFile) {
        BufferedReader br = null;
        String id = null;
        try {
            br = new BufferedReader(new FileReader(fastaFile));
            Matcher fastaHeaderMatcher = null;
            try {
                String line = br.readLine();
//                System.out.println(line);
                fastaHeaderMatcher = fastaShorthHeaderPattern.matcher(line);
                if(fastaHeaderMatcher.matches()) {
                    id = fastaHeaderMatcher.group(1);
//                    System.out.println(fastaHeaderMatcher.group(0));
//                    System.out.println(fastaHeaderMatcher.group(1));
//                    System.out.println(fastaHeaderMatcher.group(2));
                }
                else {
                    LOGGER.warn("Could not infer sequence id");
                    return null;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return id;
    }

    public Map<Integer, List<DbgGraph.Feature>> getPropertiesPos() {
        return propertiesPos;
    }

    public Map<Integer, List<DbgGraph.Feature>> getPropertiesNeg() {
        return propertiesNeg;
    }


}

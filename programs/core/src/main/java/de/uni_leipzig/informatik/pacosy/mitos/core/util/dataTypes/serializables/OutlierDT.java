package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

import java.io.Serializable;

public class OutlierDT implements Serializable {

    public static final Encoder<OutlierDT> ENCODER = Encoders.bean(OutlierDT.class);
    protected String attribute;
    protected double value;

    public OutlierDT() {
    }

    public OutlierDT(String attribute, double value) {
        this.attribute = attribute;
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}

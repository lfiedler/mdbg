package de.uni_leipzig.informatik.pacosy.mitos.core.analysis.alignment;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.AlignCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.GapCost;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.ScoreCost;

import java.util.ArrayList;
import java.util.Arrays;

public class SequenceAlignerFactory {

    public static final AlignCost LEVEL_1;
    public static final AlignCost LEVEL_2;
    public static final AlignCost LEVEL_3;
    public static final AlignCost LEVEL_4;
    public static final AlignCost LEVEL_5;
    public static final AlignCost LEVEL_6;
    public static final AlignCost LEVEL_7;
    public static final AlignCost LEVEL_8;
    public static final AlignCost LEVEL_9;
    public static final AlignCost LEVEL_10;
    public static final AlignCost LEVEL_11;
    public static final AlignCost LEVEL_12;

    static {
        // the lower the level, the more stringent: more stringent values are better suited for alignments with high sequence identity
        LEVEL_1 = new AlignCost(new ScoreCost(1,-5),
                new ArrayList<>(Arrays.asList(new GapCost(3,3))));
        LEVEL_2 = new AlignCost(new ScoreCost(1,-4),
                new ArrayList<>(Arrays.asList(
                        new GapCost(1,2),
                        new GapCost(0,2),
                        new GapCost(2,1),
                        new GapCost(1,1)
                )));
        LEVEL_3 = new AlignCost(new ScoreCost(2,-7),
                new ArrayList<>(Arrays.asList(
                        new GapCost(2,4),
                        new GapCost(0,4),
                        new GapCost(4,2),
                        new GapCost(2,2)
                )));
        LEVEL_4 = new AlignCost(new ScoreCost(1,-3),
                new ArrayList<>(Arrays.asList(
                        new GapCost(2,2),
                        new GapCost(1,2),
                        new GapCost(0,2),
                        new GapCost(2,1),
                        new GapCost(1,1)
                )));

        LEVEL_5 = new AlignCost(new ScoreCost(2,-5),
                new ArrayList<>(Arrays.asList(
                        new GapCost(2,4),
                        new GapCost(0,4),
                        new GapCost(4,2),
                        new GapCost(2,2)
                )));
        // blast default -> assumes 95% conservation
        LEVEL_6 = new AlignCost(new ScoreCost(1,-2),
                new ArrayList<>(Arrays.asList(
                        new GapCost(2,2),
                        new GapCost(1,2),
                        new GapCost(0,2),
                        new GapCost(3,1),
                        new GapCost(2,1),
                        new GapCost(1,1)
                )));
        LEVEL_7 = new AlignCost(new ScoreCost(2,-3),
                new ArrayList<>(Arrays.asList(
                        new GapCost(4,4),
                        new GapCost(2,4),
                        new GapCost(0,4),
                        new GapCost(3,3),
                        new GapCost(6,2),
                        new GapCost(5,2),
                        new GapCost(4,2),
                        new GapCost(2,2)
                )));
        LEVEL_8 = new AlignCost(new ScoreCost(3,-4),
                new ArrayList<>(Arrays.asList(
                        new GapCost(6,3),
                        new GapCost(5,3),
                        new GapCost(4,3),
                        new GapCost(6,2),
                        new GapCost(5,2),
                        new GapCost(4,2)
                )));
        LEVEL_9 = new AlignCost(new ScoreCost(4,-5),
                new ArrayList<>(Arrays.asList(
                        new GapCost(6,5),
                        new GapCost(5,5),
                        new GapCost(4,5),
                        new GapCost(3,5)
                )));
        // assumes 75 % conservation
        LEVEL_10 = new AlignCost(new ScoreCost(1,-1),
                new ArrayList<>(Arrays.asList(
                        new GapCost(3,2),
                        new GapCost(2,2),
                        new GapCost(1,2),
                        new GapCost(0,2),
                        new GapCost(4,1),
                        new GapCost(3,1),
                        new GapCost(2,1)
                )));
        LEVEL_11 = new AlignCost(new ScoreCost(3,-2),
                new ArrayList<>(Arrays.asList(
                        new GapCost(5,5)
                )));
        LEVEL_12 = new AlignCost(new ScoreCost(5,-4),
                new ArrayList<>(Arrays.asList(
                        new GapCost(10,6),
                        new GapCost(8,6)
                )));

    }

    private SequenceAlignerFactory(AlignCost alignCost) {

    }

    public static BandedAligner createBandedSequenceAligner(GapCost gapCost, ScoreCost scoreCost) {
        BandedAligner bandedAligner = new BandedAligner(gapCost.getOpenGapCost(),
                gapCost.getExtendGapCost(),new JukesCantorScoreMatrix(scoreCost));

        return bandedAligner;
    }


}

package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess;

import org.apache.spark.sql.Dataset;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class  ResultDataset{

    private Dataset dataset;
    private String identifier;
    private String description;

    public ResultDataset(Dataset dataset, String identifier, String description) {
        this.dataset = dataset;
        this.identifier = identifier;
        this.description = description;
    }

    public static void showContents(Collection<ResultDataset> resultDatasets) {
        resultDatasets.stream().forEach(r -> System.out.println(r));
    }

    public static void showContents(Map<String,ResultDataset> resultDatasets) {
        resultDatasets.entrySet().stream().forEach(r -> System.out.println(r.getValue()));
    }

    @Override
    public String toString() {
        return identifier + ": " + description ;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return ((ResultDataset) obj).identifier.equals(this.identifier);
     }


     @Override
    public int hashCode() {
        return identifier.hashCode();
     }

    public Dataset getDataset() {
        return dataset;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }
}

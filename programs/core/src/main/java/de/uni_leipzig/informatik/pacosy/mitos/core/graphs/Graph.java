package de.uni_leipzig.informatik.pacosy.mitos.core.graphs;

import de.uni_leipzig.informatik.pacosy.mitos.core.graphAcess.*;
import de.uni_leipzig.informatik.pacosy.mitos.core.psql.RecordTable;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.ProjectDirectoryManager;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.RecordDT;
import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.tinkerpop.gremlin.process.traversal.IO;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.graphdb.relations.RelationIdentifier;
import org.janusgraph.graphdb.tinkerpop.JanusGraphIoRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.security.krb5.Config;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

abstract public class Graph {

    protected static final Logger LOGGER = LoggerFactory.getLogger(Graph.class);
    protected JanusGraph graph;
    protected GraphTraversalSource g;
    protected Dataset<RecordDT> recordAttributes;

    protected interface NODE_ATTRIBUTES {
        public String getIdentifier();

    }
    protected interface EDGE_ATTRIBUTES {
        public String getIdentifier();
    }

    public static String[] getStringRepresentationForAttribute ( NODE_ATTRIBUTES[] attributes) {
        String [] stringArray = new String[attributes.length];
        for(int i = 0; i < attributes.length; i++) {
            stringArray[i] = attributes[i].getIdentifier();
        }
        return stringArray;
    }

    public static String[] getStringRepresentationForAttribute ( EDGE_ATTRIBUTES[] attributes) {
        String [] stringArray = new String[attributes.length];
        for(int i = 0; i < attributes.length; i++) {
            stringArray[i] = attributes[i].getIdentifier();
        }
        return stringArray;
    }

    public static class Builder {

        private GraphConfiguration.CONNECTION_TYPE connection_type;
        private String keyspace;
        private LaunchExternal.LAUNCH_OPTION launchOption;
        private List<AccessProperty> properties;

        private Builder() {
            synchronized (this) {
                this.properties = null;
                launchOption = LaunchExternal.LAUNCH_OPTION.VOID;
            }
        }

        public Builder(String keyspace, GraphConfiguration.CONNECTION_TYPE connection_type) {
            this();
            synchronized (this) {
                this.connection_type = connection_type;
                this.keyspace = keyspace;
            }
        }

        public synchronized Builder startOption(LaunchExternal.LAUNCH_OPTION launchOption) {
            this.launchOption = launchOption;
            return this;
        }

        public synchronized Builder properties(List<AccessProperty> properties) {
            this.properties = properties;
            return this;
        }

        public synchronized Graph build() {
            Graph graph = GraphFactory.createGraph(keyspace);
            GraphConfiguration.Builder builder =
                    new GraphConfiguration.Builder(keyspace,connection_type).useElasticSearch();
            if(properties != null) {
                builder.properties(properties);
            }
            GraphConfiguration graphConfiguration = builder.build();
            switch (launchOption) {
                case VOID:
                    break;
                case START:
                    LaunchExternal.startCassandra(false);
                    LaunchExternal.startElasticsearch(false);
                    break;
                case RESTART:
                    LaunchExternal.startCassandra(true);
                    LaunchExternal.startElasticsearch(true);
                    break;
                default:
                    LOGGER.error("Invalid lauchoption " + launchOption);
            }

            graph.openGraph(graphConfiguration);
            graph.initialize();
            return graph;
        }
    }

    Graph() {

    }

    protected void initialize() {
//        try {
//            this.recordAttributes  = RecordTable.getInstance().getTableEntries();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
    }

    protected void openGraph() {
        graph = JanusGraphFactory.open(graph.configuration());
        g = graph.traversal();
    }

    protected void openGraph(GraphConfiguration propertiesConfiguration) {
        graph = JanusGraphFactory.open(propertiesConfiguration);
        boolean reopen = changeGlobalProperties(propertiesConfiguration,graph);
        if(reopen) {
            graph = JanusGraphFactory.open(propertiesConfiguration);
        }
        g = graph.traversal();
    }

    /*** Schema creation ******************************************************************************************************************************/

    abstract protected void createSchema();

    abstract protected boolean schemaGenerationSuccessful();

    protected void createKey(String keyIdentifier, Class propertyClass) {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        assert(management.getPropertyKey(keyIdentifier) == null);
        graph.tx().rollback();
        management = graph.openManagement();
        management.makePropertyKey(keyIdentifier).
                dataType(propertyClass).make();
        management.commit();

        graph.tx().rollback();
        management = graph.openManagement();
        assert(management.getPropertyKey(keyIdentifier) != null);
        openGraph();
    }

    protected void dropKey(String keyIdentifier) {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        management.getPropertyKey(keyIdentifier).remove();
        management.commit();

        graph.tx().rollback();
        management = graph.openManagement();
        assert(management.getPropertyKey(keyIdentifier) == null);
        openGraph();
    }

    /*** Graph settings ******************************************************************************************************************************/
    protected static List<LocalProperty> getLowCacheProperties() {
        List<LocalProperty> localProperties = new ArrayList<>();
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.ENABLE_CACHING,false));
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.DATABASE_LEVEL_CACHE_PERCENTAGE,0.1));
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.MAX_TRANSACTION_LEVEL_CACHE_OF_RECENTLY_USED_VERTICES,200));
        localProperties.add(new LocalProperty(LocalProperty.ALLOWED_KEYS.ENABLE_PRE_FETCH_ALL_PROPERTIES,false));
        return localProperties;
    }

    protected PropertiesConfiguration changeGraphConfiguration(PropertiesConfiguration configuration) {
        PropertiesConfiguration oldConfiguration = (PropertiesConfiguration)graph.configuration();
        GraphConfiguration graphConfiguration = new GraphConfiguration.Builder(configuration).useElasticSearch().build();
        openGraph(graphConfiguration);
        return oldConfiguration;
    }

    protected PropertiesConfiguration changeGraphConfiguration(String keyspace, GraphConfiguration.CONNECTION_TYPE connectionType) {
        PropertiesConfiguration oldConfiguration = (PropertiesConfiguration)graph.configuration();
        GraphConfiguration graphConfiguration = new GraphConfiguration.Builder(
                keyspace,
                connectionType).useElasticSearch().build();
        openGraph(graphConfiguration);
        return oldConfiguration;
    }

    protected PropertiesConfiguration changeGraphConfiguration(GraphConfiguration.CONNECTION_TYPE connectionType) {
        return changeGraphConfiguration(graph.configuration().getString(GraphConfiguration.getKeySpaceIdentifier()),connectionType);
    }

    protected <T extends AccessProperty> PropertiesConfiguration changeGraphConfiguration(List<T> accessProperties) {
        PropertiesConfiguration oldConfiguration = (PropertiesConfiguration)graph.configuration();
        GraphConfiguration graphConfiguration = new GraphConfiguration.Builder(
                (PropertiesConfiguration)graph.configuration()).properties(accessProperties).useElasticSearch().build();
        openGraph(graphConfiguration);
        return oldConfiguration;
    }

    protected static void closeAllOtherInstances(JanusGraph graph) {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        Set<String> openInstances = management.getOpenInstances();
        for(String instance : openInstances) {
            if(!instance.contains("current")) {
                management.forceCloseInstance(instance);
            }
        }
        management.commit();
        openInstances = management.getOpenInstances();
        System.out.println(openInstances);

    }

    protected static void checkInstances(JanusGraph graph) {
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        Set<String> openInstances = management.getOpenInstances();
        System.out.println(openInstances);

    }

    protected static void showGlobalProperties(List<GlobalProperty> globalProperties, JanusGraph graph) {
        closeAllOtherInstances(graph);
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        for(GlobalProperty globalProperty : globalProperties) {
            LOGGER.info("Changing: " + globalProperty.getPropertyKey().getIdentifier()
                    + " to " + management.get(globalProperty.getPropertyKey().getIdentifier()));
        }

//        management.commit();
    }

    protected static boolean changeGlobalProperties(GraphConfiguration graphConfiguration, JanusGraph graph) {
        if(graphConfiguration.getGlobalPropertiesToCheck().size() == 0) {
            return false;
        }
        closeAllOtherInstances(graph);
        graph.tx().rollback();
        JanusGraphManagement management = graph.openManagement();
        for(GlobalProperty globalProperty : graphConfiguration.getGlobalPropertiesToCheck()) {
//            management.get(globalProperty.getPropertyKey().getIdentifier()).equals(globalProperty.g)
            LOGGER.info("Changing: " + globalProperty.getPropertyKey().getIdentifier()
                    + " from " + management.get(globalProperty.getPropertyKey().getIdentifier()));
            management.set(globalProperty.getPropertyKey().getIdentifier(),globalProperty.getValue());
        }

        management.commit();


        return true;
    }



//    protected static void changeCacheTime(Integer waitSeconds, JanusGraph graph) {
//        closeAllOtherInstances(graph);
//        graph.tx().rollback();
//        JanusGraphManagement management = graph.openManagement();
//        // The cache will hold graph elements for at most cache.db-cache-time many milliseconds.
//        // If an element expires, the data will be re-read from the storage backend on the next access.
//        System.out.println("Time: " + management.get("cache.db-cache-time"));
//        management.set("cache.db-cache-time",waitSeconds*1000);
//        System.out.println("Time: " + management.get("cache.db-cache-time"));
//        management.commit();
//    }
//
//    protected static void changeIdBlockSizeTime(Integer size, JanusGraph graph) {
//        closeAllOtherInstances(graph);
//        graph.tx().rollback();
//        JanusGraphManagement management = graph.openManagement();
//        System.out.println("Block size: " + management.get("ids.block-size"));
//        management.set("ids.block-size",size);
//        System.out.println("Block size: " + management.get("ids.block-size"));
//        management.commit();
//    }


    public void commit() {
        g.tx().commit();
    }


    /*** Deletion ******************************************************************************************************************************/

    public void entireDeletion() {
        try {
            closeAllOtherInstances(graph);
            graph.tx().rollback();
            JanusGraphFactory.drop(graph);
            openGraph();

            long vertexCount = g.V().count().next();
            long edgeCount = g.E().count().next();
            if(vertexCount != 0 || edgeCount != 0) {
                LOGGER.error("Not all data was deleted. Nodecount " + vertexCount + ", Edgecount " + edgeCount);
                System.exit(-1);
            }
        } catch (BackendException b) {
            b.printStackTrace();
        }

    }

    public void deleteGraph(){
        g.V().drop().iterate();
        g.tx().commit();
        System.out.println(g.V().count().next());
        System.out.println(g.E().count().next());
    }

    public void deleteNodes(List ids) {
        g.V(ids).drop().iterate();
        commit();
    }

    public void deleteEdges() {

        g.E().drop().iterate();
        commit();

    }

    public void deleteEdges(List ids) {
        g.E(ids).drop().iterate();
        commit();
    }

    /*** Utility ******************************************************************************************************************************/

    public static <T> Stream<T> getStream(Iterator<T> it) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(it,0),false);
    }

    /*** Filter ******************************************************************************************************************************/

    public GraphTraversal<?, ?> filterNodePropertyTraversal(NODE_ATTRIBUTES attribute, Collection values) {
        return g.V().has(attribute.getIdentifier(),P.within(values));
    }

    public GraphTraversal<?, ?> filterNodePropertyTraversal(NODE_ATTRIBUTES attribute, Collection values, Collection ids) {
        return g.V(ids).has(attribute.getIdentifier(),P.within(values));
    }

    public GraphTraversal<?, ?> filterNodePropertyTraversal(Map<NODE_ATTRIBUTES,Collection> attributes) {
        GraphTraversal<?, ?> traversal = g.V();
        for(Map.Entry<NODE_ATTRIBUTES,Collection> attribute : attributes.entrySet()) {
            traversal = traversal.has(attribute.getKey().getIdentifier(), P.within(attribute.getValue()));
        }
        return traversal;
    }

    public GraphTraversal<?, ?> filterNodePropertyTraversal(Map<NODE_ATTRIBUTES,Collection> attributes, Collection ids) {
        GraphTraversal<?, ?> traversal = g.V(ids);
        for(Map.Entry<NODE_ATTRIBUTES,Collection> attribute : attributes.entrySet()) {
            traversal = traversal.has(attribute.getKey().getIdentifier(), P.within(attribute.getValue()));
        }
        return traversal;
    }

    public List filterNodeProperty(NODE_ATTRIBUTES attribute, Collection values) {
        return filterNodePropertyTraversal(attribute,values).id().toList();
    }

    public List filterNodeProperty(NODE_ATTRIBUTES attribute, Collection values, Collection ids) {
        return filterNodePropertyTraversal(attribute,values,ids).id().toList();
    }

    public List filterNodeProperty(Map<NODE_ATTRIBUTES,Collection> attributes) {
        return filterNodePropertyTraversal(attributes).id().toList();
    }

    public List filterNodeProperty(Map<NODE_ATTRIBUTES,Collection> attributes, Collection ids) {
        return filterNodePropertyTraversal(attributes,ids).id().toList();
    }


    public GraphTraversal<?, ?>  filterEdgePropertyTraversal(EDGE_ATTRIBUTES attribute, Collection values) {
        return g.E().has(attribute.getIdentifier(),P.within(values));
    }

    public GraphTraversal<?, ?>  filterEdgePropertyTraversal(EDGE_ATTRIBUTES attribute, Collection values, Collection ids) {
        return g.E(ids).has(attribute.getIdentifier(),P.within(values));
    }

    public GraphTraversal<?, ?>  filterEdgePropertyTraversal(Map<EDGE_ATTRIBUTES,Collection> attributes) {
        GraphTraversal<?, ?> traversal = g.E();
        for(Map.Entry<EDGE_ATTRIBUTES,Collection> attribute : attributes.entrySet()) {
            traversal = traversal.has(attribute.getKey().getIdentifier(), P.within(attribute.getValue()));
        }
        return traversal;
    }

    public GraphTraversal<?, ?>  filterEdgePropertyTraversal(Map<EDGE_ATTRIBUTES,Collection> attributes, List ids) {
        GraphTraversal<?, ?> traversal = g.E(ids);
        for(Map.Entry<EDGE_ATTRIBUTES,Collection> attribute : attributes.entrySet()) {
            traversal = traversal.has(attribute.getKey().getIdentifier(), P.within(attribute.getValue()));
        }
        return traversal;
    }


    public List filterEdgeProperty(EDGE_ATTRIBUTES attribute, Collection values) {
        return filterEdgePropertyTraversal(attribute,values).id().toList();
    }

    public List filterEdgeProperty(EDGE_ATTRIBUTES attribute, Collection values, Collection ids) {
        return filterEdgePropertyTraversal(attribute,values,ids).id().toList();
    }

    public List filterEdgeProperty(Map<EDGE_ATTRIBUTES,Collection> attributes) {
        return filterEdgePropertyTraversal(attributes).id().toList();
    }

    public List filterEdgeProperty(Map<EDGE_ATTRIBUTES,Collection> attributes, List ids) {
        return filterEdgePropertyTraversal(attributes,ids).id().toList();
    }

//    public List filterSurroundingEdges(long vertexId, int hopcount) {
//        g.V(vertexId).
//    }

    /*** Subgraph ******************************************************************************************************************************/

//    public void setSubGraph(Collection edgeIds) {
//        subGraph = (TinkerGraph)g.E(edgeIds).subgraph("subgraph").cap("subgraph").next();
//        subGraphG = subGraph.traversal();
//        System.out.println(subGraphG.E().count().next());
//    }

    public GraphTraversalSource createSubGraph(Collection edgeIds) {
        TinkerGraph subGraph = (TinkerGraph)g.E(edgeIds).subgraph("subgraph").cap("subgraph").next();
        subGraph.configuration().setProperty("storage.backend","inmemory");
        System.out.println(subGraph.configuration().getString("storage.backend"));
        return subGraph.traversal();
    }

    public static GraphTraversalSource createEmptySubGraph() {
        Configuration conf = new BaseConfiguration();
        conf.setProperty("gremlin.tinkergraph.defaultVertexPropertyCardinality","list");
        return TinkerGraph.open(conf).traversal();
    }

    public GraphTraversalSource createDeepCopySubGraph(GraphTraversalSource g) {
        String tempFileName = "/tmp/g.xml";
        writeToGraphML(tempFileName,g);
        GraphTraversalSource gnew = createEmptySubGraph();
        gnew.io(tempFileName).read().iterate();
        return gnew;
    }

    /*** IO ******************************************************************************************************************************/

    public static void writeToGraphML(String fileName, GraphTraversalSource graphTraversalSource) {
        if(!fileName.contains("/")) {
            fileName = ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/" + fileName;
        }
        System.out.println(fileName);
        graphTraversalSource.io(fileName).write().iterate();
    }

    public static void writeToGryo(String fileName, GraphTraversalSource graphTraversalSource) {
        if(!fileName.contains("/")) {
            fileName = ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/" + fileName;
        }
//        System.out.println(fileName);
        graphTraversalSource.io(fileName).
                with(IO.writer,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName())
                .write().iterate();
    }

    public static GraphTraversalSource readGraphML(String fileName) {
        if(!fileName.contains("/")) {
            fileName = ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/" + fileName;
        }
        System.out.println(fileName);
        GraphTraversalSource tinkerGraphG = createEmptySubGraph();
        tinkerGraphG.io( fileName).read().iterate();
        return tinkerGraphG;
    }

    public static GraphTraversalSource readGryo(String fileName) {
        if(!fileName.contains("/")) {
            fileName = ProjectDirectoryManager.getGRAPH_FILES_DIR() + "/" + fileName;
        }
//        System.out.println(fileName);
        GraphTraversalSource tinkerGraphG = createEmptySubGraph();
        tinkerGraphG.io( fileName).
                with(IO.reader,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName()).
                read().iterate();
        return tinkerGraphG;
    }

    /*** Checks ******************************************************************************************************************************/



    /*** Getter and Setter ******************************************************************************************************************************/
    public GraphTraversalSource getG() {
        return g;
    }

    public Dataset<RecordDT> getRecordAttributes() {
        if (recordAttributes  == null) {
            try {
                recordAttributes = RecordTable.getInstance().getTableEntries().cache();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return recordAttributes;
    }

    public Dataset<RecordDT> getRecordAttributes(Collection<Integer> records) {
        return getRecordAttributes().
                filter((FilterFunction<RecordDT>)(r -> records.contains(r.getRecordId())));
    }

    public RecordDT getRecordAttributes(int record) {
        return SparkComputer.getFirstOrElseNull(getRecordAttributes().
                filter((FilterFunction<RecordDT>)(r -> r.getRecordId() == record)));
    }

    public abstract String getKeyspace();

    public List convertToElementaryIds(List<RelationIdentifier> edgeRelationIdentifier) {
        List<String> edgeIds = new ArrayList<>();
        for(RelationIdentifier relationIdentifier: edgeRelationIdentifier) {
            edgeIds.add(relationIdentifier.toString());
        }
        return edgeIds;
    }

    public void convertToElementaryIds(List<RelationIdentifier> edgeRelationIdentifier, List<String> edgeIds, List<Long> vertexIds) {
        for(RelationIdentifier relationIdentifier: edgeRelationIdentifier) {
            edgeIds.add(relationIdentifier.toString());
            vertexIds.add(relationIdentifier.getOutVertexId());
        }
    }

    public static void main(String [] args) {
        GraphTraversalSource g = createEmptySubGraph();
        g.addV("test").iterate();
        System.out.println(g.V().count().next());
        System.exit(0);
    }
}

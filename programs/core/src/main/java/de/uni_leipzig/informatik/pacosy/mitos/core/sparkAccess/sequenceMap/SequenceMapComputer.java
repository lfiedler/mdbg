package de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.sequenceMap;

import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.ResultDataset;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.Spark;
import de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.SparkComputer;

import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.CountInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.interfaces.PositionInterface;
import de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.util.*;

import static de.uni_leipzig.informatik.pacosy.mitos.core.sparkAccess.sequenceMap.SubsequenceAnalyzer.*;
import static org.apache.spark.sql.functions.col;

public class SequenceMapComputer extends SparkComputer {

    public enum SEQUENCE_MAP_PARQUET_NAMES implements PARQUET_NAMES {
        PLUS_POSITION_RECORD_COUNT_DISTRIBUTION( "plusPositionRecordCountDistribution" + PARQUET_EXTENSION),
        MINUS_POSITION_RECORD_COUNT_DISTRIBUTION( "minusPositionRecordCountDistribution" + PARQUET_EXTENSION),
        PLUS_POSITON_PROTEIN_DISTRIBUTION("plusPositionProteinDistribution" + PARQUET_EXTENSION),
        MINUS_POSITON_PROTEIN_DISTRIBUTION("minusPositionProteinDistribution" + PARQUET_EXTENSION),
        PLUS_POSITON_TRNA_DISTRIBUTION("plusPositionTrnaDistribution" + PARQUET_EXTENSION),
        MINUS_POSITON_TRNA_DISTRIBUTION("minusPositionTrnaDistribution" + PARQUET_EXTENSION),
        PLUS_POSITON_REPORIGIN_DISTRIBUTION("plusPositionReporiginDistribution" + PARQUET_EXTENSION),
        MINUS_POSITON_REPORIGIN_DISTRIBUTION("minusPositionReporiginDistribution" + PARQUET_EXTENSION),
        PLUS_POSITON_RRNA_DISTRIBUTION("plusPositionRrnaDistribution" + PARQUET_EXTENSION),
        MINUS_POSITON_RRNA_DISTRIBUTION("minusPositionRrnaDistribution" + PARQUET_EXTENSION),
        PLUS_POSITON_OTHER_DISTRIBUTION("plusPositionOtherDistribution" + PARQUET_EXTENSION),
        MINUS_POSITON_OTHER_DISTRIBUTION("minusPositionOtherDistribution" + PARQUET_EXTENSION);

        public final String identifier;


        SEQUENCE_MAP_PARQUET_NAMES(String identifier) {
            this.identifier = identifier;
        }
    }

    public enum RESULT_IDENTIFIER {
        POSITION_DATA,
        RANGE_DATA;
    }

    /*** Initialization ******************************************************************************************************************************/

    public SequenceMapComputer(String directory) {
        super(directory);
    }

    /*** IO ******************************************************************************************************************************/
    private Dataset<PositionCountIdentifierDT> fetchPositionRecordCountDistribution(boolean strand, boolean order, boolean filterZeroCount) {
        Dataset<PositionCountIdentifierDT> positionDataDataset = Spark.getInstance().read().parquet(getPositionRecordCountDistributionName(strand)).as(POSITION_DATA_ENCODER);

        if(filterZeroCount) {

            positionDataDataset = positionDataDataset.filter(col(ColumnIdentifier.COUNT).gt(0));

        }
        if(order) {
            return positionDataDataset.orderBy(positionDataDataset.col(ColumnIdentifier.POSITION));
        }
        return positionDataDataset;
    }
    public Dataset<PositionCountIdentifierDT> fetchPositionProteinDistribution(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionProteinDistributionName(strand)).as(POSITION_DATA_ENCODER);
    }
    public Dataset<PositionCountIdentifierDT> fetchPositionTrnaDistribution(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionTrnaDistributionName(strand)).as(POSITION_DATA_ENCODER);
    }
    public Dataset<PositionCountIdentifierDT> fetchPositionRrnaDistribution(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionRrnaDistributionName(strand)).as(POSITION_DATA_ENCODER);
    }
    public Dataset<PositionCountIdentifierDT> fetchPositionReporiginDistribution(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionRepOriginDistributionName(strand)).as(POSITION_DATA_ENCODER);
    }
    public Dataset<PositionCountIdentifierDT> fetchPositionOtherTypeDistribution(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionOtherDistributionName(strand)).as(POSITION_DATA_ENCODER);
    }
    public Dataset<Integer> fetchCompletePositiondata(boolean strand) {
        return Spark.getInstance().read().parquet(getPositionRecordCountDistributionName(strand)).
                select(col(ColumnIdentifier.POSITION)).
                orderBy(col(ColumnIdentifier.POSITION)).as(Encoders.INT());
    }


    public void persistPositionRecordCountDistribution(List<PositionCountIdentifierDT> positionDistribution, boolean strand) {
        Spark.getInstance().createDataFrame(positionDistribution, PositionCountIdentifierDT.class).write().
                parquet(getPositionRecordCountDistributionName(strand));
    }

    public void persistPositionPropertyDistribution(List<PositionCountIdentifierDT> positionDistribution, String parquet_name) {
        Spark.getInstance().createDataFrame(positionDistribution, PositionCountIdentifierDT.class).write().
                parquet(parquet_name);
    }

    /*** Analysis ******************************************************************************************************************************/

    public Map<String,ResultDataset> createFilteredSubsequencesForPositionDistributionData(int minCount, int minLength,
                                                                                           Dataset<PositionCountDT> positionData) {


        // filter positiondata below minCount
        Dataset<PositionCountDT> filteredPositionData = SubsequenceAnalyzer.filterPositionDistributionDataMinCount(positionData,minCount);

        // create ranges for filtered positiondata
        Dataset<RangeDT> rangeData = SubsequenceAnalyzer.createRanges(filteredPositionData);

        // if ranges are to be filtered based on minLength
        if(minLength > 1) {

            // remove all ranges with length<minLength
            rangeData = SubsequenceAnalyzer.filterRangeDataMinLength(rangeData,minLength);
            // remove all rows with positions corresponding to ranges with length<minLength
            filteredPositionData = SubsequenceAnalyzer.filterPositionDistributionDataMinLength(filteredPositionData,rangeData,minLength);
        }


        Map<String,ResultDataset> rangeStatisticDatasets = new HashMap<>();
        ResultDataset positionDataResult = new ResultDataset(
                filteredPositionData,
                "filteredPositionData",
                "Filtered PositionData dataset based on minCount and minLength");
        ResultDataset rangeDataResult = new ResultDataset(
                rangeData,
                "rangeData",
                "Range dataset of filtered positiondata dataset");

        rangeStatisticDatasets.put(positionDataResult.getIdentifier(),positionDataResult);
        rangeStatisticDatasets.put(rangeDataResult.getIdentifier(),rangeDataResult);


        return rangeStatisticDatasets;
    }

    public Map<RESULT_IDENTIFIER,Dataset> createFilteredSubsequencesForPositionDistributionData(int minCount, int minLength,
                                                                                                Dataset<PositionCountDT> positionData,
                                                                                                Dataset<RangeDT> rangeData) {


        // filter positiondata below minCount
        Dataset<PositionCountDT> filteredPositionData = SubsequenceAnalyzer.filterPositionDistributionDataMinCount(positionData,minCount);
        Dataset<RangeDT> filteredRangeData = rangeData;

        // if ranges are to be filtered based on minLength
        if(minLength > 1) {

            // remove all ranges with length<minLength
            filteredRangeData = SubsequenceAnalyzer.filterRangeDataMinLength(rangeData,minLength);
            // remove all rows with positions corresponding to ranges with length<minLength
            filteredPositionData = SubsequenceAnalyzer.filterPositionDistributionDataMinLength(filteredPositionData,rangeData,minLength);
        }


        Map<RESULT_IDENTIFIER,Dataset> datasetMap = new HashMap<>();

        datasetMap.put(RESULT_IDENTIFIER.POSITION_DATA, filteredPositionData);
        datasetMap.put(RESULT_IDENTIFIER.RANGE_DATA, filteredRangeData);


        return datasetMap;
    }

    public static <T> Dataset<RangeDT> createInverseRanges(Dataset<T> positions, Dataset<PositionCountDT> filteredPositionData, Class<T> tClass){

        if(tClass.equals(Integer.class)) {
            return createInverseRanges((Dataset<Integer>)positions,filteredPositionData);
        }
        else if(tClass.equals(PositionCountDT.class) || tClass.equals(PositionCountIdentifierDT.class)) {
            return createInverseRanges(positions.select(ColumnIdentifier.POSITION).as(Encoders.INT()),filteredPositionData);
        }
        else {
            return null;
        }
    }

    private static Dataset<RangeDT> createInverseRanges(Dataset<Integer> positions, Dataset<PositionCountDT> filteredPositionData){

        Dataset<Integer> inverseFilteredPositionData =
                        positions.
                        except(filteredPositionData.select(ColumnIdentifier.POSITION).
                                as(Encoders.INT())).orderBy(col(ColumnIdentifier.POSITION));

        return SubsequenceAnalyzer.createRangesForPositions(inverseFilteredPositionData);
    }

    public static Map<String,ResultDataset> createInverseFilteredSubsequencesForPositionRecordCountDistribution(Dataset<PositionCountDT> positionData,
                                                                                                         Dataset<PositionCountDT> filteredPositionData) {

        // create positiondata for remaining data
        Dataset<PositionCountDT> inverseFilteredPositionData =
                positionData.except(filteredPositionData);

        // create rangeData for remaining data
        Dataset<RangeDT> inverseRangeData = SubsequenceAnalyzer.createRanges(inverseFilteredPositionData);



        Map<String,ResultDataset> rangeStatisticDatasets = new HashMap<>();

        ResultDataset inversePositionDataResult = new ResultDataset(
                inverseFilteredPositionData,
                "inverseFilteredPositionData",
                "Remaining PositionData dataset (part that is filtered out)");

        ResultDataset inverseRangeDataResult = new ResultDataset(
                inverseRangeData,
                "inverseRangeData",
                "Range dataset of inverse filtered positiondata dataset");


        rangeStatisticDatasets.put(inversePositionDataResult.getIdentifier(),inversePositionDataResult);
        rangeStatisticDatasets.put(inverseRangeDataResult.getIdentifier(),inverseRangeDataResult);

        return rangeStatisticDatasets;
    }

    public static Map<String,ResultDataset> analyzeSubsequencesForPositionRecordCountDistribution(Dataset<PositionCountDT> positionData,
                                                         Dataset<RangeDT> rangeData) {

        Dataset<RangeStatisticsDT> rangeStatistics = SubsequenceAnalyzer.createRangeStatistics(positionData,rangeData,2);
        System.out.println("rangeStatistics");
        rangeStatistics.show();

        Dataset<Row> lengthStatistics = SubsequenceAnalyzer.createStatisticsForRangeStatistics(rangeStatistics, ColumnIdentifier.LENGTH,2);
        System.out.println("lengthStatistics");
        lengthStatistics.show();
        Dataset<Row> lengthCountDistribution = SubsequenceAnalyzer.createLengthCountDistribution(rangeStatistics);
        System.out.println("lengthCountDistribution");
        lengthCountDistribution.show();

        Dataset<Row> meanRecordCountStatistics = SubsequenceAnalyzer.createStatisticsForRangeStatistics(rangeStatistics, RangeStatisticsDT.COLUMN_NAME.MAXCOUNT.identifier,2);
        System.out.println("meanRecordCountStatistics");
        meanRecordCountStatistics.show();
        rangeStatistics.show();
        Dataset<Row> meanRecordCountCountDistribution = SubsequenceAnalyzer.createMeanRecordCountCountDistribution(rangeStatistics,1);
        System.out.println("meanRecordCountCountDistribution");
        meanRecordCountCountDistribution.show();

        Dataset<Row> recordCountStatistics = SubsequenceAnalyzer.createStatisticsForPositionnDistributionData(positionData, ColumnIdentifier.COUNT,2);
        System.out.println("recordCountStatistics");
        recordCountStatistics.show();
        Dataset<Row> recordCountCountDistribution = SubsequenceAnalyzer.createRecordCountCountDistribution(positionData);
        System.out.println("recordCountCountDistribution");
        recordCountCountDistribution.show();


        Map<String,ResultDataset> results = new HashMap<>();
        ResultDataset rangeStatisticsResult = new ResultDataset(rangeStatistics, "rangeStatistics", "Statistics for each range of the range dataset");

        ResultDataset lenghtStatisticsResult = new ResultDataset(lengthStatistics,"lengthStatistics", "Statistics for lengths of ranges of the range dataset");
        ResultDataset lengthCountDistributionResult = new ResultDataset(lengthCountDistribution, "lengthCountDistribution","Count distribution of lengths of ranges of the range dataset");

        ResultDataset meanRecordCountStatisticsResult = new ResultDataset(meanRecordCountStatistics,"meanRecordCountStatistics", "Statistics for meanRecordCounts of ranges of the range dataset");
        ResultDataset meanRecordCountCountDistributionResult = new ResultDataset(meanRecordCountCountDistribution, "meanRecordCountCountDistribution","Count distribution of recordCounts of ranges of the range dataset");

        ResultDataset recordCountStatisticsResult = new ResultDataset(recordCountStatistics,"recordCountStatistics", "Statistics for recordCounts of the positionData dataset");
        ResultDataset recordCountCountDistributionResult = new ResultDataset(recordCountCountDistribution,"recordCountCountDistribution", "CountDistribution for recordCounts of the positionData dataset");


        results.put(rangeStatisticsResult.getIdentifier(),rangeStatisticsResult);
        results.put(lenghtStatisticsResult.getIdentifier(),lenghtStatisticsResult);
        results.put(lengthCountDistributionResult.getIdentifier(),lengthCountDistributionResult);
        results.put(meanRecordCountStatisticsResult.getIdentifier(),meanRecordCountStatisticsResult);
        results.put(meanRecordCountCountDistributionResult.getIdentifier(),meanRecordCountCountDistributionResult);
        results.put(recordCountStatisticsResult.getIdentifier(),recordCountStatisticsResult);
        results.put(recordCountCountDistributionResult.getIdentifier(),recordCountCountDistributionResult);

        return results;
    }



    /*** Auxiliary ******************************************************************************************************************************/

    private String getParquetName(SEQUENCE_MAP_PARQUET_NAMES name) {
        return directory + "/" + name;
    }

    public String getPositionRecordCountDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITION_RECORD_COUNT_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITION_RECORD_COUNT_DISTRIBUTION);
    }

    public String getPositionProteinDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITON_PROTEIN_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITON_PROTEIN_DISTRIBUTION);
    }

    public String getPositionTrnaDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITON_TRNA_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITON_TRNA_DISTRIBUTION);
    }

    public String getPositionRrnaDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITON_RRNA_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITON_RRNA_DISTRIBUTION);
    }

    public String getPositionRepOriginDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITON_REPORIGIN_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITON_REPORIGIN_DISTRIBUTION);
    }

    public String getPositionOtherDistributionName(boolean strand) {
        return strand ?
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.PLUS_POSITON_OTHER_DISTRIBUTION) :
                getParquetName(SEQUENCE_MAP_PARQUET_NAMES.MINUS_POSITON_OTHER_DISTRIBUTION);
    }


    /*** Getter and Setter ******************************************************************************************************************************/



    public static void main(String[] args) {
//        String directory = "/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/graphFiles/NC_026039.1";
        String keyspace = "sequence_map_0";
        String id = "NC_026039.1";
        String directory = "/home/lisa/Documents/MITOS_Project/MITOS_gdb/project/graphFiles/" + id;
//        String [] names = positionDataEncoder.schema().fieldNames();
//        for(String name: names) {
//            System.out.println(name);
//        }

        SequenceMapComputer sparkSequenceMapConnection = new SequenceMapComputer(directory);
//        Dataset<PositionDistributionData> positionDistributionDataDataset = sparkSequenceMapConnection.getRecordCountPositionDistribution(true);
//        SparkConnection.showAll(sparkSequenceMapConnection.getPlusPositionRecordCountDistribution(false).groupBy(SubsequenceAnalyzer.PositionData.getIdentifierString()).count().as("count").orderBy(col("count")));
        Dataset<PositionCountIdentifierDT> positionDataDataset =  sparkSequenceMapConnection.fetchPositionReporiginDistribution(true);
        positionDataDataset.show(500);

        SubsequenceAnalyzer.createRanges(positionDataDataset).show();



    }
}

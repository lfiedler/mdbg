package de.uni_leipzig.informatik.pacosy.mitos.core.util.dataTypes.serializables;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

public class ScoredPositionPositionKmerMappingDT extends ScoredPositionPositionMappingDT {


    protected String kmer;
    public static final Encoder<ScoredPositionPositionKmerMappingDT> ENCODER = Encoders.bean(ScoredPositionPositionKmerMappingDT.class);

    public ScoredPositionPositionKmerMappingDT(int recordId, int position, int recordPosition, boolean strand, int score, double eValue, String kmer) {
        super(recordId, position, recordPosition, strand,score,eValue);

        this.kmer = kmer;

    }

    public ScoredPositionPositionKmerMappingDT() {
    }


    public String getKmer() {
        return kmer;
    }

    public void setKmer(String kmer) {
        this.kmer = kmer;
    }


}

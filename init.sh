#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`
PROJECT_DIR=${BIN}
PSQL_HOST=127.0.0.1
PSQL_PORT=5432
PSQL_PW=MITOS
VALID_OPTS=h; 
# Options requiring an argument
VALID_LONG_OPTS+=,psql-host:;
VALID_LONG_OPTS+=,psql-port:;
VALID_LONG_OPTS+=,psql-pw:;

# No argument options
VALID_LONG_OPTS+=,help;


echo "Setting project directory to ${PROJECT_DIR}"
sed -E -i -e "s#(<project.directory>).*(</project.directory>)#\1${PROJECT_DIR}\2#" ${BIN}/programs/core/pom.xml


usage() {
    echo "Usage: $0 [options] {psql-host|psql-port|psql-pw}" >&2
    echo " --psql-host:  Specify postgresql host (default localhost)" >&2
    echo " --psql-port:  Specify postgresql hostport (default 5432)" >&2
    echo " --psql-host:  Specify postgresql password (default no password)" >&2
}

exit_abnormal() {                              # Function: Exit with error.
  usage
  exit 1
}

exit_normal() {                              # Function: Exit without error.
  usage
  exit 0
}

# By calling getopt with '--options' we activate the extended mode which can
# also handle spaces and other special characters in the parameters
# this will extract the parameters as list in "PARSED"
PARSED=$(getopt --options=${VALID_OPTS} --longoptions=${VALID_LONG_OPTS} --name "$0" -- "$@")



# Set the positional parameters. By using eval we achieve, that the quoted
# output of getopt is interpreted another time by the shell (see man getopt)
eval set -- "$PARSED"

while true; do
  case "$1" in
    -h|--help)
        # print help message and exit
        echo "Help"
        exit_normal
        ;;
    --psql-host)
	PSQL_HOST=$2
        echo "Setting psql host to ${PSQL_HOST} 
        shift 1
        ;;
    --psql-port)
	PSQL_PORT=$2
        echo "Setting psql port to ${PSQL_PORT} 
        shift 1
        ;;
     --psql-pw)
	PSQL_PW=$2
        echo "Setting psql pw to ${PSQL_PW} 
        shift 1
        ;;  

    --)
        shift 1
        break
        ;;
     *)  echo "How could that happen? " $1
        exit_abnormal;;
  esac
done


if [ -n "$COMMAND" ]; then
    $COMMAND
else
    usage
    exit 1
fi

echo "Creating db"
./createDB.sh ${PSQL_HOST} ${PSQL_PORT} ${PSQL_PW}

echo "Setting psql properties"

sed -E -i -e "s#(host=).*#\1${PSQL_HOST}#" ${BIN}/programs/core/src/main/resources/psql.properties
sed -E -i -e "s#(port=).*#\1${PSQL_PORT}#" ${BIN}/programs/core/src/main/resources/psql.properties
sed -E -i -e "s#(password=).*#\1${PSQL_PW}#" ${BIN}/programs/core/src/main/resources/psql.properties
#cp psql.properties ${BIN}/programs/core/src/main/resources



# MDBG
A tool for annotation of mitochondrial genomes

## Prerequisites
- Java 8 (jdk + jre)
- PostgresSQL >= 9
- Maven

## Set up and installation

Setting project parameters and database initialization
- Move this folder to the directory from which you plan to execute this program
- Run ./init.sh for database setup (creation of database + schema). If you want to use different  
postgresql access data (host, port, password)  other than default values change variables (PSQL_HOST,PSQL_PORT,PSQL_PW) as fit BEFORE script execution.
 Do NOT move the project folder after script execution, as this sets the project directory for the java programs.


## Database population

Graph creation, i.e., population of the database with all 8015 genomes of RefSeq89.  This is a perquisite for all following options (*Gene cluster construction for RefSeq genomes*, *Annotation evaluation for RefSeq genomes*, *Gene cluster construction for provided genome*, *Dendogram analysis in a gremlin console*). The database takes 52 GB of memory.
1. This can either be done from a copy (can be downloaded from https://doi.org/10.5281/zenodo.4632893). Remark: the copy file is considerably smaller than 52 GB, since indices are still to be generated (will automatically happen by launching the script below).

    **Execute:**
 ./run.sh --create-db-from-copy &lt;name of db copy&gt;

2. Or the database can be generated from anew. WARNING: This process takes several hours. The data needed (GB,FASTA, bed Files) resides in subfolder *sequenceData*.

    **Execute:**
 ./run.sh --create-db


    **Results:**
    Database MDBG with schemas *complete* and *extracted*. In schema *complete* the following tables should have been created:
    - "record"
    - "genome_plus"
    - "genome_minus"
    - "kmer_plus_16"
    - "kmer_minus_16"
    - "record_property_plus"
    - "record_property_minus"

## Gene cluster construction for RefSeq genomes

Gene dendogram creation  (subgraph construction, LMC-BRIDGING, ClusterG) for all 100 RefSeq genomes used in the publication. A list of all associated genomes (by NCBI accession id) is stored in subfolder *graphFiles* in file records.txt. To only execute the routines for some of the 100 genomes, simply adapt this file (i.e., delete entries).

**Execute:**
./run.sh --create-annotations

**Results:**
For each considered genome, a folder named with the respective NCBI accession identifier is created in subfolder *graphFiles*. In it you should find:
- Subfolder *parquets* with parquet files:
            SEQUENCE_DATA (comprises e.g. sequence length, accession code), PLUS_GENOME (nucleotide sequence of positive strand), MINUS_GENOME (nucleotide sequence of negative strand), SEQUENCE_KMERS (all generated (k+1)-mers of the sequence), RECORD_KMERS (all mapping (k+1)-mers of MDBG), MAPPED_RANGES (all MR prior to bridging), BRIDGED_RANGES (additional MR discovered by bridging)
- Subfolder *propertyCluster* with files/folders:
            minusCluster.json (initial cluster for annotation of negative strand), minusClusterTree.gryo (constructed dendogram for annotation of negative strand),
            plusCluster.json (initial cluster for annotation of positive strand), plusClusterTree.gryo (constructed dendogram for annotation of positive strand),
            subfolders for each of the alpha values (0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1). Each alpha values subfolder should contain: plusAnnotation (gene annotation of plus strand), minusAnnotation (gene annotation of minus strand)

## Annotation evaluation for RefSeq genomes

This requires that annotations were created (by *Gene annotation for RefSeq*) beforehand.
Comparison of annotations with associated RefSeq annotations as described in the publication. For each of the genomes and every alpha (0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1) the Jaccard indices J* are computed and stored along with genes g and g_{ref}.

**Execute:**
./run.sh --evaluate-annotations

**Results:**
    - For each genome in each of the alpha subfolders in folder *propertyCluster* a file propertyAgreement (gene pairings with J*)
    - In folder graphData a subfolder MERGED_JACCARD_STATISTIC,  containing all propertyAgreements for every genome per alpha. In each such alpha subfolder, the results are once stored as CSV and once as parquet.

## Gene cluster construction for provided genome

Gene dendogram creation  (subgraph construction, LMC-BRIDGING, ClusterG) for a mitochondrial genome by supplying its nucleotide sequence stored in a FASTA file.

**Execute:**
 ./run.sh --create-annotations-fasta &lt;path-to-fasta-file&gt;

**Results:**
See *Gene cluster construction for RefSeq genomes*

## Show created annotation

Shows the generated annotation, i.e. entries of related parquet files (as created by *Gene cluster construction for RefSeq genomes* or *Gene cluster construction for provided genome*) for supplied genome (by accession id). This does not filter out gene clusters of small size.

**Execute:**
 ./run.sh --show-annotations &lt;accession ID&gt;. Here &lt;accession ID&gt; should comply with the name of folder generated in the *graphFiles* folder during the previous steps.

## Dendogram analysis in a gremlin console

This requires that annotations were created (by *Gene cluster construction* or *Gene cluster construction for provided genome*) beforehand.
Load both dendograms (of positive and negative strand) into a gremlin console. The graph traversals are readily provided as *gPlus* and *gMinus* for the positive and negative strand respectively.

**Execute:**
 ./run.sh --study-dendograms &lt;accession ID&gt;. Here &lt;accession ID&gt; should comply with the name of folder generated in the *graphFiles* folder during the previous steps.

## Result data sets
The following can be downloaded from https://doi.org/10.5281/zenodo.4632893
- Result data sets of all 100 RefSeq genomes used in the publication (comprising the jaccard statistic file as CSV, and gapcount statistic file as CSV)
- Compressed sql script (db) for database creation (for 1. in *Database population*)

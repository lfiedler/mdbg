conf = new BaseConfiguration()
conf.setProperty("gremlin.tinkergraph.defaultVertexPropertyCardinality","list")
tinkerGraph = TinkerGraph.open(conf)
gPlus = tinkerGraph.traversal()
gPlus.io(
"/home/lisa/Documents/MITOS_Project/publish/graphFiles/NC_003061/propertyCluster/plusClusterTree.gryo").with(IO.reader,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName()).
                read().iterate()
tinkerGraph2 = TinkerGraph.open(conf)
gMinus = tinkerGraph2.traversal()
gMinus.io(
"/home/lisa/Documents/MITOS_Project/publish/graphFiles/NC_003061/propertyCluster/minusClusterTree.gryo").with(IO.reader,IO.gryo).
                with(IO.registry, JanusGraphIoRegistry.class.getName()).
                read().iterate()
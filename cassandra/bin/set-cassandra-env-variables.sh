#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: https://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`

CASSANDRA_HOME=$(cd `abs_path`/.. && pwd)

# The directory where Cassandra's configs live (required)
CASSANDRA_CONF=$CASSANDRA_HOME/conf/cassandra

# This can be the path to a jar file, or a directory containing the
# compiled classes. NOTE: This isn't needed by the startup script,
# it's just used here in constructing the classpath.
cassandra_bin="$CASSANDRA_HOME/build/classes/main"
cassandra_bin="$cassandra_bin:$CASSANDRA_HOME/build/classes/thrift"
#cassandra_bin="$cassandra_home/build/cassandra.jar"

# the default location for commitlogs, sstables, and saved caches
# if not set in cassandra.yaml
cassandra_storagedir="$CASSANDRA_HOME/data"

# This system property is referenced in log4j-server.properties
logdir="$CASSANDRA_HOME/log"

runningdir="$CASSANDRA_HOME/running"

pidlog="${runningdir}/cassandraPID.log"
portlog="${runningdir}/cassandraPorts.log"

# Elasticsearch related parameters
ELASTICSEARCH_HOME="${BIN}/../elasticsearch"
ELASTICSEARCH_BIN="${ELASTICSEARCH_HOME}/bin"
ELASTICSEARCH_CONF="${ELASTICSEARCH_HOME}/config"

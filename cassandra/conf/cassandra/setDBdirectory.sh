#!/bin/bash

# Returns the absolute path of this script regardless of symlinks
abs_path() {
    # From: http://stackoverflow.com/a/246128
    #   - To resolve finding the directory after symlinks
    SOURCE="${BASH_SOURCE[0]}"
    while [ -h "$SOURCE" ]; do
        DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
        SOURCE="$(readlink "$SOURCE")"
        [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
    done
    echo "$( cd -P "$( dirname "$SOURCE" )" && pwd )"
}

BIN=`abs_path`

# extract current directory
DB_NAME_CURRENT=$(grep "/cassandra/data" ${BIN}/cassandra.yaml | sed -E -e 's#\- (.*)(/cassandra/data)#\1#')
echo "Current directory: ${DB_NAME_CURRENT}"

if [ $# -ne 0 ]
  then
    echo "Provided arguments"
    DB_NAME_NEW=$1

    if [ ${DB_NAME_NEW} == ${DB_NAME_CURRENT} ]; then
      echo "directories already match!"
    else
      # replace data_file_directories entry:
      LINE=$(($(grep -E "data_file_directories:" ${BIN}/cassandra.yaml -n | cut -f1 -d:)+1))
      sed -i -E -e ${LINE}'s#\-.*(/cassandra/data)#- '$1'\1#' ${BIN}/cassandra.yaml

      # replace commitlog_directory entry:
      sed -i -E -e 's#(commitlog_directory:\s*).*(/cassandra/commitlog)$#\1'$1'\2#' ${BIN}/cassandra.yaml


      # replace saved_caches_directory:
      sed -i -E -e 's#(saved_caches_directory:\s*).*(/cassandra/saved_caches)$#\1'$1'\2#' ${BIN}/cassandra.yaml
       echo "Set directory to $1"
    fi
fi
